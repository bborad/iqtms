using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
//using Ramp.ModbusConnector.IQTMSModbusServer;
using Ramp.Modbus;

 

namespace Ramp.ModbusConnector
{
    public class ModbusServer
    {
        //private static NLog.Logger logger = NLog.LogManager.GetLogger("Ramp.ModbusConnector.ModbusServer");
        //static IQTMSModbusServer.ReaderService objModbusServer;

        static Modbus.ModbusServer objModbusServer;

        static string WebURL { get; set; }

        static string UserName { get; set; }
        static string Password { get; set; }

        public ModbusServer()
        {
            try
            {
                InstantiateModbusServer();
            }
            catch (Exception ex)
            {
               //logger.ErrorException(ex.Message, ex);
            }
        }

        public   void InstantiateModbusServer()
        {
            try
            {
                //UserName = ConfigurationSettings.AppSettings["UserName"];
                //Password = ConfigurationSettings.AppSettings["Password"];

                //if (WebURL == null)
                //{
                //    try
                //    {
                //        if (ConfigurationSettings.AppSettings["WebURL"] != null)
                //        {
                //            WebURL = ConfigurationSettings.AppSettings["WebURL"].ToString();

                //        }
                //    }
                //    catch
                //    {

                //    }
                //}

                if (objModbusServer == null)
                {
                    objModbusServer = new  Modbus.ModbusServer();

                    //if (WebURL.Length > 0)
                    //{
                    //    objModbusServer.Url = WebURL;

                    //    if (UserName != null && UserName.Trim().Length > 0)
                    //    {
                    //        try
                    //        {
                    //            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(UserName, Password);
                    //            objModbusServer.Credentials = credentials;
                    //        }
                    //        catch (Exception)
                    //        {
                    //        }
                    //    }

                    //}
                }
            }
            catch
            {
            }
        }

        public  short ReadBoom_Gate(byte theGate)
        {
            short result = -1;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();                   
                } 
                
                result = objModbusServer.ReadBoom_Gate(theGate);
                
            }
            catch
            {
            }
            return result;
        }
        public   void SetTCPPort(int thePort)
        {
           
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                objModbusServer.SetTCPPort(thePort);

            }
            catch
            {
            }
           
        }
        //public byte StartModbusServer(byte theConnection);
        public byte StopModbusServer()
        {
            byte result = 0;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.StopModbusServer();

            }
            catch
            {
            }
            return result;
        }


        public   short ReadPAD_Match(byte thePAD)
        {
            short result = -1;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.ReadPAD_Match(thePAD);

            }
            catch
            {
            }
            return result;
        }
        public   short ReadPAD_Start(byte thePAD)
        {
            short result = -1;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.ReadPAD_Start(thePAD);

            }
            catch
            {
            }
            return result;
        }
        public   short ReadPAD_Status(byte thePAD)
        {

            short result = -1;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.ReadPAD_Status(thePAD);

            }
            catch
            {
            }
            
            return result;
        }

        public   byte WriteBoom_Gate(byte theGate, short theValue)
        {
            byte result = 0;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.WriteBoom_Gate(theGate,  theValue);

            }
            catch
            {
            }
            return result;
        }
        public   byte WritePAD_Match(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.WritePAD_Match(thePAD, theValue);

            }
            catch
            {
            }
            return result;
        }
        public   byte WritePAD_Start(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                result = objModbusServer.WritePAD_Start(thePAD, theValue);

            }
            catch
            {
            }
            return result;
        }

        public short[] ReadPAD_StatusALL(byte[] allPAD)
        {
            short[] result = new short[allPAD.Length];

            byte thePAD = 0;

            try
            {
                if (objModbusServer == null)
                {
                    InstantiateModbusServer();
                }

                for (int i = 0; i <= allPAD.Length; i++ )
                {
                    thePAD = allPAD[i];
                    result[i] = objModbusServer.ReadPAD_Status(thePAD);
                }

            }
            catch
            {
            }
            return result;
        }

    }
}
