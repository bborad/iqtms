using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.Modbus;

namespace Ramp.ModbusConnector
{
    public class ModbusClient
    {

        Ramp.Modbus.ModbusClient mClient;

        public ModbusClient()
        {
            try
            {
                if (mClient == null)
                {
                    mClient = new Ramp.Modbus.ModbusClient();
                    IsConnected = mClient.IsConnected;

                    if (!IsConnected)
                    {
                        throw new Exception("Unable to connect Modbus Server");
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static bool IsConnected { get; set; }

        public short[] ReadAllPadMatch()
        {
            short[] result = null;
            try
            {
                if (IsConnected)
                    result = mClient.ReadAllPadMatch();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public short[] ReadAllPadStart()
        {
            short[] result = null;
            try
            {

                if (IsConnected)
                    result = mClient.ReadAllPadStart();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public short[] ReadAllPadStatus()
        {
            short[] result = null;
            try
            {
                if (IsConnected)
                    result = mClient.ReadAllPadStatus();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public bool DisconnectModbusServer()
        {
            bool result = true;
            try
            {
                if (mClient != null && IsConnected)
                {
                   result = mClient.DisconnectModbusServer();
                }
            }
            catch
            {
                
            }
            return result;
        }

    }
}
