using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RMC = Ramp.Modbus.Connector;
using RMU = Ramp.Modbus.Utility;
using System.Diagnostics;

namespace Ramp.Modbus
{
    public class ModbusServer
    {
        private static RMC.Processor mServer { get; set; }

        public static bool IsConnected { get; set; }

        public ModbusServer()
        {
            InitiateModbusServer();      
        }

        protected void InitiateModbusServer()
        {
            try
            {
                if (mServer == null)
                {
                    mServer = new Ramp.Modbus.Connector.Processor();
                    byte status = mServer.StartModbusServer(RMU.ModbusConnectionTypes.TCPIP);

                    if (status == 0)
                    {
                        IsConnected = true;
                    }
                }            
            }
            catch (Exception e)
            {
                WriteToEventLog("InitiateModbusServer -- " + e.Message, EventLogEntryType.Error);
            }
        }

        public short ReadBoom_Gate(byte theGate)
        {
            short result = -1;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();   
                }

                result = mServer.ReadBoom_Gate(theGate);
            }
            catch (Exception e)
            {
                WriteToEventLog("ReadBoom_Gate -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }
        public  void SetTCPPort(int thePort)
        {
        
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                 mServer.SetTCPPort(thePort);
            }
            catch (Exception e)
            {
                WriteToEventLog("SetTCPPort -- " + e.Message, EventLogEntryType.Error);
            }
             
        }
        //public byte StartModbusServer(byte theConnection);
        public byte StopModbusServer()
        {
            byte result = 0;
            try
            {   

                 result = mServer.StopModbusServer();
                 IsConnected = false;
                 mServer = null;
            }
            catch (Exception e)
            {
                WriteToEventLog("StopModbusServer -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }

        public   short ReadPAD_Match(byte thePAD)
        {
            short result = -1;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.ReadPAD_Match(thePAD);
            }
            catch (Exception e)
            {
                WriteToEventLog("ReadPAD_Match -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }
        public   short ReadPAD_Start(byte thePAD)
        {
            short result = -1;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.ReadPAD_Start(thePAD);
            }
            catch (Exception e)
            {
                WriteToEventLog("ReadPAD_Start -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }
        public   short ReadPAD_Status(byte thePAD)
        {
            short result = -1;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.ReadPAD_Status(thePAD);
            }
            catch (Exception e)
            {
                WriteToEventLog("ReadPAD_Status -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }

        public   byte WriteBoom_Gate(byte theGate, short theValue)
        {
            byte result = 0;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.WriteBoom_Gate(theGate, theValue);
            }
            catch (Exception e)
            {
                WriteToEventLog("WriteBoom_Gate -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }
        public   byte WritePAD_Match(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.WritePAD_Match(thePAD, theValue);
            }
            catch (Exception e)
            {
                WriteToEventLog("WritePAD_Match -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }
        public   byte WritePAD_Start(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                if (IsConnected == false)
                {
                    InitiateModbusServer();
                }

                result = mServer.WritePAD_Start(thePAD, theValue);
                
            }
            catch(Exception e)
            {
                WriteToEventLog("WritePAD_Start -- " + e.Message, EventLogEntryType.Error);
            }
            return result;
        }


        private void WriteToEventLog(string strLogEntry, EventLogEntryType eType)
        {
            try
            {
                string strSource = "IQTMS Modbus Functionality"; //name of the source
                string strLogType = "IQTMSLog"; //type of the log
                string strMachine = "."; //machine name

                if (!EventLog.SourceExists(strSource, strMachine))
                {
                    EventLog.CreateEventSource(strSource, strLogType);
                }

                EventLog eLog = new EventLog(strLogType, strMachine, strSource);
                eLog.WriteEntry(strLogEntry, eType, 1000);
            }
            catch
            {
            }
        }
    }
}
