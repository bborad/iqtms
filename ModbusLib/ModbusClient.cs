using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RMC = Ramp.ModbusMaster.Connector;
using System.Diagnostics;


namespace Ramp.Modbus
{
    public class ModbusClient
    {
        private RMC.Processor mClient { get; set; } 

        public  bool IsConnected { get; set; }

        public ModbusClient()
        {
            InitiateModbusClient();
        }

        protected void InitiateModbusClient()
        {
            try
            {
                if (mClient == null)
                {
                    mClient = new RMC.Processor();

                    mClient.SetTimeout(1500);             

                    int status = mClient.ConnectModbusServer();  

                    if (status == 0)
                    {
                        IsConnected = true;
                    }
                    else
                    {                       
                        status = mClient.ConnectModbusServer();
                        if (status == 0)
                        {
                            IsConnected = true;
                        }
                        else
                        {
                            WriteToEventLog("InitiateModbusClient -- Unable to connect to server", EventLogEntryType.Warning);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteToEventLog("InitiateModbusClient -- " + e.Message, EventLogEntryType.Error);
            }
        }

        public short[] ReadAllPadMatch()
        {
            short[] result = null;
            try
            {
                result = mClient.ReadModbusRegister(1, 1);

            }
            catch (Exception ex)
            {
                WriteToEventLog("ReadAllPadMatch -- " + ex.Message, EventLogEntryType.Error);
            }

            return result;
        }

        public short[] ReadAllPadStart()
        {
            short[] result = null;
            try
            {
                result = mClient.ReadModbusRegister(2, 1);

            }
            catch (Exception ex)
            {
                WriteToEventLog("ReadAllPadMatch -- " + ex.Message, EventLogEntryType.Error);
            }

            return result;
        }

        public short[] ReadAllPadStatus()
        {
            short[] result = null;
            try
            {
                result = mClient.ReadModbusRegister(3, 13);

            }
            catch (Exception ex)
            {
                WriteToEventLog("ReadAllPadMatch -- " + ex.Message, EventLogEntryType.Error);
            }

            return result;
        }

        public bool DisconnectModbusServer()
        {
            bool result = true;
            try
            {

                if (mClient != null && IsConnected)
                {
                    mClient.DisconnectModbusServer();                    
                }
            }
            catch (Exception ex)
            {
                WriteToEventLog("DisconnectModbusServer -- Unable to disconnect to server " + ex.Message , EventLogEntryType.Warning);
                result = false;
            }
            return result;
        }

        private void WriteToEventLog(string strLogEntry, EventLogEntryType eType)
        {
            try
            {
                string strSource = "IQTMS Modbus Client"; //name of the source
                string strLogType = "IQTMSLog"; //type of the log
                string strMachine = "."; //machine name

                if (!EventLog.SourceExists(strSource, strMachine))
                {
                    EventLog.CreateEventSource(strSource, strLogType);
                }

                EventLog eLog = new EventLog(strLogType, strMachine, strSource);
                eLog.WriteEntry(strLogEntry, eType, 1000);
            }
            catch
            {
            }
        }
    }
}
