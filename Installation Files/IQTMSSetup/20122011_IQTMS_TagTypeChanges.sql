
IF NOT EXISTS (SELECT * FROM syscolumns
  WHERE id=object_id('Tags') and NAME='TagType')
    ALTER TABLE Tags ADD TagType INT NULL

-------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_GetAllForTrafficPage]    Script Date: 12/20/2011 10:40:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Somkant Pathak
-- Create date: 17 August 2010
-- Description:	Get All Active Reads For Traffic Page
-- Modified By :- Vijay Mangtani
-- Modified On :- 25-11-2011
-- Modified On :- 16-12-2011
-- ===================================================
ALTER PROCEDURE [dbo].[ActiveReads_GetAllForTrafficPage]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT     ActiveReads.ID, ActiveReads.TagID, ActiveReads.HopperID,ActiveReads.FoundAtHopper, ActiveReads.StartTime, ActiveReads.REDS, ActiveReads.Queue, ActiveReads.Entry1, 
                      ActiveReads.Entry2, ActiveReads.HopperTime, ActiveReads.TruckWash,ActiveReads.ExitTime, ActiveReads.REDS2, ActiveReads.TruckCallUp, ActiveReads.LastPosition, 
                      ActiveReads.EntryORQueue, Tags.TagRFID,Tags.TruckID, Tags.TruckID + '-' + Proponents.ProponentName + '-' + right('0' + (Convert(varchar(4),ISNULL(TimeTable.DestinationPad,0))),2) AS TruckName, --Hoppers.HopperName
                      Tags.ProponentID,ISNULL(Tags.TagType,0) as TagType
	FROM         ActiveReads 
					  INNER JOIN Tags ON ActiveReads.TagID = Tags.ID
                      INNER JOIN Hoppers ON Hoppers.ID = ActiveReads.HopperID 
					  INNER JOIN Proponents ON Proponents.ID = Tags.ProponentID 
					  LEFT OUTER JOIN TimeTable ON TimeTable.TagID = ActiveReads.TagID
	ORDER BY ActiveReads.StartTime ASC

-------------------------------------------------------------------------------------------------------------------------

	SELECT  Top 30 TruckID,TruckID + '-' + ISNULL(ProponentName, '') + '-' + ISNULL(H.HopperName,'00') AS TrasactionName, 
			ArchiveReads.ID, ExitTime, ArchiveDate, TruckWash,ISNULL(TransactionID,0) as TransID,
			ISNULL(TransMessage, '') AS TransMessage, ISNULL(TransStatus, '') AS TransStatus
	FROM    ArchiveReads
	LEFT OUTER JOIN Hoppers H ON H.ID = FoundAtHopper
	--WHERE	ArchiveDate= GETDATE()
	ORDER BY ArchiveDate DESC

------------------------------------------------------------------------------------------------------------------------
	
	SELECT Count(*) as [Count] FROM Readers WHERE ReaderStatus is not NULL AND ReaderStatus <> 'OK' 

--------------------------------------------------------------------------------------------------------------------------

--	SELECT     ActiveReads.ID, ActiveReads.TagID, ActiveReads.HopperID,ActiveReads.FoundAtHopper, ActiveReads.StartTime, ActiveReads.REDS, ActiveReads.Queue, ActiveReads.Entry1, 
--                      ActiveReads.Entry2, ActiveReads.HopperTime, ActiveReads.TruckWash,ActiveReads.ExitTime, ActiveReads.REDS2, ActiveReads.TruckCallUp, ActiveReads.LastPosition, 
--                      ActiveReads.EntryORQueue, Tags.TagRFID,Tags.TruckID, Tags.TruckID + '-' + Proponents.ProponentName + '-' + right('0' + (Convert(varchar(4),ISNULL(TimeTable.DestinationPad,0))),2) AS TruckName, --Hoppers.HopperName
--                      Tags.ProponentID,ISNULL(Tags.TagType,0) as TagType
--	FROM         Hoppers INNER JOIN
--                      ActiveReads ON Hoppers.ID = ActiveReads.HopperID INNER JOIN
--                      Proponents INNER JOIN
--                      Tags ON Proponents.ID = Tags.ProponentID ON ActiveReads.TagID = Tags.ID
--					  LEFT OUTER JOIN TimeTable ON TimeTable.TagID = ActiveReads.TagID
--	ORDER BY ActiveReads.StartTime ASC


		
END

GO