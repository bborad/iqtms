
IF NOT EXISTS (SELECT * FROM syscolumns
  WHERE id=object_id('ExpectedTimes') and NAME='UnTarpTime')
    ALTER TABLE ExpectedTimes ADD UnTarpTime INT NULL
GO
-------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 18 August 2010
-- Description:	Update Expected Times
-- =============================================
ALTER PROCEDURE [dbo].[ExpectedTimes_UpdateExpectedTimes]
	-- Add the parameters for the stored procedure here
		 @ID int
		,@REDS_Queue int
		,@Queue_Ent1 int
		,@Ent1_Ent2 int
		,@QueueTimePerTruck int
        ,@Unloading int
        ,@StartUp int
        ,@ShutDown int
        ,@TruckCallResponse int
        ,@TruckQueueMax int
        --,@End_Transaction int
        --,@End_Transaction2 int
        ,@Exit_REDS int
        ,@TruckWash_REDS int
        ,@Precision int
        ,@TagReadFrequency int
		,@MaxDelay	int
		,@Exit_Ent1 int
		,@TruckWash_Ent1 int
		,@UnTarpTime int
AS
BEGIN
	 -- Insert statements for procedure here
	UPDATE [ExpectedTimes]
    SET [REDS_Queue] = @REDS_Queue
      ,[Queue_Ent1] = @Queue_Ent1
      ,[Ent1_Ent2] = @Ent1_Ent2
      ,[QueueTimePerTruck] = @QueueTimePerTruck
      ,[Unloading] = @Unloading
      ,[StartUp] = @StartUp
      ,[ShutDown] = @ShutDown
      ,[TruckCallResponse] = @TruckCallResponse
      ,[TruckQueueMax] = @TruckQueueMax
      --,[End_Transaction] = @End_Transaction
      --,[End_Transaction2] = @End_Transaction2
      ,[Exit_REDS] = @Exit_REDS
      ,[TruckWash_REDS] = @TruckWash_REDS
      ,[Precision] = @Precision
      ,[TagReadFrequency] = @TagReadFrequency
	  ,MaxDelay = @MaxDelay	
	  ,Exit_Ent1 = @Exit_Ent1 
	  ,TruckWash_Ent1 = @TruckWash_Ent1
	  ,UnTarpTime = @UnTarpTime
	  ,ModifiedDate = getdate() 
	WHERE ID = @ID
END

GO

--------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 17 August 2010
-- Description:	Get All Expected Times values
-- =============================================
ALTER PROCEDURE [dbo].[ExpectedTimes_GetAllValues]
	-- Add the parameters for the stored procedure here
AS
BEGIN
-- Insert statements for procedure here
--1. Get all Expected times from ExpectedTimes table
	SELECT [ID]
      ,[REDS_Queue] 
      ,[Queue_Ent1]
      ,[Ent1_Ent2]
      ,[QueueTimePerTruck]
      ,[Unloading]
      ,[StartUp]
      ,[ShutDown]
      ,[TruckCallResponse]
      ,[TruckQueueMax]
      --,[End_Transaction]
      --,[End_Transaction2]
      ,[Exit_REDS]
      ,[TruckWash_REDS]
      ,[Precision]
      ,[TagReadFrequency],MaxDelay,Exit_Ent1,TruckWash_Ent1,ModifiedDate,UnTarpTime
  FROM [ExpectedTimes]

--2. Get all Expected times from Hoppers table
--  SELECT [ID]
--		,'E2' + [HopperName] as E2Hopper
--		,[TimeFromEnt2]
--		,[HopperName] + 'TW' as HopperTW
--		,[TimeToTW] 
--  FROM [Hoppers]


 SELECT [ID]
		,'Entry2 to PAD' + [HopperName] as E2Hopper
		,[TimeFromEnt2]
		,'PAD'+[HopperName] + ' to TruckWash' as HopperTW
		,[TimeToTW]
		,isnull(MarkerID,0) as MarkerID
  FROM [Hoppers]



END

GO
---------------------------------------------------------------------------------------------------------------