 
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 06 August 2010
-- Description:	Get All Hoppers
-- Modified By :- Vijay ( To select Group ID ) -- 06-12-2011
-- =============================================
ALTER PROCEDURE [dbo].[Hoppers_GetAllHoppers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	0 AS ID,
			'--Select--' AS [HopperName], 
			0 AS [MarkerID], 
			0 AS [TimeFromEnt2], 
			0 AS [TimeToTW],
			0 as GroupID,
			0 as GroupOrder
	UNION
	SELECT	[ID]
			,[HopperName]
			,[MarkerID]
			,[TimeFromEnt2]
			,[TimeToTW]
			,ISNULL(GroupID,0)as GroupID
			,GroupOrder 
	FROM	[Hoppers]
END
