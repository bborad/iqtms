

/****** Object:  Table [dbo].[Groups]    Script Date: 11/15/2011 18:52:34 ******/
/*SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](100) NOT NULL 
CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
-------------------------------------------------------------------------------------------------------

Alter Table Hoppers
Add  GroupID int Null
GO

Alter Table Hoppers
Add  GroupOrder int Null
GO
*/
-------------------------------------------------------------------------------------------------------
  
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 15-11-2011
-- Description:	Insert Groups
-- =============================================

CREATE PROCEDURE [dbo].[Groups_InsertGroup]
	 @GroupName as Varchar(100)	 
AS
BEGIN
	
   IF NOT EXISTS (SELECT ID FROM Groups Where GroupName = @GroupName)
	BEGIN
		 INSERT INTO [Groups]
           (
			 GroupName
			)
     VALUES
           (
			@GroupName
		   )
	Select @@IDENTITY
	END
   ELSE
	SELECT -1; --Already Exist

END
GO

------------------------------------------------------------------------------

GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 15-11-2011
-- Description:	Get All Groups
-- =============================================

CREATE PROCEDURE [dbo].[Groups_GetAllGroups]	 
	 
AS
BEGIN
	
   SELECT ID,GroupName FROM Groups

	SELECT ID,GroupID,GroupOrder FROM Hoppers Where GroupID IS NOT NULL

END
GO

----------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 15-11-2011
-- Description:	Get Hoppers By Group ID
-- =============================================

CREATE PROCEDURE [dbo].[Hoppers_GetHoppersByGroupID]	 
	 @GroupID INT
AS
BEGIN
	
   SELECT * FROM Hoppers 
	WHERE GroupID = @GroupID

END
GO

------------------------------------------------------------------------------------------------

 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 15-11-2011
-- Description:	Update Hopper's Group and Group Order
-- =============================================

CREATE PROCEDURE [dbo].[Hoppers_UpdateGroup]	 
	 @ID INT, 
	 @GroupID INT,	
	 @GroupOrder INT
AS
BEGIN
	
 UPDATE Hoppers SET GroupID = @GroupID, GroupOrder = @GroupOrder
	WHERE ID = @ID

END


GO

--------------------------------------------16112011_ChangesForTMSModbusScreen--------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 12-05-2011
-- Description:	Get Hoppers
-- Modified on: 16-11-2011
-- =============================================
ALTER PROCEDURE [dbo].[Hoppers_GetHoppers] 
	
AS
BEGIN
	 
	SET NOCOUNT ON;  
	SELECT	[ID]
			,'STOCKPAD ' + [HopperName] as PadName
			,0 AS PadStatus
			,0 AS PadStart
			,0 AS PadMatch
			,1 as Visibility
	FROM	[Hoppers]
	
 DECLARE @ServerName1 Varchar(50),
		 @ServerName2 Varchar(50)

 SELECT top 1 @ServerName1 = ServerName from ServerInfo WHERE IsActive = 1
 Order by LastResponseTime desc

 SELECT top 1 @ServerName2 = ServerName from ServerInfo WHERE IsActive = 0 AND ServerName <> @ServerName1
 Order by LastResponseTime desc

 Select isnull(@ServerName1,'NA') as ServerName1,isnull(@ServerName2,'NA') as ServerName2
	 
END

GO
-------------------------------------------------------------------------------------------------
