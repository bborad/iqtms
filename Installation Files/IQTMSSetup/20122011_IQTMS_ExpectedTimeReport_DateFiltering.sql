--USE [RampTMS_TestingFirst]
--GO
/****** Object:  StoredProcedure [dbo].[ExpectedTimes_GetReport]    Script Date: 12/20/2011 20:35:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 3rd Sep 2010
-- Description:	To Get the Expected Time Report
-- Modified By: Vijay Mangtani on 20-12-2011
-- =============================================
ALTER PROCEDURE [dbo].[ExpectedTimes_GetReport]  --3,1,1

	@HopperID int,
	@TableID int,
	@ID int
	,@From_Date DateTime = NULL
	,@To_Date DateTime = NULL
	,@Option INT = NULL

AS
BEGIN
	SET NOCOUNT ON;



Declare @FromDate datetime,@ToDate datetime

Declare @ArchiveHopperTimeID int
Set @ArchiveHopperTimeID = 0


Declare @Hopper nvarchar(100)
Select @Hopper= HopperName From Hoppers where ID= @HopperID


-- "Configured" column Variables
Declare @REDS_Queue int, @QueueTimePerTruck int, @TruckCallResponse int, @Queue_Ent1 int, @Ent1_Ent2 int, @E2Hx int, @HxTwEx int


IF(@TableID=1)								--- ExpectedTimes
	Begin
			SELECT @REDS_Queue=[REDS_Queue]
				  ,@QueueTimePerTruck=[QueueTimePerTruck]
				  ,@TruckCallResponse=[TruckCallResponse]
				  ,@Queue_Ent1=[Queue_Ent1]
				  ,@Ent1_Ent2=[Ent1_Ent2]
				  ,@FromDate=ModifiedDate
				  ,@ToDate=getdate()
		   
		  FROM [ExpectedTimes] WHERE ID=@ID
	End
ELSE										-- ArchiveExpectedTimes
	Begin
			SELECT @REDS_Queue=[REDS_Queue]
				  ,@QueueTimePerTruck=[QueueTimePerTruck]
				  ,@TruckCallResponse=[TruckCallResponse]
				  ,@Queue_Ent1=[Queue_Ent1]
				  ,@Ent1_Ent2=[Ent1_Ent2]
				  ,@ArchiveHopperTimeID=ArchiveHopperTimeID
				  ,@FromDate=ModifiedDate
				  ,@ToDate=ArchiveDate
		   
		  FROM ArchiveExpectedTimes WHERE ID=@ID
	End


-- ********************************************************************************

IF @From_Date IS NOT NULL
BEGIN
	SET @FromDate = @From_Date
END

IF @To_Date IS NOT NULL
BEGIN
	SET @ToDate = @To_Date
END

IF (@ArchiveHopperTimeID=0)				-- ExpectedTimes
	Begin
		SELECT	@E2Hx=TimeFromEnt2
			   ,@HxTwEx=TimeToTW
   
			FROM Hoppers  WHERE ID=@HopperID

	End
ELSE										-- ArchiveExpectedTimes
	Begin
	
		Declare @HopperColumnName nvarchar(20)
		set @HopperColumnName='Hopper'+CAST(@HopperID AS varchar(5)) 

		DECLARE @ParmDefinition nvarchar(500);
		DECLARE @ReturnValue varchar(30);
		Declare @SQL nvarchar(2000)

		set @SQL= N'SELECT @Value = ' + @HopperColumnName + ' FROM ArchiveHopperTimes WHERE ID= ' + CAST(@ArchiveHopperTimeID AS varchar(5)) 
		SET @ParmDefinition = N'@Value varchar(30) OUTPUT';
		
		EXECUTE sp_executesql @SQL, @ParmDefinition,@Value=@ReturnValue OUTPUT;
		--SELECT @ReturnValue;

		--*******************************
	
		Declare @idx int
		IF @ReturnValue IS NOT NULL 
		BEGIN  	
			set @idx = 0
			set @idx = charindex('|',@ReturnValue)  --'1|3'   
			if @idx!=0 
			  BEGIN  
				set @E2Hx = left(@ReturnValue,@idx - 1) 		
				set @HxTwEx = right(@ReturnValue,len(@ReturnValue) - @idx)  
			  END
		END

	End


-- ********************************************************************************

--Print @FromDate
--Print @FromDate


-- TruckCallResponse (3)
Declare @QueueEntry1 nvarchar(100)
SELECT @QueueEntry1=Queue_Ent1 FROM ExpectedTimes 
 
-- Entry2_HopperTime (6)
Declare @HopperName nvarchar(100)
SELECT @HopperName=HopperName FROM Hoppers WHERE ID=@HopperID 

-- HopperTime_TW/Exit (7)
Declare @HopperName1 nvarchar(100)
SELECT @HopperName1=HopperName FROM Hoppers WHERE ID=@HopperID 


-- REDS_Queue (1)
SELECT 'REDS_Queue' as R, 
ISNULL(CONVERT(char(20), @REDS_Queue), 'N.A.') AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,REDS,[Queue]))), 'N.A.') AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS StandardDeviation,
1 as Row 

FROM ArchiveReads	

WHERE REDS IS NOT NULL AND [Queue] IS NOT NULL AND [Queue]>REDS 

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper


UNION

-- QueueTimePerTruck (2)
SELECT 'QueueTimerPerTruck' as R,
@QueueTimePerTruck AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS StandardDeviation 
,2 as Row 

FROM ArchiveReads	

WHERE [Queue] IS NOT NULL AND TruckCallUp IS NOT NULL AND TruckCallUp>[Queue]
		
		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- TruckCallResponse (3)

--Declare @QueueEntry1 nvarchar(100)
--SELECT @QueueEntry1=Queue_Ent1 FROM ExpectedTimes 


-- removed @QueueEntry1 from all parameters

SELECT 'TruckCallResponse' as R,
ISNULL(CONVERT(char(20), @TruckCallResponse), 'N.A.')  AS Configured, 
--ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,TruckCallUp,Entry1)-@QueueEntry1)), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,TruckCallUp,Entry1))), 'N.A.')  AS StandardDeviation 
,3 as Row 

FROM ArchiveReads	

WHERE TruckCallUp IS NOT NULL AND Entry1 IS NOT NULL AND Entry1>TruckCallUp

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- Queue_Entry1 (4)
SELECT 'Queue_Ent1' as R,
ISNULL(CONVERT(char(20), @Queue_Ent1), 'N.A.')  AS Configured,
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS StandardDeviation 
,4 as Row 

FROM ArchiveReads	

WHERE [Queue] IS NOT NULL AND Entry1 IS NOT NULL AND Entry1>[Queue]

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- Entry1_Entry2 (5)
SELECT 'Ent1_Ent2' as R, 
ISNULL(CONVERT(char(20), @Ent1_Ent2), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS StandardDeviation 
,5 as Row 

FROM ArchiveReads	

WHERE Entry1 IS NOT NULL AND Entry2 IS NOT NULL AND Entry2>Entry1

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper


UNION

-- Entry2_HopperTime (6)

--Declare @HopperName nvarchar(100)
--SELECT @HopperName=HopperName FROM Hoppers WHERE ID=@HopperID 

SELECT 'Ent2 - PAD' as R, 
ISNULL(CONVERT(char(20), @E2Hx), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS StandardDeviation 
,6 as Row 

FROM ArchiveReads	

WHERE Entry2 IS NOT NULL AND HopperTime IS NOT NULL AND HopperTime>Entry2 AND  HopperName=@HopperName

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- HopperTime_TW/Exit (7)

--Declare @HopperName1 nvarchar(100)
--SELECT @HopperName1=HopperName FROM Hoppers WHERE ID=@HopperID 

SELECT 'PAD - TW/EX + Unloading' as R, 
ISNULL(CONVERT(char(20), @HxTwEx), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS StandardDeviation 
,7 as Row 

FROM ArchiveReads AR INNER JOIN 	
	(SELECT ID,	CASE WHEN (TruckWash IS NOT NULL AND  ExitTime IS NOT NULL) 
						THEN 
						CASE WHEN TruckWash > ExitTime THEN ExitTime 
							 ELSE TruckWash END 
					WHEN (TruckWash IS NOT NULL) THEN TruckWash	 
					WHEN (ExitTime IS NOT NULL) THEN ExitTime END TWExit

		FROM ArchiveReads WHERE TruckWash IS NOT NULL OR ExitTime IS NOT NULL) TEMP 

ON AR.ID=TEMP.ID

WHERE HopperTime IS NOT NULL AND TWExit IS NOT NULL AND TWExit>HopperTime AND  HopperName=@HopperName1

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper

ORDER BY Row
---********************************************************************


END

GO

-------------------------------------------------------------------------------------------------------------

