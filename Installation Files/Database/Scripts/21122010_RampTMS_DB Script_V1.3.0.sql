
IF NOT EXISTS (SELECT * FROM syscolumns
  WHERE id=object_id('Markers') and NAME='SerialNumber')
    ALTER TABLE Markers ADD SerialNumber VARCHAR(50) NULL

GO
/****** Object:  StoredProcedure [dbo].[Markers_InsertMarker]    Script Date: 12/21/2010 18:21:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi	
-- Create date: 19 August 2010
-- Description:	Insert Markers
-- =============================================

ALTER PROCEDURE [dbo].[Markers_InsertMarker]
	-- Add the parameters for the stored procedure here
			@MarkerLoopID nvarchar(100)
           --,@IPAddress nvarchar(20)
           ,@MarkerPosition nvarchar(50)
           ,@Location int
		,@SerialNumber Varchar(50)
AS
BEGIN
	
    -- Insert statements for procedure here
	INSERT INTO [Markers]
           (
			[MarkerLoopID]
           --,[IPAddress]
           ,[MarkerPosition]
           ,[Location]
			,[SerialNumber]
			)
     VALUES
           (
			@MarkerLoopID 
           --,@IPAddress 
           ,@MarkerPosition 
           ,@Location 
			,@SerialNumber
		   )
	Select @@IDENTITY
END

---------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Markers_GetMarkersInfo]    Script Date: 12/21/2010 18:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 DEC 2010
-- Description:	Get all Markers Info
-- =============================================
CREATE PROCEDURE [dbo].[Markers_GetMarkersInfo]
	-- Add the parameters for the stored procedure here
	@MarkerHealthCheckFlag Varchar(10) = null Output 
AS
BEGIN
-- Insert statements for procedure here
  SELECT [ID]
      ,[MarkerLoopID]
      ,SerialNumber
  FROM [Markers]
  WHERE SerialNumber is NOT NULL AND SerialNumber <> ''

  SELECT @MarkerHealthCheckFlag = [Value]
  FROM	Configurations
  WHERE ParameterType = 'MarkerHealthCheck'

END 
 
---------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'MarkerHealthCheck')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	SET IDENTITY_INSERT Configurations ON;
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'MarkerHealthCheck','Boolean [ 1 / 0 ]','1')
	SET IDENTITY_INSERT Configurations OFF;
END

---------------------------------------------------------------------------------------------------------------------



