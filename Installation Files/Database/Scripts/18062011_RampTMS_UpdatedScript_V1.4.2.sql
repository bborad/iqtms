GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Tags' AND COLUMN_NAME = 'BatteryStatus')
	ALTER TABLE [DBO].Tags ADD BatteryStatus varchar(50) NULL 

GO

-------------------------------------01022011_MarkersStatus_VMSRandomPortChanges_V1.3.2-----------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'VMSRandomPort')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	IF COLUMNPROPERTY(object_id('Configurations'), 'ID', 'IsIdentity') = 0	 
		INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'VMSRandomPort','Boolean [ 1 / 0 ]','0')	
	ELSE
		 INSERT INTO Configurations(ParameterType,Field,[Value]) Values('VMSRandomPort','Boolean [ 1 / 0 ]','0')
END

GO
---------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'VMSMinPortNo')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	IF COLUMNPROPERTY(object_id('Configurations'), 'ID', 'IsIdentity') = 0	 	 
		INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'VMSMinPortNo','Min Value','1000')	
	ELSE
		INSERT INTO Configurations(ParameterType,Field,[Value]) Values('VMSMinPortNo','Min Value','1000')
END

GO

--------------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'VMSMaxPortNo')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;	
	IF COLUMNPROPERTY(object_id('Configurations'), 'ID', 'IsIdentity') = 0	 	 
		INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'VMSMaxPortNo','Max Value','25000')
	ELSE
		INSERT INTO Configurations(ParameterType,Field,[Value]) Values('VMSMaxPortNo','Max Value','25000')	
END

GO

--------------------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- Modified date : 1 feb 2011
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100)
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int;
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
		Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
		Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
		Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 

END

---------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateEntryORQueue]    Script Date: 02/01/2011 10:26:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 6th Aug 2010
-- Description:	To Update Entry OR Queue value in ActiveReads TABLE
-- Modified Date : 1 Feb 2011
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_UpdateEntryORQueue]
	@TagID Bigint,
	@TagRFID Varchar(50),
	@EntryORQueue int
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;


	 IF @TagID = 0 
		 SELECT @TagID = ID FROM Tags WHERE TagRFID = @TagRFID

		UPDATE ActiveReads SET EntryORQueue = @EntryORQueue Where TagID = @TagID

		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
		Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
		Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
		Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
 
        
END
Go
----------------------------------------------------------------------------------------------------------------------


IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'Markers_GetMarkers' AND type = N'P')
   DROP PROCEDURE Markers_GetMarkers
GO

GO
/****** Object:  StoredProcedure [dbo].[Markers_GetAllMarkers]    Script Date: 01/19/2011 14:32:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay MAngtani
-- Create date: 19 Jan 2011
-- Description:	Get all Markers
-- =============================================
CREATE PROCEDURE [dbo].Markers_GetMarkers
	-- Add the parameters for the stored procedure here
	
AS
BEGIN 
	SELECT  
      [MarkerLoopID] as LoopID  
	  ,isnull(Locations.LocationName,'') as Location
	  ,isnull(SerialNumber,'000') as SerialNo
	  ,'Not Connected' as [Status]
  FROM [Markers]
      LEFT OUTER JOIN Locations ON Locations.ID = [Location]
END

------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------10022011_ChangesForIssueTag------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 6th Oct 2010
-- Description:	Get all the tags to dispatch from location control room
-- =============================================
ALTER PROCEDURE [dbo].[TagsToIssue_GetTagsToDispatch]

AS
BEGIN
	SET NOCOUNT ON;

	--SELECT TagRFID

	Declare @LocationID INT

	SET @LocationID = (Select ID FROM Locations Where LocationName = 'ControlRoom')

	 SELECT T.[ID]
      ,[TagRFID]
      ,[ProponentID]
	  ,[ProponentName]
	  ,[HopperID]
	  ,[HopperName]
      ,[Assigned]
      ,[TruckID]
		FROM [Tags] T
		INNER JOIN Proponents P ON P.[ID] = [ProponentID]
		INNER JOIN Hoppers H ON H.[ID] = [HopperID]
		WHERE  Assigned = 1 and (StatusID!=2 and StatusID!=3)
		AND [TagRFID] IN ( SELECT TagRFID FROM TagsToIssue WHERE IsDispatched = 0 AND (LastLocation = @LocationID))

END

---------------------------------------------------------------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 04 August 2010
-- Description:	Dispatch Tags By Tag ID (Tags id are in CSV string)
-- Modified On : - 10 Feb 2011
-- ============================================= 


ALTER PROCEDURE [dbo].[IssuedTags_DispatchTagsByTagsID] --'''-1'',''400002047'',''400002048'',''400002049''','554545','-1,0,1,2' 
--'302203124,302203125,302203120,302203121,302203124,302203125,302203126,302203127,302203128,302203129,302203130,302203131,302203132,302203133,302203134,302203135,302203136,302203137,302203139,302203140,302203123,302203122','00001'
	-- Add the parameters for the stored procedure here
	@IssuedTagsID varchar(max),@TruckRegNo varchar(50),@TagBattery varchar(MAX)
AS
BEGIN
 declare @SQL varchar(max)     
 SET @SQL =  'INSERT INTO IssuedTags(TagID,ProponentID,HopperID,TruckID,Issued,TagBattery,TruckRegNo) 
			 (SELECT  [ID],
			   [ProponentID],
			   [HopperID],
			   [TruckID],
				getdate(),0,'''
				+ @TruckRegNo + 
			'''FROM [Tags] 
			WHERE [TagRFID] IN (' +  @IssuedTagsID + ' ) and Assigned = 1 and (StatusID!=2 and StatusID!=3))'
EXEC(@SQL)	
Select @@RowCount as countaddedrows

 ---------------------------------------------------------------------------------------------------------------------

DECLARE @strTagIDs Varchar(MaX)
SET @strTagIDs = @IssuedTagsID;


	declare @idx int,
	@idx1 int, 
	@ID int    
	declare @tagID varchar(12)     
    declare @countBatteryStatus int
	declare @TBattery varchar(3)
	-- Check that the Tagid CSV string is empty or null then do nothing
	Set @countBatteryStatus = 0
	SET @ID = 0;
	select @idx = 1     
		if len(@IssuedTagsID)<1 or @IssuedTagsID is null     
		Set @countBatteryStatus = 0
	-- If Tagid CSV string is not null or not empty then the split it into single string
	while @idx!= 0     
	begin     
		set @idx = charindex(',',@IssuedTagsID) 
		--set @idx1 = charindex(',',@TagBattery)   
		if @idx!=0
			begin     
				set @tagID = left(@IssuedTagsID,@idx - 1)
				--set @TBattery = left(@TagBattery,@idx1 - 1) 
			end
		else 
			begin    
				set @tagID = @IssuedTagsID 
				--set @TBattery = @TagBattery
			end  
			
			SET @tagID = right(@tagID,len(@tagID) - 1);
			Set @tagID = left(@tagID,len(@tagID) - 1);

		if((len(@tagID)>8))
		begin
			Select @ID = ID,@TBattery = (Case When BatteryStatus Is Null then 0
											  WHEN BatteryStatus = 'Good' then 0
											  WHEN BatteryStatus = 'Poor' then 2
											  ELSE 1 END)
			from Tags where TagRFID = @tagID AND Assigned = 1 and (StatusID!=2 and StatusID!=3)

			Update IssuedTags set TagBattery = @TBattery where TagID = @ID
				IF @ID > 0
					EXEC TagsToIssue_Update @ID,@tagID,0,1
			print @tagID;
			print @ID;
			print @TBattery;
		end
		set @IssuedTagsID = right(@IssuedTagsID,len(@IssuedTagsID) - @idx)
		set @TagBattery = right(@TagBattery,len(@TagBattery) - @idx1)    
		if len(@IssuedTagsID) = 0 break     
	end 

---------------------------------------------------------------------------------------------------------------------

declare @SQLUpdateStatus varchar(max)     
SET @SQLUpdateStatus =  'UPDATE [Tags] SET StatusID = 2 
			  WHERE [TagRFID] IN (' +  @strTagIDs + ' ) and Assigned = 1 and (StatusID!=2 and StatusID!=3)'

EXEC(@SQLUpdateStatus)

---------------------------------------------------------------------------------------------------------------------

END

--------------------------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_OpenCloseTransaction]    Script Date: 02/10/2011 20:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;

	Declare @Result INT
	
	SET @MarkerID = 0;

--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF @Tag_ID IS NULL
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID		
		RETURN;
	END

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID	

							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID										
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- Set Idel
			END
	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
						
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- Set Idel
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 2
						BEGIN
							UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	 

							UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
							Where TagID = @Tag_ID							 
						END	
					ELSE
						BEGIN
							SET @TransMessage = 'Transaction Completed.';
							SET @TransStatus = 1;
							UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	
						END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID From Hoppers
			WHERE ID = @Hopper_ID			

END

----------------------------------------------------------15022011_SetTagStatusToIdelAtTW--------------------------------------------------------------------------------
GO

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'CurrentPageAutoReload')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;	 
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'CurrentPageAutoReload','Interval','3600')	
END

------------------------------------------------------------------------------------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[TagsToIssue_Update]    Script Date: 02/15/2011 12:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 4th Oct 2010
-- Description:	Insert/Update TagsToIssue
-- Modified on : 15 Feb 2011
-- =============================================
ALTER PROCEDURE [dbo].[TagsToIssue_Update] --0,'400002048',6
  @TagID as BIGINT = 0,
  @TagRFID as Varchar(50),
  @LastLocation int,
  @IsDispatched bit = 0,
  @IsDeleted bit = 0

AS
BEGIN
	SET NOCOUNT ON;

IF @TagID = 0
BEGIN
	SET @TagID = (SELECT Top 1 ID FROM Tags WHERE TagRFID = @TagRFID AND AssignDate IS NOT NULL)
END

IF @TagID is NULL
BEGIN
	DELETE FROM TagsToIssue Where TagRFID = @TagRFID;
	RETURN;
END

DECLARE  @LastModifiedDate DATETIME,
		 @TWtoREDSTime INT	

SELECT @TWtoREDSTime = TruckWash_REDS FROM ExpectedTimes

IF EXISTS (SELECT ID FROM TagsToIssue WHERE TagID = @TagID AND TagRFID = @TagRFID)
	BEGIN

		SELECT @LastModifiedDate = LastModifiedDate FROM TagsToIssue WHERE TagID = @TagID AND TagRFID = @TagRFID

		IF @LastLocation = 0
			SELECT @LastLocation = LastLocation FROM TagsToIssue WHERE TagID = @TagID AND TagRFID = @TagRFID		

		UPDATE TagsToIssue SET	LastModifiedDate = GetDate(),
								LastLocation = @LastLocation,
								IsDispatched = @IsDispatched,
								IsDeleted = @IsDeleted 
						  WHERE TagID = @TagID AND TagRFID = @TagRFID
	END
ELSE
	BEGIN
		DELETE FROM TagsToIssue Where TagRFID = @TagRFID;	

		IF @LastLocation = 0
			SET @LastLocation = 6
			
		INSERT INTO TagsToIssue(TagID,TagRFID,LastLocation,IsDeleted,IsDispatched,LastModifiedDate)
				VALUES(@TagID,@TagRFID,@LastLocation,@IsDeleted,@IsDispatched,GetDate())
		
		SET @LastModifiedDate = GetDate()
	END
 

IF @LastLocation = 6 OR @LastLocation = 7
	BEGIN
		IF DateDiff(ss, @LastModifiedDate,GetDate()) > @TWtoREDSTime
			BEGIN
				Update Tags SET [StatusID] = 1 WHERE  ID=@TagID -- Set Idel
			END
	END


END


---------------------------------------------------------------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 04 August 2010
-- Description:	Dispatch Tags By Tag ID (Tags id are in CSV string)
-- Modified On : - 10 Feb 2011
-- ============================================= 


ALTER PROCEDURE [dbo].[IssuedTags_DispatchTagsByTagsID] --'''-1'',''400002047'',''400002048'',''400002049''','554545','-1,0,1,2' 
--'302203124,302203125,302203120,302203121,302203124,302203125,302203126,302203127,302203128,302203129,302203130,302203131,302203132,302203133,302203134,302203135,302203136,302203137,302203139,302203140,302203123,302203122','00001'
	-- Add the parameters for the stored procedure here
	@IssuedTagsID varchar(max),@TruckRegNo varchar(50),@TagBattery varchar(MAX)
AS
BEGIN
 declare @SQL varchar(max)     
 SET @SQL =  'INSERT INTO IssuedTags(TagID,ProponentID,HopperID,TruckID,Issued,TagBattery,TruckRegNo) 
			 (SELECT  [ID],
			   [ProponentID],
			   [HopperID],
			   [TruckID],
				getdate(),0,'''
				+ @TruckRegNo + 
			'''FROM [Tags] 
			WHERE [TagRFID] IN (' +  @IssuedTagsID + ' ) and Assigned = 1 and (StatusID!=2 and StatusID!=3))'
EXEC(@SQL)	
Select @@RowCount as countaddedrows

 ---------------------------------------------------------------------------------------------------------------------

DECLARE @strTagIDs Varchar(MaX)
SET @strTagIDs = @IssuedTagsID;


	declare @idx int,
	@idx1 int, 
	@ID int    
	declare @tagID varchar(12)     
    declare @countBatteryStatus int
	declare @TBattery varchar(3)
	-- Check that the Tagid CSV string is empty or null then do nothing
	Set @countBatteryStatus = 0
	SET @ID = 0;
	select @idx = 1     
		if len(@IssuedTagsID)<1 or @IssuedTagsID is null     
		Set @countBatteryStatus = 0
	-- If Tagid CSV string is not null or not empty then the split it into single string
	while @idx!= 0     
	begin     
		set @idx = charindex(',',@IssuedTagsID) 
		--set @idx1 = charindex(',',@TagBattery)   
		if @idx!=0
			begin     
				set @tagID = left(@IssuedTagsID,@idx - 1)
				--set @TBattery = left(@TagBattery,@idx1 - 1) 
			end
		else 
			begin    
				set @tagID = @IssuedTagsID 
				--set @TBattery = @TagBattery
			end  
			
			SET @tagID = right(@tagID,len(@tagID) - 1);
			Set @tagID = left(@tagID,len(@tagID) - 1);

		if((len(@tagID)>8))
		begin
			Select @ID = ID,@TBattery = (Case When BatteryStatus Is Null then 0
											  WHEN BatteryStatus = 'Good' then 0
											  WHEN BatteryStatus = 'Poor' then 2
											  ELSE 1 END)
			from Tags where TagRFID = @tagID AND Assigned = 1 and (StatusID!=2 and StatusID!=3)

			Update IssuedTags set TagBattery = @TBattery where TagID = @ID
				IF @ID > 0
					EXEC TagsToIssue_Update @ID,@tagID,0,1
			print @tagID;
			print @ID;
			print @TBattery;
		end
		set @IssuedTagsID = right(@IssuedTagsID,len(@IssuedTagsID) - @idx)
		set @TagBattery = right(@TagBattery,len(@TagBattery) - @idx1)    
		if len(@IssuedTagsID) = 0 break     
	end 


declare @SQLUpdateStatus varchar(max)     
SET @SQLUpdateStatus =  'UPDATE [Tags] SET StatusID = 2 
			  WHERE [TagRFID] IN (' +  @strTagIDs + ' ) and Assigned = 1 and (StatusID!=2 and StatusID!=3)'

EXEC(@SQLUpdateStatus)


END

--------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_OpenCloseTransaction]    Script Date: 02/10/2011 20:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;

	Declare @Result INT
	
	SET @MarkerID = 0;

--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF @Tag_ID IS NULL
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID		
		RETURN;
	END

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID	

							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID										
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END
	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								ExitTime,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
						
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 2
						BEGIN
							UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	 

							UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
							Where TagID = @Tag_ID							 
						END	
					ELSE
						BEGIN
							SET @TransMessage = 'Transaction Completed.';
							SET @TransStatus = 1;
							UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	
						END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						ExitTime,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID From Hoppers
			WHERE ID = @Hopper_ID			

END

----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------18022011_ReferenceNoFiltering_V1.3.0.2----------------------------------------------------------------------------------

GO
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'IssuedTags_GetAllReferenceNo' AND type = N'P')
   DROP PROCEDURE IssuedTags_GetAllReferenceNo
GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay MAngtani
-- Create date: 18 Feb 2011
-- Description:	Get all Markers
-- =============================================
CREATE PROCEDURE [dbo].[IssuedTags_GetAllReferenceNo]
	-- Add the parameters for the stored procedure here	 
AS
BEGIN

	SELECT DISTINCT TruckRegNo as ReferenceNo FROM IssuedTags
	
END
----------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Tags_IssueTagReport]    Script Date: 02/18/2011 12:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Tags_IssueTagReport] -- '''-1''','-1','-1','01/28/2010','09/29/2010','today'
	-- Add the parameters for the stored procedure here	
	@TagRFIDs VARCHAR(6000),
	@HopperIDs VARCHAR(200),
	@ProponentIDs VARCHAR(200),
	@DateFrom VARCHAR(30),
	@DateTo VARCHAR(30),
	@ReferenceNo VARCHAR(50) = NULL
AS
BEGIN

Declare @Condition nvarchar(max)
Set @Condition = ''

DECLARE @sql NVARCHAR(MAX)
Set @sql = ''
	   
		if(@TagRFIDs != '''-1''')
			Set @Condition  = @Condition + ' AND TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+')) '+'' 
		if(@HopperIDs != '-1')
			Set @Condition  = @Condition + ' AND HopperID IN ('+@HopperIDs+') '+'' 
		if(@ProponentIDs !='-1')
			Set @Condition = @Condition + ' AND ProponentID IN('+@ProponentIDs+') '+''
	
		if(@ReferenceNo IS NOT NULL)
			Set @Condition = @Condition + ' AND TruckRegNo = '''+@ReferenceNo+''' '

		if(@DateFrom != '' )
					SET @Condition = @Condition + ' and  convert(varchar(10),Issued,101) >= convert(varchar(10),'''+ @DateFrom +''',101) '+'' 
				     --SET @Condition = ' AND AssignDate >='''+ @DateFrom +''' '+'' 
		if(@DateTo != '' )
					SET @Condition =	@Condition+ ' and  convert(varchar(10),Issued,101) <= convert(varchar(10),'''+@DateTo + ''',101) '+'' 
					--SET @Condition = @Condition + ' AND AssignDate <= '''+ @DateTo +''' '+''

SET @sql='
		SELECT	Proponents.ProponentName, Hoppers.HopperName, IssuedTags.ID, IssuedTags.TagID, 
				IssuedTags.ProponentID, IssuedTags.HopperID, IssuedTags.TruckID, 
				IssuedTags.TagBattery, IssuedTags.Issued, IssuedTags.TruckRegNo
		FROM	Hoppers INNER JOIN
                IssuedTags ON Hoppers.ID = IssuedTags.HopperID INNER JOIN
                Proponents ON IssuedTags.ProponentID = Proponents.ID
		WHERE	1=1' + @Condition	

--			--TagID IN ('+@TagRFIDs+')
--			TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+'))
--			AND HopperID IN('+@HopperIDs+')
--			AND ProponentID IN('+@ProponentIDs+')
--			AND	Issued >='''+ @DateTo +''' AND Issued <= '''+ @DateFrom +''''
		
	--PRINT @sql
	EXECUTE sp_executesql  @sql
		
END

GO

---------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'Markers_GetMarkers' AND type = N'P')
   DROP PROCEDURE Markers_GetMarkers
GO

GO
/****** Object:  StoredProcedure [dbo].[Markers_GetAllMarkers]    Script Date: 01/19/2011 14:32:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay MAngtani
-- Create date: 19 Jan 2011
-- Description:	Get all Markers
-- =============================================
CREATE PROCEDURE [dbo].Markers_GetMarkers
	-- Add the parameters for the stored procedure here	
AS
BEGIN 
	SELECT  
      [MarkerLoopID] as LoopID  
	  ,isnull(Locations.LocationName,'') as Location
	  ,isnull(SerialNumber,'000') as SerialNo
	  ,'Not Connected' as [Status]
  FROM [Markers]
      LEFT OUTER JOIN Locations ON Locations.ID = [Location]
END


-------------------------------------------------03032011_TruckCallupRetries--------------------------------------------------------

GO
---------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'TruckCallUp')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	--SET IDENTITY_INSERT Configurations ON;
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'TruckCallUp','TotalRetries','5')
	--SET IDENTITY_INSERT Configurations OFF;
END

---------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateTruckCallUp]    Script Date: 03/03/2011 15:05:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- Modified date : 1 feb 2011
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100)
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
		
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int
			--@TruckCallUpRetries INT
			
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
		Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
		Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
		Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
		--Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp' 
		Select [Value] From Configurations Where ParameterType = 'TruckCallUp'
	 

END

---------------------------------------------------------------------------------------------------------

------------------------------------------------07032011_BatteryStatus_Tags---------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Tags_IssueTagReport]    Script Date: 03/07/2011 11:56:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Tags_IssueTagReport] -- '''-1''','-1','-1','01/28/2010','09/29/2010','today'
	-- Add the parameters for the stored procedure here	
	@TagRFIDs VARCHAR(6000),
	@HopperIDs VARCHAR(200),
	@ProponentIDs VARCHAR(200),
	@DateFrom VARCHAR(30),
	@DateTo VARCHAR(30),
	@ReferenceNo VARCHAR(50) = NULL
AS
BEGIN

Declare @Condition nvarchar(max)
Set @Condition = ''

DECLARE @sql NVARCHAR(MAX)
Set @sql = ''
	   
		if(@TagRFIDs != '''-1''')
			Set @Condition  = @Condition + ' AND TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+')) '+'' 
		if(@HopperIDs != '-1')
			Set @Condition  = @Condition + ' AND HopperID IN ('+@HopperIDs+') '+'' 
		if(@ProponentIDs !='-1')
			Set @Condition = @Condition + ' AND ProponentID IN('+@ProponentIDs+') '+''
	
		if(@ReferenceNo IS NOT NULL)
			Set @Condition = @Condition + ' AND TruckRegNo = '''+@ReferenceNo+''' '

		if(@DateFrom != '' )
					SET @Condition = @Condition + ' and  convert(varchar(10),Issued,101) >= convert(varchar(10),'''+ @DateFrom +''',101) '+'' 
				     --SET @Condition = ' AND AssignDate >='''+ @DateFrom +''' '+'' 
		if(@DateTo != '' )
					SET @Condition =	@Condition+ ' and  convert(varchar(10),Issued,101) <= convert(varchar(10),'''+@DateTo + ''',101) '+'' 
					--SET @Condition = @Condition + ' AND AssignDate <= '''+ @DateTo +''' '+''

SET @sql='
		SELECT	Proponents.ProponentName, Hoppers.HopperName, IssuedTags.ID, IssuedTags.TagID, 
				IssuedTags.ProponentID, IssuedTags.HopperID, IssuedTags.TruckID, 
				isnull(Tags.BatteryStatus,''N/A'') as BatteryStatus,
				IssuedTags.TagBattery, IssuedTags.Issued, IssuedTags.TruckRegNo
		FROM	Hoppers INNER JOIN
                IssuedTags ON Hoppers.ID = IssuedTags.HopperID INNER JOIN
                Proponents ON IssuedTags.ProponentID = Proponents.ID
                INNER JOIN Tags ON IssuedTags.TagID = Tags.ID
		WHERE	1=1' + @Condition	

--			--TagID IN ('+@TagRFIDs+')
--			TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+'))
--			AND HopperID IN('+@HopperIDs+')
--			AND ProponentID IN('+@ProponentIDs+')
--			AND	Issued >='''+ @DateTo +''' AND Issued <= '''+ @DateFrom +''''
		
	--PRINT @sql
	EXECUTE sp_executesql  @sql
		
END

---------------------------------------------------------23032011_IMarkLogFileClearing----------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------
GO
IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'MarkerStatus LogFile Clearing')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	--SET IDENTITY_INSERT Configurations ON;
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'MarkerStatus LogFile Clearing','Interval [ in days ]','2')
	--SET IDENTITY_INSERT Configurations OFF;
END

---------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Markers_GetMarkersInfo]    Script Date: 03/23/2011 16:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 DEC 2010
-- Description:	Get all Markers Info
-- =============================================

ALTER PROCEDURE [dbo].[Markers_GetMarkersInfo]
	-- Add the parameters for the stored procedure here
	@MarkerHealthCheckFlag Varchar(10) = null Output,
	@LogFileClearingInterval Varchar(10) = null Output 
AS
BEGIN
-- Insert statements for procedure here
  SELECT [ID]
	  ,[MarkerLoopID]
	  ,SerialNumber
  FROM [Markers]
  WHERE SerialNumber is NOT NULL AND SerialNumber <> ''

  SELECT @MarkerHealthCheckFlag = [Value]
  FROM	Configurations
  WHERE ParameterType = 'MarkerHealthCheck'

  SELECT @LogFileClearingInterval = [Value]
  FROM	Configurations
  WHERE ParameterType = 'MarkerStatus LogFile Clearing'

END 

-------------------------------------------------------------28032011_FoundAtHopperInArchiveReads---------------------------------------------------------
GO

------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM SYSCOLUMNS
  WHERE ID=OBJECT_ID('ArchiveReads') AND NAME='FoundAtHopper')
    ALTER TABLE ArchiveReads ADD FoundAtHopper INT NULL
GO

IF NOT EXISTS (SELECT * FROM SYSCOLUMNS
  WHERE ID=OBJECT_ID('ArchiveReads') AND NAME='LastPosition')
    ALTER TABLE ArchiveReads ADD LastPosition INT NULL
GO
 
------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;

	Declare @Result INT
	
	SET @MarkerID = 0;

--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF @Tag_ID IS NULL
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID		
		RETURN;
	END

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID	

							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID										
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END
	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID
							,FoundAtHopper
							,LastPosition)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								ExitTime,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID
								,FoundAtHopper
								,LastPosition

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
						
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 2
						BEGIN
							UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	 

							UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
							Where TagID = @Tag_ID							 
						END	
					ELSE
						BEGIN
							SET @TransMessage = 'Transaction Completed.';
							SET @TransStatus = 1;
							UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
							WHERE TagRFID = @TagRFID	
						END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						ExitTime,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID
						,FoundAtHopper
						,LastPosition)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							,FoundAtHopper
							,LastPosition
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID From Hoppers
			WHERE ID = @Hopper_ID			

END

-------------------------------------------------------29032011_IssuedTagReportDateFiltration---------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Tags_IssueTagReport]    Script Date: 03/29/2011 11:26:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Tags_IssueTagReport] --'''-1''','2','-1','01/28/2010','09/29/2010','today'
	-- Add the parameters for the stored procedure here	
	@TagRFIDs VARCHAR(6000),
	@HopperIDs VARCHAR(200),
	@ProponentIDs VARCHAR(200),
	@DateFrom VARCHAR(30),
	@DateTo VARCHAR(30),
	@ReferenceNo VARCHAR(50) = NULL
AS
BEGIN

Declare @Condition nvarchar(max)
Set @Condition = ''

DECLARE @sql NVARCHAR(MAX)
Set @sql = ''
	   
		if(@TagRFIDs != '''-1''')
			Set @Condition  = @Condition + ' AND TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+')) '+'' 
		if(@HopperIDs != '-1')
			Set @Condition  = @Condition + ' AND IssuedTags.HopperID IN ('+@HopperIDs+') '+'' 
		if(@ProponentIDs !='-1')
			Set @Condition = @Condition + ' AND IssuedTags.ProponentID IN('+@ProponentIDs+') '+''
	
		if(@ReferenceNo IS NOT NULL)
			Set @Condition = @Condition + ' AND TruckRegNo = '''+@ReferenceNo+''' '

		if(@DateFrom != '' )
					SET @Condition = @Condition + ' and  cast(convert(varchar(10),Issued,101) as datetime) >= cast(convert(varchar(10),'''+ @DateFrom +''',101) as datetime) '+'' 
				     --SET @Condition = ' AND AssignDate >='''+ @DateFrom +''' '+'' 
		if(@DateTo != '' )
					SET @Condition =	@Condition+ ' and  cast(convert(varchar(10),Issued,101) as datetime) <= cast(convert(varchar(10),'''+@DateTo + ''',101) as datetime) '+'' 
					--SET @Condition = @Condition + ' AND AssignDate <= '''+ @DateTo +''' '+''

SET @sql='
		SELECT	Proponents.ProponentName, Hoppers.HopperName, IssuedTags.ID, IssuedTags.TagID, 
				IssuedTags.ProponentID, IssuedTags.HopperID, IssuedTags.TruckID, 
				isnull(Tags.BatteryStatus,''N/A'') as BatteryStatus,
				IssuedTags.TagBattery, IssuedTags.Issued, IssuedTags.TruckRegNo
		FROM	Hoppers INNER JOIN
                IssuedTags ON Hoppers.ID = IssuedTags.HopperID INNER JOIN
                Proponents ON IssuedTags.ProponentID = Proponents.ID
                INNER JOIN Tags ON IssuedTags.TagID = Tags.ID
		WHERE	1=1' + @Condition	

--			--TagID IN ('+@TagRFIDs+')
--			TagID IN (Select ID from Tags where TagRFID IN('+@TagRFIDs+'))
--			AND HopperID IN('+@HopperIDs+')
--			AND ProponentID IN('+@ProponentIDs+')
--			AND	Issued >='''+ @DateTo +''' AND Issued <= '''+ @DateFrom +''''
		
	PRINT @sql
	EXECUTE sp_executesql  @sql
		
END


--------------------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[Tags_AssignUnAssignTagReport]    Script Date: 03/29/2011 12:32:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--'29/08/2010','29/08/2010'
ALTER PROCEDURE [dbo].[Tags_AssignUnAssignTagReport] --'''-1''','-1','-1','07/01/2010','09/01/2010'
	-- Add the parameters for the stored procedure here	
	@TagRFIDs VARCHAR(6000),
	@HopperIDs VARCHAR(200),
	@ProponentIDs VARCHAR(200),
	@DateFrom VARCHAR(30),
	@DateTo VARCHAR(30)

AS
BEGIN

Declare @Condition nvarchar(max)
Set @Condition = ''

DECLARE @sql NVARCHAR(MAX)
Set @sql = ''
	   
		if(@TagRFIDs != '''-1''')
			Set @Condition  = @Condition + ' and TagRFID IN ('+@TagRFIDs+') '+'' 
		if(@HopperIDs != '-1')
			Set @Condition  = @Condition + ' AND HopperID IN ('+@HopperIDs+') '+'' 
		if(@ProponentIDs !='-1')
			Set @Condition = @Condition + ' AND ProponentID IN('+@ProponentIDs+') '+''
		if(@DateFrom != '' )
					SET @Condition =  @Condition + ' and cast(convert(varchar(10),AssignDate,101) as datetime) >= cast(convert(varchar(10),'''+ @DateFrom +''',101) as datetime) '+'' 
				     --SET @Condition = ' AND AssignDate >='''+ @DateFrom +''' '+'' 
		if(@DateTo != '' )
					SET @Condition =	@Condition+ ' and cast(convert(varchar(10),AssignDate,101) as datetime) <= cast(convert(varchar(10),'''+ @DateTo + ''',101) as datetime)  '+'' 
					--SET @Condition = @Condition + ' AND AssignDate <= '''+ @DateTo +''' '+''

SET @sql='
	SELECT	Tags.ID, Tags.TagRFID, Tags.ProponentID, Tags.HopperID, Tags.Assigned, Tags.TruckID, 
			Tags.StatusID, Tags.AssignDate,isnull(Tags.BatteryStatus,''N/A'') as BatteryStatus, 
			isnull(Proponents.ProponentName,''N/A'') as ProponentName, 
			isnull(Hoppers.HopperName,''N/A'') as HopperName 
			--Tags.TruckID + ''-'' + Proponents.ProponentName + ''-'' + Hoppers.HopperName AS TruckName
	FROM    Tags LEFT JOIN
			Hoppers ON Tags.HopperID = Hoppers.ID LEFT JOIN
			Proponents ON Tags.ProponentID = Proponents.ID
	WHERE	1=1' + @Condition + 'order by Assigned desc'
	
	PRINT @sql
	EXECUTE sp_executesql  @sql
		
END

----------------------------------------------------11042011_VMSSettingChanges-------------------------------------------------------------------------------
GO

--USE [RampTMS_TestingFirst]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VMSDevices_GetAllByLocation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VMSDevices_GetAllByLocation]

GO 
/****** Object:  StoredProcedure [dbo].[VMSDevices_GetAll]    Script Date: 04/11/2011 14:47:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 11 April 2011
-- Description:	To Get all the VMSDevices by Location
-- =============================================
CREATE PROCEDURE [dbo].[VMSDevices_GetAllByLocation] --'1'
@LocationID as INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	ID,
			IPAddress,
			PortNo,
			Location,
			VMSStatus,
			ISNULL(LEDWidth,128) as LEDWidth,
			ISNULL(LEDHeight,32) as LEDHeight,
			ISNULL(IsSCL2008,1) as IsSCL2008,
			[Password],
			ISNULL(DeviceNo,10) as DeviceNo,
			TextStartPosition,
			TextLength,
			ASCFont,
			TextColor,
			DefaultMessage,
			MessageTimeOut

	FROM	VMSDevices

WHERE Location = @LocationID
		
END

------------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateEntryORQueue]    Script Date: 04/11/2011 18:41:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 6th Aug 2010
-- Description:	To Update Entry OR Queue value in ActiveReads TABLE
-- Modified Date : 1 Feb 2011
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_UpdateEntryORQueue]
	@TagID Bigint,
	@TagRFID Varchar(50),
	@EntryORQueue int
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;


	 IF @TagID = 0 
		 SELECT @TagID = ID FROM Tags WHERE TagRFID = @TagRFID

		UPDATE ActiveReads SET EntryORQueue = @EntryORQueue Where TagID = @TagID

		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
		Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
		Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
		Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
 
		EXEC VMSDevices_GetAllByLocation '1' -- Get VMS Settings for REDS Area
        
END

---------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateTruckCallUp]    Script Date: 04/11/2011 14:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- Modified date : 1 feb 2011
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100)
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
	,@TruckCallUpRetries int = 0 Output		
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int,
			@TransID INT
			 
			
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	IF EXISTS (Select ID from ActiveReads where TagID= @Tag_ID) -- Check if transaction exists
		SET @TransID = 1
	ELSE
		SET @TransID =  0


SELECT @TransID

IF @TransID = 1
	BEGIN		
			UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

			Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
			Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
			Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
			Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
			--Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp' 
			Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp'

			EXEC VMSDevices_GetAllByLocation '2' -- Get VMS Settings for Queue Area
	END
	 
	

END

---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------
GO
IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'REDS VMS')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	--SET IDENTITY_INSERT Configurations ON;
	DECLARE @DefaultMessage Varchar(200)

    SELECT @DefaultMessage = DefaultMessage FROM VMSDevices Where Location = 1
	
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'REDS VMS','Default Message',@DefaultMessage)
	--SET IDENTITY_INSERT Configurations OFF;
END

---------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Configurations_Update]    Script Date: 05/03/2011 13:07:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 17th Sep 2010
-- Description:	To UPDATE values in Configurations TABLE
-- =============================================
ALTER PROCEDURE [dbo].[Configurations_Update]

			@ID int,
			@Value nvarchar(100),
			@VMSDeviceLocation int = -1

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE	Configurations

	SET		[Value]=@Value

	WHERE	ID=@ID

	IF @VMSDeviceLocation > 0
	BEGIN
		Update VMSDevices SET DefaultMessage = @Value Where Location = @VMSDeviceLocation
	END

END

----------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Locations WHERE LocationName = 'IssueTagOffice')
BEGIN 	
	INSERT INTO Locations(LocationName,[Description],IsDeleted) Values('IssueTagOffice','Issue Tag Office',0)	
END

---------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[TagsToIssue_GetTagsToDispatch]    Script Date: 05/05/2011 11:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 05/05/2011
-- Description:	Get all the tags to dispatch from location Issue tag office
-- =============================================
ALTER PROCEDURE [dbo].[TagsToIssue_GetTagsToDispatch]

AS
BEGIN
	SET NOCOUNT ON;

	--SELECT TagRFID

	Declare @LocationID INT

	SET @LocationID = (Select ID FROM Locations Where LocationName = 'IssueTagOffice')

	IF @LocationID IS NULL OR @LocationID = 0
		SET @LocationID = 12  -- Issue Tag Office

	 SELECT T.[ID]
      ,[TagRFID]
      ,[ProponentID]
	  ,[ProponentName]
	  ,[HopperID]
	  ,[HopperName]
      ,[Assigned]
      ,[TruckID]
		FROM [Tags] T
		INNER JOIN Proponents P ON P.[ID] = [ProponentID]
		INNER JOIN Hoppers H ON H.[ID] = [HopperID]
		WHERE  Assigned = 1 and (StatusID!=2 and StatusID!=3)
		AND [TagRFID] IN ( SELECT TagRFID FROM TagsToIssue WHERE IsDispatched = 0 AND LastLocation = @LocationID ) --(LastLocation = 6 OR LastLocation = 7))

END

---------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[TagsToIssue_Update]    Script Date: 05/05/2011 11:40:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 4th Oct 2010
-- Description:	Insert/Update TagsToIssue
-- Modified on : 05-05-2011
-- =============================================
ALTER PROCEDURE [dbo].[TagsToIssue_Update] --0,'400002048',6
  @TagID as BIGINT = 0,
  @TagRFID as Varchar(50),
  @LastLocation int,
  @IsDispatched bit = 0,
  @IsDeleted bit = 0

AS
BEGIN
	SET NOCOUNT ON;

IF @TagID = 0
BEGIN
	SET @TagID = (SELECT Top 1 ID FROM Tags WHERE TagRFID = @TagRFID AND AssignDate IS NOT NULL)
END

IF @TagID is NULL
BEGIN
	DELETE FROM TagsToIssue Where TagRFID = @TagRFID;
	RETURN;
END

DECLARE  @LastModifiedDate DATETIME,
		 @TWtoREDSTime INT	

SELECT @TWtoREDSTime = TruckWash_REDS FROM ExpectedTimes

IF EXISTS (SELECT ID FROM TagsToIssue WHERE TagID = @TagID AND TagRFID = @TagRFID)
	BEGIN

		SELECT @LastModifiedDate = LastModifiedDate FROM TagsToIssue WHERE TagID = @TagID AND TagRFID = @TagRFID
		
		UPDATE TagsToIssue SET	LastModifiedDate = GetDate(),
								LastLocation = @LastLocation,
								IsDispatched = @IsDispatched,
								IsDeleted = @IsDeleted 
						  WHERE TagID = @TagID AND TagRFID = @TagRFID
	END
ELSE
	BEGIN
		DELETE FROM TagsToIssue Where TagRFID = @TagRFID;		
		INSERT INTO TagsToIssue(TagID,TagRFID,LastLocation,IsDeleted,IsDispatched,LastModifiedDate)
				VALUES(@TagID,@TagRFID,@LastLocation,@IsDeleted,@IsDispatched,GetDate())
		
		SET @LastModifiedDate = GetDate()
	END
 

--IF @LastLocation = 6 OR @LastLocation = 7
--	BEGIN
--		IF DateDiff(ss, @LastModifiedDate,GetDate()) > @TWtoREDSTime
--			BEGIN
--				Update Tags SET [StatusID] = 1 WHERE  ID=@TagID -- Set Idel
--			END
--	END


END


---------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  UserDefinedFunction [dbo].[TagReadAtREDS]    Script Date: 05/05/2011 13:07:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 
-- Description:	<Description, ,>
-- Modified Date 05-05-2011
-- =============================================
ALTER FUNCTION [dbo].[TagReadAtREDS]
(
 @TagRFID as Varchar(50),
 @TagID as BIGINT
)
RETURNS INT
AS
BEGIN 

	DECLARE @Result INT,		
			@LastModifiedDate DATETIME,	
			@IDTagsToIssue INT,
			@LastLocation INT,
			@IsDispatched INT,
			@TWtoREDSTime INT
			
	SET @Result =-1;

	--- @Result = 0 New Transaction (Go Normal)
	--- @Result = 1 Do Nothing
	--- @Result = 2 REDS2 (Go Normal)
	--- @Result = 3 Close Transaction 

	--SELECT @TWtoREDSTime = TruckWash_REDS  FROM ExpectedTimes ( Changed to include MAxDelay )

	SELECT @TWtoREDSTime = (IsNull(TruckWash_REDS,0) + IsNull(MaxDelay,0)) FROM ExpectedTimes

	SELECT @IDTagsToIssue = ID ,@LastModifiedDate = LastModifiedDate,@IsDispatched = IsDispatched,@LastLocation = LastLocation
	FROM TagsToIssue WHERE TagRFID = @TagRFID
	
	 --Check for Active/Non Active Transaction
		IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@TagID)  -- Non Active Transaction
			BEGIN				 
				IF @IDTagsToIssue IS NOT NULL -- Found in TagsToIssue Table
					BEGIN
--						IF @IsDispatched = 1
--							BEGIN								
								IF DateDiff(ss, @LastModifiedDate,GetDate()) > @TWtoREDSTime
									BEGIN
										-- Expected time expired
										SET @Result = 0;	-- New Transaction		
									END
								ELSE
									BEGIN
										-- With In Expected time
										SET @Result = 1;	-- Do Nothing														
									END
--							END
--						ELSE
--							SET @Result = 0;	-- New Transaction							
					END		
				ELSE						-- Not Found in TagsToIssue Table
					BEGIN	
						SET @Result = 0;	-- New Transaction
					END
			END
	    ELSE				-- Active Transaction
			BEGIN				 
				IF @IDTagsToIssue IS NOT NULL -- Found in TagsToIssue Table
					BEGIN
--						IF @IsDispatched = 1
--							BEGIN								
								IF DateDiff(ss, @LastModifiedDate,GetDate()) > @TWtoREDSTime
									BEGIN
										-- Expected time expired
										SET @Result = 2;	-- REDS2	
									END
								ELSE
									BEGIN
										-- With In Expected time
										SET @Result = 3;	-- Close Transaction										
									END
--							END
--						ELSE
--							SET @Result = 0;	-- New Transaction							
					END		
				ELSE						-- Not Found in TagsToIssue Table
					BEGIN	
						SET @Result = 2;	-- REDS2		
					END
			END
	
	 
	RETURN @Result

END

-------------------------------------------------AssignTag File Upload Changes--------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Tags_GetAllAssignedTagsByProponentID]    Script Date: 05/11/2011 18:03:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi
-- Create date: 10 August 2010
-- Description: Get all assigned tags by
--				1. Different Proponent ID
--				2. Selected Proponent ID
-- Modified : Vijay Mangtani, 11 05 2011
-- =============================================
ALTER PROCEDURE [dbo].[Tags_GetAllAssignedTagsByProponentID] --'''abc''',1
	-- Add the parameters for the stored procedure here
	@TagsRFID varchar(max),
	@ProponentID int
AS
BEGIN
	-- Insert statements for procedure here
--	Select ID,TagRFID,HopperID,ProponentID,Assigned,TruckID,StatusID from tags where [TagRFID] IN ( +  @TagRFID + ) and Assigned=1 AND ProponentID <> + 
		DECLARE @SQLDiffProponent nvarchar(max)
		DECLARE @SQLSelProponent nvarchar(max)

SET @SQLDiffProponent = N'Select ID,TagRFID,HopperID,ProponentID,Assigned,TruckID,StatusID from tags where [TagRFID] IN (' +  @TagsRFID + ') and Assigned=1 AND ProponentID <>' + cast(@ProponentID as varchar);
SET @SQLSelProponent =  N'Select ID,TagRFID,HopperID,ProponentID,Assigned,TruckID,StatusID from tags where [TagRFID] IN (' +  @TagsRFID + ') and Assigned=1 AND ProponentID =' + cast(@ProponentID as varchar);

DECLARE @SQLString nvarchar(MAX),
				@ParmDefinition nvarchar(500);

		DECLARE @CountDifferent int,
				@CountSame int;
 
		SET @SQLString = N'Select @CountOut = Count(*) from tags where [TagRFID] IN (' +  @TagsRFID + ') and Assigned=1 AND ProponentID <>' + cast(@ProponentID as varchar);
		SET @ParmDefinition = N'@CountOut int OUTPUT';

		EXECUTE sp_executesql @SQLString, @ParmDefinition, @CountOut=@CountDifferent OUTPUT;
		 
		SET @SQLString = N'Select @CountOut = Count(*) from tags where [TagRFID] IN (' +  @TagsRFID + ') and Assigned=1 AND ProponentID =' + cast(@ProponentID as varchar);
		SET @ParmDefinition = N'@CountOut int OUTPUT';

		EXECUTE sp_executesql @SQLString, @ParmDefinition, @CountOut=@CountSame OUTPUT;

EXECUTE sp_executesql @SQLDiffProponent
EXECUTE sp_executesql @SQLSelProponent
SELECT @CountDifferent as CountDifferent,@CountSame as CountSame
END

-----------------------------------------Modbus Screen Changes------------------------------------------------------------------------------


GO
IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'Hoppers_GetHoppers' AND type = N'P')
   DROP PROCEDURE Hoppers_GetHoppers 

GO
/****** Object:  StoredProcedure [dbo].[Hoppers_GetAllHoppers]    Script Date: 05/12/2011 16:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 12-05-2011
-- Description:	Get Hoppers
-- =============================================
CREATE PROCEDURE [dbo].[Hoppers_GetHoppers] 
	
AS
BEGIN
	 
	SET NOCOUNT ON;  
	SELECT	[ID]
			,'STOCKPAD ' + [HopperName] as PadName
			,0 AS PadStatus
			,0 AS PadStart
			,0 AS PadMatch
			,1 as Visibility
	FROM	[Hoppers]
	
 SELECT ServerName from ServerInfo WHERE IsActive = 1
	 
END

-------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateTruckCallUp]    Script Date: 05/18/2011 15:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- Modified date : 1 feb 2011
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100)
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
	,@TruckCallUpRetries int = 0 Output		
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int,
			@TransID INT
			 
			
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	IF EXISTS (Select ID from ActiveReads where TagID= @Tag_ID AND LastPosition = 2) -- Check if transaction exists
		SET @TransID = 1
	ELSE
		SET @TransID =  0


SELECT @TransID

IF @TransID = 1
	BEGIN		
			UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

			Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
			Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
			Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
			Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
			--Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp' 
			Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp'

			EXEC VMSDevices_GetAllByLocation '2' -- Get VMS Settings for Queue Area
	END
	 
	

END

---------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_OpenCloseTransaction]    Script Date: 05/21/2011 14:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;

	Declare @Result INT
	
	SET @MarkerID = 0;

--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF ((@Tag_ID IS NULL) OR (@Hopper_ID IS NULL))
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID		
		RETURN;
	END

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID	

							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID										
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END
	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID
							,FoundAtHopper
							,LastPosition)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								ExitTime,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID
								,FoundAtHopper
								,LastPosition

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
						
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					WHERE TagRFID = @TagRFID	 

					UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					Where TagID = @Tag_ID	

					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					--	SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					--	IF @Result = 2
					--		BEGIN
					--	UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	 
					--
					--	UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					--	 	Where TagID = @Tag_ID							 
					--	 END	
					--	ELSE
					--	 BEGIN
					--  SET @TransMessage = 'Transaction Completed.';
					--	 SET @TransStatus = 1;
					--	 UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	
					--	 END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						ExitTime,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID
						,FoundAtHopper
						,LastPosition)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							,FoundAtHopper
							,LastPosition
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID From Hoppers
			WHERE ID = @Hopper_ID			

END


--------------------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateTruckCallUp]    Script Date: 05/18/2011 15:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- Modified date : 1 feb 2011
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100)
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
	,@TruckCallUpRetries int = 0 Output		
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int,
			@TransID INT
			 
			
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	IF EXISTS (Select ID from ActiveReads where TagID= @Tag_ID AND LastPosition = 2) -- Check if transaction exists
		SET @TransID = 1
	ELSE
		SET @TransID =  0


SELECT @TransID

IF @TransID = 1
	BEGIN		
			UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

			Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
			Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
			Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
			Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
			--Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp' 
			Select @TruckCallUpRetries = [Value] From Configurations Where ParameterType = 'TruckCallUp'

			EXEC VMSDevices_GetAllByLocation '2' -- Get VMS Settings for Queue Area
	END
	 
	

END

-------------------------------------------Expected Time Report Changes----------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ExpectedTimes_GetReport]    Script Date: 05/23/2011 19:24:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 3rd Sep 2010
-- Description:	To Get the Expected Time Report
-- =============================================
ALTER PROCEDURE [dbo].[ExpectedTimes_GetReport]  --3,1,1

	@HopperID int,
	@TableID int,
	@ID int

AS
BEGIN
	SET NOCOUNT ON;



Declare @FromDate datetime,@ToDate datetime

Declare @ArchiveHopperTimeID int
Set @ArchiveHopperTimeID = 0


Declare @Hopper nvarchar(100)
Select @Hopper= HopperName From Hoppers where ID= @HopperID


-- "Configured" column Variables
Declare @REDS_Queue int, @QueueTimePerTruck int, @TruckCallResponse int, @Queue_Ent1 int, @Ent1_Ent2 int, @E2Hx int, @HxTwEx int


IF(@TableID=1)								--- ExpectedTimes
	Begin
			SELECT @REDS_Queue=[REDS_Queue]
				  ,@QueueTimePerTruck=[QueueTimePerTruck]
				  ,@TruckCallResponse=[TruckCallResponse]
				  ,@Queue_Ent1=[Queue_Ent1]
				  ,@Ent1_Ent2=[Ent1_Ent2]
				  ,@FromDate=ModifiedDate
				  ,@ToDate=getdate()
		   
		  FROM [ExpectedTimes] WHERE ID=@ID
	End
ELSE										-- ArchiveExpectedTimes
	Begin
			SELECT @REDS_Queue=[REDS_Queue]
				  ,@QueueTimePerTruck=[QueueTimePerTruck]
				  ,@TruckCallResponse=[TruckCallResponse]
				  ,@Queue_Ent1=[Queue_Ent1]
				  ,@Ent1_Ent2=[Ent1_Ent2]
				  ,@ArchiveHopperTimeID=ArchiveHopperTimeID
				  ,@FromDate=ModifiedDate
				  ,@ToDate=ArchiveDate
		   
		  FROM ArchiveExpectedTimes WHERE ID=@ID
	End


-- ********************************************************************************

IF (@ArchiveHopperTimeID=0)				-- ExpectedTimes
	Begin
		SELECT	@E2Hx=TimeFromEnt2
			   ,@HxTwEx=TimeToTW
   
			FROM Hoppers  WHERE ID=@HopperID

	End
ELSE										-- ArchiveExpectedTimes
	Begin
	
		Declare @HopperColumnName nvarchar(20)
		set @HopperColumnName='Hopper'+CAST(@HopperID AS varchar(5)) 

		DECLARE @ParmDefinition nvarchar(500);
		DECLARE @ReturnValue varchar(30);
		Declare @SQL nvarchar(2000)

		set @SQL= N'SELECT @Value = ' + @HopperColumnName + ' FROM ArchiveHopperTimes WHERE ID= ' + CAST(@ArchiveHopperTimeID AS varchar(5)) 
		SET @ParmDefinition = N'@Value varchar(30) OUTPUT';
		
		EXECUTE sp_executesql @SQL, @ParmDefinition,@Value=@ReturnValue OUTPUT;
		--SELECT @ReturnValue;

		--*******************************
	
		Declare @idx int
		IF @ReturnValue IS NOT NULL 
		BEGIN  	
			set @idx = 0
			set @idx = charindex('|',@ReturnValue)  --'1|3'   
			if @idx!=0 
			  BEGIN  
				set @E2Hx = left(@ReturnValue,@idx - 1) 		
				set @HxTwEx = right(@ReturnValue,len(@ReturnValue) - @idx)  
			  END
		END

	End


-- ********************************************************************************

--Print @FromDate
--Print @FromDate


-- TruckCallResponse (3)
Declare @QueueEntry1 nvarchar(100)
SELECT @QueueEntry1=Queue_Ent1 FROM ExpectedTimes 
 
-- Entry2_HopperTime (6)
Declare @HopperName nvarchar(100)
SELECT @HopperName=HopperName FROM Hoppers WHERE ID=@HopperID 

-- HopperTime_TW/Exit (7)
Declare @HopperName1 nvarchar(100)
SELECT @HopperName1=HopperName FROM Hoppers WHERE ID=@HopperID 


-- REDS_Queue (1)
SELECT 'REDS_Queue' as R, 
ISNULL(CONVERT(char(20), @REDS_Queue), 'N.A.') AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,REDS,[Queue]))), 'N.A.') AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,REDS,[Queue]))), 'N.A.')  AS StandardDeviation,
1 as Row 

FROM ArchiveReads	

WHERE REDS IS NOT NULL AND [Queue] IS NOT NULL AND [Queue]>REDS 

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper


UNION

-- QueueTimePerTruck (2)
SELECT 'QueueTimerPerTruck' as R,
@QueueTimePerTruck AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,[Queue],TruckCallUp))), 'N.A.')  AS StandardDeviation 
,2 as Row 

FROM ArchiveReads	

WHERE [Queue] IS NOT NULL AND TruckCallUp IS NOT NULL AND TruckCallUp>[Queue]
		
		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- TruckCallResponse (3)

--Declare @QueueEntry1 nvarchar(100)
--SELECT @QueueEntry1=Queue_Ent1 FROM ExpectedTimes 


-- removed @QueueEntry1 from all parameters

SELECT 'TruckCallResponse' as R,
ISNULL(CONVERT(char(20), @TruckCallResponse), 'N.A.')  AS Configured, 
--ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,TruckCallUp,Entry1)-@QueueEntry1)), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,TruckCallUp,Entry1) )), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,TruckCallUp,Entry1))), 'N.A.')  AS StandardDeviation 
,3 as Row 

FROM ArchiveReads	

WHERE TruckCallUp IS NOT NULL AND Entry1 IS NOT NULL AND Entry1>TruckCallUp

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- Queue_Entry1 (4)
SELECT 'Queue_Ent1' as R,
ISNULL(CONVERT(char(20), @Queue_Ent1), 'N.A.')  AS Configured,
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,[Queue],Entry1))), 'N.A.')  AS StandardDeviation 
,4 as Row 

FROM ArchiveReads	

WHERE [Queue] IS NOT NULL AND Entry1 IS NOT NULL AND Entry1>[Queue]

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- Entry1_Entry2 (5)
SELECT 'Ent1_Ent2' as R, 
ISNULL(CONVERT(char(20), @Ent1_Ent2), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,Entry1,Entry2))), 'N.A.')  AS StandardDeviation 
,5 as Row 

FROM ArchiveReads	

WHERE Entry1 IS NOT NULL AND Entry2 IS NOT NULL AND Entry2>Entry1

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper


UNION

-- Entry2_HopperTime (6)

--Declare @HopperName nvarchar(100)
--SELECT @HopperName=HopperName FROM Hoppers WHERE ID=@HopperID 

SELECT 'Ent2 - PAD' as R, 
ISNULL(CONVERT(char(20), @E2Hx), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,Entry2,HopperTime))), 'N.A.')  AS StandardDeviation 
,6 as Row 

FROM ArchiveReads	

WHERE Entry2 IS NOT NULL AND HopperTime IS NOT NULL AND HopperTime>Entry2 AND  HopperName=@HopperName

		AND ArchiveDate BETWEEN @FromDate AND @ToDate
		AND HopperName = @Hopper

UNION

-- HopperTime_TW/Exit (7)

--Declare @HopperName1 nvarchar(100)
--SELECT @HopperName1=HopperName FROM Hoppers WHERE ID=@HopperID 

SELECT 'PAD - TW/EX + Unloading' as R, 
ISNULL(CONVERT(char(20), @HxTwEx), 'N.A.')  AS Configured, 
ISNULL(CONVERT(char(20), AVG(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Average,
ISNULL(CONVERT(char(20), MIN(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Minimum,
ISNULL(CONVERT(char(20), MAX(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS Maximum,
ISNULL(CONVERT(char(20), STDEV(DATEDIFF(second,HopperTime,TWExit))), 'N.A.')  AS StandardDeviation 
,7 as Row 

FROM ArchiveReads AR INNER JOIN 	
	(SELECT ID,	CASE WHEN (TruckWash IS NOT NULL AND  ExitTime IS NOT NULL) 
						THEN 
						CASE WHEN TruckWash > ExitTime THEN ExitTime 
							 ELSE TruckWash END 
					WHEN (TruckWash IS NOT NULL) THEN TruckWash	 
					WHEN (ExitTime IS NOT NULL) THEN ExitTime END TWExit

		FROM ArchiveReads WHERE TruckWash IS NOT NULL OR ExitTime IS NOT NULL) TEMP 

ON AR.ID=TEMP.ID

WHERE HopperTime IS NOT NULL AND TWExit IS NOT NULL AND TWExit>HopperTime AND  HopperName=@HopperName1

	AND ArchiveDate BETWEEN @FromDate AND @ToDate
	AND HopperName = @Hopper

ORDER BY Row
---********************************************************************


END

-------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_GetAllForTrafficPage]    Script Date: 06/18/2011 11:00:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Somkant Pathak
-- Create date: 17 August 2010
-- Description:	Get All Active Reads For Traffic Page
-- ===================================================
ALTER PROCEDURE [dbo].[ActiveReads_GetAllForTrafficPage]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT     ActiveReads.ID, ActiveReads.TagID, ActiveReads.HopperID,ActiveReads.FoundAtHopper, ActiveReads.StartTime, ActiveReads.REDS, ActiveReads.Queue, ActiveReads.Entry1, 
                      ActiveReads.Entry2, ActiveReads.HopperTime, ActiveReads.TruckWash,ActiveReads.ExitTime, ActiveReads.REDS2, ActiveReads.TruckCallUp, ActiveReads.LastPosition, 
                      ActiveReads.EntryORQueue, Tags.TagRFID,Tags.TruckID, Tags.TruckID + '-' + Proponents.ProponentName + '-' + Hoppers.HopperName AS TruckName, 
                      Tags.ProponentID
	FROM         Hoppers INNER JOIN
                      ActiveReads ON Hoppers.ID = ActiveReads.HopperID INNER JOIN
                      Proponents INNER JOIN
                      Tags ON Proponents.ID = Tags.ProponentID ON ActiveReads.TagID = Tags.ID
	ORDER BY ActiveReads.StartTime ASC

	SELECT  Top 30 TruckID,TruckID + '-' + ISNULL(ProponentName, '') + '-' + HopperName AS TrasactionName, 
			ID, ExitTime, ArchiveDate, TruckWash,ISNULL(TransactionID,0) as TransID,
			ISNULL(TransMessage, '') AS TransMessage, ISNULL(TransStatus, '') AS TransStatus
	FROM    ArchiveReads
	--WHERE	ArchiveDate= GETDATE()
	ORDER BY ArchiveDate DESC

	
	SELECT Count(*) as [Count] FROM Readers WHERE ReaderStatus is not NULL AND ReaderStatus <> 'OK' 


		
END
