--USE [RampTMS_TestingFirst]
GO
/****** Object:  StoredProcedure [dbo].[ActiveReads_OpenCloseTransaction]    Script Date: 05/21/2011 14:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;

	Declare @Result INT
	
	SET @MarkerID = 0;

--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF ((@Tag_ID IS NULL) OR (@Hopper_ID IS NULL))
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID		
		RETURN;
	END

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID	

							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID										
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END
	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID
							,FoundAtHopper
							,LastPosition)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								ExitTime,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID
								,FoundAtHopper
								,LastPosition

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
						
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					WHERE TagRFID = @TagRFID	 

					UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					Where TagID = @Tag_ID	

					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					--	SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					--	IF @Result = 2
					--		BEGIN
					--	UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	 
					--
					--	UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					--	 	Where TagID = @Tag_ID							 
					--	 END	
					--	ELSE
					--	 BEGIN
					--  SET @TransMessage = 'Transaction Completed.';
					--	 SET @TransStatus = 1;
					--	 UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	
					--	 END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						ExitTime,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID
						,FoundAtHopper
						,LastPosition)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							,FoundAtHopper
							,LastPosition
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID From Hoppers
			WHERE ID = @Hopper_ID			

END


--------------------------------------------------------------------------------------------------------------------------------------

