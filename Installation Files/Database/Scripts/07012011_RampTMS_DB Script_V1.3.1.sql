
IF NOT EXISTS (SELECT * FROM syscolumns
  WHERE id=object_id('Markers') and NAME='SerialNumber')
    ALTER TABLE Markers ADD SerialNumber VARCHAR(50) NULL

GO
/****** Object:  StoredProcedure [dbo].[Markers_InsertMarker]    Script Date: 12/21/2010 18:21:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratiksha Bandi	
-- Create date: 19 August 2010
-- Description:	Insert Markers
-- =============================================

ALTER PROCEDURE [dbo].[Markers_InsertMarker]
	-- Add the parameters for the stored procedure here
			@MarkerLoopID nvarchar(100)
           --,@IPAddress nvarchar(20)
           ,@MarkerPosition nvarchar(50)
           ,@Location int
		,@SerialNumber Varchar(50)
AS
BEGIN
	
    -- Insert statements for procedure here
	INSERT INTO [Markers]
           (
			[MarkerLoopID]
           --,[IPAddress]
           ,[MarkerPosition]
           ,[Location]
			,[SerialNumber]
			)
     VALUES
           (
			@MarkerLoopID 
           --,@IPAddress 
           ,@MarkerPosition 
           ,@Location 
			,@SerialNumber
		   )
	Select @@IDENTITY
END
GO
---------------------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'Markers_GetMarkersInfo' AND type = N'P')
   DROP PROCEDURE Markers_GetMarkersInfo
GO

/****** Object:  StoredProcedure [dbo].[Markers_GetMarkersInfo]    Script Date: 12/21/2010 18:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 DEC 2010
-- Description:	Get all Markers Info
-- =============================================

CREATE PROCEDURE [dbo].[Markers_GetMarkersInfo]
	-- Add the parameters for the stored procedure here
	@MarkerHealthCheckFlag Varchar(10) = null Output 
AS
BEGIN
-- Insert statements for procedure here
  SELECT [ID]
	  ,[MarkerLoopID]
	  ,SerialNumber
  FROM [Markers]
  WHERE SerialNumber is NOT NULL AND SerialNumber <> ''

  SELECT @MarkerHealthCheckFlag = [Value]
  FROM	Configurations
  WHERE ParameterType = 'MarkerHealthCheck'

END 
GO 
---------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'MarkerHealthCheck')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	--SET IDENTITY_INSERT Configurations ON;
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'MarkerHealthCheck','Boolean [ 1 / 0 ]','1')
	--SET IDENTITY_INSERT Configurations OFF;
END
GO
---------------------------------------------------------------------------------------------------------------------


--------------------------------------------- VMS Logging ------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
 
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateEntryORQueue]    Script Date: 12/29/2010 18:59:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 6th Aug 2010
-- Description:	To Update Entry OR Queue value in ActiveReads TABLE
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_UpdateEntryORQueue]
	@TagID Bigint,
	@TagRFID Varchar(50),
	@EntryORQueue int,
	@VMSLogging int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;


	 IF @TagID = 0 
		 SELECT @TagID = ID FROM Tags WHERE TagRFID = @TagRFID

		UPDATE ActiveReads SET EntryORQueue = @EntryORQueue Where TagID = @TagID

		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 

END
GO
-------------------------------------------------------------------------------------------------------------------
 
/****** Object:  StoredProcedure [dbo].[ActiveReads_UpdateTruckCallUp]    Script Date: 12/29/2010 19:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 6th AUG 2010
-- Description:	To Update the "TruckCallUp" Time
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_UpdateTruckCallUp]
	
	@TagRFID nvarchar(100),
	@VMSLogging int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;

	declare @Tag_ID int;
	set @Tag_ID = (Select ID from Tags where TagRFID=@TagRFID)

	UPDATE ActiveReads SET TruckCallUp= getdate()
			WHERE TagID=@Tag_ID

	Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging'  

END

GO
---------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT ID FROM Configurations WHERE ParameterType = 'VMSLogging')
BEGIN
	DECLARE @ID INT

	SELECT @ID = Count(*) FROM Configurations
	print @ID
	SET @ID = @ID + 1;
	--SET IDENTITY_INSERT Configurations ON;
	INSERT INTO Configurations(ID,ParameterType,Field,[Value]) Values(@ID,'VMSLogging','Value [ 0 / 1 / 2 ]','1')
	--SET IDENTITY_INSERT Configurations OFF;
END
GO
---------------------------------------------------------------------------------------------------------------------

