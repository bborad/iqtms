using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Ramp.MiddlewareController.Common;

namespace Ramp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        public static RunningMode appRunningMode = RunningMode.ReducedFunctionality;

        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new IQTMSService()
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
