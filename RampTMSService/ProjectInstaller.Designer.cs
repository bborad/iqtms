namespace Ramp
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IQTMSServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.IQTMSServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // IQTMSServiceProcessInstaller
            // 
            this.IQTMSServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.IQTMSServiceProcessInstaller.Password = null;
            this.IQTMSServiceProcessInstaller.Username = null;
            // 
            // IQTMSServiceInstaller
            // 
            this.IQTMSServiceInstaller.ServiceName = "IQTMSService";
            this.IQTMSServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.IQTMSServiceProcessInstaller,
            this.IQTMSServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller IQTMSServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller IQTMSServiceInstaller;
    }
}
