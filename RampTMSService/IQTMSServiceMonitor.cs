using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections; 
using System.Diagnostics;
using System.ServiceProcess;
using DBUtility = Ramp.DBConnector.Common;
using System.Data;
using Ramp.MiddlewareController.Common; 


namespace Ramp
{
    public class IQTMSServiceMonitor
    {
        static int IsLocalServiceActive = -1;

        static int SwitchOverInterval;

        public static void CheckServiceStatus()
        {
            bool result = false;
            try
            {
                ServiceController sc = new ServiceController("IQTMSService");
                if (sc.Status == ServiceControllerStatus.Running)
                {
                    sc.ExecuteCommand(1);
                    result = true;
                }

            }
            catch (Exception ex)
            {
                result = false;
            }

            try
            {
                if (result == true)
                {
                    // Update the time in the DB to Current Time and Server = localServer
                }
            }
            catch
            {
            }

        }

        public static int PrimaryServerCheckNTakeOver()
        {
            int result = -1;
            try
            {
                if (IsLocalServiceActive == -1)
                {
                    // Read the primary server running the Services

                    string serverName = "";  

                    DataTable dtServerInfo = DBUtility.DBConnectorUtility.GetRunningServerInfo();

                    if (dtServerInfo != null && dtServerInfo.Rows.Count > 0)
                    {
                        serverName = dtServerInfo.Rows[0]["ServerName"].ToString();                       

                        if (serverName.ToLower() == Environment.MachineName.ToLower())
                        {
                            IsLocalServiceActive = 1; // Active
                            //Utility.GetAllConfigurations();
                            result = 1;
                        }
                        else
                        {
                            IsLocalServiceActive = 0; // InActive
                            result = 0;
                        }

                    }
                    else
                    {
                        serverName = Environment.MachineName;
                        DBUtility.DBConnectorUtility.SwitchToNewServer(serverName.Trim());

                        IsLocalServiceActive = 1; // Active
                        result = 1;
                       // Utility.GetAllConfigurations();
                    }              

                }
                
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public static void CheckStatus()
        {
            if (IsLocalServiceActive == 1)
            {
                UpdateServiceStatus();
            }
            else if(IsLocalServiceActive == 0)
            {
                PollForTakeOver();
            }
        }

        public static int UpdateServiceStatus()
        {
            int result = -1;
            try
            {               
                // Update the response time and the running server in the db               
                string serverName = Environment.MachineName.Trim();
                result = DBUtility.DBConnectorUtility.UpdateServerStatus(serverName);

                if (result == 1)
                {
                    IsLocalServiceActive = 1; // Active
                }
                else if (result == 0)
                {
                    IsLocalServiceActive = 0; // InActive
                }

            }
            catch (Exception ex)
            {
                
            }

            return result;
        }

        public static int PollForTakeOver()
        {
            int result = -1;
            try
            {
                // Read last response time from active service 

                DateTime dtLastResponse = DateTime.Now; 

                string serverName = "";

                DataTable dtServerInfo = DBUtility.DBConnectorUtility.GetRunningServerInfo();

                if (dtServerInfo != null && dtServerInfo.Rows.Count > 0)
                {
                   // serverName = dtServerInfo.Rows[0]["ServerName"].ToString();
                    dtLastResponse = Convert.ToDateTime(dtServerInfo.Rows[0]["LastResponseTime"]);

                    if (dtLastResponse.AddSeconds(ExpectedValues.ServerResponseTime) <= DateTime.Now)
                    {
                        serverName = Environment.MachineName;
                        DBUtility.DBConnectorUtility.SwitchToNewServer(serverName.Trim());
                        IsLocalServiceActive = 1; // Active
                        result = 1;
                        //Utility.GetAllConfigurations();
                    }
                    else
                    {
                        IsLocalServiceActive = 0; // InActive
                        result = 0;
                    }

                }
                else
                {
                    serverName = Environment.MachineName;
                    DBUtility.DBConnectorUtility.SwitchToNewServer(serverName.Trim());

                    IsLocalServiceActive = 1; // Active
                    result = 1;
                   // Utility.GetAllConfigurations();

                } 

            }
            catch
            {
            }

            return result;
        }

        public static void StartServiceChecker()
        {
            if (IsLocalServiceActive == 1)
            {
                CheckServiceStatus();
            }
            else if (IsLocalServiceActive == 0)
            {
                PollForTakeOver();
            }
        }
    }
}
