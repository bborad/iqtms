using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Ramp.MiddlewareController.Common;
using Ramp.BusinessLogic.Common;
using System.Threading;
using Ramp.DBConnector.Common;
using System.Diagnostics.Eventing;

using NLog;

namespace Ramp
{
    partial class IQTMSService : ServiceBase
    {

#if DEBUG
        private static Logger log = LogManager.GetLogger("IQTMSService");
#endif
        bool appRedundancyCheck = true;

        System.Timers.Timer tmrREDS;
        System.Timers.Timer tmrQUEUE;
        System.Timers.Timer tmrENTRY1;
        System.Timers.Timer tmrHOPPER;
        System.Timers.Timer tmrTRUCKWASH;
        System.Timers.Timer tmrEXIT;

        bool serviceStopped = false;

        // 05/05/2011 ( Issue Tag office functionality changes )
        System.Timers.Timer tmrIssueTagOffice;

        System.Timers.Timer tmrModbusRead_HopperStatus;

        System.Timers.Timer tmrUpdateServerInfo;

        System.Timers.Timer tmrPollForTakeOver;

        System.Timers.Timer tmrMarkerHealthStatus;

        public IQTMSService()
        {
            InitializeComponent();
            if (!EventLog.Exists("IQTMSLog"))
                EventLog.CreateEventSource("IQTMSSource", "IQTMSLog");         

            IQTMSEventLogger.Source = "IQTMSSource";
            IQTMSEventLogger.Log = "IQTMSLog";
            IQTMSEventLogger.Clear();
            // Changes to manage the event log file size.
            IQTMSEventLogger.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 0);
          
            IQTMSEventLogger.MaximumKilobytes = 5120;

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException); 
           

          //  WriteEventLog("On Serives Initialization -- " + DateTime.Now.ToString(), EventLogEntryType.Information);
                    
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
#if DEBUG
            log.Debug("Catch unhandled exception. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            try
            {
                if (e.IsTerminating)
                {
                    if (e.ExceptionObject != null)
                    {
                        Exception ex = (Exception)e.ExceptionObject;
                        string msg = "Unhandled exception \n" + ex.Message + "\n" + Convert.ToString(ex.Source) + "\n" + Convert.ToString(ex.StackTrace);
                        WriteEventLog(msg, EventLogEntryType.Error);
                    }
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in CurrentDomain_UnhandledException method. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        bool GetAllConfigurations()
        {
            bool result = true;
            try
            {                
            
                Utility.ConfigureReaders();
                WriteEventLog("Readers Configured.", EventLogEntryType.Information);
                Utility.GetAllHoppersInFacility();
                WriteEventLog("Hoppers Configured.", EventLogEntryType.Information);
                Utility.GetAllMarkers();
                WriteEventLog("Markers Configured.", EventLogEntryType.Information);  
                Utility.GetAllVMSDevices();
                WriteEventLog("VMS Device Configuration.", EventLogEntryType.Information);
                Utility.GetConfigurationValues();
                WriteEventLog("Configuration Values.", EventLogEntryType.Information);
                
                Utility.StartModbusServer();
                if (BLUtility.IsModbusConnected)
                    WriteEventLog("Modbus Started.", EventLogEntryType.Information);
                else
                    WriteEventLog("Modbus not Started.", EventLogEntryType.Information);
               
                ConfigureModbusReadTimer();

                WriteEventLog("Getting active Trucks in facility.", EventLogEntryType.Information);

                int count = BLUtility.GetActiveTrucksInFacility(1);

                WriteEventLog( count.ToString() + " active Trucks found in facility.", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                WriteEventLog("On Serives Initialization --  " + ex.Message, EventLogEntryType.FailureAudit);
                result = false;
            }
            return result;
        }

        void CheckForActivatingLocalService()
        {      

            ConfigureOtherTimers();

            if (IQTMSServiceMonitor.PrimaryServerCheckNTakeOver() == 1)
            {
                WriteEventLog("Local Service Started as Primary Server.", EventLogEntryType.Information);
                // Configure The System
                if (GetAllConfigurations())
                {
                    ConfigureReaderTimers();
                    StartReaderTimers();
                }

                tmrUpdateServerInfo.Start();
                WriteEventLog("Timer Started for Updating Server Status. ", EventLogEntryType.Information);                
               
            }
            else
            {
                WriteEventLog("Local Service is set as Inactive.", EventLogEntryType.Information);

                tmrPollForTakeOver.Start();
                WriteEventLog("Timer Started to poll for running Server Status Check.", EventLogEntryType.Information);
            
              
            }

        }

        void ConfigureOtherTimers()
        {
            tmrUpdateServerInfo = new System.Timers.Timer();
            tmrUpdateServerInfo.Elapsed +=new System.Timers.ElapsedEventHandler(tmrUpdateServerInfo_Elapsed);
            tmrUpdateServerInfo.Interval = ExpectedValues.UpdateServerStatusTime * 1000;
            tmrUpdateServerInfo.Stop();

            tmrPollForTakeOver = new System.Timers.Timer();
            tmrPollForTakeOver.Elapsed += new System.Timers.ElapsedEventHandler(tmrPollForTakeOver_Elapsed);
            tmrPollForTakeOver.Interval = (ExpectedValues.ServerPollingTime/2) * 1000;
            tmrPollForTakeOver.Stop();
        }

        void tmrPollForTakeOver_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
            log.Debug("Enter Stop tmrPollForTakeOver timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            tmrPollForTakeOver.Stop();
            if (IQTMSServiceMonitor.PollForTakeOver() == 1)
            {
                WriteEventLog("Local Service Started as Primary Server (Take Over).", EventLogEntryType.Information);
                // Configure The System
                if (GetAllConfigurations())
                {
                    ConfigureReaderTimers();
                    StartReaderTimers();
                }

                tmrUpdateServerInfo.Start();
                WriteEventLog("Timer Started for Updating Server Status. ", EventLogEntryType.Information);
             
               
            }
            else
            {
                if (!serviceStopped)
                tmrPollForTakeOver.Start();
            }

#if DEBUG
            log.Debug("Exit- tmrPollForTakeOver timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrPollForTakeOver.Enabled, tmrPollForTakeOver.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
        }

        void tmrUpdateServerInfo_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrUpdateServerInfo.Stop();

            if (IQTMSServiceMonitor.UpdateServiceStatus() == 0)
            {
                WriteEventLog("Local Service is set as Inactive.", EventLogEntryType.Information);

                StopReaderTimers();

                tmrPollForTakeOver.Start();
                WriteEventLog("Timer Started to poll for running Server Status Check.", EventLogEntryType.Information);
                             
            }
            else
            {
                if (!serviceStopped)
                tmrUpdateServerInfo.Start();
            }
        }

        void ConfigureReaderTimers()
        {
            tmrREDS = new System.Timers.Timer();
            tmrREDS.AutoReset = true;
            tmrREDS.Elapsed += new System.Timers.ElapsedEventHandler(tmrREDS_Elapsed);
            tmrREDS.Interval = 1000 * ExpectedValues.TagReadFrequency;

            tmrQUEUE = new System.Timers.Timer();
            tmrQUEUE.AutoReset = true;
            tmrQUEUE.Elapsed += new System.Timers.ElapsedEventHandler(tmrQUEUE_Elapsed);
            tmrQUEUE.Interval = 1000 * ExpectedValues.TagReadFrequency;

            tmrENTRY1 = new System.Timers.Timer();
            tmrENTRY1.AutoReset = true;
            tmrENTRY1.Elapsed += new System.Timers.ElapsedEventHandler(tmrENTRY1_Elapsed);
            tmrENTRY1.Interval = 1000 * ExpectedValues.TagReadFrequency;

            tmrHOPPER = new System.Timers.Timer();
            tmrHOPPER.AutoReset = true;
            tmrHOPPER.Elapsed += new System.Timers.ElapsedEventHandler(tmrHOPPER_Elapsed);
            tmrHOPPER.Interval = 1000 * ExpectedValues.TagReadFrequency;

            tmrTRUCKWASH = new System.Timers.Timer();
            tmrTRUCKWASH.AutoReset = true;
            tmrTRUCKWASH.Elapsed += new System.Timers.ElapsedEventHandler(tmrTRUCKWASH_Elapsed);
            tmrTRUCKWASH.Interval = 1000 * ExpectedValues.TagReadFrequency;

            tmrEXIT = new System.Timers.Timer();
            tmrEXIT.AutoReset = true;
            tmrEXIT.Elapsed += new System.Timers.ElapsedEventHandler(tmrEXIT_Elapsed);
            tmrEXIT.Interval = 1000 * ExpectedValues.TagReadFrequency;

            // 05/05/2011 ( Issue Tag office functionality changes )
            tmrIssueTagOffice = new System.Timers.Timer();
            tmrIssueTagOffice.AutoReset = true;
            tmrIssueTagOffice.Elapsed += new System.Timers.ElapsedEventHandler(tmrIssueTagOffice_Elapsed);
            tmrIssueTagOffice.Interval = 1000 * ExpectedValues.TagReadFrequency;


        }

        void tmrIssueTagOffice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
             
            log.Debug("Enter Stop IssueTagOffice timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            tmrIssueTagOffice.Stop();
            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.IssueTagOffice);
#if DEBUG
                 int n = 0;
                if(lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for IssueTagOffice timer. Thread {1}",n , Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransactionIssueTagOffice(lstTags, Location.IssueTagOffice);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in IssueTagOffice timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif 
                WriteEventLog("Issue Tag Office - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);
            }

            if (!serviceStopped)
            tmrIssueTagOffice.Start();
#if DEBUG
            log.Debug("Exit- IssueTagOffice timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrIssueTagOffice.Enabled, tmrIssueTagOffice.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
        }

        void ConfigureModbusReadTimer()
        {
            tmrModbusRead_HopperStatus = new System.Timers.Timer();
            tmrModbusRead_HopperStatus.AutoReset = true;
            tmrModbusRead_HopperStatus.Elapsed += new System.Timers.ElapsedEventHandler(tmrModbusRead_HopperStatus_Elapsed);
            tmrModbusRead_HopperStatus.Interval = 1000 * ExpectedValues.TagReadFrequency;
            
        }

        void tmrModbusRead_HopperStatus_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrModbusRead_HopperStatus.Stop();
            try
            {

                BLUtility.UpdateHopperStatus();
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException("Exceptoin thrown while updating HopperStatus", ex);
#endif
            }
            if (!serviceStopped)
            tmrModbusRead_HopperStatus.Start();
        }

        void StartReaderTimers()
        {
            #region REDS timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.REDS))
                {
                    if (!tmrREDS.Enabled)
                    {
                        tmrREDS.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if (tmrREDS != null)
                        tmrREDS.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting REDS timer: {0}",ex.Message);
            }
            #endregion

            #region Queue Timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.QUEUE))
                {
                    if (!tmrQUEUE.Enabled)
                    {
                        tmrQUEUE.Start();
                        Thread.Sleep(1000);
                    }

                }
                else
                {
                    if(tmrQUEUE != null)
                    tmrQUEUE.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting Queue timer: {0}", ex.Message);
            }
            #endregion

            #region Entry1 timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.ENTRY1))
                {
                    if (!tmrENTRY1.Enabled)
                    {
                        tmrENTRY1.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if(tmrENTRY1 != null)
                    tmrENTRY1.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting Entry1 timer: {0}", ex.Message);
            }
            #endregion

            #region Hopper timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.HOPPER))
                {
                    if (!tmrHOPPER.Enabled)
                    {
                        tmrHOPPER.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if(tmrHOPPER != null)
                    tmrHOPPER.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting Hopper timer: {0}", ex.Message);
            }
            #endregion

            #region TruckWash timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.TRUCKWASH))
                {
                    if (!tmrTRUCKWASH.Enabled)
                    {
                        tmrTRUCKWASH.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if(tmrTRUCKWASH != null)
                    tmrTRUCKWASH.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting TruckWash timer: {0}", ex.Message);
            }
            #endregion

            #region ExitGate Timer
            try
            {
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.EXITGATE))
                {
                    if (!tmrEXIT.Enabled)
                    {
                        tmrEXIT.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if(tmrEXIT != null)
                    tmrEXIT.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting ExitGate timer: {0}", ex.Message);
            }
            #endregion

            #region IssueTagOffice
            try
            {
                // changes for Issue Tag Process
                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.IssueTagOffice))
                {
                    if (!tmrIssueTagOffice.Enabled)
                    {
                        tmrIssueTagOffice.Start();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    if(tmrIssueTagOffice != null)
                    tmrIssueTagOffice.Stop();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while starting IssueTagOFfice timer: {0}", ex.Message);
            }
            #endregion

            if (tmrModbusRead_HopperStatus == null)
            {
                ConfigureModbusReadTimer();              
            }

            tmrModbusRead_HopperStatus.Start();

            WriteEventLog("Reader Iimer(s) Started.", EventLogEntryType.Information);

            SettingsForMarkerHealthCheck();
        }

        void SettingsForMarkerHealthCheck()
        {
           tmrMarkerHealthStatus = new System.Timers.Timer();
           tmrMarkerHealthStatus.Elapsed += new System.Timers.ElapsedEventHandler(tmrMarkerHealthStatus_Elapsed);
           tmrMarkerHealthStatus.AutoReset = true;
           object value;

           try
           {
              value = System.Configuration.ConfigurationSettings.AppSettings["MarkerHealthCheckInterval"];
              int interval = Convert.ToInt32(value);
              if (interval >= 0)
              {
                  tmrMarkerHealthStatus.Interval = interval * 1000;
                  tmrMarkerHealthStatus.Start();

                  WriteEventLog("Marker Health Check Timer Started.", EventLogEntryType.Information);

                  value = System.Configuration.ConfigurationSettings.AppSettings["MarkerHealthCheckLogPath"];

                  Utility.MarkersHealthCheckPath = Convert.ToString(value);

                  if (Utility.MarkersHealthCheckPath.Length > 0)
                  {
                      if (!System.IO.Directory.Exists(Utility.MarkersHealthCheckPath))
                      {
                          System.IO.Directory.CreateDirectory(Utility.MarkersHealthCheckPath);
                          WriteEventLog("Marker Health Check - Output directory created.", EventLogEntryType.Information);
                      }
                  } 
                 
              }
           }
           catch (Exception ex)
           {
               WriteEventLog("Marker Health Check : " + ex.Message, EventLogEntryType.Error);
           }


        }

        void tmrMarkerHealthStatus_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                tmrMarkerHealthStatus.Stop();
                Utility.SetMarkersLoopID_Routine();
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException("Exception thrown in tmrMarkerHealthStatus_Elapsed method. Thread: "+Thread.CurrentThread.ManagedThreadId.ToString()+" .", ex);
#endif
                WriteEventLog("Marker Health Check Routine: " + ex.Message + "\n" + Convert.ToString(ex.StackTrace), EventLogEntryType.Error);
            }
            finally
            {
                tmrMarkerHealthStatus.Start();
            }
        }

        void StopReaderTimers()
        {
#if DEBUG
            log.Debug("Stop All timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif 
            if (tmrREDS != null)
            {
                tmrREDS.Stop();
            }
            if (tmrQUEUE != null)
            {
                tmrQUEUE.Stop();
            }
            if (tmrENTRY1 != null)
            {
                tmrENTRY1.Stop();
            }
            if (tmrHOPPER != null)
            {
                tmrHOPPER.Stop();
            }
            if (tmrTRUCKWASH != null)
            {
                tmrTRUCKWASH.Stop();
            }
            if (tmrEXIT != null)
            {
                tmrEXIT.Stop();
            }

            if (tmrIssueTagOffice!= null)
            {
                tmrIssueTagOffice.Stop();
            }

            if (tmrModbusRead_HopperStatus != null)
            {
                tmrModbusRead_HopperStatus.Stop();
            }

            if (tmrMarkerHealthStatus != null)
            {
                tmrMarkerHealthStatus.Stop();
            }

            WriteEventLog("Reader Iimer(s) Stopped.", EventLogEntryType.Information);

        }

        void tmrEXIT_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
            log.Debug("Stop/Enter Ex timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif  
            tmrEXIT.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.EXITGATE);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for Ex timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransactionTruckWash(lstTags, Location.EXITGATE);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in Ex timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif 
                WriteEventLog("Exit - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);
            }

            if (!serviceStopped)
            tmrEXIT.Start();
#if DEBUG
            log.Debug("Exit-Ex timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrEXIT.Enabled, tmrEXIT.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
        }

        void tmrTRUCKWASH_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
            log.Debug("Stop TW timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif  
            tmrTRUCKWASH.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.TRUCKWASH);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for TW timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransactionTruckWash(lstTags, Location.TRUCKWASH);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in TW timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif   
                WriteEventLog("Truck Wash - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);
            }

            if (!serviceStopped)
            tmrTRUCKWASH.Start();

#if DEBUG
            log.Debug("TW timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrTRUCKWASH.Enabled, tmrTRUCKWASH.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
        }

        void tmrHOPPER_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
            log.Debug("Enter Stop H timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif  
            tmrHOPPER.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.HOPPER);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for H timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransaction(lstTags, Location.HOPPER);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in H timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif 
                WriteEventLog("Hoppers - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);
            }

            if (!serviceStopped)
            tmrHOPPER.Start();
#if DEBUG
            log.Debug("Exit- H timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrHOPPER.Enabled, tmrHOPPER.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
        }

        void tmrENTRY1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
#if DEBUG
            log.Debug("Stop/Enter E1 timer. Thread {0}",Thread.CurrentThread.ManagedThreadId.ToString());
#endif           
            tmrENTRY1.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.ENTRY1);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for E1 timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransactionEntryGate(lstTags, Location.ENTRY1);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in E1 timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()),ex);
#endif                
                WriteEventLog("ENTRY1 - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);

            }

            if (!serviceStopped)
            tmrENTRY1.Start();

#if DEBUG
            log.Debug("Exit-E1 timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrENTRY1.Enabled, tmrENTRY1.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif

        }

        void tmrQUEUE_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

#if DEBUG
            log.Debug("Q timer Stop. Thread {0}",  Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            tmrQUEUE.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.QUEUE);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for Q timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                if (lstTags.Count > 0)
                    Utility.UpdateTransactionQueueReads(lstTags, Location.QUEUE);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in Q timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif 
                WriteEventLog("QUEUE - " + DateTime.Now.ToString() + " " + ex.Message, EventLogEntryType.FailureAudit);

            }
            if (!serviceStopped)
            tmrQUEUE.Start();
#if DEBUG
            log.Debug("Q timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped, tmrQUEUE.Enabled, tmrQUEUE.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif 
        }

        void tmrREDS_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

#if DEBUG
            log.Debug("REDS timer Stop. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            
            tmrREDS.Stop();

            try
            {
                List<RFIDTag> lstTags = Utility.ReadTagList(Location.REDS);
#if DEBUG
                int n = 0;
                if (lstTags != null)
                    n = lstTags.Count;
                log.Debug("Read {0} tags for REDS timer. Thread {1}", n, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                Utility.UpdateTransaction(lstTags, Location.REDS);

            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in REDS timer. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif 
                // throw ex;
                WriteEventLog("REDS - " + DateTime.Now.ToString() + " " + ex.Message + "\n" + Convert.ToString(ex.StackTrace), EventLogEntryType.FailureAudit);
            }

            if(!serviceStopped)
                tmrREDS.Start();

#if DEBUG
            log.Debug("REDS timer started?- {0}. tmrEn {1} tmrIn {2} Thread {3}", !serviceStopped,tmrREDS.Enabled,tmrREDS.Interval, Thread.CurrentThread.ManagedThreadId.ToString());
#endif 
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            try
            {
 
                WriteEventLog("On Start --  " + DateTime.Now.ToString(), EventLogEntryType.Information);

                DBConnectorUtility.GetExpectedTimeValues();
                WriteEventLog("Expected Values read complete.", EventLogEntryType.Information);

                if (appRedundancyCheck)
                {
                    WriteEventLog("Check for activating local Server.  " + DateTime.Now.ToString(), EventLogEntryType.Information);
                    CheckForActivatingLocalService();
                }
                else
                {
                    if (GetAllConfigurations())
                    {
                        ConfigureReaderTimers();
                        StartReaderTimers();
                    }
                }

                serviceStopped = false;
#if DEBUG
                log.Debug("IQTMS service started");
#endif
               
            }
            catch (Exception ex)
            {
                WriteEventLog("On Start --  " + ex.Message, EventLogEntryType.FailureAudit);
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in onStart method in IQTMSService. Thread {0}",  Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }

        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            try
            {
#if DEBUG
                log.Debug("Entered onStop in IQTMSSerice");
#endif
                serviceStopped = true;

                WriteEventLog("On Stop --  " + DateTime.Now.ToString(), EventLogEntryType.Information);

                WriteEventLog("Stoping Modbus Server ", EventLogEntryType.Information);
                BLUtility.StopModbusServer();

                WriteEventLog("Closing VMS Connection.", EventLogEntryType.Information);
                BLUtility.DisconnectVMSs();

                WriteEventLog("Stoping Timers.", EventLogEntryType.Information);
                StopReaderTimers();

                Utility.DisposeReaders();
                WriteEventLog("Reader Disposed ", EventLogEntryType.Information);
                 
            }
            catch (Exception ex)
            {
                WriteEventLog("On Stop " + ex.Message, EventLogEntryType.FailureAudit);
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in OnStop method Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {
                case 0:
                    {
                        Utility.GetAllConfigurations();
                        ConfigureReaderTimers();
                        StartReaderTimers();
                        break;
                    }
                case 1:
                    {
                        break;
                    }

            }
        }

        protected void WriteEventLog(string msg, EventLogEntryType eventType)
        {
            try
            {
                IQTMSEventLogger.WriteEntry(msg, eventType);
            }
            catch (Exception ex)
            {
                try
                {
                    IQTMSEventLogger.Clear();
                }
                catch
                {
                }
            }
        }
    }
}
