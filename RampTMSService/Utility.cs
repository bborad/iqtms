using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ramp.BusinessLogic.Connector;
using Ramp.MiddlewareController.Common;
using Ramp.BusinessLogic.Common;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using ReaderModule = Ramp.RFIDHWConnector.Adapters;
using Ramp.MiddlewareController.Connector.BusinessLogicConnector;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Diagnostics;
using System.ServiceProcess;
using Ramp.DBConnector.Common;
using System.Data;
using System.Xml;

using NLog;



namespace Ramp
{


    public class Utility
    {

        // public static Dictionary<string, Truck> trucksInFacility = new Dictionary<string, Truck>();

        //public static Dictionary<int, Hopper> lstHoppers = new Dictionary<int, Hopper>();

        // static ArrayList arrTagsAtREDS = new ArrayList();
        
        private static Logger log = LogManager.GetLogger("Ramp.Utility.Utility");
        
        static bool IsLocalServiceActive;

        public static string MarkersHealthCheckPath;

        static EventLog IQTMSEventLogger;

        static IMarkers objMrkEntry2 = null;
        static IMarkers objMrkLane1 = null;
        static IMarkers objMrkLane2 = null;
        static IMarkers objMrkTruckWash = null;
        static IMarkers objMrkExitGate = null;

        static IMarkers objMrkControlRoom = null;

        static DataTable dtTagsToIssue, dtTagsToIssueCR, dtTagsToIssueITO;

        static bool EnableFilteredLogging;
        static string FilteredLoggingPath;

        public static void CreateDataTable()
        {
            dtTagsToIssue = new DataTable();
            DataColumn[] dcc = new DataColumn[] { new DataColumn("TagID",typeof(System.Int64)),
                                                   new DataColumn("TagRFID",typeof(System.String)),
                                                   new DataColumn("LastLocation",typeof(System.Int32)),
                                                   new DataColumn("IsDispatched",typeof(System.Boolean)),
                                                   new DataColumn("IsDeleted",typeof(System.Boolean))
            };

            dtTagsToIssue.Columns.AddRange(dcc);
            dtTagsToIssue.AcceptChanges();

            dtTagsToIssueCR = dtTagsToIssue.Clone();
            dtTagsToIssueITO = dtTagsToIssue.Clone();
        }


        public static void SetEventLogger()
        {
            IQTMSEventLogger = new EventLog();
            IQTMSEventLogger.Source = "IQTMSSource";
            IQTMSEventLogger.Log = "IQTMSLog";
        }

        public static List<RFIDTag> ReadTagList(Location loc)
        {
            List<RFIDTag> returnTagList = new List<RFIDTag>();
            try
            {

                List<RFIDTag> lstTags = null;

                List<IReader> objListReaders;

                if (BLUtility.RFIDAdapterReaders.ContainsKey(loc))
                {
                    objListReaders = BLUtility.RFIDAdapterReaders[loc];
                }
                else
                {
                    objListReaders = ReaderModule.Reader.GetReadersAtLocation(loc);
                    if (objListReaders != null && objListReaders.Count > 0)
                        BLUtility.RFIDAdapterReaders[loc] = objListReaders;
                }

                if (objListReaders != null && objListReaders.Count > 0)
                {

                    if (objListReaders.Count == 1)
                    {
                        IReader objReader = objListReaders[0];

                        lstTags = objReader.GetTags();

                        // objReader.Dispose();

                        if (lstTags != null && lstTags.Count > 0)
                        {
                            // returnTagList = (List<RFIDTag>)lstTags.Distinct();
                            returnTagList.AddRange(lstTags.Distinct());
                        }

                    }
                    else
                    {
                        foreach (IReader objReader in objListReaders)
                        {
                            lstTags = objReader.GetTags();

                            //objReader.Dispose();

                            if (lstTags != null && lstTags.Count > 0)
                            {
                                returnTagList.AddRange(lstTags.Distinct());
                            }
                        }
                    }
                }

                // Read tags from all Readers at location
                // UpdateTransaction(Location.REDS); 


            }
            catch (Exception ex)
            {
                // EventLog.WriteEntry("IQTMSService","Reading Tags - " + ex.Message, EventLogEntryType.FailureAudit);
                throw ex;
            }
            return returnTagList;
        }

        public static void UpdateTransaction(List<RFIDTag> lstTags, Location loc)
        {
            //31-10-2011:Bhavesh
           // logger.Trace("Called UpdateTransacton for " + loc.ToString() + " with " + lstTags.Count.ToString() + " tags.");

            if (lstTags.Count > 0)
            {
#if DEBUG
            log.Debug("In UpdateTransaction method with {0} tags for {1} location", lstTags.Count, loc.ToString());
#endif
                List<IMarkers> objListMarkers = null;

                if (BLUtility.Markers.ContainsKey(loc))
                {
                    objListMarkers = BLUtility.Markers[loc];
                }

                int count = 0;

                if (objListMarkers != null)
                {
                    foreach (RFIDTag objtag in lstTags)
                    {
                        count = 0;
                        try
                        {
                            count = (from obj in objListMarkers where obj.MarkerLoopID == objtag.NewMarkerID.ToString() select obj).Count();

                            if (count > 0)
                            {
                                Algorithms.TrafficTransactionOpenClose(objtag, loc);
                            }
#if DEBUG
                            log.Debug("Processed {0} tags for {1} location in UpdateTransaction method", count, loc.ToString());
#endif

                        }
                        catch(Exception ex)
                        {
#if DEBUG
                            log.ErrorException("Exception thrown while processing tags in UdateTransaction method",ex);
#endif
                        }
                    }
                }
                else
                {
                    foreach (RFIDTag objtag in lstTags)
                    {
                        try
                        {
                            Algorithms.TrafficTransactionOpenClose(objtag, loc);
                        }
                        catch (Exception ex)
                        {
#if DEBUG
                            log.ErrorException("Exception thrown while processing tags in UdateTransaction method", ex);
#endif
        
                        }
                    }
                }

            }


        }

        public static void UpdateTransactionQueueReads(List<RFIDTag> lstTags, Location loc)
        {


            if (lstTags.Count > 0)
            {
#if DEBUG
            log.Debug("In UpdateTransactionQueueReads method with {0} tags for {1} location", lstTags.Count, loc.ToString());
#endif
                List<IMarkers> objListMarkers = null;
                if (loc == Location.QUEUE)
                {
                    if (BLUtility.Markers.ContainsKey(loc))
                    {
                        objListMarkers = BLUtility.Markers[loc];
                    }
                }

                int count = 0;

                foreach (RFIDTag objtag in lstTags)
                {
                    count = 0;
                    try
                    {
                        if (objListMarkers != null)
                        {
                            //foreach (IMarkers objMrk in objListMarkers)
                            //{
                            //    if (objMrk.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
                            //    {
                            //        Algorithms.TrafficTransactionOpenClose(objtag, loc);
                            //        break;
                            //    }
                            //}

                            count = (from obj in objListMarkers where obj.MarkerLoopID == objtag.NewMarkerID.ToString() select obj).Count();

                            if (count > 0)
                            {
                                Algorithms.TrafficTransactionOpenClose(objtag, loc);
                            }
#if DEBUG
                            log.Debug("Processed {0} tags for {1} location in updateTransactionQueueReads", count, loc.ToString());
#endif
                        }
                        else
                        {
                            Algorithms.TrafficTransactionOpenClose(objtag, loc);
                        }
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        log.ErrorException("Exception thrown in UpdateTransactionQueueReads method",ex);
#endif
                    }
                }
            }
        }

        public static void UpdateTransactionEntryGate(List<RFIDTag> lstTags, Location loc)
        {

            if (lstTags.Count > 0)
            {
#if DEBUG
            log.Debug("In UpdateTransactionEntryGate method with {0} tags for {1} location", lstTags.Count, loc.ToString());
#endif           
                if (dtTagsToIssueCR == null)
                {
                    CreateDataTable();
                }

                dtTagsToIssueCR.Rows.Clear();

                dtTagsToIssueCR.AcceptChanges();

                List<IMarkers> objListMarkers = null;

                if (objMrkEntry2 == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.ENTRY2))
                    {
                        objListMarkers = BLUtility.Markers[Location.ENTRY2];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkEntry2 = objListMarkers[0];
                    }
                }

                objListMarkers = null; // line of code added on 10/11/2010
                if (objMrkLane1 == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.EntryLane1))
                    {
                        objListMarkers = BLUtility.Markers[Location.EntryLane1];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkLane1 = objListMarkers[0];
                    }
                }

                objListMarkers = null; // line of code added on 10/11/2010
                if (objMrkLane2 == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.EntryLane2))
                    {
                        objListMarkers = BLUtility.Markers[Location.EntryLane2];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkLane2 = objListMarkers[0];
                    }
                }

                objListMarkers = null; // line of code added on 10/11/2010
                if (objMrkControlRoom == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.ControlRoom))
                    {
                        objListMarkers = BLUtility.Markers[Location.ControlRoom];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkControlRoom = objListMarkers[0];
                    }
                }

                DataRow dr;
                foreach (RFIDTag objtag in lstTags)
                {
                    try
                    {
                        if (objMrkControlRoom != null && objMrkControlRoom.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
                        {
                            dr = dtTagsToIssueCR.NewRow();

                            dr["TagRFID"] = objtag.TagID;
                            dr["TagID"] = 0;
                            dr["LastLocation"] = (int)Location.ControlRoom;
                            dr["IsDispatched"] = false;
                            dr["IsDeleted"] = false;

                            dtTagsToIssueCR.Rows.Add(dr);

                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ControlRoom);
                            continue;
                        }
                        else if (objMrkEntry2 != null && objMrkEntry2.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
                        {
                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ENTRY2);
                            continue;
                        }
                        else if ((objMrkLane1 != null && objMrkLane1.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()) || (objMrkLane2 != null && objMrkLane2.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()))
                        {
                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ENTRY1);
                            continue;
                        }

                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        log.Debug("Exception thrown in UpdateTransactionEntryGate method", ex);
#endif
                    }
                }

                if (dtTagsToIssueCR.Rows.Count > 0)
                {
                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssueCR);
                }

            }
        }

        public static void UpdateTransactionTruckWash(List<RFIDTag> lstTags, Location loc)
        {

            if (lstTags.Count > 0)
            {
#if DEBUG
            log.Debug("In UpdateTransactionTW method with {0} tags for {1} location", lstTags.Count, loc.ToString());
#endif             
                if (dtTagsToIssue == null)
                {
                    CreateDataTable();
                }

                dtTagsToIssue.Rows.Clear();

                dtTagsToIssue.AcceptChanges();

                List<IMarkers> objListMarkers = null;

                if (objMrkTruckWash == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.TRUCKWASH))
                    {
                        objListMarkers = BLUtility.Markers[Location.TRUCKWASH];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkTruckWash = objListMarkers[0];
                    }
                }

                objListMarkers = null; // line of code added on 10/11/2010
                if (objMrkExitGate == null)
                {
                    if (BLUtility.Markers.ContainsKey(Location.EXITGATE))
                    {
                        objListMarkers = BLUtility.Markers[Location.EXITGATE];
                    }

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkExitGate = objListMarkers[0];
                    }
                }

                DataRow dr;
                foreach (RFIDTag objtag in lstTags)
                {
                    try
                    {
                        DateTime dtNewMarkerPositionTime = objtag.MarkerNewPositionTime;

                        //  added on 20 Dec 2010
                        if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) < DateTime.Now)
                        {

                            continue;
                        }

                        if (objMrkTruckWash != null && objMrkTruckWash.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
                        {
                            dr = dtTagsToIssue.NewRow();

                            dr["TagRFID"] = objtag.TagID;
                            dr["TagID"] = 0;
                            dr["LastLocation"] = (int)loc;
                            dr["IsDispatched"] = false;
                            dr["IsDeleted"] = false;

                            dtTagsToIssue.Rows.Add(dr);

                            if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) >= DateTime.Now)
                            {
                                Algorithms.TrafficTransactionOpenClose(objtag, Location.TRUCKWASH);
                            }
                            continue;

                        }
                        else if ((objMrkExitGate != null && objMrkExitGate.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()))
                        {
                            dr = dtTagsToIssue.NewRow();

                            dr["TagRFID"] = objtag.TagID;
                            dr["TagID"] = 0;
                            dr["LastLocation"] = (int)loc;
                            dr["IsDispatched"] = false;
                            dr["IsDeleted"] = false;

                            dtTagsToIssue.Rows.Add(dr);

                            if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) >= DateTime.Now)
                            {
                                Algorithms.TrafficTransactionOpenClose(objtag, Location.EXITGATE);
                            }
                            continue;
                        }

                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        log.Debug("Exception thrown in UpdateTransactionTW mthod", ex);
#endif
                    }
                }
                // dtTagsToIssue.AcceptChanges();

                if (dtTagsToIssue.Rows.Count > 0)
                {
                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssue);
                }

            }


        }

        public static void UpdateTransactionIssueTagOffice(List<RFIDTag> lstTags, Location loc)
        {

            if (lstTags.Count > 0)
            {

#if DEBUG
            log.Debug("In UpdateTransactionIssueTagOffice method with {0} tags for {1} location", lstTags.Count, loc.ToString());
#endif
                if (dtTagsToIssueITO == null)
                {
                    CreateDataTable();
                }

                dtTagsToIssueITO.Rows.Clear();

                dtTagsToIssueITO.AcceptChanges();

                List<IMarkers> objListMarkers = null;

                IMarkers objMrkIssueTagOffice = null;

                if (BLUtility.Markers.ContainsKey(Location.IssueTagOffice))
                {
                    objListMarkers = BLUtility.Markers[Location.IssueTagOffice];

                    if (objListMarkers != null && objListMarkers.Count > 0)
                    {
                        objMrkIssueTagOffice = objListMarkers[0];
                    }
                }
                
                DataRow dr;
                foreach (RFIDTag objtag in lstTags)
                {
                    try
                    {
                        if (objMrkIssueTagOffice != null && objMrkIssueTagOffice.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
                        {
                            dr = dtTagsToIssueITO.NewRow();

                            dr["TagRFID"] = objtag.TagID;
                            dr["TagID"] = 0;
                            dr["LastLocation"] = (int)Location.IssueTagOffice;
                            dr["IsDispatched"] = false;
                            dr["IsDeleted"] = false;

                            dtTagsToIssueITO.Rows.Add(dr);

                            Algorithms.TrafficTransactionOpenClose(objtag, Location.IssueTagOffice);
                            continue;
                        } 

                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        log.Debug("Exception thrown in UpdateTransactionIssueTagOFfice method", ex);
#endif
                    }
                }

                if (dtTagsToIssueITO.Rows.Count > 0)
                {
                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssueITO);
                }

            }
        }

        public static void ConfigureReaders()
        {
            try
            {
                BLUtility.ReadNStoreAllReaders();

            }
            catch (Exception ex)
            {
             // logger.ErrorException("Throw exception while configuring readers in ConfigureReaders function.", ex);
            }
        }

        public static void DisposeReaders()
        {
            try
            {
                BLUtility.DisposeReaders();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetAllHoppersInFacility()
        {
            try
            {
                BLUtility.GetAllHoppersInFacility();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetAllMarkers()
        {
            try
            {
                BLUtility.GetAllMarkers();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void GetAllVMSDevices()
        {
            try
            {
                BLUtility.GetAllVMSDevices();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void StartAllVMS()
        {
            BLUtility.StartAllVMS();
        }

        public static void GetConfigurationValues()
        {
            try
            {
                BLUtility.GetConfigurationValues();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void StartModbusServer()
        {
            try
            {
                BLUtility.StartModbusServer();
            }
            catch (Exception ex)
            {
            }

        }


        public static bool GetAllConfigurations()
        {
            bool result = true;
            try
            {
                IQTMSEventLogger.WriteEntry("On Serives Initialization -- " + DateTime.Now.ToString(), EventLogEntryType.Information);

                IQTMSEventLogger.WriteEntry("Markers Configured.", EventLogEntryType.Information);
                DBConnectorUtility.GetExpectedTimeValues();

                Utility.ConfigureReaders();
                IQTMSEventLogger.WriteEntry("Readers Configured.", EventLogEntryType.Information);
                Utility.GetAllHoppersInFacility();
                IQTMSEventLogger.WriteEntry("Hoppers Configured.", EventLogEntryType.Information);
                Utility.GetAllMarkers();

                IQTMSEventLogger.WriteEntry("Expected Values read complete.", EventLogEntryType.Information);
                Utility.GetAllVMSDevices();
                IQTMSEventLogger.WriteEntry("VMS Device Configuration.", EventLogEntryType.Information);
                Utility.GetConfigurationValues();
                IQTMSEventLogger.WriteEntry("Configuration Values.", EventLogEntryType.Information);

                Utility.StartModbusServer();
                if (BLUtility.IsModbusConnected)
                    IQTMSEventLogger.WriteEntry("Modbus Started.", EventLogEntryType.Information);
                else
                    IQTMSEventLogger.WriteEntry("Modbus not Started.", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                IQTMSEventLogger.WriteEntry("On Serives Initialization --  " + ex.Message, EventLogEntryType.FailureAudit);
                result = false;
            }
            return result;
        }

        public static void SetMarkersLoopID_Routine()
        {
            try
            {
                // bool performOperation = false;
                // Get the Markers info from DB;

                DataTable dtMarkers = new DataTable("dtMarkers");

                int markerHealthCheckFlag, LogFileClearingInterval;

                dtMarkers = DBConnector.Adapter.Markers.GetMarkersInfo(out markerHealthCheckFlag, out LogFileClearingInterval);

                if (markerHealthCheckFlag == 1)
                {
                    // Request the Web service for the operation  

                    DataTable dtResult = null;

                    if (dtMarkers.Rows.Count > 0)
                    {
                        dtResult = ReaderModule.Reader.SetMarkersLoopID(dtMarkers);
                    }

                    // Write the log file from the output
                    if (dtResult != null)
                    {
                        if (MarkersHealthCheckPath != null && MarkersHealthCheckPath.Length > 0)
                        {
                            DataView dv;
                            dv = new DataView(dtResult);
                            dv.RowFilter = "Status = 'LoopID reset'";

                            if (dv.Count > 0)
                            {
                                string fileName = MarkersHealthCheckPath + "MarkerHealthCheck _" + DateTime.Now.Ticks.ToString() + ".xml";
                                // dtResult.WriteXml(fileName);                               
                                dv.ToTable().WriteXml(fileName);
                            }

                            if (LogFileClearingInterval > -1)
                            {

                                System.IO.DirectoryInfo dinfo = new System.IO.DirectoryInfo(MarkersHealthCheckPath);
                                // IEnumerable<System.IO.FileInfo> arr = dinfo.GetFiles();

                                System.IO.FileInfo[] arr = dinfo.GetFiles();

                                var filesToDel = from fileInfo in arr where fileInfo.CreationTime.Date <= DateTime.Today.AddDays(-LogFileClearingInterval).Date select fileInfo;

                                if (filesToDel.Count() > 0)
                                {
                                    foreach (System.IO.FileInfo file in filesToDel)
                                    {
                                        try
                                        {
                                            file.Delete();
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }

                                dinfo = null;
                                filesToDel = null;
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void WriteFilteredLogs(RFIDTag objtag, string location, string ipAddress, DateTime dtLogTime)
        {
            try
            {

                // object value = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogging"];

                if (EnableFilteredLogging)
                {
                    try
                    {

                        StringBuilder strtags = new StringBuilder();
                        strtags.Append("--------------------------------------------------- " + dtLogTime.ToString() + " ---------------------------------------------" + Environment.NewLine);
                        strtags.Append("TagID, NewMarker LoopID, NewMarker PositionTime, OldMarker LoopID, OldMarker PositionTime,Location,Reader IPAddress" + Environment.NewLine);

                        strtags.Append(objtag.TagID.ToString() + ", " + objtag.NewMarkerID.ToString() + ", " + objtag.MarkerNewPositionTime.ToString() + ", " + objtag.OldMarkerID.ToString() + ", " + objtag.MarkerOldPositionTime.ToString() + ", " + location + ", " + ipAddress + Environment.NewLine);

                        //object tagReadDirPath = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogDir"];

                        if (FilteredLoggingPath != null)
                        {
                            if (!System.IO.Directory.Exists(FilteredLoggingPath.ToString()))
                            {
                                System.IO.Directory.CreateDirectory(FilteredLoggingPath.ToString());
                            }

                            string tagID = objtag.TagID.ToString();
                            string fileName = FilteredLoggingPath.ToString() + tagID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".tag";

                            System.IO.File.AppendAllText(fileName, strtags.ToString());
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public bool ReadDefaultConfig()
        {
            bool ret = false;
            string fpath = System.Reflection.Assembly.GetExecutingAssembly().Location + @"\RampTMSService.exe.Config";
            bool foundAll = false;

            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(fpath);

                XmlNode node1 = xmldoc.SelectSingleNode(@"configuration/appSettings");

                XmlNodeList appnodes = node1.ChildNodes;

                foreach (XmlNode nn in appnodes)
                {
                    if (nn.NodeType == XmlNodeType.Element && nn.Attributes["key"].Value.ToLower() == "DestinationFolder".ToLower())
                    {
                        FilteredLoggingPath = nn.Attributes["value"].Value;
                        ret = true;
                        if (foundAll == false)
                        {
                            foundAll = true;
                        }
                        else
                            break;
                    }

                    if (nn.NodeType == XmlNodeType.Element && nn.Attributes["key"].Value.ToLower() == "DataLogBackupFolder".ToLower())
                    {
                        string enableFilteredLogPath = nn.Attributes["value"].Value;
                        ret = true;
                        if (foundAll == false)
                        {
                            foundAll = true;
                        }
                        else
                            break;
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return ret;
        }

    }


}
