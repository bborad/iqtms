using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Common;
using Ramp.RFIDHardWare.Common;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;

namespace Ramp.RFIDHWConnector.Common
{
    public class RFIDConnectorUtility
    {
        //public static Dictionary<Location, List<IReader>> lstAllRFIDReaders
        //{
        //    get
        //    {
        //        return RFIDUtility.lstAllRFIDReaders;
        //    }
        //    set
        //    {
        //        RFIDUtility.lstAllRFIDReaders = value;
        //    }
        //}

        public static bool IsTruckAtLocation(Location loc, string TagRFID)
        {
            bool flagResult = false;
            try
            {
                // Call appropriate method from RFIDHardware Lib
                flagResult = RFIDUtility.IsTruckAtLocation(loc, TagRFID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }


        public static Int32 GetTotalTagsAtLocation(Location loc)
        {
            try
            {
                // Call appropriate method from RFIDHardware Lib
                Int32 totalTags = RFIDUtility.GetTotalTagsAtLocation(loc);
                return totalTags;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ReadNStoreAllReaders()
        {
            try
            {
                RFIDUtility.ReadNStoreAllReaders();  
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
