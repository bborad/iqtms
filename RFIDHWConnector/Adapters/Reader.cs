using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Common;
using ReaderModule = Ramp.RFIDHardWare.Connector;
using System.Threading;
using System.Collections;
using System.Data;

namespace Ramp.RFIDHWConnector.Adapters
{

    public class Reader : IReader
    {
        #region "private variables"
        public string ReaderSerialNo { get; set; }
        public string ReaderName { get; set; }
        public string IPAddress { get; set; }
        public int Location { get; set; }
        public int PortNo { get; set; }
        public int TxPower { get; set; }

        ReaderModule.Reader objReader;

        #endregion

        public Reader(string strIPAddress, int iPort)
        {
            IPAddress = strIPAddress;
            PortNo = iPort;
        }

        public Reader()
        {
        }

        public static List<IReader> GetReadersAtLocation(Location loc)
        {
            List<IReader> lst_Readers = null;
            Reader obj;
            try
            {
                lst_Readers = ReaderModule.Reader.GetReadersAtLocation(loc);
            }
            catch (Exception)
            {
                throw;
            }
            return lst_Readers;

        }

        public bool SearchTag(string TagRFID)
        {
            bool flagResult = false;
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }
                flagResult = objReader.SearchTag(TagRFID);
            }
            catch (Exception ex)
            {
            }
            return flagResult;
        }

        public List<RFIDTag> ScanForTags(int count)
        {
            List<RFIDTag> lst = new List<RFIDTag>();
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                lst = objReader.ScanForTags(count);
            }
            catch (Exception ex)
            {
            }
            return lst;
        }

        public List<RFIDTag> ScanForTags(int count, bool testing)
        {
            List<RFIDTag> lst = new List<RFIDTag>();
            //ReaderModule.Reader objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress,PortNo);
            //lst = objReader.ScanForTags();

            RFIDTag obj = new RFIDTag();
            obj.TagID = "302203125";

            lst.Add(obj);

            obj = new RFIDTag();
            obj.TagID = "302203126";

            lst.Add(obj);

            obj = new RFIDTag();
            obj.TagID = "302203127";

            lst.Add(obj);

            obj = new RFIDTag();
            obj.TagID = "302203128";

            lst.Add(obj);

            return lst;
        }

        public bool IsReaderConnected()
        {
            bool flagResult = false;

            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                flagResult = objReader.IsReaderConnected();

            }
            catch (Exception ex)
            {
                flagResult = false;
            }
            return flagResult;
        }

        public List<RFIDTag> GetTags()
        {
            List<RFIDTag> lst = new List<RFIDTag>();
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                lst = objReader.GetTags();
            }
            catch (Exception ex)
            {
               // throw ex;
            }
            return lst;
        }

        public List<RFIDTag> ReadTags()
        {
            List<RFIDTag> lst = new List<RFIDTag>();
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                lst = objReader.ReadTags();
            }
            catch (Exception ex)
            {
                // throw ex;
            }
            return lst;
        }

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts)
        {
            bool resultFlag = false;
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                resultFlag = objReader.BlinkLED(tagToBlink, ledColor, ts);

            }
            catch (Exception ex)
            {
               // throw ex;
            }
            return resultFlag;
        }

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts,int totalBlinks)
        {
            bool resultFlag = false;
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                resultFlag = objReader.BlinkLED(tagToBlink, ledColor, ts, totalBlinks);

            }
            catch (Exception ex)
            {
                // throw ex;
            }
            return resultFlag;
        }

        public void BlinkLEDAsync(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int totalBlinks)        
        {
            
            try
            {
                //WaitCallback blinkled = new WaitCallback(ThreadProc);

                //ArrayList objState = new ArrayList() { tagToBlink, ledColor, ts };

                //ThreadPool.QueueUserWorkItem(blinkled, objState);

                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                objReader.BlinkLEDAsync(tagToBlink, ledColor, ts,totalBlinks);
             
            }
            catch (Exception ex)
            {
                // throw ex;
            }
            
        }

        private void ThreadProc(object State)
        {
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                ArrayList objState = (ArrayList)State;

                objReader.BlinkLED(objState[0].ToString(), (TagLedColor)objState[1], (TimeSpan)objState[2]);
            }
            catch
            {
            }
        }

        public void ClearTagList()
        {
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                objReader.ClearTagList();

            }
            catch (Exception ex)
            {
               // throw ex;
            }
        }

        public string GetSerialNo()
        {
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                return objReader.GetSerialNo();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static DataTable GetMarkersStatus()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = ReaderModule.Reader.GetMarkersStatus();
            }
            catch
            {
            }

            return dt;

        }

        public static DataTable SetMarkersLoopID(DataTable dtMarkers)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = ReaderModule.Reader.SetMarkersLoopID(dtMarkers);
            }
            catch
            {
            }

            return dt;

        }

        public static void ConfigureReaders(System.Data.DataTable dtReaders)
        {
            try
            {
                ReaderModule.Reader.ConfigureReaders(dtReaders);
            }
            catch
            {
            }
        }

        public string GetReaderStatus()
        {
            try
            {
                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.Connector.Reader(IPAddress, PortNo);
                }

                return objReader.GetReaderStatus();
            }
            catch (Exception ex)
            {
                return "Fault";
            }
        }
        public void Dispose()
        {
            try
            {
               // objReader.Dispose();
                objReader = null;
            }
            catch (Exception ex)
            {
                objReader = null;
            }
        }

        ~Reader()
        {
            try
            {
                objReader = null;
            }
            catch (Exception)
            {
            }
        }
    }
}
