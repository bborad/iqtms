//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections;
//using Ramp.BusinessLogic.Connector;
//using Ramp.MiddlewareController.Common;
//using Ramp.BusinessLogic.Common;
//using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
//using ReaderModule = Ramp.RFIDHWConnector.Adapters;
//using Ramp.MiddlewareController.Connector.BusinessLogicConnector;
//using Ramp.MiddlewareController.Connector.DBConnector;
//using Ramp.DBConnector.Common;
//using System.Data;

////Date: 31-10-2011
////By:   Bhavesh
////Note: Adde for logging
//using NLog;

//namespace Ramp
//{
//    public class Utility
//    {
//        // public static Dictionary<string, Truck> trucksInFacility = new Dictionary<string, Truck>();

//        //public static Dictionary<int, Hopper> lstHoppers = new Dictionary<int, Hopper>();

//        // static ArrayList arrTagsAtREDS = new ArrayList();

//        public static string MarkersHealthCheckPath;

//        static IMarkers objMrkEntry2 = null;
//        static IMarkers objMrkLane1 = null;
//        static IMarkers objMrkLane2 = null;
//        static IMarkers objMrkTruckWash = null;
//        static IMarkers objMrkExitGate = null;

//        static IMarkers objMrkControlRoom = null;

//        static DataTable dtTagsToIssue, dtTagsToIssueCR, dtTagsToIssueITO;

//        public static void CreateDataTable()
//        {
//            dtTagsToIssue = new DataTable();
//            DataColumn[] dcc = new DataColumn[] { new DataColumn("TagID",typeof(System.Int64)),
//                                                   new DataColumn("TagRFID",typeof(System.String)),
//                                                   new DataColumn("LastLocation",typeof(System.Int32)),
//                                                   new DataColumn("IsDispatched",typeof(System.Boolean)),
//                                                   new DataColumn("IsDeleted",typeof(System.Boolean))
//            };

//            dtTagsToIssue.Columns.AddRange(dcc);
//            dtTagsToIssue.AcceptChanges();

//            dtTagsToIssueCR = dtTagsToIssue.Clone();
//            dtTagsToIssueITO = dtTagsToIssue.Clone();
//        }

//        public static List<RFIDTag> ReadTagList(Location loc)
//        {
//            List<RFIDTag> returnTagList = new List<RFIDTag>();
//            try
//            {

//                List<RFIDTag> lstTags = null;

//                List<IReader> objListReaders;

//                if (BLUtility.RFIDAdapterReaders.ContainsKey(loc))
//                {
//                    objListReaders = BLUtility.RFIDAdapterReaders[loc];
//                }
//                else
//                {
//                    objListReaders = ReaderModule.Reader.GetReadersAtLocation(loc);
//                    if (objListReaders != null && objListReaders.Count > 0)
//                        BLUtility.RFIDAdapterReaders[loc] = objListReaders;
//                }

//                if (objListReaders != null && objListReaders.Count > 0)
//                {

//                    if (objListReaders.Count == 1)
//                    {
//                        IReader objReader = objListReaders[0];

//                        lstTags = objReader.GetTags();

//                        // objReader.Dispose();

//                        if (lstTags != null)
//                        {
//                            // returnTagList = (List<RFIDTag>)lstTags.Distinct();
//                            returnTagList.AddRange(lstTags.Distinct());
//                        }

//                    }
//                    else
//                    {
//                        foreach (IReader objReader in objListReaders)
//                        {
//                            lstTags = objReader.GetTags();

//                            objReader.Dispose();

//                            if (lstTags != null)
//                            {
//                                returnTagList.AddRange(lstTags.Distinct());
//                            }
//                        }
//                    }
//                }

//                // Read tags from all Readers at location
//                // UpdateTransaction(Location.REDS); 


//            }
//            catch (Exception ex)
//            {
//                // EventLog.WriteEntry("IQTMSService","Reading Tags - " + ex.Message, EventLogEntryType.FailureAudit);
//                throw ex;
//            }
//            return returnTagList;
//        }

//        public static void UpdateTransaction(List<RFIDTag> lstTags, Location loc)
//        {

//            if (lstTags.Count > 0)
//            {
//                List<IMarkers> objListMarkers = null;

//                if (BLUtility.Markers.ContainsKey(loc))
//                {
//                    objListMarkers = BLUtility.Markers[loc];
//                }

//                int count = 0;

//                if (objListMarkers != null)
//                {
//                    foreach (RFIDTag objtag in lstTags)
//                    {
//                        count = 0;
//                        try
//                        {
//                            count = (from obj in objListMarkers where obj.MarkerLoopID == objtag.NewMarkerID.ToString() select obj).Count();

//                            if (count > 0)
//                            {
//                                Algorithms.TrafficTransactionOpenClose(objtag, loc);
//                            }

//                        }
//                        catch
//                        {
//                        }
//                    }
//                }
//                else
//                {
//                    foreach (RFIDTag objtag in lstTags)
//                    {
//                        try
//                        {
//                            Algorithms.TrafficTransactionOpenClose(objtag, loc);
//                        }
//                        catch
//                        {
//                        }
//                    }
//                }

//            }


//        }

//        public static void UpdateTransactionQueueReads(List<RFIDTag> lstTags, Location loc)
//        {

//            if (lstTags.Count > 0)
//            {

//                List<IMarkers> objListMarkers = null;
//                if (loc == Location.QUEUE)
//                {
//                    if (BLUtility.Markers.ContainsKey(loc))
//                    {
//                        objListMarkers = BLUtility.Markers[loc];
//                    }
//                }

//                int count = 0;

//                foreach (RFIDTag objtag in lstTags)
//                {
//                    count = 0;
//                    try
//                    {
//                        if (objListMarkers != null)
//                        {
//                            //foreach (IMarkers objMrk in objListMarkers)
//                            //{
//                            //    if (objMrk.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
//                            //    {
//                            //        Algorithms.TrafficTransactionOpenClose(objtag, loc);
//                            //        break;
//                            //    }
//                            //}

//                            count = (from obj in objListMarkers where obj.MarkerLoopID == objtag.NewMarkerID.ToString() select obj).Count();

//                            if (count > 0)
//                            {
//                                Algorithms.TrafficTransactionOpenClose(objtag, loc);
//                            }

//                        }
//                        else
//                        {
//                            Algorithms.TrafficTransactionOpenClose(objtag, loc);
//                        }
//                    }
//                    catch (Exception)
//                    {
//                        // throw ex;
//                    }
//                }
//            }
//        }

//        public static void UpdateTransactionEntryGate(List<RFIDTag> lstTags, Location loc)
//        {

//            if (lstTags.Count > 0)
//            {
//                if (dtTagsToIssueCR == null)
//                {
//                    CreateDataTable();
//                }

//                dtTagsToIssueCR.Rows.Clear();

//                dtTagsToIssueCR.AcceptChanges();

//                List<IMarkers> objListMarkers = null;

//                if (objMrkEntry2 == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.ENTRY2))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.ENTRY2];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkEntry2 = objListMarkers[0];
//                    }
//                }

//                objListMarkers = null;
//                if (objMrkLane1 == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.EntryLane1))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.EntryLane1];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkLane1 = objListMarkers[0];
//                    }
//                }

//                objListMarkers = null;
//                if (objMrkLane2 == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.EntryLane2))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.EntryLane2];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkLane2 = objListMarkers[0];
//                    }
//                }

//                objListMarkers = null;
//                if (objMrkControlRoom == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.ControlRoom))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.ControlRoom];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkControlRoom = objListMarkers[0];
//                    }
//                }

//                DataRow dr;
//                foreach (RFIDTag objtag in lstTags)
//                {
//                    try
//                    {
//                        if (objMrkControlRoom != null && objMrkControlRoom.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
//                        {
//                            dr = dtTagsToIssueCR.NewRow();

//                            dr["TagRFID"] = objtag.TagID;
//                            dr["TagID"] = 0;
//                            dr["LastLocation"] = (int)Location.ControlRoom;
//                            dr["IsDispatched"] = false;
//                            dr["IsDeleted"] = false;

//                            dtTagsToIssueCR.Rows.Add(dr);

//                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ControlRoom);
//                            continue;
//                        }
//                        else if (objMrkEntry2 != null && objMrkEntry2.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
//                        {
//                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ENTRY2);
//                            continue;
//                        }
//                        else if ((objMrkLane1 != null && objMrkLane1.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()) || (objMrkLane2 != null && objMrkLane2.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()))
//                        {
//                            Algorithms.TrafficTransactionOpenClose(objtag, Location.ENTRY1);
//                            continue;
//                        }

//                    }
//                    catch (Exception)
//                    {
//                        // throw ex;
//                    }
//                }

//                if (dtTagsToIssueCR.Rows.Count > 0)
//                {
//                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssueCR);
//                }

//            }
//        }

//        public static void UpdateTransactionTruckWash(List<RFIDTag> lstTags, Location loc)
//        {

//            if (lstTags.Count > 0)
//            {
//                if (dtTagsToIssue == null)
//                {
//                    CreateDataTable();
//                }

//                dtTagsToIssue.Rows.Clear();

//                dtTagsToIssue.AcceptChanges();

//                List<IMarkers> objListMarkers = null;

//                if (objMrkTruckWash == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.TRUCKWASH))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.TRUCKWASH];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkTruckWash = objListMarkers[0];
//                    }
//                }

//                objListMarkers = null; // line of code added on 10/11/2010
//                if (objMrkExitGate == null)
//                {
//                    if (BLUtility.Markers.ContainsKey(Location.EXITGATE))
//                    {
//                        objListMarkers = BLUtility.Markers[Location.EXITGATE];
//                    }

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkExitGate = objListMarkers[0];
//                    }
//                }

//                DataRow dr;
//                foreach (RFIDTag objtag in lstTags)
//                {
//                    try
//                    {
//                        DateTime dtNewMarkerPositionTime = objtag.MarkerNewPositionTime;

//                        //  added on 20 Dec 2010
//                        if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) < DateTime.Now)
//                        {
//                            continue;
//                        }

//                        if (objMrkTruckWash != null && objMrkTruckWash.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
//                        {
//                            dr = dtTagsToIssue.NewRow();

//                            dr["TagRFID"] = objtag.TagID;
//                            dr["TagID"] = 0;
//                            dr["LastLocation"] = (int)loc;
//                            dr["IsDispatched"] = false;
//                            dr["IsDeleted"] = false;

//                            dtTagsToIssue.Rows.Add(dr);

//                            if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) >= DateTime.Now)
//                            {
//                                Algorithms.TrafficTransactionOpenClose(objtag, Location.TRUCKWASH);
//                            }

//                            continue;

//                        }
//                        else if ((objMrkExitGate != null && objMrkExitGate.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower()))
//                        {
//                            dr = dtTagsToIssue.NewRow();

//                            dr["TagRFID"] = objtag.TagID;
//                            dr["TagID"] = 0;
//                            dr["LastLocation"] = (int)loc;
//                            dr["IsDispatched"] = false;
//                            dr["IsDeleted"] = false;

//                            dtTagsToIssue.Rows.Add(dr);

//                            if (dtNewMarkerPositionTime.AddSeconds(BLUtility.TagNewMarkerInterval) >= DateTime.Now)
//                            {
//                                Algorithms.TrafficTransactionOpenClose(objtag, Location.EXITGATE);
//                            }
//                            continue;
//                        }

//                    }
//                    catch (Exception)
//                    {
//                        // throw ex;
//                    }
//                }
//                // dtTagsToIssue.AcceptChanges();

//                if (dtTagsToIssue.Rows.Count > 0)
//                {
//                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssue);
//                }

//            }


//        }

//        public static void UpdateTransactionIssueTagOffice(List<RFIDTag> lstTags, Location loc)
//        {

//            if (lstTags.Count > 0)
//            {
//                if (dtTagsToIssueITO == null)
//                {
//                    CreateDataTable();
//                }

//                dtTagsToIssueITO.Rows.Clear();

//                dtTagsToIssueITO.AcceptChanges();

//                List<IMarkers> objListMarkers = null;

//                IMarkers objMrkIssueTagOffice = null;

//                if (BLUtility.Markers.ContainsKey(Location.IssueTagOffice))
//                {
//                    objListMarkers = BLUtility.Markers[Location.IssueTagOffice];

//                    if (objListMarkers != null && objListMarkers.Count > 0)
//                    {
//                        objMrkIssueTagOffice = objListMarkers[0];
//                    }
//                }

//                DataRow dr;
//                foreach (RFIDTag objtag in lstTags)
//                {
//                    try
//                    {
//                        if (objMrkIssueTagOffice != null && objMrkIssueTagOffice.MarkerLoopID.Trim().ToLower() == objtag.NewMarkerID.ToString().ToLower())
//                        {
//                            dr = dtTagsToIssueITO.NewRow();

//                            dr["TagRFID"] = objtag.TagID;
//                            dr["TagID"] = 0;
//                            dr["LastLocation"] = (int)Location.IssueTagOffice;
//                            dr["IsDispatched"] = false;
//                            dr["IsDeleted"] = false;

//                            dtTagsToIssueITO.Rows.Add(dr);

//                            Algorithms.TrafficTransactionOpenClose(objtag, Location.IssueTagOffice);
//                            continue;
//                        }

//                    }
//                    catch (Exception)
//                    {
//                        // throw ex;
//                    }
//                }

//                if (dtTagsToIssueITO.Rows.Count > 0)
//                {
//                    DBConnectorUtility.AddUpdateTagsToIssue(dtTagsToIssueITO);
//                }

//            }
//        }


//        public static void ConfigureReaders()
//        {
//            try
//            {
//                BLUtility.ReadNStoreAllReaders();

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public static void DisposeReaders()
//        {
//            try
//            {
//                BLUtility.DisposeReaders();
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public static void GetAllHoppersInFacility()
//        {
//            try
//            {
//                BLUtility.GetAllHoppersInFacility();
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public static void GetAllMarkers()
//        {
//            try
//            {
//                objMrkEntry2 = null; objMrkExitGate = null; objMrkLane1 = null; objMrkLane2 = null; objMrkTruckWash = null;
//                BLUtility.GetAllMarkers();
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }

//        }

//        public static void SetMarkersLoopID_Routine()
//        {
//            try
//            {
//                // bool performOperation = false;
//                // Get the Markers info from DB;

//                DataTable dtMarkers = new DataTable("dtMarkers");

//                int markerHealthCheckFlag, LogFileClearingInterval;

//                dtMarkers = DBConnector.Adapter.Markers.GetMarkersInfo(out markerHealthCheckFlag, out LogFileClearingInterval);

//                if (markerHealthCheckFlag == 1)
//                {
//                    // Request the Web service for the operation  

//                    DataTable dtResult = null;

//                    if (dtMarkers.Rows.Count > 0)
//                    {
//                        dtResult = ReaderModule.Reader.SetMarkersLoopID(dtMarkers);
//                    }

//                    // Write the log file from the output
//                    if (dtResult != null)
//                    {
//                        if (MarkersHealthCheckPath != null && MarkersHealthCheckPath.Length > 0)
//                        {


//                            System.IO.DirectoryInfo dinfo = new System.IO.DirectoryInfo(MarkersHealthCheckPath);
//                            // IEnumerable<System.IO.FileInfo> arr = dinfo.GetFiles();

//                            System.IO.FileInfo[] arr = dinfo.GetFiles();

//                            var filesToDel = from fileInfo in arr where fileInfo.CreationTime.Date <= DateTime.Today.AddDays(-3).Date select fileInfo;

//                            if (filesToDel.Count() > 0)
//                            {
//                                foreach (System.IO.FileInfo file in filesToDel)
//                                {
//                                    try
//                                    {
//                                        file.Delete();
//                                    }
//                                    catch
//                                    {
//                                    }
//                                }
//                            }

//                            dinfo = null;
//                            filesToDel = null;

//                             DataView dv;
//                            dv = new DataView(dtResult);
//                            dv.RowFilter = "Status = 'LoopID reset'";

//                            if (dv.Count > 0)
//                            {
//                                string fileName = MarkersHealthCheckPath + "MarkerHealthCheck _" + DateTime.Now.Ticks.ToString() + ".xml";
//                                // dtResult.WriteXml(fileName);                               
//                                dv.ToTable().WriteXml(fileName);
//                            }


//                        }
//                    } 

//                }
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }
//    }
//}
