namespace TestApplication
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstex = new System.Windows.Forms.ListBox();
            this.lsttw = new System.Windows.Forms.ListBox();
            this.lstpad = new System.Windows.Forms.ListBox();
            this.lste2 = new System.Windows.Forms.ListBox();
            this.lste1 = new System.Windows.Forms.ListBox();
            this.lstreds = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button24 = new System.Windows.Forms.Button();
            this.txtpath = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.txtpadid = new System.Windows.Forms.TextBox();
            this.p3paused = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tag26 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tag54 = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.tag53 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.p1timer = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.tag52 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.p2timer = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.p3timer = new System.Windows.Forms.Label();
            this.txttagbox = new System.Windows.Forms.TextBox();
            this.p2paused = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.p1paused = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tmrautotest = new System.Windows.Forms.Timer(this.components);
            this.deadlocktimer = new System.Windows.Forms.Timer(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button25 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(107, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Configure System";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(101, 14);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Start";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(101, 54);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(74, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Start";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(101, 93);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(74, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Start";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(101, 134);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(74, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Start";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(101, 178);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(74, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Start";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "REDS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "QUEUE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "ENTRY1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Hopper";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "TruckWash";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Exit";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(101, 230);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(74, 23);
            this.button7.TabIndex = 11;
            this.button7.Text = "Start";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(3, 472);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(132, 23);
            this.button8.TabIndex = 13;
            this.button8.Text = "Clear Truck List";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(142, 462);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(132, 23);
            this.button9.TabIndex = 14;
            this.button9.Text = "Update Data";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(9, 506);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(104, 23);
            this.button10.TabIndex = 15;
            this.button10.Text = "REDS VMS Test";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(119, 501);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(155, 30);
            this.listBox1.TabIndex = 16;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(119, 537);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(155, 30);
            this.listBox2.TabIndex = 17;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(9, 535);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(104, 23);
            this.button11.TabIndex = 18;
            this.button11.Text = "Queue VMS Test";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(280, 537);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(116, 30);
            this.button12.TabIndex = 19;
            this.button12.Text = "Start Modbus Server";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(280, 479);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(140, 23);
            this.button13.TabIndex = 20;
            this.button13.Text = "Update Hopper Status";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(280, 508);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(132, 23);
            this.button14.TabIndex = 21;
            this.button14.Text = "Set Loop Id";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 284);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Issue Tag Office";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(101, 279);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(74, 23);
            this.button15.TabIndex = 22;
            this.button15.Text = "Start";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstex);
            this.groupBox1.Controls.Add(this.lsttw);
            this.groupBox1.Controls.Add(this.lstpad);
            this.groupBox1.Controls.Add(this.lste2);
            this.groupBox1.Controls.Add(this.lste1);
            this.groupBox1.Controls.Add(this.lstreds);
            this.groupBox1.Location = new System.Drawing.Point(269, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(948, 643);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // lstex
            // 
            this.lstex.FormattingEnabled = true;
            this.lstex.Location = new System.Drawing.Point(194, 303);
            this.lstex.Name = "lstex";
            this.lstex.Size = new System.Drawing.Size(182, 212);
            this.lstex.TabIndex = 12;
            // 
            // lsttw
            // 
            this.lsttw.FormattingEnabled = true;
            this.lsttw.Location = new System.Drawing.Point(382, 303);
            this.lsttw.Name = "lsttw";
            this.lsttw.Size = new System.Drawing.Size(182, 212);
            this.lsttw.TabIndex = 11;
            // 
            // lstpad
            // 
            this.lstpad.FormattingEnabled = true;
            this.lstpad.Location = new System.Drawing.Point(6, 303);
            this.lstpad.Name = "lstpad";
            this.lstpad.Size = new System.Drawing.Size(182, 212);
            this.lstpad.TabIndex = 10;
            // 
            // lste2
            // 
            this.lste2.FormattingEnabled = true;
            this.lste2.Location = new System.Drawing.Point(382, 48);
            this.lste2.Name = "lste2";
            this.lste2.Size = new System.Drawing.Size(182, 212);
            this.lste2.TabIndex = 9;
            // 
            // lste1
            // 
            this.lste1.FormattingEnabled = true;
            this.lste1.Location = new System.Drawing.Point(194, 48);
            this.lste1.Name = "lste1";
            this.lste1.Size = new System.Drawing.Size(182, 212);
            this.lste1.TabIndex = 8;
            // 
            // lstreds
            // 
            this.lstreds.FormattingEnabled = true;
            this.lstreds.Location = new System.Drawing.Point(7, 48);
            this.lstreds.Name = "lstreds";
            this.lstreds.Size = new System.Drawing.Size(182, 212);
            this.lstreds.TabIndex = 7;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1037, 444);
            this.tabControl1.TabIndex = 40;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button25);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.button24);
            this.tabPage1.Controls.Add(this.txtpath);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.listBox3);
            this.tabPage1.Controls.Add(this.button23);
            this.tabPage1.Controls.Add(this.button22);
            this.tabPage1.Controls.Add(this.txtpadid);
            this.tabPage1.Controls.Add(this.p3paused);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.tag26);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.tag54);
            this.tabPage1.Controls.Add(this.button21);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.button20);
            this.tabPage1.Controls.Add(this.tag53);
            this.tabPage1.Controls.Add(this.button19);
            this.tabPage1.Controls.Add(this.p1timer);
            this.tabPage1.Controls.Add(this.button18);
            this.tabPage1.Controls.Add(this.tag52);
            this.tabPage1.Controls.Add(this.button17);
            this.tabPage1.Controls.Add(this.p2timer);
            this.tabPage1.Controls.Add(this.button16);
            this.tabPage1.Controls.Add(this.p3timer);
            this.tabPage1.Controls.Add(this.txttagbox);
            this.tabPage1.Controls.Add(this.p2paused);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.p1paused);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.listBox1);
            this.tabPage1.Controls.Add(this.listBox2);
            this.tabPage1.Controls.Add(this.button11);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.button14);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.button15);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1029, 418);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(579, 233);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(82, 54);
            this.button24.TabIndex = 70;
            this.button24.Text = "NextTruckToCallup";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // txtpath
            // 
            this.txtpath.Location = new System.Drawing.Point(13, 376);
            this.txtpath.Name = "txtpath";
            this.txtpath.Size = new System.Drawing.Size(225, 20);
            this.txtpath.TabIndex = 69;
            this.txtpath.Text = "G:\\TempData\\RampTMS\\TestScript\\ChangPad3issue_UAT_2_4_pad_v4_Scriptv3_with_MB.csv" +
                "";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(667, 15);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 68;
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(541, 14);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(120, 212);
            this.listBox3.TabIndex = 67;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(8, 347);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 66;
            this.button23.Text = "AutoTest";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(449, 211);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 65;
            this.button22.Text = "IssueTag";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // txtpadid
            // 
            this.txtpadid.Location = new System.Drawing.Point(449, 99);
            this.txtpadid.Name = "txtpadid";
            this.txtpadid.Size = new System.Drawing.Size(75, 20);
            this.txtpadid.TabIndex = 64;
            // 
            // p3paused
            // 
            this.p3paused.AutoSize = true;
            this.p3paused.Location = new System.Drawing.Point(449, 389);
            this.p3paused.Name = "p3paused";
            this.p3paused.Size = new System.Drawing.Size(41, 13);
            this.p3paused.TabIndex = 59;
            this.p3paused.Text = "label18";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 49;
            this.label8.Text = "Pad1";
            // 
            // tag26
            // 
            this.tag26.AutoSize = true;
            this.tag26.Location = new System.Drawing.Point(185, 255);
            this.tag26.Name = "tag26";
            this.tag26.Size = new System.Drawing.Size(41, 13);
            this.tag26.TabIndex = 63;
            this.tag26.Text = "label12";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(277, 352);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "Pad2";
            // 
            // tag54
            // 
            this.tag54.AutoSize = true;
            this.tag54.Location = new System.Drawing.Point(185, 233);
            this.tag54.Name = "tag54";
            this.tag54.Size = new System.Drawing.Size(41, 13);
            this.tag54.TabIndex = 62;
            this.tag54.Text = "label11";
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(449, 182);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 48;
            this.button21.Text = "EX";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 390);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "Pad3";
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(449, 153);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 47;
            this.button20.Text = "TW";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // tag53
            // 
            this.tag53.AutoSize = true;
            this.tag53.Location = new System.Drawing.Point(185, 210);
            this.tag53.Name = "tag53";
            this.tag53.Size = new System.Drawing.Size(41, 13);
            this.tag53.TabIndex = 61;
            this.tag53.Text = "label12";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(449, 124);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 46;
            this.button19.Text = "PAD";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // p1timer
            // 
            this.p1timer.AutoSize = true;
            this.p1timer.Location = new System.Drawing.Point(325, 315);
            this.p1timer.Name = "p1timer";
            this.p1timer.Size = new System.Drawing.Size(41, 13);
            this.p1timer.TabIndex = 52;
            this.p1timer.Text = "label11";
            this.p1timer.Click += new System.EventHandler(this.p1timer_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(449, 73);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 45;
            this.button18.Text = "E2";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // tag52
            // 
            this.tag52.AutoSize = true;
            this.tag52.Location = new System.Drawing.Point(185, 188);
            this.tag52.Name = "tag52";
            this.tag52.Size = new System.Drawing.Size(41, 13);
            this.tag52.TabIndex = 60;
            this.tag52.Text = "label11";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(449, 44);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 44;
            this.button17.Text = "E1";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // p2timer
            // 
            this.p2timer.AutoSize = true;
            this.p2timer.Location = new System.Drawing.Point(325, 352);
            this.p2timer.Name = "p2timer";
            this.p2timer.Size = new System.Drawing.Size(41, 13);
            this.p2timer.TabIndex = 53;
            this.p2timer.Text = "label12";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(449, 15);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 43;
            this.button16.Text = "REDS";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // p3timer
            // 
            this.p3timer.AutoSize = true;
            this.p3timer.Location = new System.Drawing.Point(328, 389);
            this.p3timer.Name = "p3timer";
            this.p3timer.Size = new System.Drawing.Size(41, 13);
            this.p3timer.TabIndex = 54;
            this.p3timer.Text = "label13";
            // 
            // txttagbox
            // 
            this.txttagbox.Location = new System.Drawing.Point(283, 15);
            this.txttagbox.Multiline = true;
            this.txttagbox.Name = "txttagbox";
            this.txttagbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txttagbox.Size = new System.Drawing.Size(154, 158);
            this.txttagbox.TabIndex = 42;
            this.txttagbox.Text = "400002052";
            // 
            // p2paused
            // 
            this.p2paused.AutoSize = true;
            this.p2paused.Location = new System.Drawing.Point(449, 352);
            this.p2paused.Name = "p2paused";
            this.p2paused.Size = new System.Drawing.Size(41, 13);
            this.p2paused.TabIndex = 58;
            this.p2paused.Text = "label17";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(328, 286);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "Timer";
            // 
            // p1paused
            // 
            this.p1paused.AutoSize = true;
            this.p1paused.Location = new System.Drawing.Point(446, 316);
            this.p1paused.Name = "p1paused";
            this.p1paused.Size = new System.Drawing.Size(41, 13);
            this.p1paused.TabIndex = 57;
            this.p1paused.Text = "label16";
            this.p1paused.Click += new System.EventHandler(this.p1paused_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(446, 289);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 56;
            this.label15.Text = "IsPaused";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1029, 418);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tmrautotest
            // 
            this.tmrautotest.Tick += new System.EventHandler(this.tmrautotest_Tick);
            // 
            // deadlocktimer
            // 
            this.deadlocktimer.Tick += new System.EventHandler(this.deadlocktimer_Tick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(280, 180);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(100, 17);
            this.checkBox1.TabIndex = 71;
            this.checkBox1.Text = "DeadLock Test";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(551, 315);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 72;
            this.button25.Text = "DeadLockTest";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 444);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lstex;
        private System.Windows.Forms.ListBox lsttw;
        private System.Windows.Forms.ListBox lstpad;
        private System.Windows.Forms.ListBox lste2;
        private System.Windows.Forms.ListBox lste1;
        private System.Windows.Forms.ListBox lstreds;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox txtpadid;
        private System.Windows.Forms.Label p3paused;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label tag26;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label tag54;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label tag53;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Label p1timer;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label tag52;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label p2timer;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label p3timer;
        private System.Windows.Forms.TextBox txttagbox;
        private System.Windows.Forms.Label p2paused;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label p1paused;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Timer tmrautotest;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtpath;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.Timer deadlocktimer;
        private System.Windows.Forms.Button button25;
    }
}
