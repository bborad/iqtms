using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ramp.RFIDHWConnector.Adapters;
using Ramp.MiddlewareController.Common;
using Ramp.VMSHWConnector.Adapter;
using Modbus = Ramp.Modbus.Connector;
using Ramp.Modbus.Utility;

namespace TestApplication
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        System.Timers.Timer myTimer;

        private void button1_Click(object sender, EventArgs e)
        {
            Reader objReader = new Reader("192.168.1.99", 2101);
            
            List<RFIDTag> lstTags = objReader.GetTags();

            if (lstTags != null && lstTags.Count > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0, 100);
                objReader.BlinkLED(lstTags[0].TagID, TagLedColor.RED,  ts, 50);
            }
            else
            {
                lstTags = objReader.ScanForTags(10);
            }

            Reader.GetMarkersStatus();

            objReader.Dispose();
            
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Modbus.Processor objModbus = null;
            try
            {
                objModbus = new Ramp.Modbus.Connector.Processor();
                if (objModbus.StartModbusServer(Ramp.Modbus.Utility.ModbusConnectionTypes.TCPIP) == 0)
                {
                          short result = objModbus.ReadPAD_Match(1);      
                    objModbus.StopModbusServer();
                }
            }
            catch
            {
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            myTimer = new System.Timers.Timer();
            myTimer.Elapsed += new System.Timers.ElapsedEventHandler(myTimer_Elapsed);
            myTimer.Interval = 60 * 1000;
            myTimer.Start();
        }

        void myTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            myTimer.Stop();
            MessageBox.Show(myTimer.Interval.ToString());
            myTimer.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            myTimer.Stop();
            MessageBox.Show(myTimer.Interval.ToString());          

            myTimer.Interval = myTimer.Interval - 10000;

            myTimer.Start();

            MessageBox.Show(myTimer.Interval.ToString());

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            List<string> lst = new List<string>();

            lst.Add("vijay");

            lst.Remove("vijay");

           
        }
    }
}
