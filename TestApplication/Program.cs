using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Ramp.BusinessLogic.Connector;
using Ramp.BusinessLogic.Common;
using Ramp.Database.Common;
using System.Timers;
using RMC = Ramp.MiddlewareController.Common;
using Ramp;
using Ramp.VMSHWConnector;

namespace TestApplication
{
    static class Program
    {
        public static List<RMC.RFIDTag> readlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        public static List<RMC.RFIDTag> e1list = new List<Ramp.MiddlewareController.Common.RFIDTag>();
       // public static List<RMC.RFIDTag> e2list = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        public static List<RMC.RFIDTag> padlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        public static List<RMC.RFIDTag> twlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        //public static List<RMC.RFIDTag> exlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        public static List<RMC.RFIDTag> issuetaglist = new List<Ramp.MiddlewareController.Common.RFIDTag>();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form3());
        }
    }
}
