using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ramp.BusinessLogic.Connector;
using Ramp.BusinessLogic.Common;
using Ramp.Database.Common;
using System.Timers;
using RMC = Ramp.MiddlewareController.Common;
using Ramp;
using Ramp.VMSHWConnector;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Data.SqlClient;
using NLog;

namespace TestApplication
{
    public partial class Form3 : Form
    {
        Logger log = LogManager.GetCurrentClassLogger();
        System.Timers.Timer tmrREDS;
        System.Timers.Timer tmrQUEUE;
        System.Timers.Timer tmrENTRY1;
        System.Timers.Timer tmrHOPPER;
        System.Timers.Timer tmrTRUCKWASH;
        System.Timers.Timer tmrEXIT;

        System.Timers.Timer tmrIssueTagOffice;

        System.Timers.Timer tmrModbusRead;

        System.Timers.Timer tmrGarbageCollector;

        public Form3()
        {
            InitializeComponent();
        }

        DateTime dt;

        private void button1_Click(object sender, EventArgs e)
        {
            DBUtility.GetExpectedTimeValues();
            BLUtility.ReadNStoreAllReaders();
            BLUtility.GetAllHoppersInFacility();


            BLUtility.GetAllMarkers();

            BLUtility.GetAllVMSDevices();

            BLUtility.GetConfigurationValues();

            BLUtility.StartModbusServer();

            BLUtility.GetActiveTrucksInFacility(1);

            //string date = "5/20/2011 11:56:11 AM";
            //dt = Convert.ToDateTime(date); 
            BLUtility.UpdateHopperStatus();
            ConfigureTimers();

            //mdb.ConnectModbusServer();
            //mdb.WriteRegister(1, (1 + 2), 1);
            
            //mdb.WriteRegister(1, (2 + 2), 1);
            //mdb.WriteRegister(1, (11 + 2), 1);
            //mdb.WriteRegister(1, (12 + 2), 1);

        }

        void ConfigureTimers()
        {
            tmrREDS = new System.Timers.Timer();
            tmrREDS.AutoReset = true;
            tmrREDS.Elapsed += new System.Timers.ElapsedEventHandler(tmrREDS_Elapsed);
            tmrREDS.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrQUEUE = new System.Timers.Timer();
            tmrQUEUE.AutoReset = true;
            tmrQUEUE.Elapsed += new System.Timers.ElapsedEventHandler(tmrQUEUE_Elapsed);
            tmrQUEUE.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrENTRY1 = new System.Timers.Timer();
            tmrENTRY1.AutoReset = true;
            tmrENTRY1.Elapsed += new System.Timers.ElapsedEventHandler(tmrENTRY1_Elapsed);
            tmrENTRY1.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrHOPPER = new System.Timers.Timer();
            tmrHOPPER.AutoReset = true;
            tmrHOPPER.Elapsed += new System.Timers.ElapsedEventHandler(tmrHOPPER_Elapsed);
            tmrHOPPER.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrTRUCKWASH = new System.Timers.Timer();
            tmrTRUCKWASH.AutoReset = true;
            tmrTRUCKWASH.Elapsed += new System.Timers.ElapsedEventHandler(tmrTRUCKWASH_Elapsed);
            tmrTRUCKWASH.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrEXIT = new System.Timers.Timer();
            tmrEXIT.AutoReset = true;
            tmrEXIT.Elapsed += new System.Timers.ElapsedEventHandler(tmrEXIT_Elapsed);
            tmrEXIT.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;

            tmrIssueTagOffice = new System.Timers.Timer();
            tmrIssueTagOffice.AutoReset = true;
            tmrIssueTagOffice.Elapsed += new ElapsedEventHandler(tmrIssueTagOffice_Elapsed);
            tmrIssueTagOffice.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;


            tmrGarbageCollector = new System.Timers.Timer();
            tmrGarbageCollector.Elapsed += new ElapsedEventHandler(tmrGarbageCollector_Elapsed);
            tmrGarbageCollector.Interval = 1000 * 80;
            tmrGarbageCollector.Start();

            tmrModbusRead = new System.Timers.Timer();
            tmrModbusRead.AutoReset = true;
            tmrModbusRead.Elapsed += new ElapsedEventHandler(tmrModbusRead_Elapsed);
            tmrModbusRead.Interval = 1000 * RMC.ExpectedValues.TagReadFrequency;


        }

        void tmrIssueTagOffice_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmrIssueTagOffice.Stop();

            try
            {

                List<RMC.RFIDTag> lstTags = Program.issuetaglist.ToList();  //Utility.ReadTagList(RMC.Location.IssueTagOffice);
                Program.issuetaglist.Clear();
                if (lstTags.Count > 0)
                    Utility.UpdateTransactionIssueTagOffice(lstTags, RMC.Location.IssueTagOffice);
            }
            catch (Exception ex)
            {
                //throw ex;
            }

            tmrIssueTagOffice.Start();
        }

        void tmrModbusRead_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmrModbusRead.Stop();
            BLUtility.UpdateHopperStatus();
            tmrModbusRead.Start();
        }

        void tmrGarbageCollector_Elapsed(object sender, ElapsedEventArgs e)
        {
            //GC.Collect();
        }

        void tmrEXIT_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //tmrEXIT.Stop(); 

            //try
            //{
            //    List<RMC.RFIDTag> lstTags = Utility.ReadTagList(RMC.Location.EXITGATE);
            //    Utility.UpdateTransactionTruckWash(lstTags, RMC.Location.EXITGATE);
            //}
            //catch (Exception ex)
            //{
            //    //throw ex;
            //}

            //tmrEXIT.Start();



        }

        void tmrTRUCKWASH_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrTRUCKWASH.Stop();

            try
            {
                List<RMC.RFIDTag> lstTags = Program.twlist.ToList();
                List<RMC.RFIDTag> lstTags1 = Utility.ReadTagList(RMC.Location.TRUCKWASH);
                Program.twlist.Clear();
                if (lstTags.Count > 0)
                    Utility.UpdateTransactionTruckWash(lstTags, RMC.Location.TRUCKWASH);

            }
            catch (Exception ex)
            {
                //  throw ex;
            }

            tmrTRUCKWASH.Start();
        }

        void tmrHOPPER_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrHOPPER.Stop();

            try
            {
                List<RMC.RFIDTag> lstTags = Program.padlist.ToList();
                Program.padlist.Clear();
                if (lstTags.Count > 0)
                    Utility.UpdateTransaction(lstTags, RMC.Location.HOPPER);
            }
            catch (Exception ex)
            {
                // throw ex;
            }

            tmrHOPPER.Start();
        }

        void tmrENTRY1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrENTRY1.Stop();

            try
            {
                List<RMC.RFIDTag> lstTags = Program.e1list.ToList();
                Program.e1list.Clear();
                if (lstTags.Count > 0)
                    Utility.UpdateTransactionEntryGate(lstTags, RMC.Location.ENTRY1);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            tmrENTRY1.Start();
        }

        void tmrQUEUE_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrQUEUE.Stop();

            try
            {
                List<RMC.RFIDTag> lstTags = Utility.ReadTagList(RMC.Location.QUEUE);
                if (lstTags.Count > 0)
                    Utility.UpdateTransactionQueueReads(lstTags, RMC.Location.QUEUE);
            }
            catch (Exception ex)
            {
                //  throw ex;
            }
            tmrQUEUE.Start();
        }

        void tmrREDS_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrREDS.Stop();

            try
            {
                List<RMC.RFIDTag> lstTags =   Program.readlist.ToList();
                Program.readlist.Clear();
                if (lstTags.Count > 0)
                {
                    Utility.UpdateTransaction(lstTags, RMC.Location.REDS);
                    if (lstTags[0].TagID == "400002063" && checkBox1.Checked)
                    {
                        deadlocktimer_Tick(null, null);

                        //deadlocktimer.Interval = 20000;
                        //deadlocktimer.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            tmrREDS.Start();
        }

        void StartStopTimer(Button button, System.Timers.Timer timer)
        {
            if (timer.Enabled)
            {
                timer.Stop();
                button.Text = "Start";
            }
            else
            {
                timer.Start();
                button.Text = "Stop";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StartStopTimer(button2, tmrREDS);
            // DummyReadsAtREDS();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            StartStopTimer(button3, tmrQUEUE);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StartStopTimer(button4, tmrENTRY1);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            StartStopTimer(button5, tmrHOPPER);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            StartStopTimer(button6, tmrTRUCKWASH);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            StartStopTimer(button7, tmrEXIT);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            BLUtility.TrucksInTransaction.Clear();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            BLUtility.Markers.Clear();
            BLUtility.HoppersInFacility.Clear();
            BLUtility.GetAllHoppersInFacility();
            Utility.GetAllMarkers();
        }

        DateTime TimerStartTime, TimerStoppedTime;

        private void Form3_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    StringBuilder blankString = new StringBuilder("Vijay");
            //    blankString.Append(' ', 20);
            //    MessageBox.Show(blankString.ToString());
            //}
            //catch
            //{
            //}

            //   Ramp.MiddlewareController.Common.RFIDTag tag = new Ramp.MiddlewareController.Common.RFIDTag();
            //  tag.BatteryStatus = Ramp.MiddlewareController.Common.TagBatteryStatus.Intermediate;

            // MessageBox.Show(tag.BatteryStatus.ToString());

            //Test(); 

            //TimerStartTime = DateTime.Now;

            //System.Threading.Thread.Sleep(20 * 1000);

            //TimerStoppedTime = DateTime.Now.AddSeconds(60);

            //double interval = TimerStoppedTime.Subtract(TimerStartTime).TotalSeconds;

            //MessageBox.Show(interval.ToString());

            //this.Close(); 

        }

        private void button10_Click(object sender, EventArgs e)
        {
            List<Ramp.MiddlewareController.Connector.VMSHardWareConnector.IVMSDevice> obj = BLUtility.VMSDeviceList[Ramp.MiddlewareController.Common.Location.REDS];
            listBox1.Items.Clear();

            //  listBox1.DataSource = obj[0].currentMessage;
            //obj[0].ShowString("Go 123", "aaaaaa", 2);
            //obj[0].ShowString("Go 321", "aaaaaa", 1);
            //obj[0].ShowString("Go 001", "aaaaaa", 1);
            //obj[0].ShowString("Go 002", "aaaaaa", 2);
            //obj[0].ShowString("Go 003", "aaaaaa", 0);
            //obj[0].ShowString("Go 004", "aaaaaa", 1);
            //obj[0].ShowString("Go 005", "aaaaaa", 1);

            //foreach (string msg in obj[0].currentMessage)
            //{
            //    listBox1.Items.Add(msg);
            //}
        }

        private void button11_Click(object sender, EventArgs e)
        {
            List<Ramp.MiddlewareController.Connector.VMSHardWareConnector.IVMSDevice> obj = BLUtility.VMSDeviceList[Ramp.MiddlewareController.Common.Location.QUEUE];
            listBox2.Items.Clear();

            //  listBox1.DataSource = obj[0].currentMessage;


            //foreach (string msg in obj[0].currentMessage)
            //{
            //    listBox2.Items.Add(msg);
            //}
        }

        private void button12_Click(object sender, EventArgs e)
        {
            //BLUtility.StartModbusServer();
            // tmrModbusRead.Start();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            // BLUtility.UpdateHopperStatus();
            BLUtility.GetConfigurationValues();
        }

        void DummyReadsAtREDS()
        {
            try
            {
                List<RMC.RFIDTag> lstTags;
                // lstTags = Utility.ReadTagList(RMC.Location.REDS);

                lstTags = new List<Ramp.MiddlewareController.Common.RFIDTag>();

                RMC.RFIDTag objTag;

                for (int i = 700000010; i <= 700000059; i++)
                {
                    objTag = new Ramp.MiddlewareController.Common.RFIDTag();
                    objTag.TagID = i.ToString();
                    objTag.NewMarkerID = 111;
                    lstTags.Add(objTag);
                }

                Utility.UpdateTransaction(lstTags, RMC.Location.REDS);
            }
            catch (Exception ex)
            {
                // throw ex;
            }
        }

        void Test()
        {
            string blankString;

            blankString = "`C000000 ";
            blankString = blankString.PadRight(25, ' ');

            string defaultMsg = "`C00FFFF " + "Ok";

            defaultMsg = defaultMsg.PadRight(25, ' ');

            StringBuilder blankScreen = new StringBuilder(defaultMsg);

            for (int index = 0; index < 10 - 1; index++)
            {

                blankScreen.Append(System.Environment.NewLine);

                //  YPos = Convert.ToInt16(index * TextLength); 

                blankScreen.Append(blankString);

                //objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, "`C000000 " + blankString.ToString()); // Black color
            }

            MessageBox.Show(blankScreen.ToString());

        }

        private void button14_Click(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable("dtMarkers");
            //DataColumn[] dcc = new DataColumn[] { new DataColumn("SerialNo"),
            //                                       new DataColumn("LoopId")
            //};

            //dt.Columns.AddRange(dcc);

            //DataRow dr;

            //dr = dt.NewRow();
            //dr["SerialNo"] = "10073L1345";
            //dr["LoopId"] = "111";
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            //dr["SerialNo"] = "10073L1350";
            //dr["LoopId"] = "222";
            //dt.Rows.Add(dr);

            //dt.AcceptChanges();

            //DataTable dtResult =  Ramp.RFIDHWConnector.Adapters.Reader.SetMarkersLoopID(dt);

            //if (dtResult != null)
            //{
            //}

            object value = System.Configuration.ConfigurationSettings.AppSettings["MarkerHealthCheckLogPath"];
            Utility.MarkersHealthCheckPath = Convert.ToString(value);

            if (Utility.MarkersHealthCheckPath.Length > 0)
            {
                if (!System.IO.Directory.Exists(Utility.MarkersHealthCheckPath))
                {
                    System.IO.Directory.CreateDirectory(Utility.MarkersHealthCheckPath);

                }
            }

            Utility.SetMarkersLoopID_Routine();

        }

        private void button15_Click(object sender, EventArgs e)
        {
            StartStopTimer(button15, tmrIssueTagOffice);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            timer1.Stop();
            try
            {

                // listBox3.Items.Clear();
                listBox3.DataSource = BLUtility.TrucksInTransaction.Select(x => x.Value.LastPositiion.ToString() + " : " + x.Value.TagDB.TagRFID).ToList();
                dataGridView1.DataSource = BLUtility.TrucksInTransaction.Select(x => x.Value.LastPositiion.ToString() + " : " + x.Value.TagDB.TagRFID).ToList();
                dataGridView1.DataMember = "Tags";

                dataGridView1.Update();
                dataGridView1.Refresh();

            }
            catch { }
            try
            {
                //foreach (Truck truck in BLUtility.TrucksInTransaction.Values)
                //{

                //}
                if (Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction.ContainsKey("400002063"))
                    tag52.Text = "400002063 DesPad: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002063"].TagDB.DestinationPad.ToString() +
                     " LastLoc: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002063"].LastPositiion.ToString();

                if (Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction.ContainsKey("400002052"))
                    tag53.Text = "400002052 DesPad: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002052"].TagDB.DestinationPad.ToString() +
                   " LastLoc: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002052"].LastPositiion.ToString();

                if (Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction.ContainsKey("400002053"))
                    tag54.Text = "400002053 DesPad: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002053"].TagDB.DestinationPad.ToString() +
                   " LastLoc: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002053"].LastPositiion.ToString();

                if (Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction.ContainsKey("400002054"))
                    tag26.Text = "400002054 DesPad: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002054"].TagDB.DestinationPad.ToString() +
                        " FoundAt: " + Ramp.BusinessLogic.Common.BLUtility.TrucksInTransaction["400002054"].FoundAtHopperID.ToString();

            }
            catch { }
            try
            {
                p1timer.Text = DateTime.Now.ToString() + "\n PAD1 " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["1"].tmrQueue.Enabled.ToString() +
                     " : " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["1"].tmrQueue.Interval.ToString();
                p1paused.Text = DateTime.Now.ToString() + "\n" + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["1"].IsTimerPaused.ToString();
            }
            catch { }
            try
            {
                p2timer.Text = DateTime.Now.ToString() + "\n PAD12 " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["12"].tmrQueue.Enabled.ToString() +
                        " : " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["12"].tmrQueue.Interval.ToString();
                p2paused.Text = DateTime.Now.ToString() + "\n" + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["12"].IsTimerPaused.ToString();
            }
            catch { }
            try
            {
                p3timer.Text = DateTime.Now.ToString() + "\n PAD3 " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["3"].tmrQueue.Enabled.ToString() +
                    " : " + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["3"].tmrQueue.Interval.ToString();
                p3paused.Text = DateTime.Now.ToString() + "\n" + Ramp.BusinessLogic.Common.BLUtility.HoppersInFacility["3"].IsTimerPaused.ToString();


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            timer1.Start();
        }

        public static List<RMC.RFIDTag> ConvertToRFIDTag(string csvtag, int newMarker, int oldMarker)
        {
            List<RMC.RFIDTag> tlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();

            RMC.RFIDTag rfidtag = new Ramp.MiddlewareController.Common.RFIDTag();
            try
            {
                foreach (string tag in csvtag.Replace("\r", "").Split('\n'))
                {
                    rfidtag.SerialLabel = rfidtag.TagID = tag;
                    rfidtag.SerialNo = System.Convert.ToInt32(tag);
                    rfidtag.NewMarkerID = newMarker;
                    rfidtag.MarkerNewPositionTime = rfidtag.TimeFirstSeen = rfidtag.TimeLastSeen = DateTime.Now; ;
                    rfidtag.OldMarkerID = oldMarker;
                    rfidtag.MarkerOldPositionTime = DateTime.Now.AddHours(-1);
                    rfidtag.BatteryStatus = Ramp.MiddlewareController.Common.TagBatteryStatus.Good;
                    tlist.Add(rfidtag);
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\IQReaderServiceLog.txt", System.Environment.NewLine + System.DateTime.Now.ToString() +
                    " Exception thrown while converting csv to rfid tag.:");
            }
            return tlist;
        }

        private void button16_Click(object sender, EventArgs e)
        {

            Program.readlist.AddRange(ConvertToRFIDTag(txttagbox.Text, 1, 1).Distinct());

        }

        private void button17_Click(object sender, EventArgs e)
        {
            Program.e1list.AddRange(ConvertToRFIDTag(txttagbox.Text, 2, 6).Distinct());
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Program.e1list.AddRange(ConvertToRFIDTag(txttagbox.Text, 4, 2).Distinct());
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Program.padlist.AddRange(ConvertToRFIDTag(txttagbox.Text, Convert.ToInt32(txtpadid.Text.Trim()), 4).Distinct());
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Program.twlist.AddRange(ConvertToRFIDTag(txttagbox.Text, 6, 111).Distinct());
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Program.twlist.AddRange(ConvertToRFIDTag(txttagbox.Text, 5, 111).Distinct());
        }

        private void button22_Click(object sender, EventArgs e)
        {
            Program.issuetaglist.AddRange(ConvertToRFIDTag(txttagbox.Text, 99, 111).Distinct());
        }

        private void p1timer_Click(object sender, EventArgs e)
        {

        }

        private void button23_Click(object sender, EventArgs e)
        {


            Thread tagthread = new Thread(Form3.StartProcess);
            tagthread.IsBackground = true;
            tagthread.Start(txtpath.Text.Trim());
            //Program.readlist.AddRange(Form3.ConvertToRFIDTag("400002052", 1, 1).Distinct());
            //Thread.Sleep(10000);
            //Program.readlist.AddRange(Form3.ConvertToRFIDTag("400002053", 1, 1).Distinct());
            //Thread.Sleep(20000);
            //Program.readlist.AddRange(Form3.ConvertToRFIDTag("400002054", 1, 1).Distinct());
            //Thread.Sleep(15000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002052", 2, 1).Distinct());
            //Thread.Sleep(10000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002053", 2, 1).Distinct());
            //Thread.Sleep(5000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002052", 4, 2).Distinct());
            //Thread.Sleep(10000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002053", 4, 2).Distinct());
            //Thread.Sleep(5000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002054", 2, 1).Distinct());
            //Thread.Sleep(15000);
            //Program.e1list.AddRange(Form3.ConvertToRFIDTag("400002054", 4, 2).Distinct());
            //Program.padlist.AddRange(Form3.ConvertToRFIDTag("400002052", 111, 4).Distinct());
        }
 public static Ramp.ModbusMaster.ModbusMSConnector mdb = new Ramp.ModbusMaster.Connector.Processor();
        public static void StartProcess(object fpath)
        {
           
            try
            {
                
              //  mdb.ConnectModbusServer();
            }
            catch { }
            try
            {
                string fname = fpath.ToString();
                int timepassed = 0;
                if (File.Exists(fname))
                {
                    string[] reads = File.ReadAllLines(fname);

                    foreach (string tag in reads)
                    {
                        if (tag.ToLower().Contains("time") || tag.ToLower().Contains("nowhere"))
                            continue;

                        string issueTag = "";
                        if (tag.Contains("400004482"))
                            issueTag = "found";

                        string[] fields = tag.Split(',');

                        int wait = 0;
                        try
                        {
                            Int32.TryParse(fields[0], out wait);
                        }
                        catch { }

                        Thread.Sleep((wait - timepassed) * 1000);

                        timepassed = wait;
                        string read = fields[1];
                        string readerip = "192.168.168.1";
                        if (fields[2].ToLower().Contains("m"))
                        {
                            mdb.WriteRegister(1, (Convert.ToInt32(fields[2].Substring(1)) + 2), Convert.ToInt16(fields[1]));
                            continue;
                        }
                        else if (fields[2].ToLower().Contains("pad"))
                        {
                            readerip = "192.168.168.202";
                            string no = fields[2].ToLower().Substring(3);
                            string padLoopid = "11";
                            if (no == "1")
                            {
                                padLoopid = "111";
                            }
                            else
                            {
                                while (no.Length < 4)
                                    no += no;
                                padLoopid = no;
                            }
                            Program.padlist.AddRange(Form3.ConvertToRFIDTag(fields[1], Convert.ToInt32(padLoopid), 2));

                        }
                        else
                        {
                            switch (fields[2].ToLower())
                            {
                                case "reds":
                                    readerip = "192.168.168.200";
                                    read += ",21,22";
                                    Program.readlist.AddRange(Form3.ConvertToRFIDTag(fields[1], 21, 22));
                                    break;
                                case "e1":
                                    readerip = "192.168.168.201";
                                    read += ",2,21";
                                    Program.e1list.AddRange(Form3.ConvertToRFIDTag(fields[1], 2, 21));
                                    break;
                                case "e2":
                                    readerip = "192.168.168.201";
                                    read += ",4,2";
                                    Program.e1list.AddRange(Form3.ConvertToRFIDTag(fields[1], 4, 2));
                                    break;
                                case "x":
                                    readerip = "192.168.168.203";
                                    read += ",6,111";
                                    Program.twlist.AddRange(Form3.ConvertToRFIDTag(fields[1], 6, 111));
                                    break;

                            }
                        }
                        //read += System.Environment.NewLine;
                        //lock (Program.lastagloc)
                        //{
                        //    Program.lasttag.Append(DateTime.Now.ToString() + " : " + read);
                        //}
                        //File.AppendAllText(@"C:\" + readerip + "Tags.csv", read);
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("ErrorLog.txt", "\nException thrown in bg thread" + ex.Message);
            }

           // mdb.DisconnectModbusServer();

            //lock (Program.lastagloc)
            //{
            //    Program.lasttag.Append(DateTime.Now.ToString() + " : End");
            //}

        }

        private void p1paused_Click(object sender, EventArgs e)
        {

        }

        private void tmrautotest_Tick(object sender, EventArgs e)
        {

        }

        private void button24_Click(object sender, EventArgs e)
        {
            string ttocall = "";
            int wtime = 0;
           
            Ramp.DBConnector.Adapter.ActiveReads.NextTruckToCallup("", 1, out ttocall, out wtime);
        }

        private void deadlocktimer_Tick(object sender, EventArgs e)
        {
            Thread.Sleep(20000);
            deadlocktimer.Enabled = false;
            try
            {
                //mdb.ConnectModbusServer();
            }
            catch { }
            mdb.WriteRegister(1, (1 + 2), 0);
            
            BLUtility.HoppersInFacility["1"].TruckCallup();
            
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            
        }

        private void button25_Click(object sender, EventArgs e)
        {

            //Add Truck in Transaction list
            BLUtility.HoppersInFacility["1"].StopTimer();
            BLUtility.HoppersInFacility["2"].StopTimer();
            BLUtility.HoppersInFacility["11"].StopTimer();
            BLUtility.HoppersInFacility["12"].StopTimer();

            using (SqlConnection cn = new SqlConnection(@"Data Source=192.168.168.207\SQLExpress; Database =RampTMS;User ID=sa; Password=ramp;"))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Exec InsertIntoTimeTable", cn);
                cmd.ExecuteNonQuery();
            }

            Ramp.MiddlewareController.Common.RFIDTag t = new Ramp.MiddlewareController.Common.RFIDTag();
            t.TagID = "400002063";
            t.NewMarkerID =6;
            t.OldMarkerID = 1212;
            t.TimeFirstSeen = t.TimeLastSeen = DateTime.Now;
            BLUtility.TrucksInTransaction["400002063"] = new Truck(t, Ramp.MiddlewareController.Common.Location.REDS);

            Ramp.MiddlewareController.Common.RFIDTag tt = new Ramp.MiddlewareController.Common.RFIDTag();
            tt.TagID = "400002064";
            tt.NewMarkerID = 6;
            tt.OldMarkerID = 1212;
            tt.TimeFirstSeen = tt.TimeLastSeen = DateTime.Now;
            BLUtility.TrucksInTransaction["400002064"] = new Truck(tt, Ramp.MiddlewareController.Common.Location.REDS);
            
            Ramp.MiddlewareController.Common.RFIDTag t1 = new Ramp.MiddlewareController.Common.RFIDTag();
            t1.TagID = "400002052";
            t1.NewMarkerID = 6;
            t1.OldMarkerID = 1212;
            t1.TimeFirstSeen = t1.TimeLastSeen = DateTime.Now;
            BLUtility.TrucksInTransaction["400002052"] = new Truck(t1, Ramp.MiddlewareController.Common.Location.QUEUE);

            Ramp.MiddlewareController.Common.RFIDTag t2 = new Ramp.MiddlewareController.Common.RFIDTag();
            t2.TagID = "400002053";
            t2.NewMarkerID = 6;
            t2.OldMarkerID = 1212;
            t2.TimeFirstSeen = t2.TimeLastSeen = DateTime.Now;
            BLUtility.TrucksInTransaction["400002053"] = new Truck(t, Ramp.MiddlewareController.Common.Location.QUEUE);

            Ramp.MiddlewareController.Common.RFIDTag t3 = new Ramp.MiddlewareController.Common.RFIDTag();
            t3.TagID = "400002054";
            t3.NewMarkerID = 6;
            t3.OldMarkerID = 1212;
            t3.TimeFirstSeen = t3.TimeLastSeen = DateTime.Now;
            BLUtility.TrucksInTransaction["400002054"] = new Truck(t, Ramp.MiddlewareController.Common.Location.QUEUE);


            //Add few truck in ActiveRead - TimeTable 
           

            //Call OpenClose method
            DateTime stie, ntime;
             stie = DateTime.Now;
              //  BLUtility.HoppersInFacility["1"].TruckCallup();
             ntime = DateTime.Now;

             double aa = ntime.Subtract(stie).TotalMilliseconds;

            Thread th2= new Thread(delegate()
            {
             stie = DateTime.Now;
                Algorithms.TrafficTransactionOpenClose(t, Ramp.MiddlewareController.Common.Location.QUEUE);
                ntime = DateTime.Now;
            });
                 aa = ntime.Subtract(stie).TotalMilliseconds;
            Thread th3 = new Thread(delegate()
            {
                Algorithms.TrafficTransactionOpenClose(tt, Ramp.MiddlewareController.Common.Location.QUEUE);
            });
            Thread th4 = new Thread(delegate()
              {
                  BLUtility.HoppersInFacility["11"].TruckCallup();
              });
           // th1.Start();
            th2.Start();
            th4.Start();
            th3.Start();

           // th1.Join();
            th2.Join();
            th3.Join();
            th4.Join();
            MessageBox.Show("Finish truck callup"+aa.ToString());
            //call truc callup

        }



    }
}
