/*
   Friday, 24 February 20123:52:04 PM
   User: sa
   Server: DEVELOPMENT\SQLEXPRESS
   Database: RampTMS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ArchiveReads ADD
	DestinationPad int NULL
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ArchiveReads', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ArchiveReads', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ArchiveReads', 'Object', 'CONTROL') as Contr_Per 