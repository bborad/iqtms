

declare @TagRFID varchar(50)
declare	@ReadTime datetime
declare	@Location int
declare	@ResultPadID int 
declare	@ResultPadTime datetime 

set @TagRFID = '400002054'
set @ReadTime = getdate()
set @Location = 1

--select dbo.GetPadFreeTime(1,@ReadTime)
/*
		declare @TagID bigint
			declare @TravelTime int
				set @TravelTime = (select case when @Location < 2 then REDS_Queue else 0 end 
											+ case when @Location < 3 then Queue_Ent1 else 0 end
											+ case when @Location < 4 then Ent1_Ent2 else 0 end
										from ExpectedTimes)
			declare @temptable table
					( PadID int,PTime datetime,GOrder int )
			select @TagID = ID from Tags where TagRFID = @TagRFID
			insert into @temptable (PadID,PTime,GOrder)
							select ID,dbo.getPadFreeTime(ID,dateadd(ss,TimeFromEnt2+@TravelTime,@ReadTime)),GroupOrder 
								from Hoppers where Status = 0 and GroupID = dbo.Group_GetGroup(@TagID)
			select * from @temptable
			 select Top 1 @ResultPadTime = PTime, @ResultPadID = PadID 
							from @temptable 
								order by PTime,GOrder 
			select  dbo.Group_GetGroup(@TagID)

delete from TimeTable
*/
execute GetFirstFreePad @TagRFID,@ReadTime,@Location,@ResultPadID output,@ResultPadTime output

select @ReadTime,@ResultPADID,@ResultPadTime

