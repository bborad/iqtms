set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Get next arrival for given pad when pad will be free for unload time from given start time
-- =============================================
ALTER FUNCTION [dbo].[GetTravelTimeToPad] 
(
	-- Add the parameters for the function here
	 @CurrentLocation int,
	 @DestinationPad int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	--DECLARE  @result varchar(500)
	 declare @TravelTime int

	 select @TravelTime = case when @CurrentLocation < 2 then REDS_Queue else 0 end +
						  case when @CurrentLocation < 3 then Queue_Ent1 else 0 end +
						  case when @CurrentLocation < 4 then Ent1_Ent2 else 0 end +
						  case when @CurrentLocation < 5 then TimeFromEnt2 else 0 end +
						  case when @CurrentLocation = 6 or @CurrentLocation = 7 then TimeToTW else 0 end
	 from ExpectedTimes, Hoppers
		where Hoppers.ID = @DestinationPad

	return @TravelTime 

END







