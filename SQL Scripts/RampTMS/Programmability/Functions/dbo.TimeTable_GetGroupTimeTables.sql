SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex Jarkey
-- Create date: 10/11/2011
-- Description:	Function to get the Timetables for Other trucks in the same group
-- =============================================

if exists (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[TimeTable_GetGroupTimeTables]') AND OBJECTPROPERTY(id, N'IsFunction') = 1)
begin
	DROP FUNCTION [dbo].[TimeTable_GetGroupTimeTables]
end
GO

CREATE FUNCTION [dbo].[TimeTable_GetGroupTimeTables](@TagID bigint)
RETURNS TABLE
AS
	RETURN (SELECT * 
	FROM TimeTable 
	WHERE dbo.Group_GetGroup(TagID) = dbo.Group_GetGroup(@TagID)
	)