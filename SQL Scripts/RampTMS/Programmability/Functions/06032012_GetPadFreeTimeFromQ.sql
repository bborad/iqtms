USE [RampTMS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPadFreeTime]    Script Date: 03/06/2012 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Get next arrival for given pad when pad will be free for unload time from given start time
-- =============================================
CREATE FUNCTION [dbo].[GetPadFreeTimeFromQ] 
(
	-- Add the parameters for the function here
	 @PADID INT,
	 @StartTime datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	--DECLARE  @result varchar(500)
	 
	declare @unloading int
	
	if not exists (select * from TimeTable where DestinationPad = @PADID)
		return @StartTime 

/*
--Finding any gap long enough to unload 

	declare @temptable table 
	( DestinationPad int,
	  Pad datetime,
	  PadLeaving datetime)

	insert into @temptable (DestinationPad,Pad,PadLeaving) 
		 select DestinationPad,Pad,PadLeaving from TimeTable where DestinationPad = @PADID order by Pad
	 
		
	set @unloading = (select Unloading from ExpectedTimes)
	select @StartTime = coalesce(case when datediff(ss,@StartTime, Pad) >= @unloading then @StartTime else PadLeaving  end,@StartTime) 
		from @temptable
*/	
--return time last leaves pad
	--set @StartTime = 
	
  --Date: 8-1-2-12 : If there are 4 pad in group and two trucks have gone through pad 1,2 and have exited 
	-- (before expected time) and if 
	--new trucks arrives at REDS, it gets sent to Pad 3 instead of 1 (group order) as 1 & 2 are in timetable
	-- but 3 & 4 are not. To fix it check current state in Tags table to be between REDS-Pad (inclusive)
  --return  (select Top 1 PadLeaving from TimeTable where DestinationPad = @PADID and  order by Pad desc)
			
	return coalesce((select Top 1 PadLeaving 
						from TimeTable tt, Tags t
							 where tt.TagID = T.ID
								 and tt.DestinationPad = @PADID 
									and t.CurrentState in (2,3,4,5,9,10)  
							order by tt.Pad desc),getdate())

	-- Return the result of the function
	--RETURN @StartTime

END







