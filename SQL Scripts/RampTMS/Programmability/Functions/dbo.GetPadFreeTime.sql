set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Get next arrival for given pad when pad will be free for unload time from given start time
-- =============================================
ALTER FUNCTION [dbo].[GetPadFreeTime] 
(
	-- Add the parameters for the function here
	 @PADID INT,
	 @StartTime datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	--DECLARE  @result varchar(500)
	 
	declare @unloading int
	
	if not exists (select * from TimeTable where DestinationPad = @PADID)
		return @StartTime 

/*
--Finding any gap long enough to unload 

	declare @temptable table 
	( DestinationPad int,
	  Pad datetime,
	  PadLeaving datetime)

	insert into @temptable (DestinationPad,Pad,PadLeaving) 
		 select DestinationPad,Pad,PadLeaving from TimeTable where DestinationPad = @PADID order by Pad
	 
		
	set @unloading = (select Unloading from ExpectedTimes)
	select @StartTime = coalesce(case when datediff(ss,@StartTime, Pad) >= @unloading then @StartTime else PadLeaving  end,@StartTime) 
		from @temptable
*/	
--return time last leaves pad
	--set @StartTime = 
	return  (select Top 1 PadLeaving from TimeTable where DestinationPad = @PADID order by Pad desc)
			
	-- Return the result of the function
	--RETURN @StartTime

END







