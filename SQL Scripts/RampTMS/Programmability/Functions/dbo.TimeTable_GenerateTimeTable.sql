set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex Jarkey
-- Create date: 14/11/2011
-- Description:	Function to generate a new timetable or update an existing one
-- =============================================
if exists (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[TimeTable_GenerateTimetable]') AND OBJECTPROPERTY(id, N'IsFunction') = 1)
begin
	DROP FUNCTION [dbo].[TimeTable_GenerateTimetable]
end
GO

--NOTE: this function only works for REDS. Extend to generate a valid time table ANYWHERE
CREATE FUNCTION [dbo].[TimeTable_GenerateTimetable] (@TagID bigint, @NewState varchar, @TagReadTime datetime) 
RETURNS @tempTimeTable table
(
	state varchar(50) PRIMARY KEY NOT NULL,
	arrival datetime
)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	--Temporary variables for expected TRAVEL/WAITING times
	declare @inboundUntarpTime int
	declare @inboundQueueTime int
	declare @queueTime int
	declare @untarpTime int
	declare @redsQueueTime int

	--Temporary variables for expected ARRIVAL times
	declare @reds datetime
	declare @inboundUntarp datetime
	declare @inboundQueue datetime
	declare @untarp datetime
	declare @queue datetime
	declare @unload datetime
	declare @entry1 datetime
	declare @entry2 datetime
	declare @pad datetime
	declare @padLeave datetime
	declare @tw datetime
	declare @reds2 datetime

	declare @destpad int

	set @destpad = 2 -- TODO create EarliestAvailablePad Function

			--Generate Expected Travel Times
		select @redsQueueTime = coalesce(ET.REDS_Queue,0),
							@untarp = coalesce(ET.UnTarpTime,0),
							@unload = coalesce(ET.Unloading,0),
							@reds=@TagReadTime,--REDS
							@entry1=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1,0),@TagReadTime),--Entry1
							@entry2=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1 +ET.Ent1_Ent2,0),@TagReadTime),--Entry2
							@pad=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1 +ET.Ent1_Ent2 + H.TimeFromEnt2,0) ,@TagReadTime),--Pad
							@padLeave=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1 +ET.Ent1_Ent2 + H.TimeFromEnt2+ET.Unloading,0) ,@TagReadTime),--PadLeaving
							@tw=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1 +ET.Ent1_Ent2 + H.TimeFromEnt2+ET.Unloading+H.TimeToTW,0),@TagReadTime),--ExitTW
							@reds2=dateadd(ss,coalesce(ET.REDS_Queue + ET.Queue_Ent1 +ET.Ent1_Ent2 + H.TimeFromEnt2+ET.Unloading+H.TimeToTW +ET.Exit_REDS,0),@TagReadTime )--REDS2
				from Hoppers H,ExpectedTimes ET
				where H.ID = @destPad
				

	--REDS Timetable:
	if(@NewState = 'REDS')
	begin
			--Build a result table
			
		if (select TagType from Tags where ID = @TagID) = 2 --1:AutoTarp, 2:ManualTarp truck
		begin
		--Update arrival times for Untarping state
			set @inboundUntarp = @TagReadTime;
			UPDATE @tempTimeTable SET arrival = @inboundUntarpTime WHERE state = 'inboundUntarpTime'
			set @inboundQueue = NULL;
			UPDATE @tempTimeTable SET arrival = @inboundQueueTime WHERE state = 'inboundQueueTime'
			set @untarp = dateadd(ss,@redsQueueTime,@inboundUntarp);
			UPDATE @tempTimeTable SET arrival = @untarpTime WHERE state = 'untarpTime'
			set @queue = dateadd(ss,@untarpTime,@untarp);
			UPDATE @tempTimeTable SET arrival = @queueTime WHERE state = 'queueTime'
		end
		else
		begin
			set @inboundUntarp = NULL;
			set @untarp =0;
			set @inboundQueue = @TagReadTime;
			set @queue = dateadd(ss,@redsQueueTime,@TagReadTime);
			set @untarp = NULL;
		end
			
		insert into @tempTimeTable (state, arrival) values ('inboundUntarp', @inboundUntarpTime)
		insert into @tempTimeTable (state, arrival) values ('inboundQueue', @inboundQueueTime)
		insert into @tempTimeTable (state, arrival) values ('untarp', @untarp)
		insert into @tempTimeTable (state, arrival) values ('unload', @unload)
		insert into @tempTimeTable (state, arrival) values ('reds', @reds)
		insert into @tempTimeTable (state, arrival) values ('entry1', @entry1)
		insert into @tempTimeTable (state, arrival) values ('entry2', @entry2)
		insert into @tempTimeTable (state, arrival) values ('pad', @pad)
		insert into @tempTimeTable (state, arrival) values ('padLeave', @padLeave)
		insert into @tempTimeTable (state, arrival) values ('tw', @tw)
		insert into @tempTimeTable (state, arrival) values ('reds2', @reds2)
	end;
	--Other States should select the current timetable and modify it
	RETURN;
END;
