SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex Jarkey
-- Create date: 10/11/2011
-- Description:	Function to get the Timetables for Other trucks in the same group
-- =============================================

if exists (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[Group_GetGroup]') AND OBJECTPROPERTY(id, N'IsFunction') = 1)
begin
	DROP FUNCTION [dbo].[Group_GetGroup]
end
GO

CREATE FUNCTION [dbo].[Group_GetGroup] (@TagID bigint)
RETURNS TABLE
AS
RETURN (SELECT Hoppers.GroupID 
	FROM Hoppers 
	JOIN Tags 
	ON Tags.HopperID = Hoppers.ID 
	WHERE Tags.ID = @TagID)
	--Limit 1?
