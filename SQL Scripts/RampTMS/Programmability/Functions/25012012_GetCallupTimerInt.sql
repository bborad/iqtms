set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Get next arrival for given pad when pad will be free for unload time from given start time
-- =============================================
ALTER FUNCTION [dbo].[GetCallupTimerInt] 
(
	-- Add the parameters for the function here
	 @Hopper_ID int 
)
RETURNS int
AS
BEGIN
	
	declare @TimerAdjust int
	declare @readTime datetime

	set @readTime = getdate()
	set @TimerAdjust =0;
	-- Declare the return variable here
	--DECLARE  @result varchar(500)
	 	--timer adjust will be: call the next truck from current pad free time - travel time from Q to Pad.
		
		/*select @TimerAdjust = datediff(ss,@readTime,dateadd(ss,0-dbo.GetTravelTimeToPad(2,DestinationPad),PadLeaving))
			 from TimeTable where TagID = @Tag_ID		*/
		
		--22-12-2011 10:52am
		--Get the pad leave time of the last truck at Entry1 - Q-Pad time will be the next callup. In Above case there might be truck 
		-- called up after this truck which might have different pad leave due to delay.
		-- 5-1-2012 ActiveReads.E1/E2/Pad will never be > @readTime as @readTime is current time.
		/*select Top 1 @TimerAdjust = datediff(ss,@readTime,dateadd(ss,0-dbo.GetTravelTimeToPad(2,@Hopper_ID),TimeTable.PadLeaving))
			from TimeTable 
				inner join ActiveReads
				on TimeTable.TagID = ActiveReads.TagID
			where TimeTable.DestinationPad = @Hopper_ID and 
					((ActiveReads.Entry1 IS NOT NULL and ActiveReads.Entry1 > @readTime)   
						or (ActiveReads.Entry2 IS NOT NULL and ActiveReads.Entry2 > @readTime)
						or (ActiveReads.HopperTime IS NOT NULL and ActiveReads.HopperTime > @readTime))
			order by TimeTable.Pad desc 
			*/
		select Top 1 @TimerAdjust = datediff(ss,@readTime,dateadd(ss,0-dbo.GetTravelTimeToPad(2,@Hopper_ID),TimeTable.PadLeaving))
			from TimeTable 
				inner join Tags
				on TimeTable.TagID = Tags.ID
			where TimeTable.DestinationPad = @Hopper_ID and TimeTable.Pad IS NOT NULL and
					Tags.CurrentState in (3,9,10,4,5)  
			order by TimeTable.Pad desc 

	if @TimerAdjust IS NULL 
		set @TimerAdjust = 0

			/*set @TimerAdjust =datediff(ss,coalesce((select case @Location 
												when 1 then REDS  
												when 3 then Entry1
												when 9 then Entry1
												when 10 then Entry1
												when 4 then Entry2
												when 5 then Pad
												else @readTime end
											from TimeTable where TagID = @Tag_ID),@readTime),
										@readTime)
			*/
		return @TimerAdjust


END











