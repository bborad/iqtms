set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
ALTER PROCEDURE [dbo].[NextTruckToCallup]
	-- Add the parameters for the stored procedure here
	@SkipTagRFID varchar(50),
	@HopperID int,
	@CallupTagRFID varchar(50) output,
	@TimeToWait int output
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @SkipTagID int
	declare @CallupTagID int
	declare @GroupID int
	declare @NewPadTime datetime
	declare @CallupTime datetime
	declare @reassignOther bit
	declare @qTime datetime
	declare @PadFreeTime datetime
	declare @TravelTimeToPad int

	select @SkipTagID = ID from Tags where TagRFID = @SkipTagRFID
	select @GroupID = GroupID from Hoppers where ID = @HopperID;
		
	set @TimeToWait =0;
	set @CallupTagID =0;
	set @reassignOther = 0;
	set @CallupTime = getdate()
	set @TravelTimeToPad = dbo.GetTravelTimeToPad(2,@HopperID);

	if @SkipTagID > 0
		begin
			update TimeTable set Queue = @CallupTime where TagID = @SkipTagID
			update Tags set CurrentState = 2 where ID = @SkipTagID
			update ActiveReads set TruckCallUp = NULL where TagID = @SkipTagID
			set @reassignOther = 1;
		end

	--check if pad will be free by the time truck arrives at pad before calling truck
	-- checking pad leave time is not correct as all the trucks in the queue will be assigned to pad
	-- so the pad leave time also includs trucks in the queue to be finished by that time

	/*set @PadFreeTime = dbo.GetPadFreeTime(@HopperID,@CallupTime)
	if(dateadd(ss, @TravelTimeToPad,@CallupTime) < @PadFreeTime)
		begin
			set @TimeToWait = datediff(ss,dateadd(ss, @TravelTimeToPad,@CallupTime),@PadFreeTime);

		end
	else
		begin

	
		end
    */

			select top 1 @CallupTagID = TagID, @qTime = Queue 
				 from TimeTable
				  where TimeTable.TagID in (select ID From Tags where CurrentState = 2 and HopperID in (select ID from Hoppers where GroupID = @GroupID and Status = 0))
				order by Queue

			if (Select coalesce(DestinationPad,0) from TimeTable where TagID = @CallupTagID) <> @HopperID
				set @reassignOther = 1

				set @NewPadTime = dateadd(ss,dbo.GetTravelTimeToPad(2,@HopperID),@CallupTime)
				update ActiveReads set TruckCallUp = @CallupTime where TagID = @CallupTagID
				update Tags set CurrentState = 15 where ID = @CallupTagID -- set currentstate to 15:InboundEntry
				update TimeTable set DestinationPad = @HopperID, Pad = @NewPadTime where TagID = @CallupTagID
				Execute UpdateTimeTable @CallupTagID,2,@qTime
	

	if @reassignOther = 1
		execute ReassignAllPad @GroupID, ''

	 
	select @CallupTagRFID = TagRFID from Tags Where ID = @CallupTagID

	if @reassignOther = 1
		begin
			select TagRFID  ,  DestinationPad as NewDesPad
				from Tags
				inner join TimeTable
				on Tags.ID = TimeTable.TagID 
		end
		
END






















