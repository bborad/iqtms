set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 6th Aug 2010
-- Description:	To Update Entry OR Queue value in ActiveReads TABLE
-- Modified Date : 1 Feb 2011
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_UpdateEntryORQueue]
	@TagID Bigint, 
	@TagRFID Varchar(50),
	@EntryORQueue int,
	@NewPad int,
	@ExpectedArrival datetime = null
	,@VMSLogging int = 0 Output
	,@VMSUseRandomPortNo int = 0 Output
	,@VMSMinPortNo int = 0 Output
	,@VMSMaxPortNo int = 0 Output
AS
BEGIN
	SET NOCOUNT ON;


	 IF @TagID = 0 
		 SELECT @TagID = ID FROM Tags WHERE TagRFID = @TagRFID

		UPDATE ActiveReads SET EntryORQueue = @EntryORQueue Where TagID = @TagID
		
		if not exists(select * from TimeTable where TagID = @TagID)	
			insert into TimeTable(TagID) values(@TagID)
		
		declare @CurrentState int
		set @CurrentState = 1 --REDS default location

		if @EntryOrQueue = 2 --Queue
			set @CurrentState = 14 -- InboundQueue
		
		--ExpectedArrival might be assigned to min date as cann't assign NULL from TMSService
		if(datediff(d,@ExpectedArrival,getdate()) > 2 or datediff(d,@ExpectedArrival,getdate()) < -2)
			set @ExpectedArrival = NULL

		--Manual tarp truck will not be assigned to any pad yet.
		--They will be assigned to a pad when they are gets added into the queue from Untarp State.
		if (select TagType from Tags where ID = @TagID) = 2 --Manual tarp truck
			begin
				set @ExpectedArrival = NULL
				set @CurrentState = 13 --InboundUntarp
			end

		update Tags set CurrentState = @CurrentState where ID = @TagID

		update TimeTable set DestinationPad = @NewPad, Pad = @ExpectedArrival where TagID = @TagID
		declare @ReadTime datetime
		select @ReadTime = REDS from TimeTable where TagID = @TagID
		Execute UpdateTimeTable @TagID, 1, @ReadTime
			
		Select @VMSLogging = [Value] From Configurations Where ParameterType = 'VMSLogging' 
		Select @VMSUseRandomPortNo = [Value] From Configurations Where ParameterType = 'VMSRandomPort' 
		Select @VMSMinPortNo = [Value] From Configurations Where ParameterType = 'VMSMinPortNo' 
		Select @VMSMaxPortNo = [Value] From Configurations Where ParameterType = 'VMSMaxPortNo' 
 
		EXEC VMSDevices_GetAllByLocation '1' -- Get VMS Settings for REDS Area
        
END

---------------------------------------------------------------------------------------------------------------------------------------





