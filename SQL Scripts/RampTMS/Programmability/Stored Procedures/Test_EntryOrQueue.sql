 	declare @TagRFID varchar(50)
	declare @ReadTime datetime
	declare @Location int
	declare @DesPadID int  
	declare @DesLocation int  
	 
	set @TagRFID ='400002034'
	set @ReadTime = '10/12/2011 6:51:50 PM'
	set @Location =3
	 

	
	if not exists(select * from Tags where TagRFID = @TagRFID)
		begin
			set @DesPadID = 0;
			set @DesLocation = @Location;
			return;
		end
	
	declare @TagID bigint
	declare @GroupID int
	select @TagID = ID from Tags where TagRFID = @TagRFID	
	
	set @GroupID = dbo.Group_GetGroup(@TagID);
	set @DesLocation = 2;
	set @DesPadID =0;

	-- if Manual tarping truck
	-- change to check location as well if need to call from the location other than the REDS
	if @Location = 1 and (Select TagType from Tags where ID = @TagID) = 2
		begin
			Update Tags set CurrentState = 13 where ID = @TagID
			update ActiveReads set EntryORQueue = @DesLocation where TagID = @TagID
			execute UpdateTimeTable @TagID, @Location, @ReadTime
			return;
		end

	--Expected time from any location before/upto Entry 2
	declare @TravelTime int
	set @TravelTime = (select case when @Location < 2 then REDS_Queue else 0 end 
								+ case when @Location < 3 then Queue_Ent1 else 0 end
								+ case when @Location < 4 then Ent1_Ent2 else 0 end
							from ExpectedTimes)

	declare @ResultPadTime datetime
	declare @ExpPadTime datetime
	declare @temptable table
		( PadID int,PTime datetime,GOrder int, ExpectedATime datetime  )

	if @Location != 1
		begin
			-- new truck at E1-2
			--assign null to all truck in queue
			update TimeTable set DestinationPad = NULL
				where TagID in (select Tags.ID from Tags
					 inner join Hoppers
						on Hoppers.ID = Tags.HopperID 
						where Hoppers.GroupID = @GroupID and Tags.CurrentState =2)

		end
 
		insert into @temptable (PadID,PTime,GOrder,ExpectedATime)
				select ID,dbo.getPadFreeTime(ID,@ReadTime),GroupOrder, dateadd(ss,dbo.GetTravelTimeToPad(@Location,ID),@ReadTime) 
					from Hoppers where Status = 0 and GroupID = @GroupID
		 
		
			 select top 1 @ResultPadTime = PTime, @DesPadID = PadID ,@ExpPadTime = ExpectedATime
				from @temptable 
					order by PTime,GOrder 
			  
			
			select top 1 @DesPadID = PadID, @ResultPadTime = PTime,@ExpPadTime = ExpectedATime
			 from @tempTable
				where ExpectedATime >= PTime 
					order by GOrder
	
select 	@DesPadID  , @ResultPadTime ,@ExpPadTime  

			if @ExpPadTime >= @ResultPadTime   
				begin
					set @DesLocation = 3;		
					set  @ResultPadTime = @ExpPadTime
				end
	   
			if @Location = 1
				begin
					update ActiveReads set EntryORQueue = @DesLocation where TagID = @TagID
					--update Tags set CurrentState = case when @DesLocation = 15 then 1 else 14 end where ID = @TagID
				end
			
				
			execute AssignNewPad @TagRFID,@ReadTime,@Location,@DesPadID, @ResultPadTime
		
			if @Location != 1
				begin
					set @DesLocation = 3;
				update TimeTable set Pad = @ResultPadTime where TagID = @tagID
					execute ReassignAllPad @GroupID
				end
			
		
END



















