set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 4th Aug 2010
-- Description:	To GET all the Records from active reads table by HopperID.
-- =============================================

ALTER PROCEDURE [dbo].[ActiveReads_GetAllRecordsByHopperID]

	@HopperID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	ar.ID,
			ar.TagID,
			ar.HopperID,
			ar.REDS,
			ar.Queue,
			ar.Entry1,
			ar.Entry2,
			ar.HopperTime,
			ar.TruckWash,
			ar.REDS2,
			ar.TruckCallUp,
			ar.LastPosition,
			ar.EntryORQueue

		FROM ActiveReads ar
		  inner join TimeTable tt
			on tt.TagID = ar.TagID
			WHERE tt.DestinationPad=@HopperID or ar.FoundAtHopper = @HopperID

END


