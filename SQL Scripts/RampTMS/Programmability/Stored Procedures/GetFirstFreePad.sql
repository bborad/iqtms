set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
ALTER PROCEDURE [dbo].[GetFirstFreePad]
	-- Add the parameters for the stored procedure here
	@TagRFID varchar(50),
	@ReadTime datetime,
	@Location int,
	@ResultPadID int output,
	@ResultPadTime datetime output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Expected time from any location before/upto Entry 2
	declare @TravelTime int
	set @TravelTime = (select case when @Location < 2 then REDS_Queue else 0 end 
								+ case when @Location < 3 then Queue_Ent1 else 0 end
								+ case when @Location < 4 then Ent1_Ent2 else 0 end
							from ExpectedTimes)

	declare @TagID bigint

	declare @temptable table
		( PadID int,PTime datetime,GOrder int )
 
	if exists(select * from Tags where TagRFID = @TagRFID)
		begin
			
			select @TagID = ID from Tags where TagRFID = @TagRFID			
			
			--PreferredTime will be Time tag was read at REDS. Need to add Time from REDS to <each>PAD to get 
			--expected arrival time for each pad and to check if pad will be free at that time for unload time period.
			/*insert into @temptable (PadID,PTime,GOrder)
				select ID,dbo.getPadFreeTime(ID,dateadd(ss,TimeFromEnt2+@TravelTime,@ReadTime)),GroupOrder 
					from Hoppers where Status = 0 and GroupID = dbo.Group_GetGroup(@TagID) 
			*/
			
			--if the travel time to PAD is added into the starttime, it will return same time if there are no trucks in 
			-- Time table for that pad and comparing this with expected arrival time in TMS server results in invalid direction
			-- just because of few ms difference (could be when transferign sql datetime into .Net datetime). 
			insert into @temptable (PadID,PTime,GOrder)
				select ID,dbo.getPadFreeTime(ID,@ReadTime),GroupOrder 
					from Hoppers where Status = 0 and GroupID = dbo.Group_GetGroup(@TagID)


			 select top 1 @ResultPadTime = PTime, @ResultPadID = PadID 
				from @temptable 
					order by PTime,GOrder 
	   
			--Adding tag into TimeTable, UpdateTimeTable only adds new tag if it exist in ActiveReads Table.
			if not exists(select * from TimeTable where TagID = @TagID)
			begin
				Insert into TimeTable (TagID) values (@TagID)
			end
	
			/*Update TimeTable set DestinationPad = @ResultPadID,
									 Pad = @ResultPadTime where TagID = @TagID
			*/
		end    

		--Execute UpdateTimeTable @TagID,@Location,@ReadTime
END










