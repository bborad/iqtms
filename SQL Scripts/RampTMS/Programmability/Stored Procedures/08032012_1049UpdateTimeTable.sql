set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UpdateTimeTable]
	-- Add the parameters for the stored procedure here
	@TagID bigint,
	@Location int,
	@TagReadTime datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		declare @Untarp int
		declare @Unload int
		declare @REDS_Queue int
		declare @Queue_Ent1 int
		declare @Ent1_Ent2 int
		declare @Ent2_Pad int
		declare @Pad_TW int
		declare @TW_REDS int
		declare @GroupID int

		set @GroupID = dbo.Group_GetGroup(@TagID);
	
		--Destination pad might be null	
		/*select @Untarp = UnTarpTime, @Unload = Unloading, @REDS_Queue = REDS_Queue, @Queue_Ent1 = Queue_Ent1,@Ent1_Ent2 = Ent1_Ent2,@Ent2_Pad = coalesce(TimeFromEnt2,0),
				@Pad_TW = coalesce(TimeToTW,0), @TW_REDS = TruckWash_REDS 
			from ExpectedTimes, Hoppers
				inner join TimeTable
				on DestinationPad = Hoppers.ID
			where TimeTable.TagID = @TagID*/

			declare @padfreetime datetime
			declare @currentpad int


			select @Untarp = UnTarpTime, @Unload = Unloading, @REDS_Queue = REDS_Queue, @Queue_Ent1 = Queue_Ent1,@Ent1_Ent2 = Ent1_Ent2,
				 @TW_REDS = TruckWash_REDS 
				from ExpectedTimes 

			select @Ent2_Pad = coalesce(TimeFromEnt2,0),	@Pad_TW = coalesce(TimeToTW,0)
				from Hoppers
					inner join TimeTable
					on DestinationPad = Hoppers.ID
				where TimeTable.TagID = @TagID


	
	if not exists(select * from TimeTable where TagID = @TagID)
		begin
			if not exists(select * from ActiveReads where TagID= @TagID)
				return;
			insert into TimeTable (TagID) values(@TagID)
			if @Location = 5
				begin
					Update TimeTable set Pad = @TagReadTime, PadLeaving = dateadd(ss,@Unload,@TagReadTime),DestinationPad = 
						coalesce((select FoundAtHopper from ActiveReads where TagID = @TagID),coalesce(DestinationPad,0)) 
							where TagID = @TagID
						Execute ReassignAllPad @GroupID,''
					 return;
				end
		end
		
    if (select TagType from Tags where ID = @TagID) = 1
		set @Untarp = 0;



	--Have to check for each location and update expected time after that location.
	if @Location = 1 or @Location = 13 or @Location = 14 --REDS, 13,14 Inbount-Untarping/Queue is same as REDS time.
		begin
			Update TimeTable set REDS = @TagReadTime, InboundUntarping =case when @Untarp = 0 then NULL else @TagReadTime end, 
					InboundQueue = case when @Untarp = 0 then @TagReadTime else 0 end, Untarping = case when @Untarp =0 then 0 else dateadd(ss,@REDS_Queue,@TagReadTime) end,
					Queue = dateadd(ss,@REDS_Queue+@Untarp,@TagReadTime),Entry1 = dateadd(ss,0-(@Ent2_Pad+@Ent1_Ent2),Pad), Entry2 = dateadd(ss,0-@Ent2_Pad,Pad),
					PadLeaving = dateadd(ss,@Unload,Pad),ExitTW = dateadd(ss,@Pad_TW+@Unload,Pad),REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,Pad)
				where TagID = @TagID
		end
	else if @Location = 2 --Queue
		begin
			Update TimeTable set Queue = @TagReadTime, Entry1 = dateadd(ss,0-(@Ent2_Pad+@Ent1_Ent2),Pad), Entry2 = dateadd(ss,0-@Ent2_Pad,Pad),
					PadLeaving = dateadd(ss,@Unload,Pad),ExitTW = dateadd(ss,@Pad_TW+@Unload,Pad),REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,Pad)
				where TagID = @TagID
		end
	else if @Location = 3 or @Location = 9 or @Location = 10 -- Entry1, 9/10 = Entry Lane1/2
		begin
			--if (Select REDS from TimeTable where TagID = @TagID) is NULL 
			--	begin
					-- do not update the pad time as truck might arrive at E1 even if it isn't called up.
					-- but need to update if there is no other truck before this truck as it can arrive early at PAD else can't and have wait
					-- at PAD.
					-- but if truck is delay update the pad time. pad will take longer to be free
				
					-- get pad leave time for the truck ahead of this truck for same pad. can be 1+ truck
					--declare @padfreetime datetime
					--declare @currentpad int
					select Top 1 @currentpad = DestinationPad from TimeTable where TagID = @TagID
					select top 1 @padfreetime = PadLeaving from TimeTable where DestinationPad = @currentpad 
						and Entry1 IS NOT NULL and Entry1 < @TagReadTime and  TagID <> @TagID
							order by Entry1 desc
					 
						 if @padfreetime IS NULL or @padfreetime < dateadd(ss,@Ent1_Ent2+@Ent2_Pad,@TagReadTime)
								set @padfreetime = dateadd(ss,@Ent1_Ent2+@Ent2_Pad,@TagReadTime)
						 
						 
							Update TimeTable set Entry1 = @TagReadTime, 
									Entry2 = dateadd(ss,@Ent1_Ent2,@TagReadTime),
									Pad = @padfreetime,
									PadLeaving = dateadd(ss,@Unload,@padfreetime),
									ExitTW = dateadd(ss,@Pad_TW+@Unload,@padfreetime),
									REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,@padfreetime)
								where TagID = @TagID
						 
			--	end
			/*else
				begin
					Update TimeTable set Entry1 = @TagReadTime, Entry2 = dateadd(ss,@Ent1_Ent2,@TagReadTime),
							Pad = dateadd(ss,@Ent1_Ent2+@Ent2_Pad,@TagReadTime),
							PadLeaving = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload,@TagReadTime),
							ExitTW = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload+@Pad_TW,@TagReadTime),
							REDS2 = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload+@Pad_TW+@TW_REDS,@TagReadTime)
						where TagID = @TagID
				end*/
		end
	else if @Location = 4 --Entry2
		begin
			-- do not update the pad time as truck might arrive at E1 even if it isn't called up.
			-- but need to update if there is not other truck before this truck as it can arrive early at PAD else can't and have wait
			-- at PAD.
			-- but if truck is delay update the pad time. pad will take longer to be free
		
			-- get pad leave time for the truck ahead of this truck for same pad. can be 1+ truck
			
			select Top 1 @currentpad = DestinationPad from TimeTable where TagID = @TagID
			select top 1 @padfreetime = PadLeaving from TimeTable where DestinationPad = @currentpad
				and Entry2 IS NOT NULL and Entry2 < @TagReadTime and  TagID <> @TagID
				order by Entry2 desc

				
						if @padfreetime IS NULL or @padfreetime < dateadd(ss,@Ent2_Pad,@TagReadTime)
							set @padfreetime = dateadd(ss,@Ent2_Pad,@TagReadTime)
					
				 Update TimeTable set Entry2 = @TagReadTime,
							Pad = @padfreetime,
							PadLeaving = dateadd(ss,@Unload,@padfreetime),
							ExitTW = dateadd(ss,@Pad_TW+@Unload,@padfreetime),
							REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,@padfreetime)
						where TagID = @TagID

		end
	else if @Location = 5 --PAD
		begin
			declare @desPad int
			select @desPad = DestinationPad from TimeTable where TagID = @TagID
			if (select FoundAtHopper from ActiveReads where TagID = @TagID) = @desPad
			begin

				Update TimeTable set Pad = @TagReadTime,
						PadLeaving = dateadd(ss,@Unload,@TagReadTime),
						ExitTW = dateadd(ss,@Unload+@Pad_TW,@TagReadTime),
						REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,@TagReadTime)
					where TagID = @TagID
			end
		end
	else if @Location = 6 or @Location =7 --TW/ExitGate
		begin
			Update TimeTable set ExitTW =  @TagReadTime,REDS2 = dateadd(ss,@TW_REDS,@TagReadTime)
				where TagID = @TagID
		end
	
END



















