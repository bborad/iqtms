set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 16/11/2010
-- Description:	Update Hopper Status by ID
-- =============================================
ALTER PROCEDURE [dbo].[Hoppers_UpdateHopperStatus]
	-- Add the parameters for the stored procedure here
	  @HopperStatus varchar(100)      
AS
BEGIN
	-- Insert statements for procedure here

	 
	declare @pos int 
	declare @pad int

	set @pos = charindex(',',@HopperStatus);
	set @pad =1;
	while(@pos <> 0)
	begin
		
		update Hoppers set Status = substring(@HopperStatus,1,@pos-1) where ID = @pad;
		set @HopperStatus = right(@HopperStatus,len(@HopperStatus)-@pos)
		set @pos = charindex(',',@HopperStatus);
		set @pad = @pad+1;
	end
		 

	
/*declare @teststring varchar(100)
set @teststring = 'P1,0,P2,0,P3,0,P4,1,P5,1,P6,2,P7,2,P8,3,P9,3,P10,10,P11,11,P12,12,P13,13,'

update Hoppers set Status = case ID
		when 1 then substring(@teststring,charindex('P1',@teststring)+3,charindex('P2',@teststring)-(charindex('P1',@teststring)+4))
		when 2 then substring(@teststring,charindex('P2',@teststring)+3,charindex('P3',@teststring)-(charindex('P2',@teststring)+4))
		when 3 then substring(@teststring,charindex('P3',@teststring)+3,charindex('P4',@teststring)-(charindex('P3',@teststring)+4))
		when 4 then substring(@teststring,charindex('P4',@teststring)+3,charindex('P5',@teststring)-(charindex('P4',@teststring)+4))
		when 5 then substring(@teststring,charindex('P5',@teststring)+3,charindex('P6',@teststring)-(charindex('P5',@teststring)+4))
		when 6 then substring(@teststring,charindex('P6',@teststring)+3,charindex('P7',@teststring)-(charindex('P6',@teststring)+4))
		when 7 then substring(@teststring,charindex('P7',@teststring)+3,charindex('P8',@teststring)-(charindex('P7',@teststring)+4))
		when 8 then substring(@teststring,charindex('P8',@teststring)+3,charindex('P9',@teststring)-(charindex('P8',@teststring)+4))
		when 9 then substring(@teststring,charindex('P9',@teststring)+3,charindex('P10',@teststring)-(charindex('P9',@teststring)+4))
		when 10 then substring(@teststring,charindex('P10',@teststring)+4,charindex('P11',@teststring)-(charindex('P10',@teststring)+5))
		when 11 then substring(@teststring,charindex('P11',@teststring)+4,charindex('P12',@teststring)-(charindex('P11',@teststring)+5))
		when 12 then substring(@teststring,charindex('P12',@teststring)+4,charindex('P13',@teststring)-(charindex('P12',@teststring)+5))
		when 13 then substring(@teststring,charindex('P13',@teststring)+4,len(@teststring)-(charindex('P13',@teststring)+4))
		end*/
	
END



