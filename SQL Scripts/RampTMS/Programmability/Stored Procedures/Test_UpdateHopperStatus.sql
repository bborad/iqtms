declare @teststring varchar(50)
set @teststring = '1,1,1,1,1,1,1,1,1,1,1,1,'
declare @pos int 
declare @pad int

set @pos = charindex(',',@teststring);

while(@pos <> 0)
begin
	
	set @pad = substring(@teststring,1,@pos-1)
	 
	set @teststring = right(@teststring,len(@teststring)-@pos)
	set @pos = charindex(',',@teststring);
end

execute Hoppers_UpdateHopperStatus '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,'

declare @teststring varchar(100)
set @teststring = 'P1,0,P2,0,P3,0,P4,1,P5,1,P6,2,P7,2,P8,3,P9,3,P10,10,P11,11,P12,12,P13,13,'

update Hoppers set Status = case ID
		when 1 then substring(@teststring,charindex('P1',@teststring)+3,charindex('P2',@teststring)-(charindex('P1',@teststring)+4))
		when 2 then substring(@teststring,charindex('P2',@teststring)+3,charindex('P3',@teststring)-(charindex('P2',@teststring)+4))
		when 3 then substring(@teststring,charindex('P3',@teststring)+3,charindex('P4',@teststring)-(charindex('P3',@teststring)+4))
		when 4 then substring(@teststring,charindex('P4',@teststring)+3,charindex('P5',@teststring)-(charindex('P4',@teststring)+4))
		when 5 then substring(@teststring,charindex('P5',@teststring)+3,charindex('P6',@teststring)-(charindex('P5',@teststring)+4))
		when 6 then substring(@teststring,charindex('P6',@teststring)+3,charindex('P7',@teststring)-(charindex('P6',@teststring)+4))
		when 7 then substring(@teststring,charindex('P7',@teststring)+3,charindex('P8',@teststring)-(charindex('P7',@teststring)+4))
		when 8 then substring(@teststring,charindex('P8',@teststring)+3,charindex('P9',@teststring)-(charindex('P8',@teststring)+4))
		when 9 then substring(@teststring,charindex('P9',@teststring)+3,charindex('P10',@teststring)-(charindex('P9',@teststring)+4))
		when 10 then substring(@teststring,charindex('P10',@teststring)+4,charindex('P11',@teststring)-(charindex('P10',@teststring)+5))
		when 11 then substring(@teststring,charindex('P11',@teststring)+4,charindex('P12',@teststring)-(charindex('P11',@teststring)+5))
		when 12 then substring(@teststring,charindex('P12',@teststring)+4,charindex('P13',@teststring)-(charindex('P12',@teststring)+5))
		when 13 then substring(@teststring,charindex('P13',@teststring)+4,len(@teststring)-(charindex('P13',@teststring)+4))
		end