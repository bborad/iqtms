set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex Jarkey
-- Create date: 10/11/2011
-- Description:	Queue Logic
-- =============================================
ALTER PROCEDURE [dbo].[REDSLogic]
	-- Add the parameters for the stored procedure here
	@TagID bigint,
	@TagReadTime datetime
	@DestPad
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		declare tempTimeTable
		
		@tempTimeTable = CALL GenerateTimeTable(@TagReadTime, @DestPad)
				
		if (select TagType from Tags where ID = @TagID) = 2 --1:AutoTarp, 2:ManualTarp truck
		begin
		--Update arrival times for Untarping state
			set @inboundUntarpTime = @TagReadTime;
			UPDATE @tempTimeTable SET arrival = @inboundUntarpTime WHERE status = inboundUntarpTime
			set @inboundUntarpTime = NULL;
			UPDATE @tempTimeTable SET arrival = @inboundQueueTime WHERE status = inboundQueueTime
			set @untarpTime = dateadd(ss,@redsQueue,@inboundUntarpTime);
			UPDATE @tempTimeTable SET arrival = @untarpTime WHERE status = untarpTime
			set @queueTime = dateadd(ss,@untarp,@untarpTime);
			UPDATE @tempTimeTable SET arrival = @queueTime WHERE status = queueTime
		end
		else
		begin
			set @inboundUntarpTime = NULL;
			set @untarp =0;
			set @inboundQueueTime = @TagReadTime;
			set @queueTime = dateadd(ss,@redsQueue,@TagReadTime);
			set @untarpTime = NULL;
		end

	if Exists(select TagID from ActiveReads where TagID = @TagID and datediff(hh,StartTime,getdate()) <= 5)
		begin
			select @reds = coalesce(REDS,@reds),@queueTime = coalesce(Queue,@queueTime),@entry1 = coalesce(Entry1,@entry1),
					@entry2 = coalesce(Entry2,@entry2),@pad = coalesce(HopperTime,@pad),@padLeave = dateadd(ss,@unload,@pad),
					@tw = coalesce(TruckWash,coalesce(ExitTime,@tw))
			from ActiveReads where TagID = @TagID
		end
	
	if not exists(Select TagID from TimeTable where TagID = @TagID)
		begin
			Insert into TimeTable(TagID,DestinationPad,
												REDS,
												InboundUntarping,
												InboundQueue,
												Untarping,
												Queue,
												Entry1,
												Entry2,
												Pad,
												PadLeaving,
												ExitTW,
												REDS2)
							values( @TagID,@Dest_PAD,
									@TagReadTime,--REDS
									@inboundUntarpTime,--inboundUntarping
									@inboundQueueTime,--inboundQueue
									@untarpTime,--Untarping
									@queueTime,--Queue
									dateadd(ss,@untarp,@entry1),--Entry1
									dateadd(ss,@untarp,@entry2),--Entry2
									dateadd(ss,@untarp,@pad),--Pad
									dateadd(ss,@untarp,@padLeave),--PadLeaving
									dateadd(ss,@untarp,@tw),--ExitTW
									dateadd(ss,@untarp,@reds2))--REDS2
									

		end
else 
		begin

			update TimeTable set DestinationPad = @Dest_PAD,
												REDS =@TagReadTime,
												InboundUntarping=@inboundUntarpTime,
												InboundQueue=@inboundQueueTime,
												Untarping=@untarpTime,
												Queue=@queueTime,
												Entry1=dateadd(ss,@untarp,@entry1),
												Entry2=dateadd(ss,@untarp,@entry2),
												Pad=dateadd(ss,@untarp,@pad),
												PadLeaving=dateadd(ss,@untarp,@padLeave),
												ExitTW=dateadd(ss,@untarp,@tw),
												REDS2=dateadd(ss,@untarp,@reds2)
							where TagID = @TagID
									
		end
	
END






