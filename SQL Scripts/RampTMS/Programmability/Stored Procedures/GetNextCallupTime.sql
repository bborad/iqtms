USE [RampTMS]
GO
/****** Object:  StoredProcedure [dbo].[NextTruckToCallup]    Script Date: 12/22/2011 11:57:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
CREATE PROCEDURE [dbo].[GetNextCallupTime]
	-- Add the parameters for the stored procedure here
	
	@HopperID int,
	@CallupTime int output
	
AS
BEGIN
	 
select @CallupTime = dbo.GetCallupTimerInt(@HopperID)


END



















