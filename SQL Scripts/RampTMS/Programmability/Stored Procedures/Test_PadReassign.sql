/*declare @pid int
declare @ptime datetime
declare @groupid int

declare padfreetime cursor for
select ID as PID,dbo.GetPadFreeTime(ID,getdate()) as PTime,GroupOrder
	from Hoppers where Status = 0 and GroupID = 2
		order by PTime,GroupOrder

open padfreetime;

fetch next  from  padfreetime
into @pid, @ptime,@groupid;

while @@FETCH_STATUS = 0
begin
	select @pid, @ptime,@groupid
	
	fetch next from  padfreetime
	into @pid, @ptime,@groupid;
	
end
close padfreetime
deallocate padfreetime
*/
select * from TimeTable
update TimeTable set DestinationPad = 9,Pad = dateadd(ss,dbo.GetTravelTimeToPad(2,2),getdate())
				where TagID = (
								select Top 1 TagID from TimeTable
									inner join Tags
									on Tags.ID = TimeTable.TagID
										inner join Hoppers
										on Hoppers.ID = Tags.HopperID
								where Hoppers.GroupID = 2 
									and Tags.CurrentState = 2 
									and TimeTable.DestinationPad is NULL 
									and TimeTable.Queue is not NULL
								order by TimeTable.Queue 
							  )
select * from TimeTable