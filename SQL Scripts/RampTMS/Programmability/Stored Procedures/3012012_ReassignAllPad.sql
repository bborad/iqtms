set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
ALTER PROCEDURE [dbo].[ReassignAllPad]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@SkipTagID varchar(50) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		-- assign null to all truck's destination pad - trucks that are in queue - withing same group 

Declare @SkipID	int


		if (select ID from Tags where TagRFID = @SkipTagID) > 0
			begin
				select @SkipID = ID from Tags where TagRFID = @SkipTagID
				update TimeTable set Queue = getdate() where TagID = @SkipID
				--update TimeTable set Queue = @CallupTime where TagID = @SkipTagID
				update Tags set CurrentState = 2 where ID = @SkipID
				update ActiveReads set TruckCallUp = NULL where TagID = @SkipID
			end
	
		update TimeTable set DestinationPad = NULL
			where TagID in (
							select Tags.ID from Tags 
								inner join Hoppers
								on Hoppers.ID = Tags.HopperID
									where Hoppers.GroupID = @GroupID and Tags.CurrentState = 2
							)


		-- reassing all truck
		declare @pid int
		declare @ptime datetime
		declare @tagToAssign bigint
		declare @qTime datetime
		
		declare @orderid int
		declare @unload int
		declare @increment int
		declare @currentTime datetime
		declare @tagToUpdate bigint
		declare @TimeToPad datetime
		declare @queueTime datetime		

		set @currentTime = getdate()
		set @unload = (select Unloading from ExpectedTimes)	
		set @increment = 0;	

		--get all the pad in a virtual table in order of free/available time and then GroupOrder
		if cursor_status('global','padFreeTime') > -1
		begin
			close padfreetime
			deallocate padfreetime
		end
		declare padfreetime scroll cursor for
				select ID as PID,dbo.GetPadFreeTime(ID,@currentTime) as PTime,
					dateadd(ss,dbo.GetTravelTimeToPad(2,ID),@currentTime) as TimeToPad,GroupOrder
					from Hoppers where Status = 0 and GroupID = @GroupID
						order by PTime,GroupOrder

		--get all the trucks to re-assign
		if cursor_status('global','trucksToAssign') > -1
		begin
			close trucksToAssign
			deallocate trucksToAssign
		end
		declare trucksToAssign cursor for
			select TagID as tagToAssign, Queue as qTime from TimeTable
									inner join Tags
									on Tags.ID = TimeTable.TagID
										inner join Hoppers
										on Hoppers.ID = Tags.HopperID
								where Hoppers.GroupID = @GroupID
									and Tags.CurrentState = 2 
									and TimeTable.DestinationPad is NULL 
									and TimeTable.Queue is not NULL
								order by TimeTable.Queue 
		

		--Fetch from top and assign that pad to a truck 
		--		(get a truck in order they arrived in queue : truck must have DesPad = NULL, CurrentState = 2 Queue != NULL
		open trucksToAssign
		open padFreeTime

		fetch next  from  trucksToAssign
		into @tagToAssign,@qTime;

		while @@FETCH_STATUS = 0
		begin
			 
			fetch next from padFreeTime
			into @pid, @ptime,@TimeToPad,@orderid;
			
			if @@FETCH_STATUS <> 0
				begin
					fetch first from padFreeTime
					into @pid, @ptime,@TimeToPad,@orderid;
					if @@FETCH_STATUS = 0
						set @increment = @increment + @unload;
				end
			
				
			/* update TimeTable with new pad and pad time 
			*/
			update TimeTable set DestinationPad = @pid,Pad = (case when @TimeToPad > @ptime then @TimeToPad else @ptime end)+@increment 
				where TagID = @tagToAssign

			/* update arrival time for all other state
			*/
			execute UpdateTimeTable @tagToAssign,2,@qTime
			
			/*fetch next available pad
			*/
			fetch next from  trucksToAssign
			into @tagToAssign,@qTime;
			
		end
		close padfreetime
		deallocate padfreetime

		close trucksToAssign
		deallocate trucksToAssign


END















