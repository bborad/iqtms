set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
ALTER PROCEDURE [dbo].[GetNextCallupTime]
	-- Add the parameters for the stored procedure here
	
	@HopperID int,
	@CallupTime int output
	
AS
BEGIN

	Begin TRY

		declare @tid int
		declare @padfreetime datetime
		declare @unloadtime int

		select top 1 @unloadtime = Unloading from ExpectedTimes 
		select top 1 @padfreetime = Pad from TimeTable where DestinationPad = @HopperID order by Pad

		if cursor_status('global','tagsToUpdate') > -1
			begin
				close tagsToUpdate
				deallocate tagsToUpdate
			end

		declare tagsToUpdate cursor for
			select TimeTable.TagID as tid from TimeTable
								inner join Tags
								on Tags.ID = TimeTable.TagID
								inner join ActiveReads
								on ActiveReads.TagID = TimeTable.TagID
							where (Tags.CurrentState in (3,4,5,15) or (Tags.CurrentState = 1 and ActiveReads.EntryOrQueue = 3))
								and (TimeTable.DestinationPad = @HopperID)
								and (TimeTable.Pad is not NULL)
							order by TimeTable.Pad 
			
		open tagsToUpdate
			 
		fetch next  from  tagsToUpdate
			into @tid ;
			
		while @@FETCH_STATUS = 0
			begin
				 
				update TimeTable set Pad = @padfreetime, PadLeaving = dateadd(ss,@unloadtime,@padfreetime) where TagID = @tid
				set @padfreetime = dateadd(ss,@unloadtime,@padfreetime)

				fetch next  from  tagsToUpdate
				into @tid ;

			end
		close tagsToUpdate
		deallocate tagsToUpdate
	END Try
	Begin catch

	end catch

	select @CallupTime = dbo.GetCallupTimerInt(@HopperID)


END























