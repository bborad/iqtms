USE [RampTMS]
GO
/****** Object:  StoredProcedure [dbo].[GetFirstFreePad]    Script Date: 11/18/2011 15:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
CREATE PROCEDURE [dbo].[AssignNewPad]
	-- Add the parameters for the stored procedure here
	@TagRFID varchar(50),
	@ReadTime datetime,
	@Location int,
	@NewPadID int,
	@NewPadTime datetime 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @tagID bigint	

	Select @tagID = ID From Tags where TagRFID = @TagRFID

	if @tagID > 0
		begin
			
			 if not exists(select * from TimeTable where TagID = @tagID)
				begin
					insert into TimeTable (TagID) values (@tagID)
				end
	
			update TimeTable set DestinationPad = @NewPadID, Pad = @NewPadTime where TagID = @tagID

			execute UpdateTimeTable @tagID,@Location,@ReadTime

		end --end of if tag exist in tag table
 
END










