set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhavesh
-- Create date: 15-11-2011
-- Description:	Will return time and id of the first available Pad
-- =============================================
ALTER PROCEDURE [dbo].[EntryOrQueue]
	-- Add the parameters for the stored procedure here
	@TagRFID varchar(50),
	@ReadTime datetime,
	@Location int,
	@DesPadID int output,
	@DesLocation int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if not exists(select * from Tags where TagRFID = @TagRFID)
		begin
			set @DesPadID = 0;
			set @DesLocation = @Location;
			return;
		end
	
	declare @TagID bigint
	declare @PrecisionValue int
	select @TagID = ID from Tags where TagRFID = @TagRFID	
	select top 1 @PrecisionValue = [Precision] from ExpectedTimes

	set @DesLocation = 2;
	set @DesPadID =0;

	declare @ResultPadTime datetime
	declare @ExpPadTime datetime

	-- if Manual tarping truck
	-- change to check location as well if need to call from the location other than the REDS
	if @Location = 1 and (Select TagType from Tags where ID = @TagID) = 2
		begin
			Update Tags set CurrentState = 13 where ID = @TagID
			update ActiveReads set EntryORQueue = @DesLocation where TagID = @TagID
			execute UpdateTimeTable @TagID, @Location, @ReadTime
			if (select count(*) from Hoppers where GroupID =  dbo.Group_GetGroup(@TagID)) < 2
				begin
					select @DesPadID = HopperID where ID = @TagID
					execute AssignNewPad @TagRFID,@ReadTime,@Location,@DesPadID, @ResultPadTime
				end
			return;
		end

	--Expected time from any location before/upto Entry 2
	declare @TravelTime int
	set @TravelTime = (select case when @Location < 2 then REDS_Queue else 0 end 
								+ case when @Location < 3 then Queue_Ent1 else 0 end
								+ case when @Location < 4 then Ent1_Ent2 else 0 end
							from ExpectedTimes)

	
	declare @temptable table
		( PadID int,PTime datetime,GOrder int, ExpectedATime datetime )
 
		insert into @temptable (PadID,PTime,GOrder,ExpectedATime)
				select ID,dbo.getPadFreeTime(ID,@ReadTime),GroupOrder, dateadd(ss,dbo.GetTravelTimeToPad(@Location,ID),@ReadTime)
					from Hoppers where Status = 0 and GroupID = dbo.Group_GetGroup(@TagID)
	 

			 select top 1 @ResultPadTime = PTime, @DesPadID = PadID ,@ExpPadTime = ExpectedATime
				from @temptable 
					order by PTime,GOrder 
			  
			
			select top 1 @DesPadID = PadID, @ResultPadTime = PTime,@ExpPadTime = ExpectedATime
			 from @tempTable
				where datediff(ss,ExpectedATime, PTime) <= @PrecisionValue
					order by GOrder
			
	   
		
			if  datediff(ss,@ExpPadTime,@ResultPadTime) <= @PrecisionValue   
				begin
					set @DesLocation = 3;
					
					set  @ResultPadTime = @ExpPadTime
				end
			
			

			-- if calling from location other than REDS
			if @Location = 1
				begin
					update ActiveReads set EntryORQueue = @DesLocation where TagID = @TagID
					--update Tags set CurrentState = case when @DesLocation = 15 then 1 else 14 end where ID = @TagID
				end
			
				
			execute AssignNewPad @TagRFID,@ReadTime,@Location,@DesPadID, @ResultPadTime
			
		
END


















