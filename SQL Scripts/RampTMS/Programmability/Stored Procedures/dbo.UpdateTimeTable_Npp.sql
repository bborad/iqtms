set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UpdateTimeTable]
	-- Add the parameters for the stored procedure here
	@TagID bigint,
	@Location int,
	@TagReadTime datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		declare @Untarp int
		declare @Unload int
		declare @REDS_Queue int
		declare @Queue_Ent1 int
		declare @Ent1_Ent2 int
		declare @Ent2_Pad int
		declare @Pad_TW int
		declare @TW_REDS int
		
		select @Untarp = UnTarpTime, @Unload = Unloading, @REDS_Queue = REDS_Queue, @Queue_Ent1 = Queue_Ent1,@Ent1_Ent2 = Ent1_Ent2,@Ent2_Pad = coalesce(TimeFromEnt2,0),
				@Pad_TW = coalesce(TimeToTW,0), @TW_REDS = TruckWash_REDS 
			from ExpectedTimes, Hoppers
				inner join TimeTable
				on DestinationPad = Hoppers.ID
			where TimeTable.TagID = @TagID
	
	if not exists(select * from TimeTable where TagID = @TagID)
		begin
			insert into TimeTable (TagID) values(@TagID)
		end
		
	--Have to check for each location and update expected time after that location.
	if @Location = 1 or @Location = 13 or @Location = 14 --REDS, 13,14 Inbount-Untarping/Queue is same as REDS time.
		begin
			Update TimeTable set REDS = @TagReadTime, InboundUntarping = @TagReadTime, InboundQueue = @TagReadTime, Untarping = dateadd(ss,@REDS_Queue,@TagReadTime),
					Queue = dateadd(ss,@REDS_Queue,@TagReadTime),Entry1 = dateadd(ss,0-(@Ent2_Pad+@Ent1_Ent2),Pad), Entry2 = dateadd(ss,0-@Ent2_Pad,Pad),
					PadLeaving = dateadd(ss,@Unload,Pad),ExitTW = dateadd(ss,@Pad_TW+@Unload,Pad),REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,Pad)
				where TagID = @TagID
		end
	else if @Location = 2 --Queue
		begin
			Update TimeTable set Queue = @TagReadTime, Entry1 = dateadd(ss,0-(@Ent2_Pad+@Ent1_Ent2),Pad), Entry2 = dateadd(ss,0-@Ent2_Pad,Pad),
					PadLeaving = dateadd(ss,@Unload,Pad),ExitTW = dateadd(ss,@Pad_TW+@Unload,Pad),REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,Pad)
				where TagID = @TagID
		end
	else if @Location = 3 or @Location = 9 or @Location = 10 -- Entry1, 9/10 = Entry Lane1/2
		begin
			Update TimeTable set Entry1 = @TagReadTime, Entry2 = dateadd(ss,@Ent1_Ent2,@TagReadTime),Pad = dateadd(ss,@Ent1_Ent2+@Ent2_Pad,@TagReadTime),
					PadLeaving = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload,@TagReadTime),ExitTW = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload+@Pad_TW,@TagReadTime),
					REDS2 = dateadd(ss,@Ent1_Ent2+@Ent2_Pad+@Unload+@Pad_TW+@TW_REDS,@TagReadTime)
				where TagID = @TagID
		end
	else if @Location = 4 --Entry2
		begin
			Update TimeTable set Entry2 = @TagReadTime,Pad = dateadd(ss,@Ent2_Pad,@TagReadTime),
					PadLeaving = dateadd(ss,@Ent2_Pad+@Unload,@TagReadTime),ExitTW = dateadd(ss,@Ent2_Pad+@Unload+@Pad_TW,@TagReadTime),
					REDS2 = dateadd(ss,@Ent2_Pad+@Unload+@Pad_TW+@TW_REDS,@TagReadTime)
				where TagID = @TagID
		end
	else if @Location = 5 --PAD
		begin
			Update TimeTable set Pad = @TagReadTime,
					PadLeaving = dateadd(ss,@Unload,@TagReadTime),ExitTW = dateadd(ss,@Unload+@Pad_TW,@TagReadTime),
					REDS2 = dateadd(ss,@Unload+@Pad_TW+@TW_REDS,@TagReadTime)
				where TagID = @TagID
		end
	else if @Location = 6 or @Location =7 --TW/ExitGate
		begin
			Update TimeTable set ExitTW =  @TagReadTime,REDS2 = dateadd(ss,@TW_REDS,@TagReadTime)
				where TagID = @TagID
		end
	
END







