set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 12 Aug 2010
-- Description:	To Open/Close a Traffic Transaction
---------------------- Modified --------------------
-- Date:31-10-2011
-- By: Bhavesh
-- Note: Modify to handle ManualTarp tags
-- =============================================
ALTER PROCEDURE [dbo].[ActiveReads_OpenCloseTransaction]

	@TagRFID nvarchar(100),
	@Location int,
	@Option int,
	@NewLoopID Varchar(10) = null,
	@OldLoopID Varchar(10) = null,
	@TransStatus INT = NULL,
	@TransMessage Varchar(200) = '',
	@TagBatteryStatus Varchar(50) = null
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tag_ID INT;
	DECLARE @Proponent_ID INT;
	DECLARE @Hopper_ID INT,@FoundAtHopper INT;
	DECLARE @Truck_ID Varchar(10);
	DECLARE @Proponent_Name nvarchar(150);
	DECLARE @Hopper_Name nvarchar(100);
	DECLARE @MarkerID INT;
	DECLARE @TimerAdjust int;
	Declare @Result INT;

 
	declare @TagType int
	
	SET @MarkerID = 0;
	set @TimerAdjust = 0;
--	IF @NewLoopID IS NOT NULL
--		BEGIN
--			IF EXISTS(SELECT ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
--				SET  @MarkerID = (SELECT ID FROM Markers WHERE NewLoopID = @NewLoopID)
--			ELSE
--		END

	SELECT	@Tag_ID=ID, 
			@Proponent_ID=ProponentID, 
			@Hopper_ID=HopperID, 
			@Truck_ID=TruckID,
			@TagType = coalesce(TagType,1) 
		FROM Tags 
			WHERE TagRFID=@TagRFID

	IF ((@Tag_ID IS NULL) OR (@Hopper_ID IS NULL))
	BEGIN
		SELECT	ISNULL(@Tag_ID,0) as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			'00' as HopperName,
			0 as TimeFromEnt2,
			0 as TimeToTW,
			0 as MarkerID,	
			1 as TagType,
			@TimerAdjust as TimerAdjust	
		RETURN;
	END

	if @Location > 0
		Update Tags set CurrentState = @Location where TagRFID = @TagRFID

	IF (@Option=1)	--  OPEN
	BEGIN
			-- If Truck has NOT a Running Transaction
			IF @Location = 1 -- REDS
				BEGIN -- chec for re-read at REDS or new read. Return if is re-read at REDS else proceed as normal
					
					--- @Result = 0 New Transaction (Go Normal)
					--- @Result = 1 Do Nothing
					SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					IF @Result = 1
						BEGIN

							UPDATE TagsToIssue SET IsDispatched = 1,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID	

							SELECT	0 as ID, 
							@TagRFID as TagRFID,
							@Proponent_ID as ProponentID, 
							@Hopper_ID as HopperID, 
							@Truck_ID as TruckID,
							1 as Assigned,
							'00' as HopperName,
							0 as TimeFromEnt2,
							0 as TimeToTW,
							0 as MarkerID,
							@TagType as TagType,	
							@TimerAdjust as TimerAdjust
							RETURN;
						END	
					ELSE
							UPDATE TagsToIssue SET IsDispatched = 0,LastModifiedDate = GetDate(), LastLocation = 1
							WHERE TagRFID = @TagRFID
																	
				END		 
			 
			
			IF NOT EXISTS(SELECT TagID FROM ActiveReads WHERE TagID=@Tag_ID)
				BEGIN	
						-- OPEN A NEW TRANSACTION
						INSERT INTO ActiveReads(TagID,HopperID,LastPosition)
						 Values(@Tag_ID,@Hopper_ID,@Location)
					
						Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID -- Set Operational
				END
			
		
			--Update Tags SET BatteryStatus =@TagBatteryStatus  WHERE  ID=@Tag_ID
					 
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID AND Location = @Location)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							Update TimeTable set DestinationPad = @FoundAtHopper where TagID = @Tag_ID
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
							
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END		
		ELSE IF  @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END
		

	END


	ELSE IF (@Option=2)	--  CLOSE AND OPEN
		BEGIN

			Declare @_TruckWashTime DateTime,
					@_ExitTime DateTime

			SET @_TruckWashTime = NULL;
			SET @_ExitTime = NULL;

			-- CLOSE
				SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID
				SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID
				-- ARHIVE the original record
				INSERT INTO ArchiveReads
							(TagRFID,
							ProponentName,
							HopperName,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							ArchiveDate,
							TruckID,
							TransStatus,
							TransMessage
							,TransactionID
							,FoundAtHopper
							,LastPosition)

						SELECT
								@TagRFID,
								@Proponent_Name,
								@Hopper_Name,
								REDS,
								[Queue],
								Entry1,
								Entry2,
								HopperTime,
								TruckWash,
								ExitTime,
								REDS2,
								TruckCallUp,
								getdate(),
								@Truck_ID,
								@TransStatus,
								@TransMessage
								,ID
								,FoundAtHopper
								,LastPosition

								FROM ActiveReads WHERE TagID= @Tag_ID
						
					SELECT @_ExitTime = ExitTime,@_TruckWashTime = TruckWash FROM ActiveReads WHERE TagID= @Tag_ID
					-- REMOVE the original record after archiving it 
					DELETE FROM ActiveReads WHERE TagID= @Tag_ID
					
					delete from TimeTable where TagID = @Tag_ID
				
			-- OPEN
					
				-- OPEN A NEW TRANSACTION
					INSERT INTO ActiveReads(TagID,HopperID,LastPosition,ReArrivedAtEntry)
					 Values(@Tag_ID,@Hopper_ID,@Location,1)
					
										 
				Update Tags SET [StatusID] = 3,BatteryStatus =@TagBatteryStatus WHERE  ID=@Tag_ID -- Set Operational
					
			IF @Location = 1 -- REDS	
				UPDATE ActiveReads SET REDS = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID Where TagID = @Tag_ID
			ELSE IF @Location = 2 -- QUEUE
				UPDATE ActiveReads SET [Queue] = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 3 -- ENTRY1
				UPDATE ActiveReads SET Entry1 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN @Location ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 4 -- ENTRY2
				UPDATE ActiveReads SET Entry2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 5 -- HOPPER
				BEGIN
					IF @NewLoopID IS NOT NULL
					BEGIN
						IF EXISTS(SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)
							SET  @MarkerID = (SELECT Top 1 ID FROM Markers WHERE MarkerLoopID = @NewLoopID)						
					END					
					IF @MarkerID = 0
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
					ELSE IF EXISTS(SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
						BEGIN
							SET @FoundAtHopper = (SELECT Top 1 ID FROM Hoppers WHERE MarkerID = @MarkerID)
							Update TimeTable set DestinationPad = @FoundAtHopper where TagID = @Tag_ID
							UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,FoundAtHopper = @FoundAtHopper,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
						END
					ELSE
						UPDATE ActiveReads SET HopperTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
				END
			ELSE IF @Location = 6 -- TruckWash
				UPDATE ActiveReads SET TruckWash = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 7 -- ExitGate
				UPDATE ActiveReads SET ExitTime = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID
			ELSE IF @Location = 8 -- REDS2
				UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID,EntryOrQueue = (CASE WHEN EntryOrQueue IS NULL THEN 3 ELSE EntryOrQueue end) Where TagID = @Tag_ID

		IF @Location = 6 OR @Location = 7
			BEGIN
				EXEC TagsToIssue_Update @Tag_ID,@TagRFID,@Location
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
			END	
		ELSE IF @Location <> 8
            BEGIN
				Update Tags SET [StatusID] = 3 WHERE  ID=@Tag_ID -- -- Set Operational
			END	

		END


	ELSE IF (@Option=3)		-- ONLY CLOSE
		BEGIN

			SELECT @Proponent_Name= ProponentName FROM Proponents WHERE ID= @Proponent_ID

			SELECT @Hopper_Name= HopperName FROM Hoppers WHERE ID= @Hopper_ID

			IF @Location = 8 -- REDS2
				BEGIN
					
					UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					WHERE TagRFID = @TagRFID	 

					UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					Where TagID = @Tag_ID	

					--- @Result = 2 REDS2 (Go Normal)
					--- @Result = 3 Close
					--	SET @Result = dbo.[TagReadAtREDS](@TagRFID,@Tag_ID)
					--	IF @Result = 2
					--		BEGIN
					--	UPDATE TagsToIssue SET LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	 
					--
					--	UPDATE ActiveReads SET REDS2 = GetDate(),LastPosition = @Location,NewLoopID=@NewLoopID,OldLoopID=@OldLoopID 
					--	 	Where TagID = @Tag_ID							 
					--	 END	
					--	ELSE
					--	 BEGIN
					--  SET @TransMessage = 'Transaction Completed.';
					--	 SET @TransStatus = 1;
					--	 UPDATE TagsToIssue SET  LastModifiedDate = GetDate(), LastLocation = @Location
					--	 WHERE TagRFID = @TagRFID	
					--	 END									
				END	

	
			-- ARHIVE the original record
			INSERT INTO ArchiveReads
						(TagRFID,
						ProponentName,
						HopperName,
						REDS,
						[Queue],
						Entry1,
						Entry2,
						HopperTime,
						TruckWash,
						ExitTime,
						REDS2,
						TruckCallUp,
						ArchiveDate,
						TruckID,
						TransStatus,
						TransMessage
						,TransactionID
						,FoundAtHopper
						,LastPosition)
					SELECT
							@TagRFID,
							@Proponent_Name,
							@Hopper_Name,
							REDS,
							[Queue],
							Entry1,
							Entry2,
							HopperTime,
							TruckWash,
							ExitTime,
							REDS2,
							TruckCallUp,
							getdate(),
							@Truck_ID,
							@TransStatus,
							@TransMessage
							,ID
							,FoundAtHopper
							,LastPosition
							FROM ActiveReads WHERE TagID= @Tag_ID
					
				-- REMOVE the original record after archiving it 
				DELETE FROM ActiveReads WHERE TagID= @Tag_ID
				
				delete from TimeTable where TagID = @Tag_ID
				
				Update Tags SET [StatusID] = 1 WHERE  ID=@Tag_ID -- Set Idel
				
		END


	declare @readTime datetime
	set @readTime = GetDate()
	set @TimerAdjust = 0
	declare @expadTime datetime
	 

		--shouldn't change pad, padLeaving time unless truck is at/location is pad
	if(@Location > 0)
		Exec UpdateTimeTable @Tag_ID,@Location,@readTime

	set @Hopper_ID = coalesce((select DestinationPad from TimeTable where TagID = @Tag_ID),@Hopper_ID)

	if @Location = 3 or @Location = 4 or @Location = 5 or @Location = 9 or @Location = 10 or @Location = 15
	begin
		
		set @TimerAdjust = dbo.GetCallupTimerInt(@Hopper_ID);

		/*--timer adjust will be: call the next truck from current pad free time - travel time from Q to Pad.
		
		/*select @TimerAdjust = datediff(ss,@readTime,dateadd(ss,0-dbo.GetTravelTimeToPad(2,DestinationPad),PadLeaving))
			 from TimeTable where TagID = @Tag_ID		*/
		
		--22-12-2011 10:52am
		--Get the pad leave time of the last truck at Entry1 - Q-Pad time will be the next callup. In Above case there might be truck 
		-- called up after this truck which might have different pad leave due to delay.
		select Top 1 @TimerAdjust = datediff(ss,@readTime,dateadd(ss,0-dbo.GetTravelTimeToPad(2,@Hopper_ID),PadLeaving))
			from TimeTable where DestinationPad = @Hopper_ID and Entry1 IS NOT NULL and Entry1 > @readTime 
				order by Entry1 desc 


			/*set @TimerAdjust =datediff(ss,coalesce((select case @Location 
												when 1 then REDS  
												when 3 then Entry1
												when 9 then Entry1
												when 10 then Entry1
												when 4 then Entry2
												when 5 then Pad
												else @readTime end
											from TimeTable where TagID = @Tag_ID),@readTime),
										@readTime)
			*/*/

	end --end of if Location >0	
	
	

--above change should take care of if truck misses pad 
/*
			if @Location = 6 or @Location = 7
				begin
					if (select HopperTime from ActiveReads where TagID = @Tag_ID) is NULL
						begin
								--if locaiton is Ex/TW, need to find diff for pad time in case if truck has missed Pad to call next truck early
								select top 1  @expadTime = dateadd(ss,(0 - (dbo.GetTravelTimeToPad(6,@Hopper_ID)+Unloading)),@readTime)
									from ExpectedTimes order by ID desc
								set @TimerAdjust = datediff(ss,coalesce((select Pad from TimeTable where TagID = @Tag_ID),@expadTime),@expadTime)
								--to only adjust timer once
								--next time if truck is read at the same location adjustTimer should be 0 as already adjusted previously
								--do not update HopperTime in ActiveReads as it can be used to see if truck was read at Pad or not. 
								update TimeTable set Pad = @expadTime where TagID = @Tag_ID
						end
				end

*/		
			
	

	
		
	-- RETURN VALUES 

	SELECT	@Tag_ID as ID, 
			@TagRFID as TagRFID,
			@Proponent_ID as ProponentID, 
			@Hopper_ID as HopperID, 
			@Truck_ID as TruckID,
			1 as Assigned,
			HopperName,TimeFromEnt2,TimeToTW,isnull(MarkerID,0) AS MarkerID,
			@TagType as TagType,
			@TimerAdjust as TimerAdjust
			From Hoppers
			WHERE ID = @Hopper_ID			

END


--------------------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------------------------------------------


















