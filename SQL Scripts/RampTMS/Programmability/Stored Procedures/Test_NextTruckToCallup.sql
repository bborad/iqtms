 	
	declare @SkipTagRFID varchar(50)
	declare @HopperID int
	declare @trucktocall varchar(50)

	set @HopperID = 1;
	 set @SkipTagRFID = '400002029'

execute NextTruckToCallup @SkipTagRFID,@HopperID,@trucktocall output
select @trucktocall
/*
	declare @SkipTagID int
	declare @CallupTagID int
	declare @GroupID int
		declare @pid int
		declare @ptime datetime
		declare @orderid int
		
		declare @tagToAssign bigint
		declare @qTime datetime
		
		declare @currentTime datetime
		declare @tagToUpdate bigint
		declare @expectedArrivalTime datetime
		declare @queueTime datetime	
	
	select @SkipTagID = ID from Tags where TagRFID = @SkipTagRFID
	select @GroupID = GroupID from Hoppers where ID = @HopperID;
    set @currentTime = getdate()

if cursor_status('global','padfreetime') > -1
begin
	close padfreetime
	deallocate padfreetime
end

 declare padfreetime scroll cursor for
				select ID as PID,dbo.GetPadFreeTime(ID,@currentTime) as PTime,GroupOrder
					from Hoppers where Status = 0 and GroupID = @GroupID
						order by PTime,GroupOrder

if cursor_status('global','trucksToAssign') > -1
begin
	close trucksToAssign
	deallocate trucksToAssign
end
	declare trucksToAssign cursor for
		select TagID as tagToAssign from TimeTable
									inner join Tags
									on Tags.ID = TimeTable.TagID
										inner join Hoppers
										on Hoppers.ID = Tags.HopperID
								where Hoppers.GroupID = @GroupID
									and Tags.CurrentState = 2 
									and TimeTable.DestinationPad is NULL 
									and TimeTable.Queue is not NULL
								order by TimeTable.Queue 

	open trucksToAssign
	open padfreetime
 
		--Fetch from top and assign that pad to a truck 
		--		(get a truck in order they arrived in queue : truck must have DesPad = NULL, CurrentState = 2 Queue != NULL
	
	--open padfreetime;

		fetch next  from  trucksToAssign
		into @tagToAssign;

		while @@FETCH_STATUS = 0
		begin
			
 
			--get next tag/truck to update/assign new pad to
			
			fetch next from padfreetime
			into @pid, @ptime,@orderid;
			
			if @@FETCH_STATUS <> 0
				begin
					fetch first from padfreetime
					into @pid, @ptime,@orderid;
				end
			
			select @tagToAssign,@pid, @ptime,@orderid;
			/*
			set @expectedArrivalTime = dateadd(ss,dbo.GetTravelTimeToPad(2,@pid),@currentTime)
		
			/* update TimeTable with new pad and pad time 
			*/
			update TimeTable set DestinationPad = @pid,Pad = case when @expectedArrivalTime > @ptime then @expectedArrivalTime else @ptime end 
				where TagID = @tagToUpdate

			/* update arrival time for all other state
			*/
			execute UpdateTimeTable @tagToUpdate,2,@queueTime

			*/

			fetch next from  trucksToAssign
			into @tagToAssign;

		end
		
close trucksToAssign
deallocate trucksToAssign
close padfreetime
deallocate padfreetime

	/*if @SkipTagID > 0
		begin
			update TimeTable set Queue = getdate() where TagID = @SkipTagID
			update Tags set CurrentState = 2 where ID = @SkipTagID
			update ActiveReads set TruckCallUp = NULL where TagID = @SkipTagID
		end

	select top 1 @CallupTagID = TagID 
		 from TimeTable
		  where TimeTable.TagID in (select ID From Tags where CurrentState = 2 and HopperID in (select ID from Hoppers where GroupID = @GroupID))
		order by Queue

	update ActiveReads set TruckCallUp = getdate() where TagID = @CallupTagID
	update Tags set CurrentState = 15 where ID = @CallupTagID -- set currentstate to 15:InboundEntry
	update TimesTable set DestinationPad = @HopperID where TagID = @CallupTagID
	
	
	--update ActiveReads set TruckCallUp = getdate() where TagID = @CallupTagID

select @CallupTagID

		*/
*/