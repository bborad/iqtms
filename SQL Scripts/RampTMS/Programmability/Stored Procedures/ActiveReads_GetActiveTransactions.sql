set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Vijay Mangtani
-- Create date: 28 09 2010
-- Description:	Get All Active Transactions
-- ===================================================
ALTER PROCEDURE [dbo].[ActiveReads_GetActiveTransactions] --1
 @option int = 0
AS
BEGIN
	SET NOCOUNT ON;
 
IF @option = 0
	BEGIN
		SELECT	ar.TagID as TagID,ar.NewLoopID as NewLoopID,ar.OldLoopID as OldLoopID,Isnull(ar.LastPosition,1) as LastPosition,
		Isnull(ar.EntryORQueue,2) as EntryORQueue,ar.HopperID as HopperID,ProponentID ,TruckID,
		Tags.Assigned,Tags.TagRFID,isnull(ar.FoundAtHopper,0) as FoundAtHopper,IsNull(ar.ReArrivedAtEntry,0) as ReArrivedAtEntry,
		Hoppers.HopperName,Hoppers.TimeFromEnt2,Hoppers.TimeToTW,isnull(Hoppers.MarkerID,0) AS MarkerID, 
		IsNull(Hoppers.Status,0) as HopperStatus,isnull(DestinationPad,0) as DestinationPad
		FROM ActiveReads ar
		INNER JOIN Tags ON
		Tags.ID = ar.TagID
		inner join TimeTable tt on
		tt.TagID = ar.TagID
		INNER JOIN Hoppers ON
		Hoppers.ID = ar.HopperID 
		Where Convert(varchar(20),StartTime,101) = Convert(varchar(20),GetDate(),101)		
	END
ELSE
	BEGIN
		/*SELECT	TagID,NewLoopID,OldLoopID,Isnull(LastPosition,1) as LastPosition,Isnull(EntryORQueue,2) as EntryORQueue,ActiveReads.HopperID ,ProponentID,TruckID,
		Tags.Assigned,Tags.TagRFID,isnull(FoundAtHopper,0) as FoundAtHopper,IsNull(ReArrivedAtEntry,0) as ReArrivedAtEntry,
		Hoppers.HopperName,Hoppers.TimeFromEnt2,Hoppers.TimeToTW,isnull(Hoppers.MarkerID,0) AS MarkerID,IsNull(Hoppers.Status,0) as HopperStatus
		*/
		SELECT	ar.TagID as TagID,ar.NewLoopID as NewLoopID,ar.OldLoopID as OldLoopID,Isnull(ar.LastPosition,1) as LastPosition,
		Isnull(ar.EntryORQueue,2) as EntryORQueue,ar.HopperID as HopperID,ProponentID,TruckID,
		Tags.Assigned,Tags.TagRFID,isnull(ar.FoundAtHopper,0) as FoundAtHopper,IsNull(ar.ReArrivedAtEntry,0) as ReArrivedAtEntry,
		Hoppers.HopperName,Hoppers.TimeFromEnt2,Hoppers.TimeToTW,isnull(Hoppers.MarkerID,0) AS MarkerID, 
		IsNull(Hoppers.Status,0) as HopperStatus,isnull(DestinationPad,0) as DestinationPad
		FROM ActiveReads ar
		INNER JOIN Tags ON
		Tags.ID = ar.TagID
		inner join TimeTable tt on
		tt.TagID = ar.TagID
		INNER JOIN Hoppers ON
		Hoppers.ID = ar.HopperID 		
	END	

END

 