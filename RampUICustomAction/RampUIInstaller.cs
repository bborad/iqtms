using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.IO;
using System.Security.AccessControl;
using System.Diagnostics;

namespace RampUICustomAction
{
    [RunInstaller(true)]
    public partial class RampUIInstaller : Installer
    {
        public RampUIInstaller()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
            stateSaver.Add("FilePath", Context.Parameters["FilePath"]); 
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);

            try
            {

                DirectorySecurity ds = null;
                //System.Security.Principal.NTAccount user = new System.Security.Principal.NTAccount("ASPNET");

                bool modified;

                FileSystemAccessRule far1 = new FileSystemAccessRule("EVERYONE", FileSystemRights.Write | FileSystemRights.Read | FileSystemRights.Modify | FileSystemRights.ReadAndExecute, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);
                FileSystemAccessRule far = new FileSystemAccessRule("ASPNET", FileSystemRights.Write | FileSystemRights.Read | FileSystemRights.Modify | FileSystemRights.ReadAndExecute, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

                if (Directory.Exists(savedState["FilePath"].ToString() + "uploadfiles"))
                {
                    ds = Directory.GetAccessControl(savedState["FilePath"].ToString() + "uploadfiles");
                    if (ds != null)
                    {
                        try
                        {
                            ds.ModifyAccessRule(AccessControlModification.Reset, far1, out modified);
                            ds.SetAccessRuleProtection(false, false);
                            Directory.SetAccessControl(savedState["FilePath"].ToString() + "uploadfiles", ds);
                        }
                        catch
                        {
                        }

                        try
                        {

                            ds.ModifyAccessRule(AccessControlModification.Reset, far, out modified);
                            ds.SetAccessRuleProtection(false, false);
                            Directory.SetAccessControl(savedState["FilePath"].ToString() + "uploadfiles", ds);
                        }
                        catch
                        {
                        }

                    }
                }

                // code to copy required dll in system32 folder

                if (Directory.Exists(savedState["FilePath"].ToString() + "bin"))
                {
                    try
                    {
                        string dllPath = savedState["FilePath"].ToString() + @"bin\libmbusslave.dll";
                        if (File.Exists(dllPath))
                        {
                            File.Copy(dllPath, @"C:\Windows\System32\libmbusslave.dll", true);
                        }
                    }
                    catch
                    {
                    }
                }
            
            }
            catch (Exception ex)
            {
            }
        }
        
    }
}
