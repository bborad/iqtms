using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBUtility = Ramp.Database.Common;
using Ramp.MiddlewareController.Common;
using System.Data;

using NLog;

namespace Ramp.DBConnector.Common
{
    public class DBConnectorUtility
    {
        public static bool IsTruckArrived(Location loc, string TagRFID)
        {
            bool flagResult = false;
            try
            {
                flagResult = DBUtility.DBUtility.IsTruckArrived(loc, TagRFID);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }
        /// <summary>
        /// Send Email 
        /// </summary>
        /// <param name="FromAddress"></param>
        /// <param name="ToAddress"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="Attachment"></param>
        /// <param name="Subject"></param>
        /// <param name="MessageBody"></param>
        public static void SendEmail(string FromAddress, string ToAddress, string CC, string BCC, string Attachment, string Subject, string MessageBody)
        {
            try
            {
                DBUtility.DBUtility.SendEmail(FromAddress, ToAddress, CC, BCC, Attachment, Subject, MessageBody);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get ALL Expected Time Values
        /// </summary>
        public static DataSet GetALLExpectedTimeValues()
        {
            try
            {
                return DBUtility.DBUtility.GetALLExpectedTimeValues();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateExpectedTimeValues(string ID, string REDS_Queue, string Queue_Ent1, string Ent1_Ent2, string QueueTimePerTruck, string Unloading, string StartUp,
           string ShutDown, string TruckCallResponse, string TruckQueueMax, string Exit_REDS, string TruckWash_REDS, string Precision, string TagReadFrequency,
           string MaxDelay, string Exit_Ent1, string TruckWash_Ent1)
        {
            try
            {
                DBUtility.DBUtility.UpdateExpectedTimeValues(ID, REDS_Queue, Queue_Ent1, Ent1_Ent2, QueueTimePerTruck, Unloading, StartUp,
                ShutDown, TruckCallResponse, TruckQueueMax, Exit_REDS, TruckWash_REDS, Precision, TagReadFrequency, MaxDelay, Exit_Ent1, TruckWash_Ent1);
            }
            catch (Exception ed)
            {
                throw ed;
            }
        }

        public static void ArchiveExpectedAndHopperTime()
        {
            try
            {
                DBUtility.DBUtility.ArchiveExpectedAndHopperTime();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataSet GetVMSStatusForSystemReport()
        {
            try
            {
                return DBUtility.DBUtility.GetVMSStatusForSystemReport();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static void GetExpectedTimeValues()
        {
            try
            {
                DBUtility.DBUtility.GetExpectedTimeValues();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GetTodaysAltersForSystemReport()
        {
            try
            {
                return DBUtility.DBUtility.GetTodaysAltersForSystemReport();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataSet GetAllSecurityGroup()
        {
            try
            {
                return DBUtility.DBUtility.GetAllSecurityGroup();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataSet GetConfigurationValues()
        {
            try
            {
                return DBUtility.DBUtility.GetConfigurationValues();
            }
            catch (Exception)
            {
                throw;
            }

        }
        
        public static void UpdateConfigurations(Int32 ID, string Value,int VMSDeviceLocation)
        {
            try
            {
                DBUtility.DBUtility.UpdateConfigurations(ID, Value, VMSDeviceLocation);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void UpdateAlerts(string alertDesc, int alertID,bool IsDeleted)
        {
            try
            {
                DBUtility.DBUtility.UpdateAlerts(alertDesc, alertID,IsDeleted);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetTagsToDispatch()
        {
            DataTable dt = new DataTable();
            try
            {
                 dt = DBUtility.DBUtility.GetTagsToDispatch();
            }
            catch (Exception ex)
            {
                
            }
            return dt;
        }

        public static DataTable GetRunningServerInfo()
        {
            DataTable dt = null;
            try
            {
                dt = DBUtility.DBUtility.GetRunningServerInfo();
            }
            catch (Exception)
            {

            }           
            return dt;
        }

        public static int SwitchToNewServer(string ServerName)
        {
            int result = 0;
            try
            {
                result = DBUtility.DBUtility.SwitchToNewServer(ServerName);
            }
            catch (Exception)
            {
                result = -1;
            }

            return result;

        }

        public static int UpdateServerStatus(string ServerName)
        {
            int result = 0;
            try
            {
                result = DBUtility.DBUtility.UpdateServerStatus(ServerName);
            }
            catch (Exception)
            {
                result = -1;
            }

            return result;

        }

        public static void UpdateTagsToIssue(string TagRFID, Int64 TagID, Location lastLoc, bool IsDispatched, bool IsDeleted)
        {
            try
            {
                DBUtility.DBUtility.UpdateTagsToIssue(TagRFID, TagID, lastLoc, IsDispatched, IsDeleted);
            }
            catch (Exception)
            {
                //throw;
            }
        }

        public static void AddUpdateTagsToIssue(DataTable dtTagsToIssue)
        {
            try
            {
                DBUtility.DBUtility.AddUpdateTagsToIssue(dtTagsToIssue);
            }
            catch (Exception ex)
            {
            }
        }

        public static DataTable GetAllRefernceNo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = DBUtility.DBUtility.GetAllRefernceNo();
            }
            catch (Exception ex)
            {}
            return dt;
            
        }

    }
}
