using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;


namespace Ramp.DBConnector.Adapter
{
    public class Tags : ITags
    {
        #region "private variables"
        public int ID { get; set; }
        public string TagRFID { get; set; }
        public int ProponentID { get; set; }
        public int HopperID { get; set; }
        public bool Assigned { get; set; }
        //public int TruckID { get; set; }
        public string TruckID { get; set; }
        public IHoppers AssignedHopper { get; set; }
        public int TagType { get; set; }
        public int DestinationPad { get; set; }
        public DateTime LastReadTime { get; set; }

        Ramp.Database.Connector.Tags objTag;

        public Tags()
        {
            objTag = new Ramp.Database.Connector.Tags();
        }

        #endregion

        #region "Public Methods"      


        public void InsertTagDetails(string TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                objTag.InsertTagDetails(TagRFID, ProponentID, HopperID, Assigned, TruckID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int InsertTags(string TagRFID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                return Convert.ToInt32(objTag.InsertTags(TagRFID));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Check tag already assigned
        /// </summary>
        /// <param name="TagRFID"></param>
        /// <returns></returns>
        public DataTable CheckTagAlreadyAssigned(string TagRFID, int ProponentID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();
                return (DataTable)(objTag.CheckTagAlreadyAssigned(TagRFID, ProponentID));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public void UpdateTagDetails(int ID, int TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                objTag.UpdateTagDetails(ID, TagRFID, ProponentID, HopperID, Assigned, TruckID);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int DispatchTagsByTagsID(string IssuedTagsID, string TruckRegNo, string TagBattery)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                return Convert.ToInt32(objTag.DispatchTagsByTagsID(IssuedTagsID, TruckRegNo, TagBattery));

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int AssignTags(string TagRFID, string TruckID, int HopperID, int ProponentID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                return Convert.ToInt32(objTag.AssignTags(TagRFID, TruckID, HopperID, ProponentID));

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int UnAssignTags(string TagRFID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                return Convert.ToInt32(objTag.UnAssignTags(TagRFID));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int RemoveTags(string TagRFID)
        {
            try
            {
                if (objTag == null)
                    objTag = new Ramp.Database.Connector.Tags();

                return Convert.ToInt32(objTag.RemoveTags(TagRFID));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion

        #region "Public Static Methods"

        public static List<ITags> GetAllTags()
        {
            try
            {
                return Ramp.Database.Connector.Tags.GetAllTags();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static ITags GetTagDetailsByTagID(string TagRFID)
        {
            try
            {
                return DBModule.Tags.GetTagDetailsByTagID(TagRFID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable GetTagDetailsByTagsID(string TagsRFID)
        {
            try
            {
                return DBModule.Tags.GetTagsDetailsByTagsID(TagsRFID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataSet GetAllAssignedTagsByProponentID(string TagsRFID, int ProponentID)
        {
            try
            {
                return DBModule.Tags.GetAllAssignedTagsByProponentID(TagsRFID, ProponentID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable GetTagsDetailForUnAssign(string TagsRFID)
        {
            try
            {
                return DBModule.Tags.GetTagsDetailForUnAssign(TagsRFID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public static DataTable GetDetailsByTruckIDs(string TruckIDs)
        {
            try
            {
                return DBModule.Tags.GetDetailsByTruckIDs(TruckIDs);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable AssignUnAssignTagReport(string TagRFIDs, string HopperIDs, string ProponentIDs, string DateFrom, string DateTo)
        {
            try
            {
                return DBModule.Tags.AssignUnAssignTagReport(TagRFIDs, HopperIDs, ProponentIDs, DateFrom, DateTo);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public static DataTable IssueTagReport(string TagRFIDs, string HopperIDs, string ProponentIDs, string DateFrom, string DateTo, string ReferenceNo)
        {
            try
            {
                return DBModule.Tags.IssueTagReport(TagRFIDs, HopperIDs, ProponentIDs, DateFrom, DateTo, ReferenceNo);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion
    }
}
