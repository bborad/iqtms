using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using DBModule = Ramp.Database.Connector;

namespace Ramp.DBConnector.Adapter
{
    public class ExpectedTimes
    {

        public static DataSet GetDateRange()
        {
            try
            {
                return DBModule.ExpectedTimes.GetDateRange();
            }
            catch (Exception)
            {
                throw;
            }
        }



        public static DataSet GetReport(int HopperID, int TableID, int ID)
        {
            try
            {
                return DBModule.ExpectedTimes.GetReport(HopperID, TableID, ID);
            }
            catch (Exception)
            {
                throw;
            }
        }






    }
}
