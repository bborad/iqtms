using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.DBConnector.Adapter
{
    public class Locations : ILocations
    {
        #region "private variables"

        public int ID { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        Ramp.Database.Connector.Locations objLocation;

        public Locations()
        {
            objLocation = new Ramp.Database.Connector.Locations();
        }

        #endregion

        #region "public methods"
        public int InsertLocation(string LocationName, string Description)
        {
            try
            {
                if (objLocation == null)
                    objLocation = new Ramp.Database.Connector.Locations();

                return Convert.ToInt32(objLocation.InsertLocation(LocationName, Description));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }



        #endregion

        #region "public static methods"

        /// <summary>
        /// Get All Locations
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        public static List<ILocations> GetAllLocations()
        {
            try
            {
                List<ILocations> lstLocations = DBModule.Locations.GetAllLocations();
                return lstLocations;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
