using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using DBModule = Ramp.Database.Connector;

namespace Ramp.DBConnector.Adapter
{
    public class ArchiveReads
    {
        public static int UploadOfflineTransactions(DataTable dtAdd)
        {
            try
            {
                return DBModule.ArchiveReads.UploadOfflineTransactions(dtAdd); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
