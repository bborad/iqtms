using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using DBModule = Ramp.Database.Connector;
using System.Data;

namespace Ramp.DBConnector.Adapter
{
    public class SecurityGroups
    {

        public int ID { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }

        public static List<ISecurityGroups> GetSecurityGroups()
        {
            try
            {
                return DBModule.SecurityGroups.GetSecurityGroups();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
