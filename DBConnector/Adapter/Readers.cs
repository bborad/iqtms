using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
 

namespace Ramp.DBConnector.Adapter
{
    public class Readers : IReaders
    {
        //private static NLog.Logger logger = NLog.LogManager.GetLogger("Ramp.DBConnector.Adapter.Readers");

        #region "private variables"
        public int ID { get; set; }
        public string ReaderSerialNo { get; set; }
        public string ReaderName { get; set; }
        public string IPAddress { get; set; }

        public int Location { get; set; }
        public int PortNo { get; set; }
        public int Power { get; set; }
        public string ReaderStatus { get; set; }

        Ramp.Database.Connector.Readers objReader;

        public Readers()
        {
            objReader = new Ramp.Database.Connector.Readers();
        }

        #endregion

        #region "Public Methods"

        public int InsertReader(string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power)
        {
            try
            {
                if (objReader == null)
                    objReader = new Ramp.Database.Connector.Readers();

                return Convert.ToInt32(objReader.InsertReader(ReaderSerialNo, ReaderName, IPAddress, PortNo, MarkerIDs, Location, Power));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int UpdateReader(int ReaderID, string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power)
        {
            try
            {
                if (objReader == null)
                    objReader = new Ramp.Database.Connector.Readers();

                return Convert.ToInt32(objReader.UpdateReader(ReaderID, ReaderSerialNo, ReaderName, IPAddress, PortNo, MarkerIDs, Location, Power));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public void DeleteReaderByID(int ID)
        {
            try
            {
                if (objReader == null)
                    objReader = new Ramp.Database.Connector.Readers();

                objReader.DeleteReaderByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }


        #endregion

        #region "Public Static Methods"
        /// <summary>
        /// Get All Readers
        /// </summary>
        /// <returns></returns>
        public static List<IReaders> GetAllReaders()
        {
            List<IReaders> lstReaders = null;

            try
            {
                lstReaders = DBModule.Readers.GetAllReaders();

            }
            catch (Exception ex)
            {
                //logger.ErrorException("Exception thrown in GetAllReaders", ex);
               
            }
            return lstReaders;
        }

        public static List<IReaders> GetReadersAtLocation(Location loc)
        {
            try
            {
                List<IReaders> lstReaders = DBModule.Readers.GetReadersAtLocation(loc);
                return lstReaders;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static IReaders GetReaderDetailsByID(int ID)
        {
            IReaders objReader = null;

            try
            {
                objReader = DBModule.Readers.GetReaderDetailsByID(ID);

            }
            catch (Exception)
            {
                throw;
            }
            return objReader;
        }

        public static DataSet ReaderAndItsMarkersDetailByID(int ReaderID)
        {
            try
            {
                return DBModule.Readers.ReaderAndItsMarkersDetailByID(ReaderID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
        
        public static DataTable GetAllReadersWithItsMarker()
        {
            try
            {
                return DBModule.Readers.GetAllReadersWithItsMarker();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
        
        public static int CheckIPAddressAlreadyExists(int ReaderID,string IPAddress)
        {
            try
            {
                return DBModule.Readers.CheckIPAddressAlreadyExists(ReaderID,IPAddress);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataSet GetReaderStatusForSystemReport()
        {
            try
            {
                return DBModule.Readers.GetReaderStatusForSystemReport();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable UpdateStatus(string IpAddress, string Status)
        {
            try
            {
                return DBModule.Readers.UpdateStatus(IpAddress, Status);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #endregion
    }
}
