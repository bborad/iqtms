using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using DBModule = Ramp.Database.Connector;

namespace Ramp.DBConnector.Adapter
{
    public class Markers : IMarkers
    {
        #region "private variables"
        public int ID { get; set; }
        public string MarkerLoopID { get; set; }
        public string IPAddress { get; set; }
        //public Int32 MarkerPosition { get; set; }
        public string MarkerPosition { get; set; }
        public Int32 Location { get; set; }

        Ramp.Database.Connector.Markers objMarker;

        public Markers()
        {
            objMarker = new Ramp.Database.Connector.Markers();
        }

        #endregion

        #region "Public Methods"

        public int InsertMarker(string MarkerLoopID, string MarkerPosition, int Location, string SerialNo)
        {
            try
            {
                if (objMarker == null)
                    objMarker = new Ramp.Database.Connector.Markers();

                return Convert.ToInt32(objMarker.InsertMarker(MarkerLoopID, MarkerPosition, Location,SerialNo));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Delete Marker by Marker IDs (in CSV)
        /// </summary>
        /// <param name="MarkerLoopID"></param>
        /// <param name="MarkerPosition"></param>
        /// <returns></returns>
        public int DeleteMarkerByID(string MarkerIDs)
        {
            try
            {
                if (objMarker == null)
                    objMarker = new Ramp.Database.Connector.Markers();

                return Convert.ToInt32(objMarker.DeleteMarkerByID(MarkerIDs));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }
        #endregion

        #region "Public Static Methods"

        public static List<IMarkers> GetMarkersAtLocation(Location loc)
        {
            try
            {
                List<IMarkers> lstMarkers = DBModule.Markers.GetMarkersAtLocation(loc);
                return lstMarkers;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get All Markers
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        public static List<IMarkers> GetAllMarkers()
        {
            try
            {
                List<IMarkers> lstMarkers = DBModule.Markers.GetAllMarkers();
                return lstMarkers;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetMarkers()
        {
            DataTable dtMarkers = null;
            try
            {
                dtMarkers = DBModule.Markers.GetMarkers(); 
            }
            catch
            {

            }
            return dtMarkers;
        }

        public static DataTable GetMarkersInfo(out int markerHealthCheck, out int LogFileClearingInterval)
        {
            DataTable dtMarkers = null;

            try
            {
                dtMarkers = DBModule.Markers.GetMarkersInfo(out markerHealthCheck, out LogFileClearingInterval);
            }
            catch (Exception ex)
            {
                markerHealthCheck = -1;
                throw ex;
            }

            return dtMarkers;
        }

        public static DataSet GetLoopStatusForSystemReport()
        {
            try
            {
                return DBModule.Markers.GetLoopStatusForSystemReport();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
