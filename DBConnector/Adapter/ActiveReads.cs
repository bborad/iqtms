using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.DBConnector.Adapter
{
    public class ActiveReads : IActiveReads
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        #region "private variables"

        public int ID { get; set; }
        public int TagID { get; set; }
        public int HopperID { get; set; }
        public int LastPosition { get; set; }
        public int EntryORQueue { get; set; }
        public DateTime REDS { get; set; }
        public DateTime Queue { get; set; }
        public DateTime Entry1 { get; set; }
        public DateTime Entry2 { get; set; }
        public DateTime HopperTime { get; set; }
        public DateTime TruckWash { get; set; }
        public DateTime REDS2 { get; set; }
        public DateTime TruckCallUp { get; set; }

        #endregion

        #region "Public Methods"

        public void SendTruckToQueueBack(string TagRFID)
        {
            try
            {
                DBModule.ActiveReads obj = new Ramp.Database.Connector.ActiveReads();
                obj.SendTruckToQueueBack(TagRFID);
            }
            catch (Exception)
            {
                throw;
            }
        }


        //  MAX QUEUE 
        public Int32 GetTotalTagsAtLocation(Location loc)
        {
            try
            {
                DBModule.ActiveReads obj = new Ramp.Database.Connector.ActiveReads();
                return obj.GetTotalTagsAtLocation(loc);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // QUEUE LENGTH
        public Int32 GetQueueLengthForHopper(Int32 HopperID)
        {
            try
            {
                DBModule.ActiveReads obj = new Ramp.Database.Connector.ActiveReads();
                return obj.GetQueueLengthForHopper(HopperID);
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region "Public Static Methods"


        public static List<IActiveReads> GetAllRecordsByHopperID(int HopperID)
        {
            List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();
            try
            {
                objActiveReads_Lst = DBModule.ActiveReads.GetAllRecordsByHopperID(HopperID);
            }
            catch (Exception)
            {
                
            }
            return objActiveReads_Lst;
        }

        public static int UpdateTransaction(Int64 TagID, string TagRFID, Location LastPosition)
        {
            try
            {
                return DBModule.ActiveReads.UpdateTransaction(TagID, TagRFID, LastPosition);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static bool UpdateTruckCallUp(string TagRFID, ref VMSParameters vmsParam, ref int TruckCallUpRetries, ref DataTable dtVMSSettings)
        {
            bool IsTruckExist = false;
            try
            {
               IsTruckExist = DBModule.ActiveReads.UpdateTruckCallUp(TagRFID, ref vmsParam, ref TruckCallUpRetries, ref dtVMSSettings);
            }
            catch (Exception)
            {
               // throw;
            }

            return IsTruckExist;
            
        }

        public static ITags OpenCloseTransaction(string TagRFID, Location loc, Int32 Option, string NewLoopID, string OldLoopID, int TransStatus, string TransMessage,string TagBatteryStatus,out int timerAdjust,out string newHOrder)
        {
            timerAdjust = 0;
            try
            {
                return DBModule.ActiveReads.OpenCloseTransaction(TagRFID, loc, Option, NewLoopID, OldLoopID, TransStatus, TransMessage, TagBatteryStatus,out timerAdjust,out   newHOrder);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public static List<IActiveReads> GetRecordsByTagIDs(string strTagIDs, Location loc)
        //{
        //    try
        //    {
        //        return DBModule.ActiveReads.GetRecordsByTagIDs(strTagIDs, loc);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static DataSet GetRecordsByTagIDs(string strTagIDs, Location loc)
        {
            try
            {
                return DBModule.ActiveReads.GetRecordsByTagIDs(strTagIDs, loc);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataSet GetAllRecordsForTrafficPage()
        {
            try
            {
                return DBModule.ActiveReads.GetAllRecordsForTrafficPage();
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static DataSet GetClsedTxForTrafficPage()
        {
            try
            {
                return DBModule.ActiveReads.GetClsedTxForTrafficPage();
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static void AssignNewPad(string tagRFID,DateTime readTime, Location loc,int newPad,DateTime newPadTime)
        {
            try
            {
                DBModule.ActiveReads.AssignNewPad(tagRFID,readTime,loc, newPad, newPadTime);
            }
            catch (Exception ex)
            {
            }

        }

        public static void ReassignAllPad(int GroupID,string skipTagID)
        {
            DBModule.ActiveReads.ReassignAllPad(GroupID,skipTagID);
        }
        public static Dictionary<string,int> NextTruckToCallup(string skipTagRFID,int HopperID,out string CallupTagRFID,out int TimeToWait)
        {
            return DBModule.ActiveReads.NextTruckToCallup(skipTagRFID,HopperID,out CallupTagRFID,out TimeToWait);
        }
        public static void EntryOrQueue(string tagRFID, DateTime readTime, Location loc, out int desPadId, out Location desLoc)
        {
            DBModule.ActiveReads.EntryOrQueue(tagRFID, readTime, loc, out desPadId, out desLoc);
        }

        public static void GetNextCallupTime(int HopperID, out int callTimer)
        {
            DBModule.ActiveReads.GetNextCallupTime( HopperID, out callTimer);
        }

        public static void GetNextFreePad(string tagID,DateTime startTime,Location loc,out int padId,out DateTime padTime)
        {

            try
            {
                 DBModule.ActiveReads.GetNextFreePad(tagID,startTime,loc,out  padId,out  padTime);
            }
            catch (Exception ex)
            {
                padId = 0;
                padTime = DateTime.MaxValue;
            }

        }

        public static void UpdateEntryORQueue(int TagID, string TagRFID, Location EntryORQueue,int newPad,DateTime expectedArrival,ref VMSParameters vmsParam,ref DataTable dtVMSSettings)
        {
           // vmsParam = new VMSParameters();
            try
            {
                DBModule.ActiveReads.UpdateEntryORQueue(TagID, TagRFID, EntryORQueue,newPad,expectedArrival, ref vmsParam, ref dtVMSSettings);
            }
            catch (Exception)
            {
               // throw;
            }
           
        }

        public static DataTable GetActiveTransactions(int forAllDays)
        {
            try
            {
                return DBModule.ActiveReads.GetActiveTransactions(forAllDays);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
