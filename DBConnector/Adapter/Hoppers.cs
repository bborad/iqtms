using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;

using DBModule = Ramp.Database.Connector;
using System.Data;

namespace Ramp.DBConnector.Adapter
{
    public class Hoppers : IHoppers
    {

        #region "private variables"

        public int ID { get; set; }
        public string HopperName { get; set; }
        public int MarkerID { get; set; }
        public Int32 TimeFromEnt2 { get; set; }
        public Int32 TimeToTW { get; set; }

        Ramp.Database.Connector.Hoppers objHopper;

        public Hoppers()
        {
            objHopper = new Ramp.Database.Connector.Hoppers();
        }
        #endregion

        #region "public methods"
        public int AssignHoppersProponents(int ProponentID, int HopperID)
        {
            try
            {
                if (objHopper == null)
                    objHopper = new Ramp.Database.Connector.Hoppers();

                return Convert.ToInt32(objHopper.AssignHoppersProponents(ProponentID, HopperID));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public void UpdateExpectedTimeEntry2(string ExpectedTimeID , string TimeFromEnt2, string TimeToTW,int MarkerID)
        {
            try
            {
                if (objHopper == null)
                    objHopper = new Ramp.Database.Connector.Hoppers();

                objHopper.UpdateExpectedTimeEntry2(ExpectedTimeID, TimeFromEnt2, TimeToTW, MarkerID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        //public void UpdateExpectedTimeTW(string ID, string TimeToTW)
        //{
        //    try
        //    {
        //        if (objHopper == null)
        //            objHopper = new Ramp.Database.Connector.Hoppers();

        //        objHopper.UpdateExpectedTimeTW(ID, TimeToTW);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}

        #endregion

        #region "Public Static Methods"
        public static IHoppers GetHopperDetailsByHopperID(int HopperID)
        {
            try
            {
                return DBModule.Hoppers.GetHopperDetailsByHopperID(HopperID);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static string GetPriority(string Lane1_TagRFID, string Lane2_TagRFID)
        {
            try
            {
                return DBModule.Hoppers.GetPriority(Lane1_TagRFID, Lane2_TagRFID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get All Hoppers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllHoppers()
        {
            try
            {
                return DBModule.Hoppers.GetAllHoppers();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get PAD Status
        /// </summary>
        /// <returns></returns>
        public static DataSet GetHoppers()
        {
            try
            {
                return DBModule.Hoppers.GetHoppers();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Hopper Details By Hopper ID
        /// </summary>
        /// <param name="HopperID"></param>
        /// <returns></returns>
        public static DataSet GetProponentsByHopperID(int HopperID)
        {
            try
            {
                return DBModule.Hoppers.GetProponentsByHopperID(HopperID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable CheckProponentIsAlreadyAssignedToOtherHopper(int ProponentID, int HopperID)
        {
            try
            {
                return DBModule.Hoppers.CheckProponentIsAlreadyAssignedToOtherHopper(ProponentID, HopperID);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get Hopper Details By Hopper ID
        /// </summary>
        /// <param name="HopperID"></param>
        /// <returns></returns>
        public static DataSet GetDetailsForExpectedTimes()
        {
            try
            {
                return DBModule.Hoppers.GetDetailsForExpectedTimes();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all proponents
        /// </summary>
        /// <param name="ProponentID"></param>
        /// <param name="HopperID"></param>
        /// <returns></returns>
        public static DataTable GetAllProponents()
        {
            try
            {
                return DBModule.Hoppers.GetAllProponents();
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void UpdateHoppersStatus(string dtHopperStatus)
        {
            

            try
            {
                 DBModule.Hoppers.UpdateHoppersStatus(dtHopperStatus);
            }
            catch 
            {
                
            }

            
        }

        #endregion



    }
}
