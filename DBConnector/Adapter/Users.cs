using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using DBModule = Ramp.Database.Connector;
using System.Data;

namespace Ramp.DBConnector.Adapter
{
    public class Users : IUsers
    {
        #region "private variables"
        public int ID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int SecurityGroupID { get; set; }
        public string EmailID { get; set; }
        #endregion

        Ramp.Database.Connector.Users objUser;

        public Users()
        {
            objUser = new Ramp.Database.Connector.Users();
        }

        #region "Public Methods"

        public int AddUser()
        {
            try
            {
                if (objUser == null)
                {
                    objUser = new Ramp.Database.Connector.Users();
                }

                objUser.UserID = UserID;
                objUser.UserName = UserName;
                objUser.EmailID = EmailID;
                objUser.Password = Password;
                objUser.SecurityGroupID = SecurityGroupID;


                return objUser.AddUser();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateUser()
        {
            try
            {
                if (objUser == null)
                {
                    objUser = new Ramp.Database.Connector.Users();
                }

                objUser.ID = ID;
                objUser.UserName = UserName;
                objUser.SecurityGroupID = SecurityGroupID;
                objUser.EmailID = EmailID;

                return objUser.UpdateUser();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region "Public Static Methods"
        /// <summary>
        /// Get data for user to login if usename and Password exists
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static IUsers UserLoginCheck(string Username, string Password)
        {
            try
            {
                return DBModule.Users.UserLoginCheck(Username, Password);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get data for user to fet password if usename and EmailID exists
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static IUsers GetPasswordByUserNameEmailID(string Username, string EmailID)
        {
            try
            {
                return DBModule.Users.GetPasswordByUserNameEmailID(Username, EmailID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable GetAllUsers()
        {
            try
            {
                return DBModule.Users.GetAllUsers();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static bool IsDuplicateUserName(string userid)
        {
            try
            {
                return DBModule.Users.IsDuplicateUserName(userid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IUsers GetUserDetail(int ID)
        {
            try
            {
                return DBModule.Users.GetUserDetail(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
