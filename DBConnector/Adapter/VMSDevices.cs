using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Common;

namespace Ramp.DBConnector.Adapter
{
    public class VMSDevices : IVMSDevices
    {
        public short DeviceNo { get;  set; } // value 10
        public string IPAddress { get; set; }
        public bool SCL2008 { get; set; } // value true
        public string Password { get; set; }
        public short UDPPort { get; set; }
        public short LedWidth { get; set; } // value 128
        public short LedHeight { get; set; } // value 32
        public int Location { get; set; }

        public Int64 ID { get; set; }
        public string VMSStatus { get; set; }


        public int TextStartPosition { get; set; }
        public int TextLength { get; set; }
        public int TextColor { get; set; }
        public int ASCFont { get; set; }
        public int MessageTimeOut { get; set; }

        public string DefaultMessage { get; set; }

        public static List<IVMSDevices> GetAllVMSDevices()
        {
            List<IVMSDevices> lstVMSDevices = new List<IVMSDevices>();
            try
            {
                lstVMSDevices = DBModule.VMSDevices.GetAllVMSDevices();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstVMSDevices;
        }

        public static IVMSDevices GetVMSDevicesByLocation(Location loc)
        {
            IVMSDevices objVMSDevices = null;
            try
            {
                  objVMSDevices = DBModule.VMSDevices.GetVMSDevicesByLocation(loc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objVMSDevices;
        }

        public static void UpdateVMSStatus(string IPAddress, string VMSStatus)
        {
            try
            {
                DBModule.VMSDevices.UpdateVMSStatus(IPAddress, VMSStatus);
            }
            catch (Exception)
            {
                throw;
            }

        }


    }
}
