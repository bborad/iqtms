using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net;

using IDENTEC;
using IDENTEC.ILRGen3;
using IDENTEC.ILRGen3.Readers;
using IDENTEC.ILRGen3.Tags;
using IDENTEC.Utilities.BeaconData;
using Ramp.IdentacReaderLib.Common;
using Ramp.MiddlewareController.Common;
using DBModule = Ramp.DBConnector.Adapter;
using System.Data;
using IDENTEC.PositionMarker;

namespace Ramp.IdentacReaderLib.Readers
{
    public class ReaderIPortM350
    {
        iPortM350 readerToUse;

        public string IPAddress { get; set; }
        public int PortNo { get; set; }
        public bool IsConnected { get; set; }
        public bool IsReady { get; set; }
        public int TxPower { get; set; }

        public int Location { get; set; }

        public bool clearTagList { get; set; }

        public bool IsFirstTimeCall { get; set; }

        IDENTEC.DataStream stream;

        public iBusAdapter myBus;

        public object readerLock = new object();

        private ReaderIPortM350(string ipAddress, int portNo)
        {
            IDENTEC.ILRGen3.Readers.Gen3Reader.EventModuleStatusError += new iBusModule.OnModuleStatusError(Gen3Reader_EventModuleStatusError);
            IPAddress = ipAddress;
            PortNo = portNo;
            TxPower = 5;
            clearTagList = true;
            //Connect();
        }

        private ReaderIPortM350(string ipAddress, int portNo, int txPower)
        {
            IDENTEC.ILRGen3.Readers.Gen3Reader.EventModuleStatusError += new iBusModule.OnModuleStatusError(Gen3Reader_EventModuleStatusError);
            IPAddress = ipAddress;
            PortNo = portNo;
            TxPower = txPower;
            clearTagList = true;
            //Connect();
        }

        public void Connect()
        {
            try
            {
                if (ConnectToDataStream())
                {
                    FindReader(stream);
                }
                else
                {
                    RetryFindReader();
                }

            }
            catch (Exception ex)
            {
                IsConnected = false;
                WriteToEventLog("Connect -- " + ex.Message, EventLogEntryType.Error);
            }
        }

        protected bool ConnectToDataStream()
        {
            bool flagResult = false;

            try
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
            catch
            {
            }

            stream = null;
            bool UseTCP = true;
            if (UseTCP)
                stream = new TCPSocketStream(IPAddress, PortNo);
            else
            {
                stream = new SerialPortStream("COM2");
                /// the following code could be used to find an iCard CF 350
                ///int Port = IDENTEC.Readers.CFReaderSearch.FindReaderComPort();
                ///stream = new SerialPortStream(Port);
            }

            try
            {

                stream.ReadTimeout = new TimeSpan(0, 10, 0);
                stream.WriteTimeout = new TimeSpan(0, 10, 0);

                stream.Open();

                flagResult = true;
            }
            catch (Exception ex)
            {
                WriteToEventLog("ConnectToDataStream -- " + ex.Message, EventLogEntryType.Error);
            }
            return flagResult;
        }

        /// <summary>
        /// Will enumerates the reader and select the first Gen3 reader
        /// </summary>
        /// <param name="stream">The astream to use</param>
        public void FindReader(IDENTEC.DataStream stream)
        {
            try
            {
                bool readerFound = false;

                string key = IPAddress + ":" + PortNo;

                IDENTEC.PositionMarker.PositionMarker pMarker = null;
               // List<PositionMarker> lstMarkers = new List<PositionMarker>();

                MarkerDevice markerDevice;

                IsConnected = false;
                // iBusAdapter myBus = new iBusAdapter(stream);

                myBus = new iBusAdapter(stream);

                //IDENTEC.IBusDevice[] devices = myBus.GetProgrammer(60000);

                IDENTEC.IBusDevice[] devices = myBus.EnumerateBusModules();


                /// for demo we use only the first ILRGen3 reader
                foreach (IDENTEC.IBusDevice dev in devices)
                {
                    if (readerFound == false && dev is IDENTEC.ILRGen3.Readers.iPortM350)
                    {
                        readerToUse = dev as IDENTEC.ILRGen3.Readers.iPortM350;
                        if (readerToUse != null)
                        {
                            IsConnected = true;
                            readerFound = true;
                            UpdateStatusNGetPower(GetReaderStatus());
                            //break;
                        }
                    }
                    else if (dev is IDENTEC.PositionMarker.PositionMarker)
                    {
                      
                        pMarker = dev as IDENTEC.PositionMarker.PositionMarker; 

                        if (pMarker != null)
                        {
                           // lstMarkers.Add(pMarker);
                            markerDevice = new MarkerDevice();
                            lock (IdentacReaderUtility.lockMarkersList)
                            {
                                markerDevice.SerialNumber = pMarker.SerialNumber; 
                                markerDevice.readerLoc = (Location)Location;
                                markerDevice.IpPortKey = key;
                                markerDevice.LoopID = pMarker.GetLoopID();

                                markerDevice.objMarker = pMarker;

                                if (!IdentacReaderUtility.dicSerialNoMarkers.ContainsKey(markerDevice.SerialNumber))
                                {
                                    IdentacReaderUtility.dicSerialNoMarkers.Add(markerDevice.SerialNumber, markerDevice);
                                }
                                else
                                {
                                    IdentacReaderUtility.dicSerialNoMarkers[markerDevice.SerialNumber] = markerDevice;
                                }
                            }
                        }
                    }

                }

                //UpdateStatusNGetPower(GetReaderStatus());

                if (readerToUse != null)
                {
                    ConfigureReader();
                    if ((Ramp.MiddlewareController.Common.Location)Location == Ramp.MiddlewareController.Common.Location.ENTRY1)
                    {
                        ConfigureEntryGateReaderListManagement();
                    }
                    else
                    {
                        ConfigureReaderListManagement();
                    }

                    ConfigureOptionnalReaderParameter();

                    if (!IdentacReaderUtility.lstAllRFIDReaders.ContainsKey(key))
                    {
                        IdentacReaderUtility.lstAllRFIDReaders.Add(IPAddress + ":" + PortNo, null);
                    }
                }

                //if (lstMarkers != null)
                //{
                //    lock (IdentacReaderUtility.lstAllMarkers)
                //    {
                //        if (IdentacReaderUtility.lstAllMarkers.ContainsKey(Location))
                //        {
                //            IdentacReaderUtility.lstAllMarkers[Location] = lstMarkers;
                //        }
                //        else
                //        {
                //            IdentacReaderUtility.lstAllMarkers.Add(Location, lstMarkers);
                //        }
                //    }
                //}

            }
            catch (System.Net.Sockets.SocketException SocketEx)
            {
                WriteToEventLog("FindReader -- " + SocketEx.Message, EventLogEntryType.Error);
                Dispose();
                RetryFindReader();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            catch (Exception ex)
            {
                WriteToEventLog("FindReader -- " + ex.Message, EventLogEntryType.Error);
                Dispose();
                RetryFindReader();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            //if (readerToUse != null)
            //    Console.WriteLine(String.Format("Reader : {0} {1}", readerToUse.SerialNumber, readerToUse.Information));
        }

        void RetryFindReader()
        {
            try
            {
                GC.Collect();
                Thread.Sleep(500);
                IsConnected = false;

                string key = IPAddress + ":" + PortNo;

                bool readerFound = false;

                if (ConnectToDataStream())
                {

                    IDENTEC.PositionMarker.PositionMarker pMarker = null;
                    List<PositionMarker> lstMarkers = new List<PositionMarker>();

                    MarkerDevice markerDevice;

                    myBus = new iBusAdapter(stream);
                    IDENTEC.IBusDevice[] devices = myBus.EnumerateBusModules();
                    /// for demo we use only the first ILRGen3 reader

                    foreach (IDENTEC.IBusDevice dev in devices)
                    {
                        if (readerFound == false && dev is IDENTEC.ILRGen3.Readers.iPortM350)
                        {
                            readerToUse = dev as IDENTEC.ILRGen3.Readers.iPortM350;
                            if (readerToUse != null)
                            {
                                IsConnected = true;
                                readerFound = true;
                                UpdateStatusNGetPower(GetReaderStatus());
                                //break;
                            }
                        }
                        else if (dev is IDENTEC.PositionMarker.PositionMarker)
                        {
                            pMarker = dev as IDENTEC.PositionMarker.PositionMarker;
                            if (pMarker != null)
                            {
                                markerDevice = new MarkerDevice();
                                lock (IdentacReaderUtility.lockMarkersList)
                                {
                                    markerDevice.SerialNumber = pMarker.SerialNumber;
                                    markerDevice.readerLoc = (Location)Location;
                                    markerDevice.IpPortKey = key;
                                    markerDevice.LoopID = pMarker.GetLoopID();

                                    if (!IdentacReaderUtility.dicSerialNoMarkers.ContainsKey(markerDevice.SerialNumber))
                                    {
                                        IdentacReaderUtility.dicSerialNoMarkers.Add(markerDevice.SerialNumber, markerDevice);
                                    }
                                    else
                                    {
                                        IdentacReaderUtility.dicSerialNoMarkers[markerDevice.SerialNumber] = markerDevice;
                                    }
                                }
                            }
                        }

                    }

                  //  UpdateStatusNGetPower(GetReaderStatus());

                    if (readerToUse != null)
                    {  
                        ConfigureReader();
                        if ((Ramp.MiddlewareController.Common.Location)Location == Ramp.MiddlewareController.Common.Location.ENTRY1)
                        {
                            ConfigureEntryGateReaderListManagement();
                        }
                        else
                        {
                            ConfigureReaderListManagement();
                        }
                        ConfigureOptionnalReaderParameter();

                        if (!IdentacReaderUtility.lstAllRFIDReaders.ContainsKey(key))
                        {

                            IdentacReaderUtility.lstAllRFIDReaders.Add(IPAddress + ":" + PortNo, null);
                        }
                    }
                }
                else
                {
                    UpdateStatusNGetPower(GetReaderStatus());
                }
            }
            catch (Exception ex)
            {
                UpdateStatusNGetPower(GetReaderStatus());
                throw ex;
            }
        }

        void Gen3Reader_EventModuleStatusError(object module, iBusDeviceStatus status)
        {
            try
            {
                WriteToEventLog("Gen3Reader_EventModuleStatusError -- " + status.ToString(), EventLogEntryType.Error);
            }
            catch
            {
            }
        }

        /// <summary>
        /// The method changes the reader critical parameters
        /// If 1 of the parameters is not correct we may not be able to communicate with the tag
        /// </summary>
        /// <param name="reader"></param>
        public void ConfigureReader()
        {
            /// make sure that we use default parameters
            /// If we do not reset to factory default we have to make sure to set all parameters 
            /// of the reader that may have impact on the application
            /// WARNING resetting to factory default will erase all tags in the reader internal list
            readerToUse.ResetToFactoryDefault();



            /// now we set the parameters we want to use
            /// the tag default beacon baudrate is 115200 so use 115200
            /// WARNING: the beacon baudrate is not the communication baudrate see reader.RFBaudRate
            readerToUse.SetRFBeaconBaudrate(RFBaudRate.RF_115200);

            readerToUse.SetFrequency(Frequency.NorthAmerican);

            /// The wake up duration is duration of the wake up signal, the reader will send to wake up a tag
            /// 
            /// the wake up duration is on most tags 2 seconds but on special tags it could be different
            /// so changing the wake up duration must be done only if you know you have a special tag
            /// otherwise this value is defined by default to 0.
            /// a wake up duration of 0 means the SDK will automatically select the correct wakeUpDuration
            readerToUse.WakeUpDuration = new TimeSpan(0, 0, 2);
            //  ConfigureOptionnalReaderParameter(reader);
            //  ConfigureReaderListManagement(reader);

        }

        /// <summary>
        /// This shows how to set optionnal parameters
        /// changing those parameters will only have some impact on the performance
        /// </summary>
        /// <param name="reader"></param>
        public void ConfigureOptionnalReaderParameter()
        {
            /// the TX power must be set based on the antenna used and the expected range
            readerToUse.TXPower = TxPower;

            /// the following parameters are optionnal, and should be changed only for fine tuning of the system
            /// we recommend to use the lowest baudrate, 
            /// increasing the baudrate will reduce the range and communication reliability.
            /// Using a higher range is recommended only when trying to read/write a lot of data 
            /// like for example the temperature log
            /// CAUTION: changing the communication baudrate must be done only when the tag is sleeping and never in the middle of a communication
            readerToUse.RFBaudRate = RFBaudRate.RF_115200;
        }

        /// <summary>
        /// this shows how to configure the reader internal bacon list
        /// </summary>
        /// <param name="reader"></param>
        public void ConfigureReaderListManagement()
        {
            /// Beacon list management configuration
            /// all the following parameters define the management of the beacon tag list on the reader
            /// 
            /// This is the maximum number of beaconed bytes saved in the list
            /// A tag can send a maximum of 50 bytes.
            /// WARNING calling this methodt will erase all tags in the reader internal list
            readerToUse.SetDataLen(50);

            /// The following parameter defines what the reader will do when a tag has been reported:
            /// remove the tag in the list or leave it
            readerToUse.SetTagListBehavior(IDENTEC.Readers.BeaconReaders.TagListBehavior.RemoveTagsWhenReported);
            /// Defines when the tag is removed from the list
            /// If a tag has been reported and not seen for the amount of time specified in the method
            /// Then after this time the tag is removed from the reader list
            /// 0 --> tag will never be removed from the list, it will be removed only when the list is full
            readerToUse.SetTagListInhibitTime(new TimeSpan(0, 0, 10));
            /// Specify if we want to report multiple time a tag.
            /// If a tag has been reported and is still detected by the reader (beacon message)
            /// it will be reported again but only after the rereport interval time has elapsed
            /// since last time it was reported
            /// Setting a rereport interval of 10 minutes means the tag will only be reported 
            /// every 10 minutes if it is in the reader field.
            /// 0 --> the tag will never be reported unless it is removed form the list and added again
            readerToUse.SetTagReReportingInterval(new TimeSpan(0, 0, 1));

            /// This is the beacon message RSSI limit the reader will use to process the message. 
            /// By setting an RSSI level of -60, the reader will discard all beacon message with an RSSI level below -60
            /// minimum rssi level the readeer can detect is approx -100 so setting a value to -128 mean the reader will not discard any beacon message.
            readerToUse.SetTagSignalFilterLevel(-128);
        }

        public void ConfigureEntryGateReaderListManagement()
        {
            /// Beacon list management configuration
            /// all the following parameters define the management of the beacon tag list on the reader
            /// 
            /// This is the maximum number of beaconed bytes saved in the list
            /// A tag can send a maximum of 50 bytes.
            /// WARNING calling this methodt will erase all tags in the reader internal list
            readerToUse.SetDataLen(50);

            /// The following parameter defines what the reader will do when a tag has been reported:
            /// remove the tag in the list or leave it
            readerToUse.SetTagListBehavior(IDENTEC.Readers.BeaconReaders.TagListBehavior.LeaveTagWhenReported);
            /// Defines when the tag is removed from the list
            /// If a tag has been reported and not seen for the amount of time specified in the method
            /// Then after this time the tag is removed from the reader list
            /// 0 --> tag will never be removed from the list, it will be removed only when the list is full
            readerToUse.SetTagListInhibitTime(new TimeSpan(0, 0, 0));
            /// Specify if we want to report multiple time a tag.
            /// If a tag has been reported and is still detected by the reader (beacon message)
            /// it will be reported again but only after the rereport interval time has elapsed
            /// since last time it was reported
            /// Setting a rereport interval of 10 minutes means the tag will only be reported 
            /// every 10 minutes if it is in the reader field.
            /// 0 --> the tag will never be reported unless it is removed form the list and added again
            readerToUse.SetTagReReportingInterval(new TimeSpan(0, 0, 1));

            /// This is the beacon message RSSI limit the reader will use to process the message. 
            /// By setting an RSSI level of -60, the reader will discard all beacon message with an RSSI level below -60
            /// minimum rssi level the readeer can detect is approx -100 so setting a value to -128 mean the reader will not discard any beacon message.
            readerToUse.SetTagSignalFilterLevel(-128);
        }

        public bool SearchTag(string TagRFID)
        {
            bool flagResult = false;
            try
            {
                // uint tagID = Convert.ToUInt32(TagRFID,16); 
                uint tagID = Convert.ToUInt32(TagRFID);
                if (readerToUse.PingTag(tagID) != null)
                {
                    flagResult = true;
                }

            }
            catch (Exception ex)
            {
            }
            return flagResult;

        }

        public List<RFIDTag> ScanForTags(int count)
        {
            List<RFIDTag> lstRFIDTags = new List<RFIDTag>();
            try
            {
                // Gen3TagCollection lstScannedTags = readerToUse.GetBeaconTags();
                Gen3TagCollection lstScannedTags = readerToUse.ScanForTags(count, true);

                List<Gen3Tag> distinctTagList = null;

                if (lstScannedTags != null && lstScannedTags.Count > 0)
                {
                    distinctTagList = new List<Gen3Tag>(lstScannedTags.Distinct());
                    lstRFIDTags = new List<RFIDTag>();
                    foreach (Gen3Tag objGenTag in distinctTagList)
                    {
                        lstRFIDTags.Add(ConvertTags(objGenTag));
                    }
                }

            }
            catch (System.Net.Sockets.SocketException SocketEx)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            catch (IDENTEC.Readers.ReaderTimeoutException TimeOutEx)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstRFIDTags;
        }


        public List<RFIDTag> GetTags()
        {
            List<RFIDTag> lstRFIDTags = new List<RFIDTag>();
            try
            {
                lstRFIDTags = GetBeaconTags();
            }
            catch (System.Net.Sockets.SocketException SocketEx)
            {
                WriteToEventLog("GetTags -- " + SocketEx.Message, EventLogEntryType.Error);
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                lstRFIDTags = GetBeaconTags();
            }
            catch (IDENTEC.Readers.ReaderTimeoutException TimeOutEx)
            {
                WriteToEventLog("GetTags -- " + TimeOutEx.Message, EventLogEntryType.Error);
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                lstRFIDTags = GetBeaconTags();
            }
            catch (Exception ex)
            {
                WriteToEventLog("GetTags -- " + ex.Message, EventLogEntryType.Error);
            } 
         
            // Tag Read Logging ( 24/02/2011 )

            // Chnages as per Bhavesh            

            //if (lstRFIDTags.Count > 0)
            //{
            //    object value = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogging"];

            //    if (value != null && value.ToString().ToLower() == "true")
            //    {
            //        TagReadLog tagreadLog = new TagReadLog(lstRFIDTags, IPAddress);

            //        // call writelog method in seperate thread.
            //        WaitCallback wcTagReadLog = new WaitCallback(tagreadLog.WriteTagReadLogs);
            //        ThreadPool.QueueUserWorkItem(wcTagReadLog, null);
            //    } 
            //}

            return lstRFIDTags;
        }

        private List<RFIDTag> GetBeaconTags()
        {
            List<RFIDTag> lstRFIDTags = new List<RFIDTag>();

            Gen3TagCollection lstScannedTags = readerToUse.GetBeaconTags();
            List<Gen3Tag> distinctTagList = null;
            //  Gen3TagCollection lstScannedTags =readerToUse.ScanForTags(3, true);  

            if (lstScannedTags != null && lstScannedTags.Count > 0)
            {
                if ((Ramp.MiddlewareController.Common.Location)Location == Ramp.MiddlewareController.Common.Location.ENTRY1 && clearTagList)
                {
                    readerToUse.ClearTagList();
                }

                distinctTagList = new List<Gen3Tag>(lstScannedTags.Distinct());
                lstRFIDTags = new List<RFIDTag>();

                //foreach (Gen3Tag objGenTag in distinctTagList)
                //{  
                //    lstRFIDTags.Add(ConvertTags(objGenTag));                    
                //}

                StringBuilder strtags = new StringBuilder();
                strtags.Append("--------------------------------------------------- " + DateTime.Now.ToString() + " ---------------------------------------------" + Environment.NewLine);
                strtags.Append("TagID, NewMarker LoopID, NewMarker PositionTime, OldMarker LoopID, OldMarker PositionTime, TimeFirstSeen, TimeLastSeen, BatteryStatus,Location,Reader IP" + Environment.NewLine);              
                // Change on 28th MArch 2011 ( to add reader locaiton and IP Address in logs)

                RFIDTag objtag;

                foreach (Gen3Tag objGenTag in distinctTagList)
                {
                    objtag = ConvertTags(objGenTag);
                    lstRFIDTags.Add(objtag);
                    strtags.Append(objGenTag.SerialNumber.ToString() + ", " + objtag.NewMarkerID.ToString() + ", " + objtag.MarkerNewPositionTime.ToString() + ", " + objtag.OldMarkerID.ToString() + ", " + objtag.MarkerOldPositionTime.ToString() + ", " + objGenTag.TimeFirstSeen.ToString() + ", " + objGenTag.TimeLastSeen.ToString() + ", " + objGenTag.BatteryStatus.ToString() + ", " + Location.ToString() + ", " + IPAddress.ToString() + Environment.NewLine);
                    // Change on 28th MArch 2011 ( to add reader locaiton and IP Address in logs)
                }

                strtags.Append("----------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);

                 object value = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogging"];

                 if (lstRFIDTags.Count > 0 && value != null && value.ToString().ToLower() == "true")
                 {
                     try
                     {
                         object tagReadDirPath = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogDir"];
                         if (tagReadDirPath != null)
                         {
                             if (!System.IO.Directory.Exists(tagReadDirPath.ToString()))
                             {
                                 System.IO.Directory.CreateDirectory(tagReadDirPath.ToString());
                             }

                             string ip = IPAddress;
                             string fileName = tagReadDirPath.ToString() + ip.Replace('.', '-') + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".tag";

                             System.IO.File.AppendAllText(fileName, strtags.ToString());
                         }
                     }
                     catch
                     {
                     }
                 }

                
            }

            return lstRFIDTags;

        }

       

        protected RFIDTag ConvertTags(Gen3Tag tag)
        {
            RFIDTag objtag = new RFIDTag();

            // Changes for TagRead logging ( as per Bhavesh ) 
            StringBuilder strTags = new StringBuilder();
            
            objtag.TagID = tag.SerialNumber.ToString();
            objtag.SerialLabel = tag.SerialLabel;
            List<BeaconInfo> infos = BeaconDataConverter.Convert(tag);
            LFMarker marker = null;
            foreach (BeaconInfo info in infos)
            {
                marker = info as LFMarker;
                if (marker != null)
                {
                    objtag.NewMarkerID = marker.NewerPosition.LoopID;
                    objtag.OldMarkerID = marker.OlderPosition.LoopID;
                    objtag.MarkerNewPositionTime = marker.NewerPosition.PositionTime;
                    objtag.MarkerOldPositionTime = marker.OlderPosition.PositionTime;
                    objtag.MarkerName = marker.Name;
                    break;
                }

            }
            objtag.TimeFirstSeen = tag.TimeFirstSeen;
            objtag.TimeLastSeen = tag.TimeLastSeen;

            objtag.SerialNo = Convert.ToInt32(tag.SerialNumber);
            if (tag.BatteryStatus == BatteryStatus.Good)
                objtag.BatteryStatus = TagBatteryStatus.Good;
            else if (tag.BatteryStatus == BatteryStatus.Indeterminate)
                objtag.BatteryStatus = TagBatteryStatus.Intermediate;
            else
                objtag.BatteryStatus = TagBatteryStatus.Poor; 

            return objtag;
        }

        public bool IsReaderConnected()
        {
            bool flagResult = false;

            try
            {
                if (readerToUse != null)
                {
                    if (readerToUse.DataStream.IsOpen)
                    {
                        if (!readerToUse.GetStatus().HasError)
                        {
                            flagResult = true;
                            IsConnected = true;
                            return flagResult;
                        }
                        else
                        {
                            FindReader(stream);
                            if (readerToUse != null)
                            {
                                flagResult = true;
                            }
                        }
                    }
                    else
                    {
                        Connect();
                        flagResult = true;
                    }

                }

            }
            catch (Exception ex)
            {
                flagResult = false;
            }
            return flagResult;
        }

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts)
        {
            bool resultFlag = false;
            try
            {
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, 50);
            }
            catch (System.Net.Sockets.SocketException)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, 50);
            }
            catch (IDENTEC.Readers.ReaderTimeoutException)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, 50);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultFlag;
        }

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int amount)
        {
            bool resultFlag = false;
            try
            {
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, amount);
            }
            catch (System.Net.Sockets.SocketException socEx)
            {
                WriteToEventLog("BlinkLED -- " + socEx.Message, EventLogEntryType.Error);
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, amount);
            }
            catch (IDENTEC.Readers.ReaderTimeoutException timeOutEx)
            {
                WriteToEventLog("BlinkLED -- " + timeOutEx.Message, EventLogEntryType.Error);
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
                resultFlag = TryBlinkLED(tagToBlink, ledColor, ts, amount);
            }
            catch (Exception ex)
            {
                WriteToEventLog("BlinkLED -- " + ex.Message, EventLogEntryType.Error);
            }
            return resultFlag;
        }

        private bool TryBlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int amount)
        {
            bool resultFlag = false;
           // Gen3Tag tag = readerToUse.PingTag(Convert.ToUInt32(tagToBlink));

            Gen3Tag tag = new iQ350Logger();
            tag.SerialNumber = Convert.ToUInt32(tagToBlink);

            LEDColor tagLedColor = LEDColor.ALL_LED;
            if (tag == null)
                return false;
            else
            {
                if (ledColor == TagLedColor.All_LED)
                    tagLedColor = LEDColor.ALL_LED;
                else if (ledColor == TagLedColor.GREEN)
                    tagLedColor = LEDColor.GREEN;
                else if (ledColor == TagLedColor.ORANGE)
                    tagLedColor = LEDColor.ORANGE;
                else if (ledColor == TagLedColor.RED)
                    tagLedColor = LEDColor.RED;
                else if (ledColor == TagLedColor.YELLOW)
                    tagLedColor = LEDColor.YELLOW;


                resultFlag = IdentacReaderUtility.BlinkLED(tag, readerToUse, tagLedColor, ts, amount);

                return resultFlag;
            }
        }

        public string GetSerialNo()
        {
            try
            {
                return readerToUse.SerialNumber;
            }
            catch (Exception ex)
            {
                WriteToEventLog("GetSerialNo -- " + ex.Message, EventLogEntryType.Error);
                return "";
            }
        }

        public string GetReaderStatus()
        {
            try
            {
                if (IsConnected)
                {
                    return "ok";
                }
                else
                {
                    return "Fault";
                }
            }
            catch
            {
                return "Fault";
            }
        }

        public void ClearTagList()
        {
            try
            {
                readerToUse.ClearTagList();
            }
            catch (System.Net.Sockets.SocketException SocketEx)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            catch (IDENTEC.Readers.ReaderTimeoutException TimeOutEx)
            {
                Dispose();
                Connect();
                IdentacReaderUtility.lstAllReaders[IPAddress + ":" + PortNo] = this;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ~ReaderIPortM350()
        {
            //try
            //{
            //    stream.Close();
            //    stream = null;
            //    readerToUse.Disconnect();

            //}
            //catch (Exception ex)
            //{
            //    stream = null;
            //    readerToUse = null;
            //}
        }

        public void Dispose()
        {
            try
            {
                stream.Close();
                readerToUse.Disconnect();

                stream = null;
                readerToUse = null;

            }
            catch (Exception ex)
            {
                stream = null;
                readerToUse = null;
            }
        }

        public static ReaderIPortM350 GetReader(string ipAddress, int portNo, int txPower)
        {
          
            ReaderIPortM350 objReader = null;
            try
            {
                string key = ipAddress + ":" + portNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key,new object());
                }

                if (IdentacReaderUtility.lstAllReaders.ContainsKey(key))
                {
                    objReader = IdentacReaderUtility.lstAllReaders[key];

                    if (objReader.readerToUse == null)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, txPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }
                    else if (objReader.readerToUse.DataStream == null)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, txPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }
                    else if (!objReader.readerToUse.DataStream.IsOpen)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, txPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }

                }
                else
                {
                    objReader = new ReaderIPortM350(ipAddress, portNo, txPower);
                    objReader.Connect();
                    IdentacReaderUtility.lstAllReaders.Add(key, objReader);
                }

            }
            catch (Exception ex)
            {
                WriteToEventLog("GetReader -- " + ex.Message, EventLogEntryType.Error);
            }
            return objReader;
        }

        public static ReaderIPortM350 GetReader(string ipAddress, int portNo)
        {
            ReaderIPortM350 objReader = null;
             
            try
            {
                string key = ipAddress + ":" + portNo.ToString();

                //if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                //{
                //    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                //}

                if (IdentacReaderUtility.lstAllReaders.ContainsKey(key))
                {
                    objReader = IdentacReaderUtility.lstAllReaders[key]; 

                    Ping p = new System.Net.NetworkInformation.Ping();

                    if (p.Send(ipAddress.Trim(), IdentacReaderUtility.pingTimeOut).Status != IPStatus.Success)
                    {
                        objReader.IsConnected = false;                        
                    }
                    else if (objReader.readerToUse == null)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, objReader.TxPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }
                    else if (objReader.readerToUse.DataStream == null)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, objReader.TxPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }
                    else if (!objReader.readerToUse.DataStream.IsOpen)
                    {
                        objReader.Dispose();
                        objReader = new ReaderIPortM350(ipAddress, portNo, objReader.TxPower);
                        objReader.Connect();
                        IdentacReaderUtility.lstAllReaders[key] = objReader;
                    }
                    else
                    {
                        objReader.IsConnected = true;
                    }

                }
                else
                {
                    objReader = new ReaderIPortM350(ipAddress, portNo);

                    Ping p = new System.Net.NetworkInformation.Ping();

                    if (p.Send(ipAddress.Trim(), IdentacReaderUtility.pingTimeOut).Status != IPStatus.Success)
                    {
                        objReader.IsConnected = false;
                    }
                    else
                    {
                        objReader.Connect();
                    }

                    IdentacReaderUtility.lstAllReaders.Add(key, objReader);
                }

            }
            catch (Exception ex)
            {
                objReader = new ReaderIPortM350(ipAddress, portNo);
                objReader.IsConnected = false;
                WriteToEventLog("GetReader -- " + ex.Message, EventLogEntryType.Error);
            }
            return objReader;
        }

        public static void ConfigureReaders(DataTable dtReaders)
        {
            string ipAddress = string.Empty;
            int portNo = 0;
            int txPower = 5;
            string key = string.Empty;

            ReaderIPortM350 objReader = null;

            try
            {
                foreach (DataRow dr in dtReaders.Rows)
                {
                    ipAddress = dr["IpAddress"].ToString();
                    portNo = Convert.ToInt32(dr["PortNo"]);
                    txPower = Convert.ToInt32(dr["Power"]);

                    key = ipAddress + ":" + portNo.ToString();

                    if (!IdentacReaderUtility.lstAllReaders.ContainsKey(key))
                    {
                        objReader = new ReaderIPortM350(ipAddress, portNo);
                        objReader.TxPower = txPower;
                        IdentacReaderUtility.lstAllReaders.Add(key, objReader);
                    }
                }
                key = ipAddress + ":" + portNo.ToString();
            }
            catch
            {
            }
        }

        protected void UpdateStatusNGetPower(string status)
        {
            try
            {
                DataTable dtResult = null;

                dtResult = DBModule.Readers.UpdateStatus(IPAddress, status);

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["TxPower"] != DBNull.Value)
                    {
                        TxPower = Convert.ToInt32(dtResult.Rows[0]["TxPower"]);
                    }
                    else
                    {
                        TxPower = 5;
                    }
                    if (dtResult.Rows[0]["Location"] != DBNull.Value)
                    {
                        Location = Convert.ToInt32(dtResult.Rows[0]["Location"]);
                    }
                    else
                    {
                        Location = 5;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToEventLog("UpdateStatusNGetPower -- " + ex.Message, EventLogEntryType.Error);
            }
        }

        private static void WriteToEventLog(string strLogEntry, EventLogEntryType eType)
        {
            try
            {
                string strSource = "IQTMS Reader Functionality"; //name of the source
                string strLogType = "IQTMSLog"; //type of the log
                string strMachine = "."; //machine name

                if (!EventLog.SourceExists(strSource, strMachine))
                {
                    EventLog.CreateEventSource(strSource, strLogType);
                }

                EventLog eLog = new EventLog(strLogType, strMachine, strSource);
               // eLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 0);
                eLog.WriteEntry(strLogEntry, eType, 1000);
            }
            catch
            {
            }
        }
    }

    public class MarkerDevice
    {
        public IDENTEC.PositionMarker.PositionMarker objMarker;
        public Location readerLoc;
        public int LoopID;
        public string SerialNumber;
        public string IpPortKey;

        public MarkerDevice()
        {
            LoopID = -999;
            objMarker = null;
        }

    }
}
