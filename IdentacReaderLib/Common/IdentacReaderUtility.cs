using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IDENTEC;
using IDENTEC.ILRGen3;
using IDENTEC.ILRGen3.Readers;
using IDENTEC.ILRGen3.Tags;
using Ramp.IdentacReaderLib.Readers;
using Ramp.MiddlewareController.Common;
using System.Data;

using NLog;

namespace Ramp.IdentacReaderLib.Common
{
    public static class IdentacReaderUtility
    {
        public static int pingTimeOut;

        static IdentacReaderUtility()
        {
            object value = System.Configuration.ConfigurationSettings.AppSettings["PingTimeOut"]; 

            if (value != null)
            {
                try
                {
                    pingTimeOut = Convert.ToInt32(value);
                }
                catch
                {
                    pingTimeOut = 1000;
                }

            }
        }

        public static object lockMarkersList = new object();

        public static Dictionary<string, object> lstIpAddressLocks = new Dictionary<string, object>();

        public static Dictionary<string, iPortM350> lstAllRFIDReaders = new Dictionary<string, iPortM350>();

        public static Dictionary<string, ReaderIPortM350> lstAllReaders = new Dictionary<string, ReaderIPortM350>();

        public static Dictionary<int, List<IDENTEC.PositionMarker.PositionMarker>> lstAllMarkers = new Dictionary<int, List<IDENTEC.PositionMarker.PositionMarker>>();

        public static Dictionary<string, MarkerDevice> dicSerialNoMarkers = new Dictionary<string, MarkerDevice>();

        public static bool BlinkLED(Gen3Tag tagToBlink, Gen3Reader readerToUse, LEDColor ledColor, TimeSpan ts)
        {
            try
            {
                return tagToBlink.BlinkLED(readerToUse, ledColor, ts, 50);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static bool BlinkLED(Gen3Tag tagToBlink, Gen3Reader readerToUse, LEDColor ledColor, TimeSpan ts, int amount)
        {
            try
            {
                return tagToBlink.BlinkLED(readerToUse, ledColor, ts, (byte)amount, 2);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void DisposeAllReaders()
        {
            try
            {
                foreach (string key in lstAllReaders.Keys)
                {
                    lstAllReaders[key].Dispose();
                }

                lstAllReaders.Clear();
            }
            catch (Exception ex)
            {
			}
        }

        public static void DisposeReader(string IPAddress, int PortNo)
        {
            try
            {
                string key = IPAddress + ":" + PortNo.ToString();
                if (lstAllReaders.ContainsKey(key))
                {
                    lstAllReaders[key].Dispose();
                    lstAllReaders.Remove(key);
                }
            }
            catch (Exception ex)
            {
			}
        }

        public static DataTable GetMarkersStatus()
        {
            DataTable dtMarkers = new DataTable("dtMarkers");
            DataColumn[] dcc = new DataColumn[] { new DataColumn("LoopID",typeof(System.String)),
                                                    new DataColumn("SerialNo",typeof(System.String)),
                                                    new DataColumn("Location",typeof(System.String)),
                                                    new DataColumn("Status",typeof(System.String)),
            };

            dtMarkers.Columns.AddRange(dcc);

            try
            {
                //List<Int32> locations;
                //lock (lstAllMarkers)
                //{
                //    locations = lstAllMarkers.Keys.ToList();
                //}

                // Location eLoc;

                //  List<IDENTEC.PositionMarker.PositionMarker> markers = null; 

                DataRow dr = null;

                object ipLock;

                int loopID = 0;

                lock (lockMarkersList)
                {
                    foreach (MarkerDevice objMarkerDevice in dicSerialNoMarkers.Values)
                    {

                        dr = dtMarkers.NewRow();
                        dr["Location"] = objMarkerDevice.readerLoc.ToString();
                        try
                        {
                            dr["SerialNo"] = objMarkerDevice.SerialNumber;
                            loopID = objMarkerDevice.LoopID;

                            ipLock = lstIpAddressLocks[objMarkerDevice.IpPortKey];

                            if (loopID <= 0)
                            {
                                lock (ipLock)
                                {
                                    loopID = objMarkerDevice.objMarker.GetLoopID();
                                }
                                objMarkerDevice.LoopID = loopID;
                            }

                            dr["LoopID"] = loopID.ToString();

                            if (objMarkerDevice.objMarker.Status.LoopError)
                            {
                                dr["Status"] = "Fault";
                            }
                            else
                            {
                                dr["Status"] = "ok";
                            }
                        }
                        catch
                        {
                            dr["LoopID"] = "---";
                            dr["SerialNo"] = objMarkerDevice.SerialNumber;
                            dr["Status"] = "Fault";
                        }

                        dtMarkers.Rows.Add(dr);
                    }
                }


                //foreach (int loc in locations)
                //{
                //    lock (lstAllMarkers)
                //    {
                //        markers = lstAllMarkers[loc];
                //    }

                //    eLoc = (Location)loc;

                //    if (markers != null && markers.Count > 0)
                //    {
                //        foreach (IDENTEC.PositionMarker.PositionMarker objMarker in markers)
                //        {
                //            dr = dtMarkers.NewRow();
                //            dr["Location"] = eLoc.ToString();
                //            try
                //            {

                //                string deviceinfo = objMarker.iBusAdapter.QueryDeviceInformation(objMarker.Address);
                //                dr["LoopID"] = objMarker.GetLoopID().ToString();
                //                dr["SerialNo"] = objMarker.SerialNumber;
                //                if (objMarker.Status.LoopError)
                //                {
                //                    dr["Status"] = "Fault";
                //                }
                //                else
                //                {
                //                    dr["Status"] = "ok";
                //                }
                //            }
                //            catch
                //            {
                //                dr["LoopID"] = "---";
                //                dr["SerialNo"] = objMarker.SerialNumber;
                //                dr["Status"] = "Fault";
                //            }

                //            dtMarkers.Rows.Add(dr);
                //        }
                //    }
                //}


                dtMarkers.AcceptChanges();

            }
            catch (Exception ex)
            {
			}
            return dtMarkers;
        }

        public static DataTable _GetMarkersStatus()
        {
            DataTable dtMarkers = new DataTable("dtMarkers");
            DataColumn[] dcc = new DataColumn[] { new DataColumn("LoopID",typeof(System.String)),
                                                    new DataColumn("SerialNo",typeof(System.String)),
                                                    new DataColumn("Location",typeof(System.String)),
                                                    new DataColumn("Status",typeof(System.String)),
            };

            dtMarkers.Columns.AddRange(dcc);

            try
            {
                DataRow dr = null;

                Location eLoc;

                List<IDENTEC.PositionMarker.PositionMarker> markers = null;
                IDENTEC.PositionMarker.PositionMarker objMarker;


                foreach (ReaderIPortM350 reader in lstAllReaders.Values)
                {
                    eLoc = (Location)reader.Location;

                    IBusDevice[] devices = reader.myBus.EnumerateBusModules();
                    foreach (IBusDevice dev in devices)
                    {
                        if (dev is IDENTEC.PositionMarker.PositionMarker)
                        {
                            objMarker = (IDENTEC.PositionMarker.PositionMarker)dev;
                            dr = dtMarkers.NewRow();
                            dr["Location"] = eLoc.ToString();
                            try
                            {

                                string deviceinfo = objMarker.iBusAdapter.QueryDeviceInformation(objMarker.Address);
                                dr["LoopID"] = objMarker.GetLoopID().ToString();
                                dr["SerialNo"] = objMarker.SerialNumber;
                                if (objMarker.Status.LoopError)
                                {
                                    dr["Status"] = "Fault";
                                }
                                else
                                {
                                    dr["Status"] = "ok";
                                }
                            }
                            catch
                            {
                                dr["LoopID"] = "---";
                                dr["SerialNo"] = objMarker.SerialNumber;
                                dr["Status"] = "Fault";
                            }

                            dtMarkers.Rows.Add(dr);
                        }
                    }
                }


                //List<Int32> locations = lstAllMarkers.Keys.ToList();

                //foreach (int loc in locations)
                //{
                //    markers = lstAllMarkers[loc];

                //    eLoc = (Location)loc;

                //    if (markers != null && markers.Count > 0)
                //    {
                //        foreach (IDENTEC.PositionMarker.PositionMarker objMarker in markers)
                //        {
                //            dr = dtMarkers.NewRow();
                //            dr["Location"] = eLoc.ToString();
                //            try
                //            {

                //                string deviceinfo = objMarker.iBusAdapter.QueryDeviceInformation(objMarker.Address);
                //                dr["LoopID"] = objMarker.GetLoopID().ToString();
                //                dr["SerialNo"] = objMarker.SerialNumber;
                //                if (objMarker.Status.LoopError)
                //                {
                //                    dr["Status"] = "Fault";
                //                }
                //                else
                //                {
                //                    dr["Status"] = "ok";
                //                }
                //            }
                //            catch
                //            {
                //                dr["LoopID"] = "---";
                //                dr["SerialNo"] = objMarker.SerialNumber;
                //                dr["Status"] = "Fault";
                //            }

                //            dtMarkers.Rows.Add(dr);
                //        }
                //    }
                //}



                //IDENTEC.PositionMarker.PositionMarker pMarker = null;
                //foreach (ReaderIPortM350 reader in lstAllReaders.Values)
                //{
                //    eLoc = (Location)reader.Location;
                //    IDENTEC.IBusDevice[] devices = reader.myBus.EnumerateBusModules();
                //    /// for demo we use only the first ILRGen3 reader
                //    foreach (IDENTEC.IBusDevice dev in devices)
                //    {
                //        if (dev is IDENTEC.PositionMarker.PositionMarker)
                //        {
                //            pMarker = dev as IDENTEC.PositionMarker.PositionMarker;
                //            if (pMarker != null)
                //            {
                //               // markers.Add(pMarker);

                //                dr = dtMarkers.NewRow();
                //                dr["Location"] = eLoc.ToString();
                //                try
                //                {
                //                    dr["LoopID"] = pMarker.GetLoopID().ToString();
                //                    //dr["LoopID"] = pMarker.SerialNumber;
                //                    if (pMarker.Status.LoopError)
                //                    {
                //                        dr["Status"] = "Fault";
                //                    }
                //                    else
                //                    {
                //                        dr["Status"] = "OK";
                //                    }
                //                }
                //                catch
                //                {
                //                    dr["LoopID"] = pMarker.SerialNumber;
                //                    dr["Status"] = "Fault";
                //                }
                //                dtMarkers.Rows.Add(dr); 
                //            }
                //        }

                //    }            

                //}


                dtMarkers.AcceptChanges();

            }
            catch (Exception ex)
            {
			}
            return dtMarkers;
        }

        public static DataTable GetMarkersStatus(int loc)
        {
            DataTable dtMarkers = new DataTable("dtMarkers");
            DataColumn[] dcc = new DataColumn[] { new DataColumn("LoopID",typeof(System.String)),
                                                    new DataColumn("Location",typeof(System.Int32)),
                                                    new DataColumn("Status",typeof(System.String)),
            };
            dtMarkers.Columns.AddRange(dcc);

            try
            {
                List<Int32> locations = lstAllMarkers.Keys.ToList();

                DataRow dr = null;

                List<IDENTEC.PositionMarker.PositionMarker> markers = null;

                Location eLoc = (Location)loc;

                if (lstAllMarkers.ContainsKey(loc))
                {
                    markers = lstAllMarkers[loc];
                    if (markers != null && markers.Count > 0)
                    {
                        foreach (IDENTEC.PositionMarker.PositionMarker objMarker in markers)
                        {
                            dr = dtMarkers.NewRow();
                            dr["Location"] = eLoc.ToString();
                            try
                            {

                                string deviceinfo = objMarker.iBusAdapter.QueryDeviceInformation(objMarker.Address);
                                dr["LoopID"] = objMarker.GetLoopID().ToString();
                                dr["SerialNo"] = objMarker.SerialNumber;
                                if (objMarker.Status.LoopError)
                                {
                                    dr["Status"] = "Fault";
                                }
                                else
                                {
                                    dr["Status"] = "ok";
                                }
                            }
                            catch
                            {
                                dr["LoopID"] = "---";
                                dr["SerialNo"] = objMarker.SerialNumber;
                                dr["Status"] = "Fault";
                            }

                            dtMarkers.Rows.Add(dr);
                        }
                    }
                }

                dtMarkers.AcceptChanges();

            }
            catch (Exception ex)
            {}
                return dtMarkers;
            
        }

        public static DataTable SetMarkersLoopID(DataTable dtMarkers)
        {

            DataTable dtMarkerInfo = new DataTable("dtMarkerInfo");
            DataColumn[] dcc = new DataColumn[] { new DataColumn("SerialNo"),
                                                  new DataColumn("ActualLoopID"),
                                                  new DataColumn("CurrentLoopID"),
                                                  new DataColumn("Status"),
                                                  new DataColumn("Error")
            };

            dtMarkerInfo.Columns.AddRange(dcc);

            DataRow drInfo;

            MarkerDevice marker;
            string serialNo;
            int loopID, currLoopID;

            object ipLock;

            try
            {
                foreach (DataRow dr in dtMarkers.Rows)
                {
                    if (dr["LoopId"] != DBNull.Value)
                    {
                        drInfo = dtMarkerInfo.NewRow();
                        drInfo["ActualLoopID"] = dr["LoopId"].ToString();
                        try
                        {
                            loopID = Convert.ToInt32(dr["LoopId"]);
                            if (loopID != 0)
                            {
                                if (dr["SerialNo"] != DBNull.Value)
                                {
                                    serialNo = dr["SerialNo"].ToString();
                                    drInfo["SerialNo"] = serialNo;

                                    lock (lockMarkersList)
                                    {
                                        marker = null;
                                        if (dicSerialNoMarkers.ContainsKey(serialNo.Trim()))
                                        {
                                            marker = dicSerialNoMarkers[serialNo.Trim()];
                                        }
                                    }

                                    if (marker != null)
                                    {
                                        ipLock = lstIpAddressLocks[marker.IpPortKey];

                                        lock (ipLock)
                                        {
                                            //currLoopID = marker.GetLoopID();
                                            currLoopID = marker.LoopID;

                                            if (currLoopID <= 0)
                                            {
                                                currLoopID = marker.objMarker.GetLoopID();
                                            }

                                            drInfo["CurrentLoopID"] = currLoopID.ToString();

                                            if (currLoopID != loopID)
                                            {
                                                marker.objMarker.SetLoopID(loopID);
                                                marker.LoopID = loopID;
                                                dicSerialNoMarkers[serialNo.Trim()] = marker;
                                                drInfo["Status"] = "LoopID reset";
                                            }
                                            else
                                            {
                                                drInfo["Status"] = "No Mismatch";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        drInfo["Status"] = "Marker not found.";
                                    }

                                }
                                else
                                {

                                }
                            }

                            drInfo["Error"] = "No Error";
                        }
                        catch (Exception ex)
                        {
                            drInfo["Error"] = ex.Message;
                        }

                        dtMarkerInfo.Rows.Add(drInfo);
                    }

                }
                dtMarkerInfo.AcceptChanges();
            }
            catch (Exception ex)
            {
                return null;
            }

            return dtMarkerInfo;
        }         

    }


    // Tag Read Logging ( 24/02/2011 )
    public class TagReadLog
    {
        static Dictionary<string, object> LogFileLocks = new Dictionary<string, object>();

        List<RFIDTag> lstTagReads;
        string IpAddress;

        public TagReadLog(List<RFIDTag> list, string ip)
        {
            lstTagReads = list;
            IpAddress = ip; 
        }

        public  void WriteTagReadLogs(object obj)
        {
            try
            {
                object objLock;
                if (LogFileLocks.ContainsKey(IpAddress))
                {
                    objLock = LogFileLocks[IpAddress];
                }
                else
                {
                    objLock = new object();
                    LogFileLocks[IpAddress] = objLock;
                }

                object tagReadDirPath = System.Configuration.ConfigurationSettings.AppSettings["TagReadLogDir"];
                if (tagReadDirPath != null)
                {
                    if (!System.IO.Directory.Exists(tagReadDirPath.ToString()))
                    {
                        System.IO.Directory.CreateDirectory(tagReadDirPath.ToString());
                    } 

                    string fileName = tagReadDirPath.ToString() + IpAddress.Replace('.', '-') + "_" + DateTime.Today.ToString("MM-dd-yyyy") + ".txt";

                    StringBuilder tagReadData = new StringBuilder();

                    tagReadData.Append("--------------------------------------------------- " + DateTime.Now.ToString() + " ---------------------------------------------" + Environment.NewLine);

                    foreach (RFIDTag tag in lstTagReads)
                    {
                        tagReadData.Append(tag.TagID + ", " + tag.NewMarkerID.ToString() + ", " + tag.MarkerNewPositionTime.ToString() + ", " + tag.OldMarkerID.ToString() + ", " + tag.MarkerOldPositionTime.ToString() + ", " + tag.TimeFirstSeen.ToString() + ", " + tag.TimeLastSeen.ToString() + ", " + tag.BatteryStatus.ToString() + Environment.NewLine);
                    }

                    tagReadData.Append("----------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
                    lock (objLock)
                    {
                        bool fileExist = false;
                        if (System.IO.File.Exists(fileName))
                        {
                            fileExist = true;
                        }

                        System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName, true);
                        sw.NewLine = Environment.NewLine;
                        if (!fileExist)
                            sw.WriteLine("TagID, NewMarker LoopID, NewMarker PositionTime, OldMarker LoopID, OldMarker PositionTime, TimeFirstSeen, TimeLastSeen, BatteryStatus" + Environment.NewLine);

                        sw.Write(tagReadData.ToString());
                        sw.Close();
                    }
                }

            }
            catch(Exception)
            {
            }
        }
    }

}
