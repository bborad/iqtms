using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;


namespace Ramp.VMSHardWare
{
    public static class SCLAPI
    {
        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_NetInitial(short DevID, String Password, String IP, Int32 TimeOut, Int32 Retry, short UDPPort, bool SCL2008);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_ComInitial(short DevID, Int32 ComPort, Int32 Baudrate, Int32 LedNum, Int32 TimeOut, Int32 Retry, bool SCL2008);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_Close(short DevID);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_SetRemoteIP(short DevID, string sIP);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_SendFile(short DevID, Int32 DrvNo, string Path, string FileName);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_LoadFile(short DevID, Int32 DrvNo, string FileName);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_SaveFile(short DevID, Int32 DrvNo, string FileName, Int32 Length, Int32 Da, Int32 Ti);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_RemoveFile(short DevID, Int32 DrvNo, string FileName);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_SendData(short DevID, Int32 Offset, Int32 SendBytes, ref byte Buff);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_ReceiveData(short DevID, Int32 Offset, Int32 ReadBytes, ref byte Buff);

        [DllImport("SCL_API_Stdcall", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern bool SCL_ShowString(short DevID, ref short TextInfo, string Str_);


        //Declare Function SCL_GetRunTimeInfo Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByRef Buff512Bytes As Byte) As Boolean
        //Declare Function SCL_GetPlayInfo Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByRef PlayInfo As Byte) As Boolean
        //Declare Function SCL_LedShow Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByVal OnOff As Boolean) As Boolean
        //Declare Function SCL_SetExtSW Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByVal OnOff As Short) As Boolean


        //Declare Function SCL_Reset Lib "SCL_API_Stdcall" (ByVal DevID As Short) As Boolean
        //Declare Function SCL_Replay Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByVal Drv As Integer, ByVal Index As Integer) As Boolean
        //Declare Function SCL_SetTimer Lib "SCL_API_Stdcall" (ByVal DevID As Short) As Boolean  


        // Declare Function SCL_SetLEDNum Lib "SCL_API_Stdcall" (ByVal DevID As Short, ByVal LedNum As Integer) As Boolean


        public static bool SCLNetInitial(short DevID, String Password, String IP, Int32 TimeOut, Int32 Retry, short UDPPort, bool SCL2008)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_NetInitial(DevID, Password, IP, TimeOut, Retry, UDPPort, SCL2008);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLComInitial(short DevID, Int32 ComPort, Int32 Baudrate, Int32 LedNum, Int32 TimeOut, Int32 Retry, bool SCL2008)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_ComInitial(DevID, ComPort, Baudrate, LedNum, TimeOut, Retry, SCL2008);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLClose(short DevID)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_Close(DevID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLSetRemoteIP(short DevID, string sIP)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_SetRemoteIP(DevID, sIP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLSendFile(short DevID, Int32 DrvNo, string Path, string FileName)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_SendFile(DevID,   DrvNo,   Path,   FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLLoadFile(short DevID, Int32 DrvNo, string FileName)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_LoadFile(DevID, DrvNo, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLSaveFile(short DevID, Int32 DrvNo, string FileName, Int32 Length, Int32 Da, Int32 Ti)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_SaveFile(DevID, DrvNo, FileName, Length, Da, Ti);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLRemoveFile(short DevID, Int32 DrvNo, string FileName)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_RemoveFile(DevID, DrvNo, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLSendData(short DevID, Int32 Offset, Int32 SendBytes, ref byte Buff)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_SendData(DevID, Offset, SendBytes, ref Buff);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLReceiveData(short DevID, Int32 Offset, Int32 ReadBytes, ref byte Buff)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_ReceiveData(DevID, Offset, ReadBytes, ref Buff);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static bool SCLShowString(short DevID, TextInfoType TextInfo, string Str_)
        {
            bool flagResult = false;

            try
            {
                flagResult = SCL_ShowString(DevID, ref TextInfo.Left_, FormatMessage(Str_));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        static string FormatMessage(string Str_)
        {
            StringBuilder formatString = new StringBuilder("`A1");
            formatString.Append(Str_);
            return formatString.ToString();
        }
    }
    
    [StructLayout(LayoutKind.Sequential)]
    public struct TextInfoType
    {
        public short Left_;
        public short Top_;
        public short Width_;
        public short Height_;
        public short Color;
        public short ASCFont;
        public short HZFont;
        public short XPos;
        public short YPos; 

    }

}
