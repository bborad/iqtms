

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBModule = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Common;
using Ramp.RFIDHardWare.Connector;
//using Ramp.IdentacReaderLib.Readers;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Configuration;
using System.Net.Security;
using System.Data;
//using NLog;
 

namespace Ramp.RFIDHardWare.Connector
{
    public class Reader : IReader
    {
        #region "private variables"
        public string ReaderSerialNo { get; set; }
        public string ReaderName { get; set; }
        public string IPAddress { get; set; }
        public int Location { get; set; }
        public int PortNo { get; set; }
        public int TxPower { get; set; }
      //  public ReaderIPortM350 objReader;
        static Ramp.RFIDHardWare.IQTMSReaderService.ReaderService objReader;

        static string WebURL { get; set; }

        static string UserName { get; set; }
        static string Password { get; set; }
       

        #endregion

         //private static Logger log = LogManager.GetLogger("Ramp.RFIDHardWare.Connector.Reader");

        public Reader(string strIPAddress, int iPort)
        {
            IPAddress = strIPAddress;
            PortNo = iPort;
            InstantiateReader();
        }

        public static void InstantiateReader()
        {
            try
            {
                UserName = ConfigurationSettings.AppSettings["UserName"];
                Password = ConfigurationSettings.AppSettings["Password"];

                if (WebURL == null)
                {
                    try
                    {
                        if (ConfigurationSettings.AppSettings["WebURL"] != null)
                        {
                            WebURL = ConfigurationSettings.AppSettings["WebURL"].ToString();

                        }
                    }
                    catch
                    {

                    }
                }

                if (objReader == null)
                {
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                    if (WebURL.Length > 0)
                    {
                        objReader.Url = WebURL;

                        if (UserName != null && UserName.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(UserName, Password);
                                objReader.Credentials = credentials;
                            }
                            catch (Exception)
                            {
                            }
                        }

                    }
                }
            }
            catch
            {
            }
        }

        public Reader()
        {
        }

        public static List<IReader> GetReadersAtLocation(Location loc)
        {
            List<IReader> lst_Readers = null;
            IReader obj;
            try
            {
                List<IReaders> lstReaders;

                lstReaders = DBModule.Readers.GetReadersAtLocation(loc);

                if (lstReaders != null && lstReaders.Count > 0)
                {
                    lst_Readers = new List<IReader>();
                    foreach (IReaders objreader in lstReaders)
                    {
                        obj = RFIDHardWare.Common.RFIDUtility.DBConnectorToRFIDReader(objreader);
                        lst_Readers.Add(obj);
                    }

                }

                //if (RFIDHardWare.Common.RFIDUtility.lstAllRFIDReaders.ContainsKey(loc))
                //{
                //    lst_Readers = RFIDHardWare.Common.RFIDUtility.lstAllRFIDReaders[loc];
                //}
                //else
                //{
                //    lstReaders = DBModule.Readers.GetReadersAtLocation(loc);

                //    if (lstReaders != null && lstReaders.Count > 0)
                //    {
                //        lst_Readers = new List<IReader>();
                //        foreach (IReaders objreader in lstReaders)
                //        {
                //            obj = RFIDHardWare.Common.RFIDUtility.DBConnectorToRFIDReader(objreader);
                //            lst_Readers.Add(obj);
                //        }

                //        RFIDHardWare.Common.RFIDUtility.lstAllRFIDReaders.Add(loc, lst_Readers);

                //    }
                //}
            }
            catch (Exception)
            {
                throw;
            }
            return lst_Readers;

        }

        public bool SearchTag(string TagRFID)
        {
            bool flagResult = false;

            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                flagResult = objReader.SearchTag(IPAddress, PortNo, TagRFID);

            }
            catch
            {
            }
            
            return flagResult;


        }

        public List<RFIDTag> ScanForTags(int count)
        {
            List<RFIDTag> lst = new List<RFIDTag>();

            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();                

                //Ramp.RFIDHardWare.IQTMSReaderService.RFIDTag[] returnLst = null;
                RFIDTag[] returnLst = null;

                returnLst = objReader.ScanForTags(IPAddress, PortNo,count);
                if (returnLst != null && returnLst.Length > 0)
                {
                    lst = returnLst.ToList<RFIDTag>();
                }
            }
            catch
            {
            }

            return lst;


        }

        public bool IsReaderConnected()
        {
            bool flagResult = false;

            try
            {
                if(objReader == null)
                  objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();             
 
                return objReader.IsReaderConnected(IPAddress,PortNo);

            }
            catch (Exception ex)
            {
                flagResult = false;
            }
            return flagResult;
        }

        public List<RFIDTag> GetTags()
        {
            List<RFIDTag> lst = new List<RFIDTag>();

            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();    

                //Ramp.RFIDHardWare.IQTMSReaderService.RFIDTag[] returnLst = null;

                RFIDTag[] returnLst = null;

                returnLst = objReader.GetTags(IPAddress, PortNo);
                if (returnLst != null && returnLst.Length > 0)
                {
                    //lst = ConvertRFIDTag(returnLst);
                    lst = returnLst.ToList<RFIDTag>();
                }
            }
            catch (Exception ex)
            {
                //logger.ErrorException("Exception thrown in Get Tags method.", ex);
            }

            return lst;
        }

        public List<RFIDTag> ReadTags()
        {
            List<RFIDTag> lst = new List<RFIDTag>();

            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                //Ramp.RFIDHardWare.IQTMSReaderService.RFIDTag[] returnLst = null;

                RFIDTag[] returnLst = null;

                returnLst = objReader.ReadTags(IPAddress, PortNo);
                if (returnLst != null && returnLst.Length > 0)
                {
                    //lst = ConvertRFIDTag(returnLst);
                    lst = returnLst.ToList<RFIDTag>();
                }
            }
            catch
            {
            }

            return lst;
        }

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts)
        {
            try
            {
                bool flagResult = false;
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                //Ramp.RFIDHardWare.IQTMSReaderService.TagLedColor blinkColor = (Ramp.RFIDHardWare.IQTMSReaderService.TagLedColor)ledColor;

                //Ramp.RFIDHardWare.IQTMSReaderService.TimeSpan tss = new Ramp.RFIDHardWare.IQTMSReaderService.TimeSpan();


                flagResult = objReader.BlinkLEDWithColor(IPAddress, PortNo, tagToBlink, ledColor, Convert.ToInt32(ts.TotalMilliseconds), 50);

                //  objReader.Dispose();

                return flagResult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearTagList()
        {
            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                objReader.ClearTagList(IPAddress, PortNo);
                // objReader.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSerialNo()
        {
            string serialNo = "";
            try
            {

                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                serialNo = objReader.GetSerialNo(IPAddress,PortNo);
                // objReader.Dispose();

            }
            catch (Exception ex)
            {

            }
            return serialNo;
        }

        public string GetReaderStatus()
        {
            string readerStatus = "Fault";
            try
            {

                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                readerStatus = objReader.GetReaderStatus(IPAddress,PortNo);
                // objReader.Dispose();

            }
            catch (Exception ex)
            {

            }
            return readerStatus;
        }

        public static void ConfigureReaders(System.Data.DataTable dtReaders)
        {
            try
            {
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();


                objReader.ConfigureReaders(dtReaders);
                
            }
            catch
            {
            }
        }

        ~Reader()
        {
            try
            {
               // objReader = null;
            }
            catch (Exception)
            {
            }
        }

        public void Dispose()
        {
            try
            {
                // objReader.Dispose();
               // objReader = null;
            }
            catch (Exception ex)
            {
               // objReader = null;
            }
        }

        //private List<RFIDTag> ConvertRFIDTag(Ramp.RFIDHardWare.IQTMSReaderService.RFIDTag[] lstReturnTags)
        //{
        //    List<RFIDTag> lstTags = new List<RFIDTag>();

        //    RFIDTag value;

        //    foreach (Ramp.RFIDHardWare.IQTMSReaderService.RFIDTag objTag in lstReturnTags)
        //    {
        //        value.TagID = objTag.TagID;
        //        value.BatteryStatus = (TagBatteryStatus)Convert.ToInt32(objTag.BatteryStatus);
        //        value.MarkerName = objTag.MarkerName;
        //        value.NewMarkerID = objTag.NewMarkerID;
        //        value.OldMarkerID = objTag.OldMarkerID;
        //        value.SerialLabel = objTag.SerialLabel;
        //        value.SerialNo = objTag.SerialNo;
        //        value.TimeFirstSeen = objTag.TimeFirstSeen;
        //        value.TimeLastSeen = objTag.TimeLastSeen;
        //        value.MarkerNewPositionTime = objTag.MarkerNewPositionTime;
        //        value.TimeLastSeen = objTag.TimeLastSeen;
        //        lstTags.Add(value);
        //    }

        //    return lstTags;
        //}

        public bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int totalBlinks)
        {
            try
            {
                bool flagResult = false;
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                //Ramp.RFIDHardWare.IQTMSReaderService.TagLedColor blinkColor = (Ramp.RFIDHardWare.IQTMSReaderService.TagLedColor)ledColor;

                //Ramp.RFIDHardWare.IQTMSReaderService.TimeSpan tss = new Ramp.RFIDHardWare.IQTMSReaderService.TimeSpan();


                flagResult = objReader.BlinkLEDWithColor(IPAddress, PortNo, tagToBlink, ledColor, Convert.ToInt32(ts.TotalMilliseconds), totalBlinks);

                //  objReader.Dispose();

                return flagResult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BlinkLEDAsync(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int totalBlinks)
        {
            try
            {
               
                if (objReader == null)
                    objReader = new Ramp.RFIDHardWare.IQTMSReaderService.ReaderService();

                objReader.BlinkLEDWithColorAsync(IPAddress, PortNo, tagToBlink, ledColor, Convert.ToInt32(ts.TotalMilliseconds), totalBlinks);
                              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetMarkersStatus()
        {
            DataTable dt = new DataTable();
            try
            {
                if (objReader == null)
                    InstantiateReader();

                dt = objReader.GetMarkersStatus();
               
            }
            catch
            {
            }

            return dt;

        }

        public static DataTable SetMarkersLoopID(DataTable dtMarkers)
        {
            DataTable dt = new DataTable();
            try
            {
                if (objReader == null)
                    InstantiateReader();

                dt = objReader.SetMarkersLoopID(dtMarkers);

            }
            catch
            {
            }

            return dt;

        }

    }
}
