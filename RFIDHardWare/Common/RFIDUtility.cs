using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Common; 
using Ramp.RFIDHardWare.Connector;
//using Ramp.IdentacReaderLib.Readers; 
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using DBModule = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.RFIDHardWare.Common
{
    public class RFIDUtility
    {
        public static Dictionary<Location, List<IReader>> lstAllRFIDReaders = new Dictionary<Location, List<IReader>>();

        public static bool IsTruckAtLocation(Location loc, string TagRFID)
        {
            bool flagResult = false;
            try
            {
                // Get all the readers for the Location loc
                List<IReader> lstReaders = Reader.GetReadersAtLocation(loc);

              //  string[] lstTagRFID = null;
                // Perform a tag read operation, BY READERS, to search TagRFID AT loc

                foreach (Reader objReader in lstReaders)
                {
                   if (objReader.SearchTag(TagRFID))
                    {
                        flagResult = true;
                        objReader.Dispose();
                        break;
                    }

                   objReader.Dispose();

                }  
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static string[] GetTagRFIDListAtReader(Reader objReader)
        {
            string[] lstTagRFID = null;
            try
            {
                lstTagRFID = new string[5];
                lstTagRFID[0] = "123456789";
                lstTagRFID[1] = "555436789";
                lstTagRFID[2] = "646446464";
                lstTagRFID[3] = "124756878";
                lstTagRFID[4] = "357434324";
            }
            catch (Exception ex)
            {
            }
            return lstTagRFID;
        }

        public static bool SearchTag(string TagRFID)
        {
            bool flagResult = false;

            return flagResult;
        }

        public static Int32 GetTotalTagsAtLocation(Location loc)
        {
            // Get all the readers for the Location loc
            List<IReader> lstReaders = Reader.GetReadersAtLocation(loc);
            int totalTags = 0;

            string[] lstTagRFID = null;
            // Perform a tag read operation, BY READERS, to search TagRFID AT loc

            foreach (Reader objReader in lstReaders)
            {
                lstTagRFID = GetTagRFIDListAtReader(objReader);
                totalTags = totalTags + lstTagRFID.Length; 
            }

            return totalTags;
        }

        public static void ReadNStoreAllReaders()
        {
            List<IReaders> lstDBReaders = DBModule.Readers.GetAllReaders();
            IReader obj = null;

            List<IReader> lstRFIDReaders;

            Location key;

           foreach (IReaders dbReader in lstDBReaders)
           {
               obj = DBConnectorToRFIDReader(dbReader);
               key = (Location)obj.Location;
               if (lstAllRFIDReaders.ContainsKey(key))
               {
                   lstRFIDReaders =  lstAllRFIDReaders[key];
               }
               else
               {
                   lstRFIDReaders = new List<IReader>();
                   lstAllRFIDReaders.Add(key, lstRFIDReaders);
               }

               lstRFIDReaders.Add(obj);
               lstAllRFIDReaders[key] = lstRFIDReaders;
           }
        }

        public static IReader DBConnectorToRFIDReader(IReaders objreader)
        {
            IReader obj;
            try
            {
                obj = new Reader(objreader.IPAddress, objreader.PortNo);
                obj.ReaderName = objreader.ReaderName;
                obj.ReaderSerialNo = objreader.ReaderSerialNo;
                obj.IPAddress = objreader.IPAddress;
                obj.PortNo = objreader.PortNo;
                obj.Location = objreader.Location;
            }
            catch (Exception)
            {
                throw;
            }
            return obj;
        }

    }
}
