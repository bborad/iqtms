using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.BusinessLogicConnector
{
    public interface IHopper
    {
        int Ent2_Hopper { get; set; }
        int HopperNo { get; set; }
        void InitializeTimer();
        System.Collections.ArrayList lstQueue { get; set; }
        void SetTimer(int interval);
        void StartTimer();
        void StopTimer();
        System.Threading.Thread thQueue { get; }
        int tmrInterval { get; set; }
        System.Timers.Timer tmrQueue { get; }
    }
}
