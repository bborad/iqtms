using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.MiddlewareController.Connector.BusinessLogicConnector
{
    public interface ITruck
    {
        int End_Transaction { get; }
        void GateQueue();
        void InitializeTimer();
        Location LastPositiion { get; set; }
        int FoundAtHopperID { get; set; }
        DateTime LastReadTime { get; set; }
        Location EntryOrQueue { get; set; }
        ITags TagDB { get; set; }
        RFIDTag TagRFID { get; set; }

        string TransMessage { get; set; }
        int TransStatus { get; set; }
        bool CalledUp { get; set; }

        int iTruckCallupCount { get; set; } 

        void SkipNonRespondingTruck();
        string statusMessage { get; set; }
        System.Timers.Timer tmrTransaction { get; }
        System.Timers.Timer tmrCheckResponseTime { get; set; }
        int UpdateTransaction();
        void SetTimer(int interval);
        void StartTimer(double interval);
        void SetResTimer(int interval);
        void StopResTimer();
        void StartResTimer(double interval);



    }
}
