using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IMarkers
    {
        int ID { get; set; }
        string MarkerLoopID { get; set; }
        string IPAddress { get; set; }
        //Int32 MarkerPosition { get; set; }
        string MarkerPosition { get; set; }
        Int32 Location { get; set; }

        int InsertMarker(string MarkerLoopID, string MarkerPosition,int Location,string SerialNo);
        int DeleteMarkerByID(string MarkerIDs);
    }
}
