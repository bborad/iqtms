using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IHoppers
    {
        int ID { get; set; }
        string HopperName { get; set; }
        int MarkerID { get; set; }
        Int32 TimeFromEnt2 { get; set; }
        Int32 TimeToTW { get; set; }

        int AssignHoppersProponents(int ProponentID, int HopperID);
        void UpdateExpectedTimeEntry2(string ID, string TimeFromEnt2, string TimeToTW, int MarkerID);
    }
}
