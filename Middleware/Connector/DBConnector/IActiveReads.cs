using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Common;


namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IActiveReads
    {
        int ID { get; set; }
        int TagID { get; set; }
        int HopperID { get; set; }
        int LastPosition { get; set; }
        int EntryORQueue { get; set; }
        DateTime REDS { get; set; }
        DateTime Queue { get; set; }
        DateTime Entry1 { get; set; }
        DateTime Entry2 { get; set; }
        DateTime HopperTime { get; set; }
        DateTime TruckWash { get; set; }
        DateTime REDS2 { get; set; }
        DateTime TruckCallUp { get; set; }


        void SendTruckToQueueBack(string TagRFID);
        Int32 GetTotalTagsAtLocation(Location loc);
        Int32 GetQueueLengthForHopper(Int32 HopperID);

    }
}
