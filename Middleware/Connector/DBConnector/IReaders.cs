using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IReaders
    {

        int ID { get; set; }
        string IPAddress { get; set; }
        int Location { get; set; }
        int PortNo { get; set; }
        string ReaderName { get; set; }
        string ReaderSerialNo { get; set; }
        int Power { get; set; }
        string ReaderStatus { get; set; }

        int InsertReader(string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power);
        int UpdateReader(int ReaderID, string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power);
        void DeleteReaderByID(int ID);
    }
}
