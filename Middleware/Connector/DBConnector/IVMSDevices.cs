using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IVMSDevices
    {
        short DeviceNo { get; set; }
        string IPAddress { get; set; }
        short LedHeight { get; set; }
        short LedWidth { get; set; }
        string Password { get; set; }
        bool SCL2008 { get; set; }
        short UDPPort { get; set; }
        int Location { get; set; }

        Int64 ID { get; set; }
        string VMSStatus { get; set; }

          int TextStartPosition { get; set; }
          int TextLength { get; set; }
          int TextColor { get; set; }
          int ASCFont { get; set; }
            int MessageTimeOut { get; set; }
          string DefaultMessage { get; set; }

    }
}
