using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface IUsers
    {
        int ID { get; set; }
        string UserID { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int SecurityGroupID { get; set; }
        string EmailID { get; set; }

         int AddUser();
    }
}
