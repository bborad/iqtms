using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
    public interface ITags
    {
        bool Assigned { get; set; }
        int ProponentID { get; set; }
        string TagRFID { get; set; }
       // int TruckID { get; set; }       
        string TruckID { get; set; }       
        int HopperID { get; set; }
        int ID { get; set; }
        int TagType { get; set; }
        int DestinationPad { get; set; }
        IHoppers AssignedHopper { get; set; }

        DataTable CheckTagAlreadyAssigned(string TagRFID, int ProponentID);
        void InsertTagDetails(string TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID);       
        void UpdateTagDetails(int ID, int TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID);
        int InsertTags(string TagRFID);
        int DispatchTagsByTagsID(string IssuedTagsID, string TruckRegNo,string TagBattery);
        int AssignTags(string TagRFID, string TruckID, int HopperID, int ProponentID);
        int UnAssignTags(string TagRFID);
        int RemoveTags(string TagRFID);
        DateTime LastReadTime { get; set; }

    }
}
