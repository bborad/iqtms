using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.DBConnector
{
   public interface ISecurityGroups
    {
          int ID { get; set; }
          string Description { get; set; }
          int Level { get; set; }

    }
}
