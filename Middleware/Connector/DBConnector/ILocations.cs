using System;
namespace Ramp.MiddlewareController.Connector.DBConnector
{
   public interface ILocations
    {
        string Description { get; set; }
        int ID { get; set; }

        bool IsDeleted { get; set; }
        string LocationName { get; set; }
        int InsertLocation(string LocationName, string Description);
    }
}
