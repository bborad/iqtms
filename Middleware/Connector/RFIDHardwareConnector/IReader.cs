using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Common;
using System.Data;
 

namespace Ramp.MiddlewareController.Connector.RFIDHardwareConnector
{
    public interface IReader
    {
        string IPAddress { get; set; }
        int Location { get; set; }
        int PortNo { get; set; }
        int TxPower { get; set; }
       // bool IsReady { get; set; }
        string ReaderName { get; set; }
        string ReaderSerialNo { get; set; }
        List<RFIDTag> ScanForTags(int count);
        bool SearchTag(string TagRFID);
        List<RFIDTag> GetTags();
        List<RFIDTag> ReadTags();
        bool IsReaderConnected();
        bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts);
        bool BlinkLED(string tagToBlink, TagLedColor ledColor, TimeSpan ts,int totalBlinks);
        void BlinkLEDAsync(string tagToBlink, TagLedColor ledColor, TimeSpan ts, int totalBlinks);
        void ClearTagList();
        void Dispose();
        string GetSerialNo();
        string GetReaderStatus();
        
    }
}
