using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.MiddlewareController.Connector.VMSHardWareConnector
{
    public interface IVMSDevice
    {
        string Password { get; set; }
        bool SCL2008 { get; set; }
        short UDPPort { get; set; }
        short DeviceNo { get; set; }
        string IPAddress { get; set; }
        int Location { get; set; }

        short LedNum { get; set; } // value 0
        short ComPort { get; set; } // value 1     

        short LedWidth { get; set; } // value 128
        short LedHeight { get; set; } // value 32

        short TextColor { get; set; } // value 255
        short AscFont { get; set; }  
        short TextStartPosition { get; set; }  
        short TextLength { get; set; }
         int MessageTimeOut { get; set; }

        string DefaultMessage { get; set; }

        //List<string> currentMessage { get; set; }

          bool UseRandomPortNo { get; set; }

          short MinUDPPortNo { get; set; }

          short MaxUDPPortNo { get; set; }

        bool Open();
        bool Disconnect();
 
        //bool ShowString(short Left, short Top, short XPos, short YPos, short Color, short ASCFont, string Str_);
        //bool ShowString(short XPos, short YPos, short Color, short ASCFont, string Str_);
        bool ShowString(string Str_, string formatInfo, int VMSLogging);
        void DisplayDefaultMessage();
        bool ShowCurrentString(string Str_, string formatInfo);
       // bool ShowString(string Str_, string formatInfo);
        void RemoveMessage(string Str_, string formatInfo);
       bool VMSInit();


        
    }
}
