using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net.Mail;

namespace Ramp.MiddlewareController.Common
{

    public enum TagType
    {
        AutoTarp = 1,
        ManualTarp
    }

    public enum Location
    {
        //REDS = 0,
        //QUEUE,
        //ENTRY1,
        //ENTRY2,
        //HOPPER,
        //TRUCKWASH,
        //EXITGATE,
        //REDS2


        REDS = 1,
        QUEUE,
        ENTRY1,
        ENTRY2,
        HOPPER,
        TRUCKWASH,
        EXITGATE,
        REDS2,
        EntryLane1,
        EntryLane2,
        ControlRoom,
        IssueTagOffice,
        InboundUntarping,
        InboundQueue,
        InboundEntry,
        InboundExit,
        UnKnown = -9999

    }

    public enum MarkersPosition
    {
        TruckPark = 1,
        ENTRY1Lane1,
        ENTRY1Lane2,
        ENTRY2,
        HOPPER,
        TRUCKWASH,
        EXITGATE
    }

    public struct RFIDTag
    {
        public string TagID;
        public string SerialLabel;
        public int SerialNo;
        public int NewMarkerID;
        public int OldMarkerID;

        public DateTime MarkerNewPositionTime;
        public DateTime MarkerOldPositionTime;

        public DateTime TimeFirstSeen;
        public DateTime TimeLastSeen;

        public TagBatteryStatus BatteryStatus;
        public string MarkerName;
    }

    public enum TagBatteryStatus
    {
        Good,
        Intermediate,
        Poor
    }

    public enum RunningMode
    {
        ReducedFunctionality,
        FullFunctionality
    }

    public enum TagLedColor
    {
        All_LED,
        GREEN,
        ORANGE,
        RED,
        YELLOW

    }

    public enum TransactionStatus
    {
        Complete = 1,
        Incomplete,
        Alert
    }

    public enum TagStatus
    {
        Idel = 1,
        Issued,
        Operational,
        Returned
    }

    public static class TransactionMessages
    {
        public const string _TransactionComplete = "Transaction Completed.";
        public const string _TransactionInComplete = "InComplete Transaction.";
        public const string _TruckLeavingWithTags = "Truck Leaving With Tags.";
        public const string _TagsAtControlRoom = "Invalid transactions closed at Control room.";
        public const string _TruckNotRespondOnCallup = "Truck didnot respond for specified times on called up from queue area.";
    }

    //  MAX QUEUE 
    public class ExpectedValues
    {
        public static Int32 ID;
        public static Int32 REDS_Queue;
        public static Int32 Queue_Ent1;
        public static Int32 Ent1_Ent2;
        public static Int32 QueueTimePerTruck;
        public static Int32 Unloading;
        public static Int32 StartUp;
        public static Int32 ShutDown;
        public static Int32 TruckCallResponse;
        public static Int32 TruckQueueMax;
        public static Int32 End_Transaction;
        public static Int32 End_Transaction2;

        public static Int32 Max_Delay;

        public static Int32 Exit_Ent1;
        public static Int32 TruckWash_Ent1;

        public static Int32 Exit_REDS;
        public static Int32 TruckWash_REDS;
        public static Int32 Precision;
        public static Int32 TagReadFrequency;

        public static Int32 ServerResponseTime = 60;

        public static Int32 UpdateServerStatusTime = 15;

        public static Int32 ServerPollingTime = 20;
        public static Int32 UnTarpTime;

    }

    public class GlobalValues
    {
        public static Int32 CurrentQueueSize;
    }


    public enum UserSecurityLevel
    {
        Administrator = 1,
        Employee,
        EntryGate
    }

    public static class MiddlewareUtility
    {
        public static void SendEmail(string FromAddress, string ToAddress, string CC, string BCC, string Attachment, string Subject, string MessageBody)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromAddress);
                mailMessage.To.Add(ToAddress);

                if (CC.Trim() != string.Empty)
                {
                    //mailMessage.CC.Add(new MailAddress(CC));
                    string[] strCC = CC.Split(',');
                    foreach (string sCC in strCC)
                    {
                        if (sCC.Trim() != string.Empty)
                            mailMessage.CC.Add(new MailAddress(sCC));
                    }
                }
                if (BCC.Trim() != string.Empty)
                    mailMessage.Bcc.Add(new MailAddress(BCC));
                mailMessage.Subject = Subject;//"Online Account at firstrehab.com";
                mailMessage.Body = MessageBody;
                if (Attachment.Trim() != string.Empty)
                {
                    mailMessage.Attachments.Add(new Attachment(Attachment));
                }
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = MailPriority.Normal;
                SmtpClient smtpClient = new SmtpClient();
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                smtpClient.Send(mailMessage);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

    }

    public enum ConfigurationValues
    {
        SolidLedInterval = 1,
        SolidLedTotalBlink,

        FlashingLedInterval,
        FlashingLedTotalBlink,

        CurrentPageRefresh,
        EntryPageRefresh,

        CallupRetries = 13

    }

    public struct VMSParameters
    {
        public int VMSLogging;
        public bool UseRandomPort;
        public short MinPortNo;
        public short MaxPortNo; 
      
    }

}
