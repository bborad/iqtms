using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.Common;
using Ramp.DBConnector.Adapter;
using System.Text;
using DBAdapter = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Common;  

namespace Ramp.UI
{
    public partial class offlinetransactions : System.Web.UI.Page
    {
        DataSet dset = new DataSet();


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    lblMessage.Text = String.Empty;
            //}

            lblMessage.Text = String.Empty;
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileUpOffTrans.PostedFile != null)
                {
                    string filename = System.IO.Path.GetFileName(fileUpOffTrans.PostedFile.FileName);

                    string filelocation = Server.MapPath("UploadFiles") + "\\" + filename;
                    //string filelocation = Server.MapPath("../UploadFiles") + "\\OfflineTransactions.txt";


                    FileInfo file = new FileInfo(filelocation);
                    if (file.Exists)
                    {
                        file.Delete(); // Delete if already exists
                    }

                    // SAVE file
                    fileUpOffTrans.PostedFile.SaveAs(filelocation);

                    CreateTable(filelocation, filename);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateTable(string filePath, string fileName)
        {
            try
            {
                // Create a connection string DBQ attribute sets the path of directory which contains CSV or text files 
                string strConnString = @"Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Server.MapPath("UploadFiles") + ";";
                string sql_select;
                System.Data.Odbc.OdbcConnection conn;

                //Create connection to CSV/text file
                conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());

                //Open the connection 
                conn.Open();

                //Fetch records from CSV/text
                sql_select = "select * from [" + fileName + "]";

                //Initialise DataAdapter with file data
                DataAdapter obj_oledb_da = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);

                //Fill dataset with the records from CSV file
                obj_oledb_da.Fill(dset);

                //Declare variable for Error
                string strError = "";

                //Close Connection to CSV file
                conn.Close();


                if (dset != null)
                {
                    if (IsValidCSVstructure(dset))
                    {
                        dset.Tables[0].Columns.Add("Status", Type.GetType("System.String"));
                        dset.Tables[0].AcceptChanges();


                        //.................................................................
                        StringBuilder strTruckIDs = new StringBuilder("'-1'");

                        foreach (DataRow objrow in dset.Tables[0].Rows)
                        {
                            strTruckIDs.Append("," + "'" + objrow["Truck ID"].ToString() + "'");
                        }

                        DataTable dtData = DBAdapter.Tags.GetDetailsByTruckIDs(strTruckIDs.ToString());

                        //.................................................................






                        // new feild (status)
                        SetValidationStatus(dset, dtData);

                        if (dset.Tables[0].Rows.Count == 0)
                        {
                            gvPreview.DataSource = null;
                            // Error message
                            lblMessage.Text = "No record(s) found";

                        }
                        else
                        {
                            //gvPreview.DataSource = dset.Tables[0];
                            //LoadData(dset.Tables[0]);

                            DataTable dtTransactions = (DataTable)Session["dtTransactions"];
                            gvPreview.DataSource = dtTransactions;
                        }

                        gvPreview.DataBind();
                    }
                    else
                    {
                        // Error message
                        lblMessage.Text = "Invalid CSV format, please check the file";
                    }
                }
            }
            catch (Exception ex) //Error
            {

            }
        }

        private Boolean IsValidCSVstructure(DataSet dset)
        {
            Boolean flagInvalidformat = false;

            if (dset.Tables[0].Columns.Count != 10)
            {
                flagInvalidformat = true;
            }
            else
            {
                if (dset.Tables[0].Columns[0].ColumnName.ToLower() != "Date".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[1].ColumnName.ToLower() != "Truck ID".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[2].ColumnName.ToLower() != "REDS Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[3].ColumnName.ToLower() != "Queue Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[4].ColumnName.ToLower() != "Entry1 Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[5].ColumnName.ToLower() != "Entry2 Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[6].ColumnName.ToLower() != "PAD ID".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[7].ColumnName.ToLower() != "PAD Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[8].ColumnName.ToLower() != "TW Time".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[9].ColumnName.ToLower() != "Exit Time".ToLower())
                {
                    flagInvalidformat = true;
                }
            }

            return !flagInvalidformat;
        }

        protected void gvPreview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblTransNo = (Label)e.Row.FindControl("lblTransNo");
                if (lblTransNo != null)
                {
                    lblTransNo.Text = (e.Row.RowIndex + 1).ToString();
                }
            }
        }


        private void SetValidationStatus(DataSet dset, DataTable dTable)
        {
            try
            {
                DataTable dtAdd = new DataTable();

                DataColumn dc = new DataColumn("TransDate", typeof(DateTime));
                DataColumn dc1 = new DataColumn("TruckID", typeof(string));
                DataColumn dc2 = new DataColumn("REDS", typeof(DateTime));
                DataColumn dc3 = new DataColumn("Queue", typeof(DateTime));
                DataColumn dc4 = new DataColumn("Entry1", typeof(DateTime));
                DataColumn dc5 = new DataColumn("Entry2", typeof(DateTime));
                DataColumn dc6 = new DataColumn("HopperName", typeof(string));
                DataColumn dc7 = new DataColumn("HopperTime", typeof(DateTime));
                DataColumn dc8 = new DataColumn("TruckWash", typeof(DateTime));
                DataColumn dc9 = new DataColumn("ExitTime", typeof(DateTime));

                DataColumn dc10 = new DataColumn("TransStatus", typeof(Int32));
                DataColumn dc11 = new DataColumn("TransMessage", typeof(string));


                dtAdd.Columns.Add(dc);
                dtAdd.Columns.Add(dc1);
                dtAdd.Columns.Add(dc2);
                dtAdd.Columns.Add(dc3);
                dtAdd.Columns.Add(dc4);
                dtAdd.Columns.Add(dc5);
                dtAdd.Columns.Add(dc6);
                dtAdd.Columns.Add(dc7);
                dtAdd.Columns.Add(dc8);
                dtAdd.Columns.Add(dc9);

                dtAdd.Columns.Add(dc10);
                dtAdd.Columns.Add(dc11);


                DataTable dtTransactions = new DataTable();
                dtTransactions.Columns.Add("Truck ID", typeof(string));
                dtTransactions.Columns.Add("REDS Time", typeof(string));
                dtTransactions.Columns.Add("Queue Time", typeof(string));
                dtTransactions.Columns.Add("EntryGate", typeof(string));
                dtTransactions.Columns.Add("PAD Time", typeof(string));
                dtTransactions.Columns.Add("TW Time", typeof(string));
                dtTransactions.Columns.Add("Exit Time", typeof(string));

                dtTransactions.Columns.Add("Status", typeof(string));

                DataRow transRow;

                //string date = DateTime.Now.Date.ToString("dd/MM/yyyy");
                string date = DateTime.Now.Date.ToString("MM/dd/yyyy");

                DataRow dr;
                DataRow[] drr = null;

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    Boolean flagValid = true;

                    transRow = dtTransactions.NewRow();

                    drr = null;

                    if (!String.IsNullOrEmpty(row["Date"].ToString()))
                    {
                        date = row["Date"].ToString();

                        DateTime dtime = Convert.ToDateTime(date);
                        //date = dtime.ToString("dd/MM/yyyy");
                        date = dtime.ToString("MM/dd/yyyy");
                    }

                    // check TruckID & HopperID
                    if (String.IsNullOrEmpty(row["Truck ID"].ToString()))
                    {
                        flagValid = false;
                        row["Status"] = "Error";
                        transRow["Truck ID"] = "Required";
                    }
                    else
                    {
                        drr = dTable.Select("TruckID = '" + row["Truck ID"].ToString() + "'");
                        if (drr.Length > 0)
                        {
                            transRow["Truck ID"] = row["Truck ID"].ToString() + "-" + drr[0]["ProponentName"].ToString() + "-" + drr[0]["HopperName"].ToString();
                        }
                        else
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            transRow["Truck ID"] = "Invalid";
                        }

                    }

                    if (String.IsNullOrEmpty(row["PAD ID"].ToString()) && flagValid)
                    {
                        flagValid = false;
                        row["Status"] = "Error";
                    }


                    // check all DATE feilds

                    // REDS
                    if (!String.IsNullOrEmpty(row["REDS Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["REDS Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            transRow["REDS Time"] = "Invalid";
                        }
                        else
                        {
                            transRow["REDS Time"] = "Passed";
                        }
                    }
                    else
                    {
                        transRow["REDS Time"] = "NA";
                    }


                    // Queue
                    if (!String.IsNullOrEmpty(row["Queue Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["Queue Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            transRow["Queue Time"] = "Invalid";
                        }
                        else
                        {
                            transRow["Queue Time"] = "Arrived";
                        }
                    }
                    else
                    {
                        transRow["Queue Time"] = "NA";
                    }



                    // Entry Gate
                    string entryGate = String.Empty;

                    if (!String.IsNullOrEmpty(row["Entry2 Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["Entry2 Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            entryGate = "Invalid";
                        }
                        else
                        {
                            entryGate = "Departed";
                        }
                    }


                    if (!String.IsNullOrEmpty(row["Entry1 Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["Entry1 Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            entryGate = "Invalid";
                        }
                        else if (entryGate.Length <= 0)
                        {
                            entryGate = "Arrived";
                        }
                    }
                    else if (entryGate.Length <= 0)
                    {
                        entryGate = "NA";
                    }

                    transRow["EntryGate"] = entryGate;


                    // PAD
                    if (!String.IsNullOrEmpty(row["PAD Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["PAD Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            transRow["PAD Time"] = "Invalid";
                        }
                        else
                        {

                            if (drr != null && drr.Length > 0)
                            {
                                object HopperID = drr[0]["HopperID"];
                                int padID = HopperID != DBNull.Value ? Convert.ToInt32(HopperID) : -1;

                                HopperID = row["PAD ID"];
                                int padID1 = HopperID != DBNull.Value ? Convert.ToInt32(HopperID) : 0;

                                if (padID == padID1)
                                {
                                    transRow["PAD Time"] = "Arrived";
                                }
                                else
                                {
                                    transRow["PAD Time"] = "WrongPAD";
                                }
                            }
                            else
                            {
                                flagValid = false;
                                row["Status"] = "Error";
                                transRow["PAD Time"] = "Invalid";
                            }

                            // Check  using  row["HopperTime"].ToString() if the PAD is CORRECT ?
                            //YES
                            //   transRow["PAD Time"] = "Arrived";

                            //NO
                            //   transRow["PAD Time"] = "WrongPAD";
                        }
                    }
                    else
                    {
                        transRow["PAD Time"] = "NA";
                    }


                    string TwStatus = String.Empty;
                    string ExitStatus = String.Empty;

                    // Truck Wash
                    if (!String.IsNullOrEmpty(row["TW Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["TW Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            TwStatus = "Invalid";
                        }
                        else
                        {
                            TwStatus = "Arrived";
                        }
                    }
                    else
                    {
                        TwStatus = "NA";

                    }


                    // Exit
                    if (!String.IsNullOrEmpty(row["Exit Time"].ToString()))
                    {
                        if (!IsValidDate(date, row["Exit Time"].ToString()))
                        {
                            flagValid = false;
                            row["Status"] = "Error";
                            ExitStatus = "Invalid";
                        }
                        else
                        {
                            ExitStatus = "Arrived";
                        }
                    }
                    else
                    {
                        ExitStatus = "NotArrived";
                    }

                    //transRow["TW Time"] = TwStatus;
                    //transRow["Exit Time"] = ExitStatus;


                    //For Truck Wash & Exit ...............................
                    // Arrived (TW)
                    if (TwStatus == "Arrived" && ExitStatus == "Arrived")   // 1 1
                    {
                        transRow["TW Time"] = "Arrived";
                        transRow["Exit Time"] = "Arrived";
                    }

                    else if (TwStatus == "Arrived" && ExitStatus == "Invalid")   // 1 0
                    {
                        transRow["TW Time"] = "Arrived";
                        transRow["Exit Time"] = "Invalid";
                    }

                    else if (TwStatus == "Arrived" && ExitStatus == "NotArrived")   // 1 0
                    {
                        transRow["TW Time"] = "Arrived";
                        transRow["Exit Time"] = "NotArrived";
                    }



                    // Invalid (TW)
                    if (TwStatus == "Invalid" && ExitStatus == "Arrived")   // 0 1
                    {
                        transRow["TW Time"] = "Invalid";
                        transRow["Exit Time"] = "Arrived";
                    }

                    else if (TwStatus == "Invalid" && ExitStatus == "Invalid")   // 0 0
                    {
                        transRow["TW Time"] = "Invalid";
                        transRow["Exit Time"] = "Invalid";
                    }

                    else if (TwStatus == "Invalid" && ExitStatus == "NotArrived")   // 0 0
                    {
                        transRow["TW Time"] = "Invalid";
                        transRow["Exit Time"] = "NotArrived";
                    }



                    // NA (TW)
                    if (TwStatus == "NA" && ExitStatus == "Arrived")   // 0 1
                    {
                        transRow["TW Time"] = "Skipped";
                        transRow["Exit Time"] = "Arrived";
                    }

                    else if (TwStatus == "NA" && ExitStatus == "Invalid")   // 0 0
                    {
                        transRow["TW Time"] = "NA";
                        transRow["Exit Time"] = "Invalid";
                    }

                    else if (TwStatus == "NA" && ExitStatus == "NotArrived")   // 0 0
                    {
                        transRow["TW Time"] = "NA";
                        transRow["Exit Time"] = "NotArrived";
                    }



                    //transRow["Status"] = row["Status"];

                    //dtTransactions.Rows.Add(transRow);
                    //dtTransactions.AcceptChanges();
                    ///***************************************************************


                    DateTime dtValue;

                    if (flagValid)
                    {
                        dr = dtAdd.NewRow();
                        dr["TransDate"] = Convert.ToDateTime(date);
                        dr["TruckID"] = row["Truck ID"].ToString();
                        dr["HopperName"] = row["PAD ID"].ToString();

                        dtValue = getDate(date, row["REDS Time"].ToString());
                        if (dtValue == DateTime.MinValue)
                            dr["REDS"] = DBNull.Value;
                        else
                            dr["REDS"] = dtValue;

                        dtValue = getDate(date, row["Queue Time"].ToString());
                        if (dtValue == DateTime.MinValue)
                            dr["Queue"] = DBNull.Value;
                        else
                            dr["Queue"] = dtValue;

                        dtValue = getDate(date, row["Entry1 Time"].ToString());
                        if (dtValue == DateTime.MinValue)
                            dr["Entry1"] = DBNull.Value;
                        else
                            dr["Entry1"] = dtValue;

                        dtValue = getDate(date, row["Entry2 Time"].ToString());
                        if (dtValue == DateTime.MinValue)
                            dr["Entry2"] = DBNull.Value;
                        else
                            dr["Entry2"] = dtValue;

                        dtValue = getDate(date, row["PAD Time"].ToString());
                        if (dtValue == DateTime.MinValue)
                            dr["HopperTime"] = DBNull.Value;
                        else
                            dr["HopperTime"] = dtValue;

                        //dtValue = getDate(date, row["TW Time"].ToString());
                        //if (dtValue == DateTime.MinValue)
                        //    dr["TruckWash"] = DBNull.Value;
                        //else
                        //    dr["TruckWash"] = dtValue;

                        //dtValue = getDate(date, row["Exit Time"].ToString());
                        //if (dtValue == DateTime.MinValue)
                        //    dr["ExitTime"] = DBNull.Value;
                        //else
                        //    dr["ExitTime"] = dtValue;


                        if (String.IsNullOrEmpty(row["TW Time"].ToString()))
                        {
                            dr["TruckWash"] = DBNull.Value;
                        }
                        else
                        {
                            dtValue = getDate(date, row["TW Time"].ToString());
                            if (dtValue == DateTime.MinValue)
                                dr["TruckWash"] = DBNull.Value;
                            else
                                dr["TruckWash"] = dtValue;
                        }

                        if (String.IsNullOrEmpty(row["Exit Time"].ToString()))
                        {
                            dr["ExitTime"] = DBNull.Value;
                        }
                        else
                        {
                            dtValue = getDate(date, row["Exit Time"].ToString());
                            if (dtValue == DateTime.MinValue)
                                dr["ExitTime"] = DBNull.Value;
                            else
                                dr["ExitTime"] = dtValue;
                        }


                        dr["TransStatus"] = Convert.ToInt32(TransactionStatus.Complete);
                        dr["TransMessage"] = "Transaction compelete";

                        row["Status"] = "Ok";

                        dtAdd.Rows.Add(dr);

                    }


                    transRow["Status"] = row["Status"];

                    dtTransactions.Rows.Add(transRow);
                    dtTransactions.AcceptChanges();

                }

                // dtAdd.AcceptChanges();

                Session["dtAdd"] = dtAdd;

                Session["dtTransactions"] = dtTransactions;



            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool IsValidDate(string date, string value)
        {
            try
            {
                if (value.Trim().Length <= 11)
                {
                    value = date + " " + value;
                }
                Convert.ToDateTime(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        private DateTime getDate(string date, string value)
        {
            try
            {
                if (value.Trim().Length <= 11)
                {
                    value = date + " " + value;
                }
                DateTime dt = Convert.ToDateTime(value);
                return dt;
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                if (Session["dtAdd"] == null)
                {
                    return;
                }

                DataTable dtAdd = (DataTable)Session["dtAdd"];
                int result = ArchiveReads.UploadOfflineTransactions(dtAdd);

                Session["dtAdd"] = null;

                // Success message
                lblMessage.Text = "Transaction(s) uploded successfully!";

                gvPreview.DataSource = null;
                gvPreview.DataBind();

            }
            catch (Exception ex)
            {

            }

            //foreach (DataRow row in dset.Tables[0].Rows)
            //{
            //    if (Convert.ToString(row["Status"]).Equals("Ok"))
            //    {

            //        DateTime Date;
            //        string TruckID;
            //        DateTime REDS = DateTime.MinValue;
            //        DateTime Queue = DateTime.MinValue;
            //        DateTime Entry1 = DateTime.MinValue;
            //        DateTime Entry2 = DateTime.MinValue;
            //        string HopperName;
            //        DateTime HopperTime = DateTime.MinValue;
            //        DateTime TruckWash = DateTime.MinValue;
            //        DateTime ExitTime = DateTime.MinValue;

            //        string date = DateTime.Now.Date.ToString("MM/dd/yyyy");

            //        if (!String.IsNullOrEmpty(row["Date"].ToString()))
            //        {
            //            date = row["Date"].ToString();

            //            DateTime dtime = Convert.ToDateTime(date);
            //            //date = dtime.ToString("dd/MM/yyyy");
            //            date = dtime.ToString("MM/dd/yyyy");
            //        }

            //        Date = Convert.ToDateTime(date);


            //        if (String.IsNullOrEmpty(row["Truck ID"].ToString()))
            //        {
            //            TruckID = row["Truck ID"].ToString();
            //        }
            //        if (String.IsNullOrEmpty(row["PAD ID"].ToString()))
            //        {
            //            HopperName = row["PAD ID"].ToString();
            //        }



            //        if (!String.IsNullOrEmpty(row["REDS Time"].ToString()))
            //        {
            //            REDS = getDate(date, row["REDS Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["Queue Time"].ToString()))
            //        {
            //            Queue = getDate(date, row["Queue Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["Entry1 Time"].ToString()))
            //        {
            //            Entry1 = getDate(date, row["Entry1 Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["Entry2 Time"].ToString()))
            //        {
            //            Entry2 = getDate(date, row["Entry2 Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["PAD Time"].ToString()))
            //        {
            //            HopperTime = getDate(date, row["PAD Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["TW Time"].ToString()))
            //        {
            //            TruckWash = getDate(date, row["TW Time"].ToString());
            //        }
            //        if (!String.IsNullOrEmpty(row["Exit Time"].ToString()))
            //        {
            //            ExitTime = getDate(date, row["Exit Time"].ToString());
            //        }


            //        // Insert


            //   }
            //}
        }



        //public void LoadData(DataTable dt)
        //{

        //    //Truck ID
        //    //REDS Time
        //    //Queue Time
        //    //Entry1 Time
        //    //Entry2 Time
        //    //PAD ID
        //    //PAD Time
        //    //TW Time
        //    //Exit Time


        //    DataTable dtTransactions = new DataTable();
        //    dtTransactions.Columns.Add("Truck ID", typeof(string));
        //    dtTransactions.Columns.Add("REDS Time", typeof(string));
        //    dtTransactions.Columns.Add("Queue Time", typeof(string));
        //    dtTransactions.Columns.Add("EntryGate", typeof(string));
        //    dtTransactions.Columns.Add("PAD Time", typeof(string));
        //    dtTransactions.Columns.Add("TW Time", typeof(string));
        //    dtTransactions.Columns.Add("Exit Time", typeof(string));

        //    dtTransactions.Columns.Add("Status", typeof(string));

        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        DataRow row = dtTransactions.NewRow();


        //        //For Truck ....................................
        //        if (!string.IsNullOrEmpty(dr["Truck ID"].ToString()))
        //        {
        //            row["Truck ID"] = dr["Truck ID"];
        //            // Get ProponentName & HopperName
        //        }


        //        //For REDS ......................................
        //        if (!string.IsNullOrEmpty(dr["REDS Time"].ToString()))
        //        {
        //            row["REDS Time"] = "Passed";
        //        }
        //        else
        //        {
        //            row["REDS Time"] = "NA";
        //        }


        //        //For Queue ......................................
        //        if (!string.IsNullOrEmpty(dr["Queue Time"].ToString()))
        //        {
        //            row["Queue Time"] = "Arrived";
        //        }
        //        else
        //        {
        //            row["Queue Time"] = "NA";
        //        }


        //        //For Entry Gate .................................
        //        if (!string.IsNullOrEmpty(dr["Entry2 Time"].ToString()))
        //        {
        //            row["EntryGate"] = "Departed";
        //        }
        //        else
        //        {
        //            if (!string.IsNullOrEmpty(dr["Entry1 Time"].ToString()))
        //            {
        //                row["EntryGate"] = "Arrived";
        //            }
        //            else
        //            {
        //                row["EntryGate"] = "NA";
        //            }
        //        }



        //        //For PAD .....................................
        //        if (!string.IsNullOrEmpty(dr["PAD Time"].ToString()))
        //        {
        //            row["PAD Time"] = "Arrived";  // temp

        //            // Check  using  dr["HopperTime"].ToString() if the PAD is CORRECT ?
        //            //YES
        //            //  row["PAD"] = "Arrived";

        //            //NO
        //            //   row["PAD"] = "WrongPAD";
        //        }
        //        else
        //        {
        //            row["PAD Time"] = "NA";
        //        }



        //        //For Truck Wash & Exit ...............................
        //        if (!string.IsNullOrEmpty(dr["TW Time"].ToString()) && !string.IsNullOrEmpty(dr["Exit Time"].ToString()))   // 1 1
        //        {
        //            row["TW Time"] = "Arrived";
        //            row["Exit Time"] = "Arrived";
        //        }
        //        else if (string.IsNullOrEmpty(dr["TW Time"].ToString()) && !string.IsNullOrEmpty(dr["Exit Time"].ToString()))  // 0 1
        //        {
        //            row["TW Time"] = "Skipped";
        //            row["Exit Time"] = "Arrived";
        //        }
        //        else if (!string.IsNullOrEmpty(dr["TW Time"].ToString()) && string.IsNullOrEmpty(dr["Exit Time"].ToString()))  // 1 0
        //        {
        //            row["TW Time"] = "Arrived";
        //            row["Exit Time"] = "NA";
        //        }
        //        else if (string.IsNullOrEmpty(dr["TW Time"].ToString()) && string.IsNullOrEmpty(dr["Exit Time"].ToString()))   // 0 0
        //        {
        //            row["TW Time"] = "NA";
        //            row["Exit Time"] = "NA";
        //        }


        //        row["Status"] = dr["Status"];

        //        dtTransactions.Rows.Add(row);
        //        dtTransactions.AcceptChanges();
        //    }

        //    gvPreview.DataSource = dtTransactions;
        //    gvPreview.DataBind();

        //}


    }
}
