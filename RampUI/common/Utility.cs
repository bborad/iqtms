using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Connector.DBConnector;
using DBModule = Ramp.DBConnector.Adapter;
using Ramp.RFIDHWConnector.Adapters;
using System.Data;

namespace Ramp.UI.common
{
    public class Utility
    {
        public const string StatusArrived = "Status: Arrived";
        public const string StatusDeparted = "Status: Departed";
        public const string StatusNotArrived = "Not Arrived";
        public const string StatusInBound = "InBound: Truck yet not arrived at queue";
        public const string StatusNA = "NA";

        public const string ErrorSkipped = "Error: Truck wash skipped";
        public const string ErrorArrivedOvr = "Error: Expected arrival time exceed";
        public const string ErrorDepartedOvr = "Error: Expected departure time exceed";
        public const string ErrorOverdue = "Error: Expected time exceed";
        public const string ErrorWrongPAD = "Error: Truck at wrong PAD";


        // Closed transactions status messages
        public const string ErrorTransincomplete = "Incomplete transaction";
        public const string ErrorLeftWithTag = "Truck left with Tag";
        public const string StatusTransSuccess = "Transaction closed successfully";

        public const string VersionNo = "Version 1.3.1";

        //public static List<IReaders> GetAllReaderStatus()
        //{
        //    List<IReaders> lstDBReaders = DBModule.Readers.GetReaderStatusForSystemReport();

        //    IReader obj = null;

        //    foreach (IReaders dbReader in lstDBReaders)
        //    {
        //        if (dbReader.ID <= 0)
        //            continue;

        //        obj = DBConnectorToRFIDReader(dbReader);

        //        dbReader.ReaderStatus = obj.GetReaderStatus();
        //        obj.Dispose();
        //    }
        //    return lstDBReaders;
        //}

        //public static IReader DBConnectorToRFIDReader(IReaders objreader)
        //{
        //    IReader obj;
        //    try
        //    {
        //        obj = new RFIDHWConnector.Adapters.Reader();
        //        obj.ReaderName = objreader.ReaderName;
        //        obj.ReaderSerialNo = objreader.ReaderSerialNo;
        //        obj.IPAddress = objreader.IPAddress;
        //        obj.PortNo = objreader.PortNo;
        //        obj.Location = objreader.Location;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return obj;
        //}

        public static DataTable GetMarkersStatus()
        {
            DataTable dtMarkers1 , dtMarkers2 = null;
            try
            {

                dtMarkers1 = DBConnector.Adapter.Markers.GetMarkers(); //from DB

                dtMarkers2 = Reader.GetMarkersStatus(); // From Reader

                if (dtMarkers1 != null && dtMarkers2 == null)  // NO Record from Reader
                {
                    return dtMarkers1;
                }
                else if (dtMarkers1 == null && dtMarkers2 != null) // NO Record from Database
                {
                    return dtMarkers2;
                }
                else if (dtMarkers1 != null && dtMarkers2 != null)
                {
                    DataRow[] drr;
                    DataRow newRow;

                    foreach (DataRow dr in dtMarkers2.Rows)
                    {
                        drr = dtMarkers1.Select("LoopID = '" + dr["LoopID"].ToString() + "' OR SerialNo = '" + dr["SerialNo"].ToString() + "'");
                        if (drr != null && drr.Length > 0)
                        {
                            drr[0]["Status"] = dr["Status"].ToString();
                            drr[0]["Location"] = dr["Location"].ToString(); 
                        }
                        else
                        {
                            newRow = dtMarkers2.NewRow();
                            newRow["LoopID"] = dr["LoopID"];
                            newRow["Status"] = dr["Status"];
                            newRow["Location"] = dr["Location"];
                            newRow["SerialNo"] = dr["SerialNo"];

                            dtMarkers1.Rows.Add(newRow);
                        }
                    }

                    return dtMarkers1;
                }

            }
            catch
            {
            }

            return dtMarkers2;
        }

    }
}
