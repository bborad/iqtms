using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.RFIDHWConnector.Adapters;
using Ramp.DBConnector.Adapter;
using System.Text;
using System.Data;

namespace Ramp.UI
{
    public partial class hardwareconfigurationpage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["ReaderID"] = null;
                FillMrakers();
                FillLocations();
                FillReadersGrid();
                FillMarkerLocation();
            }
        }

        #region Locations

        /// <summary>
        /// Fill Locations 
        /// </summary>
        private void FillLocations()
        {
            List<ILocations> objLocationList = Locations.GetAllLocations();
            if (objLocationList != null)
            {
                ddlLocations.DataTextField = "LocationName";
                ddlLocations.DataValueField = "ID";
                ddlLocations.DataSource = objLocationList;
                ddlLocations.DataBind();
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = "No Locations exists";
            }
        }

        private void FillMarkerLocation()
        {
            List<ILocations> objMarkerLocationList = Locations.GetAllLocations();
            if (objMarkerLocationList != null)
            {
                ddlLoopLocation.DataTextField = "LocationName";
                ddlLoopLocation.DataValueField = "ID";
                ddlLoopLocation.DataSource = objMarkerLocationList;
                ddlLoopLocation.DataBind();
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = "No Locations exists";
            }
        }



        ///// <summary>
        ///// Open Add Location
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnAddLoc_Click(object sender, EventArgs e)
        //{
        //    divAddLocation.Visible = true;
        //}

        ///// <summary>
        ///// Add Location
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnAddLocation_Click(object sender, EventArgs e)
        //{
        //    Locations objLocation = new Locations();
        //    int LocationID = objLocation.InsertLocation(txtAddLocation.Text.Trim(), txtDescription.Text.Trim());
        //    if (LocationID > 0)
        //    {
        //        //litMsg.Text = "Markers added successfully";
        //        txtAddLocation.Text = string.Empty;
        //        txtDescription.Text = string.Empty;
        //        FillLocations();
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnCloseLoc_Click(object sender, EventArgs e)
        //{
        //    divAddLocation.Visible = false;
        //}

        #endregion

        #region marker

        /// <summary>
        /// Fill Markers
        /// </summary>
        private void FillMrakers()
        {
            // Get all markers and bind to checkbox list
            List<IMarkers> objMarkersList = Markers.GetAllMarkers();
            List<IMarkers> lstNewMarker = new List<IMarkers>();
            if (objMarkersList != null)
            {
                if (objMarkersList.Count > 0)
                {
                    foreach (IMarkers objMarker in objMarkersList)
                    {
                        objMarker.MarkerPosition = objMarker.MarkerLoopID + " - " + objMarker.MarkerPosition;
                        lstNewMarker.Add(objMarker);
                    }
                }
            }

            if (lstNewMarker != null)
            {

                if (objMarkersList != null)
                {
                    if (objMarkersList.Count > 0)
                    {
                        chklstMarkers.DataTextField = "MarkerPosition";
                        chklstMarkers.DataValueField = "ID";
                        chklstMarkers.DataSource = objMarkersList;
                        chklstMarkers.DataBind();
                    }
                    else
                    {
                        divMessage.Visible = true;
                        litMsg.Text = "No Marker exists";
                    }
                }
                else
                {
                    chklstMarkers.Items.Clear();  
                    divMessage.Visible = true;
                    litMsg.Text = "No Marker exists";
                }
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = "No Marker exists";
            }
        }

        /// <summary>
        /// Add Reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            divAddMarkers.Visible = true;
        }

        /// <summary>
        /// Close Location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClose_Click(object sender, EventArgs e)
        {
            divAddMarkers.Visible = false;
        }

        /// <summary>
        /// Add Marker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddMarker_Click(object sender, EventArgs e)
        {
            Markers objMarker = new Markers();
            //int markerID = objMarker.InsertMarker(txtLoopID.Text.Trim(), txtLocation.Text.Trim());
            
            int markerID = objMarker.InsertMarker(txtLoopID.Text.Trim(), Convert.ToString(ddlLoopLocation.SelectedItem.Text), Convert.ToInt32(ddlLoopLocation.SelectedValue),txtSerialNo.Text.Trim());
            if (markerID > 0)
            {
                //litMsg.Text = "Markers added successfully";
                txtLoopID.Text = string.Empty;
                txtSerialNo.Text = string.Empty;
                //txtLocation.Text = string.Empty;
                FillMarkerLocation();
                FillMrakers();
            }
        }

        /// <summary>
        /// Remove Marker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (divAllControls.Disabled == false)
            {
                divMessage.Visible = false;
                bool flgDelete = false;
                Markers objMarker = new Markers();
                StringBuilder markerIDs = new StringBuilder("");
                // Iterate through the Items collection of the CheckBoxList 
                // control and display the selected items.
                for (int i = 0; i < chklstMarkers.Items.Count; i++)
                {
                    if (chklstMarkers.Items[i].Selected)
                    {
                        markerIDs.Append("," + chklstMarkers.Items[i].Value);
                        flgDelete = true;
                    }
                }
                if (flgDelete == false)
                {
                    divMessage.Visible = true;
                    litMsg.Text = "Please select atleast one Loop ID to remove";
                }
                else
                {
                    divMessage.Visible = false;
                    int markerID = objMarker.DeleteMarkerByID(Convert.ToString(markerIDs));
                }
                FillMrakers();
                FillReadersGrid();
            }
            else
            {
                return;
            }
        }

        #endregion

        #region Reader

        /// <summary>
        /// Add Reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddReader_Click(object sender, EventArgs e)
        {
            // Check IPAddress Already Exists
            int id = 0;
            if (ViewState["ReaderID"] == null)
            {
                id = 0;
            }
            else
            {
                id = Convert.ToInt32(ViewState["ReaderID"]);
            }
            int ReaderID = Readers.CheckIPAddressAlreadyExists(id, txtIPAddress.Text.Trim());
            if (ReaderID != -1)
            {
                divMessage.Visible = true;
                litMsg.Text = "IP Address already exists";
                return;
            }
            else
            {
                divMessage.Visible = false;
                litMsg.Text = string.Empty;
                Readers objReaderDB = new Readers();
                StringBuilder markerIDs = new StringBuilder("");
                // Iterate through the Items collection of the CheckBoxList 
                // control and display the selected items.
                for (int i = 0; i < chklstMarkers.Items.Count; i++)
                {
                    if (chklstMarkers.Items[i].Selected)
                    {
                        markerIDs.Append("," + chklstMarkers.Items[i].Value);
                    }
                }
                int markerID = 0;
                IReader objReader = new Reader(txtIPAddress.Text.Trim(), Convert.ToInt32(txtPort.Text.Trim()));
                string serialNo = objReader.GetSerialNo();
                objReader.Dispose();
                if (btnAddReader.Text == "Add")
                {
                    markerID = objReaderDB.InsertReader(serialNo, Convert.ToString(ddlLocations.SelectedItem.Text), txtIPAddress.Text.Trim(), Convert.ToInt32(txtPort.Text.Trim()), Convert.ToString(markerIDs), Convert.ToInt32(ddlLocations.SelectedValue), Convert.ToInt32(txtPower.Text.Trim()));
                }
                else
                {
                    markerID = objReaderDB.UpdateReader(Convert.ToInt32(ViewState["ReaderID"]), serialNo, Convert.ToString(ddlLocations.SelectedItem.Text), txtIPAddress.Text.Trim(), Convert.ToInt32(txtPort.Text.Trim()), Convert.ToString(markerIDs), Convert.ToInt32(ddlLocations.SelectedValue), Convert.ToInt32(txtPower.Text.Trim()));
                }
                ResetControls();
            }
        }

        /// <summary>
        /// Fill Readers in grid
        /// </summary>
        private void FillReadersGrid()
        {
            DataTable dtReaders = Readers.GetAllReadersWithItsMarker();
            gvReaders.DataSource = dtReaders;
            gvReaders.DataBind();
        }

        /// <summary>
        /// Page index change for reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvReaders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //bind the values to our GridView
            gvReaders.PageIndex = e.NewPageIndex;
            FillReadersGrid();
            EnableControls();
        }

        /// <summary>
        /// Edit reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            if (ViewState["ReaderID"] != null)
            {
                FillReadersDetailToEdit(Convert.ToInt32(ViewState["ReaderID"]));
                btnAddReader.Text = "Apply";
                EnableControls();
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = "Please select reader to edit";
            }
        }

        /// <summary>
        /// Get selected reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvReaders_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                int iGdKey = Convert.ToInt32(e.CommandArgument.ToString());
                LinkButton lbtn = (LinkButton)e.CommandSource;
                if (lbtn.Text == "Select")
                {
                    FillReadersDetailToEdit(iGdKey);
                    DisableControls();
                    ViewState["ReaderID"] = iGdKey;
                    divMessage.Visible = false;
                }
            }
        }

        /// <summary>
        /// Fiil readers detail to edit
        /// </summary>
        /// <param name="ReaderID"></param>
        private void FillReadersDetailToEdit(int ReaderID)
        {
            DataSet dsReader = Readers.ReaderAndItsMarkersDetailByID(ReaderID);
            if (dsReader != null)
            {
                if (dsReader.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsReader.Tables[0].Rows)
                    {
                        ddlLocations.SelectedValue = Convert.ToString(dr["Location"]);
                        txtIPAddress.Text = Convert.ToString(dr["IPAddress"]);
                        txtPort.Text = Convert.ToString(dr["PortNo"]);
                        txtPower.Text = Convert.ToString(dr["Power"]);
                    }
                    if (dsReader.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsReader.Tables[1].Rows)
                        {
                            for (int i = 0; i < chklstMarkers.Items.Count; i++)
                            {
                                if (chklstMarkers.Items[i].Value == Convert.ToString(dr["MarkerID"]))
                                {
                                    chklstMarkers.Items[i].Selected = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delete selected readers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            if (ViewState["ReaderID"] != null)
            {
                Readers objReader = new Readers();
                FillReadersDetailToEdit(Convert.ToInt32(ViewState["ReaderID"]));
                objReader.DeleteReaderByID(Convert.ToInt32(ViewState["ReaderID"]));
                ResetControls();
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = "Please select reader to delete";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //if (btnAddReader.Text == "Apply")
            //{
            //    if (ViewState["ReaderID"] != null)
            //    {
            //        FillReadersDetailToEdit(Convert.ToInt32(ViewState["ReaderID"]));
            //        divMessage.Visible = false;
            //        litMsg.Text = string.Empty;
            //    }
            //}
            //else
            //{
            ResetControls();
            //}
        }

        /// <summary>
        /// Reset all controls
        /// </summary>
        protected void ResetControls()
        {
            txtLoopID.Text = string.Empty;
            //txtLocation.Text = string.Empty;
            ddlLoopLocation.SelectedIndex = 0;
            //txtAddLocation.Text = string.Empty;
            //txtDescription.Text = string.Empty;
            txtIPAddress.Text = string.Empty;
            txtPort.Text = string.Empty;
            txtPower.Text = string.Empty;
            FillReadersGrid();
            FillMrakers();
            FillLocations();
            ddlLocations.SelectedIndex = 0;
            EnableControls();
            ViewState["ReaderID"] = null;
            divMessage.Visible = false;
            litMsg.Text = string.Empty;
            btnAddReader.Text = "Add";
        }
        /// <summary>
        /// Enable all controls
        /// </summary>
        protected void EnableControls()
        {
            //divAllControls.Disabled = false;
            btnRemove.Enabled = true;
            btnAddReader.Enabled = true;
            btnCancel.Enabled = true;
            txtIPAddress.Enabled = true;
            txtPort.Enabled = true;
            txtPower.Enabled = true;
            //btnAddLoc.Enabled = true;
            btnAddNew.Enabled = true;
            ddlLocations.Enabled = true;
            chklstMarkers.Enabled = true;
        }
        /// <summary>
        /// Disable all controls
        /// </summary>
        protected void DisableControls()
        {
            //divAllControls.Disabled = true;
            btnRemove.Enabled = false;
            btnAddReader.Enabled = false;
            //btnCancel.Enabled = false;
            txtIPAddress.Enabled = false;
            txtPort.Enabled = false;
            txtPower.Enabled = false;
            //btnAddLoc.Enabled = false;
            btnAddNew.Enabled = false;
            ddlLocations.Enabled = false;
            chklstMarkers.Enabled = false;
        }
        #endregion

         


    }
}
