﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="hardwareconfigurationpage.aspx.cs"
    Inherits="Ramp.UI.hardwareconfigurationpage" MasterPageFile="~/masterpages/SiteMaster.Master"
    Title="Hardware Configuration " %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

    <script type="text/javascript">
        /* this function shows the pop-up when
        user moves the mouse over the link */



        /* this function shows the pop-up when
        user moves the mouse over the link */
        function ShowMarkerDiv() {
            /* get the mouse left position */
            // x = event.clientX + document.body.scrollLeft;
            /* get the mouse top position  */
            //            y = event.clientY + document.body.scrollTop + 25;
            /* display the pop-up */
            document.getElementById("<%= divAddMarkers.ClientID %>").style.display = "block";
            /* set the pop-up's left */
            //            document.getElementById("<%= divAddMarkers.ClientID %>").style.left = x;
            /* set the pop-up's top */
            //            document.getElementById("<%= divAddMarkers.ClientID %>").style.top = y;
        }
        /* this function hides the pop-up when
        user moves the mouse out of the link */
        function HideMarkerDiv() {
            /* hide the pop-up */
            document.getElementById("<%= divAddMarkers.ClientID %>").style.display = "none";
        }
        function checkNumerickFormat(oEvent) {
            retVal = true;

            oEvent = oEvent || window.event;
            var txtField = oEvent.target || oEvent.srcElement;



            var keyCode = oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;

            // alert(keyCode);

            if (keyCode == 8 || keyCode == 9)
                return true;
            if (!(keyCode >= 48 && keyCode <= 57))
                return false;
            if (keyCode != null) {
                var strkey = String.fromCharCode(keyCode);

                if ((strkey < '0') || (strkey > '9')) {
                    if (AllowKeys.indexOf(keyCode) != -1)
                        retVal = true;
                    else
                        retVal = false;
                }
            }
            document.getElementById(txtField.id).focus();
            return retVal;
        }

        function checkNumerickFormatWithDecimal(oEvent) {
            retVal = true;

            oEvent = oEvent || window.event;
            var txtField = oEvent.target || oEvent.srcElement;

         

            var keyCode = oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;

            if (keyCode == 46 && txtField.value.indexOf(".") >= 0) {
                return false;
            }

            if (keyCode == 8 || keyCode == 9 || keyCode == 46)
                return true;
            if (!(keyCode >= 48 && keyCode <= 57))
                return false;
            if (keyCode != null) {
                var strkey = String.fromCharCode(keyCode);

                if ((strkey < '0') || (strkey > '9')) {
                    if (AllowKeys.indexOf(keyCode) != -1)
                        retVal = true;
                    else
                        retVal = false;
                }
            }
            document.getElementById(txtField.id).focus();
            return retVal;
        }
        
    </script>

    <div id="wrap-page">
        <div id="mid_section">
            <div id="divMessage" runat="server" visible="false">
                <center>
                    <table style="width: 50%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;"><span style="color: red">
                                    <asp:Literal ID="litMsg" runat="server"></asp:Literal></span> </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <table width="70%" class="border_strcture1">
                <tr>
                    <td colspan="3" align="center">
                        <b>READER CONFIGURATION </b>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="border_strcture1">
                        <div id="divAllControls" runat="server" style="display: block;">
                            <table width="100%">
                                <tr>
                                    <td align="right" width="30%" valign="top">
                                        Name:
                                    </td>
                                    <td width="70%">
                                        <asp:DropDownList ID="ddlLocations" runat="server" DataTextField="LocationName" DataValueField="ID"
                                            ValidationGroup="valGrpAddReader">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvLocations" runat="server" ErrorMessage="* Required"
                                            ControlToValidate="ddlLocations" Display="Dynamic" ValidationGroup="valGrpAddReader"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                        <%--<asp:Button ID="btnAddLoc" runat="server" Text="Add" class="but" OnClientClick="ShowLocationDiv();return false;" />
                                        <br />
                                        <div id="divAddLocation" runat="server" style="display: none; position: absolute;
                                            background: #fff;" class="border_strcture1">
                                            <table width="100%">
                                                <tr>
                                                    <th class="border_strcture1" colspan="2" align="left">
                                                        Add New Location
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Location Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddLocation" runat="server" class="txtBox" MaxLength="100" ValidationGroup="valGrpAddLocation"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvAddLocation" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="txtAddLocation" Display="Dynamic" ValidationGroup="valGrpAddLocation"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDescription" runat="server" class="txtBox" MaxLength="50" ValidationGroup="valGrpAddLocation"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="txtDescription" Display="Dynamic" ValidationGroup="valGrpAddLocation"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddLocation" runat="server" Text="Add" class="but" CausesValidation="true"
                                                            ValidationGroup="valGrpAddLocation" OnClick="btnAddLocation_Click" />
                                                        <asp:Button ID="btnCloseLoc" runat="server" Text="Close" class="but" OnClientClick="HideLocationDiv();return false;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        IP Address:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtIPAddress" runat="server" class="txtBox" MaxLength="15" ValidationGroup="valGrpAddReader"
                                            Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvTruckID" runat="server" ErrorMessage="* Required"
                                            ControlToValidate="txtIPAddress" Display="Dynamic" ValidationGroup="valGrpAddReader"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regExpIPAddress" runat="server" ValidationExpression="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
                                            ErrorMessage="Invalid" ValidationGroup="valGrpAddReader" ControlToValidate="txtIPAddress"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Port:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPort" runat="server" class="txtBox" MaxLength="4" ValidationGroup="valGrpAddReader"
                                            onkeypress="if(!checkNumerickFormat(event)){return false}"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPort" runat="server" ErrorMessage="* Required"
                                            ControlToValidate="txtPort" Display="Dynamic" ValidationGroup="valGrpAddReader"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="rngValPort" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                            MaximumValue="9999" ControlToValidate="txtPort" ValidationGroup="valGrpAddReader"> </asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Power:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPower" runat="server" class="txtBox" MaxLength="4" ValidationGroup="valGrpAddReader"
                                            onkeypress="if(!checkNumerickFormatWithDecimal(event)){return false}"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPower" runat="server" ErrorMessage="* Required"
                                            ControlToValidate="txtPower" Display="Dynamic" ValidationGroup="valGrpAddReader"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="rngValPower" runat="server" ErrorMessage="Incorrect" MinimumValue="-10"
                                            Type="Integer" MaximumValue="30" ControlToValidate="txtPower" ValidationGroup="Upvalidation"> </asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Loop ID:<br />
                                        <br />
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add New" class="but" Width="70px"
                                            OnClientClick="ShowMarkerDiv(); return false;" /><br />
                                        <br />
                                        <asp:Button ID="btnRemove" runat="server" Text="Remove" class="but" Width="70px"
                                            OnClick="btnRemove_Click" />
                                    </td>
                                    <td valign="top">
                                        <div class="add_remove_box" style="height: 100px; width: 150px">
                                            <asp:CheckBoxList ID="chklstMarkers" runat="server" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                        <div id="divAddMarkers" runat="server" style="height: 170px; width: 250px;display: none; position: absolute;
                                            background: #fff; margin-bottom:15px;" class="border_strcture1">
                                            <table width="100%">
                                                <tr>
                                                    <th class="border_strcture1" colspan="2" align="left">
                                                        Add New Loop
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Loop ID:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtLoopID" runat="server" class="txtBox" MaxLength="100" ValidationGroup="valGrpAddMarker"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvLoopID" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="txtLoopID" Display="Dynamic" ValidationGroup="valGrpAddMarker"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                       Serial No:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtSerialNo" runat="server" class="txtBox" MaxLength="100"></asp:TextBox>
                                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="txtLoopID" Display="Dynamic" ValidationGroup="valGrpAddMarker"></asp:RequiredFieldValidator>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Location:
                                                    </td>
                                                    <td>
                                                        <%--<asp:TextBox ID="txtLocation" runat="server" class="txtBox" MaxLength="50" ValidationGroup="valGrpAddMarker"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="txtLocation" Display="Dynamic" ValidationGroup="valGrpAddMarker"></asp:RequiredFieldValidator>--%>
                                                        <asp:DropDownList ID="ddlLoopLocation" runat="server" DataTextField="LocationName"
                                                            DataValueField="ID" ValidationGroup="valGrpAddMarker">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ErrorMessage="* Required"
                                                            ControlToValidate="ddlLoopLocation" Display="Dynamic" ValidationGroup="valGrpAddMarker"
                                                            InitialValue="0"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Button ID="btnAddMarker" runat="server" Text="Add" class="but" CausesValidation="true"
                                                            ValidationGroup="valGrpAddMarker" OnClick="btnAddMarker_Click" />
                                                        <asp:Button ID="btnClose" runat="server" Text="Close" class="but" OnClientClick="HideMarkerDiv();return false;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAddReader" runat="server" Text="Add" Width="55px" class="but"
                                            ValidationGroup="valGrpAddReader" OnClick="btnAddReader_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="but" Width="55px"
                                            OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="40%" valign="top" class="border_strcture1">
                        <asp:GridView ID="gvReaders" runat="server" AutoGenerateColumns="False" Width="100%"
                            border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture" DataKeyNames="ID"
                            AllowPaging="true" PageSize="10" OnPageIndexChanging="gvReaders_PageIndexChanging"
                            OnRowCommand="gvReaders_RowCommand">
                            <Columns>
                                <asp:TemplateField ShowHeader="False" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSelect" runat="server" CausesValidation="False" CommandName="Select"
                                            CommandArgument='<%# Bind("ID") %>' Text="Select"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Left" HeaderText="ID" SortExpression="ID"
                                    Visible="false" />
                                <asp:BoundField DataField="ReaderName" ItemStyle-HorizontalAlign="Left" HeaderText="Reader"
                                    SortExpression="ReaderName" />
                                <asp:BoundField DataField="IPAddress" ItemStyle-HorizontalAlign="Left" HeaderText="IP Address"
                                    SortExpression="IPAddress" />
                                <asp:BoundField DataField="PortNo" ItemStyle-HorizontalAlign="Left" HeaderText="Port"
                                    SortExpression="PortNo" />
                                <asp:BoundField DataField="LoopIDs" ItemStyle-HorizontalAlign="Left" HeaderText="LoopIDs"
                                    SortExpression="LoopIDs" />
                                <asp:BoundField DataField="Power" ItemStyle-HorizontalAlign="Left" HeaderText="Power"
                                    SortExpression="Power" />
                            </Columns>
                            <EmptyDataTemplate>
                                No Readers found
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                    <td width="5%" align="center" valign="top">
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" class="but" Width="55px" OnClick="btnEdit_Click" />
                        <br />
                        <br />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" class="but" Width="55px"
                            OnClick="btnDelete_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
