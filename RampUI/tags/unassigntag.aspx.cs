using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.IO;
using System.Data.Common;
using Ramp.DBConnector.Adapter;
using System.Collections;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Common;

namespace Ramp.UI.tags
{
    public partial class unassigntag : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //check added by Som 31 Aug 2010
                if (Session["LoggedInUser"] != null)
                {
                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
                
                ClearAllSessions();

                FinalTagList();
                ClearAllSessions();
            }
        }

        /// <summary>
        /// Upload Tags by CSV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                // Upload CSV file in Upload file folder of the application
                if (upFileTags.PostedFile.FileName.Trim() != string.Empty)
                {
                    string fn = System.IO.Path.GetFileName(upFileTags.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("../UploadFiles") + "\\" + fn;
                    if (SaveLocation.Trim().Length > 0)
                    {
                        FileInfo fi = new FileInfo(SaveLocation);
                        if (fi.Exists)//if file exists then delete it
                        {
                            fi.Delete();
                        }
                    }
                    try
                    {
                        upFileTags.PostedFile.SaveAs(SaveLocation);
                        // Import tag ids from the CSV file and list them into list box
                        ImportFile(SaveLocation, fn);

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Import data from CSV / text files
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private void ImportFile(string filePath, string fileName)
        {
            try
            {
                //btnAddAll.Enabled = true;
                btnRemove.Enabled = true;
                DataSet ds = new DataSet();
                try
                {
                    // Create a connection string DBQ attribute sets the path of directory which contains CSV or text files 
                    string strConnString = @"Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Server.MapPath("../UploadFiles") + ";";
                    string sql_select;
                    System.Data.Odbc.OdbcConnection conn;
                    //Create connection to CSV/text file
                    conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());
                    //Open the connection 
                    conn.Open();
                    //Fetch records from CSV/text
                    sql_select = "select TagID from [" + fileName + "]";
                    //Initialise DataAdapter with file data
                    DataAdapter obj_oledb_da = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);
                    //Fill dataset with the records from CSV file
                    obj_oledb_da.Fill(ds);
                    //Declare variable for Error
                    string strError = "";
                    //Close Connection to CSV file
                    conn.Close();
                    // Validate Dataset is empty or not
                    int tagsRemoved = 0;
                    int tagsadded = 0;
                    int totaltags = 0;

                    if (ds != null)
                    {
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            totaltags = ds.Tables[0].Rows.Count;
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                // Remove the tagid if its length is less than 9
                                if ((dr["TagID"] != DBNull.Value))
                                {
                                    int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();
                                    if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1))
                                    {
                                        dr.Delete();
                                        tagsRemoved++;
                                    }
                                }
                            }
                            ds.Tables[0].AcceptChanges();
                        }
                    }
                    //// Total tags added from the CSV
                    //tagsadded = totaltags - tagsRemoved;
                    //ViewState["tagsadded"] = tagsadded;
                    // Assign dataset value to tag list

                    Session["dtUploadedTags"] = ds.Tables[0];

                    //if (Session["dtUploadedUnAssignTagsList"] != null)
                    //{
                    //    DataTable dtSessionUp = new DataTable();
                    //    dtSessionUp = (DataTable)Session["dtUploadedUnAssignTagsList"];
                    //    dtSessionUp.Merge(ds.Tables[0], true, MissingSchemaAction.Ignore);
                    //    Session["dtUploadedUnAssignTagsList"] = dtSessionUp;
                    //}
                    //else
                    //{
                    //    Session["dtUploadedUnAssignTagsList"] = ds.Tables[0];
                    //}
                    if (ds != null)
                    {
                        litMessage.Text = string.Empty;


                        Tags objTag = new Tags();
                        // Check if tag is already assigned 


                        // Create tags CSV string
                        ArrayList lstTagCsv = new ArrayList();
                        StringBuilder strLstTagIDs = new StringBuilder("'-1'");

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if ((dr["TagID"] != null))
                            {
                                if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(strLstTagIDs);
                                    strLstTagIDs = new StringBuilder("'-1'");
                                }
                                else
                                {
                                    strLstTagIDs.Append("," + "'" + Convert.ToString(dr["TagID"]) + "'");
                                }
                            }
                        }
                        // Insert bulk Tagids by CSV string

                        if (strLstTagIDs.Length >= 5)
                        {
                            lstTagCsv.Add(strLstTagIDs);
                        }

                        // Insert bulk nuber of tags using CSV string
                        DataTable dtTagsforUnassign = new DataTable();
                        ArrayList lstDataTables = new ArrayList();
                        int countaddedrows = 0;

                        foreach (StringBuilder strCsv in lstTagCsv)
                        {
                            dtTagsforUnassign = Tags.GetTagsDetailForUnAssign(Convert.ToString(strCsv));
                            lstDataTables.Add(dtTagsforUnassign);
                        }
                        DataTable dtUnAssignTagsList = new DataTable();
                        foreach (DataTable dtTags in lstDataTables)
                        {
                            dtUnAssignTagsList.Merge(dtTags);
                        }

                        // if session already contain tags

                        if (Session["dtUploadedUnAssignTagsList"] != null)
                        {
                            DataTable dtSessionUp = new DataTable();
                            dtSessionUp = (DataTable)Session["dtUploadedUnAssignTagsList"];
                            dtSessionUp.Merge(dtUnAssignTagsList, true, MissingSchemaAction.Ignore);
                            Session["dtUploadedUnAssignTagsList"] = dtSessionUp;
                        }
                        else
                        {
                            Session["dtUploadedUnAssignTagsList"] = dtUnAssignTagsList;
                        }

                        if (dtUnAssignTagsList != null)
                        {
                            FinalTagList();
                        }
                    }
                    else
                    {
                        litMessage.Visible = true;
                        litMessage.Text = "Please select appropriate PAD ID and Proponent ID";

                    }
                }

                catch (Exception) //Error
                {
                    throw;
                }
            }
            catch (Exception) //Error
            {
                throw;
            }

        }

        protected void btnAddTag_Click(object sender, EventArgs e)
        {
            litMessage.Text = "";
            try
            {
                DataTable dtTagsforUnassign = Tags.GetTagsDetailForUnAssign("'" + Convert.ToString(txtTagID.Text.Trim()) + "'");
                if (dtTagsforUnassign != null && dtTagsforUnassign.Rows.Count > 0)
                {
                    // Add Tags detail to grid
                    if (Session["dtManuallyAddedTags"] != null)
                    {
                        DataTable dtSessionAdd = new DataTable();
                        dtSessionAdd = (DataTable)Session["dtManuallyAddedTags"];
                        dtSessionAdd.Merge(dtTagsforUnassign, true, MissingSchemaAction.Ignore);
                        Session["dtManuallyAddedTags"] = dtSessionAdd;
                    }
                    else
                    {
                        Session["dtManuallyAddedTags"] = dtTagsforUnassign;
                    }
                    FinalTagList();
                }
                else
                {
                    litMessage.Text =  "Tag may be issued Or not found.";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        ///  Delete selcted tags from tag list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // Delete from Uploaded tag list
                DataTable dtUploadedUnAssignTagsList = null;
                if (Session["dtUploadedUnAssignTagsList"] != null)
                {
                    dtUploadedUnAssignTagsList = (DataTable)Session["dtUploadedUnAssignTagsList"];
                }

                for (int i = 0; i <= gvUnAssignTags.Rows.Count - 1; i++)
                {
                    CheckBox chkTagID = (CheckBox)gvUnAssignTags.Rows[i].FindControl("chkTagID");
                    if (chkTagID.Checked == true)
                    {
                        if (dtUploadedUnAssignTagsList != null)
                        {
                            foreach (DataRow dr in dtUploadedUnAssignTagsList.Rows)
                            {
                                if ((dr["TagRFID"] != DBNull.Value))
                                {
                                    if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(gvUnAssignTags.DataKeys[i].Value))


                                    //int TagIDcount = (dsSessionUploadTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(gvAssignTags.DataKeys[i].Value) + "'")).Count();
                                    //if (TagIDcount > 0)
                                    {
                                        dr.Delete();
                                        i = i - 1;

                                        break;
                                    }
                                }
                            }
                            dtUploadedUnAssignTagsList.AcceptChanges();
                        }
                    }
                }

                // Delete from manually added tag list
                DataTable dtManuallyAddedTags = null;
                if (Session["dtManuallyAddedTags"] != null)
                {
                    dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                }

                for (int i = 0; i <= gvUnAssignTags.Rows.Count - 1; i++)
                {
                    CheckBox chkTagID = (CheckBox)gvUnAssignTags.Rows[i].FindControl("chkTagID");
                    if (chkTagID.Checked == true)
                    {
                        if (dtManuallyAddedTags != null)
                        {
                            foreach (DataRow dr in dtManuallyAddedTags.Rows)
                            {
                                if ((dr["TagRFID"] != DBNull.Value))
                                {
                                    if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(gvUnAssignTags.DataKeys[i].Value))


                                    //int TagIDcount = (dsSessionUploadTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(gvAssignTags.DataKeys[i].Value) + "'")).Count();
                                    //if (TagIDcount > 0)
                                    {
                                        dr.Delete();
                                        i = i - 1;

                                        break;
                                    }
                                }
                            }
                            dtManuallyAddedTags.AcceptChanges();
                        }
                    }
                }

                // Delete from final tag list
                DataTable dsFinalTagList = new DataTable();
                if (Session["dsFinalTagList"] != null)
                {
                    dsFinalTagList = (DataTable)Session["dsFinalTagList"];
                }

                for (int i = 0; i <= gvUnAssignTags.Rows.Count - 1; i++)
                {
                    CheckBox chkTagID = (CheckBox)gvUnAssignTags.Rows[i].FindControl("chkTagID");
                    if (chkTagID.Checked == true)
                    {
                        if (dsFinalTagList != null)
                        {
                            foreach (DataRow dr in dsFinalTagList.Rows)
                            {
                                if ((dr["TagRFID"] != DBNull.Value))
                                {
                                    if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(gvUnAssignTags.DataKeys[i].Value))
                                    {
                                        dr.Delete();
                                        i = i - 1;
                                        break;
                                    }
                                }
                            }
                            dsFinalTagList.AcceptChanges();
                        }
                    }
                }
                gvUnAssignTags.DataSource = dsFinalTagList;
                gvUnAssignTags.DataBind();
                Session["dtUploadedUnAssignTagsList"] = dtUploadedUnAssignTagsList;
                Session["dtManuallyAddedTags"] = dtManuallyAddedTags;
                Session["dsFinalTagsList"] = dsFinalTagList;

                litTotal.Text = Convert.ToString(dsFinalTagList.Rows.Count);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Un Assign Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUnAssign_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList lstTagCsv = new ArrayList();
                StringBuilder strLstTagIDs = new StringBuilder("'-1'");
                if (gvUnAssignTags.Rows.Count != 0)
                {
                    if (Session["dsFinalTagList"] != null)
                    {
                        DataTable dsFinalTagList = (DataTable)Session["dsFinalTagList"];
                        foreach (DataRow dr in dsFinalTagList.Rows)
                        {
                            if (dr["TagRFID"] != DBNull.Value)
                            {
                                if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(Convert.ToString(dr["TagRFID"]));
                                    strLstTagIDs = new StringBuilder("'-1'");
                                }
                                else
                                {
                                    strLstTagIDs.Append("," + "'" + Convert.ToString(dr["TagRFID"]) + "'");
                                }
                            }
                        }
                        // Insert bulk Tagids by CSV string

                        if (strLstTagIDs.Length >= 5)
                        {
                            lstTagCsv.Add(strLstTagIDs);
                        }
                        //int index = 0;
                        //int countTagsAdded = 0;
                        string tags = string.Empty, trucks = string.Empty;
                        Tags objTags = new Tags();
                        int countaddedrows = 0;
                        foreach (StringBuilder strCsv in lstTagCsv)
                        {
                            countaddedrows += countaddedrows + objTags.UnAssignTags(Convert.ToString(strCsv));
                        }
                        if (countaddedrows != 0)
                        {
                           
                            ClearAllSessions();
                            txtTagID.Text = string.Empty;
                            FinalTagList();
                            litMessage.Text = countaddedrows + " Tag(s) successfully unassigned ";
                        }
                        else
                        {
                            litMessage.Text = "Tag(s) not exists/already unassigned. Please try again with other tag(s) list";
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Remove Tags from Tags list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList lstTagCsv = new ArrayList();
                StringBuilder strLstTagIDs = new StringBuilder("'-1'");

                Hoppers objHopper = new Hoppers();

                if (gvUnAssignTags.Rows.Count != 0)
                {
                    if (Session["dsFinalTagList"] != null)
                    {
                        DataTable dsFinalTagList = (DataTable)Session["dsFinalTagList"];
                        foreach (DataRow dr in dsFinalTagList.Rows)
                        {
                            if (dr["TagRFID"] != DBNull.Value)
                            {
                                if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(Convert.ToString(dr["TagRFID"]));
                                    strLstTagIDs = new StringBuilder("'-1'");
                                }
                                else
                                {
                                    strLstTagIDs.Append(",'" + Convert.ToString(dr["TagRFID"]) + "'");
                                }
                            }
                        }
                        // Insert bulk Tagids by CSV string

                        if (strLstTagIDs.Length >= 5)
                        {
                            lstTagCsv.Add(strLstTagIDs);
                        }
                        //int index = 0;
                        //int countTagsAdded = 0;
                        string tags = string.Empty;
                        Tags objTags = new Tags();
                        int countaddedrows = 0;
                        foreach (StringBuilder strCsv in lstTagCsv)
                        {
                            countaddedrows += countaddedrows + objTags.RemoveTags(Convert.ToString(strCsv));
                        }

                        ClearAllSessions();
                        txtTagID.Text = string.Empty;
                        FinalTagList();
                        litMessage.Text = " Tag(s) successfully removed ";
                        
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Create final list of tags
        /// </summary>
        private void FinalTagList()
        {
            try
            {
                EnableControls();
                gvUnAssignTags.Enabled = true;
                DataTable dsFinalTagsList = new DataTable();

                //dsFinalTagsList.Tables[0].Columns.Add("TagID", typeof(string));
                //dsFinalTagsList.Tables[0].Columns.Add("TruckID", typeof(int));

                if ((Session["dtUploadedUnAssignTagsList"] != null) && (Session["dtManuallyAddedTags"] != null))
                {
                    DataTable dsUploadedTags = (DataTable)Session["dtUploadedUnAssignTagsList"];
                    DataTable dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                    dsFinalTagsList.Merge(dsUploadedTags);
                    dsFinalTagsList.Merge(dtManuallyAddedTags, true, MissingSchemaAction.Ignore);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);
                }
                else if (Session["dtUploadedUnAssignTagsList"] != null)
                {
                    DataTable dsUploadedTags = (DataTable)Session["dtUploadedUnAssignTagsList"];
                    dsFinalTagsList.Merge(dsUploadedTags);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);

                }
                else if (Session["dtManuallyAddedTags"] != null)
                {
                    DataTable dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                    dsFinalTagsList.Merge(dtManuallyAddedTags);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);
                }
                else
                {
                    dsFinalTagsList = EmptyGridDtatSource();
                    DisableControls();
                }
                if (dsFinalTagsList.Rows.Count == 0)
                {
                    dsFinalTagsList = EmptyGridDtatSource();
                    DisableControls();
                }
                Session["dsFinalTagList"] = dsFinalTagsList;
                gvUnAssignTags.DataSource = dsFinalTagsList;
                gvUnAssignTags.DataBind();
                if (gvUnAssignTags.Enabled == true)
                {
                    litTotal.Text = Convert.ToString(dsFinalTagsList.Rows.Count);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Manage indexing on page index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnAssignTags_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dsFinalTagList = (DataTable)Session["dsFinalTagList"];
            //bind the values to our GridView
            gvUnAssignTags.PageIndex = e.NewPageIndex;
            gvUnAssignTags.DataSource = dsFinalTagList;
            gvUnAssignTags.DataBind();
        }

        private DataTable EmptyGridDtatSource()
        {
            DataTable dtEmptyTagList = new DataTable("dtEmptyTagList");

            dtEmptyTagList.Columns.Add("ID");
            dtEmptyTagList.Columns.Add("TagRFID");
            dtEmptyTagList.Columns.Add("ProponentID");
            dtEmptyTagList.Columns.Add("ProponentName");
            dtEmptyTagList.Columns.Add("HopperID");
            dtEmptyTagList.Columns.Add("HopperName");
            dtEmptyTagList.Columns.Add("Assigned");
            dtEmptyTagList.Columns.Add("TruckID");

            // Add new row of data to datatable
            DataRow drEmptyTagList = dtEmptyTagList.NewRow();

            //Adding the created data row to the datatable
            dtEmptyTagList.Rows.Add(drEmptyTagList);
            dtEmptyTagList.AcceptChanges();
            //dsFinalTagsList = dtEmptyTagList;
            gvUnAssignTags.Enabled = false;
            litTotal.Text = "0";

            return dtEmptyTagList;
        }

        /// <summary>
        /// Disable all controls
        /// </summary>
        private void DisableControls()
        {
            btnDelete.Enabled = false;
            btnRemove.Enabled = false;
            btnUnAssign.Enabled = false;
        }

        /// <summary>
        /// Enable all controls
        /// </summary>
        private void EnableControls()
        {
            btnDelete.Enabled = true;
            btnRemove.Enabled = true;
            btnUnAssign.Enabled = true;
        }
        private void ClearAllSessions()
        {
            Session["dtUploadedUnAssignTagsList"] = null;
            Session["dtManuallyAddedTags"] = null;
            Session["dsFinalTagList"] = null;
            litMessage.Text = string.Empty;
        }

        /// <summary>
        /// Remove duplicate tags from list
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable RemoveDuplicateTagsFromList(DataTable dt)
        {
            bool flgTagRemoved = false;
            if (dt != null)
            {
                if (dt.Rows.Count != 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // Remove the tagid if its length is less than 9
                        if ((dr["TagRFID"] != null))
                        {
                            int TagIDcount = (dt.Select("TagRFID=" + "'" + Convert.ToString(dr["TagRFID"]) + "'")).Count();
                            int TruckIDCount = 0;

                            if ((TagIDcount > 1))
                            {
                                dr.Delete();
                                flgTagRemoved = true;
                            }
                        }
                    }
                    dt.AcceptChanges();
                }
            }
            return dt;
        }
    }
}
