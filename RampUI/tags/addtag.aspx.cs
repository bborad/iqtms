using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.IO;
using System.Data;
using System.Data.Common;
using Ramp.DBConnector.Adapter;
using System.Text;
using System.Collections;
using Ramp.RFIDHWConnector.Adapters;
using Ramp.MiddlewareController.Common;

namespace Ramp.UI.tags
{
    public partial class addtag : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
         
            litExMsg.Text = string.Empty;
            if (!IsPostBack)
            {

            }
            if (lstAddRemoveTag.Items.Count == 0)
            {
                DisableAllButtons();
            }
            else
            {
                EnableAllButtons();
            }
        }

        #region Add Tags

        /// <summary>
        /// Upload text/csv file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            try
            {
                // Upload CSV file in Upload file folder of the application
                if (upFileTags.PostedFile.FileName.Trim() != "")
                {
                    string fn = System.IO.Path.GetFileName(upFileTags.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("../UploadFiles") + "\\" + fn;
                    if (SaveLocation.Trim().Length > 0)
                    {
                        FileInfo fi = new FileInfo(SaveLocation);
                        if (fi.Exists)//if file exists then delete it
                        {
                            fi.Delete();
                        }
                    }
                    try
                    {
                        upFileTags.PostedFile.SaveAs(SaveLocation);
                        // Import tag ids from the CSV file and list them into list box
                        ImportFile(SaveLocation, fn);

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Import data from CSV / text files
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private void ImportFile(string filePath, string fileName)
        {
            divMessage.Visible = false;
            DataSet ds = new DataSet();
            try
            {
                // Create a connection string DBQ attribute sets the path of directory which contains CSV or text files 
                string strConnString = @"Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Server.MapPath("../UploadFiles") + ";";
                string sql_select;
                System.Data.Odbc.OdbcConnection conn;
                //Create connection to CSV/text file
                conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());
                //Open the connection 
                conn.Open();
                //Fetch records from CSV/text
                sql_select = "select * from [" + fileName + "]";
                //Initialise DataAdapter with file data
                DataAdapter obj_oledb_da = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);
                //Fill dataset with the records from CSV file
                obj_oledb_da.Fill(ds);
                //Declare variable for Error
                string strError = "";
                //Close Connection to CSV file
                conn.Close();
                // Validate Dataset is empty or not
                int tagsRemoved = 0;
                int tagsadded = 0;
                int totaltags = 0;

                if (ds != null)
                {
                    bool IsCloExists = ColumnExists(ds);
                    if (IsCloExists == false)
                    {
                        divMessage.Visible = true;
                        litMsg.Text = "Please enter a file with TagID";
                        return;
                    }

                    if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                    {
                        totaltags = ds.Tables[0].Rows.Count;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            // Remove the tagid if its length is less than 9
                            //if (Convert.ToString(dr["TagID"]).Length < 9)
                            //{
                            //    dr.Delete();
                            //    tagsRemoved++;
                            //}

                            if ((dr["TagID"] != DBNull.Value))
                            {
                                int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();

                                if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1))
                                {
                                    dr.Delete();
                                    tagsRemoved++;
                                }
                            }
                        }
                        ds.Tables[0].AcceptChanges();
                    }
                }
                // Total tags added from the CSV
                tagsadded = totaltags - tagsRemoved;
                //ViewState["tagsadded"] = tagsadded;
                // Assign dataset value to tag list
                if (ds != null)
                {
                    if (lstAddRemoveTag.Items.Count == 0)
                    {
                        if (ds != null)
                        {
                            lstAddRemoveTag.DataSource = ds;
                            lstAddRemoveTag.DataBind();
                            DisableAllButtons();
                        }
                    }
                    else
                    {
                        if (ds.Tables != null && ds.Tables[0].Rows.Count != 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if ((dr["TagID"] != DBNull.Value))
                                {
                                    int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();
                                    if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1))
                                    {
                                        dr.Delete();
                                        tagsRemoved++;
                                    }
                                }

                                lstAddRemoveTag.Items.Add(Convert.ToString(dr["TagID"]));
                            }
                        }
                    }
                }
                int dupTags = RemoveDuplicateTagsFromList();
                if (totaltags != 0)
                {
                    EnableAllButtons();
                    //divMessage.Visible = true;
                    //litMsg.Text = "Tags imported successfully from CSV/text file <br/> Total Tags: " + totaltags + "<br/> Tags added: " + tagsadded + "<br/> Tags removed due to inconsistency: " + tagsRemoved;
                    if (ViewState["tagsadded"] != null)
                    {
                        ViewState["tagsadded"] = Convert.ToInt32(ViewState["tagsadded"]) + tagsadded - dupTags;
                    }
                    else
                    {
                        ViewState["tagsadded"] = tagsadded - dupTags;
                    }
                    litTotalUpTags.Text = Convert.ToString(ViewState["tagsadded"]);
                }
                else
                {
                    divMessage.Visible = true;
                    //litMsg.Text = "No tags exists in CSV/text file <br/> Total Tags: " + totaltags + "<br/> Tags added: " + tagsadded + "<br/> Tags removed due to inconsistency: " + tagsRemoved;
                    litMsg.Text = "No tag(s) exists in CSV file";

                }
            }
            catch (Exception) //Error
            {
                throw;
            }
        }

        /// <summary>
        /// Add all tags to database table Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            try
            {
                divMessage.Visible = false;
                if (lstAddRemoveTag.Items.Count != 0)
                {
                    Tags objTags = new Tags();

                    // List all the tags in CSV form 
                    ArrayList lstTagCsv = new ArrayList();
                    StringBuilder strLstTagIDs = new StringBuilder("-1");

                    foreach (ListItem tagitem in lstAddRemoveTag.Items)//lstbox is ID of ListBox
                    {
                        //To set all tag ids in a single CSV string
                        if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                        {
                            lstTagCsv.Add(strLstTagIDs);
                            strLstTagIDs = new StringBuilder("-1");
                        }
                        else
                        {
                            strLstTagIDs.Append("," + tagitem.Value);
                        }
                    }

                    // Insert bulk Tagids by CSV string

                    if (strLstTagIDs.Length >= 9)
                    {
                        lstTagCsv.Add(strLstTagIDs);
                    }

                    // Insert bulk nuber of tags using CSV string
                    int countaddedrows = 0;
                    foreach (StringBuilder strCsv in lstTagCsv)
                    {
                        countaddedrows = countaddedrows + objTags.InsertTags(Convert.ToString(strCsv));
                    }
                    // Show number of inserted and rejected tags in the list
                    ////int tagsAlreadyExists = 0;
                    ////int totalTagsAdded = 0;
                    //////if ((ViewState["tagsadded"] != null) && (ViewState["tagsRemovebyRemovebtn"] != null))
                    //////{
                    //////totalTagsAdded = Convert.ToInt32(ViewState["tagsadded"]) - Convert.ToInt32(ViewState["tagsRemovebyRemovebtn"]);
                    ////totalTagsAdded = lstAddRemoveTag.Items.Count;
                    //////}
                    ////tagsAlreadyExists = totalTagsAdded - countaddedrows;
                    ////if (tagsAlreadyExists <= 0)
                    ////{
                    ////    litMsg.Text = countaddedrows + " Tags added successfully <br/>";
                    ////}
                    ////else if (tagsAlreadyExists > 0)
                    ////{
                    ////    litMsg.Text = countaddedrows + " Tags added successfully <br/>" + tagsAlreadyExists + " Tags already exists";
                    ////}

                    /// Message to show number of Tags added successfully
                    divMessage.Visible = true;
                    if (countaddedrows > 0)
                    {
                        litMsg.Text = countaddedrows + " Tag(s) added successfully ";
                    }
                    else
                    {
                        litMsg.Text = "No tag(s) added ";
                    }

                    // Clear list after successful insertion of records
                    lstAddRemoveTag.Items.Clear();
                    ViewState["tagsadded"] = null;
                    ViewState["tagsFromReader"] = null;
                    litTotalUpTags.Text = "0";
                    litTotReadTags.Text = "0";
                }
                else
                {
                    divMessage.Visible = true;
                    litMsg.Text = "Tag(s) List is empty, Please select appropriate tag(s) file or Reader to add tag(s) in tag(s) list";
                    DisableAllButtons();
                }
            }
            catch (Exception) //Error
            {
                throw;
            }
        }

        /// <summary>
        /// Clear all the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ResetControls();
        }

        /// <summary>
        /// Read Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReadTags_Click(object sender, EventArgs e)
        {
            try
            {
                EnableAllButtons();
                divMessage.Visible = false;
                // Get Reader details by ID to get reader information
                IReaders objReaders = Readers.GetReaderDetailsByID(Convert.ToInt32(ddlReader.SelectedValue));
                Reader objReader = new Reader(objReaders.IPAddress, objReaders.PortNo);

                // Scan all tags from reader in list
               // List<RFIDTag> lstRFIDTag = objReader.ScanForTags(2);
                List<RFIDTag> lstRFIDTag = objReader.GetTags();
                //objReader.ClearTagList();
                objReader.Dispose();
                ///List<RFIDTag> lstRFIDTag = objReader.GetTags();
                int tagsFromReader = 0;
                int totalTagsFromReader = 0;
                int tagsRemoveFromReader = 0;
                totalTagsFromReader = lstRFIDTag.Count;
                if (lstRFIDTag != null)
                {
                    // Check whether the items are in list box or not
                    foreach (RFIDTag objRFIDTag in lstRFIDTag)
                    {
                        if (objRFIDTag.TagID.Length >= 9)
                        {
                            lstAddRemoveTag.Items.Add(Convert.ToString(objRFIDTag.TagID));
                            tagsFromReader++;
                        }
                    }
                }
                int dupTags = RemoveDuplicateTagsFromList();
                tagsRemoveFromReader = totalTagsFromReader - tagsFromReader;

                if (totalTagsFromReader == 0)
                {
                    //divMessage.Visible = true;
                    //litMsg.Text = "No tags exists in Reader <br/> Total Tags: " + totalTagsFromReader + "<br/> Tags added: " + tagsFromReader + "<br/> Tags removed due to inconsistency: " + tagsRemoveFromReader;
                    DisableAllButtons();
                }
                if (ViewState["tagsFromReader"] != null)
                {
                    ViewState["tagsFromReader"] = Convert.ToInt32(ViewState["tagsFromReader"]) + tagsFromReader - dupTags;
                }
                else
                {
                    ViewState["tagsFromReader"] = tagsFromReader - dupTags;
                }
                litTotReadTags.Text = Convert.ToString(ViewState["tagsFromReader"]);
                ddlReader.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                litExMsg.Text = ex.Message;
            }
        }

        /// <summary>
        /// Disable all buttons
        /// </summary>
        private void DisableAllButtons()
        {
            btnAddAll.Enabled = false;
            btnRemove.Enabled = false;
        }

        /// <summary>
        /// Enable all buttons
        /// </summary>
        private void EnableAllButtons()
        {
            btnAddAll.Enabled = true;
            btnRemove.Enabled = true;
        }
        /// <summary>
        /// OK after Success Messages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOK_Click(object sender, EventArgs e)
        {
            ResetControls();
        }
        private void ResetControls()
        {
            divMessage.Visible = false;
            lstAddRemoveTag.Items.Clear();
            litMsg.Text = string.Empty;
            ddlReader.SelectedIndex = 0;
            litTotalUpTags.Text = "0";
            litTotReadTags.Text = "0";
            ViewState["tagsadded"] = null;
            ViewState["tagsRemovebyRemovebtn"] = null;
            ViewState["tagsFromReader"] = null;
            divRemoveTagMsg.Visible = false;
            litRmvMsg.Text = string.Empty;
        }

        #endregion Add Tags

        #region Remove Tags
        /// <summary>
        /// Remove selected tags from the tags list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            litMsg.Text = string.Empty;
            divRemoveTagMsg.Visible = true;
            litRmvMsg.Text = "Do you want to remove the tag(s) from list?";
        }

        /// <summary>
        /// Remove Tags 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRmvOK_Click(object sender, EventArgs e)
        {
            try
            {
                divMessage.Visible = false;
                if (lstAddRemoveTag.Items.Count != 0)
                {
                    EnableAllButtons();
                    // Remove slected tags from the list
                    int tagsRemovebyRemovebtn = 0;
                    bool tagRemaoved = false;
                    for (int i = 0; i < lstAddRemoveTag.Items.Count; i++)
                    {
                        if (lstAddRemoveTag.Items[i].Selected)
                        {
                            lstAddRemoveTag.Items.RemoveAt(i);
                            i = i - 1;
                            tagRemaoved = true;
                            tagsRemovebyRemovebtn++;
                        }
                    }
                    ViewState["tagsRemovebyRemovebtn"] = tagsRemovebyRemovebtn;

                    if (tagRemaoved == true)
                    {
                        //litMsg.Text = tagsRemovebyRemovebtn + " selected tags get removed";
                        //btnOK.Visible = false;
                        divRemoveTagMsg.Visible = false;
                    }
                    else
                    {
                        divMessage.Visible = true;
                        litMsg.Text = "Please select the tag(s) to remove";
                        btnOK.Visible = false;
                        divRemoveTagMsg.Visible = false;
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    litMsg.Text = "Tags List is empty, Please select appropriate Tags file or Reader to add tags in Tags list";
                    DisableAllButtons();

                }
                if (lstAddRemoveTag.Items.Count == 0)
                {
                    ResetControls();
                    divMessage.Visible = true;
                    litMsg.Text = "Tags List is empty";
                    DisableAllButtons();
                }
                else
                {
                    EnableAllButtons();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cancel remove Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRmvCancel_Click(object sender, EventArgs e)
        {
            litRmvMsg.Text = string.Empty;
            divRemoveTagMsg.Visible = false;
            lstAddRemoveTag.SelectedIndex = -1;
        }
        #endregion Remove Tags

        /// <summary>
        /// Column exists
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="ColumnName"></param>
        /// <returns></returns>
        private bool ColumnExists(DataSet ds)
        {
            bool colExists1 = false;
            bool colExists2 = false;
            bool colExists = false;
            foreach (DataColumn col in ds.Tables[0].Columns)
            {
                if (col.ColumnName == "TagID")
                {
                    colExists1 = true;
                }
            }
            if ((colExists1 == true))
            {
                colExists = true;
            }

            return colExists;
        }

        /// <summary>
        /// Remove duplicate records from tag list
        /// </summary>
        private int RemoveDuplicateTagsFromList()
        {
            try
            {
                int rmvTags = 0;
                bool flgExists = false;
                ArrayList lstTags = new ArrayList();
                foreach (ListItem li in lstAddRemoveTag.Items)
                {
                    if (!lstTags.Contains(Convert.ToString(li)))
                    {
                        lstTags.Add(Convert.ToString(li));
                    }
                    else
                    {
                        rmvTags++;
                        flgExists = true;
                    }
                }

                lstAddRemoveTag.Items.Clear();
                foreach (string tag in lstTags)
                {
                    lstAddRemoveTag.Items.Add(tag);
                }
                if (flgExists == true)
                {
                    divMessage.Visible = true;
                    litMsg.Text = "Some tag(s) already exists in tag(s) list";
                    btnOK.Visible = false;
                }
                return rmvTags;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
