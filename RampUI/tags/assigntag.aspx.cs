using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.IO;
using System.Data.Common;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace Ramp.UI.tags
{
    public partial class assigntag : System.Web.UI.Page
    {
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            divConfirmMsg.Visible = false;

            if (!IsPostBack)
            {
                ClearAllSessions();
                FillHoppers();
                FillProponents();
                FinalTagsList();
                DisableControls();
            }
        }

        #region Tags Manual Addition by Textbox Control

        /// <summary>
        /// Populate Hoppers/Pad dropdown list
        /// </summary>
        private void FillHoppers()
        {
            try
            {
                ClearAllSessions();
                DataTable dtHoppers = Hoppers.GetAllHoppers();
                if (dtHoppers != null)
                {
                    if (dtHoppers.Rows.Count != 0)
                    {
                        ddlPad.DataSource = dtHoppers;
                        ddlPad.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Fill proponents by hopper id
        /// </summary>
        private void FillProponents()
        {
            try
            {
                ClearAllSessions();
                // Dataset get Propponents 1. Proponent of selected hopper 2. All proponent
                DataSet dsProponents = Hoppers.GetProponentsByHopperID(Convert.ToInt32(ddlPad.SelectedValue));
                Session["Proponent"] = dsProponents;
                if (dsProponents != null)
                {
                    if (Convert.ToInt32(ddlPad.SelectedValue) != 0)
                    {
                        if (dsProponents.Tables[0].Rows.Count != 0)
                        {
                            if (dsProponents.Tables[1].Rows.Count != 0)
                            {
                                ddlProponent.DataSource = dsProponents.Tables[1];
                                ddlProponent.DataBind();
                                foreach (DataRow dr in dsProponents.Tables[0].Rows)
                                {
                                    if (Convert.ToInt32(dr["ProponentID"]) != 0)
                                    {
                                        ddlProponent.SelectedValue = Convert.ToString(dr["ProponentID"]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dsProponents.Tables[1].Rows.Count != 0)
                            {
                                ddlProponent.DataSource = dsProponents.Tables[1];
                                ddlProponent.DataBind();
                            }
                        }
                    }
                    else
                    {
                        if (dsProponents.Tables[1].Rows.Count != 0)
                        {
                            ddlProponent.DataSource = dsProponents.Tables[1];
                            ddlProponent.SelectedValue = "0";
                            ddlProponent.DataBind();
                        }
                    }
                }
                if ((Convert.ToInt32(ddlPad.SelectedValue) != 0) && (Convert.ToInt32(ddlProponent.SelectedValue) != 0))
                {
                    EnableControls();
                }
                else
                {
                    DisableControls();
                    ClearAllSessions();
                    FinalTagsList();
                    //litMessage.Text = "Please select the appropriate PAD ID and Proponent ID";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Selected index changed event of Pad Drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DisableAllDivMessage();
                divAddControls.Disabled = false;
                if ((Convert.ToInt32(ddlPad.SelectedValue) != 0))
                {
                    FillProponents();
                }
                else
                {
                    // Disable all add controls
                    //DisableControls();
                    divAddControls.Disabled = true;
                    DisableControls();
                    ddlProponent.SelectedValue = "0";
                    divMessage.Visible = true;
                    litMessage.Text = "Please select the appropriate PAD ID";
                    ClearAllSessions();
                    FinalTagsList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Selected index changed event of Proponent Drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlProponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divAddControls.Disabled = false;
                btnAssign.Enabled = true;
                DisableAllDivMessage();
                if ((Convert.ToInt32(ddlProponent.SelectedValue) != 0) && (Convert.ToInt32(ddlPad.SelectedValue) != 0))
                {
                    EnableControls();
                    DataTable dtProponentHoppers = Hoppers.CheckProponentIsAlreadyAssignedToOtherHopper(Convert.ToInt32(ddlProponent.SelectedValue), Convert.ToInt32(ddlPad.SelectedValue));
                    foreach (DataRow dr in dtProponentHoppers.Rows)
                    {
                        if (Convert.ToInt32(dr["ID"]) == 0)
                        {
                            // Not associated with any hopper
                            divValidateProponentYesNo.Visible = false;
                            litValidateProponent.Text = string.Empty;
                            litMessage.Text = string.Empty;
                            divMessage.Visible = false;
                            litMessage.Text = "Not associated with any PAD";
                            ClearAllSessions();
                        }
                        else if (Convert.ToInt32(dr["ID"]) == -1)
                        {
                            // Already assigned to the selected hopper and not associated with other hopper
                            divValidateProponentYesNo.Visible = false;
                            litValidateProponent.Text = string.Empty;
                            litMessage.Text = "Already assigned to the selected PAD and not associated with other PAD";
                            ClearAllSessions();
                        }
                        else
                        {
                            // Already assigned to the other hopper also
                            DisableControls();
                            divValidateProponentYesNo.Visible = true;
                            divMessage.Visible = false;
                            litMessage.Text = string.Empty;
                            litValidateProponent.Text = "A different proponent is already assigned to " + Convert.ToString(ddlPad.SelectedItem.Text) + " . Do you want to re-assign this PAD?";
                            Session["AlreadyAssignedTagCount"] = null;
                            Session["dsAllAssignedTagsByProponentID"] = null;
                            Session["dtManuallyAddedTags"] = null;
                            Session["dsAlreadyAssignedTagsList"] = null;
                            Session["dsUploadedTags"] = null;
                            Session["dsFinalTagList"] = null;
                            //Session["Proponent"] = null;
                            ViewState["tagsadded"] = null;
                            ViewState["dtCSVAddedTags"] = null;
                        }
                    }
                }
                else
                {
                    DisableControls();
                    divAddControls.Disabled = true;
                    //btnAssign.Enabled = false;
                    litMessage.Text = "Please select the appropriate PAD ID AND Proponent ID";
                    ClearAllSessions();
                    FinalTagsList();
                }
                // ClearAllSessions();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Allow reassign to proponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                DisableAllDivMessage();
                ddlPad.SelectedValue = Convert.ToString(ddlPad.SelectedValue);
                ddlProponent.SelectedValue = Convert.ToString(ddlProponent.SelectedValue);
                //divAllcontrols.Disabled = false;
                EnableControls();
                litValidateProponent.Text = "";
                divValidateProponentYesNo.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Not allow reassign to hopper
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNo_Click(object sender, EventArgs e)
        {
            try
            {
                DisableAllDivMessage();
                if (Session["Proponent"] != null)
                {
                    DataSet dsProponents = (DataSet)Session["Proponent"];
                    if (dsProponents.Tables[0].Rows.Count != 0)
                    {
                        if (dsProponents.Tables[1].Rows.Count != 0)
                        {
                            foreach (DataRow dr in dsProponents.Tables[0].Rows)
                            {
                                if (Convert.ToInt32(dr["ID"]) != 0)
                                {
                                    ddlProponent.SelectedValue = Convert.ToString(dr["ProponentID"]);
                                }
                            }
                        }
                    }
                }
                //divAllcontrols.Disabled = false;
                EnableControls();
                litValidateProponent.Text = "";
                divValidateProponentYesNo.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Add individual tagid to Tag List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddTag_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Convert.ToInt32(ddlPad.SelectedValue) != 0) && (Convert.ToInt32(ddlProponent.SelectedValue) != 0))
                {
                    litMessage.Text = string.Empty;
                    DisableAllDivMessage();
                    divAllcontrols.Disabled = false;
                    if ((txtTagID.Text != string.Empty) && (txtTruckID.Text != string.Empty))
                    {
                        Tags objTag = new Tags();
                        // Check if tag is already assigned 
                        DataTable dtalreadyAssignedTagCount = objTag.CheckTagAlreadyAssigned("'" + Convert.ToString(txtTagID.Text) + "'", Convert.ToInt32(ddlProponent.SelectedValue));
                        Session["AlreadyAssignedTagCount"] = dtalreadyAssignedTagCount;
                        if (dtalreadyAssignedTagCount != null)
                        {
                            foreach (DataRow dr in dtalreadyAssignedTagCount.Rows)
                            {
                                //Give dataset with two DataTables // Get all assigned tags by // 1. Different Proponent ID // 2. Selected Proponent ID
                                if ((Convert.ToInt32(dr["CountDifferent"]) != 0) || (Convert.ToInt32(dr["CountSame"]) != 0))
                                {
                                    DataSet dsAllAssignedTagsByProponentID = Tags.GetAllAssignedTagsByProponentID("'" + txtTagID.Text.Trim() + "'", Convert.ToInt32(ddlProponent.SelectedValue));
                                    Session["dsAllAssignedTagsByProponentID"] = dsAllAssignedTagsByProponentID;
                                }
                                // Check if tag is already assigned to different proponent and return the number of counts as CountDifferent in datatable
                                if (Convert.ToInt32(dr["CountDifferent"]) != 0)
                                {
                                    DisableAllDivMessage();
                                    //divAllcontrols.Disabled = true;
                                    divTagReassignmentDiffProp.Visible = true;
                                    litTagReassignmentDiffProp.Text = Convert.ToInt32(dr["CountDifferent"]) + " Tag(s) already assigned to different proponent. Do you want to re-assign them?";
                                    // Get All the tags assigned to different proponent
                                }
                                // Check if tag is already assigned to the selected proponent and return the number of counts as CountSame in datatable
                                else if (Convert.ToInt32(dr["CountSame"]) != 0)
                                {
                                    DisableAllDivMessage();
                                    divAllcontrols.Disabled = true;
                                    divTagReassignmentSelctedProp.Visible = true;
                                    litTagReassignmentSelctedProp.Text = Convert.ToInt32(dr["CountSame"]) + " Tag(s) already assigned to selected proponent. Do you want to re-assign them?";
                                }
                                else
                                {
                                    DisableAllDivMessage();
                                    divAllcontrols.Disabled = false;
                                    AddTagsManuallyToTagList();
                                    litMessage.Text = "TagID added successfully to the tag list";
                                    // Allow adding tags (As tag is not already assigned to the different tags/Selected tags/ unasssigned tags)
                                }
                            }
                        }
                        //    txtTagID.Text = "";
                        //    txtTruckID.Text = "";
                    }
                    else
                    {
                        divAllcontrols.Disabled = false;
                        DisableAllDivMessage();
                        litMessage.Visible = true;
                        litMessage.Text = "Please enter appropriate Tag ID and Truck ID to add in tag list";
                    }
                }
                else
                {
                    //divAllcontrols.Disabled = false;
                    DisableControls();
                    DisableAllDivMessage();
                    litMessage.Visible = true;
                    litMessage.Text = "Please select appropriate PAD ID and Proponent ID";
                    ClearAllSessions();
                    FinalTagsList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Allow Reassignment of tags to different Proponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYesTagReassignmentDiffProp_Click(object sender, EventArgs e)
        {
            try
            {
                // Allow Adding 
                AddTagsManuallyToTagList();
                //divTagReassignmentDiffProp.Visible = false;
                //divAllcontrols.Disabled = false;
                EnableControls();
                DisableAllDivMessage();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Not allow reassignment of tag to selected proponent because already assigned to another proponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNoTagReassignmentDiffProp_Click(object sender, EventArgs e)
        {
            try
            {
                // Remove all the tags from the list which are already assigned to different Proponent
                if (Session["dsAllAssignedTagsByProponentID"] != null)
                {
                    DataSet dsAllAssignedTagsByProponentID = (DataSet)Session["dsAllAssignedTagsByProponentID"];
                    if (dsAllAssignedTagsByProponentID != null)
                    {
                        if (dsAllAssignedTagsByProponentID.Tables[0] != null)
                        {
                            if (dsAllAssignedTagsByProponentID.Tables[0].Rows.Count != 0)
                            {
                                foreach (DataRow dr in dsAllAssignedTagsByProponentID.Tables[0].Rows)
                                {
                                    if ((dr["TagRFID"]) != null)
                                    {
                                        if ((txtTagID.Text.Trim() == Convert.ToString(dr["TagRFID"])))
                                        {
                                            litMessage.Text = "This TagID is already assigned to another Proponent, please add another TagID";
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //divTagReassignmentDiffProp.Visible = false;
                //divAllcontrols.Disabled = false;
                EnableControls();
                DisableAllDivMessage();
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// System will skip all the truck IDs that are already assigned to selected proponent and continue with other truck IDs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSkip_Click(object sender, EventArgs e)
        {
            try
            {
                // Remove all the tags from the list which are already assigned to selected Proponent
                if (Session["dsAllAssignedTagsByProponentID"] != null)
                {
                    DataSet dsAllAssignedTagsByProponentID = (DataSet)Session["dsAllAssignedTagsByProponentID"];
                    if (dsAllAssignedTagsByProponentID != null)
                    {
                        if (dsAllAssignedTagsByProponentID.Tables[1] != null)
                        {
                            if (dsAllAssignedTagsByProponentID.Tables[1].Rows.Count != 0)
                            {
                                foreach (DataRow dr in dsAllAssignedTagsByProponentID.Tables[1].Rows)
                                {
                                    if ((dr["TagRFID"]) != null)
                                    {
                                        if ((txtTagID.Text.Trim() == Convert.ToString(dr["TagRFID"])))
                                        {
                                            litMessage.Text = "This TagID is already assigned to selected Proponent, please add another TagID";
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //divTagReassignmentSelctedProp.Visible = false;
                //divAllcontrols.Disabled = false;
                EnableControls();
                DisableAllDivMessage();
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// System will overwrites already assigned tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                AddTagsManuallyToTagList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// System will cancel uploading operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtTruckID.Text = string.Empty;
                txtTagID.Text = string.Empty;
                litMessage.Text = string.Empty;
                divTagReassignmentSelctedProp.Visible = false;
                divValidateProponentYesNo.Visible = false;
                divTagReassignmentSelctedProp.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Add Tags manually (i.e. through textbox) to tag list
        /// </summary>
        private void AddTagsManuallyToTagList()
        {
            try
            {
                // Add functionality
                divAllcontrols.Disabled = false;
                DisableAllDivMessage();
                DataTable dtManuallyAddedTags = new DataTable("dtManuallyAddedTags");

                dtManuallyAddedTags.Columns.Add("TagID");
                dtManuallyAddedTags.Columns.Add("TruckID");

                // If data already exists in the datatable
                if (Session["dtManuallyAddedTags"] != null)
                {
                    dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                }

                if (dtManuallyAddedTags != null)
                {
                    if (dtManuallyAddedTags.Rows.Count != 0)
                    {
                        foreach (DataRow dr in dtManuallyAddedTags.Rows)
                        {
                            if ((dr["TagID"]) != null)
                            {
                                if ((txtTagID.Text.Trim() == Convert.ToString(dr["TagID"])))
                                {
                                    litMessage.Text = "This TagID is already added into tag list, please add another TagID";
                                    return;
                                }
                            }
                        }
                    }
                }

                // Add new row of data to datatable
                DataRow drManuallyAddedTags = dtManuallyAddedTags.NewRow();

                //The following code adds the data to the table.
                drManuallyAddedTags["TagID"] = Convert.ToString(txtTagID.Text);
                drManuallyAddedTags["TruckID"] = Convert.ToString(txtTruckID.Text);

                //Adding the created data row to the datatable
                dtManuallyAddedTags.Rows.Add(drManuallyAddedTags);
                dtManuallyAddedTags.AcceptChanges();

                //The following code binds “EmployeeDetail” table present in DataSet to Gridview
                Session["dtManuallyAddedTags"] = dtManuallyAddedTags;
                //gvAssignTags.DataSource = dtManuallyAddedTags;
                //gvAssignTags.DataBind();
                FinalTagsList();
                litMessage.Text = "TagID added successfully to the tag list";
                //divTagReassignmentSelctedProp.Visible = false;
                //divTagReassignmentDiffProp.Visible = false;
                divAllcontrols.Disabled = false;
                DisableAllDivMessage();
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Disable add controls if appropriate PAD ID and Proponent ID is not selected 
        /// </summary>
        private void DisableControls()
        {
            try
            {
                divTagReassignmentDiffProp.Visible = false;
                divTagReassignmentSelctedProp.Visible = false;
                divTagReassignmentSelctedProp.Visible = false;
                btnUpload.Enabled = false;
                btnAddTag.Enabled = false;
                btnRemove.Enabled = false;
                btnAssign.Enabled = false;
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;
                txtTagID.Enabled = false;
                txtTruckID.Enabled = false;
                btnAddTag.Enabled = false;
                upFileTags.Enabled = false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable add controls if appropriate PAD ID and Proponent ID is not selected 
        /// </summary>
        private void EnableControls()
        {
            try
            {
                txtTagID.Enabled = true;
                txtTruckID.Enabled = true;
                btnAddTag.Enabled = true;
                btnUpload.Enabled = true;
                btnAddTag.Enabled = true;
                btnRemove.Enabled = true;
                btnAssign.Enabled = true;
                upFileTags.Enabled = true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tags Manual Addition by Textbox Control

        #region Add Tags By Upload CSV


        void CreateDataset()
        {
            ds  = new DataSet();
            DataTable dt = new DataTable();

            DataColumn[] dcc = new DataColumn[] { new DataColumn("TagID",typeof(System.String)),
                                                  new DataColumn("TruckID",typeof(System.String))
            };

            dt.Columns.AddRange(dcc);
            ds.Tables.Add(dt);
        }

        /// <summary>
        /// Upload text/csv file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                // Upload CSV file in Upload file folder of the application
                if (upFileTags.PostedFile.FileName.Trim() != string.Empty)
                {
                    string fn = System.IO.Path.GetFileName(upFileTags.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("../UploadFiles") + "\\" + fn;
                    if (SaveLocation.Trim().Length > 0)
                    {
                        FileInfo fi = new FileInfo(SaveLocation);
                        if (fi.Exists)//if file exists then delete it
                        {
                            fi.Delete();
                        }
                    }
                    try
                    {
                        upFileTags.PostedFile.SaveAs(SaveLocation);
                        // Import tag ids from the CSV file and list them into list box
                        ImportFile(SaveLocation, fn);

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Import data from CSV / text files
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private void ImportFile(string filePath, string fileName)
        {
            try
            {
                //btnAddAll.Enabled = true;
                //btnRemove.Enabled = true;
                divAllcontrols.Disabled = false;
                DisableAllDivMessage();
               DataSet ds = new DataSet();
               // CreateDataset();
                try
                {
                    // Create a connection string DBQ attribute sets the path of directory which contains CSV or text files 
                    string strConnString = @"Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Server.MapPath("../UploadFiles") + ";";
                    string sql_select;
                    System.Data.Odbc.OdbcConnection conn;
                    //Create connection to CSV/text file
                    conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());
                    //Open the connection 
                    conn.Open();
                    //Fetch records from CSV/text
                    //sql_select = "select TagID,TruckID from [" + fileName + "]";
                    sql_select = "select distinct * from [" + fileName + "]";
                    //Initialise DataAdapter with file data
                    DataAdapter obj_oledb_da = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);
                    //Fill dataset with the records from CSV file

                   // obj_oledb_da.MissingMappingAction = MissingMappingAction.Ignore;
                  //  obj_oledb_da.MissingSchemaAction = MissingSchemaAction.Ignore;

                    obj_oledb_da.Fill(ds);

                   
                    //Declare variable for Error
                    string strError = "";
                    //Close Connection to CSV file
                    conn.Close();
                    // Validate Dataset is empty or not
                    int tagsRemoved = 0;
                    int tagsadded = 0;
                    int totaltags = 0;

                    if (ds != null)
                    {
                        //bool IsCloExists = ColumnExists(ds);

                        bool IsCloExists = IsValidCSVstructure(ds); 

                        if (IsCloExists == false)
                        {
                            divMessage.Visible = true;
                            litMessage.Text = "Invalid file! Please enter a file with TagID and TruckID fields only";
                            return;
                        }

                        // "^[0-9a-zA-Z]+$"
                    }
                    if (ds != null)
                    {
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            totaltags = ds.Tables[0].Rows.Count;                            

                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                // Remove the tagid if its length is less than 9                               

                                if ((dr["TagID"] != DBNull.Value) && (dr["TruckID"] != DBNull.Value))
                                {
                                    if (IsValidData(dr["TagID"].ToString()) && IsValidData(dr["TruckID"].ToString()))
                                    {
                                        int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();
                                        int TruckIDCount = 0;
                                        if ((dr["TruckID"] != DBNull.Value))
                                        {
                                            TruckIDCount = (ds.Tables[0].Select("TruckID=" + "'" + Convert.ToString(dr["TruckID"]) + "'")).Count();
                                        }
                                        if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1) || (TruckIDCount > 1))
                                        {
                                            dr.Delete();
                                            tagsRemoved++;
                                        }
                                    }
                                    else
                                    {
                                        dr.Delete();
                                        tagsRemoved++;
                                    }
                                }
                                else
                                {
                                    dr.Delete();
                                    tagsRemoved++;
                                }
                            }
                            ds.Tables[0].AcceptChanges();
                        }
                    }
                    // Total tags added from the CSV
                    tagsadded = totaltags - tagsRemoved;
                    ViewState["tagsadded"] = tagsadded;
                    // Assign dataset value to tag list
                    if (Session["dsUploadedTags"] != null)
                    {
                        DataTable dtSessionUp = new DataTable();
                        dtSessionUp = (DataTable)Session["dsUploadedTags"];
                        dtSessionUp.Merge(ds.Tables[0], true, MissingSchemaAction.Ignore);
                        Session["dsUploadedTags"] = dtSessionUp;
                    }
                    else
                    {
                        Session["dsUploadedTags"] = ds.Tables[0];
                    }
                    if (ds != null)
                    {
                        if ((Convert.ToInt32(ddlPad.SelectedValue) != 0) && (Convert.ToInt32(ddlProponent.SelectedValue) != 0))
                        {
                            litMessage.Text = string.Empty;
                            divTagReassignmentDiffProp.Visible = false;
                            //if ((txtTagID.Text != string.Empty) && (txtTruckID.Text != string.Empty))
                            //{
                            Tags objTag = new Tags();
                            // Check if tag is already assigned 


                            // Create tags CSV string
                            ArrayList lstTagCsv = new ArrayList();
                            StringBuilder strLstTagIDs = new StringBuilder("'-1'");

                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if ((dr["TagID"] != DBNull.Value) && (dr["TruckID"] != DBNull.Value))
                                {
                                    if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                                    {
                                        lstTagCsv.Add(strLstTagIDs);
                                        strLstTagIDs = new StringBuilder("'-1'");
                                    }
                                    else
                                    {
                                        strLstTagIDs.Append("," + "'" + Convert.ToString(dr["TagID"]) + "'");
                                    }
                                }
                            }
                            // Insert bulk Tagids by CSV string

                            if (strLstTagIDs.Length >= 9)
                            {
                                lstTagCsv.Add(strLstTagIDs);
                            }

                            // Insert bulk nuber of tags using CSV string
                            DataSet dsAlreadyAssignedTags = new DataSet();
                            ArrayList lstDataSets = new ArrayList();
                            int countaddedrows = 0;

                            DataSet dsAlreadyAssignedTagsList = null;

                            if (lstTagCsv.Count > 0)
                            {
                                foreach (StringBuilder strCsv in lstTagCsv)
                                {
                                    dsAlreadyAssignedTags = Tags.GetAllAssignedTagsByProponentID(Convert.ToString(strCsv), Convert.ToInt32(ddlProponent.SelectedValue));
                                    lstDataSets.Add(dsAlreadyAssignedTags);
                                }

                                if (lstDataSets.Count > 0)
                                    dsAlreadyAssignedTagsList = new DataSet();

                                foreach (DataSet dsAssignedTags in lstDataSets)
                                {
                                    dsAlreadyAssignedTagsList.Merge(dsAssignedTags);
                                }
                                Session["dsAlreadyAssignedTagsList"] = dsAlreadyAssignedTagsList;
                            }

                            if (dsAlreadyAssignedTagsList != null && dsAlreadyAssignedTagsList.Tables[2] != null)
                            {
                                foreach (DataRow dr in dsAlreadyAssignedTagsList.Tables[2].Rows)
                                {
                                    //Give dataset with two DataTables // Get all assigned tags by // 1. Different Proponent ID // 2. Selected Proponent ID // 3. Number of count for different and selected proponent

                                    // Check if tag is already assigned to different proponent and return the number of counts as CountDifferent in datatable
                                    if (Convert.ToInt32(dr["CountDifferent"]) != 0)
                                    {
                                        //divAllcontrols.Disabled = false;
                                        DisableControls();
                                        DisableAllDivMessage();
                                        divUploadTagsDiiffProp.Visible = true;
                                        litUploadTagsDiiffProp.Text = Convert.ToInt32(dr["CountDifferent"]) + " Tag(s) already assigned to different proponent. Do you want to re-assign them?";
                                        // Get All the tags assigned to different proponent
                                    }
                                    // Check if tag is already assigned to the selected proponent and return the number of counts as CountSame in datatable
                                    else if (Convert.ToInt32(dr["CountSame"]) != 0)
                                    {
                                        //divAllcontrols.Disabled = false;
                                        DisableControls();
                                        DisableAllDivMessage();
                                        divUploadTagsSelectedProp.Visible = true;
                                        litUploadTagsSelectedProp.Text = Convert.ToInt32(dr["CountSame"]) + " Tag(s) already assigned to selected proponent. Do you want to re-assign them?";
                                    }
                                    else
                                    {
                                        //divAllcontrols.Disabled = false;
                                        EnableControls();
                                        DisableAllDivMessage();
                                        AddTagToTagListByUploadingCSV();
                                        litMessage.Text = "TagID added successfully to the tag list";
                                        // Allow adding tags (As tag is not already assigned to the different tags/Selected tags/ unasssigned tags)
                                    }
                                }
                            }
                            else
                            {
                                DisableAllDivMessage();
                                litMessage.Text = "No valid entry found in the list.";
                                divMessage.Visible = true;
                            }
                        }
                        else
                        {
                            divAllcontrols.Disabled = false;
                            DisableAllDivMessage();
                            litMessage.Visible = true;
                            litMessage.Text = "Please select appropriate PAD ID and Proponent ID";
                            ClearAllSessions();
                            FinalTagsList();
                        }
                    }
                }
                catch (Exception ex) //Error
                {
                    throw ex;
                }
            }
            catch (Exception ex) //Error
            {
                throw ex;
            }

        }

        bool IsValidData(string value)
        {
            bool result = false;
            try
            {
               result = Regex.IsMatch(value, "^[0-9a-zA-Z]+$");
            }
            catch { }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYesUploadTagsDiiffProp_Click(object sender, EventArgs e)
        {
            try
            {
                divTagReassignmentDiffProp.Visible = false;
                DataSet dsAlreadyAssignedTagsList = (DataSet)Session["dsAlreadyAssignedTagsList"];

                if (dsAlreadyAssignedTagsList != null)
                {
                    foreach (DataRow dr in dsAlreadyAssignedTagsList.Tables[2].Rows)
                    {
                        // Check if tag is already assigned to the selected proponent and return the number of counts as CountSame in datatable
                        if (Convert.ToInt32(dr["CountSame"]) != 0)
                        {
                            //divAllcontrols.Disabled = false;
                            EnableControls();
                            DisableAllDivMessage();
                            divUploadTagsSelectedProp.Visible = true;
                            litUploadTagsSelectedProp.Text = Convert.ToInt32(dr["CountSame"]) + " Tag(s) already assigned to selected proponent. Do you want to re-assign them?";
                        }
                        else
                        {
                            EnableControls();
                            //divAllcontrols.Disabled = false;
                            DisableAllDivMessage();
                            AddTagToTagListByUploadingCSV();
                            divTagReassignmentDiffProp.Visible = false;
                        }
                    }
                }
                else
                {
                    AddTagToTagListByUploadingCSV();
                    //divTagReassignmentDiffProp.Visible = false;
                    divAllcontrols.Disabled = false;
                    DisableAllDivMessage();
                    EnableControls();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNoUploadTagsDiiffProp_Click(object sender, EventArgs e)
        {
            try
            {
                // Remove all the tags from the list which are already assigned to different Proponent
                DataTable dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                if (dsUploadedTags != null)
                {
                    if (Session["dsAlreadyAssignedTagsList"] != null)
                    {
                        DataSet dsAlreadyAssignedTagsList = (DataSet)Session["dsAlreadyAssignedTagsList"];
                        if (dsAlreadyAssignedTagsList != null)
                        {
                            if (dsAlreadyAssignedTagsList.Tables[0] != null)
                            {
                                if (dsAlreadyAssignedTagsList.Tables[0].Rows.Count != 0)
                                {
                                    foreach (DataRow dr in dsAlreadyAssignedTagsList.Tables[0].Rows)
                                    {
                                        if ((dr["TagRFID"]) != DBNull.Value)
                                        {
                                            foreach (DataRow drUpTags in dsUploadedTags.Rows)
                                            {
                                                // Remove the tagid if its length is less than 9
                                                if ((drUpTags["TagID"] != DBNull.Value))
                                                {
                                                    //int TagIDcount = (dsUploadedTags.Select("TagID=" + "'" + Convert.ToString(dr["TagRFID"]) + "'")).Count();
                                                    //int TruckIDCount = 0;
                                                    //if ((dr["TruckID"] != DBNull.Value))
                                                    //{
                                                    //    TruckIDCount = (dsUploadedTags.Select("TruckID=" + "'" + Convert.ToInt32(dr["TruckID"]) + "'")).Count();
                                                    //}
                                                    //if ((Convert.ToString(drUpTags["TagID"]).Length < 9) || (TagIDcount > 1) || (TruckIDCount > 1))
                                                    //{
                                                    //    drUpTags.Delete();
                                                    //}
                                                    if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(drUpTags["TagID"]))
                                                    {
                                                        //  if (dr["TruckID"] != DBNull.Value)
                                                        //  {
                                                        //     TruckIDCount = (dsUploadedTags.Tables[0].Select("TruckID=" + "'" + Convert.ToInt32(dr["TruckID"]) + "'")).Count();
                                                        //  }
                                                        //if ((Convert.ToString(drUpTags["TagID"]).Length < 9) || (TagIDcount > 0) || (TruckIDCount > 0))
                                                        //{
                                                        drUpTags.Delete();
                                                        break;
                                                    }
                                                }
                                            }
                                            dsUploadedTags.AcceptChanges();
                                            Session["dsUploadedTags"] = dsUploadedTags;
                                        }
                                    }
                                }
                            }
                            if (dsUploadedTags.Rows.Count > 0)
                            {
                                if (dsAlreadyAssignedTagsList.Tables[1] != null)
                                {
                                    if (dsAlreadyAssignedTagsList.Tables[1].Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dsAlreadyAssignedTagsList.Tables[2].Rows)
                                        {
                                            // Check if tag is already assigned to the selected proponent and return the number of counts as CountSame in datatable
                                            if (Convert.ToInt32(dr["CountDifferent"]) != 0)
                                            {
                                                divAllcontrols.Disabled = false;
                                                DisableAllDivMessage();
                                                divUploadTagsSelectedProp.Visible = true;
                                                litUploadTagsSelectedProp.Text = Convert.ToInt32(dr["CountDifferent"]) + " Tag(s) already assigned to selected proponent. Do you want to re-assign them?";
                                            }
                                            else
                                            {
                                                AddTagToTagListByUploadingCSV();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AddTagToTagListByUploadingCSV();
                                    }
                                }
                            }
                        }
                        else
                        {
                            AddTagToTagListByUploadingCSV();
                        }
                    }
                    else
                    {
                        AddTagToTagListByUploadingCSV();
                    }
                }
                // divAllcontrols.Disabled = false;
                EnableControls();
                DisableAllDivMessage();
                //divTagReassignmentDiffProp.Visible = false;
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSkipUploadTagsSelectedProp_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                if (dsUploadedTags != null)
                {
                    if (Session["dsAlreadyAssignedTagsList"] != null)
                    {
                        DataSet dsAlreadyAssignedTagsList = (DataSet)Session["dsAlreadyAssignedTagsList"];
                        if (dsAlreadyAssignedTagsList != null)
                        {
                            if (dsAlreadyAssignedTagsList.Tables[1] != null)
                            {
                                if (dsAlreadyAssignedTagsList.Tables[1].Rows.Count != 0)
                                {
                                    foreach (DataRow dr in dsAlreadyAssignedTagsList.Tables[1].Rows)
                                    {
                                        if ((dr["TagRFID"]) != DBNull.Value)
                                        {
                                            foreach (DataRow drUpTags in dsUploadedTags.Rows)
                                            {
                                                // Remove the tagid if its length is less than 9
                                                if ((drUpTags["TagID"] != DBNull.Value))
                                                {
                                                    //int TagIDcount = (dsUploadedTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagRFID"]) + "'")).Count();
                                                    //int TruckIDCount = 0;
                                                    if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(drUpTags["TagID"]))
                                                    {
                                                        //  if (dr["TruckID"] != DBNull.Value)
                                                        //  {
                                                        //     TruckIDCount = (dsUploadedTags.Tables[0].Select("TruckID=" + "'" + Convert.ToInt32(dr["TruckID"]) + "'")).Count();
                                                        //  }
                                                        //if ((Convert.ToString(drUpTags["TagID"]).Length < 9) || (TagIDcount > 0) || (TruckIDCount > 0))
                                                        //{
                                                        drUpTags.Delete();
                                                        break;
                                                    }
                                                }
                                            }
                                            dsUploadedTags.AcceptChanges();
                                            Session["dsUploadedTags"] = dsUploadedTags;
                                        }
                                    }
                                }
                                else
                                {
                                    AddTagToTagListByUploadingCSV();
                                }
                            }
                            else
                            {
                                AddTagToTagListByUploadingCSV();
                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        litMessage.Text = "Please select the appropriate PAD ID";
                    }
                }
                AddTagToTagListByUploadingCSV();
                EnableControls();
                //divAllcontrols.Disabled = false;
                DisableAllDivMessage();
                //divTagReassignmentDiffProp.Visible = false;
                txtTagID.Text = string.Empty;
                txtTruckID.Text = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinueUploadTagsSelectedProp_Click(object sender, EventArgs e)
        {
            try
            {
                AddTagToTagListByUploadingCSV();
                DisableAllDivMessage();
                EnableControls();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelUploadTagsSelectedProp_Click(object sender, EventArgs e)
        {
            DisableAllDivMessage();
            EnableControls();
        }

        /// <summary>
        /// Add tags by uploading the csv file
        /// </summary>
        private void AddTagToTagListByUploadingCSV()
        {
            try
            {
                if (Session["dsUploadedTags"] != null)
                {
                    DataTable dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                    //The following code binds “EmployeeDetail” table present in DataSet to Gridview
                    ViewState["dtCSVAddedTags"] = dsUploadedTags;
                    FinalTagsList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Add Tags By Upload CSV

        private void FinalTagsList()
        {
            try
            {
                gvAssignTags.Enabled = true;
                DataTable dsFinalTagsList = new DataTable();

                //dsFinalTagsList.Tables[0].Columns.Add("TagID", typeof(string));
                //dsFinalTagsList.Tables[0].Columns.Add("TruckID", typeof(int));

                if ((Session["dsUploadedTags"] != null) && (Session["dtManuallyAddedTags"] != null))
                {
                    DataTable dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                    DataTable dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];

                    dsFinalTagsList.Merge(dsUploadedTags);
                    dsFinalTagsList.Merge(dtManuallyAddedTags, true, MissingSchemaAction.Ignore);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);
                }
                else if (Session["dsUploadedTags"] != null)
                {
                    DataTable dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                    dsFinalTagsList.Merge(dsUploadedTags);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);

                }
                else if (Session["dtManuallyAddedTags"] != null)
                {
                    DataTable dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                    dsFinalTagsList.Merge(dtManuallyAddedTags);
                    dsFinalTagsList = RemoveDuplicateTagsFromList(dsFinalTagsList);
                }
                else
                {
                    DataTable dtEmptyTagList = new DataTable("dtEmptyTagList");

                    dtEmptyTagList.Columns.Add("TagID");
                    dtEmptyTagList.Columns.Add("TruckID");

                    // Add new row of data to datatable
                    DataRow drEmptyTagList = dtEmptyTagList.NewRow();

                    //Adding the created data row to the datatable
                    dtEmptyTagList.Rows.Add(drEmptyTagList);
                    dtEmptyTagList.AcceptChanges();
                    dsFinalTagsList = dtEmptyTagList;
                    gvAssignTags.Enabled = false;
                    litTotal.Text = "0";
                }
                Session["dsFinalTagList"] = dsFinalTagsList;
                gvAssignTags.DataSource = dsFinalTagsList;
                gvAssignTags.DataBind();
                if (gvAssignTags.Enabled == true)
                {
                    litTotal.Text = Convert.ToString(dsFinalTagsList.Rows.Count);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Assign tag from Taglist to database

        /// <summary>
        /// Assign Hopper to the Proponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAssign_Click(object sender, EventArgs e)
        {
            try
            {
                // Create tags CSV string

                ArrayList lstTagCsv = new ArrayList();
                StringBuilder strLstTagIDs = new StringBuilder("-1");
                ArrayList lstTruckIDCSV = new ArrayList();
                StringBuilder strLstTruckID = new StringBuilder("-1");

                Hoppers objHopper = new Hoppers();
                int Result = objHopper.AssignHoppersProponents(Convert.ToInt32(ddlProponent.SelectedValue), Convert.ToInt32(ddlPad.SelectedValue));
                if (gvAssignTags.Rows.Count != 0)
                {
                    if (Session["dsFinalTagList"] != null)
                    {
                        DataTable dsFinalTagList = (DataTable)Session["dsFinalTagList"];
                        foreach (DataRow dr in dsFinalTagList.Rows)
                        {
                            if ((dr["TagID"] != DBNull.Value) && (dr["TruckID"] != DBNull.Value))
                            {
                                if (strLstTagIDs.Length >= 4200) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(Convert.ToString(dr["TagID"]));
                                    strLstTagIDs = new StringBuilder("-1");
                                }
                                else
                                {
                                    strLstTagIDs.Append("," + Convert.ToString(dr["TagID"]));
                                }
                                if (strLstTruckID.Length >= 2800) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTruckIDCSV.Add(Convert.ToString(dr["TruckID"]));
                                    strLstTruckID = new StringBuilder("-1");
                                }
                                else
                                {
                                    strLstTruckID.Append("," + Convert.ToString(dr["TruckID"]));
                                }
                            }
                        }
                        // Insert bulk Tagids by CSV string

                        if (strLstTagIDs.Length >= 5)
                        {
                            lstTagCsv.Add(strLstTagIDs);
                            lstTruckIDCSV.Add(strLstTruckID);
                        }
                        int index = 0;
                        int countTagsAdded = 0;
                        string tags = string.Empty, trucks = string.Empty;
                        Tags objTags = new Tags();
                        for (index = 0; index < lstTagCsv.Count; index++)
                        {
                            tags = lstTagCsv[index].ToString();
                            trucks = lstTruckIDCSV[index].ToString();
                            countTagsAdded += objTags.AssignTags(Convert.ToString(tags), Convert.ToString(trucks), Convert.ToInt32(ddlPad.SelectedValue), Convert.ToInt32(ddlProponent.SelectedValue));
                        }
                        if (countTagsAdded != 0)
                        {
                            DisableAllDivMessage();
                            divConfirmMsg.Visible = true;
                            litConfirmMsg.Text = countTagsAdded + " Tag(s) successfully assigned to PAD " + Convert.ToString(ddlPad.SelectedItem.Text) + " and Proponent " + Convert.ToString(ddlProponent.SelectedItem.Text);
                            ClearAllSessions();
                            FillHoppers();
                            FillProponents();
                            FinalTagsList();
                        }
                        else
                        {
                            DisableAllDivMessage();
                            divConfirmMsg.Visible = true;
                            litConfirmMsg.Text = "Tag(s) not exists/already issued. Please try again with other tag(s) list";
                            ClearAllSessions();
                            FillHoppers();
                            FillProponents();
                            FinalTagsList();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Assign tag from Taglist to database

        /// <summary>
        /// Remove Tags from tag list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvAssignTags.Rows.Count != 0)
                {
                    // Remove from Uploaded tag list
                    DataTable dsUploadedTags = null;
                    if (Session["dsUploadedTags"] != null)
                    {
                        dsUploadedTags = (DataTable)Session["dsUploadedTags"];
                    }

                    for (int i = 0; i <= gvAssignTags.Rows.Count - 1; i++)
                    {
                        CheckBox chkTagID = (CheckBox)gvAssignTags.Rows[i].FindControl("chkTagID");
                        if (chkTagID.Checked == true)
                        {
                            if (dsUploadedTags != null)
                            {
                                foreach (DataRow dr in dsUploadedTags.Rows)
                                {
                                    if ((dr["TagID"] != DBNull.Value))
                                    {
                                        if (Convert.ToString(dr["TagID"]) == Convert.ToString(gvAssignTags.DataKeys[i].Value))


                                        //int TagIDcount = (dsSessionUploadTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(gvAssignTags.DataKeys[i].Value) + "'")).Count();
                                        //if (TagIDcount > 0)
                                        {
                                            dr.Delete();
                                            i = i - 1;

                                            break;
                                        }
                                    }
                                }
                                dsUploadedTags.AcceptChanges();
                            }
                        }
                    }

                    // Remove from manually added tag list
                    DataTable dtManuallyAddedTags = null;
                    if (Session["dtManuallyAddedTags"] != null)
                    {
                        dtManuallyAddedTags = (DataTable)Session["dtManuallyAddedTags"];
                    }

                    for (int i = 0; i <= gvAssignTags.Rows.Count - 1; i++)
                    {
                        CheckBox chkTagID = (CheckBox)gvAssignTags.Rows[i].FindControl("chkTagID");
                        if (chkTagID.Checked == true)
                        {
                            if (dtManuallyAddedTags != null)
                            {
                                foreach (DataRow dr in dtManuallyAddedTags.Rows)
                                {
                                    if ((dr["TagID"] != DBNull.Value))
                                    {
                                        if (Convert.ToString(dr["TagID"]) == Convert.ToString(gvAssignTags.DataKeys[i].Value))


                                        //int TagIDcount = (dsSessionUploadTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(gvAssignTags.DataKeys[i].Value) + "'")).Count();
                                        //if (TagIDcount > 0)
                                        {
                                            dr.Delete();
                                            i = i - 1;

                                            break;
                                        }
                                    }
                                }
                                dtManuallyAddedTags.AcceptChanges();
                            }
                        }
                    }
                    DataTable dsFinalTagList = null;
                    if (Session["dsFinalTagList"] != null)
                    {
                        dsFinalTagList = (DataTable)Session["dsFinalTagList"];
                    }

                    bool flgRemoveTag = false;
                    // Remove from final tag list
                    for (int i = 0; i <= gvAssignTags.Rows.Count - 1; i++)
                    {
                        CheckBox chkTagID = (CheckBox)gvAssignTags.Rows[i].FindControl("chkTagID");
                        if (chkTagID.Checked == true)
                        {
                            if (dsFinalTagList != null)
                            {
                                foreach (DataRow dr in dsFinalTagList.Rows)
                                {
                                    if ((dr["TagID"] != DBNull.Value))
                                    {
                                        if (Convert.ToString(dr["TagID"]) == Convert.ToString(gvAssignTags.DataKeys[i].Value))


                                        //int TagIDcount = (dsSessionUploadTags.Tables[0].Select("TagID=" + "'" + Convert.ToString(gvAssignTags.DataKeys[i].Value) + "'")).Count();
                                        //if (TagIDcount > 0)
                                        {
                                            dr.Delete();
                                            i = i - 1;
                                            flgRemoveTag = true;
                                            break;
                                        }
                                    }
                                }
                                dsFinalTagList.AcceptChanges();
                            }
                        }
                    }
                    gvAssignTags.DataSource = dsFinalTagList;
                    gvAssignTags.DataBind();
                    litTotal.Text = Convert.ToString(dsFinalTagList.Rows.Count);
                    Session["dsFinalTagsList"] = dsFinalTagList;
                    Session["dtManuallyAddedTags"] = dtManuallyAddedTags;
                    Session["dsUploadedTags"] = dsUploadedTags;

                    if (flgRemoveTag == false)
                    {
                        divMessage.Visible = true;
                        litMessage.Text = "Please select tag(s) to remove";
                        return;
                    }
                    else
                    {
                        divMessage.Visible = false;
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    litMessage.Text = "Tag(s) list is empty";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        protected void gvAssignTags_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dsFinalTagList = (DataTable)Session["dsFinalTagList"];
            //bind the values to our GridView
            gvAssignTags.PageIndex = e.NewPageIndex;
            gvAssignTags.DataSource = dsFinalTagList;
            gvAssignTags.DataBind();

        }
        protected void gvAssignTags_PageIndexChanged(object sender, GridViewPageEventArgs e)
        {
            //gvAssignTags.PageIndex = e.;
            //gvAssignTags.DataBind();
        }

        private void ClearAllSessions()
        {
            Session["AlreadyAssignedTagCount"] = null;
            Session["dsAllAssignedTagsByProponentID"] = null;
            Session["dtManuallyAddedTags"] = null;
            Session["dsAlreadyAssignedTagsList"] = null;
            Session["dsUploadedTags"] = null;
            Session["dsFinalTagList"] = null;
            Session["Proponent"] = null;
            ViewState["tagsadded"] = null;
            ViewState["dtCSVAddedTags"] = null;
            FinalTagsList();
        }

        private void DisableAllDivMessage()
        {
            divTagReassignmentDiffProp.Visible = false;
            divTagReassignmentSelctedProp.Visible = false;
            divUploadTagsDiiffProp.Visible = false;
            divUploadTagsSelectedProp.Visible = false;
            divValidateProponentYesNo.Visible = false;
        }

        /// <summary>
        /// Column exists
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="ColumnName"></param>
        /// <returns></returns>
        private bool ColumnExists(DataSet ds)
        {
            bool colExists1 = false;
            bool colExists2 = false;
            bool colExists = false;
            foreach (DataColumn col in ds.Tables[0].Columns)
            {
                if (col.ColumnName == "TagID")
                {
                    colExists1 = true;
                }
                if (col.ColumnName == "TruckID")
                {
                    colExists2 = true;
                }
            }
            if ((colExists1 == true) && (colExists2 == true))
            {
                colExists = true;
            }

            return colExists;
        }

        private Boolean IsValidCSVstructure(DataSet dset)
        {
            Boolean flagInvalidformat = false;

            if (dset.Tables[0].Columns.Count != 2)
            {
                flagInvalidformat = true;
            }
            else
            {
                if (dset.Tables[0].Columns[0].ColumnName.ToLower() != "TagID".ToLower())
                {
                    flagInvalidformat = true;
                }
                else if (dset.Tables[0].Columns[1].ColumnName.ToLower() != "TruckID".ToLower())
                {
                    flagInvalidformat = true;
                } 
            }

            return !flagInvalidformat;
        }

        private DataTable RemoveDuplicateTagsFromList(DataTable dt)
        {
            bool flgTagRemoved = false;
            if (dt != null)
            {
                if (dt.Rows.Count != 0)
                {

                    foreach (DataRow dr in dt.Rows)
                    {
                        // Remove the tagid if its length is less than 9
                        if ((dr["TagID"] != null) && (dr["TruckID"] != null))
                        {
                            int TagIDcount = (dt.Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();
                            int TruckIDCount = 0;
                            if ((dr["TruckID"] != DBNull.Value))
                            {
                                TruckIDCount = (dt.Select("TruckID=" + "'" + Convert.ToString(dr["TruckID"]) + "'")).Count();
                            }
                            if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1) || (TruckIDCount > 1))
                            {
                                dr.Delete();
                                flgTagRemoved = true;
                            }
                        }
                    }
                    dt.AcceptChanges();
                }
            }
            return dt;
        }

        // Confirmation message
        protected void btnConfirmMsg_Click(object sender, EventArgs e)
        {
            litConfirmMsg.Text = string.Empty;
            divConfirmMsg.Visible = false;
        }
    }
}
