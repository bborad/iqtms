﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assigntag.aspx.cs" Inherits="Ramp.UI.tags.assigntag"
    MasterPageFile="~/masterpages/SiteMaster.Master" Theme="tags" Title="Assign Tag(s)" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>ASSIGN TAGS</b></center>
            <br />
            <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                <tr>
                    <td align="center" valign="top">
                        PAD ID:
                        <asp:DropDownList ID="ddlPad" runat="server" ValidationGroup="valGrpAssign" DataTextField="HopperName"
                            DataValueField="ID" OnSelectedIndexChanged="ddlPad_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        Proponent:
                        <asp:DropDownList ID="ddlProponent" runat="server" ValidationGroup="valGrpAssign"
                            AutoPostBack="true" DataTextField="ProponentName" DataValueField="ID" OnSelectedIndexChanged="ddlProponent_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="left" valign="top">
                        <div id="divAddControls" runat="server">
                            <table width="100%" class="border_strcture1">
                                <tr>
                                    <td width="50%" align="left" valign="top">
                                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                            <tr>
                                                <td>
                                                    Upload
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="border_strcture1">
                                                        <tr>
                                                            <td>
                                                                <asp:FileUpload ID="upFileTags" runat="server" />
                                                                <asp:RequiredFieldValidator ID="reqValFileTags" runat="server" Display="Dynamic"
                                                                    ErrorMessage="* Required" ControlToValidate="upFileTags" ValidationGroup="valGrpAddTags"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regexpupFileTags" runat="server" Display="Dynamic"
                                                                    ErrorMessage="Only csv files are allowed." ControlToValidate="upFileTags" ValidationGroup="valGrpAddTags"
                                                                    ValidationExpression="^([a-zA-Z].*|[1-9].*)\.(((c|C)(s|S)(v|V)))$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnUpload" runat="server" Text="Upload" CausesValidation="true" ValidationGroup="valGrpAddTags"
                                                                    class="but" OnClick="btnUpload_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Add
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                                                        <tr>
                                                            <td align="right">
                                                                Tag ID:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtTagID" runat="server" class="txtBox" MaxLength="9" ValidationGroup="valGrpAddManualTags"></asp:TextBox>
                                                                  <asp:RegularExpressionValidator ID="revTruckID" runat="server" ControlToValidate="txtTagID"
                                                                    ValidationExpression="^[0-9a-zA-Z]+$" Display="Dynamic" ValidationGroup="valGrpAddManualTags" ErrorMessage = "*Invalid text">
                                                                </asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="rfvRefrence" runat="server" ErrorMessage="* Required"
                                                                    ControlToValidate="txtTagID" Display="Dynamic" ValidationGroup="valGrpAddManualTags"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                Truck ID:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtTruckID" runat="server" class="txtBox" MaxLength="5" ValidationGroup="valGrpAddManualTags"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvTruckID" runat="server" ErrorMessage="* Required"
                                                                    ControlToValidate="txtTruckID" Display="Dynamic" ValidationGroup="valGrpAddManualTags"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regExpVal" runat="server" ControlToValidate="txtTruckID"
                                                                    ValidationExpression="^[0-9a-zA-Z]+$" Display="Dynamic" ValidationGroup="valGrpAddManualTags" ErrorMessage = "*Invalid text">
                                                                </asp:RegularExpressionValidator>
                                                                <asp:Button ID="btnAddTag" runat="server" Text="Add" CausesValidation="true" ValidationGroup="valGrpAddManualTags"
                                                                    class="but" OnClick="btnAddTag_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%" align="right" valign="top">
                                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                            <tr>
                                                <td colspan="2" valign="top">
                                                    <asp:GridView ID="gvAssignTags" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture" DataKeyNames="TagID"
                                                        AllowPaging="true" PageSize="10" OnPageIndexChanging="gvAssignTags_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkTagID" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="TagID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                                                SortExpression="TagID" />
                                                            <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Right" HeaderText="Truck ID"
                                                                SortExpression="TruckID" />
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No tag(s) added to tag list
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnRemove" runat="server" Text="Remove" class="but" OnClick="btnRemove_Click" />
                                                </td>
                                                <td align="right">
                                                    Total:
                                                    <asp:Literal ID="litTotal" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Click <a href="../reports/assignunassigntagreport.aspx">here</a> to see tag list
                        report.
                    </td>
                    <td align="right" colspan="2">
                        <asp:Button ID="btnAssign" runat="server" Text="Assign" class="but" OnClick="btnAssign_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <%--Hopper and proponent reassignment check--%>
            <div id="divValidateProponentYesNo" width="20%" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;">
                                    <asp:Literal ID="litValidateProponent" runat="server"></asp:Literal>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <center>
                                    <asp:Button ID="btnYes" CssClass="but" runat="server" Text=" YES " OnClick="btnYes_Click" />
                                    <asp:Button ID="btnNo" CssClass="but" runat="server" Text=" NO " OnClick="btnNo_Click" />
                                </center>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <%--Tags Reassignment - different Proponent check adding manually--%>
            <div id="divTagReassignmentDiffProp" width="20%" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;">
                                    <asp:Literal ID="litTagReassignmentDiffProp" runat="server"></asp:Literal>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <center>
                                    <asp:Button ID="btnYesTagReassignmentDiffProp" CssClass="but" runat="server" Text=" YES "
                                        OnClick="btnYesTagReassignmentDiffProp_Click" />
                                    <asp:Button ID="btnNoTagReassignmentDiffProp" CssClass="but" runat="server" Text=" NO "
                                        OnClick="btnNoTagReassignmentDiffProp_Click" />
                                </center>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <%--Tags Reassignment - Selected Proponent check adding manually--%>
            <div id="divTagReassignmentSelctedProp" width="20%" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;">
                                    <asp:Literal ID="litTagReassignmentSelctedProp" runat="server"></asp:Literal>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <center>
                                    <asp:Button ID="btnSkip" CssClass="but" runat="server" Text=" SKIP " OnClick="btnSkip_Click" />
                                    <asp:Button ID="btnContinue" CssClass="but" runat="server" Text=" CONTINUE " OnClick="btnContinue_Click" />
                                    <asp:Button ID="btnCancel" CssClass="but" runat="server" Text=" CANCEL " OnClick="btnCancel_Click" />
                                </center>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <%--Tags Reassignment - different Proponent check adding by uploading--%>
            <div id="divUploadTagsDiiffProp" width="20%" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;">
                                    <asp:Literal ID="litUploadTagsDiiffProp" runat="server"></asp:Literal>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <center>
                                    <asp:Button ID="btnYesUploadTagsDiiffProp" CssClass="but" runat="server" Text=" YES "
                                        OnClick="btnYesUploadTagsDiiffProp_Click" />
                                    <asp:Button ID="btnNoUploadTagsDiiffProp" CssClass="but" runat="server" Text=" NO "
                                        OnClick="btnNoUploadTagsDiiffProp_Click" />
                                </center>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <%--Tags Reassignment - Selected Proponent check adding by uploading--%>
            <div id="divUploadTagsSelectedProp" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: Red;">
                                    <asp:Literal ID="litUploadTagsSelectedProp" runat="server"></asp:Literal>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <center>
                                    <asp:Button ID="btnSkipUploadTagsSelectedProp" CssClass="but" runat="server" Text=" SKIP "
                                        OnClick="btnSkipUploadTagsSelectedProp_Click" />
                                    <asp:Button ID="btnContinueUploadTagsSelectedProp" CssClass="but" runat="server"
                                        Text=" CONTINUE " OnClick="btnContinueUploadTagsSelectedProp_Click" />
                                    <asp:Button ID="btnCancelUploadTagsSelectedProp" CssClass="but" runat="server" Text=" CANCEL "
                                        OnClick="btnCancelUploadTagsSelectedProp_Click" />
                                </center>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <div id="divMessage" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: red">
                                    <asp:Literal ID="litMessage" runat="server"></asp:Literal></span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <div id="divConfirmMsg" runat="server" visible="false">
                <center>
                    <table style="width: 20%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <span style="color: red">
                                    <asp:Literal ID="litConfirmMsg" runat="server"></asp:Literal></span>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnConfirmMsg" CssClass="but" runat="server" Text=" OK " OnClick="btnConfirmMsg_Click" />
                            </td>
                        </tr>
                    </table>
                </center>
                <br />
            </div>
            <div id="divAllcontrols" runat="server">
            </div>
        </div>
    </div>
</asp:Content>
