﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="unassigntag.aspx.cs" Inherits="Ramp.UI.tags.unassigntag"
    Theme="tags" MasterPageFile="~/masterpages/SiteMaster.Master" Title="Un-assign/Remove Tags" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
          <center><b>UN-ASSIGN TAGS</b></center><br />
            <table width="50%" class="border_strcture1">
                <tr>
                    <td colspan="2">
                        <center>
                            <span style="color: Red;">
                                <asp:Literal ID="litMessage" runat="server"></asp:Literal></span>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <table width="100%">
                            <tr>
                                <td>
                                    Upload
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="upFileTags" runat="server" ValidationGroup="valGrpUpAddTags" />
                                                <asp:RequiredFieldValidator ID="reqValFileTags" runat="server" Display="Dynamic"
                                                    ErrorMessage="* Required" ControlToValidate="upFileTags" ValidationGroup="valGrpUpAddTags"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regexpupFileTags" runat="server" Display="Dynamic"
                                                    ErrorMessage="Only csv files are allowed." ControlToValidate="upFileTags" ValidationGroup="valGrpUpAddTags"
                                                    ValidationExpression="^([a-zA-Z].*|[1-9].*)\.(((c|C)(s|S)(v|V)))$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload" CausesValidation="true" ValidationGroup="valGrpUpAddTags"
                                                    class="but" OnClick="btnUpload_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table width="100%">
                            <tr>
                                <td>
                                    Add
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                                        <tr>
                                            <td>
                                                Tag ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTagID" runat="server" MaxLength="9" class="txtBox" Style="width: 135px"
                                                    ValidationGroup="valGrpAddTags"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvRefrence" runat="server" ErrorMessage="* Required"
                                                    ControlToValidate="txtTagID" Display="Dynamic" ValidationGroup="valGrpAddTags"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnAddTag" runat="server" Text="Add" CausesValidation="true" ValidationGroup="valGrpAddTags"
                                                    class="but" OnClick="btnAddTag_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="85%" valign="top">
                                    <asp:GridView ID="gvUnAssignTags" runat="server" AutoGenerateColumns="False" Width="100%"
                                        DataKeyNames="TagRFID" border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture"
                                        OnPageIndexChanging="gvUnAssignTags_PageIndexChanging" PageSize="10" AllowPaging="true">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkTagID" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                                SortExpression="ID" Visible="false" />
                                            <asp:BoundField DataField="TagRFID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                                SortExpression="TagRFID" />
                                            <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Left" HeaderText="Truck ID"
                                                SortExpression="TruckID" />
                                            <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent ID"
                                                SortExpression="ProponentID" Visible="false" />
                                            <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                                SortExpression="ProponentName" />
                                            <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Left" HeaderText="Hopper ID"
                                                SortExpression="HopperID" Visible="false" />
                                            <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD"
                                                SortExpression="HopperName" />
                                            <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                                SortExpression="Assigned" Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td width="17%" align="center" valign="top">
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CausesValidation="true" class="but"
                                        OnClick="btnDelete_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    Total:<asp:Literal ID="litTotal" runat="server"></asp:Literal>
                                    <table width="72%">
                                        <tr>
                                            <td align="right">
                                                <%--</td>
                                            <td width="35%" align="center">--%>
                                                <asp:Button ID="btnUnAssign" runat="server" Text="Un-Assign" CausesValidation="true"
                                                    class="but" OnClick="btnUnAssign_Click" />
                                                <asp:Button ID="btnRemove" runat="server" Text="Remove From DB" CausesValidation="true"
                                                    class="but" OnClick="btnRemove_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Click <a href="../reports/assignunassigntagreport.aspx">here</a> to see tag list
                                                report.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
