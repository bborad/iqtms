﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addtag.aspx.cs" Inherits="Ramp.UI.tags.addtag"
    MasterPageFile="~/masterpages/SiteMaster.Master" Theme="tags" Title="Add New Tag(s)" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
         <center><b>ADD TAGS</b></center><br />
            <table width="50%" border="0" align="center" cellpadding="2" cellspacing="2" class="border_strcture1">
                <tr>
                    <td width="14%" rowspan="4" align="right" valign="top">
                        Tag ID:
                    </td>
                    <td width="32%" rowspan="3" valign="top">
                        <div>
                            <asp:ListBox ID="lstAddRemoveTag" runat="server" SelectionMode="Multiple" DataTextField="TagID"
                                CssClass="add_remove_box" DataValueField="TagID"></asp:ListBox>
                        </div>
                    </td>
                    <td width="54%" valign="top">
                        Upload :
                        <br />
                        <asp:FileUpload ID="upFileTags" runat="server" />
                        <asp:RequiredFieldValidator ID="reqValFileTags" runat="server" Display="Dynamic"
                            ErrorMessage="* Required" ControlToValidate="upFileTags" ValidationGroup="valGrpAddTags"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexpupFileTags" runat="server" Display="Dynamic"
                            ErrorMessage="Only csv files are allowed." ControlToValidate="upFileTags" ValidationGroup="valGrpAddTags"
                            ValidationExpression="^([a-zA-Z].*|[1-9].*)\.(((c|C)(s|S)(v|V)))$"></asp:RegularExpressionValidator>
                        <br />
                        <br />
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_OnClick"
                            CausesValidation="true" ValidationGroup="valGrpAddTags" class="but" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total:
                        <asp:Literal ID="litTotalUpTags" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Reader :
                        <asp:DropDownList ID="ddlReader" runat="server" DataTextField="ReaderName" DataValueField="ID"
                            DataSourceID="odsReader" ValidationGroup="valGrpReader">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvReader" runat="server" ErrorMessage="* Required"
                            ControlToValidate="ddlReader" InitialValue="0" Display="Dynamic" ValidationGroup="valGrpReader"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                        <asp:Button ID="btnReadTags" runat="server" Text="Read Tags" class="but" OnClick="btnReadTags_Click"
                            ValidationGroup="valGrpReader" />
                        &nbsp;&nbsp; Total:
                        <asp:Literal ID="litTotReadTags" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" valign="top">
                        <asp:Button ID="btnAddAll" runat="server" Text="Add" OnClick="btnAddAll_Click" class="but" />
                        <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click"
                            class="but" />
                        <asp:Button ID="btnCancel" runat="server" Text="Clear" OnClick="btnCancel_Click"
                            class="but" />
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        &nbsp;
                    </td>
                    <td colspan="2" align="right" valign="top">
                        Click <a href="../reports/assignunassigntagreport.aspx">here</a> to see tag list report.
                    </td>
                </tr>
            </table>
            <table width="50%">
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <div id="divMessage" runat="server" visible="false">
                            <center>
                                <table style="width: 50%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                                    <tr>
                                        <th align="left" class="border_strcture1">
                                            Add New Tag
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <span style="color: Red;"><span style="color: red">
                                                <asp:Literal ID="litMsg" runat="server"></asp:Literal></span> </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnOK" runat="server" Text="OK" class="but" OnClick="btnOK_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </div>
                        <div id="divRemoveTagMsg" runat="server" visible="false">
                            <center>
                                <table style="width: 50%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                                    <tr>
                                        <th align="left" class="border_strcture1">
                                            Remove Tag(s)
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <span style="color: Red;"><span style="color: red">
                                                <asp:Literal ID="litRmvMsg" runat="server"></asp:Literal></span> </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnRmvOK" runat="server" Text="YES" class="but" OnClick="btnRmvOK_Click" />
                                            <asp:Button ID="btnRmvCancel" runat="server" Text="NO" class="but" OnClick="btnRmvCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
            <center>
            <span style="color: Red;text-align:">
                <asp:Literal ID="litExMsg" runat="server"></asp:Literal></span></center>
            <asp:ObjectDataSource ID="odsReader" runat="server" SelectMethod="GetAllReaders"
                TypeName="Ramp.DBConnector.Adapter.Readers"></asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
