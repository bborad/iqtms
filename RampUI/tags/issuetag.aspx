﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="issuetag.aspx.cs" Inherits="Ramp.UI.tags.issuetag"
    MasterPageFile="~/masterpages/SiteMaster.Master" Title="Issue Tag(s)" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>ISSUE TAGS</b></center>
            <br />
            <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                <tr>
                    <td colspan="3">
                        <%--  <span style="color: red">
                                <asp:Literal ID="litMsg" runat="server"></asp:Literal></span>--%>
                        <div id="divMessage" runat="server" visible="false">
                            <center>
                                <table style="width: 50%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <span style="color: Red;"><span style="color: red">
                                                <asp:Literal ID="litMsg" runat="server"></asp:Literal></span> </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </div>
                    </td>
                </tr>
                <tr visible = "false">
                    <td width="30%">
                       <%-- Readers:--%>
                        <asp:DropDownList ID="ddlReader" runat="server" DataTextField="ReaderName" DataValueField="ID"
                            Visible="false" ValidationGroup="valGrpReader" DataSourceID="odsReader">
                        </asp:DropDownList>
                    </td>
                    <td width="25%" colspan="2" align = "left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Select Reader"
                            ControlToValidate="ddlReader" Display="Dynamic" ValidationGroup="valGrpReader"
                            InitialValue="0"></asp:RequiredFieldValidator>
                        <asp:Button ID="btnReadTags" runat="server" Text="Read Tags" class="but" OnClick="btnReadTags_Click"
                            CausesValidation="true" ValidationGroup="valGrpReader" Visible="false" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="30%">
                        Reference:
                        <asp:TextBox ID="txtTruckRegNo" runat="server" class="txtBox" ValidationGroup="valGrpIssueTags"
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Required"
                            ControlToValidate="txtTruckRegNo" Display="Dynamic" ValidationGroup="valGrpIssueTags"></asp:RequiredFieldValidator>
                    </td>
                    <td width="25%">
                        Proponent:
                        <asp:DropDownList ID="ddlProponent" runat="server" AutoPostBack="true" DataTextField="ProponentName"
                            DataValueField="ID" OnSelectedIndexChanged="ddlProponent_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td width="25%">
                        PAD ID:
                        <asp:DropDownList ID="ddlPad" runat="server" DataTextField="HopperName" DataValueField="ID"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPad_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="top">
                        <asp:GridView ID="gvIssueTags" runat="server" AutoGenerateColumns="False" Width="100%"
                            border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture" AllowPaging="true"
                            PageSize="10" OnPageIndexChanging="gvIssueTags_PageIndexChanging" DataKeyNames="TagRFID"
                            OnRowDataBound="gvIssueTags_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkHeader" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkTagID" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                    SortExpression="ID" Visible="false" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="TagRFID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag RFID"
                                    SortExpression="TagRFID" Visible="false" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="left" HeaderText="Truck ID"
                                    SortExpression="TruckID" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Right" HeaderText="Proponent ID"
                                    SortExpression="ProponentID" Visible="false" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                    SortExpression="ProponentName" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Right" HeaderText="Hopper ID"
                                    SortExpression="HopperID" Visible="false" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD"
                                    SortExpression="HopperName" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture" />
                                <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                    SortExpression="Assigned" Visible="false" ItemStyle-CssClass="border_strcture" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td width="21%" valign="top">
                        <p>
                            <asp:Button ID="btnDispatched" runat="server" Text="Issue" class="but" OnClick="btnDispatched_Click"
                                CausesValidation="true" ValidationGroup="valGrpIssueTags" />
                        </p>
                        <br />
                        <p>
                            <asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="true" ValidationGroup="valGrpAddTags"
                                class="but" Style="height: 26px" OnClick="btnRemove_Click" />
                        </p>
                        <br />
                        <p>
                            <asp:Button ID="btnReturnedTags" runat="server" Text="Returned Tags" class="but"
                                CausesValidation="false" ValidationGroup="valGrpReader" OnClick="btnReturnedTags_Click"
                                Visible="false" />
                        </p>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        Total:<asp:Literal ID="litTotal" runat="server"></asp:Literal>
                    </td>
                    <td align="right" valign="top">
                        Click <a href="../reports/assignunassigntagreport.aspx">here</a> for Issued
                        Tag report.
                    </td>
                    <td valign="top">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <center>
                <span style="color: Red; text-align: ">
                    <asp:Literal ID="litExMsg" runat="server"></asp:Literal></span></center>
            <asp:ObjectDataSource ID="odsReader" runat="server" SelectMethod="GetAllReaders"
                TypeName="Ramp.DBConnector.Adapter.Readers"></asp:ObjectDataSource>
        </div>
    </div>

    <script type="text/javascript">

        function SelectAll(id) {
            //get reference of GridView control
            var grid = document.getElementById("<%= gvIssueTags.ClientID %>");
            //variable to contain the cell of the grid
            var cell;

            if (grid.rows.length > 0) {
                //loop starts from 1. rows[0] points to the header.
                for (i = 1; i < grid.rows.length; i++) {
                    //get the reference of first column
                    cell = grid.rows[i].cells[0];

                    //loop according to the number of childNodes in the cell
                    for (j = 0; j < cell.childNodes.length; j++) {
                        //if childNode type is CheckBox                 
                        if (cell.childNodes[j].type == "checkbox") {
                            //assign the status of the Select All checkbox to the cell checkbox within the grid
                            cell.childNodes[j].checked = document.getElementById(id).checked;
                        }
                    }
                }
            }
        }

        function checkNumerickFormat(oEvent) {
            retVal = true;

            oEvent = oEvent || window.event;
            var txtField = oEvent.target || oEvent.srcElement;



            var keyCode = oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;

            // alert(keyCode);

            if (keyCode == 46 || keyCode == 8 || keyCode == 9)
                return true;
            if (!(keyCode >= 48 && keyCode <= 57))
                return false;
            if (keyCode != null) {
                var strkey = String.fromCharCode(keyCode);

                if ((strkey < '0') || (strkey > '9')) {
                    if (AllowKeys.indexOf(keyCode) != -1)
                        retVal = true;
                    else
                        retVal = false;
                }
            }
            document.getElementById(txtField.id).focus();
            return retVal;
        }

    </script>

</asp:Content>
