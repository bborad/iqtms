using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.RFIDHWConnector.Adapters;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Common;
using System.Collections;
using System.Text;

namespace Ramp.UI.tags
{
    public partial class issuetag : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            litExMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                EmptyGridDtatSource();
                ClearAllSessions();
                GetTagsToDispatch();
                FillHoppers();
                FillProponents();
            }
            Session["IssuedTagPage"] = true;
        }

        /// <summary>
        /// Dispatch issue tags 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDispatched_Click(object sender, EventArgs e)
        {
            litMsg.Text = "";
            gvIssueTags.Enabled = true;
            divMessage.Visible = false;
            if (gvIssueTags.Rows.Count != 0)
            {
                Tags objTags = new Tags();
                // Get Tag list
                if (Session["ListTagID"] != null)
                {
                   // ArrayList lstTagCsv = (ArrayList)Session["ListTagID"];

                    ArrayList lstTagCsv = new ArrayList();

                    ArrayList lstTagBattery = (ArrayList)Session["lstTagBattery"];
                    // Dispatch all tags of list
                    int countaddedrows = 0;
                    int index = 0;
                    string tags = string.Empty, tagBattery = string.Empty;

                    StringBuilder strLstTagIDs = new StringBuilder("'-1'");

                    DataTable dt;
                    dt = (DataTable)Session["dtIssueTagsList"];

                    if (dt == null)
                    {   
                        return;
                    }

                  //  DataTable dtCopy = dt.Copy();

                    DataView dv = new DataView(dt);

                    string rowFilter = "";
                    string joinner = "";

                    if ((Convert.ToInt32(ddlProponent.SelectedValue) != 0))
                    {
                        rowFilter = "ProponentID = " + Convert.ToInt32(ddlProponent.SelectedValue);
                        joinner = " AND ";
                    }

                    if ((Convert.ToInt32(ddlPad.SelectedValue) != 0))
                    {
                        rowFilter = rowFilter + joinner + "HopperID = " + Convert.ToInt32(ddlPad.SelectedValue);
                    }

                    if (rowFilter != "")
                    {
                        dv.RowFilter = rowFilter;
                    }

                    if (dv.Count > 0)
                    {
                        for (int i = 0; i < dv.Count; i++)
                        {
                            if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
                            {
                                lstTagCsv.Add(strLstTagIDs);
                                strLstTagIDs = new StringBuilder("'-1'");
                            }
                            else
                            {
                                strLstTagIDs.Append("," + "'" + dv[i]["TagRFID"].ToString() + "'");
                            }
                        }
                    } 

                    //for (int i = 0; i < gvIssueTags.Rows.Count; i++)
                    //{                          
                    //    if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
                    //    {
                    //        lstTagCsv.Add(strLstTagIDs);
                    //        strLstTagIDs = new StringBuilder("'-1'");
                    //    }
                    //    else
                    //    {
                    //        strLstTagIDs.Append("," + "'" + gvIssueTags.DataKeys[i].Value + "'");
                    //    }
                    //}

                    if (strLstTagIDs.Length >= 5)
                    {
                        lstTagCsv.Add(strLstTagIDs);                         
                    }

                    for (index = 0; index < lstTagCsv.Count; index++)
                    {
                        tags = lstTagCsv[index].ToString();
                        tagBattery = "-1";
                        countaddedrows = objTags.DispatchTagsByTagsID(Convert.ToString(tags), Convert.ToString(txtTruckRegNo.Text), Convert.ToString(tagBattery));
                    }

                    //foreach (StringBuilder strCsv in lstTagCsv)
                    //{
                    //    countaddedrows = objTags.DispatchTagsByTagsID(Convert.ToString(strCsv), Convert.ToString(txtTruckRegNo.Text));
                    //}
                    divMessage.Visible = true;
                    //if (countaddedrows != 0)
                    //{
                        //litMsg.Text = "Tags imported successfully from CSV/text file <br/> Total Tags: " + totaltags + "<br/> Tags added: " + tagsadded + "<br/> Tags removed due to inconsistency: " + tagsRemoved;  
                      
                        Session["dtIssueTagsList"] = null;
                        Session["ListTagID"] = null;
                        Session["AllTags"] = null;
                        ClearAllSessions();
                        txtTruckRegNo.Text = "";
                        EmptyGridDtatSource();
                        GetTagsToDispatch();
                        litMsg.Text = "Tag(s) issued successfully";
                        divMessage.Visible = true;
                        //ddlReader.SelectedIndex = 0;
                  //  }
                }
                else
                {
                    divMessage.Visible = true;
                    litMsg.Text = " Tag(s) list is empty";
                }
            }
            else
            {
                divMessage.Visible = true;
                litMsg.Text = " Tag(s) list is empty";
                EmptyGridDtatSource();
            }
        }

        /// <summary>
        /// Read Tags 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReadTags_Click(object sender, EventArgs e)
        {
            try
            {
                litMsg.Text = "";
                divMessage.Visible = false;
                EnableAllButton();
                gvIssueTags.Enabled = true;
                // Get Reader details by ID to get reader information
                IReaders objReaderDB = Readers.GetReaderDetailsByID(Convert.ToInt32(ddlReader.SelectedValue));
                Reader objReader = new Reader(objReaderDB.IPAddress, objReaderDB.PortNo);

                // Scan all tags from reader in list
                // List<RFIDTag> lstRFIDTag = objReader.ScanForTags(10);                
                List<RFIDTag> lstRFIDTag = objReader.GetTags();
                //objReader.ClearTagList();
                objReader.Dispose();

                ArrayList lstTagCsv = new ArrayList();
                StringBuilder strLstTagIDs = new StringBuilder("'-1'");
                ArrayList lstTagBattery = new ArrayList();
                StringBuilder strTagBattery = new StringBuilder("-1");

                if (Session["ListTagID"] != null)
                {
                    lstTagCsv = (ArrayList)Session["ListTagID"];
                    lstTagBattery = (ArrayList)Session["lstTagBattery"];
                }

                if (lstRFIDTag != null)
                {
                    if (lstRFIDTag.Count > 0)
                    {
                        List<string> ListSessionTagID;

                        if (Session["AllTags"] != null)
                        {
                            ListSessionTagID = (List<string>)Session["AllTags"];
                        }
                        else
                        {
                              ListSessionTagID = new List<string>();     
                        }

                        foreach (RFIDTag objRFIDTag in lstRFIDTag)
                        {
                            if (!ListSessionTagID.Contains(objRFIDTag.TagID))
                            {
                                ListSessionTagID.Add(objRFIDTag.TagID);

                                if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(strLstTagIDs);
                                    strLstTagIDs = new StringBuilder("'-1'");
                                }
                                else
                                {
                                    strLstTagIDs.Append("," + "'" + objRFIDTag.TagID + "'");
                                }
                                if (strTagBattery.Length >= 2800) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagBattery.Add(strTagBattery);
                                    strTagBattery = new StringBuilder("-1");
                                }
                                else
                                {
                                    strTagBattery.Append("," + Convert.ToString(Convert.ToInt32(objRFIDTag.BatteryStatus)));
                                }

                            }
                        }

                        Session["AllTags"] = ListSessionTagID;

                        //if (Session["AllTags"] != null)
                        //{
                        //    List<string> lst = (List<string>)Session["AllTags"];
                        //    lst.AddRange(ListSessionTagID);
                        //}
                        //else
                        //{
                        //    Session["AllTags"] = ListSessionTagID;
                        //}

                        if (strLstTagIDs.Length >= 5)
                        {
                            lstTagCsv.Add(strLstTagIDs);
                            lstTagBattery.Add(strTagBattery);
                        }

                        Session["lstTagBattery"] = lstTagBattery;
                        Session["ListTagID"] = lstTagCsv;

                        DataTable dtIssuedTags = null;
                        ArrayList lstDataTables = new ArrayList();
                        foreach (StringBuilder strCsv in lstTagCsv)
                        {
                            dtIssuedTags = Tags.GetTagDetailsByTagsID(Convert.ToString(strCsv));
                            lstDataTables.Add(dtIssuedTags);
                        }
                        DataTable dtIssueTagsList = new DataTable();
                        //DataTable dtIssueTagsList = null;
                        foreach (DataTable dtIssuedTag in lstDataTables)
                        {
                            dtIssueTagsList.Merge(dtIssuedTag);
                        }

                        // Bind final datatable of all tags detail to gridview
                        if (dtIssueTagsList != null)
                        {
                            if (Session["dtIssueTagsList"] != null)
                            {
                                DataTable dtSessionTags = (DataTable)Session["dtIssueTagsList"];
                                dtIssueTagsList.Merge(dtSessionTags);
                                litTotal.Text = Convert.ToString(dtIssueTagsList.Rows.Count);
                                if (dtIssueTagsList.Rows.Count != 0)
                                {
                                    dtIssueTagsList = RemoveDuplicateTagsFromList(dtIssueTagsList);
                                    litTotal.Text = Convert.ToString(dtIssueTagsList.Rows.Count);
                                    gvIssueTags.DataSource = dtIssueTagsList;
                                    gvIssueTags.DataBind();
                                }
                                else
                                {
                                    EmptyGridDtatSource();
                                }
                            }
                            else
                            {
                                if (dtIssueTagsList.Rows.Count != 0)
                                {
                                    dtIssueTagsList = RemoveDuplicateTagsFromList(dtIssueTagsList);
                                    litTotal.Text = Convert.ToString(dtIssueTagsList.Rows.Count);
                                    gvIssueTags.DataSource = dtIssueTagsList;
                                    gvIssueTags.DataBind();
                                }
                                else
                                {
                                    EmptyGridDtatSource();
                                }
                            }
                        }
                        else
                        {
                            litTotal.Text = "0";
                            EmptyGridDtatSource();
                        }
                        Session["dtIssueTagsList"] = dtIssueTagsList;
                    }
                    else
                    {
                        divMessage.Visible = true;
                        litMsg.Text = "No tag(s) found for issue in selected reader OR may be the tag(s) have running transaction";
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    litMsg.Text = "No tag(s) found for issue in selected reader OR may be the tag(s) have running transaction";
                }
                ddlReader.SelectedIndex = 0;

                ddlPad.SelectedValue = "0";
                ddlProponent.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                litExMsg.Text = ex.Message;
                DisableAllButton();
            }
        }

        /// <summary>
        /// Remove the seleted tags from the Tags list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
               
                divMessage.Visible = false;
                //DataTable dtIssueTagsList = new DataTable();
                DataTable dtIssueTagsList = null;
                List<string> ListTagID = new List<string>();
                if (Session["dtIssueTagsList"] != null)
                {
                    dtIssueTagsList = (DataTable)Session["dtIssueTagsList"];
                }
                if (Session["AllTags"] != null)
                {
                    ListTagID = (List<string>)Session["AllTags"];
                }

                DataRow[] drr;

                for (int i = 0; i <= gvIssueTags.Rows.Count - 1; i++)
                {
                    CheckBox chkTagID = (CheckBox)gvIssueTags.Rows[i].FindControl("chkTagID");
                    if (chkTagID.Checked == true)
                    {
                        if (dtIssueTagsList != null)
                        {
                            drr = dtIssueTagsList.Select("TagRFID = '" + gvIssueTags.DataKeys[i].Value + "'");

                            if (drr != null && drr.Length > 0)
                            {
                                foreach (DataRow dr in drr)
                                {
                                    if ((dr["TagRFID"] != DBNull.Value))
                                    {
                                        ListTagID.Remove(dr["TagRFID"].ToString());
                                        dr.Delete();                                          
                                         
                                    }
                                   
                                }
                            }
                            //foreach (DataRow dr in dtIssueTagsList.Rows)
                            //{
                            //    if ((dr["TagRFID"] != DBNull.Value))
                            //    {
                            //        if (Convert.ToString(dr["TagRFID"]) == Convert.ToString(gvIssueTags.DataKeys[i].Value))
                            //        {
                            //            dr.Delete();
                            //            break;
                            //        }
                            //    }
                            //}
                            dtIssueTagsList.AcceptChanges();

                            //if (ListTagID != null)
                            //{
                            //    foreach (string objTagRFID in ListTagID)
                            //    {

                            //        if ((objTagRFID != null) || (objTagRFID != string.Empty))
                            //        {
                            //            if (Convert.ToString(objTagRFID) == Convert.ToString(gvIssueTags.DataKeys[i].Value))
                            //            {
                            //                ListTagID.Remove(objTagRFID);
                            //                i = i - 1;
                            //                break;
                            //            }
                            //        }
                            //    }
                            //}

                        }
                    }
                }

                Session["dtIssueTagsList"] = dtIssueTagsList;

                if (dtIssueTagsList.Rows.Count != 0)
                {
                    //gvIssueTags.DataSource = dtIssueTagsList;
                    //gvIssueTags.DataBind();
                    BindFilteredGrid();
                }
                else
                {
                    EmptyGridDtatSource();
                }
                Session["AllTags"] = ListTagID;
                CreateArrayList(ListTagID);
               
               

               
               
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void gvIssueTags_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //DataTable dsFinalTagList = (DataTable)Session["dtIssueTagsList"];
            //bind the values to our GridView
            gvIssueTags.PageIndex = e.NewPageIndex;

            BindFilteredGrid();

            //gvIssueTags.DataSource = dsFinalTagList;
            //gvIssueTags.DataBind();
        }

        private DataTable EmptyGridDtatSource()
        {
            DataTable dtEmptyTagList = new DataTable("dtEmptyTagList");

            dtEmptyTagList.Columns.Add("ID");
            dtEmptyTagList.Columns.Add("TagRFID");
            dtEmptyTagList.Columns.Add("TruckID");
            dtEmptyTagList.Columns.Add("ProponentID");
            dtEmptyTagList.Columns.Add("ProponentName");
            dtEmptyTagList.Columns.Add("HopperID");
            dtEmptyTagList.Columns.Add("HopperName");
            dtEmptyTagList.Columns.Add("Assigned");


            // Add new row of data to datatable
            DataRow drEmptyTagList = dtEmptyTagList.NewRow();

            //Adding the created data row to the datatable
            dtEmptyTagList.Rows.Add(drEmptyTagList);
            dtEmptyTagList.AcceptChanges();
            //dsFinalTagsList = dtEmptyTagList;
            gvIssueTags.Enabled = false;
            litTotal.Text = "0";
            gvIssueTags.DataSource = dtEmptyTagList;
            gvIssueTags.DataBind();
            DisableAllButton();
            Session["dtIssueTagsList"] = null;
            Session["ListTagID"] = null;
            Session["AllTags"] = null;
            return dtEmptyTagList;

        }

        private DataTable EmptyGridDataSourceAfterFilter()
        {
            DataTable dtEmptyTagList = new DataTable("dtEmptyTagList");

            dtEmptyTagList.Columns.Add("ID");
            dtEmptyTagList.Columns.Add("TagRFID");
            dtEmptyTagList.Columns.Add("TruckID");
            dtEmptyTagList.Columns.Add("ProponentID");
            dtEmptyTagList.Columns.Add("ProponentName");
            dtEmptyTagList.Columns.Add("HopperID");
            dtEmptyTagList.Columns.Add("HopperName");
            dtEmptyTagList.Columns.Add("Assigned");


            // Add new row of data to datatable
            DataRow drEmptyTagList = dtEmptyTagList.NewRow();

            //Adding the created data row to the datatable
            dtEmptyTagList.Rows.Add(drEmptyTagList);
            dtEmptyTagList.AcceptChanges();
            //dsFinalTagsList = dtEmptyTagList;
            gvIssueTags.Enabled = false;
            litTotal.Text = "0";
            gvIssueTags.DataSource = dtEmptyTagList;
            gvIssueTags.DataBind();
           // DisableAllButton();
           // Session["dtIssueTagsList"] = null;
           // Session["ListTagID"] = null;
          //  Session["AllTags"] = null;
            return dtEmptyTagList;

        }

        private void DisableAllButton()
        {
            btnRemove.Enabled = false;
            btnDispatched.Enabled = false;
        }
        private void EnableAllButton()
        {
            btnRemove.Enabled = true;
            btnDispatched.Enabled = true;
        }

        private ArrayList CreateArrayList(List<string> lstRFIDTag)
        {
            // Create an array list for creating CSV of all RFID tags
            ArrayList lstTagCsv = new ArrayList();
            StringBuilder strLstTagIDs = new StringBuilder("'-1'");
            ArrayList lstTagBattery = new ArrayList();
            StringBuilder strTagBattery = new StringBuilder("-1");

            //if (Session["ListTagID"] != null)
            //{
            //    lstTagCsv = (ArrayList)Session["ListTagID"];
            //    lstTagBattery = (ArrayList)Session["lstTagBattery"];
            //}

            foreach (string objRFIDTag in lstRFIDTag)
            {
                //To set all tag ids in a single string
                if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
                {
                    lstTagCsv.Add(strLstTagIDs);
                    strLstTagIDs = new StringBuilder("'-1'");
                }
                else
                {
                    strLstTagIDs.Append("," + "'" + objRFIDTag + "'");
                }
                if (strTagBattery.Length >= 2800) // To check the length value supported by SQL to insert record properly
                {
                    lstTagBattery.Add(strTagBattery);
                    strTagBattery = new StringBuilder("-1");
                }
                else
                {
                    //strTagBattery.Append("," + Convert.ToString(Convert.ToInt32(objRFIDTag.BatteryStatus)));
                    strTagBattery.Append("," + 0);
                }

            }
            if (strLstTagIDs.Length >= 5)
            {
                lstTagCsv.Add(strLstTagIDs);
                lstTagBattery.Add(strTagBattery);
            }
            Session["lstTagBattery"] = lstTagBattery;
            Session["ListTagID"] = lstTagCsv;
            return lstTagCsv;
        }

        //private ArrayList CreateArrayList(List<RFIDTag> lstRFIDTag)
        //{
        //    // Create an array list for creating CSV of all RFID tags
        //    ArrayList lstTagCsv = new ArrayList();
        //    StringBuilder strLstTagIDs = new StringBuilder("'-1'");
        //    ArrayList lstTagBattery = new ArrayList();
        //    StringBuilder strTagBattery = new StringBuilder("-1");

        //    if (Session["ListTagID"] != null)
        //    {
        //        lstTagCsv = (ArrayList)Session["ListTagID"];
        //        lstTagBattery = (ArrayList)Session["lstTagBattery"];
        //    }

        //    foreach (RFIDTag objRFIDTag in lstRFIDTag)
        //    {
        //        //To set all tag ids in a single string
        //        if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
        //        {
        //            lstTagCsv.Add(strLstTagIDs);
        //            strLstTagIDs = new StringBuilder("'-1'");
        //        }
        //        else
        //        {
        //            strLstTagIDs.Append("," + "'" + objRFIDTag.TagID + "'");
        //        }
        //        if (strTagBattery.Length >= 2800) // To check the length value supported by SQL to insert record properly
        //        {
        //            lstTagBattery.Add(strTagBattery);
        //            strTagBattery = new StringBuilder("-1");
        //        }
        //        else
        //        {
        //            strTagBattery.Append("," + Convert.ToString(Convert.ToInt32(objRFIDTag.BatteryStatus)));
        //        }

        //    }
        //    if (strLstTagIDs.Length >= 5)
        //    {
        //        lstTagCsv.Add(strLstTagIDs);
        //        lstTagBattery.Add(strTagBattery);
        //    }
        //    Session["lstTagBattery"] = lstTagBattery;
        //    Session["ListTagID"] = lstTagCsv;
        //    return lstTagCsv;
        //}

        /// <summary>
        /// Remove duplicate Tags from list
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable RemoveDuplicateTagsFromList(DataTable dt)
        {
            bool flgTagRemoved = false;
            if (dt != null)
            {
                if (dt.Rows.Count != 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // Remove duplicate Tags from list
                        if ((dr["TagRFID"] != null))
                        {
                            int TagIDcount = (dt.Select("TagRFID=" + "'" + Convert.ToString(dr["TagRFID"]) + "'")).Count();
                            if ((TagIDcount > 1))
                            {
                                dr.Delete();
                                flgTagRemoved = true;
                            }
                        }
                    }
                    dt.AcceptChanges();
                }
            }
            return dt;
        }

        private void ClearAllSessions()
        {
            Session["ListTagID"] = null;
            Session["dtIssueTagsList"] = null;
            Session["ListTagID"] = null;
            Session["AllTags"] = null;
        }

        protected void btnReturnedTags_Click(object sender, EventArgs e)
        {
            GetTagsToDispatch();
        }

        void GetTagsToDispatch()
        {

            try
            {
                divMessage.Visible = false;
                EnableAllButton();
                gvIssueTags.Enabled = true;

                DataTable dtIssueTagsList = new DataTable();
                //DataTable dtIssueTagsList = null;

                dtIssueTagsList = DBConnector.Common.DBConnectorUtility.GetTagsToDispatch();

                if (dtIssueTagsList != null && dtIssueTagsList.Rows.Count > 0)
                {
                    ArrayList lstTagCsv = new ArrayList();
                    StringBuilder strLstTagIDs = new StringBuilder("'-1'");
                    ArrayList lstTagBattery = new ArrayList();
                    StringBuilder strTagBattery = new StringBuilder("-1");

                    if (Session["ListTagID"] != null)
                    {
                        lstTagCsv = (ArrayList)Session["ListTagID"];
                        lstTagBattery = (ArrayList)Session["lstTagBattery"];
                    }

                    List<string> ListSessionTagID  ;

                    if (Session["AllTags"] != null)
                    {
                        ListSessionTagID = (List<string>)Session["AllTags"];                        
                    }
                    else
                    {
                        ListSessionTagID = new List<string>();
                    }


                    string tagID;

                    foreach (DataRow dr in dtIssueTagsList.Rows)
                    {
                        if (dr["TagRFID"] != DBNull.Value && dr["TagRFID"].ToString() != "")
                        {

                            tagID = dr["TagRFID"].ToString();
                            if (!ListSessionTagID.Contains(tagID))
                            {
                                ListSessionTagID.Add(tagID);

                                if (strLstTagIDs.Length >= 7500) // to check the length value supported by SQL to insert record properly
                                {
                                    lstTagCsv.Add(strLstTagIDs);
                                    strLstTagIDs = new StringBuilder("'-1'");
                                }
                                else
                                {
                                    strLstTagIDs.Append("," + "'" + tagID + "'");
                                }
                                if (strTagBattery.Length >= 2800) // To check the length value supported by SQL to insert record properly
                                {
                                    lstTagBattery.Add(strTagBattery);
                                    strTagBattery = new StringBuilder("-1");
                                }
                                else
                                {
                                    strTagBattery.Append("," + 0);
                                }

                            }
                        }
                    }

                    Session["AllTags"] = ListSessionTagID;
               
                    if (strLstTagIDs.Length >= 5)
                    {
                        lstTagCsv.Add(strLstTagIDs);
                        lstTagBattery.Add(strTagBattery);
                    }

                    Session["lstTagBattery"] = lstTagBattery;
                    Session["ListTagID"] = lstTagCsv;
                }

                if (Session["dtIssueTagsList"] != null)
                {
                    DataTable dtSessionTags = (DataTable)Session["dtIssueTagsList"];
                    dtIssueTagsList.Merge(dtSessionTags);
                    litTotal.Text = Convert.ToString(dtIssueTagsList.Rows.Count);
                }
              

                // Bind final datatable of all tags detail to gridview
                if (dtIssueTagsList != null && dtIssueTagsList.Rows.Count > 0)
                {
                    dtIssueTagsList = RemoveDuplicateTagsFromList(dtIssueTagsList);
                    litTotal.Text = Convert.ToString(dtIssueTagsList.Rows.Count);
                    gvIssueTags.DataSource = dtIssueTagsList;
                    gvIssueTags.DataBind();
                }
                else
                {
                    litTotal.Text = "0";
                    litMsg.Text = "No tag(s) found for issue OR may be the tag(s) have running transaction";
                    EmptyGridDtatSource();
                }

                ddlPad.SelectedValue = "0";
                ddlProponent.SelectedValue = "0";

                Session["dtIssueTagsList"] = dtIssueTagsList;

                //ddlReader.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                litExMsg.Text = ex.Message;
                DisableAllButton();
            }
        }

        protected void gvIssueTags_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //Find the checkbox control in header and add an attribute
                ((CheckBox)e.Row.FindControl("chkHeader")).Attributes.Add("onclick", "javascript:SelectAll('" +
                        ((CheckBox)e.Row.FindControl("chkHeader")).ClientID + "')");
            }
        }

        /// <summary>
        /// Populate Hoppers/Pad dropdown list
        /// </summary>
        private void FillHoppers()
        {
            try
            {              
                DataTable dtHoppers = Hoppers.GetAllHoppers();
                if (dtHoppers != null)
                {
                    if (dtHoppers.Rows.Count != 0)
                    {
                        ddlPad.DataSource = dtHoppers;
                        ddlPad.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Fill proponents by hopper id
        /// </summary>
        private void FillProponents()
        {
            try
            {
                DataTable dtProponent = Hoppers.GetAllProponents();
                if (dtProponent != null)
                {
                    if (dtProponent.Rows.Count != 0)
                    {
                        ddlProponent.DataSource = dtProponent;
                        ddlProponent.DataBind();
                    }
                }
               
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Selected index changed event of Pad Drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                BindFilteredGrid();
            }
            catch (Exception)
            {
               // throw;
                EmptyGridDtatSource();
            }
        }

        /// <summary>
        /// Selected index changed event of Proponent Drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlProponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindFilteredGrid();
               
            }
            catch (Exception)
            {
                throw;
            }
        }

        void BindFilteredGrid()
        {
            DataTable dt;
            dt = (DataTable)Session["dtIssueTagsList"];

            if (dt == null)
            {
                EmptyGridDtatSource();
                return;
            }

            DataTable dtCopy = dt.Copy();

            DataView dv = new DataView(dtCopy);

            string rowFilter = "";
            string joinner = "";

            if ((Convert.ToInt32(ddlProponent.SelectedValue) != 0))
            {
                rowFilter = "ProponentID = " + Convert.ToInt32(ddlProponent.SelectedValue);
                joinner = " AND ";
            }

            if ((Convert.ToInt32(ddlPad.SelectedValue) != 0))
            {
                rowFilter = rowFilter +  joinner + "HopperID = " + Convert.ToInt32(ddlPad.SelectedValue);
            }

            if (rowFilter != "")
            {
                dv.RowFilter = rowFilter;
            }

            if (dv.Count > 0)
            {
                gvIssueTags.DataSource = dv;
                gvIssueTags.DataBind();
                gvIssueTags.Enabled = true;
                EnableAllButton();
               
            }
            else
            {
                EmptyGridDataSourceAfterFilter();
               
            }

            litTotal.Text = dv.Count.ToString() + "/" + dv.Table.Rows.Count.ToString();
        }
    }

}
