<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="genericerrorpage.aspx.cs" Inherits="Ramp.UI.genericerrorpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <%-- <div style="text-align:center">--%>
    <div id="wrap-page">
        <div id="mid_section">
            <p>
                <br />
            </p>
            <table width="75%" border="0" cellspacing="0" cellpadding="0" style="cursor: help; text-align:center" >
                <tr>
                    <td>
                    </td>
                    <td>
                        
                            <b><span>APPLICATION ERROR</span></b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <p align="center">
                We are sorry, but an unhandled error occured on the server.
                <%-- <br />The Server Administrator has been notified and the error logged.--%>
            </p>
            <p align="center">
                <asp:Label ID="lblErrorMsg" runat="server" Font-Bold = "true"></asp:Label>
            </p>
            <p align="center">
                Please continue on by either clicking the <a href="javascript:history.go(-1);">back</a>
                button and retrying your request or by returning to the <a href="Default.aspx">home
                    page</a>.
            </p>
            <br />
        </div>
    </div>
</asp:Content>
