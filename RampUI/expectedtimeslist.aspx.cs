using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ramp.MiddlewareController.Common;
using Ramp.DBConnector.Adapter;
using Ramp.DBConnector.Common;
using System.Collections;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.UI
{
    public partial class expectedtimeslist : System.Web.UI.Page
    {
        List<IMarkers> lstMarkers = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session["dsExpectedTimes"] = null;
                ViewState["ModifiedDate"] = null;
                lstMarkers = DBConnector.Adapter.Markers.GetMarkersAtLocation(Location.HOPPER);
                IMarkers objMarker = new DBConnector.Adapter.Markers();
                objMarker.ID = 0;
                objMarker.MarkerLoopID = "-Select-";
                objMarker.Location = (int)Location.HOPPER;
                lstMarkers.Insert(0, objMarker);
                Session["Markers"] = lstMarkers;
                FillGrids();
            }
            else
            {
                if (Session["Markers"] == null)
                {
                    lstMarkers = DBConnector.Adapter.Markers.GetMarkersAtLocation(Location.HOPPER);
                    IMarkers objMarker = new DBConnector.Adapter.Markers();
                    objMarker.ID = 0;
                    objMarker.MarkerLoopID = "-Select-";
                    objMarker.Location = (int)Location.HOPPER;
                    lstMarkers.Insert(0, objMarker);
                    Session["Markers"] = lstMarkers;
                }
                else
                {
                    lstMarkers = (List<IMarkers>)Session["Markers"];
                }
            }
           
        }
        /// <summary>
        /// Fill grids 
        /// </summary>
        private void FillGrids()
        {
            try
            {
                DataSet dsExpectedTimes = DBConnectorUtility.GetALLExpectedTimeValues();

                //odsMarkers.SelectParameters["loc"].DefaultValue = Convert.ToInt32(Location.HOPPER).ToString();

                Session["dsExpectedTimes"] = dsExpectedTimes;
                if (dsExpectedTimes != null)
                {
                    if (dsExpectedTimes.Tables[0].Rows.Count != 0)
                    {
                        fvExpectedConfig.DefaultMode = FormViewMode.Edit;
                        fvExpectedConfig.DataSource = dsExpectedTimes.Tables[0];
                        fvExpectedConfig.DataBind();
                    }

                    if (dsExpectedTimes.Tables[1] != null)
                    {
                        if (dsExpectedTimes.Tables[1].Rows.Count != 0)
                        {
                            gvHopperExpectedTime.DataSource = dsExpectedTimes.Tables[1];
                            gvHopperExpectedTime.DataBind();
                        }
                    }


                }




                DataSet ds = DBConnectorUtility.GetConfigurationValues();
                gvConfiguration.DataSource = ds.Tables[0];
                gvConfiguration.DataBind();


            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        /// <summary>
        /// Convert Column to rows and rows to column
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable Transpose(DataTable dt)
        {
            try
            {
                DataTable dtNew = new DataTable();

                //adding columns	
                for (int i = 0; i <= dt.Rows.Count; i++)
                {
                    dtNew.Columns.Add(i.ToString());
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dtNew.Columns[i + 1].ColumnName = dt.Rows[i].ItemArray[0].ToString();
                }

                //Adding Row Data
                for (int k = 1; k < dt.Columns.Count; k++)
                {
                    DataRow r = dtNew.NewRow();
                    r[0] = dt.Columns[k].ToString();
                    for (int j = 1; j <= dt.Rows.Count; j++)
                        r[j] = dt.Rows[j - 1][k];
                    dtNew.Rows.Add(r);
                }

                return dtNew;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save the changes related to Expected Times 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                // Update data through Formview 

                TextBox txtRQ = (TextBox)fvExpectedConfig.Row.FindControl("txtRQ");
                TextBox txtQE1 = (TextBox)fvExpectedConfig.Row.FindControl("txtQE1");
                TextBox txtE1E2 = (TextBox)fvExpectedConfig.Row.FindControl("txtE1E2");
                TextBox txtQTPT = (TextBox)fvExpectedConfig.Row.FindControl("txtQTPT");
                TextBox txtUT = (TextBox)fvExpectedConfig.Row.FindControl("txtUT");
                TextBox txtXR = (TextBox)fvExpectedConfig.Row.FindControl("txtXR");
                TextBox txtTWR = (TextBox)fvExpectedConfig.Row.FindControl("txtTWR");
                TextBox txtXE1 = (TextBox)fvExpectedConfig.Row.FindControl("txtXE1");
                TextBox txtTWE1 = (TextBox)fvExpectedConfig.Row.FindControl("txtTWE1");
                TextBox txtSU = (TextBox)fvExpectedConfig.Row.FindControl("txtSU");
                TextBox txtSD = (TextBox)fvExpectedConfig.Row.FindControl("txtSD");
                TextBox txtRT = (TextBox)fvExpectedConfig.Row.FindControl("txtRT");
                TextBox txtTQM = (TextBox)fvExpectedConfig.Row.FindControl("txtTQM");
                TextBox txtPrecision = (TextBox)fvExpectedConfig.Row.FindControl("txtPrecision");
                TextBox txtTRF = (TextBox)fvExpectedConfig.Row.FindControl("txtTRF");
                TextBox txtMD = (TextBox)fvExpectedConfig.Row.FindControl("txtMD");

                // Compare Expected Times Configuration and System configuration new values with previous values
                bool flgExpectedTimesUpdate = false;
                bool flgHopperExpectedTimesUpdate = false;
                if (Session["dsExpectedTimes"] != null)
                {
                    DataSet dsExpectedTimes = (DataSet)Session["dsExpectedTimes"];
                    if (dsExpectedTimes.Tables.Count != 0)
                    {
                        if (dsExpectedTimes.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsExpectedTimes.Tables[0].Rows)
                            {
                                ViewState["ModifiedDate"] = Convert.ToDateTime(dr["ModifiedDate"]);

                                if (Convert.ToString(dr["REDS_Queue"]) != txtRQ.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["Queue_Ent1"]) != txtQE1.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["Ent1_Ent2"]) != txtE1E2.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["QueueTimePerTruck"]) != txtQTPT.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["Unloading"]) != txtUT.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["StartUp"]) != txtSU.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["ShutDown"]) != txtSD.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["TruckCallResponse"]) != txtRT.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["TruckQueueMax"]) != txtTQM.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }

                                if (Convert.ToString(dr["Exit_REDS"]) != txtXR.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["TruckWash_REDS"]) != txtTWR.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["Precision"]) != txtPrecision.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["TagReadFrequency"]) != txtTRF.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["MaxDelay"]) != txtMD.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["Exit_Ent1"]) != txtXE1.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                                if (Convert.ToString(dr["TruckWash_Ent1"]) != txtTWE1.Text)
                                {
                                    flgExpectedTimesUpdate = true;
                                    break;
                                }
                            }
                        }
                    }

                    DataRow[] drr = null;

                    if (flgExpectedTimesUpdate == false)
                    {
                        if (dsExpectedTimes.Tables.Count != 0)
                        {
                            if (dsExpectedTimes.Tables[1].Rows.Count > 0)
                            {

                                //foreach (DataRow dr in dsExpectedTimes.Tables[1].Rows)
                                //{
                                foreach (GridViewRow gvRow in gvHopperExpectedTime.Rows)
                                {
                                    Label lblEntryExpectedTime = (Label)gvRow.FindControl("lblEntryExpectedTime");
                                    TextBox txtEntryExpectedTime = (TextBox)gvRow.FindControl("txtEntryExpectedTime");
                                    TextBox txtExpectedTimeTW = (TextBox)gvRow.FindControl("txtExpectedTimeTW");
                                    string ID = Convert.ToString(lblEntryExpectedTime.Text.Trim());
                                    string EntryExpectedTime = Convert.ToString(txtEntryExpectedTime.Text.Trim());
                                    string TimeToTW = Convert.ToString(txtExpectedTimeTW.Text.Trim());

                                    DropDownList ddlMarkers = (DropDownList)gvRow.FindControl("ddlLoopID");

                                    int markerID = 0;

                                    if (ddlMarkers != null && ddlMarkers.SelectedValue != "")
                                    {
                                        markerID = Convert.ToInt32(ddlMarkers.SelectedValue);
                                    }

                                    drr = dsExpectedTimes.Tables[1].Select("ID = " + Convert.ToInt32(ID));

                                    if (drr != null && drr.Length > 0)
                                    {
                                        if (Convert.ToString(drr[0]["TimeFromEnt2"]) != EntryExpectedTime)
                                        {
                                            flgHopperExpectedTimesUpdate = true;
                                            break;
                                        }
                                        if (Convert.ToString(drr[0]["TimeToTW"]) != TimeToTW)
                                        {
                                            flgHopperExpectedTimesUpdate = true;
                                            break;
                                        }
                                        if (Convert.ToInt32(drr[0]["MarkerID"]) != markerID)
                                        {
                                            flgHopperExpectedTimesUpdate = true;
                                            break;
                                        }
                                         
                                    }
                                }
                                //if (flgHopperExpectedTimesUpdate == true)
                                //{
                                //    break;
                                //}
                                //}
                            }
                        }
                    }
                }
                if ((flgHopperExpectedTimesUpdate == true) || (flgExpectedTimesUpdate == true))
                {
                    //if (ViewState["ModifiedDate"] != null)
                    //{
                    //    if (Convert.ToDateTime(ViewState["ModifiedDate"]).ToShortDateString() != (DateTime.Now.Date).ToShortDateString())
                    //    {
                    DBConnectorUtility.ArchiveExpectedAndHopperTime();
                    //    }
                    //}
                    // Hopper Entry Expected Time Grid update data
                    if (gvHopperExpectedTime.Rows.Count > 0)
                    {
                        Hoppers objHopper = new Hoppers();
                        foreach (GridViewRow gvRow in gvHopperExpectedTime.Rows)
                        {
                            Label lblEntryExpectedTime = (Label)gvRow.FindControl("lblEntryExpectedTime");
                            TextBox txtEntryExpectedTime = (TextBox)gvRow.FindControl("txtEntryExpectedTime");
                            TextBox txtExpectedTimeTW = (TextBox)gvRow.FindControl("txtExpectedTimeTW");

                            DropDownList ddlMarkers = (DropDownList)gvRow.FindControl("ddlLoopID");

                            string ID = Convert.ToString(lblEntryExpectedTime.Text.Trim());
                            string EntryExpectedTime = Convert.ToString(txtEntryExpectedTime.Text.Trim());
                            string TimeToTW = Convert.ToString(txtExpectedTimeTW.Text.Trim());

                            int markerID = 0;

                            if (ddlMarkers != null && ddlMarkers.SelectedValue != "")
                            {
                                markerID = Convert.ToInt32(ddlMarkers.SelectedValue);
                            }

                            objHopper.UpdateExpectedTimeEntry2(Convert.ToString(ID), Convert.ToString(EntryExpectedTime), Convert.ToString(TimeToTW), markerID);
                        }
                    }
                    // Expected Time update
                    DBConnectorUtility.UpdateExpectedTimeValues(Convert.ToString(fvExpectedConfig.DataKey.Value), Convert.ToString(txtRQ.Text.Trim()), Convert.ToString(txtQE1.Text.Trim()), Convert.ToString(txtE1E2.Text.Trim()),
                          Convert.ToString(txtQTPT.Text.Trim()), Convert.ToString(txtUT.Text.Trim()), Convert.ToString(txtSU.Text.Trim()), Convert.ToString(txtSD.Text.Trim()),
                          Convert.ToString(txtRT.Text.Trim()), Convert.ToString(txtTQM.Text.Trim()), Convert.ToString(txtXR.Text.Trim()),
                          Convert.ToString(txtTWR.Text.Trim()), Convert.ToString(txtPrecision.Text.Trim()), Convert.ToString(txtTRF.Text.Trim()),
                          Convert.ToString(txtMD.Text.Trim()), Convert.ToString(txtXE1.Text.Trim()), Convert.ToString(txtTWE1.Text.Trim()));
                }


                //..............................

                int VMSDeviceLocation = -1;

                foreach (GridViewRow gvRow in gvConfiguration.Rows)
                {
                    HiddenField hdnID = (HiddenField)gvRow.FindControl("hdnID");
                    TextBox txtConfiguration = (TextBox)gvRow.FindControl("txtConfiguration");

                    Int32 ID = Convert.ToInt32(hdnID.Value);
                    string Value = Convert.ToString(txtConfiguration.Text.Trim());

                    HiddenField hdnField = (HiddenField)gvRow.FindControl("hdnField");

                    HiddenField hdnParameterType = (HiddenField)gvRow.FindControl("hdnParameterType"); 

                    if (hdnField.Value == "Default Message")
                    {
                        if (hdnParameterType.Value == "REDS VMS")
                        {
                            VMSDeviceLocation = 1;
                        }
                        else if (hdnParameterType.Value == "")
                        { 

                        }

                    }
                    else
                    {
                        VMSDeviceLocation = -1;
                    }
                    DBConnectorUtility.UpdateConfigurations(ID, Value, VMSDeviceLocation);

                }
                //..............................


                Session["dsExpectedTimes"] = null;
                ViewState["ModifiedDate"] = null;
                FillGrids();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Session["dsExpectedTimes"] = null;
                ViewState["ModifiedDate"] = null;
                FillGrids();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvConfiguration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnID = (HiddenField)e.Row.FindControl("hdnID");
                Label lblUnit = (Label)e.Row.FindControl("lblUnit");

                ConfigurationValues val;

                if (hdnID != null)
                {
                    try
                    {
                        val = (ConfigurationValues)Convert.ToInt32(hdnID.Value);
                        if (val == ConfigurationValues.SolidLedInterval || val == ConfigurationValues.FlashingLedInterval)
                        {
                            lblUnit.Text = "ms"; 

                        }                    
                        else if (val == ConfigurationValues.CurrentPageRefresh || val == ConfigurationValues.EntryPageRefresh)
                        {
                            lblUnit.Text = "sec";
                        }
                        else //if (val == ConfigurationValues.FlashingLedTotalBlink || val == ConfigurationValues.SolidLedTotalBlink)
                        {
                            HiddenField hdnField = (HiddenField)e.Row.FindControl("hdnField");
                            if(hdnField.Value == "Interval")
                                lblUnit.Text = "sec";
                            else if (hdnField.Value == "Default Message")
                            {
                                TextBox txt = (TextBox)e.Row.FindControl("txtConfiguration");

                                RangeValidator rngValConfiguration = (RangeValidator)e.Row.FindControl("rngValConfiguration");
                                rngValConfiguration.Enabled = false;
                                txt.TextMode = TextBoxMode.MultiLine;
                                txt.Height = 50;
                                txt.Width = 150;
                                txt.MaxLength = 100;
                                lblUnit.Text = "";
                            }
                            else
                            {
                                lblUnit.Text = "";
                            }
                        }

                    }
                    catch
                    {
                    }
                }
                
            }
        }

        protected void gvHopperExpectedTime_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlLoopID = (DropDownList)e.Row.FindControl("ddlLoopID");
                HiddenField hdnLoopID = (HiddenField)e.Row.FindControl("hdnLoopID");
                if (ddlLoopID != null)
                {
                    ddlLoopID.DataSource = lstMarkers;
                    ddlLoopID.DataBind();
                }
                if (hdnLoopID != null && hdnLoopID.Value != "")
                {
                    ddlLoopID.SelectedValue = hdnLoopID.Value;
                }
            }
        }
    }
}
