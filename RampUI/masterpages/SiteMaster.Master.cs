using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Common;
using Ramp.UI.common;

namespace Ramp.UI.masterpages
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        List<String> pages;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoggedInUser"] == null)
                Response.Redirect("~/login.aspx");
            else
            {
                IUsers objUser = (IUsers)Session["LoggedInUser"];
                lblUserName.InnerText = objUser.UserName;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                // Redirect user to the specific page according to his/her security level id get form the loggedin user session
                if (Session["LoggedInUser"] != null)
                {
                   // lblVersion.Text = Utility.VersionNo;
                  

                    object value = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    if (value != null)
                    {
                        lblVersion.Text = "Version " + Convert.ToString(value);
                    }
                    else
                    {
                        lblVersion.Text = Utility.VersionNo;
                    }

                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Administrator))
                    {
                        // Only for testing need to change the URL when connfirmed where to move

                        // Show 
                        tagmenu.Visible = true;
                        lnkOffline.Visible = true;
                        configmenu.Visible = true;
                    }
                    else if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        // Only for testing need to change the URL when connfirmed where to move
                       
                        lnkUnAssignTag.Visible = false;
                        lnkOffline.Visible = false;
                        // Hide 
                        tagmenu.Visible = false;
                        usermenu.Visible = false;                       
                        configmenu.Visible = false;

                        if (Session["RestrictedPages"] == null)
                        {
                            pages = new List<string>() { "offlinetransactions.aspx", "expectedtimeslist.aspx", "addtag.aspx", "assigntag.aspx", "issuetag.aspx", "unassigntag.aspx", "adduser.aspx", "viewusers.aspx", "hardwareconfigurationpage.aspx"};

                            Session["RestrictedPages"] = pages;
                        }
                        else
                        {
                            pages = (List<string>)Session["RestrictedPages"];
                        }

                        bool pageFound = false;

                        foreach (string page in pages)
                        {
                            if (Request.Url.AbsoluteUri.ToLower().Contains(page))
                            {
                                pageFound = true;
                                break;
                            }
                        }

                        if (pageFound)
                        {
                            Response.Redirect("~/Default.aspx");
                        }

                    }
                    else if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.EntryGate))
                    {
                        // Only for testing need to change the URL when connfirmed where to move
                        // Hide 

                        lnkUnAssignTag.Visible = false;                                      
                        lnkOffline.Visible = false;
                        lnkCurrentTraffic.Visible = false;

                        configmenu.Visible = false;                       
                        reportmenu.Visible = false;
                        usermenu.Visible = false;
                        tagmenu.Visible = false;

                        if (Session["RestrictedPages"] == null)
                        {
                            pages = new List<string>() { "offlinetransactions.aspx", "expectedtimeslist.aspx", "addtag.aspx", "assigntag.aspx", "issuetag.aspx", "unassigntag.aspx", "adduser.aspx", "viewusers.aspx", "hardwareconfigurationpage.aspx", "assignunassigntagreport.aspx","expectedtimereport.aspx","systemstatusreport.aspx","currenttraffic.aspx" };

                            Session["RestrictedPages"] = pages;
                        }
                        else
                        {
                            pages = (List<string>)Session["RestrictedPages"];
                        }

                        bool pageFound = false;

                        foreach (string page in pages)
                        {
                            if (Request.Url.AbsoluteUri.ToLower().Contains(page))
                            {
                                pageFound = true;
                                break;
                            }
                        }

                        if (pageFound)
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                    }

                    
                }
                else
                {
                    // only added the page for testing need to change the URL when connfirmed where to move
                    Response.Redirect("~/login.aspx");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
    }
}
