﻿<%@ Page Title="Users" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="viewusers.aspx.cs" Inherits="Ramp.UI.users.viewusers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
             <center><b>VIEW USER</b></center><br />
            <table width="80%">
                <tr>
                    <td>
                        <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" Width="100%"
                            DataSourceID="ODSUsers" DataKeyNames="ID" OnRowCommand="gvUsers_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                              <%--  <asp:BoundField DataField="UserName" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />--%>
                                <asp:BoundField DataField="UserID" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" />
                             <%--   <asp:BoundField DataField="Password" HeaderText="Password" HeaderStyle-HorizontalAlign="Left" />--%>
                                <asp:BoundField DataField="SecurityGroup" HeaderText="Security Group" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="EmailID" HeaderText="Email" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnEdit" runat="server" CommandArgument='<%#Eval ("ID")%>' CommandName="EditUser"
                                            Text="Edit" CssClass="but" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnAddUsers" runat="server" Text="Add New User" CssClass="but" OnClick="btnAddUsers_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:ObjectDataSource ID="ODSUsers" runat="server" SelectMethod="GetAllUsers" TypeName="Ramp.DBConnector.Adapter.Users">
    </asp:ObjectDataSource>
</asp:Content>
