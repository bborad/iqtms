using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using DBModule = Ramp.DBConnector.Adapter;
using System.Web.Security;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Common;

namespace Ramp.UI.users
{
    [ScriptService]
    public partial class adduser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["LoggedInUser"] != null)
                {
                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtUserID.Text.Trim() != "" && txtUserName.Text.Trim() != "" && txtPassword.Text.Trim() != "" && txtEmailID.Text.Trim() != "")
            {
                DBModule.Users objUser = new Ramp.DBConnector.Adapter.Users();
                objUser.UserID = txtUserID.Text;
                objUser.UserName = txtUserName.Text;
                objUser.EmailID = txtEmailID.Text;
                objUser.Password = txtPassword.Text;
                objUser.SecurityGroupID = Convert.ToInt32(ddlSecurityGroups.SelectedValue);
                int result = objUser.AddUser();
                if (result > 0)
                {
                    lblMsg.Text = "Record added successfully.";
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else if (result == -1)
                {
                    lblMsg.Text = "Username already exist.";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblMsg.Text = "Error in adding record.";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {

            }


        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtConPassword.Text = "";
            txtEmailID.Text = "";
            txtPassword.Text = "";
            txtUserID.Text = "";
            txtUserName.Text = "";
        }

        [WebMethod]
        public static int CheckUserName(string userid)
        {
            //System.Threading.Thread.Sleep(5000);
            try
            {
                if (DBModule.Users.IsDuplicateUserName(userid))
                {
                    return 1;  // if user exist                    
                }

                return 0;
            }
            catch
            {
            }
            return 0;
        }
    }
}
