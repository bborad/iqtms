﻿<%@ Page Title="Add User" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="adduser.aspx.cs" Inherits="Ramp.UI.users.adduser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <asp:ScriptManager ID="scrptmngr" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div id="wrap-page">
        <div id="mid_section">
           <center><b>ADD USER</b></center><br />
            <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture1">
                <tr>
                  <td style = "width:20%">
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        Name :
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" class = "txtBoxes"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtUserName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        Email :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailID" runat="server" class = "txtBoxes"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmailID" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtEmailID"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        Security :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSecurityGroups" runat="server" DataTextField="Description"
                            DataValueField="ID" DataSourceID="ODSSecurityGroup" class = "txtBoxes">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvddlSecurityGroups" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="ddlSecurityGroups" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        User Name :
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserID" runat="server" onblur="checkForusername()" class = "txtBoxes"></asp:TextBox>
                        <span id="divProgress" style="display: none; width: 20px; height: 20px; border: 0px solid #000000;
                            background-color: #ffffff; vertical-align: middle;" visible="false">
                            <img alt="wait" src="../images/progress.gif" width="15px" height="15px" style="border: none" />
                        </span>
                        <asp:RequiredFieldValidator ID="rfvUserID" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtUserID"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvUsedID" runat="server" ClientValidationFunction="CheckValidUserFlag"
                            ErrorMessage="" ForeColor="Red" Display="None" ValidationGroup="VGAddUser"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        Password :
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class = "txtBoxes"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td align="right">
                        Confirm Password :
                    </td>
                    <td>
                        <asp:TextBox ID="txtConPassword" runat="server" TextMode="Password" class = "txtBoxes"></asp:TextBox>
                        <asp:CompareValidator ID="cvConPassword" runat="server" ControlToCompare="txtPassword"
                            ControlToValidate="txtConPassword" ErrorMessage="*Check the password" ValidationGroup="VGAddUser"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                   <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CommandName="Insert" CssClass="but"
                            OnClick="btnAdd_Click" ValidationGroup="VGAddUser" CausesValidation="true" />&nbsp;
                        <asp:Button ID="btnClear" runat="server" Text="Clear" CommandName="Clear" CssClass="but"
                            OnClick="btnClear_Click" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:ObjectDataSource ID="ODSSecurityGroup" runat="server" SelectMethod="GetAllSecurityGroup"
        TypeName="Ramp.DBConnector.Common.DBConnectorUtility"></asp:ObjectDataSource>

    <script language="javascript" type="text/javascript">
        var validuserFlag;
        function checkForusername() {

            var txtuser = document.getElementById('<%= txtUserID.ClientID%>');
            var lblMsg = document.getElementById('<%= lblMsg.ClientID%>');
            var divProgress = document.getElementById('divProgress');

            if (txtuser != null && txtuser.value != '') {
                lblMsg.innerHTML = '';
                divProgress.visible = true;
                divProgress.style.display = "inline";

                PageMethods.CheckUserName(txtuser.value, CheckUsername_callback);
            }
        }

        function CheckUsername_callback(rValue) {

            var divProgress = document.getElementById('divProgress');
            divProgress.style.display = "none";
            divProgress.visible = false;
            var lblMsg = document.getElementById('<%= lblMsg.ClientID%>');

            if (rValue == 1) {
                lblMsg.innerHTML = 'Please choose different user name.';
                validuserFlag = 0;
            }
            else {
                lblMsg.innerHTML = '';
                validuserFlag = 1;
            }
        }

        function CheckValidUserFlag(source, argument) {

            if (validuserFlag == 0)
                argument.IsValid = false;
            else
                argument.IsValid = true;
        }

    </script>

</asp:Content>
