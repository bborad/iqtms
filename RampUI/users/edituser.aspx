﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edituser.aspx.cs" Inherits="Ramp.UI.users.edituser"
    MasterPageFile="~/masterpages/SiteMaster.Master" Title="Edit User" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
             <center><b>EDIT USER</b></center><br />
            <table width="60%">
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblMsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Name :
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtUserName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                   <td align="right">
                        Email :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmailID" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="txtEmailID"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Security :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSecurityGroups" runat="server" DataTextField="Description"
                            DataValueField="ID" DataSourceID="ODSSecurityGroup">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvddlSecurityGroups" runat="server" ErrorMessage="* Required"
                            ValidationGroup="VGAddUser" ControlToValidate="ddlSecurityGroups" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        User Name :
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                    </td>
                </tr>                  
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="but"
                            OnClick="btnUpdate_Click" ValidationGroup="VGAddUser" CausesValidation="true" />&nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="cancel" CssClass="but"
                            OnClick="btnCancel_Click" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:ObjectDataSource ID="ODSSecurityGroup" runat="server" SelectMethod="GetAllSecurityGroup"
        TypeName="Ramp.DBConnector.Common.DBConnectorUtility"></asp:ObjectDataSource>
</asp:Content>
