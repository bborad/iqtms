using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Common;

namespace Ramp.UI.users
{
    public partial class viewusers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["LoggedInUser"] != null)
                {
                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
            }
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditUser")
            {
                Response.Redirect("edituser.aspx?userid=" + e.CommandArgument.ToString());
            }
        }

        public void btnAddUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("adduser.aspx");
        }
    }
}
