using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBModule = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Common;


namespace Ramp.UI.users
{
    public partial class edituser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["LoggedInUser"] != null)
                {
                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }

                if (Request.QueryString["userid"] != null)
                {
                    IUsers objUser = DBModule.Users.GetUserDetail(Convert.ToInt32(Request.QueryString["userid"]));
                    if (objUser != null)
                    {
                        txtEmailID.Text = objUser.EmailID;
                        txtUserName.Text = objUser.UserName;
                        txtUserID.Text = objUser.UserID;
                        txtUserID.Enabled = false;
                        ddlSecurityGroups.SelectedValue = objUser.SecurityGroupID.ToString();
                    }
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim() != "" && txtEmailID.Text.Trim() != "")
            {
                DBModule.Users objUser = new Ramp.DBConnector.Adapter.Users();
                objUser.ID = Convert.ToInt32(Request.QueryString["userid"]);
                objUser.UserName = txtUserName.Text;
                objUser.EmailID = txtEmailID.Text;
                objUser.SecurityGroupID = Convert.ToInt32(ddlSecurityGroups.SelectedValue);

                int result = objUser.UpdateUser();
                if (result >= 0)
                {
                    Response.Redirect("viewusers.aspx");
                    //lblMsg.Text = "Record updated successfully.";
                    //lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else if (result == -1)
                {
                    lblMsg.Text = "Username already exist.";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblMsg.Text = "Error in updating record.";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("viewusers.aspx");
        }
    }
}
