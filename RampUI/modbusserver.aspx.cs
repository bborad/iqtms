using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ramp.ModbusConnector;
using Ramp.DBConnector.Adapter;

namespace Ramp.UI
{
    public partial class modbusserver : System.Web.UI.Page
    {
        DataTable dtPadStatus;

        string ServerName = "";

        DataSet dsPadStatus;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["ModbusLock"] == null)
            {
                Application["ModbusLock"] = new object();
            }

            lblMessage.Text = "";

            GetPadStatus();

            FillTable();

        }

        void GetPadStatus()
        {
            //dtModbus = new DataTable("dtModbus");
            //DataColumn[] dcc = new DataColumn[] { new DataColumn("PadName",typeof(String)),
            //                                      new DataColumn("PadStatus", typeof(Int32)), 
            //                                      new DataColumn("PadStart", typeof(Int32)), 
            //                                      new DataColumn("PadMatch", typeof(Int32))
            //};

            //dtModbus.Columns.AddRange(dcc);
            //dtModbus.AcceptChanges();


            //Hoppers_GetHoppers stored procedure is changed on 7-12-2011 might not work as expected/before
            dsPadStatus = Hoppers.GetHoppers();

            dtPadStatus = dsPadStatus.Tables[0];

            if (dsPadStatus.Tables[1] != null && dsPadStatus.Tables[1].Rows.Count > 0)
                ServerName = dsPadStatus.Tables[1].Rows[0][0].ToString();

            lblServerName.Text = "SERVER " + ServerName;

        }

        void FillTable()
        {

            try
            {
                string padMatchValues = ""; char[] padMatches = null;
                string padStartValues = ""; char[] padStarts = null;

                short[] padStart = null; short[] padStatus = null; short[] padMatch = null;

                object objLock = Application["ModbusLock"];

                try
                {
                    lock (objLock)
                    {
                        ModbusClient mClient = new ModbusClient();
                        padStart = mClient.ReadAllPadStart();
                        padStatus = mClient.ReadAllPadStatus();
                        padMatch = mClient.ReadAllPadMatch();
                        mClient.DisconnectModbusServer();
                    }

                    if (padMatch != null && padMatch.Length > 0)
                    {
                        padMatchValues = Convert.ToString(padMatch[0], 2);
                    }

                    padMatchValues = padMatchValues.PadLeft(16, '0');
                    padMatches = padMatchValues.ToCharArray();

                    if (padStart != null && padStart.Length > 0)
                    {
                        padStartValues = Convert.ToString(padStart[0], 2);
                    }
                    padStartValues = padStartValues.PadLeft(16, '0');
                    padStarts = padStartValues.ToCharArray();
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                }

                DataRow dr = null;

                dtPadStatus.AcceptChanges();

                for (int i = 0; i < dtPadStatus.Rows.Count; i++)
                {
                    try
                    {
                        dr = dtPadStatus.Rows[i];

                        if (dr != null)
                        {
                            dr["PadMatch"] = Convert.ToInt32(padMatches[i+3].ToString());
                            dr["PadStart"] = Convert.ToInt32(padStarts[i+3].ToString());
                            dr["PadStatus"] = Convert.ToInt32(padStatus[i].ToString());

                            if (i == 5 || i == 6)
                            {
                                dr.Delete();
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        if (dr != null && (i == 5 || i == 6))
                        {
                            dr.Delete();
                        }
                    }
                }

                dtPadStatus.AcceptChanges();

                lvModbusDetails.DataSource = dtPadStatus;
                lvModbusDetails.DataBind();


            }
            catch (Exception ex)
            {
                //throw ex;
            }

        }

        protected void lvModbusDetails_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            //    try
            //    {
            //        HiddenField hdnPadStart, hdnPadMatch, hdnPadStatus;
            //        hdnPadStart = (HiddenField)e.Item.FindControl("hdnPadStart");
            //        hdnPadMatch = (HiddenField)e.Item.FindControl("hdnPadMatch");
            //        hdnPadStatus = (HiddenField)e.Item.FindControl("hdnPadStatus");

            //        Label lblReady = (Label)e.Item.FindControl("lblReady");
            //        Label lblFalut = (Label)e.Item.FindControl("lblFault");
            //        Label lblNotAvailable = (Label)e.Item.FindControl("lblNotAvailable");
            //        Label lblInvalid = (Label)e.Item.FindControl("lblInvalid");

            //        Image imgPadStart, imgPadMatch;

            //        imgPadStart = (Image)e.Item.FindControl("imgPadStart");
            //        imgPadMatch = (Image)e.Item.FindControl("imgPadMatch");

            //        if (Convert.ToInt32(hdnPadStart.Value) == 0)
            //        {
            //            // Gray Image
            //            imgPadStart.ImageUrl = "~/images/grey-dot.png";
            //        }
            //        else
            //        {
            //            // Green Image
            //            imgPadStart.ImageUrl = "~/images/green-dot.png";
            //        }

            //        if (Convert.ToInt32(hdnPadMatch.Value) == 0)
            //        {
            //            // Gray Image
            //            imgPadMatch.ImageUrl = "~/images/grey-dot.png";
            //        } 
            //        else
            //        {
            //            // Green Image
            //            imgPadMatch.ImageUrl = "~/images/green-dot.png";
            //        }

            //        lblReady.ForeColor = System.Drawing.Color.Gray;
            //        lblFalut.ForeColor = System.Drawing.Color.Gray;
            //        lblNotAvailable.ForeColor = System.Drawing.Color.Gray;
            //        lblInvalid.ForeColor = System.Drawing.Color.Gray;

            //        if (lblMessage.Text == "")
            //        {
            //            if (Convert.ToInt32(hdnPadStatus.Value) == 0)
            //            {
            //                lblReady.ForeColor = System.Drawing.Color.Green;
            //            }
            //            else if (Convert.ToInt32(hdnPadStatus.Value) == 1)
            //            {
            //                lblFalut.ForeColor = System.Drawing.Color.Orange;
            //            }
            //            else if (Convert.ToInt32(hdnPadStatus.Value) == 2)
            //            {
            //                lblNotAvailable.ForeColor = System.Drawing.Color.Red;
            //            }
            //            else
            //            {
            //                lblInvalid.ForeColor = System.Drawing.Color.Red;
            //            }
            //        }

            //    }
            //    catch
            //    {
            //    }
        }

        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            GetPadStatus();
            // Bind Closed Transactions
            FillTable();

        }
    }
}
