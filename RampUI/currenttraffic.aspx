﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="currenttraffic.aspx.cs"
    Inherits="Ramp.UI.currenttraffic" MasterPageFile="~/masterpages/SiteMaster.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">



    <script type="text/javascript">
        var OpenedDivID;
        function ToggleDisplayDiv(DivID) {
           try
           {
            if (OpenedDivID != null && OpenedDivID != DivID) {
                document.getElementById(OpenedDivID).style.display = 'none';
            }
            var Display = document.getElementById(DivID).style.display;
            if (Display != null && Display == 'none') {
                document.getElementById(DivID).style.display = 'block';
                OpenedDivID = DivID;
            }
            else {
                document.getElementById(DivID).style.display = 'none';
                OpenedDivID = null;
            }
           }
           catch(ex)
           {
           }
        }
    </script>

    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>CURRENT TRAFFIC</b></center>
            <br />
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            </asp:ScriptManager>
            <div id="Div1">
                <asp:Timer ID="UpdateTimer" runat="server" Interval="7000" OnTick="UpdateTimer_Tick">
                </asp:Timer>
                <asp:UpdatePanel ID="updatepnl" runat="server" RenderMode="Inline" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%">
                            <tr id="trShowLink" runat="server" visible="false">
                                <td colspan="2">
                                    Some of the reader(s) are not working. Click <a href="reports/systemstatusreport.aspx">
                                        here</a> to see System Status report.
                                </td>
                            </tr>
                            <tr>
                                <td width="85%" valign="top">
                                    <asp:GridView ID="gvCurrentTraffic" runat="server" AutoGenerateColumns="False" Width="95%"
                                        OnRowDataBound="gvCurrentTraffic_RowDataBound" AllowSorting="True" OnSorting="gvCurrentTraffic_Sorting"
                                        CssClass="border_strcture">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                                            <asp:BoundField DataField="StartTime" HeaderText="Date Time" SortExpression = "StartTime"
                                                ItemStyle-Width="130px" >
                                                <HeaderStyle Width="130px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TruckName" HeaderText="Truck" ItemStyle-Width="110px" SortExpression = "TruckName" >
                                                <ItemStyle Width="110px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="REDS" SortExpression="REDS" HeaderStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblREDS" Text='<%# Bind("REDS") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_4\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_4' style="display: none; width: 130px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgREDS" Text='<%# Bind("REDS") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_4")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblRTransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblRTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblRRFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblRREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblRQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblREntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblRPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblRTruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblRExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Truck Queue" SortExpression="Queue" HeaderStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblQueue" Text='<%# Bind("Queue") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_5\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_5' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgQueue" Text='<%# Bind("Queue") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_5")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblQTransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblQTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblQRFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblQREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblQQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblQEntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblQPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblQTruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblQExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Entry Gate" SortExpression="EntryGate" ItemStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEntryGate" Text='<%# Bind("EntryGate") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_6\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_6' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgEntryGate" Text='<%# Bind("EntryGate") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_6")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblETransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblETruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblERFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblEREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblEQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblEEntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblEPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblETruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblEExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PAD" SortExpression="PAD" ItemStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblPAD" Text='<%# Bind("PAD") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_7\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_7' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgPAD" Text='<%# Bind("PAD") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_7")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblPTransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblPTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblPRFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblPREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblPQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblPEntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblPPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblPTruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblPExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Truck Wash" SortExpression="TruckWash" ItemStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTruckWash" Text='<%# Bind("TruckWash") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_8\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_8' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgTruckWash" Text='<%# Bind("TruckWash") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_8")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblTTransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblTTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblTRFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblTREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblTQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblTEntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblTPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblTTruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblTExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Exit" SortExpression="Exit" ItemStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblExit" Text='<%# Bind("Exit") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_9\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_9' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgExit" Text='<%# Bind("Exit") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_9")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblExTransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblExTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblExRFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblExREDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblExQueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblExEntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblExPADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblExTruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblExExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="REDS Outbound" SortExpression="REDS2" ItemStyle-Width="130px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblREDS2" Text='<%# Bind("REDS2") %>' onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID") + "_10\")"%>'></asp:Label>
                                                    <div id='dv<%# Eval("ID") %>_10' style="display: none; width: 168px; position: absolute;
                                                        z-index: 0; font-size: 10px" class="msg_yellow">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;">
                                                                    <asp:Label runat="server" ID="lblMsgREDS2" Text='<%# Bind("REDS2") %>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" style="padding: 0px; width: 10%">
                                                                    <input type="button" value="X" style="background-color: Transparent; border: none;
                                                                        color: Red;" onclick='ToggleDisplayDiv("dv<%# Eval("ID") %>_10")' />&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding: 4px;" colspan="2">
                                                                    Trans ID:
                                                                    <asp:Label runat="server" ID="lblREDS2TransactionID" Text='<%# "T"+ Eval("ID") %>'></asp:Label><br />
                                                                    Truck ID:
                                                                    <asp:Label runat="server" ID="lblREDS2TruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                                    RFID:
                                                                    <asp:Label runat="server" ID="lblREDS2RFID" Text='<%# Bind("RFID") %>'></asp:Label><br />
                                                                    REDS:
                                                                    <asp:Label runat="server" ID="lblREDS2REDSTime" Text='<%# Bind("REDSTime") %>'></asp:Label><br />
                                                                    T-Park:
                                                                    <asp:Label runat="server" ID="lblREDS2QueueTime" Text='<%# Bind("QueueTime") %>'></asp:Label><br />
                                                                    E-Gate:
                                                                    <asp:Label runat="server" ID="lblREDS2EntryGateTime" Text='<%# Bind("EntryGateTime") %>'></asp:Label><br />
                                                                    PAD:
                                                                    <asp:Label runat="server" ID="lblREDS2PADTime" Text='<%# Bind("PADTime") %>'></asp:Label><br />
                                                                    T-Wash:
                                                                    <asp:Label runat="server" ID="lblREDS2TruckWashTime" Text='<%# Bind("TruckWashTime") %>'></asp:Label><br />
                                                                    Exit:
                                                                    <asp:Label runat="server" ID="lblREDS2ExitTime" Text='<%# Bind("ExitTime") %>'></asp:Label><br />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Records Found!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                                <td width="10%" valign="top">
                                    <asp:GridView ID="gvClosedTx" runat="server" AutoGenerateColumns="false" Width="100%"
                                        OnRowDataBound="gvClosedTx_RowDataBound" CssClass="border_strcture">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                                            <asp:BoundField DataField="ExitTime" HeaderText="ExitTime" Visible="false" />
                                            <asp:BoundField DataField="TruckWash" HeaderText="TruckWash" Visible="false" />
                                            <asp:TemplateField HeaderText="Last Closed Trans." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblClosedTxName" Text='<%# Eval("TrasactionName") %>'
                                                        onmouseover='<%# "ToggleDisplayDiv(\"dv" + Eval("ID")+"\")"%>'></asp:Label>
                                                    <asp:HiddenField ID="hdnTransStatus" runat="server" Value='<%# Eval("TransStatus") %>' />
                                                    <div id='dv<%# Eval("ID") %>' style="display: none; width: 150px; height: 90px; position: absolute;
                                                        z-index: 0; font-size: 10px; padding: 4px; border: solid 1px; text-align: left"
                                                        class="msg_yellow">
                                                        Trans ID:
                                                        <asp:Label runat="server" ID="lblExTransactionID" Text='<%# "T"+ Eval("TransID") %>'></asp:Label><br />
                                                        Truck ID:
                                                        <asp:Label runat="server" ID="lblExTruckID" Text='<%# Bind("TruckID") %>'></asp:Label><br />
                                                        Trans Closed on :
                                                        <asp:Label runat="server" ID="lblClosedDate" Text='<%# Bind("ArchiveDate") %>'></asp:Label><br />
                                                        Info:
                                                        <asp:Label runat="server" ID="lblMsgTrans" Text='<%# Bind("TransMessage") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Records Found!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
                        <asp:AsyncPostBackTrigger ControlID="gvCurrentTraffic" EventName="Sorting" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
