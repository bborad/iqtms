﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="expectedtimeslist.aspx.cs"
    Inherits="Ramp.UI.expectedtimeslist" MasterPageFile="~/masterpages/SiteMaster.Master"
    Title="Expected Times" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>EXPECTED TIME CONFIGURATION</b></center>
            <br />
            <table width="80%" class="border_strcture1">
                <tr>
                    <td width="35%" align="center">
                        <asp:FormView ID="fvExpectedConfig" runat="server" DataKeyNames="ID" CssClass="border_strcture"
                            Width="100%" DefaultMode="Edit">
                            <EditItemTemplate>
                                <table width="100%" class="border_strcture1">
                                    <tr>
                                        <th>
                                            Time Configuration
                                        </th>
                                    </tr>
                                </table>
                                <table width="100%" class="border_strcture">
                                    <tr>
                                        <th>
                                            Configuration
                                        </th>
                                        <th>
                                            Data
                                        </th>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--RQ--%>
                                            REDS to Queue
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtRQ" runat="server" Text='<%# Bind("REDS_Queue") %>' MaxLength="10"
                                                Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngRQ" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtRQ" ValidationGroup="Upvalidation" Type ="Integer"  > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--QE1--%>
                                            Queue to Entry1
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtQE1" runat="server" Text='<%# Bind("Queue_Ent1") %>' MaxLength="10"
                                                Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngQE1" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtQE1" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--E1E2--%>
                                            Entry1 to Entry2
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtE1E2" runat="server" Text='<%# Bind("Ent1_Ent2") %>' MaxLength="10"
                                                Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngE1E2" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtE1E2" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Queue Time Per Truck
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtQTPT" runat="server" Text='<%# Bind("QueueTimePerTruck") %>'
                                                MaxLength="10" Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngQTPT" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtQTPT" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Unload Time
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtUT" runat="server" Text='<%# Bind("Unloading") %>' MaxLength="10"
                                                Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngUT" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtUT" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--XR--%>
                                            Exit to REDS
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtXR" runat="server" Text='<%# Bind("Exit_REDS") %>' MaxLength="10"
                                                Width="75px" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngXR" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtXR" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%-- TWR--%>
                                            TruckWash to REDS
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtTWR" runat="server" Text='<%# Bind("TruckWash_REDS") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngTWR" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtTWR" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--XE1--%>
                                            Exit to Entry1
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtXE1" runat="server" Text='<%# Bind("Exit_Ent1") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngXE1" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtXE1" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--TWE1--%>
                                            TruckWash to Entry1
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtTWE1" runat="server" Text='<%# Bind("TruckWash_Ent1") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngTWE1" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtTWE1" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="border_strcture">
                                    <tr>
                                        <th>
                                            System Configuration
                                        </th>
                                    </tr>
                                </table>
                                <table width="100%" class="border_strcture">
                                    <tr>
                                        <th>
                                            Configuration
                                        </th>
                                        <th>
                                            Data
                                        </th>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--SU--%>
                                            StartUp
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtSU" runat="server" Text='<%# Bind("StartUp") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngSU" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtSU" ValidationGroup="Upvalidation" Type = "Integer"> </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            <%--SD--%>
                                            ShutDown
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtSD" runat="server" Text='<%# Bind("ShutDown") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngSD" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtSD" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Response Time
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtRT" runat="server" Text='<%# Bind("TruckCallResponse") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngRT" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtRT" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Max Queue Length
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtTQM" runat="server" Text='<%# Bind("TruckQueueMax") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;
                                            <asp:RangeValidator ID="rngTQM" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtTQM" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Precision
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtPrecision" runat="server" Text='<%# Bind("Precision") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngPrecision" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtPrecision" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Tag Read Frequency
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtTRF" runat="server" Text='<%# Bind("TagReadFrequency") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngTRF" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtTRF" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right;">
                                            Max Delay
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            <asp:TextBox ID="txtMD" runat="server" Text='<%# Bind("MaxDelay") %>' Width="75px"
                                                MaxLength="10" Style="text-align: right" />&nbsp;&nbsp;sec
                                            <asp:RangeValidator ID="rngMD" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                                MaximumValue="999999999" ControlToValidate="txtMD" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </asp:FormView>
                    </td>
                    <td width="65%" valign="top">
                        <asp:GridView ID="gvHopperExpectedTime" runat="server" AutoGenerateColumns="False"
                            border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture" AllowPaging="true"
                            PageSize="50" DataKeyNames="ID" Width="100%" 
                            onrowdatabound="gvHopperExpectedTime_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Configuration" SortExpression="0"
                                    Visible="false" ItemStyle-CssClass="border_strcture" HeaderStyle-Width="25%"
                                    ItemStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblEntryExpectedTime" Text='<%# Bind("ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="E2Hopper" ItemStyle-HorizontalAlign="right" SortExpression="E2Hopper"
                                    ItemStyle-CssClass="border_strcture" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true"
                                    HeaderText="Configuration" />
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture"
                                    ItemStyle-Width="20%" HeaderStyle-Font-Bold="true" HeaderText="Data">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtEntryExpectedTime" Text='<%# Bind("TimeFromEnt2") %>'
                                            MaxLength="10" Width="75px" Style="text-align: right"></asp:TextBox>&nbsp;&nbsp;sec
                                        <asp:RangeValidator ID="rngValData" runat="server" ErrorMessage="Incorrect" MinimumValue="0"
                                            MaximumValue="999999999" ControlToValidate="txtEntryExpectedTime" ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="HopperTW" ItemStyle-HorizontalAlign="right" SortExpression="HopperTW"
                                    ItemStyle-CssClass="border_strcture" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true"
                                    HeaderText="Configuration" />
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture"
                                    ItemStyle-Width="20%" HeaderStyle-Font-Bold="true" HeaderText="Data">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtExpectedTimeTW" Text='<%# Bind("TimeToTW") %>'
                                            MaxLength="10" Width="75px" Style="text-align: right"></asp:TextBox>&nbsp;&nbsp;sec
                                        <asp:RangeValidator ID="rngValDataTimeTW" runat="server" ErrorMessage="Incorrect"
                                            MinimumValue="0" MaximumValue="999999999" ControlToValidate="txtExpectedTimeTW"
                                            ValidationGroup="Upvalidation" Type ="Integer" > </asp:RangeValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture"
                                    ItemStyle-Width="10%" HeaderStyle-Font-Bold="true" HeaderText="Loop ID">
                                    <ItemTemplate>
                                        <asp:HiddenField ID = "hdnLoopID" runat = "server" Value = '<%# Bind("MarkerID") %>' />
                                        <asp:DropDownList ID="ddlLoopID" runat="server" DataTextField="MarkerLoopID" DataValueField="ID"
                                             ValidationGroup="Upvalidation" >
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLoopID" ControlToValidate="ddlLoopID"
                                            Display="Dynamic" ErrorMessage="*Required" InitialValue="0" ValidationGroup="Upvalidation"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <br />
                        <asp:GridView ID="gvConfiguration" runat="server" DataKeyNames="ID" Width="100%"
                            AutoGenerateColumns="False" border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture"
                            AllowPaging="true" PageSize="50" OnRowDataBound="gvConfiguration_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture"
                                    ItemStyle-Width="20%" HeaderStyle-Font-Bold="true" HeaderText="Configuration">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ID") %>' />
                                         <asp:HiddenField ID="hdnField" runat="server" Value='<%# Eval("Field") %>' />
                                          <asp:HiddenField ID="hdnParameterType" runat="server" Value='<%# Eval("ParameterType") %>' />
                                        <asp:Label ID="lblParameterTypeField" runat="server" Text='<%# Eval("ParameterType") +"-"+ Eval("Field")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="border_strcture" ItemStyle-CssClass="border_strcture"
                                    ItemStyle-Width="20%" HeaderStyle-Font-Bold="true" HeaderText="Data">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtConfiguration" Text='<%# Bind("Value") %>' MaxLength="10"
                                            Width="75px" Style="text-align: right"></asp:TextBox> &nbsp;&nbsp;                                       
                                        <asp:Label ID="lblUnit" runat="server" Text=""></asp:Label>
                                        <asp:RangeValidator ID="rngValConfiguration" runat="server" ErrorMessage="Incorrect"
                                            Type="Integer" MinimumValue="0" MaximumValue="999999999" ControlToValidate="txtConfiguration"
                                            ValidationGroup="Upvalidation"> </asp:RangeValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>                   
                   
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right">
                        <asp:Button ID="btnApply" runat="server" Text="Apply" class="but" OnClick="btnApply_Click"
                            ValidationGroup="Upvalidation" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="but" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
            <asp:ObjectDataSource ID="odsMarkers" runat="server" SelectMethod="GetMarkersAtLocation"
                TypeName="Ramp.DBConnector.Adapter.Markers">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="loc" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
