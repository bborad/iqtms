﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Ramp.UI.login"
    Title="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap-page">
        <div id="header">
            <table width="100%" cellspacing="0" cellpadding="4">
                <tr>
                <td width="32%" align="left" valign="top">
                        <asp:Image ID="Image1" runat="server" AlternateText="POAGS" ImageUrl="~/images/poags-logo.jpg"
                            Width="249" Height="83" />
                    </td>
                    <td width="50%" align="left" valign="top">
                        <asp:Image ID="logo" runat="server" AlternateText="IQ-TMS" ImageUrl="~/images/iq-tms-logo.jpg"
                            Width="249" Height="83" />
                    </td>
                </tr>
            </table></div>
        <div id="navigation">
        </div>
        <div id="mid_section">
            <table align="center" class="border_strcture1">
                <tr>
                    <td colspan="2">
                        <span style="color: Red;">
                            <asp:Label ID="lblErrorMsg" runat="server" SkinID="ErrorLabel"></asp:Label></span>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="right">
                        User Name:
                    </td>
                    <td valign="middle">
                        <asp:TextBox ID="txtUserName" runat="server" MaxLength="150" CssClass="txtBox" Width="175px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalusername" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage=" Required" ValidationGroup="valgrpLogin"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="right">
                        Password:
                    </td>
                    <td valign="middle">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20" CssClass="txtBox"
                            Width="175px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalPassword" runat="server" ControlToValidate="txtPassword"
                            ErrorMessage=" Required" ValidationGroup="valgrpLogin"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" ValidationGroup="valgrpLogin"
                            CssClass="but" />
                        <asp:Button ID="btnForgotPass" runat="server" Text="Forgot Password" OnClick="btnForgotPass_Click"
                            CssClass="but" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="text-align: center;padding-top:5px;">
        <asp:Label ID = "lblVersion" runat = "server"></asp:Label>       
    </div>
    </form>
</body>
</html>
