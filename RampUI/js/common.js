﻿/**************************************************************************************
 * Author : Anupan Garg
 * Created Date : 30-March-2008
 * Last Modified by : Anupan Garg
 * Last Modified : 30-March-2008
 * Used : Deepanshu Jouhari 14 Jan 2009, found in Staffing Universe Web.
 * Module Name : DomainObjects
 * Decription :  This JS file will have all common functions which will use in entire application.
 **************************************************************************************/
/*    36 Home 35 End  37 Left Arrow   39 Right Arrow  46 Delete   8  Backspace  */
var AllowKeys ="36,35,37,39,46,8";
var Restaurant_Id=0;

// Format the phone number as xxx-xxx-xxxx
function checkDashedPhoneFormat(oEvent)
{
    oEvent = oEvent || window.event;
    var txtField = oEvent.target || oEvent.srcElement;
  
    var keyCode  =  oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;

    if(keyCode != null)
    {
        var strkey = String.fromCharCode(keyCode);
        if((strkey < '0') || (strkey > '9'))
        {          
           if(AllowKeys.indexOf(keyCode) != -1)
            return true;
           else            
            return false;
        }
    }
    
    switch(document.getElementById(txtField.id).value.length)
    {
        case 3:
                document.getElementById(txtField.id).value = document.getElementById(txtField.id).value + '-';
            break;
        
        case 7:
            document.getElementById(txtField.id).value = document.getElementById(txtField.id).value + '-';
            break;
    }
            
    document.getElementById(txtField.id).focus();
    
    return true;
}

//To format the phone number on onblur event of textbox.
function isValidPhoneNumber (phoneNo) 
{

    var strPhone = document.getElementById(phoneNo).value;
    strPhone = strPhone.replace(/-/g, '');
    strPhone = strPhone.replace('(', '');
    strPhone = strPhone.replace(')', '');
    var strTemp = ''; 
      if (strPhone.length>3)
      {
          strTemp = strTemp + strPhone.substring(0,3) + '-';
          strTemp = strTemp + strPhone.substring(3,strPhone.length);
          document.getElementById(phoneNo).value = strTemp;
      }
      
      if (strTemp.length>7)
      {
          strTemp = strTemp.substring(0,7) + '-';
          strTemp = strTemp + strPhone.substring(6,10);
          document.getElementById(phoneNo).value = strTemp;
      }
  
  }
  
  //Decimal Format
  function checkDecimalFormat(oEvent)
{
    retVal = true;
    
    oEvent = oEvent || window.event;
    var txtField = oEvent.target || oEvent.srcElement;

    var keyCode  =  oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;
                    
    //alert(keyCode);
//    alert(oEvent.delKey);

    if (keyCode == 46 || keyCode == 8 || keyCode == 9 || keyCode == 13 || keyCode == 23  || keyCode == 46)
        return true;
    if(!(keyCode>= 48 && keyCode <= 57))
        return false;
    if(keyCode != null)
    {
        var strkey = String.fromCharCode(keyCode);
        
        if(strkey == '.')
        {
           
            if(document.getElementById(txtField.id).value.indexOf('.') > -1)
                return false;
        }
        
        if(((strkey < '0') || (strkey > '9')) && (strkey != '.'))
        {
           if(AllowKeys.indexOf(keyCode) != -1)
                retVal = true;
           else            
                retVal = false;
        }
    }

    document.getElementById(txtField.id).focus();
    return retVal;
}

          
function checkNumerickFormat(oEvent)
{
    retVal = true;
    
    oEvent = oEvent || window.event;
    var txtField = oEvent.target || oEvent.srcElement;

    var keyCode  =  oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;

    //alert(keyCode);
    if (keyCode == 8 || keyCode == 9 || keyCode == 13 || keyCode == 23)
        return true;
    if(!(keyCode>= 48 && keyCode <= 57))
        return false;
        
    if(keyCode != null)
    {
        var strkey = String.fromCharCode(keyCode);

        if((strkey < '0') || (strkey > '9'))
        {
           if(AllowKeys.indexOf(keyCode) != -1)
                retVal = true;
           else            
                retVal = false;
        }
    }

    document.getElementById(txtField.id).focus();
    return retVal;
}

//Convert Lower case letter to Upper case letter
function ConvertToupperCase(oEvent)
{

    oEvent = oEvent || window.event;
    var txtField = oEvent.target || oEvent.srcElement;
    var keyCode  =  oEvent.keyCode ? oEvent.keyCode :
                    oEvent.charCode ? oEvent.charCode :
                    oEvent.which ? oEvent.which : void 0;
                    if(keyCode == 37 || keyCode == 39)
                    return true;
    document.getElementById(txtField.id).value = document.getElementById(txtField.id).value.toUpperCase();
    return true;
}



function textCounter(field, maxlimit) {
    var thisfield = $get(field);
    //var counter = $get(countfield);

    if (thisfield.value.length >= maxlimit)
        return false;
    else
        return true;
       // thisfield.value = thisfield.value.substring(0, maxlimit);
        
        
   // else
       // counter.innerHTML = maxlimit - thisfield.value.length;
}


function setHourglass()
  {
    document.body.style.cursor = 'wait';
}

function OnCompanyChange() {
    
    if (document.getElementById('ctl00_mPageContainer_ddlCompanyType').selectedIndex == 0) {
        document.getElementById("typeofPosition").style.display = "none";
        document.getElementById("SelectAll").style.display = "none";
    }
    else {
        document.getElementById("typeofPosition").style.display = "block";
        document.getElementById("SelectAll").style.display = "block";
    }
    
    document.getElementById("chkSelectAll").checked = false;
}



function selectall(t, c) {
    var flag = 0;
    var chkList1 = document.getElementById(t);
    if (chkList1 != null) {
        var arrayOfCheckBoxes = chkList1.getElementsByTagName("input");
        var lenght = arrayOfCheckBoxes.length;
        for (counter = 0; counter < arrayOfCheckBoxes.length; counter++) {
            //                for (i = 1; i < t.length; i++)
            arrayOfCheckBoxes[counter].checked = c.checked;
        }
    }
}

//function HideShowTypeofPosition()
//    {
//        if (document.getElementById("typeofPosition").style.display=="none")
//        {
//            document.getElementById("typeofPosition").style.display="block";
//            document.getElementById("imgexpand").style.display="none";
//            document.getElementById("imgcollapse").style.display="block";
//        }
//        else if (document.getElementById("typeofPosition").style.display=="block")
//        {
//            document.getElementById("typeofPosition").style.display="none";
//            document.getElementById("imgexpand").style.display="block";
//            document.getElementById("imgcollapse").style.display="none";
//        } 
//    }

