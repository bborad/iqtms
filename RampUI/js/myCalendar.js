/*
Module			: DHTML Calendar with time.
Credits			: Sunil Kumtakar
Date Started	: 20-Sep-2006
Last Updated	: 09-Jan-2006
version			: 1.0.0

Note:
It brings pleasure to present this Calendar control. I have tested this with IE 6.0, Netscape 8, and Opera 9,
and it worked fine.

You are free to use and distribute this script.  If you are making any improvements to this code please keep me posted.
your Suggessions/  Comments are most welcomed at sunilkumtakar@gmail.com

Thanks n Enjoy.
*/

myCalendar = function() {
//Constructor
}

myCalendar.MonthName = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
myCalendar.Days_In_Month = new Array("31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31");
myCalendar.dateSeperator="/";
myCalendar.fillRequired=false;
myCalendar.created=false;
myCalendar.ctrlRef=new Array();
myCalendar.eventTriggeredBy = "";
myCalendar.isHidden=true;
myCalendar.browser="";
myCalendar.ControlId = {calendar : 'div$Calendar#', yearText: 'txt$Year#', 
                  yearDDL: 'ddl$Year#', monthDDL:'ddl$Month#', dayDDL:'ddl$Day#',
                  hourDDL:'ddl$Hour#', minuteDDL:'ddl$Minute#', dayPartDDL:'ddl$DayPart#',
                  buttonSelect: "btn$Select#", tblDate: "tbl$CalendarDate#", cellAmPm: "cell$DayPartText#",
                  tblTime: "tbl$CalendarTime#", tdDisplay: "td$CalendarDisplay#", imgCalendar: "img$Calendar#",
                  tdDisplay1: "td$CalendarDisplay1#"};

myCalendar.monthCodes = Array(5,1,1,4,6,2,4,0,3,5,1,3);
myCalendar.weekdays = Array('Sun', 'Mon','Tue','Wed','Thu','Fri','Sat');
myCalendar.isCalendarVisible=false;
myCalendar.storedValues = {year: 0, month: 0};

myCalendar.validYearKeys = function(obj) {
	var _keyCode;
	_keyCode = myCalendar.getKeyCode(obj);
	if (_keyCode >= 48 && _keyCode <= 57) {
		return true;
	}
	else {
		if (myCalendar.browser=="ie") {		
			event.returnValue = false;
			return false;
		}
		else {
			if (obj.keyCode==0) {
				obj.preventDefault();
			}
			return false;
		}
	}
}

myCalendar.getDayCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['dayDDL']);
}

myCalendar.getMonthCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['monthDDL']);
}

myCalendar.getYearCtrl = function(obj) {
    if (typeof obj == "undefined") {
        if (myCalendar.getParameterValue('isYearddl')) {
            return myCalendar.getElementById(myCalendar.ControlId['yearDDL']);
        }
        else {
            return myCalendar.getElementById(myCalendar.ControlId['yearText']);
        }
    }
    else {
        if (obj=='ddl') {
            return myCalendar.getElementById(myCalendar.ControlId['yearDDL']);
        }
        else if(obj=='text') {
            return myCalendar.getElementById(myCalendar.ControlId['yearText']);
        }        
    }
}

myCalendar.getClientDateCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.getParameterValue('inputField'));
}

myCalendar.getHourCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['hourDDL']);    
}

myCalendar.getMinuteCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['minuteDDL']);
}

myCalendar.getSecondCtrl = function(obj) {
    return myCalendar.getElementById("ddl$Second#");
}

myCalendar.getDayPartCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['dayPartDDL']);    
}

myCalendar.getDateCtrl = function(obj) {
	return myCalendar.getElementById(myCalendar.ControlId['tblDate']);
}

myCalendar.getTimeCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['tblTime']);
}

myCalendar.getCalendarCtrl = function(obj) {
    return myCalendar.getElementById(myCalendar.ControlId['calendar']);
}

myCalendar.getCellDayPartText = function(obj) {
    return myCalendar.getElementById("cell$DayPartText#");
}

myCalendar.getCellDayPartCtrl = function(obj) {
    return myCalendar.getElementById("cell$DayPartCtrl#");
}

myCalendar.getDay = function(obj) {
    return myCalendar.getDayCtrl().value;
}

myCalendar.getMonth = function(obj) {
    return myCalendar.getMonthCtrl().value;    
}

myCalendar.getMonthInNumber = function(obj) {
    return myCalendar.getMonthCtrl().selectedIndex;
}

myCalendar.getYear = function(obj) {
    return myCalendar.getYearCtrl().value;
}

myCalendar.getHour = function(obj) {
    return myCalendar.getHourCtrl().value;
}

myCalendar.getMinute = function(obj) {
    return myCalendar.getMinuteCtrl().value;
}

myCalendar.getSecond = function(obj) {
    return myCalendar.getSecondCtrl().value;
}

myCalendar.getDayPart = function(obj) {
    return myCalendar.getDayPartCtrl().value;
}

myCalendar.getDateValue = function(obj) {
    var _get = myCalendar.getFormat();
    var _iformat, sDate, sTime;
    sDate=sTime="";
    for(_iformat=0; _iformat< _get.length; _iformat++) {
        if (obj=='date') {
            if (_get[_iformat] == "dd") {
                if (sDate != "") sDate += myCalendar.dateSeperator;
                //to add '0' prefix
                //sDate += myCalendar.getDay();
                if (myCalendar.getDay().length<2) {
					sDate += '0' + myCalendar.getDay();
                }
                else {
					sDate += myCalendar.getDay();
                }
            }
            else if (_get[_iformat] == "mon") {
                if (sDate != "") sDate += myCalendar.dateSeperator;
                sDate += myCalendar.getMonth();            
            }
            else if (_get[_iformat] == "yyyy") {
                if (sDate != "") sDate += myCalendar.dateSeperator;
                sDate += myCalendar.getYear();
            }
        }
        else if(obj=='time') {
            if (_get[_iformat] == "hh") {
                if (sTime!="") sTime += myCalendar.getParameterValue('timeSeperator');
                sTime += myCalendar.getHour();            
            }
            else if (_get[_iformat] == "mm") {
                if (sTime!="") sTime += myCalendar.getParameterValue('timeSeperator');
                sTime += myCalendar.getMinute();
            }
            else if (_get[_iformat] == "ss") {
                if (sTime!="") sTime += myCalendar.getParameterValue('timeSeperator');
                sTime += myCalendar.getSecond();
            }
            else if (_get[_iformat] == "ampm") {
                if (sTime!="") sTime += " ";
                sTime += myCalendar.getDayPart();
            }
        }
    }    
    if (sTime != "" && sDate != "") {
        sDate += " " + sTime;
    }
    else if (sTime != "" && sDate == ""){
        sDate = sTime;
    }
    return sDate;
}

myCalendar.monthChanged = function(obj) {
	var objDay;
	var objCounter;		
	var objMonth = myCalendar.getMonthCtrl();
	var lastday = myCalendar.Days_In_Month[objMonth.selectedIndex-1];	
	if (objMonth.selectedIndex == 2 && myCalendar.isLeapYear(myCalendar.getYear(), myCalendar.getMonthInNumber())) {
	    lastday = 29;
	}	
	var selValue;
	objDay = myCalendar.getDayCtrl();
	selValue = objDay.value;	
	objDay.length=0;
	for(objCounter=1; objCounter <= lastday; objCounter++) {
	    if (objCounter==1) {
		    objDay.length++;
		    objDay.options[objDay.length-1].text = '';
		    objDay.options[objDay.length-1].value = '';
	    }
		objDay.length++;
		objDay.options[objDay.length-1].text = objCounter;
		objDay.options[objDay.length-1].value = objCounter;
		if (objCounter == selValue) {
			objDay.options[objDay.length-1].selected=true;
		}
	}    
}

myCalendar.yearChanged = function(obj) {    
    myCalendar.monthChanged();
}

myCalendar.fillDate = function(obj) {
    var objCounter;
    //static display start
    if (!myCalendar.fillRequired) {
	    var objDay, objMonth, objYear;
	    objDay = myCalendar.getDayCtrl();
	    objMonth = myCalendar.getMonthCtrl();
	    objYear = myCalendar.getYearCtrl();
	    for(objCounter=1; objCounter <= 31; objCounter++) {
	        if (objCounter==1) {
	            objDay.length++;
		        objDay.options[objDay.length-1].text = '';
		        objDay.options[objDay.length-1].value = '';
	        }
		    objDay.length++;
		    objDay.options[objDay.length-1].text = objCounter;
		    objDay.options[objDay.length-1].value = objCounter;
	    }
	    for(objCounter=1; objCounter <= 12; objCounter++) {
	        if (objCounter==1) {
	            objMonth.length++;
		        objMonth.options[objMonth.length-1].text = '';
		        objMonth.options[objMonth.length-1].value = '';
	        }
		    objMonth.length++;
		    objMonth.options[objMonth.length-1].text = myCalendar.MonthName[objCounter-1];
		    objMonth.options[objMonth.length-1].value = myCalendar.MonthName[objCounter-1];
	    }
	    myCalendar.fillRequired = true;
	}
	//static display end
	
	//dynamic display start
	if (myCalendar.getParameterValue('includeTime') || myCalendar.getParameterValue('mode')=="time") {
	    var objHour, objMinute, objSecond, objDayPart, objTimeFormat;
	    objTimeFormat = 12; //default
	    if (myCalendar.getParameterValue('timeFormat') == "24") {
	        objTimeFormat = 24;
	    }
	    objHour = myCalendar.getHourCtrl();
	    objMinute = myCalendar.getMinuteCtrl();
	    objDayPart = myCalendar.getDayPartCtrl();
	    objHour.length = 0; objMinute.length=0; /*objSecond.length =0;*/ objDayPart.length=0;
	    
	    if (objTimeFormat == 12) {
	        for(objCounter=1; objCounter <= 12; objCounter++) {
	            if (objCounter==1) {
		            objHour.length++;
		            objHour.options[objHour.length-1].text = '';
		            objHour.options[objHour.length-1].value = '';
	            }
		        objHour.length++;
		        objHour.options[objHour.length-1].text = myCalendar.padRight(objCounter, 2, '0');
		        objHour.options[objHour.length-1].value = myCalendar.padRight(objCounter, 2, '0');
	        }	        
	    }
	    else {
	        for(objCounter=0; objCounter <= 23; objCounter++) {
	            if (objCounter==0) {
		            objHour.length++;
		            objHour.options[objHour.length-1].text = '';
		            objHour.options[objHour.length-1].value = '';
	            }
		        objHour.length++;
		        objHour.options[objHour.length-1].text = myCalendar.padRight(objCounter, 2, '0');
		        objHour.options[objHour.length-1].value = myCalendar.padRight(objCounter, 2, '0');
	        }	    
	    }
	    for(objCounter=0; objCounter <= 59; objCounter++) {
	        if (objCounter==0) {
		        objMinute.length++;
		        objMinute.options[objMinute.length-1].text = '';
		        objMinute.options[objMinute.length-1].value = '';
	        }
		    objMinute.length++;
		    objMinute.options[objMinute.length-1].text = myCalendar.padRight(objCounter, 2, '0');
		    objMinute.options[objMinute.length-1].value = myCalendar.padRight(objCounter, 2, '0');
	    }
		objDayPart.length++;
		objDayPart.options[objDayPart.length-1].text = "";
		objDayPart.options[objDayPart.length-1].value = "";
		objDayPart.length++;
		objDayPart.options[objDayPart.length-1].text = "AM";
		objDayPart.options[objDayPart.length-1].value = "AM";
		objDayPart.length++;
		objDayPart.options[objDayPart.length-1].text = "PM";
		objDayPart.options[objDayPart.length-1].value = "PM";
	}
	
	//year ddl start	
	if (myCalendar.getParameterValue('isYearddl')) {
	    objYear = myCalendar.getYearCtrl();
	    objYear.length=0;
	    for(objCounter=myCalendar.getParameterValue('yearStart'); objCounter<=myCalendar.getParameterValue('yearEnd'); objCounter++) {
	        if (objCounter==myCalendar.getParameterValue('yearStart')) {
	            objYear.length++;
		        objYear.options[objYear.length-1].text = '';
		        objYear.options[objYear.length-1].value = '';
	        }
		    objYear.length++;
		    objYear.options[objYear.length-1].text = objCounter;
		    objYear.options[objYear.length-1].value = objCounter;
	    }
	    myCalendar.getYearCtrl('text').style.display='none';
	    myCalendar.getYearCtrl('ddl').style.display='block';
	}
	else {
	    myCalendar.getYearCtrl('text').style.display='block';
	    myCalendar.getYearCtrl('ddl').style.display='none';
	}
	//year ddl end
	//static display end
}

myCalendar.validTime = function(obj) {
    var _retVal;
    _retVal = true;
    if (myCalendar.getParameterValue('timeFormat')=="12") {
        if (myCalendar.getHour()=='' || myCalendar.getMinute()=='' || myCalendar.getDayPart()=='') {
            _retVal = false;
        }
    }
    else {//24 hrs
        if (myCalendar.getHour()=='' || myCalendar.getMinute()=='') {
            _retVal = false;
        }
    }
    return _retVal;
}

myCalendar.validDate = function(obj) {
    var _retVal;
    _retVal = true;
    if (myCalendar.getDay()=='' || myCalendar.getMonth()=='' 
            || myCalendar.getYear()=='') {
        _retVal = false;
    }
    return _retVal;
}

myCalendar.actionShow = function(obj) {
    myCalendar.actionHide();
    myCalendar.eventTriggeredBy = myCalendar.eventRaisedBy(obj);
    myCalendar.fillDate(obj);
    try {
		myCalendar.setDate();
    }
    catch(e){}
    myCalendar.positionSelf();
    myCalendar.prepareForDisplay();    
    myCalendar.clearCalendarDisplay();
    myCalendar.isCalendarVisible=false;
    
    var divCal = myCalendar.getCalendarCtrl();
    divCal.style.display="block";
    myCalendar.hideShowCovered();
    if (myCalendar.getParameterValue('mode')=="time") {
        myCalendar.getHourCtrl().focus();
    }
    else {
        myCalendar.getYearCtrl().focus();
    }
    myCalendar.isHidden=false;
}

myCalendar.setup = function(param) {
    myCalendar.setParameter(param);
    var divCal = myCalendar.getCalendarCtrl();
    myCalendar.createCalendar();
    myCalendar.getElementById(param['button']).onclick = myCalendar.actionShow;
}

myCalendar.isDebug = true;

myCalendar.debug = function(obj) {
    if (myCalendar.isDebug) {
        if (obj!=null) alert(obj);
    }
}

myCalendar.actionHide = function(obj) {
    var divCal = myCalendar.getCalendarCtrl();
    myCalendar.hideShowCovered();
    divCal.style.display="none";
    myCalendar.isHidden=true;
}

myCalendar.isLeapYear = function(nYear, nMonth) {
    return (((0 == (nYear%4)) && ( (0 != (nYear%100)) || (0 == (nYear%400)))) && nMonth == 2 && nYear !=0);
}

myCalendar.dateSelectComplete = function(obj) {    
    var _sDate, _sTime, _valid=true;
    _sDate=_sTime='';
    
    if (myCalendar.getParameterValue('mode')=='date') {
        if (!myCalendar.validDate()) _valid = false;
        if (myCalendar.getParameterValue('includeTime')) {
            if (!myCalendar.validTime()) _valid = false;
        }
        if (_valid) {
            _sDate = myCalendar.getDateValue('date')+'';
            _sTime = myCalendar.getDateValue('time')+'';
            if (_sDate != '' && _sTime != '') {
                _sDate += myCalendar.getParameterValue('dateTimeSeperator') + _sTime;
            }
            else {
                _sDate += _sTime;
            }
        }
    }
    else if(myCalendar.getParameterValue('mode')=='time') {
        if (!myCalendar.validTime()) _valid = false;
        if (_valid) {
            _sDate = myCalendar.getDateValue('time')+'';
        }
    }
    if (_valid) {
        myCalendar.getClientDateCtrl().value = _sDate;
        myCalendar.actionHide();
        if (typeof myCalendar.getParameterValue('onSelectionCompleteCallBack') == "function") {
            myCalendar.getParameterValue('onSelectionCompleteCallBack')();//client callback
        }       
        try{
        FillPassword(); 
        }   catch(e){}
    }
}

myCalendar.controlKeys4Year = function(obj) {        
	var _keyCode;
	_keyCode = myCalendar.getKeyCode(obj);
    if (_keyCode==38 || _keyCode==40 || _keyCode==33 || _keyCode==34) {
        var nYear = parseInt(myCalendar.getYear());
        if(_keyCode==38) {
            nYear--;
        }
        else if(_keyCode==40) {
            nYear++;
        }
        else if (_keyCode==33) {
            nYear-=10;
        }
        else if (_keyCode==34) {            
            nYear+=10;
        }
        if (nYear>0) {
            myCalendar.getYearCtrl().value = nYear;
            myCalendar.yearChanged(obj);
        }
        else {
            nYear = (new Date()).getFullYear();
            myCalendar.getYearCtrl().value = nYear;
            myCalendar.yearChanged(obj);
        }
    }
}

myCalendar.createElement = function(type, parent) {    
    var el =null;
	el = document.createElement(type);
	if (typeof parent != "undefined") {
		parent.appendChild(el);
	}
	return el;    
}

myCalendar.createCalendar = function(obj) {
    if (!myCalendar.created) {
        myCalendar.getBrowserInfo();
        var table, div, tblDate, tblTime, divDummy;
        var tblDummy, cellDummy;
        div = myCalendar.createElement("div");
        div.id = myCalendar.ControlId['calendar'];
        div.name = myCalendar.ControlId['calendar'];
        div.style.display = 'none';
        div.style.position = "absolute";
        div.className = "calendar";

        tblDate = myCalendar.createElement("table");
        tblDate.className = "calendarTable";
        tblDate.id = myCalendar.ControlId['tblDate'];
        tblDate.name = myCalendar.ControlId['tblDate'];
        myCalendar.setStyleAttribute(tblDate, "border-collapse:collapse");
        tblDate.border = '1';

        var tbody = myCalendar.createElement("tbody", tblDate);
        var row = myCalendar.createElement("tr", tbody);
        var cell = myCalendar.createElement("td", row);
        cell.appendChild(document.createTextNode("Day")); //cell.appendChild(document.createTextNode("Year"));
        cell = myCalendar.createElement("td", row);
        cell.appendChild(document.createTextNode("Month"));
        cell = myCalendar.createElement("td", row);
        cell.appendChild(document.createTextNode("Year")); //cell.appendChild(document.createTextNode("Day"));

        var objCtrl;
        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);

        //        objCtrl = myCalendar.createElement("INPUT");
        //        objCtrl.type='text';
        //        objCtrl.name= myCalendar.ControlId['yearText'];
        //        objCtrl.id=myCalendar.ControlId['yearText'];
        //        objCtrl.setAttribute("size", "4");
        //        objCtrl.setAttribute("maxlength", "4");
        //        
        //        cell.appendChild(objCtrl);
        //        objCtrl = myCalendar.createElement("select");
        //        objCtrl.name=myCalendar.ControlId['yearDDL'];
        //        objCtrl.id=myCalendar.ControlId['yearDDL'];
        //        objCtrl.style.display='none';
        //        cell.appendChild(objCtrl);


        //...............................................

        //cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['dayDDL'];
        objCtrl.id = myCalendar.ControlId['dayDDL'];
        cell.appendChild(objCtrl);
        //...............................................

        cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['monthDDL'];
        objCtrl.id = myCalendar.ControlId['monthDDL'];
        cell.appendChild(objCtrl);

        //        cell = myCalendar.createElement("td", row);
        //        objCtrl = myCalendar.createElement("select");
        //        objCtrl.name=myCalendar.ControlId['dayDDL'];
        //        objCtrl.id=myCalendar.ControlId['dayDDL'];        
        //        cell.appendChild(objCtrl);

        //...............................................

        cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("INPUT");
        objCtrl.type = 'text';
        objCtrl.name = myCalendar.ControlId['yearText'];
        objCtrl.id = myCalendar.ControlId['yearText'];
        objCtrl.setAttribute("size", "4");
        objCtrl.setAttribute("maxlength", "4");

        cell.appendChild(objCtrl);
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['yearDDL'];
        objCtrl.id = myCalendar.ControlId['yearDDL'];
        objCtrl.style.display = 'none';
        cell.appendChild(objCtrl);
        //...............................................       



        tblTime = myCalendar.createElement("table");
        tblTime.className = "calendarTable";
        tblTime.id = myCalendar.ControlId['tblTime'];
        tblTime.name = myCalendar.ControlId['tblTime'];

        myCalendar.setStyleAttribute(tblTime, "border-collapse:collapse");
        tblTime.border = '1';

        tbody = myCalendar.createElement("tbody", tblTime);
        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);
        cell.appendChild(document.createTextNode("Hour"));
        cell = myCalendar.createElement("td", row);
        cell.appendChild(document.createTextNode("Minute"));
        cell = myCalendar.createElement("td", row);
        cell.id = myCalendar.ControlId['cellAmPm'];
        cell.name = myCalendar.ControlId['cellAmPm'];
        cell.appendChild(document.createTextNode("AM/PM"));

        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['hourDDL'];
        objCtrl.id = myCalendar.ControlId['hourDDL'];
        cell.appendChild(objCtrl);

        cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['minuteDDL'];
        objCtrl.id = myCalendar.ControlId['minuteDDL'];
        cell.appendChild(objCtrl);

        cell = myCalendar.createElement("td", row);
        cell.id = "cell$DayPartCtrl#";
        cell.name = "cell$DayPartCtrl#";
        objCtrl = myCalendar.createElement("select");
        objCtrl.name = myCalendar.ControlId['dayPartDDL'];
        objCtrl.id = myCalendar.ControlId['dayPartDDL'];
        cell.appendChild(objCtrl);

        table = myCalendar.createElement("table");
        table.id = 'tbl$Calendar#';
        table.name = 'tbl$Calendar#';
        myCalendar.setStyleAttribute(table, "border-collapse:collapse");
        table.border = '0';
        table.cellspacing = 0;
        table.cellpadding = 0;

        tbody = myCalendar.createElement("tbody", table);
        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);
        cell.setAttribute("rowspan", "2");
        cell.appendChild(tblDate);

        cell = myCalendar.createElement("td", row);
        cell.setAttribute("rowspan", "2");
        cell.appendChild(tblTime);

        cell = myCalendar.createElement("td", row);

        // cell.align = "right";      

        objCtrl = myCalendar.createElement("img");
        objCtrl.id = "img$CalendarClose#";
        objCtrl.name = "img$CalendarClose#";
        objCtrl.src = '../images/imgclose.jpg';
        objCtrl.title = "Close";
        //objCtrl.align = "right";
       // objCtrl.cl = "padding-right:8px;";

        cell.appendChild(objCtrl);

        objCtrl = myCalendar.createElement("img");
        objCtrl.id = myCalendar.ControlId['imgCalendar'];
        objCtrl.name = myCalendar.ControlId['imgCalendar'];
        objCtrl.src = '../images/imgsmall.gif';
        //objCtrl.align = "right";
        objCtrl.title = "Show/Hide Calendar";
        cell.appendChild(objCtrl);

        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);
        objCtrl = myCalendar.createElement("input");
        objCtrl.type = 'button';
        objCtrl.name = myCalendar.ControlId['buttonSelect'];
        objCtrl.id = myCalendar.ControlId['buttonSelect'];
        objCtrl.value = 'Go';

        cell.appendChild(objCtrl);

        row = myCalendar.createElement("tr", tbody);
        cell = myCalendar.createElement("td", row);
        cell.setAttribute("colspan", "3");
        cell.id = myCalendar.ControlId['tdDisplay'];
        cell.name = myCalendar.ControlId['tdDisplay'];
        div.appendChild(table);
        divDummy = myCalendar.createElement("div");
        divDummy.appendChild(div);
        document.write(divDummy.innerHTML);
        myCalendar.created = true;

        //start: event handler
        objCtrl = myCalendar.getYearCtrl('text');
        myCalendar.addEvent(objCtrl, 'keypress', myCalendar.validYearKeys);
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.controlKeys4Year);
        myCalendar.addEvent(objCtrl, 'change', myCalendar.yearChanged);
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.keyDownHandler);

        objCtrl = myCalendar.getYearCtrl('ddl');
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);
        myCalendar.addEvent(objCtrl, 'change', myCalendar.yearChanged);

        objCtrl = myCalendar.getMonthCtrl();
        myCalendar.addEvent(objCtrl, 'change', myCalendar.monthChanged);
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);

        objCtrl = myCalendar.getDayCtrl();
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);

        objCtrl = myCalendar.getHourCtrl();
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);

        objCtrl = myCalendar.getMinuteCtrl();
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);

        objCtrl = myCalendar.getDayPartCtrl();
        myCalendar.addEvent(objCtrl, 'keydown', myCalendar.onEscape);

        objCtrl = myCalendar.getElementById('img$CalendarClose#');
        myCalendar.addEvent(objCtrl, 'click', myCalendar.actionHide);

        objCtrl = myCalendar.getElementById('btn$Select#');
        myCalendar.addEvent(objCtrl, 'click', myCalendar.dateSelectComplete);
        myCalendar.addEvent(objCtrl, 'keyDown', myCalendar.keyDownHandler);

        objCtrl = myCalendar.getElementById(myCalendar.ControlId['imgCalendar']);
        myCalendar.addEvent(objCtrl, 'click', myCalendar.showCalendar);
        //end: event handler
    }
}

myCalendar.setDate = function(obj) {
    var _value=myCalendar.getClientDateCtrl().value;
    var date$, _stemp;
    var x$, aData, _counter;
        
    _value=myCalendar.trimChar(_value, '  ', ' ');
    if (_value != "") {
        date$ = myCalendar.parseDate(_value);
        aData = myCalendar.getFormat();
        //year
        for(_counter=0; _counter<aData.length;_counter++) {
            if(aData[_counter]=="yyyy") {
                if (date$['yyyy']!='') {
                    x$ = myCalendar.getYearCtrl();
                    x$.value = date$['yyyy'];
                }
                break;
            }
        }
        //month
        for(_counter=0; _counter<aData.length;_counter++) {
            if(aData[_counter]=="mon") {
                if (date$['mon']!='') {
                    x$ = myCalendar.getMonthCtrl();
                    x$.selectedIndex=0;	//reset to blank
                    for(i=0; i< x$.length; i++) {
                        if (x$.options[i].value.toLowerCase()== date$['mon'].toString().toLowerCase()) {
                            x$.selectedIndex = i;
                            break;
                        }
                    }
                    myCalendar.monthChanged();
                }
                break;
            }
        }
        for(_counter=0; _counter<aData.length;_counter++) {
            if (aData[_counter]=="dd") {
                if (date$['dd'] != "") {
                    x$ = myCalendar.getDayCtrl();
                    x$.selectedIndex=0; //reset to blank
                    for(i=0; i< x$.length; i++) {
                        if (x$.options[i].value==date$['dd']) {
                            x$.selectedIndex = i;
                            break;
                        }
                    }
                }
            }
            else if(aData[_counter]=="hh") {
                if (date$['hh']!='') {
                    _stemp = myCalendar.padRight(date$['hh'], 2, '0');
                    x$ = myCalendar.getHourCtrl();
                    for(i=0; i< x$.length; i++) {
                        if (x$.options[i].value==_stemp) {
                            x$.selectedIndex = i;                
                            break;
                        }
                    }
                }
            }
            else if(aData[_counter]=="mm") {
                if (date$['mm']!='') {
                    _stemp = myCalendar.padRight(date$['mm'], 2, '0');                    
                    x$ = myCalendar.getMinuteCtrl();
                    for(i=0; i< x$.length; i++) {
                        if (x$.options[i].value==_stemp) {
                            x$.selectedIndex = i;                
                            break;
                        }
                    }
                }
            }
            else if(aData[_counter]=="ss") {
                if (date$['ss']!='') {
                    _stemp = myCalendar.padRight(date$['ss'], 2, '0');
                    x$ = myCalendar.getSecondCtrl();
                    for(i=0; i< x$.length; i++) {
                        if (x$.options[i].value==_stemp) {
                            x$.selectedIndex = i;                
                            break;
                        }
                    }
                }
            }
            else if(aData[_counter]=="ampm") {
                x$ = myCalendar.getDayPartCtrl();
                for(i=0; i< x$.length; i++) {
                    if (x$.options[i].value== date$['ampm']) {
                        x$.selectedIndex = i;
                        break;
                    }
                }            
            }
        }
    }
    else {
        myCalendar.getDayCtrl().selectedIndex = 0;
        myCalendar.getMonthCtrl().selectedIndex = 0;
        myCalendar.getYearCtrl().value="";
    }
}

myCalendar.positionSelf = function() {
    var objDiv = myCalendar.getCalendarCtrl();
    var p = myCalendar.getAbsolutePos(myCalendar.getElementById(myCalendar.eventTriggeredBy));    
	objDiv.style.left = p.x + "px";
	objDiv.style.top = p.y + "px";    
}

myCalendar.getAbsolutePos = function(el) {
	var SL = 0, ST = 0;
	var is_div = /^div$/i.test(el.tagName);
	if (is_div && el.scrollLeft)
		SL = el.scrollLeft;
	if (is_div && el.scrollTop)
		ST = el.scrollTop;
	var r = { x: el.offsetLeft - SL, y: el.offsetTop - ST };
	if (el.offsetParent) {
		var tmp = myCalendar.getAbsolutePos(el.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
}

myCalendar.onEscape = function(obj) {
	var _keyCode;
	_keyCode = myCalendar.getKeyCode(obj);
    if (_keyCode==27) {
        myCalendar.actionHide();
    }
}

myCalendar.getElementById = function(obj) {
	var _obj;
	_obj = document.getElementById(obj);
	if (_obj == null) {		
		_obj = document.getElementsByName(obj)[0];
	}
    return _obj;
}

myCalendar.setParameter = function(param) {    
    function param_default(pname, def) { if (typeof param[pname] == "undefined") { param[pname] = def; }};
    
    param_default("inputField",     null);
    
    
    param_default("button",         null);
    param_default("readOnly",   false);
    param_default("includeTime",   false);
    param_default("timeFormat",   "12");
    param_default("includeSeconds",   true);
    param_default("mode",   "date");
    param_default("dateTimeSeperator",   " ");
    param_default("timeSeperator",   ":");
    param_default("onSelectionCompleteCallBack", null);
    param_default("yearStart",   0);
    param_default("yearEnd",     0);
    
    if (param['button'] == null || param['inputField'] == null) {
        alert('Nothing to setup.');
        return false;
    }
    else {
        var _calendarParameter;
        if (typeof myCalendar.ctrlRef[param['button']] == "undefined") {
            myCalendar.ctrlRef[param['button']] = new Array();
        }
        myCalendar.ctrlRef[param['button']] ['inputField'] = param['inputField'];
        
        
        myCalendar.ctrlRef[param['button']] ['includeTime'] = param['includeTime'];
        myCalendar.ctrlRef[param['button']] ['timeFormat'] = param['timeFormat'];
        myCalendar.ctrlRef[param['button']] ['includeSeconds'] = param['includeSeconds'];
        myCalendar.ctrlRef[param['button']] ['mode'] = param['mode'];
        myCalendar.ctrlRef[param['button']] ['onSelectionCompleteCallBack'] = param['onSelectionCompleteCallBack'];
        myCalendar.ctrlRef[param['button']] ['dateTimeSeperator'] = param['dateTimeSeperator'];
        myCalendar.ctrlRef[param['button']] ['timeSeperator'] = param['timeSeperator'];
        myCalendar.ctrlRef[param['button']] ['yearStart'] = param['yearStart']+'';
        myCalendar.ctrlRef[param['button']] ['yearEnd'] = param['yearEnd']+'';
        
        if (param['readOnly']) {
            myCalendar.getElementById(param['inputField']).readOnly = true;
        }
        if (myCalendar.ctrlRef[param['button']] ['yearStart']!=0 && myCalendar.ctrlRef[param['button']] ['yearEnd']!=0) {
            if (myCalendar.ctrlRef[param['button']] ['yearStart'] > 0 && 
                    myCalendar.ctrlRef[param['button']] ['yearEnd'] > 0) {
                myCalendar.ctrlRef[param['button']] ['isYearddl'] = true;
            }
        }
    }
}

myCalendar.getParameterValue = function(para) {
    return myCalendar.ctrlRef[myCalendar.eventTriggeredBy][para];
}

myCalendar.padRight = function(sourceString, length, padCharacter) {
    var objtempString;
    objtempString = sourceString.toString();
    while(objtempString.length < length) {
        objtempString  = padCharacter + objtempString;           
    }
    return objtempString;
}

myCalendar.prepareForDisplay = function(obj) {
    if (myCalendar.getParameterValue('mode')=="time") {
        myCalendar.getTimeCtrl().style.display="block";
        myCalendar.getDateCtrl().style.display="none";
        if (myCalendar.getParameterValue('timeFormat')=="24") {
            myCalendar.getCellDayPartText().style.display="none";
            myCalendar.getCellDayPartCtrl().style.display="none";            
        }
        else {
            myCalendar.getCellDayPartText().style.display="block";
            myCalendar.getCellDayPartCtrl().style.display="block";
        }
        myCalendar.getElementById(myCalendar.ControlId['imgCalendar']).style.visibility="hidden";
    }
    else {        
        myCalendar.getDateCtrl().style.display="block";        
        if (myCalendar.getParameterValue('includeTime')) {
            myCalendar.getTimeCtrl().style.display="block";
        }
        else {
            myCalendar.getTimeCtrl().style.display="none";
        }
        if (myCalendar.getParameterValue('timeFormat')=="24") {
            myCalendar.getCellDayPartText().style.display="none";
            myCalendar.getCellDayPartCtrl().style.display="none";            
        }
        else {
            myCalendar.getCellDayPartText().style.display="block";
            myCalendar.getCellDayPartCtrl().style.display="block";
        }
        myCalendar.getElementById(myCalendar.ControlId['imgCalendar']).style.visibility="visible";
    }
}

myCalendar.getFormat = function(obj) {
    var _picture = new Array();
    var _pictureCount=0;
    if (myCalendar.getParameterValue('mode')=="time") {
        _picture[_pictureCount++]="hh";
        _picture[_pictureCount++]="mm";
        if (myCalendar.getParameterValue('includeSeconds')) {
        }
        if (myCalendar.getParameterValue('timeFormat')=="12") {
            _picture[_pictureCount++]="ampm";
        }
    }
    else {
        _picture[_pictureCount++] = "dd";
        _picture[_pictureCount++] = "mon";
        _picture[_pictureCount++] = "yyyy";        
        if (myCalendar.getParameterValue('includeTime')) {
            _picture[_pictureCount++]="hh";
            _picture[_pictureCount++]="mm";
            if (myCalendar.getParameterValue('includeSeconds')) {
            }
            if (myCalendar.getParameterValue('timeFormat')=="12") {
                _picture[_pictureCount++]="ampm";
            }
        }
    }
    return _picture;
}

myCalendar.trimChar = function(srcString, findChar, replaceChar) {    
    while(srcString.indexOf(findChar) >0) {
        srcString = srcString.replace(findChar, replaceChar);
    }
    return srcString;
}

myCalendar.resolveSeperator = function(sDate) {
    var _nCount=0;
    var _seperator;
    if (sDate.indexOf('-')>0) {
        _seperator = '-';
        _nCount++;
    }
    if (sDate.indexOf('/')>0) {
        _seperator = '/';
        _nCount++;
    }
    if (sDate.indexOf('\\')>0) {
        _seperator = '\\';
        _nCount++;
    }
    if (sDate.indexOf('.')>0) {
        _seperator = '.';
        _nCount++;
    }
    if (_nCount!=1) {
        _seperator="";  //could not resolve seperator
    }
    return _seperator;
}

myCalendar.parseDate = function(sValue) {
    var _seperator;
    var _sDate, _sTime, _sDayPart;
    var aData, date$;
    if (myCalendar.getParameterValue('mode')=="time") {
        _sTime = sValue.split(' ')[0];
        _sDayPart = sValue.split(' ')[1];    
    }
    else {
        _sDate = sValue.split(' ')[0];
        _sTime = sValue.split(' ')[1];
        _sDayPart = sValue.split(' ')[2];    
    }
    _seperator = myCalendar.resolveSeperator(sValue);
    date$ = new Array();
    
    if (_seperator!="") {
        if (typeof _sDate != "undefined") {
            aData = _sDate.split(_seperator);
            date$['dd']=parseFloat(aData[0]);  date$['mon']=aData[1]; date$['yyyy']=aData[2];            
        }
        else {
            date$['dd']="";  date$['mon']=""; date$['yyyy']="";
        }    
    }
    else {
        date$['dd']="";  date$['mon']=""; date$['yyyy']="";
    }
        
    if (typeof _sTime != "undefined") {
        aData = _sTime.split(':');
        if (aData.length>0) {
            date$['hh']=aData[0]+"";    date$['mm']=aData[1]+"";    date$['ss']=aData[2]+"";
            date$['ampm'] = _sDayPart;
        }
        else {
            date$['hh']="";    date$['mm']="";    date$['ss']="";
            date$['ampm'] ="";
        }
    }
    else {
        date$['hh']="";    date$['mm']="";    date$['ss']="";
        date$['ampm'] ="";
    }
    return date$;
}

myCalendar.addEvent = function(el, eventName, eventHandler) {
    var _retVal=false;
    if (el.addEventListener) {
        _retVal = el.addEventListener(eventName, eventHandler, false);
    }
    else if (el.attachEvent) {
        _retVal = el.attachEvent('on'+eventName, eventHandler);
    }
    return _retVal;
}

myCalendar.removeEvent = function(el, eventName, eventHandler) {
    var _retVal=false;
    if (el.addEventListener) {
        _retVal = el.removeEventListener(eventName, eventHandler, false);        
    }
    else if (el.attachEvent) {
        _retVal = el.detachEvent('on'+eventName, eventHandler);
    }
    return _retVal;
}

myCalendar.hideShowCovered = function() {
    var tags = new Array("applet", "iframe", "select");
	var el = myCalendar.getCalendarCtrl();

	var p = myCalendar.getAbsolutePos(el);
	var SX1 = p.x;
	var SX2 = el.offsetWidth + SX1;
	var SY1 = p.y;
	var SY2 = el.offsetHeight + SY1;

	for (var k = tags.length; k > 0; ) {
		var ar = document.getElementsByTagName(tags[--k]);
		var cc = null;
		
		for (var i = ar.length; i > 0;) {
			cc = ar[--i];

			p = myCalendar.getAbsolutePos(cc);
			var CX1 = p.x;
			var CX2 = cc.offsetWidth + CX1;
			var CY1 = p.y;
			var CY2 = cc.offsetHeight + CY1;

			if (cc.id.toString().indexOf('$')!=-1 && cc.id.toString().indexOf('#')!=-1) {
			    continue;
			}			
			if ( myCalendar.isHidden && (
                ((CX1 >= SX1 || CX1 <= SX1) && ((CY1 >= SY1 && CY1 <= SY2) || (CY2 >= SY1 && CY2 <= SY2))) || 
                ((CX1 >= SX2 || CX1 <= SX2) && ((CY1 >= SY1 && CY1 <= SY2) || (CY2 >= SY1 && CY2 <= SY2))) || 
                ((CX2 >= SX1 || CX2 <= SX1) && ((CY1 >= SY1 && CY1 <= SY2) || (CY2 >= SY1 && CY2 <= SY2))) || 
                ((CX2 >= SX2 || CX2 <= SX2) && ((CY1 >= SY1 && CY1 <= SY2) || (CY2 >= SY1 && CY2 <= SY2)))
                )
			) {
				if (!cc.__msh_save_visibility) {				    
					cc.__msh_save_visibility = myCalendar.getVisibility(cc);
				}
				cc.style.visibility = "hidden";
			} 
			else {
				if (!cc.__msh_save_visibility) {
					cc.__msh_save_visibility = myCalendar.getVisibility(cc);
				}
				cc.style.visibility = cc.__msh_save_visibility;
			}
		}
	}
}

myCalendar.getVisibility = function (obj) {
	var value = obj.style.visibility;
//	if (!value) {
//		if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") { // Gecko, W3C
//			if (!Calendar.is_khtml)
//				value = document.defaultView.getComputedStyle(obj, "").getPropertyValue("visibility");
//			else
//				value = '';
//		} else if (obj.currentStyle) {
//			value = obj.currentStyle.visibility;
//		} else
//			value = '';
//	}
	return value;
}

myCalendar.keyDownHandler = function(obj) {
	var _keyCode;
	_keyCode = myCalendar.getKeyCode(obj);
    if (_keyCode==9) { 
    }
}

myCalendar.focusHandler = function(obj) {
    var _id;
    _id = event.srcElement.id;    
}

myCalendar.eventRaisedBy = function(obj) {	
	if (myCalendar.browser=="ns" || myCalendar.browser=="opera") {
		return obj.target.id;
	}
	else {
		return window.event.srcElement.id;
	}
}

myCalendar.getBrowserInfo = function() {
	if (navigator.appName=="Microsoft Internet Explorer") {
		myCalendar.browser="ie";
	}
	else if (navigator.appName=="Opera") {
		myCalendar.browser="opera";	
	}
	else if (navigator.appName=="Netscape") {
		myCalendar.browser="ns";	
	}
}

myCalendar.setStyleAttribute = function(el, style) {
	if (myCalendar.browser=="ie") {
		el.style.setAttribute("cssText", style);		
	}
	else {
		el.setAttribute("style", style);
	}
}

myCalendar.getKeyCode = function(obj) {
	if (myCalendar.browser=="ie") {
		return event.keyCode;
	}
	else {		
		return obj.which;
	}
}

myCalendar.clearCalendarDisplay = function() {
	myCalendar.getElementById(myCalendar.ControlId['tdDisplay']).innerHTML = "";
}

myCalendar.showCalendar = function(obj) {
	if (!myCalendar.isCalendarVisible) {		
		myCalendar.hideShowCovered();
		myCalendar.isHidden=true;
		myCalendar.showDayCalendar(myCalendar.getYear(), myCalendar.getMonthInNumber());
		myCalendar.isCalendarVisible=true;
		myCalendar.hideShowCovered();
		myCalendar.isHidden=false;
	}
	else {
		myCalendar.hideShowCovered();
		myCalendar.isHidden=true;
		myCalendar.isCalendarVisible=false;
		myCalendar.getElementById(myCalendar.ControlId['tdDisplay']).innerHTML = "";
		myCalendar.hideShowCovered();		
		myCalendar.isHidden=false;
	}
}

myCalendar.showDayCalendar = function(year, mth) {
	var d, mth, yr, cen, weekday;
	var nDay, nTotalDays, firstWeek;
	var nlastDay;
	var sCal;
	d = 1;

	if (mth== "")  mth = (new Date).getMonth()+1;
	if (year=="") year = (new Date).getFullYear();
	myCalendar.storedValues['year'] = year;
	myCalendar.storedValues['month'] = mth;
	
	nlastDay = myCalendar.Days_In_Month[mth-1];
	if (mth==2 && myCalendar.isLeapYear(year, mth)) {
		nlastDay = 29;
	}
	yr = year%100;
	cen = Math.floor(year/100);
	if( myCalendar.isLeapYear(year, mth) && (mth==1 || mth==2)) isLeapJanFeb=1;
	else isLeapJanFeb=0;
	weekday = (1*d + 1*myCalendar.monthCodes[mth-1] + 1*yr + 1*Math.floor(yr/4) - 2*(cen%4) - 1*isLeapJanFeb) % 7;
	sCal = "<table class='calendarDisplay' border=1 width='100%' valign='top'>";
	
	
	sCal += "<tr><td colspan='7' align='center' style='border:none'>";
	sCal += "<table class='calendarDisplayHead' border='0' cellspacing='0' cellpadding='0' valign='center' width='100%' style='border:none'><tr>"+ 
			"<td align='right' style='border:none'><img src='../images/down.gif' style='cursor:hand' onclick='myCalendar.scrollMonth(-1);'>&nbsp;" + myCalendar.MonthName[mth-1] + 
			"&nbsp;<img src='../images/up.gif' style='cursor:hand' onclick='myCalendar.scrollMonth(1);'>&nbsp;&nbsp;</td>" + 
			"<td align='left' style='border:none'>&nbsp;&nbsp;<img src='../images/down.gif' style='cursor:hand' onclick='myCalendar.scrollYear(-1);'>&nbsp;" + year + 
			"&nbsp;<img src='../images/up.gif' style='cursor:hand' onclick='myCalendar.scrollYear(1);'></td>" + 
			"</tr></table>";
	
	sCal +=  "</td></tr>";
	
	sCal += "<tr>";
	for(var i=0; i<myCalendar.weekdays.length; i++) {
		sCal += "<td align='center'>" + myCalendar.weekdays[i] + "</td>";
	}
	sCal += "</tr>"
	for(nDay=1,firstWeek=true; nDay <= nlastDay;) {
		sCal += "<tr>";
		if (firstWeek) {
			if(weekday==6) {
				j=1;
			}
			else {
				j = weekday+1;
			}
			if (weekday !=6) {
				for(x=1; x<=j; x++) {
					sCal += "<td>&nbsp;</td>";
				}
				j++;
			}
			firstWeek = false;
		}
		else {
			j=1;
		}
		for(; j <= 7 && nDay<=nlastDay; j++, nDay++) {
			sCal += "<td align='center' onclick='myCalendar.daySelected("+ nDay + ")'>" + nDay + "</td>";
		}
		if (nDay > myCalendar.Days_In_Month[mth-1]) {
			for(x=j; x<=7; x++) {
				sCal += "<td>&nbsp;</td>"
			}
		}
		sCal += "</tr>";
	}
	myCalendar.getElementById(myCalendar.ControlId['tdDisplay']).innerHTML = sCal;
}

myCalendar.scrollYear = function(no) {
	var nYear;
	nYear = parseFloat(myCalendar.storedValues['year']) + no;
	myCalendar.storedValues['year'] = nYear;
	myCalendar.showDayCalendar(nYear, myCalendar.storedValues['month']);
}

myCalendar.scrollMonth = function(no) {
	var nMonth;
	nMonth = myCalendar.storedValues['month'] + no;
	if (nMonth>12) {
		nMonth=1;		
		myCalendar.storedValues['year']++;
	}
	if (nMonth<1) {
		nMonth=12;
		myCalendar.storedValues['year']--;
	}
	myCalendar.storedValues['month'] = nMonth;
	myCalendar.showDayCalendar(myCalendar.storedValues['year'], nMonth);
}

myCalendar.daySelected = function(day) {
	myCalendar.getYearCtrl().value=myCalendar.storedValues['year'];
	myCalendar.getMonthCtrl().selectedIndex=myCalendar.storedValues['month'];
	myCalendar.monthChanged();
	myCalendar.getDayCtrl().value=day;
}

