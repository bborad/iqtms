using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Common;

namespace Ramp.UI
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect user to the specific page according to his/her security level id get form the loggedin user session
                if (Session["LoggedInUser"] != null)
                {
                    IUsers objUser = (IUsers)Session["LoggedInUser"];
                    if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.Employee))
                    {
                        // Only for testing need to change the URL when connfirmed where to move

                        dvUserBox.Visible = false;
                        lnkUnAssignTag.Visible = false;
                        divTag.Visible = false;

                        // hide
                        lnkoffline.Visible = false;
                        divConfig.Visible = false;  
                    }
                    else if (objUser.SecurityGroupID == Convert.ToInt32(UserSecurityLevel.EntryGate))
                    {
                        // Only for testing need to change the URL when connfirmed where to move
                        lnkoffline.Visible = false;
                        lnkCurrentTraffic.Visible = false;                       
                        lnkUnAssignTag.Visible = false;

                        divTag.Visible = false;
                        dvUserBox.Visible = false;
                        divConfig.Visible = false;
                        divReports.Visible = false;
                        // hide
                       

                         
                    }
                }
                else
                {
                    // only added the page for testing need to change the URL when connfirmed where to move
                    Response.Redirect("~/login.aspx");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
