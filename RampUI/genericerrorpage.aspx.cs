using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramp.UI
{
    public partial class genericerrorpage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Exception"] != null)
            {
                Exception ex = (Exception)Session["Exception"];
                if (ex != null)
                {
                    lblErrorMsg.Text = ex.InnerException.Message;
                    lblErrorMsg.Visible = true;
                }
                else
                {
                    lblErrorMsg.Text = "";
                    lblErrorMsg.Visible = false;
                }
                
            }

            Session["Exception"] = null;

        }
    }
}
