﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="modbusserver.aspx.cs" Inherits="Ramp.UI.modbusserver" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <div id="mid_section">
            <asp:Timer ID="UpdateTimer" runat="server" Interval="3000" OnTick="UpdateTimer_Tick">
            </asp:Timer>
            <div>
                <asp:UpdatePanel ID="updatepnl" runat="server" RenderMode="Inline" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <br />
                        <div style="text-align: center">
                            <table width="30%" style="border: 1px solid #333">
                                <tr>
                                    <td style="padding: 5px; border: 1px solid #333">
                                        TMS TO CSS COMMS STATUS
                                    </td>
                                    <td style="padding: 5px; border: 1px solid #333">
                                        <asp:Label ID="lblServerName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div>
                            <asp:ListView ID="lvModbusDetails" runat="server" ItemPlaceholderID="itemPlaceholder"
                                GroupPlaceholderID="groupPlaceholder" GroupItemCount="3" OnItemDataBound="lvModbusDetails_ItemDataBound">
                                <EmptyDataTemplate>
                                    <span style="font-size: 12px; text-align: center; color: #002F5E">No records </span>
                                </EmptyDataTemplate>
                                <LayoutTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="tblUsers">
                                        <tr runat="server" id="groupPlaceHolder" />
                                    </table>
                                </LayoutTemplate>
                                <GroupTemplate>
                                    <tr runat="server" id="padStatus">
                                        <td runat="server" id="itemPlaceholder" />
                                    </tr>
                                </GroupTemplate>
                                <ItemTemplate>
                                    <td width="33%" valign="top">
                                        <div class="modbus-status">
                                            <div>
                                                <asp:Label ID="padName" runat="server" Text='<%#Eval("PadName") %>' Font-Bold="true"></asp:Label></div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        STATUS
                                                    </td>
                                                    <td>
                                                        START REQUESTED
                                                    </td>
                                                    <td>
                                                        TRUCK AT PAD
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="hdnPadStatus" runat="server" Text='<%#Eval("PadStatus")%>'>
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="hdnPadStart" runat="server" Text='<%#Eval("PadStart") %>'>
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="hdnPadMatch" runat="server" Text='<%#Eval("PadMatch") %>'>
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <%--
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                      STATUS
                                                    </td>
                                                    <td>
                                                        START REQUESTED
                                                    </td>
                                                    <td>
                                                        TRUCK AT PAD
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblReady" runat="server" Text="READY" ForeColor="GrayText" Font-Bold = "true"></asp:Label><br />
                                                        <asp:Label ID="lblFault" runat="server" Text="FAULT" ForeColor="GrayText" Font-Bold = "true"></asp:Label><br />
                                                        <asp:Label ID="lblNotAvailable" runat="server" Text="NOT AVAILABLE" ForeColor="GrayText" Font-Bold = "true"></asp:Label><br />
                                                        <asp:Label ID="lblInvalid" runat="server" Text="INVALID" ForeColor="GrayText" Font-Bold = "true"></asp:Label>
                                                        <asp:HiddenField ID="hdnPadStatus" runat="server" Value='<%#Eval("PadStatus")%>'>
                                                        </asp:HiddenField>
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="imgPadStart" runat="server" AlternateText='<%#Eval("PadStart") %>'
                                                            Height="25px" Width="25px" />
                                                        <asp:HiddenField ID="hdnPadStart" runat="server" Value='<%#Eval("PadStart") %>'>
                                                        </asp:HiddenField>
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="imgPadMatch" runat="server" AlternateText='<%#Eval("PadMatch") %>'
                                                            Height="25px" Width="25px" />
                                                        <asp:HiddenField ID="hdnPadMatch" runat="server" Value='<%#Eval("PadMatch") %>'>
                                                        </asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </table>--%>
    </div>
</asp:Content>
