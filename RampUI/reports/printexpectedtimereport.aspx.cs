using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Ramp.UI.reports
{
    public partial class printexpectedtimereport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((Session["dateRange"] != null) && (Session["hopperID"] != null) && (Session["expTimeRpt"] != null))
                {
                    FillReportGrid();
                }
                else
                {
                    //divMessage.Visible = true;
                    //litMessage.Text = "No report selected to print";
                    //divPrintUp.Visible = false;
                    //divPrintDown.Visible = false;
                }
            }

        }


        protected void FillReportGrid()
        {
            try
            {
                lblDateRange.Text = Session["dateRange"].ToString();
                lblHopper.Text = Session["hopperID"].ToString();
                gvReport.DataSource = (DataTable)Session["expTimeRpt"];
                gvReport.DataBind();
            }
            catch (Exception)
            {

            }
        }

        //protected void lnkbtnBack_OnClick(object sender, EventArgs e)
        //{
        //    Response.Redirect(Request.UrlReferrer.ToString());
        //}






    }
}
