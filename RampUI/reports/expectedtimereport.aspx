﻿<%@ Page Title="Expected Time Report" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="expectedtimereport.aspx.cs" Inherits="Ramp.UI.reports.expectedtimereport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="mid_section">
    <center><b>EXPECTED TIME REPORT</b></center><br />
        <table width="98%" border="0" cellspacing="0" cellpadding="0" class="border_strcture">
            <tr>
                <td align="center">
                    <br />
                    <br />
                    <strong>Date:</strong>
                    <asp:DropDownList ID="ddlDateRange" runat="server" DataMember="ID" DataTextField="DateRange" />
                    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <b>PAD:</b>
                    <asp:DropDownList ID="ddlHoppers" runat="server" DataTextField="HopperName" DataValueField="ID"
                        DataMember="ID" />
                    <br />
                    <br />
                    <asp:Button ID="btnSearch" runat="server" CssClass="but" Text="Search" OnClick="btnSearch_Click" />
                    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnPrint" runat="server" Text="Print" class="but" OnClick="btnPrint_Click" />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false" Width="98%"
                        border="0" CellSpacing="0" CellPadding="0">
                        <Columns>
                            <asp:BoundField DataField="R" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField HeaderText="Configured" DataField="Configured" ItemStyle-Width="15%"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Average" DataField="Average" ItemStyle-Width="17%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Min" DataField="Minimum" ItemStyle-Width="19%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Max" DataField="Maximum" ItemStyle-Width="18%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="S.D." DataField="StandardDeviation" ItemStyle-Width="16%"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <EmptyDataTemplate>
                            No record(s) found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
