﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="systemstatusreport.aspx.cs"
    Title="System Status Report" Inherits="Ramp.UI.reports.systemstatusreport" MasterPageFile="~/masterpages/SiteMaster.Master" %>

<asp:Content ContentPlaceHolderID="body" ID="bodycontent" runat="server">
    <div id="mid_section">
        <center>
            <b>SYSTEM STATUS REPORT</b></center>
        <br />
        <div style="margin-left: 10px;">
            &nbsp;<h3>
                Hardware Status</h3>
            <br />
            <div style="border: 1px solid #000; margin-right: 10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="34%" valign="top">
                            <h5>
                                Loop Status</h5>
                            <asp:GridView ID="gvLoopStatus" runat="server" AutoGenerateColumns="false" Width="100%"
                                CssClass="border_strcture" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                PageSize="5" onpageindexchanged="gvLoopStatus_PageIndexChanged" 
                                onpageindexchanging="gvLoopStatus_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="Loop ID" DataField="LoopID" ItemStyle-Width="25%" />
                                    <asp:BoundField HeaderText="Serial No" DataField="SerialNo" ItemStyle-Width="25%" />
                                    <asp:BoundField HeaderText="Location" DataField="Location" ItemStyle-Width="25%" />
                                    <asp:BoundField HeaderText="Status" DataField="Status" ItemStyle-Width="25%" />
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td width="33%" valign="top">
                            <h5>
                                Reader Status</h5>
                            <asp:GridView ID="gvReaderStatus" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="border_strcture" CellPadding="0" DataSourceID="odsReaderStatus" AllowPaging="True"
                                PageSize="5">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ReaderSerialNo" ItemStyle-Width="33%">
                                        <ItemStyle Width="33%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Location" DataField="LocationName" ItemStyle-Width="33%">
                                        <ItemStyle Width="33%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" ItemStyle-Width="33%" DataField="ReaderStatus">
                                        <ItemStyle Width="33%"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td width="33%" valign="top">
                            <h5>
                                VMS Status</h5>
                            <asp:GridView ID="gvVMSStatus" runat="server" AutoGenerateColumns="false" Width="100%"
                                CssClass="border_strcture" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                PageSize="5" DataSourceID="odsVMSStatus">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="IPAddress" ItemStyle-Width="33%" />
                                    <asp:BoundField HeaderText="Location" DataField="LocationName" ItemStyle-Width="33%" />
                                    <asp:BoundField HeaderText="Status" DataField="VMSStatus" ItemStyle-Width="33%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No Records Found!</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="36%">
                        &nbsp;<h3>
                            Software Status</h3>
                        <br />
                        <div style="height: 150px; border: 1px solid #000; padding: 10px;">
                            DB Connection:
                            <asp:Literal ID="ltrlConnection" runat="server"></asp:Literal></div>
                    </td>
                    <td width="64%">
                        &nbsp;<h3>
                            Alerts</h3>
                        <br />
                        <div style="height: 150px; border: 1px solid #000; padding: 10px; overflow: auto;">
                            <asp:GridView ID="gvAlerts" runat="server" AutoGenerateColumns="false" Width="95%"
                                GridLines="None" CellSpacing="0" CellPadding="0" DataSourceID="odsAlerts" ShowHeader="false"
                                OnRowDataBound="gvAlerts_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td width="20%" align="left">
                                                        <%# Eval("AlertDate") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Description") %>
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Records Found!</EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:ObjectDataSource ID="odsReaderStatus" runat="server" SelectMethod="GetReaderStatusForSystemReport"
        TypeName="Ramp.DBConnector.Adapter.Readers"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="odsVMSStatus" runat="server" SelectMethod="GetVMSStatusForSystemReport"
        TypeName="Ramp.DBConnector.Common.DBConnectorUtility"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="odsAlerts" runat="server" SelectMethod="GetTodaysAltersForSystemReport"
        TypeName="Ramp.DBConnector.Common.DBConnectorUtility"></asp:ObjectDataSource>
    <%--<asp:ObjectDataSource ID="odsMarkers" runat="server" SelectMethod="GetMarkersStatus"
        TypeName="Ramp.UI.common.Utility"></asp:ObjectDataSource>--%>
</asp:Content>
