﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="printexpectedtimereport.aspx.cs"
    Inherits="Ramp.UI.reports.printexpectedtimereport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Expected Time Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="mid_section">
        <center>
            <b>EXPECTED TIME REPORT</b></center>
        <br />
        <table width="98%" border="0" cellspacing="0" cellpadding="0" class="border_strcture">
            <tr>
                <td align="center">
                    <br />
                    <br />
                    <strong>Date:</strong>
                    <asp:Label ID="lblDateRange" runat="server" />
                    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <b>PAD:</b>
                    <asp:Label ID="lblHopper" runat="server" />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:LinkButton ID="lnkbtnBackToReport" runat="server" Text="Go Back" OnClientClick="javascript:history.back(); return false;"></asp:LinkButton>
                    <%--OnClick="lnkbtnBack_OnClick"--%>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <br />
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" OnClientClick="javascript:self.print();return false;"
                        Text="Print report" UseSubmitBehavior="False"></asp:LinkButton>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false" Width="98%"
                        border="0" CellSpacing="0" CellPadding="0">
                        <Columns>
                            <asp:BoundField DataField="R" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField HeaderText="Configured" DataField="Configured" ItemStyle-Width="15%"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Average" DataField="Average" ItemStyle-Width="17%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Min" DataField="Minimum" ItemStyle-Width="19%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Max" DataField="Maximum" ItemStyle-Width="18%" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="S.D." DataField="StandardDeviation" ItemStyle-Width="16%"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <EmptyDataTemplate>
                            No record(s) found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <br />
                    <br />
                    <asp:LinkButton ID="lnkbtnUpPrint" runat="server" CausesValidation="False" OnClientClick="javascript:self.print();return false;"
                        Text="Print report" UseSubmitBehavior="False"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="right">
                 <br />
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Go Back" OnClientClick="javascript:history.back(); return false;"></asp:LinkButton>
                    <%--OnClick="lnkbtnBack_OnClick"--%>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
