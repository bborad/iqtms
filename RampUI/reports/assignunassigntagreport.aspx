<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignunassigntagreport.aspx.cs"
    Inherits="Ramp.UI.reports.assignunassigntagreport" MasterPageFile="~/masterpages/SiteMaster.Master"
    Theme="calendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

    <script src="../js/myCalendar.js" type="text/javascript"></script>

    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>TAG(S) REPORT</b></center>
            <br />
            <div>
                <div id="divMessage" runat="server" visible="false">
                    <table style="width: 25%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                        <tr>
                            <th align="left">
                                <center>
                                    <span style="color: Red;">
                                        <asp:Literal ID="litMessage" runat="server"></asp:Literal></span>
                                </center>
                            </th>
                        </tr>
                    </table>
                </div>
                <div id="divRemoveTagMsg" runat="server" visible="false">
                    <center>
                        <table style="width: 25%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                            <tr>
                                <th align="left" class="border_strcture1">
                                    Remove Tag(s)
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    <span style="color: Red;">
                                        <asp:Literal ID="litRmvMsg" runat="server"></asp:Literal></span>
                                </th>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnRmvOK" runat="server" Text="YES" class="but" OnClick="btnRmvOK_Click" />
                                    <asp:Button ID="btnRmvCancel" runat="server" Text="NO" class="but" OnClick="btnRmvCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
                <br />
                <table width="60%" class="border_strcture1">
                    <tr>
                        <td width="27%" valign="top">
                            <table class="border_strcture1" height="60px" width="100%">
                                <tr>
                                    <td>
                                        Tag IDs
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtTagID" runat="server" MaxLength="9" class="txtBox" Style="width: 135px"
                                            ValidationGroup="valGrpAddTags" CssClass="txtBox"></asp:TextBox>
                                        <asp:Button ID="btnAddTag" runat="server" Text="Add" CausesValidation="true" ValidationGroup="valGrpAddTags"
                                            class="but" OnClick="btnAddTag_Click" />
                                        <asp:RequiredFieldValidator ID="rfvRefrence" runat="server" ErrorMessage="* Required"
                                            ControlToValidate="txtTagID" Display="Dynamic" ValidationGroup="valGrpAddTags"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:FileUpload ID="upFileTags" runat="server" />
                                        <asp:RequiredFieldValidator ID="reqValFileTags" runat="server" Display="Dynamic"
                                            ErrorMessage="* Required" ControlToValidate="upFileTags" ValidationGroup="valGrpUpAddTags"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regexpupFileTags" runat="server" Display="Dynamic"
                                            ErrorMessage="Only csv/text files are allowed." ControlToValidate="upFileTags"
                                            ValidationGroup="valGrpUpAddTags" ValidationExpression="^([a-zA-Z].*|[1-9].*)\.(((t|T)(x|X)(t|T))|((c|C)(s|S)(v|V)))$"></asp:RegularExpressionValidator>
                                        <br />
                                        <br />
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" CausesValidation="true" ValidationGroup="valGrpUpAddTags"
                                            class="but" OnClick="btnUpload_OnClick" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table class="border_strcture1" height="60px" width="100%">
                                <tr>
                                    <td>
                                        Date
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        From:&nbsp;<asp:TextBox ID="txtFromDate" runat="server" CssClass="txtBox"></asp:TextBox>
                                        <img id="imgCalFrom" src="../images/smcal.gif" alt="" />

                                        <script type="text/javascript">
                                            myCalendar.setup({ inputField: 'ctl00_body_txtFromDate', button: "imgCalFrom", dateFormat: 1 }); 
                                        </script>

                                        <asp:RegularExpressionValidator ID="regexpvalfromdate" runat="server" ControlToValidate="txtFromDate"
                                            Display="dynamic" CssClass="error" ErrorMessage="valid date format is dd/mm/yyyy "
                                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To:&nbsp;<asp:TextBox ID="txtToDate" runat="server"
                                            CssClass="txtBox"></asp:TextBox>
                                        <img id="imgCalTo" src="../images/smcal.gif" alt="" />

                                        <script type="text/javascript">
                                            myCalendar.setup({ inputField: 'ctl00_body_txtToDate', button: "imgCalTo", dateFormat: 1 }); 
                                        </script>

                                        <asp:RegularExpressionValidator ID="regexpvalToDate" runat="server" ControlToValidate="txtToDate"
                                            Display="Dynamic" ErrorMessage="Valid date format is dd/mm/yyyy " ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                            <div style="text-align: right; padding-top: 5px;">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" class="but" OnClick="btnSearch_Click" /></div>
                        </td>
                        <td width="12%" valign="top">
                            <table class="border_strcture1" height="150px" width="100%">
                                <tr>
                                    <td>
                                        Tag IDs
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListBox ID="lstAddRemoveTag" runat="server" SelectionMode="Multiple" DataTextField="TagID"
                                            CssClass="add_remove_box setVisible" DataValueField="TagID" Height="210px"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="text-align: right;">
                                            <asp:Button ID="btnRemove" runat="server" Text="Remove" class="but" OnClick="btnRemove_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="12%" valign="top">
                            <table class="border_strcture1" height="165px" width="100%">
                                <tr>
                                    <td>
                                        PADs
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="add_remove_box" style="height: 247px; width: 153px">
                                            <asp:CheckBoxList ID="chklstPads" runat="server" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="15%" valign="top">
                            <table class="border_strcture1" height="145px" width="100%">
                                <tr>
                                    <td>
                                        Proponents
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="add_remove_box" style="height: 247px; width: 150px">
                                            <asp:CheckBoxList ID="chklstProponents" runat="server" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="60%">
                    <tr>
                        <td class="border_strcture1" width="18%">
                            Total Tag(s) :
                            <asp:Literal ID="litTotalTag" runat="server"></asp:Literal>
                        </td>
                        <td class="border_strcture1" width="25%">
                            <asp:RadioButtonList ID="rdobtnList" runat="server" RepeatDirection="Horizontal"
                                Width="380px" OnSelectedIndexChanged="rdobtnList_OnSelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Selected="True" Value="0" Text="Assign/Un-Assign Tag Report"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Issue Tag Report"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td id="tdReferenceNo" class="border_strcture1" width="25%" runat="server">
                            Reference No :
                            <asp:DropDownList ID="ddlReferenceNo" runat="server" OnSelectedIndexChanged="ddlReferenceNo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="right" width="10%">
                            <asp:Button ID="btnPrint" runat="server" Text="Print" class="but" OnClick="btnPrint_Click" />
                            <%-- <b>
                                <asp:HyperLink ID="lnkPrint" runat="server" NavigateUrl="~/reports/printtagreport.aspx">Print Report</asp:HyperLink></b>--%>
                        </td>
                    </tr>
                </table>
                <div id="divAssignUnAssignTags" runat="server" visible="false">
                    <table width="61%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="85%" valign="top">
                                <asp:GridView ID="gvAssignUnAssignTags" runat="server" AutoGenerateColumns="False"
                                    Width="100%" DataKeyNames="TagRFID" border="0" CellSpacing="0" CellPadding="2"
                                    CssClass="border_strcture" OnPageIndexChanging="gvAssignUnAssignTags_PageIndexChanging"
                                    PageSize="10" AllowPaging="true" AllowSorting="true" OnSorting="gvAssignUnAssignTags_Sorting">
                                    <Columns>
                                        <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                            SortExpression="ID" Visible="false" />
                                        <asp:BoundField DataField="TagRFID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                            SortExpression="TagRFID" />
                                        <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Left" HeaderText="Truck ID"
                                            SortExpression="TruckID" />
                                        <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Left" HeaderText="Hopper ID"
                                            SortExpression="HopperID" Visible="false" />
                                        <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD ID"
                                            SortExpression="HopperName" />
                                        <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent ID"
                                            SortExpression="ProponentID" Visible="false" />
                                        <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                            SortExpression="ProponentName" />
                                        <asp:BoundField DataField="AssignDate" ItemStyle-HorizontalAlign="Left" HeaderText="Assign Date"
                                            SortExpression="AssignDate" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="BatteryStatus" ItemStyle-HorizontalAlign="Left" HeaderText="Battery"
                                            SortExpression="BatteryStatus" />
                                        <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                            SortExpression="Assigned" Visible="false" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divIssueTags" runat="server" visible="false">
                    <table width="61%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="85%" valign="top">
                                <asp:GridView ID="gvIssueTags" runat="server" AutoGenerateColumns="False" Width="100%"
                                    DataKeyNames="ID" border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture"
                                    OnPageIndexChanging="gvIssueTags_PageIndexChanging" PageSize="10" AllowPaging="true"
                                    AllowSorting="true" OnSorting="gvIssueTags_Sorting">
                                    <Columns>
                                        <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                            SortExpression="ID" Visible="false" />
                                        <asp:BoundField DataField="TagID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                            SortExpression="TagID" Visible="false" />
                                        <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Left" HeaderText="Truck ID"
                                            SortExpression="TruckID" />
                                        <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Left" HeaderText="Hopper ID"
                                            SortExpression="HopperID" Visible="false" />
                                        <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD"
                                            SortExpression="HopperName" />
                                        <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent ID"
                                            SortExpression="ProponentID" Visible="false" />
                                        <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                            SortExpression="ProponentName" />
                                        <asp:BoundField DataField="Issued" ItemStyle-HorizontalAlign="Left" HeaderText="Assign Date"
                                            SortExpression="Issued" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="TruckRegNo" ItemStyle-HorizontalAlign="Left" HeaderText="Reference"
                                            SortExpression="TruckRegNo" />
                                        <asp:BoundField DataField="BatteryStatus" ItemStyle-HorizontalAlign="Left" HeaderText="Battery"
                                            SortExpression="BatteryStatus" />
                                        <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                            SortExpression="Assigned" Visible="false" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
