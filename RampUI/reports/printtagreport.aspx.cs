using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Data;
using Ramp.DBConnector.Adapter;

namespace Ramp.UI.reports
{
    public partial class printtagreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((Session["strlstTagCsv"] != null) && (Session["HopperIDs"] != null) && (Session["ProponentIDs"] != null) && (Session["FromDate"] != null) && (Session["ToDate"] != null)
                    && (Session["ReportType"] != null))
                {
                    FillReportGrid();
                }
                else
                {
                    divReportCriteria.Visible = true;
                    divMessage.Visible = true;
                    litMessage.Text = "No report selected to print";
                    divPrintUp.Visible = false;
                    divPrintDown.Visible = false;
                }
            }
        }

        /// <summary>
        /// Fill Report grid
        /// </summary>
        protected void FillReportGrid()
        {
            try
            {
                litTotal.Text = "0";
                divPrintUp.Visible = true;
                divPrintDown.Visible = true;
                ArrayList lstTagCsv = new ArrayList();
                StringBuilder HopperIDs = new StringBuilder("");
                StringBuilder ProponentIDs = new StringBuilder("");
                string strFromDate = string.Empty;
                string strToDate = string.Empty;
                string ReferenceNo = string.Empty;
                //string strTagRFIDs = string.Empty;
                // CSV data into session

                lstTagCsv = (ArrayList)Session["strlstTagCsv"];
                HopperIDs = (StringBuilder)Session["HopperIDs"];
                ProponentIDs = (StringBuilder)Session["ProponentIDs"];
                strFromDate = Convert.ToString(Session["FromDate"]);
                strToDate = Convert.ToString(Session["ToDate"]);
                ReferenceNo = Convert.ToString(Session["ReferenceNo"]);

                // From date
                string FromDate = string.Empty;
                if (strFromDate != string.Empty)
                {
                    string[] arrFromDate = strFromDate.Split('/');
                    FromDate = Convert.ToString(arrFromDate[1]) + "/" + Convert.ToString(arrFromDate[0]) + "/" + Convert.ToString(arrFromDate[2]);
                }
                else
                {
                    FromDate = string.Empty;
                }

                // To date
                string ToDate = string.Empty;
                if (strToDate != string.Empty)
                {
                    string[] arrToDate = strToDate.Split('/');
                    ToDate = Convert.ToString(arrToDate[1]) + "/" + Convert.ToString(arrToDate[0]) + "/" + Convert.ToString(arrToDate[2]);
                }
                else
                {
                    ToDate = string.Empty;
                }

                // Get Tag report value i.e. Assign/Un-Assign or Issue Tag report and call search method accordingly

                ArrayList lstDataTables = new ArrayList();
                DataTable dtResult = null;
                if (Convert.ToString(Session["ReportType"]) == "Assign")
                {
                    litTitle.Text = "ASSIGN/UN-ASSIGN TAG(S) REPORT";
                    foreach (StringBuilder TagRFIDs in lstTagCsv)
                    {
                        dtResult = Tags.AssignUnAssignTagReport(Convert.ToString(TagRFIDs), Convert.ToString(HopperIDs), Convert.ToString(ProponentIDs), FromDate, ToDate);
                        lstDataTables.Add(dtResult);
                    }
                    DataTable dtResultList = null;
                    //DataTable dtIssueTagsList = null;
                    foreach (DataTable dtReportResult in lstDataTables)
                    {
                        dtResultList = new DataTable();
                        dtResultList.Merge(dtReportResult);
                    }

                    litTotal.Text = dtResultList.Rows.Count.ToString();

                    // Set the visibility of result grid as per the report value selected i.e. Assign/Un-Assign or Issue Tag report
                    divIssueTags.Visible = false;
                    divAssignUnAssignTags.Visible = true;
                    // Set result data into respective grid that Assign/Un-Assign report or Issue Tag report
                    gvAssignUnAssignTags.DataSource = dtResultList;
                    gvAssignUnAssignTags.DataBind();
                }
                else if (Convert.ToString(Session["ReportType"]) == "Issued")
                {
                    litTitle.Text = "ISSUED TAG(S) REPORT";
                    foreach (StringBuilder TagRFIDs in lstTagCsv)
                    {
                        dtResult = Tags.IssueTagReport(Convert.ToString(TagRFIDs), Convert.ToString(HopperIDs), Convert.ToString(ProponentIDs), FromDate, ToDate,ReferenceNo);
                        lstDataTables.Add(dtResult);
                    }
                    DataTable dtResultList = null;
                    //DataTable dtIssueTagsList = null;
                    foreach (DataTable dtReportResult in lstDataTables)
                    {
                        dtResultList = new DataTable();
                        dtResultList.Merge(dtReportResult);
                    }
                    // Set the visibility of result grid as per the report value selected i.e. Assign/Un-Assign or Issue Tag report

                    litTotal.Text = dtResultList.Rows.Count.ToString();

                    divAssignUnAssignTags.Visible = false;
                    divIssueTags.Visible = true;

                    // Set result data into respective grid that Assign/Un-Assign report or Issue Tag report
                    gvIssueTags.DataSource = dtResultList;
                    gvIssueTags.DataBind();
                }
                else
                {
                    divMessage.Visible = true;
                    litMessage.Text = "No report selected to print";
                    divPrintUp.Visible = false;
                    divPrintDown.Visible = false;
                }

                // Set report criteria values on page
                divReportCriteria.Visible = true;

                if ((FromDate == string.Empty) && (ToDate == string.Empty))
                {
                    litFromDate.Text = "<b>Date Range:</b> All";
                }
                else
                {
                    if (FromDate == string.Empty)
                    {
                        litFromDate.Text = "";
                    }
                    else
                    {
                        litFromDate.Text = "<b>Date From:</b> " + FromDate;
                    }

                    if (ToDate == string.Empty)
                    {
                        litDateTo.Text = "";
                    }
                    else
                    {
                        litDateTo.Text = "<b>Date To:</b> " + ToDate;
                    }

                }
                //litDateTo.Text = ToDate;
                //litFromDate.Text = FromDate;

                //if (ToDate == string.Empty)
                //{
                //    litDateTo.Text = "All";
                //}
                //else
                //{
                //    litDateTo.Text = ToDate;
                //}
                //if (FromDate == string.Empty)
                //{
                //    litFromDate.Text = "All";
                //}
                //else
                //{
                //    litFromDate.Text = FromDate;
                //}
                //if (Convert.ToString(HopperIDs) == "-1")
                //{
                //    litPads.Text = "All";
                //}
                //else
                //{
                //    litPads.Text = Convert.ToString(Session["PadsName"]);
                //}
                //if (Convert.ToString(ProponentIDs) == "-1")
                //{
                //    litProponents.Text = "All";
                //}
                //else
                //{
                //    litProponents.Text = Convert.ToString(Session["ProponentsName"]);
                //}
                //foreach (StringBuilder TagRFIDs in lstTagCsv)
                //{
                //    strTagRFIDs += TagRFIDs;
                //}
                //if (strTagRFIDs == "'-1'")
                //{
                //    litTagIDs.Text = "All";
                //}
                //else
                //{
                //    litTagIDs.Text = strTagRFIDs.Replace(",",", ");
                //    litTagIDs.Text = litTagIDs.Text.Replace("'","");
                //}
            }
            catch (Exception Ex)
            {
                divMessage.Visible = true;
                litMessage.Text = Ex.Message;
            }
        }

        /// <summary>
        ///  Back to tag report button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnBackTagReport_OnClick(object sender, EventArgs e)
        {
            Session["strlstTagCsv"] = null;
            Session["HopperIDs"] = null;
            Session["ProponentIDs"] = null;
            Session["FromDate"] = null;
            Session["ToDate"] = null;
            Session["ReportType"] = null;
            Response.Redirect("~/reports/assignunassigntagreport.aspx");
        }

        protected void lnkbtnUpPrint_Click(object sender, EventArgs e)
        {

        }
    }
}
