using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.Common;
using Ramp.DBConnector.Adapter;
using System.Collections;
using System.Text;

namespace Ramp.UI.reports
{
    public partial class assignunassigntagreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tdReferenceNo.Visible = false;
                if ((Session["IssuedTagPage"] != null))
                {
                    if (Convert.ToBoolean(Session["IssuedTagPage"]) == true)
                    {
                        rdobtnList.ClearSelection();
                        rdobtnList.Items[1].Selected = true;
                        tdReferenceNo.Visible = true;

                       // rdobtnList.AutoPostBack = true;
                       
                    }

                    Session.Remove("IssuedTagPage");  
                }

                ClearAllControls();
            }
        }

        #region Add Tags

        /// <summary>
        /// Upload text/csv file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            try
            {
                // Upload CSV file in Upload file folder of the application
                if (upFileTags.PostedFile.FileName.Trim() != "")
                {
                    string fn = System.IO.Path.GetFileName(upFileTags.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("../UploadFiles") + "\\" + fn;
                    if (SaveLocation.Trim().Length > 0)
                    {
                        FileInfo fi = new FileInfo(SaveLocation);
                        if (fi.Exists)//if file exists then delete it
                        {
                            fi.Delete();
                        }
                    }
                    try
                    {
                        upFileTags.PostedFile.SaveAs(SaveLocation);
                        // Import tag ids from the CSV file and list them into list box
                        ImportFile(SaveLocation, fn);

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Import data from CSV / text files
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private void ImportFile(string filePath, string fileName)
        {
            divMessage.Visible = false;
            DataSet ds = new DataSet();
            try
            {
                // Create a connection string DBQ attribute sets the path of directory which contains CSV or text files 
                string strConnString = @"Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Server.MapPath("../UploadFiles") + ";";
                string sql_select;
                System.Data.Odbc.OdbcConnection conn;
                //Create connection to CSV/text file
                conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());
                //Open the connection 
                conn.Open();
                //Fetch records from CSV/text
                sql_select = "select * from [" + fileName + "]";
                //Initialise DataAdapter with file data
                DataAdapter obj_oledb_da = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);
                //Fill dataset with the records from CSV file
                obj_oledb_da.Fill(ds);
                //Declare variable for Error
                string strError = "";
                //Close Connection to CSV file
                conn.Close();
                // Validate Dataset is empty or not
                int tagsRemoved = 0;
                int tagsadded = 0;
                int totaltags = 0;

                if (ds != null)
                {
                    bool IsCloExists = ColumnExists(ds);
                    if (IsCloExists == false)
                    {
                        divMessage.Visible = true;
                        litMessage.Text = "Please enter a file with TagID";
                        return;
                    }

                    if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                    {
                        totaltags = ds.Tables[0].Rows.Count;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            // Remove the tagid if its length is less than 9
                            //if (Convert.ToString(dr["TagID"]).Length < 9)
                            //{
                            //    dr.Delete();
                            //    tagsRemoved++;
                            //}

                            if ((dr["TagID"] != DBNull.Value))
                            {
                                int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();

                                if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1))
                                {
                                    dr.Delete();
                                    tagsRemoved++;
                                }
                            }
                        }
                        ds.Tables[0].AcceptChanges();
                    }
                }
                // Total tags added from the CSV
                tagsadded = totaltags - tagsRemoved;
                //ViewState["tagsadded"] = tagsadded;
                // Assign dataset value to tag list
                if (ds != null)
                {
                    if (lstAddRemoveTag.Items.Count == 0)
                    {
                        if (ds != null)
                        {
                            lstAddRemoveTag.DataSource = ds;
                            lstAddRemoveTag.DataBind();
                            //DisableAllButtons();
                        }
                    }
                    else
                    {
                        if (ds.Tables != null && ds.Tables[0].Rows.Count != 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if ((dr["TagID"] != DBNull.Value))
                                {
                                    int TagIDcount = (ds.Tables[0].Select("TagID=" + "'" + Convert.ToString(dr["TagID"]) + "'")).Count();
                                    if ((Convert.ToString(dr["TagID"]).Length < 9) || (TagIDcount > 1))
                                    {
                                        dr.Delete();
                                        tagsRemoved++;
                                    }
                                }

                                lstAddRemoveTag.Items.Add(Convert.ToString(dr["TagID"]));
                            }
                        }
                    }
                }
                int dupTags = RemoveDuplicateTagsFromList();
                if (totaltags != 0)
                {
                    //EnableAllButtons();
                    //divMessage.Visible = true;
                    //litMsg.Text = "Tags imported successfully from CSV/text file <br/> Total Tags: " + totaltags + "<br/> Tags added: " + tagsadded + "<br/> Tags removed due to inconsistency: " + tagsRemoved;
                    if (ViewState["tagsadded"] != null)
                    {
                        ViewState["tagsadded"] = Convert.ToInt32(ViewState["tagsadded"]) + tagsadded - dupTags;
                    }
                    else
                    {
                        ViewState["tagsadded"] = tagsadded - dupTags;
                    }
                    // litTotalUpTags.Text = Convert.ToString(ViewState["tagsadded"]);
                }
                else
                {
                    divMessage.Visible = true;
                    //litMsg.Text = "No tags exists in CSV/text file <br/> Total Tags: " + totaltags + "<br/> Tags added: " + tagsadded + "<br/> Tags removed due to inconsistency: " + tagsRemoved;
                    litMessage.Text = "No tag(s) exists in CSV file";

                }
            }
            catch (Exception) //Error
            {
                throw;
            }
        }

        /// <summary>
        /// Add all tags to database table Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            try
            {
                divMessage.Visible = false;
                if (lstAddRemoveTag.Items.Count != 0)
                {
                    Tags objTags = new Tags();

                    // List all the tags in CSV form 
                    ArrayList lstTagCsv = new ArrayList();
                    StringBuilder strLstTagIDs = new StringBuilder("-1");

                    foreach (ListItem tagitem in lstAddRemoveTag.Items)//lstbox is ID of ListBox
                    {
                        //To set all tag ids in a single CSV string
                        if (strLstTagIDs.Length >= 7500) // To check the length value supported by SQL to insert record properly
                        {
                            lstTagCsv.Add(strLstTagIDs);
                            strLstTagIDs = new StringBuilder("-1");
                        }
                        else
                        {
                            strLstTagIDs.Append("," + tagitem.Value);
                        }
                    }

                    // Insert bulk Tagids by CSV string

                    if (strLstTagIDs.Length >= 9)
                    {
                        lstTagCsv.Add(strLstTagIDs);
                    }

                    // Insert bulk nuber of tags using CSV string
                    int countaddedrows = 0;
                    foreach (StringBuilder strCsv in lstTagCsv)
                    {
                        countaddedrows = countaddedrows + objTags.InsertTags(Convert.ToString(strCsv));
                    }

                    /// Message to show number of Tags added successfully
                    divMessage.Visible = true;
                    if (countaddedrows > 0)
                    {
                        litMessage.Text = countaddedrows + " Tag(s) added successfully ";
                    }
                    else
                    {
                        litMessage.Text = "No tag(s) added ";
                    }

                    // Clear list after successful insertion of records
                    lstAddRemoveTag.Items.Clear();
                    ViewState["tagsadded"] = null;
                    ViewState["tagsFromReader"] = null;
                    //litTotalUpTags.Text = "0";
                    //litTotReadTags.Text = "0";
                }
                else
                {
                    divMessage.Visible = true;
                    litMessage.Text = "Tag(s) List is empty, Please select appropriate tag(s) file or Reader to add tag(s) in tag(s) list";
                    //DisableAllButtons();
                }
            }
            catch (Exception) //Error
            {
                throw;
            }
        }

        /// <summary>
        /// Disable all div messages
        /// </summary>
        private void DisableAllDivMessage()
        {
            divMessage.Visible = false;
            divRemoveTagMsg.Visible = false;
        }

        /// <summary>
        /// Add Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddTag_Click(object sender, EventArgs e)
        {
            try
            {
                //EnableAllButtons();
                divMessage.Visible = false;
                if ((txtTagID.Text != string.Empty) && (txtTagID.Text.Length > 8))
                {
                    lstAddRemoveTag.Items.Add(Convert.ToString(txtTagID.Text));
                }
                txtTagID.Text = string.Empty;
                int dupTags = RemoveDuplicateTagsFromList();
            }
            catch (Exception ex)
            {
                //litExMsg.Text = ex.Message;
            }
        }

        #endregion Add Tags

        #region Remove Tags

        /// <summary>
        /// Remove selected tags from the tags list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstAddRemoveTag.Items.Count > 0)
            {
                DisableAllDivMessage();
                divRemoveTagMsg.Visible = true;
                litRmvMsg.Text = "Do you want to remove the tag(s) from list?";
            }
            else
            {
                divMessage.Visible = true;
                litMessage.Text = "Tags List is empty, Please select appropriate tag(s) file or add tags in tag(s) list";
            }
        }

        /// <summary>
        /// Remove Tags 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRmvOK_Click(object sender, EventArgs e)
        {
            try
            {
                DisableAllDivMessage();
                if (lstAddRemoveTag.Items.Count != 0)
                {
                    //EnableAllButtons();
                    // Remove slected tags from the list
                    int tagsRemovebyRemovebtn = 0;
                    bool tagRemaoved = false;
                    for (int i = 0; i < lstAddRemoveTag.Items.Count; i++)
                    {
                        if (lstAddRemoveTag.Items[i].Selected)
                        {
                            lstAddRemoveTag.Items.RemoveAt(i);
                            i = i - 1;
                            tagRemaoved = true;
                            tagsRemovebyRemovebtn++;
                        }
                    }
                    ViewState["tagsRemovebyRemovebtn"] = tagsRemovebyRemovebtn;

                    if (tagRemaoved == true)
                    {
                        //litMsg.Text = tagsRemovebyRemovebtn + " selected tags get removed";
                        //btnOK.Visible = false;
                        //divRemoveTagMsg.Visible = false;
                    }
                    else
                    {
                        divMessage.Visible = true;
                        litMessage.Text = "Please select the tag(s) to remove";
                        //btnOK.Visible = false;
                        //divRemoveTagMsg.Visible = false;
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    litMessage.Text = "Tag(s) List is empty, Please select appropriate Tags file or add tags in tag(s) list";
                    //DisableAllButtons();

                }
                if (lstAddRemoveTag.Items.Count == 0)
                {
                    //ResetControls();
                    divMessage.Visible = true;
                    litMessage.Text = "Tag(s) List is empty, Please select appropriate Tags file or add tags in tag(s) list";
                    //DisableAllButtons();
                }
                else
                {
                    //EnableAllButtons();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cancel remove Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRmvCancel_Click(object sender, EventArgs e)
        {
            DisableAllDivMessage();
            litRmvMsg.Text = string.Empty;
            divRemoveTagMsg.Visible = false;
            lstAddRemoveTag.SelectedIndex = -1;
        }

        /// <summary>
        /// Column exists
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="ColumnName"></param>
        /// <returns></returns>
        private bool ColumnExists(DataSet ds)
        {
            bool colExists1 = false;
            bool colExists = false;
            foreach (DataColumn col in ds.Tables[0].Columns)
            {
                if (col.ColumnName == "TagID")
                {
                    colExists1 = true;
                }
            }
            if ((colExists1 == true))
            {
                colExists = true;
            }
            return colExists;
        }

        /// <summary>
        /// Remove duplicate records from tag list
        /// </summary>
        private int RemoveDuplicateTagsFromList()
        {
            try
            {
                int rmvTags = 0;
                bool flgExists = false;
                ArrayList lstTags = new ArrayList();
                foreach (ListItem li in lstAddRemoveTag.Items)
                {
                    if (!lstTags.Contains(Convert.ToString(li)))
                    {
                        lstTags.Add(Convert.ToString(li));
                    }
                    else
                    {
                        rmvTags++;
                        flgExists = true;
                    }
                }

                lstAddRemoveTag.Items.Clear();
                foreach (string tag in lstTags)
                {
                    lstAddRemoveTag.Items.Add(tag);
                }
                if (flgExists == true)
                {
                    divMessage.Visible = true;
                    //litMsg.Text = "Some tag(s) already exists in tag(s) list";
                    //btnOK.Visible = false;
                }
                return rmvTags;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion Remove Tags

        #region PADS

        private void FillHoppers()
        {
            try
            {
                DataTable dtHoppers = Hoppers.GetAllHoppers();

                if (dtHoppers != null)
                {
                    foreach (DataRow dr in dtHoppers.Rows)
                    {
                        if (dr["ID"] != null)
                        {
                            if ((Convert.ToInt32(dr["ID"]) == 0) && (Convert.ToString(dr["HopperName"]) == "--Select--"))
                            {
                                dr.Delete();
                                break;
                            }
                        }
                    }
                    dtHoppers.AcceptChanges();
                    if (dtHoppers.Rows.Count != 0)
                    {
                        chklstPads.DataValueField = "ID";
                        chklstPads.DataTextField = "HopperName";
                        chklstPads.DataSource = dtHoppers;
                        chklstPads.DataBind();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void FillReferenceNo()
        {
            try
            {
                DataTable dtReferenceNo = DBConnector.Common.DBConnectorUtility.GetAllRefernceNo();  

                if (dtReferenceNo != null)
                {

                    if (dtReferenceNo.Columns.Count == 0)
                    {
                        dtReferenceNo.Columns.Add(new DataColumn("ReferenceNo"));
                    }

                    DataRow dr = dtReferenceNo.NewRow();
                    dr["ReferenceNo"] = "-- Select --";

                    dtReferenceNo.Rows.InsertAt(dr, 0);

                    dtReferenceNo.AcceptChanges();

                    ddlReferenceNo.DataSource = dtReferenceNo;
                    ddlReferenceNo.DataValueField = "ReferenceNo";
                    ddlReferenceNo.DataTextField = "ReferenceNo";

                    ddlReferenceNo.DataBind();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Proponents

        /// <summary>
        /// Get all proponents
        /// </summary>
        private void FillProponents()
        {
            try
            {
                DataTable dtProponents = Hoppers.GetAllProponents();

                if (dtProponents != null)
                {
                    foreach (DataRow dr in dtProponents.Rows)
                    {
                        if (dr["ID"] != null)
                        {
                            if ((Convert.ToInt32(dr["ID"]) == 0) && (Convert.ToString(dr["ProponentName"]) == "--Select--"))
                            {
                                dr.Delete();
                                break;
                            }
                        }
                    }
                    dtProponents.AcceptChanges();
                    if (dtProponents.Rows.Count != 0)
                    {
                        chklstProponents.DataValueField = "ID";
                        chklstProponents.DataTextField = "ProponentName";
                        chklstProponents.DataSource = dtProponents;
                        chklstProponents.DataBind();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ArrayList lstTagCsv = new ArrayList();
            StringBuilder HopperIDs = new StringBuilder("");
            StringBuilder ProponentIDs = new StringBuilder("");

            // Create Tags CSV
            lstTagCsv = CreateTagsCSV();

            // Create PADs CSV

            HopperIDs = CreatePadsCSV();

            // Create Proponents CSV
            ProponentIDs = CreateProponentsCSV();

            // From date
            string FromDate = string.Empty;
            if (txtFromDate.Text.Trim() != string.Empty)
            {
                string[] arrFromDate = txtFromDate.Text.Split('/');
                FromDate = Convert.ToString(arrFromDate[1]) + "/" + Convert.ToString(arrFromDate[0]) + "/" + Convert.ToString(arrFromDate[2]);
            }
            else
            {
                FromDate = string.Empty;
            }

            // To date
            string ToDate = string.Empty;
            if (txtToDate.Text.Trim() != string.Empty)
            {
                string[] arrToDate = txtToDate.Text.Split('/');
                ToDate = Convert.ToString(arrToDate[1]) + "/" + Convert.ToString(arrToDate[0]) + "/" + Convert.ToString(arrToDate[2]);
            }
            else
            {
                ToDate = string.Empty;
            }

            // CSV data into session

            Session["strlstTagCsv"] = lstTagCsv;
            Session["HopperIDs"] = HopperIDs;
            Session["ProponentIDs"] = ProponentIDs;
            Session["FromDate"] = txtFromDate.Text.Trim();
            Session["ToDate"] = txtToDate.Text.Trim();
          

            // Get Tag report value i.e. Assign/Un-Assign or Issue Tag report and call search method accordingly

            ArrayList lstDataTables = new ArrayList();
            DataTable dtResult = null;
            if (rdobtnList.Items[0].Selected == true)
            {
                Session["ReportType"] = "Assign";
                foreach (StringBuilder TagRFIDs in lstTagCsv)
                {
                    dtResult = Tags.AssignUnAssignTagReport(Convert.ToString(TagRFIDs), Convert.ToString(HopperIDs), Convert.ToString(ProponentIDs), Convert.ToString(FromDate), Convert.ToString(ToDate));
                    lstDataTables.Add(dtResult);
                }
                DataTable dtResultList = null;
                //DataTable dtIssueTagsList = null;
                foreach (DataTable dtReportResult in lstDataTables)
                {
                    dtResultList = new DataTable();
                    dtResultList.Merge(dtReportResult);
                }
                Session["dtResult"] = dtResultList;
                litTotalTag.Text = Convert.ToString(dtResultList.Rows.Count);
                // Set the visibility of result grid as per the report value selected i.e. Assign/Un-Assign or Issue Tag report
                divIssueTags.Visible = false;
                divAssignUnAssignTags.Visible = true;
                // Set result data into respective grid that Assign/Un-Assign report or Issue Tag report
                gvAssignUnAssignTags.DataSource = dtResultList;
                gvAssignUnAssignTags.DataBind();


            }
            else if (rdobtnList.Items[1].Selected == true)
            {
                Session["ReportType"] = "Issued";

                string ReferenceNo = "";

                if (ddlReferenceNo.SelectedIndex > 0)
                {
                    ReferenceNo = ddlReferenceNo.SelectedValue.Trim();
                }

                Session["ReferenceNo"] = ReferenceNo.Trim();

                foreach (StringBuilder TagRFIDs in lstTagCsv)
                {
                    dtResult = Tags.IssueTagReport(Convert.ToString(TagRFIDs), Convert.ToString(HopperIDs), Convert.ToString(ProponentIDs), Convert.ToString(FromDate), Convert.ToString(ToDate), ReferenceNo);
                    lstDataTables.Add(dtResult);
                }
                DataTable dtResultList = null;
                //DataTable dtIssueTagsList = null;
                foreach (DataTable dtReportResult in lstDataTables)
                {
                    dtResultList = new DataTable();
                    dtResultList.Merge(dtReportResult);
                }
                Session["dtResult"] = dtResultList;
                litTotalTag.Text = Convert.ToString(dtResultList.Rows.Count);
                // Set the visibility of result grid as per the report value selected i.e. Assign/Un-Assign or Issue Tag report
                divAssignUnAssignTags.Visible = false;
                divIssueTags.Visible = true;

                // Set result data into respective grid that Assign/Un-Assign report or Issue Tag report
                gvIssueTags.DataSource = dtResultList;
                gvIssueTags.DataBind();
            }
            else
            {
                divMessage.Visible = true;
                litMessage.Text = "Please select report type (Assign/Un-Assign Report OR Issue Report)";
            }

            // Reset all the controls
        }

        /// <summary>
        /// Create Tags CSV
        /// </summary>
        private ArrayList CreateTagsCSV()
        {
            try
            {
                ArrayList lstTagCsv = new ArrayList();
                StringBuilder strLstTagIDs = new StringBuilder("'-1'");
                divMessage.Visible = false;
                if (lstAddRemoveTag.Items.Count != 0)
                {


                    // List all the tags in CSV form 
                    foreach (ListItem tagitem in lstAddRemoveTag.Items)//lstbox is ID of ListBox
                    {
                        //To set all tag ids in a single CSV string
                        if (strLstTagIDs.Length >= 6000) // To check the length value supported by SQL to insert record properly
                        {
                            lstTagCsv.Add(strLstTagIDs);
                            strLstTagIDs = new StringBuilder("'-1'");
                        }
                        else
                        {
                            strLstTagIDs.Append("," + "'" + tagitem.Value + "'");
                        }
                    }

                    // Insert bulk Tagids by CSV string

                    if (strLstTagIDs.Length >= 9)
                    {
                        lstTagCsv.Add(strLstTagIDs);
                    }

                    //// Insert bulk nuber of tags using CSV string
                    //int countaddedrows = 0;
                    //foreach (StringBuilder strCsv in lstTagCsv)
                    //{
                    //    //countaddedrows = countaddedrows + objTags.InsertTags(Convert.ToString(strCsv));
                    //}

                    ///// Message to show number of Tags added successfully
                    //divMessage.Visible = true;
                    //if (countaddedrows > 0)
                    //{
                    //    //litMessage.Text = countaddedrows + " Tag(s) added successfully ";
                    //}
                    //else
                    //{
                    //    //litMessage.Text = "No tag(s) added ";
                    //}

                    //// Clear list after successful insertion of records
                    //lstAddRemoveTag.Items.Clear();
                    ////ViewState["tagsadded"] = null;
                    ////ViewState["tagsFromReader"] = null;
                    ////litTotalUpTags.Text = "0";
                    ////litTotReadTags.Text = "0";

                    return lstTagCsv;
                }
                else
                {
                    //divMessage.Visible = true;
                    //litMessage.Text = "Tag(s) List is empty, Please select appropriate tag(s) file or Reader to add tag(s) in tag(s) list";
                    lstTagCsv.Add(strLstTagIDs);
                    return lstTagCsv;
                }
            }
            catch (Exception) //Error
            {
                throw;
            }
        }

        /// <summary>
        /// Create PADs CSV
        /// </summary>
        private StringBuilder CreatePadsCSV()
        {
            // control and display the selected items.
            StringBuilder strPads = new StringBuilder("-1");
            StringBuilder strPadsName = new StringBuilder("");
            for (int i = 0; i < chklstPads.Items.Count; i++)
            {
                if (chklstPads.Items[i].Selected)
                {
                    strPads.Append("," + chklstPads.Items[i].Value);
                    strPadsName.Append(", " + chklstPads.Items[i].Text);

                }
            }
            Session["PadsName"] = strPadsName;
            return strPads;
        }

        /// <summary>
        /// Create Proponents CSV
        /// </summary>
        private StringBuilder CreateProponentsCSV()
        {
            // control and display the selected items.
            StringBuilder strProponents = new StringBuilder("-1");
            StringBuilder strProponentsName = new StringBuilder("");
            for (int i = 0; i < chklstProponents.Items.Count; i++)
            {
                if (chklstProponents.Items[i].Selected)
                {
                    strProponents.Append("," + chklstProponents.Items[i].Value);
                    strProponentsName.Append(", " + chklstProponents.Items[i].Text);

                }
            }
            Session["ProponentsName"] = strProponentsName;
            return strProponents;
        }

        /// <summary>
        /// Page index change for Assign, UnAssign Tag report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAssignUnAssignTags_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["dtResult"] != null)
            {
                //bind the values to our GridView
                gvAssignUnAssignTags.PageIndex = e.NewPageIndex;
                gvAssignUnAssignTags.DataSource = (DataTable)Session["dtResult"];
                gvAssignUnAssignTags.DataBind();
                //EnableControls();
            }
        }

        /// <summary>
        /// Page index change for Issue Tags
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvIssueTags_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["dtResult"] != null)
            {
                //bind the values to our GridView
                gvIssueTags.PageIndex = e.NewPageIndex;
                gvIssueTags.DataSource = (DataTable)Session["dtResult"];
                gvIssueTags.DataBind();
                //EnableControls();
            }
        }

        /// <summary>
        /// change report type i.e. Assign/UnAssign tag report or Issue tag report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdobtnList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (rdobtnList.SelectedValue == "0")
            {
                tdReferenceNo.Visible = false;
            }
            else
            {
                tdReferenceNo.Visible = true;
            }
            ClearAllControls();
        }

        /// <summary>
        /// Clear all controls
        /// </summary>
        private void ClearAllControls()
        {
            txtFromDate.Text = string.Empty;
            txtToDate.Text = string.Empty;
            lstAddRemoveTag.Items.Clear();
            FillHoppers();
            FillProponents();
            if (tdReferenceNo.Visible)
            {
                FillReferenceNo();
            }
            Session["dtResult"] = null;
            gvAssignUnAssignTags.DataSource = null;
            gvAssignUnAssignTags.DataBind();
            gvIssueTags.DataSource = null;
            gvIssueTags.DataBind();
            litTotalTag.Text = "0";
            Session["strlstTagCsv"] = null;
            Session["HopperIDs"] = null;
            Session["ProponentIDs"] = null;
            Session["FromDate"] = null;
            Session["ToDate"] = null;
            Session["ReportType"] = null;
        }

        /// <summary>
        /// Get Sorting direction for Assign/Unassign tag report
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private string GetSortDirectionAssignUnAssignTagReport(string column)
        {
            //By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            //Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpressionAssignUnAssign"] as string;

            if (sortExpression != null)
            {
                //Check if the same column is being sorted.
                //Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirectionAssignUnAssign"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            //Save new values in ViewState.
            ViewState["SortDirectionAssignUnAssign"] = sortDirection;
            ViewState["SortExpressionAssignUnAssign"] = column;

            return sortDirection;
        }

        /// <summary>
        /// Apply sorting on Assign/Unassign tag report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAssignUnAssignTags_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the DataTable from the session object.
            DataTable dt = (DataTable)Session["dtResult"];

            if (dt != null)
            {
                //Sort the table.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirectionAssignUnAssignTagReport(e.SortExpression);
                //Bind the sorted table to the gridview.
                gvAssignUnAssignTags.DataSource = dt;
                gvAssignUnAssignTags.DataBind();
                //Update the UpdatePanel or the changes won't show.
                //up.Update();
            }
        }

        /// <summary>
        /// Get Sorting direction for Issued tag report
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private string GetSortDirectionIssuedTagReport(string column)
        {
            //By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            //Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpressionIssuedTags"] as string;

            if (sortExpression != null)
            {
                //Check if the same column is being sorted.
                //Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirectionIssuedTags"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            //Save new values in ViewState.
            ViewState["SortDirectionIssuedTags"] = sortDirection;
            ViewState["SortExpressionIssuedTags"] = column;

            return sortDirection;
        }

        /// <summary>
        /// Apply sorting on Issued tag report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvIssueTags_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the DataTable from the session object.
            DataTable dt = (DataTable)Session["dtResult"];

            if (dt != null)
            {
                //Sort the table.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirectionIssuedTagReport(e.SortExpression);
                //Bind the sorted table to the gridview.
                gvIssueTags.DataSource = dt;
                gvIssueTags.DataBind();
                //Update the UpdatePanel or the changes won't show.
                //up.Update();
            }
        }

        /// <summary>
        /// Print Tag(s) report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/printtagreport.aspx");
        }

        protected void ddlReferenceNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}















