using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Ramp.RFIDHWConnector.Adapters;
using System.Data;

namespace Ramp.UI.reports
{
    public partial class systemstatusreport : System.Web.UI.Page
    {
        DataTable dtMarkers;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //check the connection
                if (checkConnection())
                {
                    ltrlConnection.Text = "Connected";
                }
                else
                {
                    ltrlConnection.Text = "Not Connected";
                }

               // dtMarkers = Reader.GetMarkersStatus();

                dtMarkers = Ramp.UI.common.Utility.GetMarkersStatus();

                Session["MarkersData"] = dtMarkers;

                gvLoopStatus.DataSource = dtMarkers;
                gvLoopStatus.DataBind();
            }
        }

        public static bool checkConnection()
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["activeConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(ConnectionString);
            try
            {
                conn.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void gvAlerts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }

        }

        protected void gvLoopStatus_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvLoopStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["MarkersData"] == null)
            {
                dtMarkers = Ramp.UI.common.Utility.GetMarkersStatus();
                Session["MarkersData"] = dtMarkers;
            }
            else
            {
                dtMarkers = (DataTable)Session["MarkersData"];
            } 
            gvLoopStatus.PageIndex = e.NewPageIndex;
            gvLoopStatus.DataSource = dtMarkers;
            gvLoopStatus.DataBind();
        }

    }
}
