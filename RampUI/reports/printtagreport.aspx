﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="printtagreport.aspx.cs"
    Inherits="Ramp.UI.reports.printtagreport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Tag(s) Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap-page">
        <div id="header">
            <%--<img src="../images/iq-tms-logo.jpg" width="249" height="83" />--%>
            <asp:Image ID="logo" runat="server" AlternateText="IQ-TMS" ImageUrl="~/images/iq-tms-logo.jpg"
                Width="249" Height="83" />
        </div>
        <div id="navigation">
        </div>
        <div id="mid_section">
            <div id="divMessage" runat="server" visible="false">
                <table style="width: 25%" cellpadding="0" cellspacing="0" border="0" class="border_strcture1">
                    <tr>
                        <th align="left">
                            <center>
                                <span style="color: Red;">
                                    <asp:Literal ID="litMessage" runat="server"></asp:Literal></span>
                            </center>
                        </th>
                    </tr>
                </table>
            </div>
            <div id="divReportCriteria" runat="server" visible="false">
                <table width="61%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th style="font-size: 12px">
                            <asp:Literal ID="litTitle" runat="server"></asp:Literal>
                        </th>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="lnkbtnbacktagreport" runat="server" Text="Go Back" OnClick="lnkbtnBackTagReport_OnClick"></asp:LinkButton>
                            <%-- <asp:HyperLink ID="lnkbacktagreport" runat="server" NavigateUrl="~/reports/assignunassigntagreport.aspx">back to tag(s) report</asp:HyperLink>--%>
                        </td>
                    </tr>
                    <%-- <tr>
                        <th align="left">
                            &nbsp; Report Criteria
                        </th>
                    </tr><tr>
                        <td valign="top">
                            <b>TagIDs:</b>
                            <asp:Literal ID="litTagIDs" runat="server"></asp:Literal>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <asp:Literal ID="litFromDate" runat="server"></asp:Literal>
                            <asp:Literal ID="litDateTo" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <%--  <tr>
                        <td>
                            <b>Total : </b>
                            <asp:Literal ID="litTotal" runat="server"></asp:Literal>
                        </td>
                    </tr>     --%>
                </table>
            </div>
            <div id="divPrintUp" runat="server" visible="false">
                <table width="61%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <b>Total Tag(s): </b>
                            <asp:Literal ID="litTotal" runat="server"></asp:Literal>
                        </td>
                        <td align="right" valign="top">
                            <%--<a href="#" onclick="print();">Print Tag(s) Report</a>--%>
                            <%--<asp:Button ID="btnPrint" runat="server" Text="Print Tag(s) Report" />--%>
                            <asp:LinkButton ID="lnkbtnUpPrint" runat="server" CausesValidation="False" OnClientClick="javascript:self.print();return false;"
                                Text="Print Tag(s) Report" UseSubmitBehavior="False"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divAssignUnAssignTags" runat="server" visible="false">
                <table width="61%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="85%" valign="top">
                            <asp:GridView ID="gvAssignUnAssignTags" runat="server" AutoGenerateColumns="False"
                                Width="100%" DataKeyNames="TagRFID" border="0" CellSpacing="0" CellPadding="2"
                                CssClass="border_strcture">
                                <Columns>
                                    <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                        SortExpression="ID" Visible="false" />
                                    <asp:BoundField DataField="TagRFID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                        SortExpression="TagRFID" />
                                    <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Left" HeaderText="Truck ID"
                                        SortExpression="TruckID" />
                                    <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Left" HeaderText="Hopper ID"
                                        SortExpression="HopperID" Visible="false" />
                                    <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD ID"
                                        SortExpression="HopperName" />
                                    <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent ID"
                                        SortExpression="ProponentID" Visible="false" />
                                    <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                        SortExpression="ProponentName" />
                                    <asp:BoundField DataField="AssignDate" ItemStyle-HorizontalAlign="Left" HeaderText="Assign Date"
                                        SortExpression="AssignDate" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="BatteryStatus" ItemStyle-HorizontalAlign="Left" HeaderText="Battery"
                                        SortExpression="BatteryStatus" />
                                    <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                        SortExpression="Assigned" Visible="false" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No records found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divIssueTags" runat="server" visible="false">
                <table width="61%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="85%" valign="top">
                            <asp:GridView ID="gvIssueTags" runat="server" AutoGenerateColumns="False" Width="100%"
                                DataKeyNames="ID" border="0" CellSpacing="0" CellPadding="2" CssClass="border_strcture">
                                <Columns>
                                    <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Right" HeaderText="ID"
                                        SortExpression="ID" Visible="false" />
                                    <asp:BoundField DataField="TagID" ItemStyle-HorizontalAlign="Left" HeaderText="Tag ID"
                                        SortExpression="TagID" Visible="false" />
                                    <asp:BoundField DataField="TruckID" ItemStyle-HorizontalAlign="Left" HeaderText="Truck ID"
                                        SortExpression="TruckID" />
                                    <asp:BoundField DataField="HopperID" ItemStyle-HorizontalAlign="Left" HeaderText="Hopper ID"
                                        SortExpression="HopperID" Visible="false" />
                                    <asp:BoundField DataField="HopperName" ItemStyle-HorizontalAlign="Left" HeaderText="PAD"
                                        SortExpression="HopperName" />
                                    <asp:BoundField DataField="ProponentID" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent ID"
                                        SortExpression="ProponentID" Visible="false" />
                                    <asp:BoundField DataField="ProponentName" ItemStyle-HorizontalAlign="Left" HeaderText="Proponent"
                                        SortExpression="ProponentName" />
                                    <asp:BoundField DataField="Issued" ItemStyle-HorizontalAlign="Left" HeaderText="Assign Date"
                                        SortExpression="Issued" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TruckRegNo" ItemStyle-HorizontalAlign="Left" HeaderText="Reference"
                                        SortExpression="TruckRegNo" />
                                    <asp:BoundField DataField="BatteryStatus" ItemStyle-HorizontalAlign="Left" HeaderText="Battery"
                                        SortExpression="BatteryStatus" />
                                    <asp:BoundField DataField="Assigned" ItemStyle-HorizontalAlign="Left" HeaderText="Assigned"
                                        SortExpression="Assigned" Visible="false" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No records found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divPrintDown" runat="server" visible="false">
                <table width="61%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right" valign="top">
                            <%--<a href="#" onclick="print();">Print Tag(s) Report</a>--%>
                            <%--<asp:Button ID="btnPrint" runat="server" Text="Print Tag(s) Report" />--%>
                            <asp:LinkButton ID="lnkbtnDownPrint" runat="server" CausesValidation="False" OnClientClick="javascript:self.print();return false;"
                                Text="Print Tag(s) Report" UseSubmitBehavior="False"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        <br />
                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Go Back" OnClick="lnkbtnBackTagReport_OnClick"></asp:LinkButton>
                            <%-- <asp:HyperLink ID="lnkbacktagreport" runat="server" NavigateUrl="~/reports/assignunassigntagreport.aspx">back to tag(s) report</asp:HyperLink>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
