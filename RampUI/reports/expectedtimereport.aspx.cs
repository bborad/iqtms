using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.UI.reports
{
    public partial class expectedtimereport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadDropboxes();

                    Session["dateRange"] = null;
                    Session["hopperID"] = null;
                    Session["expTimeRpt"] = null;
                }
            }
            catch (Exception)
            {

            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception)
            {

            }
        }


        public void LoadDropboxes()
        {
            try
            {
                FillHoppers();
                FillDateRange();
            }
            catch (Exception)
            {

            }
        }


        private void FillHoppers()
        {
            try
            {

                DataTable dtHoppers = Hoppers.GetAllHoppers();
                if (dtHoppers != null)
                {

                    if (dtHoppers.Rows.Count != 0)
                    {
                        dtHoppers.Rows[0].Delete();
                        dtHoppers.AcceptChanges();

                        ddlHoppers.DataTextField = "HopperName";
                        ddlHoppers.DataValueField = "ID";


                        ddlHoppers.DataSource = dtHoppers;
                        ddlHoppers.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                // display error message
            }
        }


        private void FillDateRange()
        {
            try
            {

                DataTable dtDateRange = (DataTable)ExpectedTimes.GetDateRange().Tables[0];
                if (dtDateRange != null)
                {
                    if (dtDateRange.Rows.Count != 0)
                    {
                        ddlDateRange.DataSource = dtDateRange;
                        ddlDateRange.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                // display error message
            }
        }


        private void FillGrid()
        {
            try
            {

                int HopperID, TableID, ID;

                HopperID = Convert.ToInt32(ddlHoppers.SelectedValue);

                if (ddlDateRange.SelectedIndex == 0)
                {
                    TableID = 1;
                }
                else
                {
                    TableID = 2;
                }


                if (ddlDateRange.SelectedIndex == 0 || ddlDateRange.SelectedIndex == 1)
                {
                    ID = 1;
                }
                else
                {
                    ID = Convert.ToInt32(ddlDateRange.SelectedIndex);
                }



                DataTable dtResult = ExpectedTimes.GetReport(HopperID, TableID, ID).Tables[0];
                if (dtResult != null)
                {

                    if (dtResult.Rows.Count != 0)
                    {

                        gvReport.DataSource = dtResult;
                        gvReport.DataBind();

                        Session["expTimeRpt"] = dtResult;
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (gvReport.Rows.Count > 0)
            {
                Session["dateRange"] = ddlDateRange.SelectedItem.Text;
                Session["hopperID"] = ddlHoppers.SelectedValue;
                //Session["expTimeRpt"] = (DataTable)gvReport.DataSource;

                Response.Redirect("printexpectedtimereport.aspx");
            }
        }





    }
}
