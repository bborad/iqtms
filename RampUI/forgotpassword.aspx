﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs"
    Inherits="Ramp.UI.forgotpassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forgot Password</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap-page">
        <div id="header">
            <%--<img src="../images/iq-tms-logo.jpg" width="249" height="83" />--%>
            <asp:Image ID="logo" runat="server" AlternateText="IQ-TMS" ImageUrl="~/images/iq-tms-logo.jpg"
                Width="249" Height="83" />
        </div>
        <div id="navigation">
        </div>
        <div id="mid_section">
            <table class="border_strcture1" align="center">
                <tr>
                    <th colspan="2">
                        <center>
                            FORGOT PASSWORD</center>
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        <span style="color: Red;">
                            <asp:Label ID="lblErrorMsg" runat="server" SkinID="ErrorLabel"></asp:Label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="right">
                        User Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" MaxLength="150" Width="175px" CssClass="txtBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalusername" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage=" Required" ValidationGroup="valGrpForgotPass"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="right">
                        EmailID:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="175px" CssClass="txtBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage=" Required" ValidationGroup="valGrpForgotPass"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegExEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="valGrpForgotPass"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSendPassword" runat="server" Text="Send Password" OnClick="btnSendPassword_Click"
                            ValidationGroup="valGrpForgotPass" CssClass="but" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                            CssClass="but" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:HyperLink ID="hlBackLogin" runat="server" NavigateUrl="~/login.aspx">Back to Login</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="text-align: center;padding-top:5px;">
       <asp:Label ID = "lblVersion" runat = "server"></asp:Label>
    </div>
    </form>
</body>
</html>
