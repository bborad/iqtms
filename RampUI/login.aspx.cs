using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.DBConnector.Adapter;
using Ramp.UI.common;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.UI
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblVersion.Text = Utility.VersionNo;
            if (!IsPostBack)
            {
                object value = System.Configuration.ConfigurationManager.AppSettings["Version"];
                if (value != null)
                {
                    lblVersion.Text = "Version " + Convert.ToString(value);
                }
                else
                {
                    lblVersion.Text = Utility.VersionNo;
                }
            }

           
        }

        /// <summary>
        /// Check that username and password exists or not and redirect to the required page or login page as per result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Session["LoggedInUser"] = null;
            // Check Username and password
            IUsers objUser = Users.UserLoginCheck(txtUserName.Text.Trim(), txtPassword.Text.Trim());
            lblErrorMsg.Visible = true;
            if (objUser != null)
            {
                if (objUser.ID == -1)
                {
                    lblErrorMsg.Text = "Invalid user name or password";
                }
                else
                {
                    //lblErrorMsg.Text = "Valid Username and Password";
                    Session["LoggedInUser"] = objUser;
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                lblErrorMsg.Text = "Invalid user name or password";
                Session["LoggedInUser"] = null;
            }
        }

        /// <summary>
        /// Forgot Password page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnForgotPass_Click(object sender, EventArgs e)
        {
            // If username is entered then send it with URL otherwise not send
            if (txtUserName.Text == string.Empty)
            {
                Response.Redirect("forgotpassword.aspx");
            }
            else
            {
                Response.Redirect("forgotpassword.aspx?UN=" + txtUserName.Text.Trim());
            }
        }

    }
}
