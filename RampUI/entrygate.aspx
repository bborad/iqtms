﻿<%@ Page Title="Entry Gate" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="entrygate.aspx.cs" Inherits="Ramp.UI.entrygate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

  

    <asp:ScriptManager runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div id="mid_section">
        <center>
            <b>ENTRY GATE</b></center>
        <br />
        <asp:Timer ID="UpdateTimer" runat="server" Interval="6000" OnTick="UpdateTimer_Tick">
        </asp:Timer>
        <asp:UpdatePanel ID="updatepnl" runat="server" RenderMode="Inline" ChildrenAsTriggers="false"
            UpdateMode="Conditional">
            <ContentTemplate>
                <table width="80%" border="0" align="center" cellpadding="2" cellspacing="2" class="border_strcture">
                    <tr>
                        <td colspan="2" align="right">
                            <%--<input name="" type="button" class="but" value="Re-read" />--%>
                            <asp:Button ID="btnReRead" runat="server" Text="Re-read" class="but" OnClick="btnReRead_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <strong>Lane 1</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="84%" valign="top">
                            <div class="manual_process_div">
                                <asp:GridView ID="gvLane1" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-HorizontalAlign="Left" border="0" CellSpacing="0" CellPadding="0">
                                    <Columns>
                                        <asp:BoundField DataField="TruckID" HeaderText="Truck ID" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="ProponentName" HeaderText="Proponent" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="HopperName" HeaderText="PAD" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="HopperStatus" HeaderText="Status" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="Entry1" HeaderText="Time" ItemStyle-Width="20%" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                        <%--<td id="colLane1Status" runat="server" width="16%" align="center" valign="middle" class="green_box" >--%>
                        <td id="colLane1Status" runat="server" width="16%" align="center" valign="middle">
                            <%--<asp:Label ID="lblLane1Status" runat="server" Text="1"></asp:Label>--%>
                            <asp:Label ID="lblLane1Status" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <%-- <td id="colLane1Msg" runat="server" class="border_strcture msg_green">--%>
                                    <td id="colLane1Msg" runat="server">
                                        <p>
                                            Message:
                                            <%--Proceed to Hopper--%>
                                            <%--<asp:Label ID="lblLane1Message" runat="server" Text="Proceed to Hopper."></asp:Label>--%>
                                            <asp:Label ID="lblLane1Message" runat="server"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="80%" border="0" align="center" cellpadding="2" cellspacing="2" class="border_strcture">
                    <tr>
                        <td colspan="2">
                            <strong>Lane 2</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="84%" valign="top">
                            <div class="manual_process_div">
                                <asp:GridView ID="gvLane2" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-HorizontalAlign="Left" border="0" CellSpacing="0" CellPadding="0">
                                    <Columns>
                                        <asp:BoundField DataField="TruckID" HeaderText="Truck ID" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="ProponentName" HeaderText="Proponent" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="HopperName" HeaderText="PAD" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="HopperStatus" HeaderText="Status" ItemStyle-Width="20%" />
                                        <asp:BoundField DataField="Entry1" HeaderText="Time" ItemStyle-Width="20%" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                        <%-- <td id="colLane2Status" runat="server" width="16%" align="center" valign="middle"
                    class="red_box">--%>
                        <td id="colLane2Status" runat="server" width="16%" align="center" valign="middle">
                            <%--<asp:Label ID="lblLane2Status" runat="server" Text="2"></asp:Label>--%>
                            <asp:Label ID="lblLane2Status" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <%-- <td id="colLane2Msg" runat="server" class="border_strcture msg_red">--%>
                                    <td id="colLane2Msg" runat="server">
                                        <p>
                                            Message:
                                            <%--Multiple Tags Found.--%>
                                            <%--<asp:Label ID="lblLane2Message" runat="server" Text="Multiple Tags Found."></asp:Label>--%>
                                            <asp:Label ID="lblLane2Message" runat="server"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID = "hdnRefreshCount" runat = "server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnReRead" />
                <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
        <%--  <table width="80%" border="0" cellspacing="2" cellpadding="2">
            <tr>
                <td align="right" class="status_report">
                    <img src="images/green_status.jpg" width="113" height="143" /><img src="images/wait.jpg"
                        width="117" height="143" /><img src="images/n_status.jpg" width="113" height="143" /><img
                            src="images/x_ststus.jpg" width="113" height="143" />
                </td>
            </tr>
        </table>--%>
    </div>
</asp:Content>
