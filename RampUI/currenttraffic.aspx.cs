using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Common;
using Ramp.RFIDHWConnector.Adapters;
using Ramp.UI.common;
using DBAdapter = Ramp.DBConnector.Adapter;
using Ramp.DBConnector.Common;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Configuration;


namespace Ramp.UI
{
    public partial class currenttraffic : System.Web.UI.Page
    {
        DataTable dtHoppers = null;
        Dictionary<string, string> SortOrder;


        DataSet dsResult = null;

        DataTable dtTraffic;

        public int ScreenRefereshInterval = 5;

        int PageAutoReloadInterval = 1800;


        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["TrafficTable"] != null)
            {
                dtTraffic = (DataTable)Session["TrafficTable"];
            }
            else
            {
                SetTrafficTable();
            }

            if (Session["dtHoppers"] == null)
            {
                dtHoppers = DBAdapter.Hoppers.GetAllHoppers();
                Session["dtHoppers"] = dtHoppers;
                DBConnectorUtility.GetExpectedTimeValues();
                GetConfigurationValues();
            }
            else
            {
                dtHoppers = (DataTable)Session["dtHoppers"];
                ScreenRefereshInterval = (int)Session["CurrentPageInterval"];

                PageAutoReloadInterval = (int)Session["CurrentPageAutoReload"];
            }

            if (!ScriptManager1.IsInAsyncPostBack)
            {
                Response.AppendHeader("Refresh", PageAutoReloadInterval.ToString());
            }         

            if (ScreenRefereshInterval / 1000 > 0)
                UpdateTimer.Interval = ScreenRefereshInterval;
            else
                UpdateTimer.Interval = ScreenRefereshInterval * 1000;

            if (!IsPostBack)
            {
                // Bind Current Traffic
                LoadData();
                SortOrder = new Dictionary<string, string>();
                SortOrder.Add("StartTime", "ASC");
                SortOrder.Add("TruckName", "ASC");
                SortOrder.Add("REDS", "ASC");
                SortOrder.Add("Queue", "ASC");
                SortOrder.Add("EntryGate", "ASC");
                SortOrder.Add("PAD", "ASC");
                SortOrder.Add("TruckWash", "ASC");
                SortOrder.Add("Exit", "ASC");
                SortOrder.Add("REDS2", "ASC");
                ViewState["SortOrder"] = SortOrder;

                // Bind Closed Transactions
                BindClosedTx();
                CheckReaderStatus();
            }
        }

        public void BindClosedTx()
        {
            DataSet dsClosedTx = null;
            DataTable dtClosedTrans = null;

            try
            {
                if (dsResult != null && dsResult.Tables[1] != null)
                {
                    dtClosedTrans = dsResult.Tables[1];
                }
                else
                {
                    dsClosedTx = DBAdapter.ActiveReads.GetClsedTxForTrafficPage();
                    if (dsClosedTx != null && dsClosedTx.Tables[0].Rows.Count > 0)
                    {
                        dtClosedTrans = dsClosedTx.Tables[0];
                    }

                }

                if (dtClosedTrans != null)
                {
                    gvClosedTx.DataSource = dtClosedTrans;
                    gvClosedTx.DataBind();
                }
            }
            catch
            {
            }

        }

        public void CheckReaderStatus()
        {

            DataTable dtReaderStatus = null;

            try
            {
                if (dsResult != null && dsResult.Tables[2] != null)
                {
                    dtReaderStatus = dsResult.Tables[2];
                }

                if (dtReaderStatus != null && dtReaderStatus.Rows.Count > 0)
                {
                    if (dtReaderStatus.Rows[0]["Count"] != DBNull.Value)
                    {
                        if (Convert.ToInt32(dtReaderStatus.Rows[0]["Count"]) > 0)
                        {
                            trShowLink.Visible = true;
                        }
                        else
                        {
                            trShowLink.Visible = false;
                        }
                    }
                }
            }
            catch
            {
            }


        }

        public void LoadData()
        {

            if (dtTraffic == null)
            {
                if (Session["TrafficTable"] != null)
                {
                    dtTraffic = (DataTable)Session["TrafficTable"];
                }
                else
                {
                    SetTrafficTable();
                }
            }

            dtTraffic.Rows.Clear();

            dsResult = DBAdapter.ActiveReads.GetAllRecordsForTrafficPage();
            if (dsResult != null && dsResult.Tables[0].Rows.Count > 0)
            {

                DataRow[] drr;

                foreach (DataRow dr in dsResult.Tables[0].Rows)
                {
                    DataRow row = dtTraffic.NewRow();

                    if (!string.IsNullOrEmpty(dr["ID"].ToString()))
                    {
                        row["ID"] = dr["ID"];
                    }

                    if (!string.IsNullOrEmpty(dr["StartTime"].ToString()))
                    {
                        row["StartTime"] = dr["StartTime"].ToString();
                    }

                    if (!string.IsNullOrEmpty(dr["TruckName"].ToString()))
                    {
                        row["TruckName"] = dr["TruckName"].ToString();
                    }

                    if (!string.IsNullOrEmpty(dr["TruckID"].ToString()))
                    {
                        row["TruckID"] = dr["TruckID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["TagRFID"].ToString()))
                    {
                        row["RFID"] = dr["TagRFID"].ToString();
                    }

                    #region // For REDS status
                    //For REDS status
                    if (!string.IsNullOrEmpty(dr["REDS"].ToString()))
                    {
                        if (!string.IsNullOrEmpty(dr["EntryORQueue"].ToString()))
                        {
                            if (Convert.ToInt32(dr["EntryORQueue"]) == Convert.ToInt32((Location.ENTRY1)))
                            {
                                row["REDS"] = "Gate";
                            }
                            else if (Convert.ToInt32(dr["EntryORQueue"]) == Convert.ToInt32((Location.QUEUE)))
                            {
                                row["REDS"] = "Queue";
                            }
                            else
                            {
                                row["REDS"] = "NA";
                            }
                        }
                        else
                        {
                            row["REDS"] = "Passed";
                        }
                        row["REDSTime"] = dr["REDS"].ToString();
                    }
                    else
                    {
                        row["REDS"] = "NA";
                        row["REDSTime"] = "NA";
                    }

                    #endregion

                    #region //For Queue status
                    //For Queue status
                    if (!string.IsNullOrEmpty(dr["Queue"].ToString()))
                    {
                        if (dr["EntryOrQueue"] != DBNull.Value && (Location)Convert.ToInt32(dr["EntryOrQueue"]) == Location.ENTRY1)
                        {
                            row["Queue"] = "Arrived.";
                        }
                        else
                        {
                            row["Queue"] = "Arrived";
                        }

                        if (dr["LastPosition"] != DBNull.Value && (Location)Convert.ToInt32(dr["LastPosition"]) == Location.QUEUE)
                        {
                            if (dr["TruckCallup"] != DBNull.Value)
                            {
                                row["Queue"] = "Called Up";
                            }

                        }

                        row["QueueTime"] = dr["Queue"].ToString();
                    }
                    else
                    {
                        if (dr["LastPosition"] != DBNull.Value)
                        {
                            if ((Location)Convert.ToInt32(dr["LastPosition"]) == Location.REDS)
                            {
                                if (dr["EntryOrQueue"] != DBNull.Value && (Location)Convert.ToInt32(dr["EntryOrQueue"]) == Location.QUEUE)
                                {
                                    //if (dr["REDS"] != DBNull.Value)
                                    //{
                                    //    DateTime REDSArrival = Convert.ToDateTime(dr["REDS"]);

                                    //    if (REDSArrival.AddSeconds(ExpectedValues.REDS_Queue) < DateTime.Now)
                                    //    {
                                    //        row["Queue"] = "InBound";
                                    //    }
                                    //    else
                                    //    {
                                    //        row["Queue"] = "NA";
                                    //    }

                                    //    row["QueueTime"] = REDSArrival.AddSeconds(ExpectedValues.REDS_Queue);
                                    //}
                                    //else
                                    //{
                                    //    row["Queue"] = "NA";
                                    //    row["QueueTime"] = row["Queue"];
                                    //}
                                    row["Queue"] = "Not Arrived";
                                    row["QueueTime"] = row["Queue"];
                                }
                                else
                                {
                                    row["Queue"] = "NA";
                                    row["QueueTime"] = row["Queue"];
                                }
                            }
                            else
                            {
                                row["Queue"] = "NA";
                                row["QueueTime"] = row["Queue"];
                            }
                        }
                        else
                        {
                            row["Queue"] = "NA";
                            row["QueueTime"] = row["Queue"];
                        }
                        // row["QueueTime"] = row["Queue"];
                    }
                    #endregion

                    #region  //For Entry gate status Possible values: Arr. Overdue / Arrived / Departed / Dep. Overdue
                    //For Entry gate status Possible values: Arr. Overdue / Arrived / Departed / Dep. Overdue
                    if (!string.IsNullOrEmpty(dr["Entry1"].ToString())) // && string.IsNullOrEmpty(dr["Entry2"].ToString()))
                    {
                        row["EntryGate"] = "Arrived";

                        row["EntryGateTime"] = dr["Entry1"].ToString();

                        if (!string.IsNullOrEmpty(dr["Entry2"].ToString()))
                        {
                            row["EntryGate"] = "Departed";
                        }
                        else
                        {
                            if ((Location)Convert.ToInt32(dr["LastPosition"]) == Location.ENTRY1)
                            {
                                DateTime Entry1 = Convert.ToDateTime(dr["Entry1"]);

                                if (Entry1.AddSeconds(ExpectedValues.Ent1_Ent2) < DateTime.Now)
                                {
                                    row["EntryGate"] = "Dep. Overdue";
                                }

                                row["EntryGateTime"] = Entry1.AddSeconds(ExpectedValues.Ent1_Ent2);
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(dr["Entry2"].ToString()))
                    {
                        DateTime chkOverdue = DateTime.Now;
                        int expectdVal = 0;
                        if (dr["LastPosition"] != DBNull.Value)
                        {
                            Location current = (Location)Convert.ToInt32(dr["LastPosition"]);

                            if (current == Location.HOPPER || current == Location.TRUCKWASH || current == Location.EXITGATE)
                            {
                                row["EntryGate"] = "Not Arrived";
                                row["EntryGateTime"] = row["EntryGate"];
                            }
                            else
                            {
                                Location entryORQueue;

                                if (dr["EntryOrQueue"] != DBNull.Value)
                                {
                                    entryORQueue = (Location)Convert.ToInt32(dr["EntryOrQueue"]);
                                }
                                else
                                {
                                    entryORQueue = Location.ENTRY1;
                                }

                                if (entryORQueue == Location.ENTRY1)
                                {
                                    if (current == Location.REDS && dr["REDS"] != DBNull.Value)
                                    {
                                        chkOverdue = Convert.ToDateTime(dr["REDS"]);
                                        expectdVal = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;
                                    }
                                    else if (current == Location.QUEUE && dr["QUEUE"] != DBNull.Value)
                                    {
                                        chkOverdue = Convert.ToDateTime(dr["QUEUE"]);
                                        expectdVal = ExpectedValues.Queue_Ent1;
                                    }

                                    if (chkOverdue.AddSeconds(expectdVal) < DateTime.Now)
                                    {
                                        row["EntryGate"] = "Arr. Overdue";
                                    }
                                    else
                                    {
                                        row["EntryGate"] = "Not Arrived";
                                    }

                                    row["EntryGateTime"] = chkOverdue.AddSeconds(expectdVal);

                                }
                                else if (entryORQueue == Location.QUEUE && dr["TruckCallup"] != DBNull.Value)
                                {
                                    if (current == Location.QUEUE)
                                    {
                                        chkOverdue = Convert.ToDateTime(dr["TruckCallup"]);
                                        expectdVal = ExpectedValues.Queue_Ent1;
                                    }

                                    if (chkOverdue.AddSeconds(expectdVal) < DateTime.Now)
                                    {
                                        row["EntryGate"] = "Arr. Overdue";
                                    }
                                    else
                                    {
                                        row["EntryGate"] = "Not Arrived";
                                    }


                                    row["EntryGateTime"] = chkOverdue.AddSeconds(expectdVal);

                                }
                                else
                                {
                                    row["EntryGate"] = "Not Arrived";
                                    row["EntryGateTime"] = row["EntryGate"];
                                }
                            }


                        }
                        else
                        {
                            row["EntryGate"] = "Not Arrived";
                            row["EntryGateTime"] = row["EntryGate"];
                        }

                        //row["EntryGateTime"] = row["EntryGate"];

                    }
                    else
                    {
                        row["EntryGate"] = "Departed";
                        row["EntryGateTime"] = dr["Entry2"].ToString();
                    }
                    #endregion

                    #region  //For PAD status
                    //For PAD status
                    int hopperID = 0;
                    if (dr["HopperID"] != DBNull.Value)
                    {
                       hopperID = Convert.ToInt32(dr["HopperID"]);
                    }

                    int Entry2_Hopper = 0;
                    int Hopper_TW = 0;
                    drr = dtHoppers.Select("ID = " + hopperID);

                    if (drr.Length > 0)
                    {
                        Entry2_Hopper = Convert.ToInt32(drr[0]["TimeFromEnt2"]);
                        Hopper_TW = Convert.ToInt32(drr[0]["TimeToTW"]);
                    }

                    if (!string.IsNullOrEmpty(dr["HopperTime"].ToString()))
                    {
                        row["PADTime"] = dr["HopperTime"].ToString();

                        if (dr["FoundAtHopper"] != DBNull.Value)
                        {
                            int foundAtHopper = Convert.ToInt32(dr["FoundAtHopper"]);
                            if (hopperID == foundAtHopper)
                            {
                                row["PAD"] = "Arrived"; 
                            }
                            else
                            {
                                row["PAD"] = "Wrong PAD " + foundAtHopper.ToString();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(dr["Entry2"].ToString()))
                            {
                                DateTime Entry2 = Convert.ToDateTime(dr["Entry2"]);
                                if (Entry2.AddSeconds(Entry2_Hopper) < DateTime.Now)
                                {
                                    row["PAD"] = "Overdue";
                                }
                                else
                                {
                                    row["PAD"] = "Not Arrived";
                                }
                            }
                            else
                            {
                                row["PAD"] = "Not Arrived";
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dr["Entry2"].ToString()))
                        {
                            DateTime Entry2 = Convert.ToDateTime(dr["Entry2"]);

                            if (Entry2.AddSeconds(Entry2_Hopper) < DateTime.Now)
                            {
                                row["PAD"] = "Overdue";
                            }
                            else
                            {
                                row["PAD"] = "Not Arrived";
                            }

                            row["PADTime"] = Entry2.AddSeconds(Entry2_Hopper);
                        }
                        else
                        {
                            row["PAD"] = "Not Arrived";
                            row["PADTime"] = row["PAD"];
                        }

                    }
                    #endregion

                    #region //For Truck Wash status
                    //For Truck Wash status
                    if (!string.IsNullOrEmpty(dr["TruckWash"].ToString()))
                    {
                        row["TruckWash"] = "Arrived";
                        row["Exit"] = "NA";
                        row["TruckWashTime"] = dr["TruckWash"].ToString();
                        row["ExitTime"] = row["Exit"];

                        if (row["PAD"].ToString() == "Overdue")
                        {
                            row["PAD"] = "Not Arrived";
                            row["PADTime"] = "NA";
                        }
                    }
                    else
                    {
                        if (dr["LastPosition"] != DBNull.Value)
                        {
                            if ((Location)Convert.ToInt32(dr["LastPosition"]) == Location.EXITGATE)
                            {
                                row["TruckWash"] = "Skipped";
                                row["Exit"] = "Arrived";
                                row["TruckWashTime"] = row["TruckWash"];
                                row["ExitTime"] = dr["ExitTime"].ToString();

                                if (row["PAD"].ToString() == "Overdue")
                                {
                                    row["PAD"] = "Not Arrived";
                                    row["PADTime"] = "NA" ;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(dr["HopperTime"].ToString()))
                                {
                                    DateTime HopperTime = Convert.ToDateTime(dr["HopperTime"]);
                                    if (HopperTime.AddSeconds(Hopper_TW) < DateTime.Now)
                                    {
                                        row["TruckWash"] = "Overdue";
                                    }
                                    else
                                    {
                                        row["TruckWash"] = "Not Arrived";
                                    }

                                    row["TruckWashTime"] = HopperTime.AddSeconds(Hopper_TW);
                                }
                                else
                                {
                                    row["TruckWash"] = "NA";
                                }

                                row["Exit"] = "NA";
                                row["TruckWashTime"] = row["TruckWash"];
                            }
                        }
                        else
                        {
                            row["TruckWash"] = "NA";
                            row["Exit"] = "NA";
                            row["TruckWashTime"] = row["TruckWash"];
                            row["ExitTime"] = row["Exit"];
                        }

                    }
                    #endregion

                    #region //For REDS2 status
                    //For REDS2 status
                    if (!string.IsNullOrEmpty(dr["REDS2"].ToString()))
                    {
                        if (dr["LastPosition"] != DBNull.Value)
                        {
                            if ((Location)Convert.ToInt32(dr["LastPosition"]) == Location.REDS2)
                            {
                                row["REDS2"] = "Arrived";
                            }
                            else
                            {
                                row["REDS2"] = "NA";
                            }
                        }
                        else
                        {
                            row["REDS2"] = "Not Arrived";
                        }
                    }
                    else
                    {
                        row["REDS2"] = "Not Arrived";
                    }

                    #endregion

                    dtTraffic.Rows.Add(row);
                    dtTraffic.AcceptChanges();

                    // for future use like sorting
                    if (Session["dtTraffic"] != null)
                    {
                        Session["dtTraffic"] = null;
                        Session["dtTraffic"] = dtTraffic.DefaultView;
                    }
                    else
                    {
                        Session["dtTraffic"] = dtTraffic.DefaultView;
                    }
                }

                //gvCurrentTraffic.DataSource = dtTraffic.DefaultView;
                //gvCurrentTraffic.DataBind();
            }

            gvCurrentTraffic.DataSource = dtTraffic.DefaultView;
            gvCurrentTraffic.DataBind();
        }

        public void gvCurrentTraffic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //REDS Gate=Green Queue=Yello
                Label lblREDS = (Label)e.Row.FindControl("lblREDS");
                if (lblREDS != null)
                {
                    if (lblREDS.Text != "NA")
                    {
                        //e.Row.Cells[3].BackColor = System.Drawing.Color.Green;
                        e.Row.Cells[3].CssClass = "msg_green";
                        //e.Row.Cells[3].ForeColor = System.Drawing.Color.White;
                    }

                }

                //Queue Arrived=Green InBound=Yello
                Label lblQueue = (Label)e.Row.FindControl("lblQueue");
                Label lblMsgQueue = (Label)e.Row.FindControl("lblMsgQueue");
                if (lblQueue != null)
                {
                    if (lblQueue.Text == "Arrived")
                    {
                        // e.Row.Cells[4].BackColor = System.Drawing.Color.Green;
                        // e.Row.Cells[4].ForeColor = System.Drawing.Color.White;
                        e.Row.Cells[4].CssClass = "msg_green";
                        lblMsgQueue.Text = Utility.StatusArrived;
                    }
                    else if (lblQueue.Text == "Arrived.")
                    {
                        // e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[4].CssClass = "msg_yellow";
                        lblMsgQueue.Text = Utility.StatusArrived;
                    }
                    else if (lblQueue.Text == "InBound")
                    {
                        //e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[4].CssClass = "msg_yellow";
                        lblMsgQueue.Text = Utility.StatusInBound;
                    }
                    else if (lblQueue.Text == "Called Up")
                    {
                        e.Row.Cells[4].CssClass = "flashYellowImage";
                        lblMsgQueue.Text = Utility.StatusArrived;
                    }
                }

                //Entry Gate   Arrived=Green Departed=Green  Arr. Overdue=Yello  Dep. Overdue=Yello
                Label lblEntryGate = (Label)e.Row.FindControl("lblEntryGate");
                Label lblMsgEntryGate = (Label)e.Row.FindControl("lblMsgEntryGate");
                if (lblEntryGate != null)
                {
                    if (lblEntryGate.Text == "Arrived")
                    {
                        //e.Row.Cells[5].BackColor = System.Drawing.Color.Green;
                        //e.Row.Cells[5].ForeColor = System.Drawing.Color.White;
                        e.Row.Cells[5].CssClass = "msg_green";
                        lblMsgEntryGate.Text = Utility.StatusArrived;
                    }
                    else if (lblEntryGate.Text == "Departed")
                    {
                        //e.Row.Cells[5].ForeColor = System.Drawing.Color.White;
                        //e.Row.Cells[5].BackColor = System.Drawing.Color.Green;
                        e.Row.Cells[5].CssClass = "msg_green";
                        lblMsgEntryGate.Text = Utility.StatusDeparted;
                    }
                    else if (lblEntryGate.Text == "Arr. Overdue")
                    {
                        // e.Row.Cells[5].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[5].CssClass = "msg_yellow";
                        lblMsgEntryGate.Text = Utility.ErrorArrivedOvr;
                    }
                    else if (lblEntryGate.Text == "Dep. Overdue")
                    {
                        //e.Row.Cells[5].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[5].CssClass = "msg_yellow";
                        lblMsgEntryGate.Text = Utility.ErrorDepartedOvr;
                    }
                }

                //PAD   Arrived=Green  Overdue=Yello  WrongPAD=Red
                Label lblPAD = (Label)e.Row.FindControl("lblPAD");
                Label lblMsgPAD = (Label)e.Row.FindControl("lblMsgPAD");
                if (lblPAD != null)
                {
                    if (lblPAD.Text == "Arrived")
                    {
                        // e.Row.Cells[6].BackColor = System.Drawing.Color.Green;
                        //  e.Row.Cells[6].ForeColor = System.Drawing.Color.White;
                        e.Row.Cells[6].CssClass = "msg_green";
                        lblMsgPAD.Text = Utility.StatusArrived;
                    }
                    else if (lblPAD.Text == "Overdue")
                    {
                        // e.Row.Cells[6].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[6].CssClass = "msg_yellow";
                        lblMsgPAD.Text = Utility.ErrorOverdue;
                    }
                    else if (lblPAD.Text.Substring(0,9) == "Wrong PAD")
                    {
                        //e.Row.Cells[6].BackColor = System.Drawing.Color.Red;
                        e.Row.Cells[6].CssClass = "flashRedImage";
                        lblMsgPAD.Text = Utility.ErrorWrongPAD;
                    }
                }

                //TruckWash   Arrived=Green  Overdue=Yello  Skipped=Red
                Label lblTruckWash = (Label)e.Row.FindControl("lblTruckWash");
                Label lblMsgTruckWash = (Label)e.Row.FindControl("lblMsgTruckWash");
                if (lblTruckWash != null)
                {
                    if (lblTruckWash.Text == "Arrived")
                    {
                        // e.Row.Cells[7].BackColor = System.Drawing.Color.Green;
                        // e.Row.Cells[7].ForeColor = System.Drawing.Color.White;
                        e.Row.Cells[7].CssClass = "msg_green";
                        lblMsgTruckWash.Text = Utility.StatusArrived;
                    }
                    else if (lblTruckWash.Text == "Overdue")
                    {
                        // e.Row.Cells[7].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[7].CssClass = "msg_yellow";
                        lblMsgTruckWash.Text = Utility.ErrorOverdue;
                    }
                    else if (lblTruckWash.Text == "Skipped")
                    {
                        //e.Row.Cells[7].BackColor = System.Drawing.Color.Red;
                        e.Row.Cells[7].CssClass = "flashRedImage";
                        lblMsgTruckWash.Text = Utility.ErrorSkipped;
                    }
                }

                //Exit   Arrived=Red
                Label lblExit = (Label)e.Row.FindControl("lblExit");
                if (lblExit.Text == "Arrived")
                {
                    // e.Row.Cells[8].BackColor = System.Drawing.Color.Green;
                    // e.Row.Cells[8].ForeColor = System.Drawing.Color.White;
                    e.Row.Cells[8].CssClass = "msg_green";
                }
            }
        }

        protected void gvCurrentTraffic_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the dataview from the session object.
            DataView dv = Session["dtTraffic"] as DataView;
            SortOrder = (Dictionary<string, string>)ViewState["SortOrder"];
            string sortDirection = SortOrder[e.SortExpression];

            if (dv != null)
            {
                //Sort the data.

                if (sortDirection == "ASC")
                {
                    sortDirection = "DESC";
                    SortOrder[e.SortExpression] = sortDirection;
                }
                else
                {
                    sortDirection = "ASC";
                    SortOrder[e.SortExpression] = sortDirection;
                }

                dv.Sort = e.SortExpression + " " + sortDirection;
                gvCurrentTraffic.DataSource = dv;
                gvCurrentTraffic.DataBind();
                Session["dtTraffic"] = dv;
            }
        }

        public void gvClosedTx_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblMsgTrans = (Label)e.Row.FindControl("lblMsgTrans");
                HiddenField hdnTransStatus = (HiddenField)e.Row.FindControl("hdnTransStatus");

                if (lblMsgTrans != null && hdnTransStatus != null)
                {
                    if (Convert.ToInt32(hdnTransStatus.Value) == 1)
                    {
                        // e.Row.Cells[3].BackColor = System.Drawing.Color.Green;
                        // e.Row.Cells[3].ForeColor = System.Drawing.Color.White;
                        e.Row.Cells[3].CssClass = "msg_green";
                        lblMsgTrans.Text = Utility.StatusTransSuccess;
                    }
                    else if (Convert.ToInt32(hdnTransStatus.Value) == 3)
                    {
                        // e.Row.Cells[3].BackColor = System.Drawing.Color.Yellow;
                        e.Row.Cells[3].CssClass = "msg_yellow";
                        lblMsgTrans.Text = Utility.ErrorLeftWithTag;
                    }
                    else if (Convert.ToInt32(hdnTransStatus.Value) == 2)
                    {
                        // e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                        e.Row.Cells[3].CssClass = "msg_red";
                        // e.Row.Cells[3].CssClass = "red_box blink";                        
                        lblMsgTrans.Text = Utility.ErrorTransincomplete;
                    }

                    else { }
                }
            }
        }

        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            LoadData();
            // Bind Closed Transactions
            BindClosedTx();
            CheckReaderStatus();
        }

        protected void SetTrafficTable()
        {
            dtTraffic = new DataTable();
            dtTraffic.Columns.Add("ID", typeof(int));
            dtTraffic.Columns.Add("StartTime", typeof(DateTime));
            dtTraffic.Columns.Add("TruckName", typeof(string));
            dtTraffic.Columns.Add("REDS", typeof(string));
            dtTraffic.Columns.Add("Queue", typeof(string));
            dtTraffic.Columns.Add("EntryGate", typeof(string));
            dtTraffic.Columns.Add("PAD", typeof(string));
            dtTraffic.Columns.Add("TruckWash", typeof(string));
            dtTraffic.Columns.Add("Exit", typeof(string));
            dtTraffic.Columns.Add("REDS2", typeof(string));

            dtTraffic.Columns.Add("TruckID", typeof(string));
            dtTraffic.Columns.Add("RFID", typeof(string));
            dtTraffic.Columns.Add("REDSTime", typeof(string));
            dtTraffic.Columns.Add("QueueTime", typeof(string));
            dtTraffic.Columns.Add("EntryGateTime", typeof(string));
            dtTraffic.Columns.Add("PADTime", typeof(string));
            dtTraffic.Columns.Add("TruckWashTime", typeof(string));
            dtTraffic.Columns.Add("ExitTime", typeof(string));

            Session["TrafficTable"] = dtTraffic;
        }

        public void GetConfigurationValues()
        {
            try
            {
                DataSet ds = DBConnectorUtility.GetConfigurationValues();

                int ID = 0;

                ConfigurationValues key;

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ID = Convert.ToInt32(dr["ID"]);
                            key = (ConfigurationValues)ID;

                            if (key == ConfigurationValues.CurrentPageRefresh)
                            {
                                ScreenRefereshInterval = Convert.ToInt32(dr["Value"]);

                                Session["CurrentPageInterval"] = ScreenRefereshInterval;

                                break;
                            }

                        }

                        DataRow[] drr = ds.Tables[0].Select("ParameterType = 'CurrentPageAutoReload'");

                        if (drr != null && drr.Length > 0)
                        {
                            PageAutoReloadInterval = Convert.ToInt32(drr[0]["Value"]);

                            Session["CurrentPageAutoReload"] = PageAutoReloadInterval;

                        }

                    }
                }

            }
            catch
            {
            }
        }

    }
}

