﻿<%@ Page Title="Uploading Offline Transactions" Language="C#" MasterPageFile="~/masterpages/SiteMaster.Master"
    AutoEventWireup="true" CodeBehind="offlinetransactions.aspx.cs" Inherits="Ramp.UI.offlinetransactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div id="mid_section">
            <center>
                <b>UPLOADING OFFLINE TRANSACTIONS</b></center>
            <br />
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_strcture">
                <tr>
                    <td height="80">
                        File :
                        <%--<input name="" type="file" />--%>
                        <asp:FileUpload ID="fileUpOffTrans" runat="server" />
                        <asp:RequiredFieldValidator ID="reqValFileTags" runat="server" Display="Dynamic"
                            ErrorMessage="* Required" ControlToValidate="fileUpOffTrans" ValidationGroup="valGrp"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexUpOffTrans" runat="server" Display="Dynamic"
                            ErrorMessage="Only csv files are allowed" ControlToValidate="fileUpOffTrans"
                            ValidationGroup="valGrp" ValidationExpression="^([a-zA-Z].*|[1-9].*)\.(((c|C)(s|S)(v|V)))$"></asp:RegularExpressionValidator>
                        <asp:Button ID="btnPreview" runat="server" Text="Preview" OnClick="btnPreview_Click"
                            class="but" ValidationGroup="valGrp" />
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table width="95%">
                <tr>
                    <td>
                        <asp:GridView ID="gvPreview" runat="server" AutoGenerateColumns="false" Width="100%"
                            border="0" CellSpacing="0" CellPadding="0" CssClass="border_strcture" OnRowDataBound="gvPreview_RowDataBound">
                            <%-- <Columns>
                                <asp:BoundField DataField="Date" HeaderText="Date" />
                                <asp:BoundField DataField="Truck ID" HeaderText="Truck ID" />
                                <asp:BoundField DataField="REDS Time" HeaderText="REDS Time" />
                                <asp:BoundField DataField="Queue Time" HeaderText="Queue Time" />
                                <asp:BoundField DataField="Entry1 Time" HeaderText="Entry1 Time" />
                                <asp:BoundField DataField="Entry2 Time" HeaderText="Entry2 Time" />
                                <asp:BoundField DataField="PAD ID" HeaderText="PAD ID" />
                                <asp:BoundField DataField="PAD Time" HeaderText="PAD Time" />
                                <asp:BoundField DataField="TW Time" HeaderText="TW Time" />
                                <asp:BoundField DataField="Exit Time" HeaderText="Exit Time" />
                            </Columns>--%>
                            <Columns>
                                <asp:TemplateField HeaderText="Trans ID" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Truck ID" HeaderText="Truck" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="REDS Time" HeaderText="REDS" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Queue Time" HeaderText="Truck Queue" ItemStyle-Width="15%"
                                    ItemStyle-HorizontalAlign="Center" />
                                <%--<asp:BoundField DataField="Entry1 Time" HeaderText="Entry Gate" ItemStyle-Width="15%"
                                    ItemStyle-HorizontalAlign="Center" />--%>
                                <asp:BoundField DataField="EntryGate" HeaderText="Entry Gate" ItemStyle-Width="15%"
                                    ItemStyle-HorizontalAlign="Center" />
                                <%--<asp:BoundField DataField="PAD ID" HeaderText="PAD" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />--%>
                                <asp:BoundField DataField="PAD Time" HeaderText="PAD" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TW Time" HeaderText="Truck Wash" ItemStyle-Width="15%"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <%-- <EmptyDataTemplate>
                                No record(s) found
                            </EmptyDataTemplate>--%>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <%--<input type="submit" name="button" id="button" value="Upload" class="but" />--%>
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" class="but" OnClick="btnUpload_Click" />
                    </td>
                </tr>
            </table>
            <%-- <table width="95%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr>
                    <td width="100%" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <th class="border_strcture" width="7%" align="center">
                                    Trans ID
                                </th>
                                <th class="border_strcture" width="17%" align="center">
                                    Truck
                                </th>
                                <th class="border_strcture" width="15%" align="center">
                                    REDS
                                </th>
                                <th class="border_strcture" width="16%" align="center">
                                    T-Park
                                </th>
                                <th class="border_strcture" width="15%" align="center">
                                    E-Gate
                                </th>
                                <th class="border_strcture" width="17%" align="center">
                                    Hopper
                                </th>
                                <th class="border_strcture" width="13%" align="center">
                                    T-Wash
                                </th>
                            </tbody>
                            <tr>
                                <td class="border_strcture" align="center">
                                    T1
                                </td>
                                <td class="border_strcture" align="center">
                                    T001-ATLAS-3
                                </td>
                                <td align="center" class="border_strcture">
                                    Passed
                                </td>
                                <td align="center" class="border_strcture">
                                    Parked
                                </td>
                                <td align="center" class="border_strcture">
                                    <p align="center">
                                        Departed</p>
                                </td>
                                <td class="border_strcture" align="center">
                                    Arrived
                                </td>
                                <td class="border_strcture" align="center">
                                    Arrived
                                </td>
                            </tr>
                            <tr>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                                <td height="300" align="center" class="border_strcture">
                                    &nbsp;
                                </td>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                                <td class="border_strcture" align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" align="right">
                                    <input name="button" type="submit" class="but" id="button" value="Upload" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>--%>
        </div>
    </div>
</asp:Content>
