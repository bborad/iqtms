﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ramp.UI._Default"
    MasterPageFile="~/masterpages/SiteMaster.Master" Title="Home" %>

<asp:Content ID="bodycontent" ContentPlaceHolderID="body" runat="server">
    <div id="wrap-page">
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="line-height: 24px;
                margin-top: 10px;" class="border_strcture1">
                <tr>
                    <td width="70%" valign="top" align="left">
                        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tr>
                                <%-- <td valign="top" width="33%" align="left">
                            <div id="divTag" runat="server">
                                <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                    <tr height="30">
                                        <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                            &nbsp;Tags
                                        </th>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="margin-top: 10px">
                                            <a href="tags/assigntag.aspx">Assign Tags</a><br />
                                            <a href="tags/addtag.aspx">Add New Tag</a><br />
                                            <a href="tags/issuetag.aspx">Issue Tags</a><br />
                                            <a id="lnkUnAssignTag" runat="server" href="tags/unassigntag.aspx">Un-Assign Tags</a><br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>--%>
                                <td valign="top" width="33%" align="left">
                                    <div id="divTraffTrans" runat="server">
                                        <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                            <tr height="30">
                                                <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                                    &nbsp;Traffic Transactions
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="margin-top: 10px">
                                                    <asp:LinkButton ID="lnkCurrentTraffic" runat="server" PostBackUrl="~/currenttraffic.aspx"
                                                        Text="Current Traffic Page"></asp:LinkButton><br />
                                                    <a href="entrygate.aspx">Entry Gate</a><br />
                                                    <asp:LinkButton ID="lnkoffline" runat="server" PostBackUrl="~/offlinetransactions.aspx"
                                                        Text="Offline Transactions"></asp:LinkButton><br />
                                                    <asp:LinkButton ID="lnkModbusServer" runat="server" PostBackUrl="~/modbusserver.aspx"
                                                        Text="MODBUS Server"></asp:LinkButton>
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td valign="top" width="33%" align="left">
                                    <div id="divReports" runat="server">
                                        <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                            <tr height="30">
                                                <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                                    &nbsp;Reports
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="margin-top: 10px">
                                                    <a href="reports/systemstatusreport.aspx">System Status Report</a><br />
                                                    <a href="reports/assignunassigntagreport.aspx">Tag Report</a><br />
                                                    <a href="reports/expectedtimereport.aspx">Expected Time Report</a><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td valign="top" width="33%" align="left">
                                    <div id="divConfig" runat="server">
                                        <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                            <tr height="30">
                                                <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                                    &nbsp;Configurations
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="margin-top: 10px">
                                                    <a href="hardwareconfigurationpage.aspx">Hardware Configuration</a><br />
                                                    <a href="expectedtimeslist.aspx">Expected Time Configuration</a><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <%--<td valign="top" width="33%" align="left">
                            <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                <tr height="30">
                                    <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                        &nbsp;Configurations
                                    </th>
                                </tr>
                                <tr>
                                    <td valign="top" style="margin-top: 10px">
                                        <a href="hardwareconfigurationpage.aspx">Hardware Configuration</a><br />
                                        <a href="expectedtimeslist.aspx">Expected Time Configuration</a><br />
                                    </td>
                                </tr>
                            </table>
                        </td>--%>
                                <td valign="top" width="33%" align="left">
                                    <div id="divTag" runat="server">
                                        <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                            <tr height="30">
                                                <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                                    &nbsp;Tags
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="margin-top: 10px">
                                                    <a href="tags/assigntag.aspx">Assign Tags</a><br />
                                                    <a href="tags/addtag.aspx">Add New Tag</a><br />
                                                    <a href="tags/issuetag.aspx">Issue Tags</a><br />
                                                    <a id="lnkUnAssignTag" runat="server" href="tags/unassigntag.aspx">Un-Assign Tags</a><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td valign="top" width="33%" align="left">
                                    <div id="dvUserBox" runat="server">
                                        <table width="100%" style="border: 1px solid  #035EAB" cellspacing="0" cellpadding="0">
                                            <tr height="30">
                                                <th height="30" background="images/nav_hover.jpg" style="color: #fff;">
                                                    &nbsp;Users
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="margin-top: 10px">
                                                    <a id="lnkViewUser" runat="server" href="users/viewusers.aspx">View User</a><br />
                                                    <a id="lnkAddUser" runat="server" href="users/adduser.aspx">Add User</a><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td valign="top" width="33%" align="left">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="30%" valign="top" align="left">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
