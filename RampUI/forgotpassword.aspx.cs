using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.DBConnector.Adapter;
using Ramp.DBConnector.Common;
using Ramp.MiddlewareController.Common;
using System.Configuration;
using Ramp.UI.common;


namespace Ramp.UI
{
    public partial class forgotpassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblVersion.Text = Utility.VersionNo;
            if (!IsPostBack)
            {
            }
            // check if Username is entered by user on Login screen then fill it to Username screen of forgot password
            if (Request.QueryString["UN"] != null)
            {
                if (Convert.ToString(Request.QueryString["UN"]) != string.Empty)
                {
                    txtUserName.Text = Convert.ToString(Request.QueryString["UN"]);
                }
            }
        }

        /// <summary>
        /// If correct emailID exists then send password to user by Email 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSendPassword_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorMsg.Text = string.Empty;
                // Check Username and EmailID
                IUsers objUser = Users.GetPasswordByUserNameEmailID(txtUserName.Text.Trim(), txtEmail.Text.Trim());
                lblErrorMsg.Visible = true;
                if (objUser != null)
                {
                    if ((objUser.ID == -1) && ((objUser.EmailID == string.Empty) || (objUser.EmailID == null)))
                    {
                        lblErrorMsg.Text = "Incorrect UserName/EmailID";
                    }
                    else if ((objUser.ID != -1) && ((objUser.EmailID == string.Empty) || (objUser.EmailID == null)))
                    {
                        lblErrorMsg.Text = "EmailID not exists for specified username. Please contact admin to get password";
                    }
                    else if ((objUser.ID != -1) && ((objUser.EmailID != string.Empty) || (objUser.EmailID != null)))
                    {
                        //SendEmail to user for the password
                        //DBConnectorUtility.SendEmail(FromAddress,ToAddress,CC,BCC,Attachment,Subject,MessageBody);
                        string fromAddress = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                        if (fromAddress.Length > 0)
                        {
                            MiddlewareUtility.SendEmail(fromAddress, txtEmail.Text.Trim(), "", "", "", "Forgot Password", "Hi, <br /> Here is your account password.<br /><br /> UserName : " + txtUserName.Text.Trim() + "<br /> EmailID : " + txtEmail.Text.Trim() + " <br /> Password : " + objUser.Password);
                        }

                        ClearControls();
                    }
                }
                else
                {
                    lblErrorMsg.Text = "Incorrect Username/EmailID";
                }
            }
            catch (Exception ex)
            {
                lblErrorMsg.Text = ex.Message;
            }
        }
        /// <summary>
        /// Clear all the controls and messages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
        private void ClearControls()
        {
            txtEmail.Text = string.Empty;
            txtUserName.Text = string.Empty;
            lblErrorMsg.Text = string.Empty;
        }
    }
}
