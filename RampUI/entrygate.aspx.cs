using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.MiddlewareController.Common;
using Ramp.RFIDHWConnector.Adapters;

using DBAdapter = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;

//using Modbus = Ramp.ModbusConnector.IQTMSModbusServer;
//using Ramp.ModbusConnector;
//using Ramp.Modbus.Utility;
using System.Configuration;

using Ramp.DBConnector.Common;


namespace Ramp.UI
{
    public partial class entrygate : System.Web.UI.Page
    {

        string str_Lane1Marker = String.Empty;
        string str_Lane2Marker = String.Empty;
        string str_TruckWashMarker = String.Empty;
        string str_ExitGateMarker = String.Empty;
        string str_QueueMarker = String.Empty;

        DataTable dtLane1, dtLane2;

        Dictionary<string, int> tagList;

        public int ScreenRefereshInterval = 5;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["EntryPageInterval"] == null)
                {
                    GetConfigurationValues();
                }
                else
                {
                    ScreenRefereshInterval = (int)Session["EntryPageInterval"];
                }

                if (Session["dtLane"] == null)
                {
                    CreateDataTables();
                }
                else
                {
                    dtLane1 = (DataTable)Session["dtLane"];
                    dtLane2 = dtLane1.Clone();
                }


                if (ScreenRefereshInterval / 1000 > 0)
                    UpdateTimer.Interval = ScreenRefereshInterval;
                else
                    UpdateTimer.Interval = ScreenRefereshInterval * 1000;

                if (!Page.IsPostBack)
                {
                    // Get all markers and bind to checkbox list
                    List<IMarkers> objMarkersList = DBAdapter.Markers.GetAllMarkers();
                    Session["MarkersList"] = objMarkersList;

                    LoadData();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnReRead_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void LoadData()
        {
            bool flagModbus = true;
            try
            {
                //ModbusServer objModbus = new ModbusServer();
                
               // Modbus.ReaderService objModbus = null;
             //  objModbus = new Modbus.ReaderService();                   

                //List<IReader> lstReadersAtEntry1 = null;
                //lstReadersAtEntry1 = Ramp.RFIDHWConnector.Adapters.Reader.GetReadersAtLocation(Location.ENTRY1);

                StringBuilder strTagIDs = new StringBuilder("'-1'");

                //if (Session["TagList"] == null)
                //{
                //    tagList = new Dictionary<string, int>();
                //}
                //else
                //{
                //    tagList = (Dictionary<string, int>)Session["TagList"];
                //}

                // Perform a tag read operation, BY READERS, to search TagRFID AT loc
               // List<RFIDTag> lstRFIDTags = new List<RFIDTag>();

                //foreach (IReader objReader in lstReadersAtEntry1)
                //{
                //    List<RFIDTag> lstTemp = new List<RFIDTag>();
                //    //lstTemp = objReader.ScanForTags();
                //    try
                //    {
                //        lstTemp = objReader.ReadTags();                    

                //       // objReader.Dispose();

                //        // objReader.ClearTagList();
                //        foreach (RFIDTag objTag in lstTemp)
                //        {
                //            if (!tagList.ContainsKey(objTag.TagID))
                //            {
                //                tagList.Add(objTag.TagID, 0);
                //                // strTagIDs.Append("," + "'" + objTag.TagID + "'");                              
                //            }
                //            else
                //            {
                //                tagList[objTag.TagID] = 0;
                //            }
                //        }

                //    }
                //    catch
                //    {
                //    }
                //}

                //List<string> lstRFIDTags = new List<string>();

                //lstRFIDTags = tagList.Keys.ToList();

                //int value;

                //foreach (string tagId in lstRFIDTags)
                //{
                //    value = tagList[tagId];
                //    if (value <= 3)
                //    {
                //        strTagIDs.Append("," + "'" + tagId + "'");
                //        tagList[tagId] = value + 1;
                //    }
                //    else
                //    {
                //        tagList.Remove(tagId);
                //    }
                //}

                //Session["TagList"] = tagList;

                DataSet dsResult = DBAdapter.ActiveReads.GetRecordsByTagIDs(strTagIDs.ToString(), Location.ENTRY1);

                dtLane1.Rows.Clear();
                dtLane2.Rows.Clear();

               // short hopperID, hopperStatus;

                int hopperID, hopperStatus;

                List<IMarkers> objMarkersList = new List<IMarkers>();
                if (Session["MarkersList"] != null)
                {
                    objMarkersList = (List<IMarkers>)Session["MarkersList"];
                }

                var MarkerLoopID = (from obj in objMarkersList where obj.Location == Convert.ToInt32(Location.EntryLane1) select obj.MarkerLoopID);
                if (MarkerLoopID.Count() > 0)
                {
                    str_Lane1Marker = MarkerLoopID.First();
                }

                MarkerLoopID = from obj in objMarkersList where obj.Location == Convert.ToInt32(Location.EntryLane2) select obj.MarkerLoopID;
                if (MarkerLoopID.Count() > 0)
                {
                    str_Lane2Marker = MarkerLoopID.First();
                }

                MarkerLoopID = from obj in objMarkersList where obj.Location == Convert.ToInt32(Location.TRUCKWASH) select obj.MarkerLoopID;
                if (MarkerLoopID.Count() > 0)
                {
                    str_TruckWashMarker = MarkerLoopID.First();
                }

                MarkerLoopID = from obj in objMarkersList where obj.Location == Convert.ToInt32(Location.EXITGATE) select obj.MarkerLoopID;
                if (MarkerLoopID.Count() > 0)
                {
                    str_ExitGateMarker = MarkerLoopID.First();
                }

                MarkerLoopID = from obj in objMarkersList where obj.Location == Convert.ToInt32(Location.QUEUE) select obj.MarkerLoopID;
                if (MarkerLoopID.Count() > 0)
                {
                    str_QueueMarker = MarkerLoopID.First();
                }
                

                //foreach (DataRow rowRead in dtTagRead.Rows)
                //{
                foreach (DataRow rowDB in dsResult.Tables[0].Rows)
                {
                    //if (rowRead["TagID"].ToString() == rowDB["TagRFID"].ToString())
                    //{
                    if (rowDB["NewLoopID"].ToString() == str_Lane1Marker)  // Entry1
                    {
                        DataRow row1 = dtLane1.NewRow();
                        row1["ID"] = rowDB["ID"];
                        row1["TagID"] = rowDB["TagID"];
                        row1["HopperID"] = rowDB["HopperID"];
                        row1["REDS"] = rowDB["REDS"];
                        row1["Queue"] = rowDB["Queue"];

                        if (rowDB["Entry1"] != DBNull.Value)
                        {
                            row1["Entry1"] = Convert.ToDateTime(rowDB["Entry1"]).ToString("hh:mm:ss tt");
                        }
                        else
                        {
                            row1["Entry1"] = rowDB["Entry1"];
                        }
                        row1["TruckCallUp"] = rowDB["TruckCallUp"];
                        row1["LastPosition"] = rowDB["LastPosition"];
                        row1["EntryORQueue"] = rowDB["EntryORQueue"];
                        row1["TagRFID"] = rowDB["TagRFID"];
                        row1["TruckID"] = rowDB["TruckID"];
                        row1["ProponentID"] = rowDB["ProponentID"];
                        row1["ProponentName"] = rowDB["ProponentName"];
                        row1["HopperName"] = rowDB["HopperName"];

                        row1["TruckWash"] = rowDB["TruckWash"];
                        row1["ExitTime"] = rowDB["ExitTime"];

                        row1["NewMarkerID"] = rowDB["NewLoopID"];
                        row1["OldMarkerID"] = rowDB["OldLoopID"];
                        row1["ReArrivedAtEntry"] = rowDB["ReArrivedAtEntry"];
                        

                        //......................Status.............................
                        if (flagModbus == true)
                        {
                           // hopperID = Convert.ToInt32(rowDB["HopperName"]);
                            hopperStatus = 0;

                            try
                            {
                                //hopperStatus = objModbus.ReadPAD_Status((byte)hopperID);
                                hopperStatus = Convert.ToInt32(rowDB["HopperStatus"]);
                            }
                            catch
                            {
                            }


                            if (hopperStatus == 0)
                            {
                                row1["HopperStatus"] = "Ready";
                            }
                            else if (hopperStatus == 1)
                            {
                                row1["HopperStatus"] = "Fault";
                            }

                            else if (hopperStatus == 2)
                            {
                                row1["HopperStatus"] = "Not available. Maintenance.";
                            }
                            else
                            {
                                row1["HopperStatus"] = "NA " + hopperStatus.ToString();
                            }

                        }
                        else
                        {
                            row1["HopperStatus"] = "Ready";
                        }
                        //................................................................


                        dtLane1.Rows.Add(row1);
                    }
                    else if (rowDB["NewLoopID"].ToString() == str_Lane2Marker)  // Entry2
                    {
                        DataRow row2 = dtLane2.NewRow();
                        row2["ID"] = rowDB["ID"];
                        row2["TagID"] = rowDB["TagID"];
                        row2["HopperID"] = rowDB["HopperID"];
                        row2["REDS"] = rowDB["REDS"];
                        row2["Queue"] = rowDB["Queue"];

                        if (rowDB["Entry1"] != DBNull.Value)
                        {
                            row2["Entry1"] = Convert.ToDateTime(rowDB["Entry1"]).ToString("hh:mm:ss tt");
                        }
                        else
                        {
                            row2["Entry1"] = rowDB["Entry1"];
                        }

                        row2["TruckCallUp"] = rowDB["TruckCallUp"];
                        row2["LastPosition"] = rowDB["LastPosition"];
                        row2["EntryORQueue"] = rowDB["EntryORQueue"];
                        row2["TagRFID"] = rowDB["TagRFID"];
                        row2["TruckID"] = rowDB["TruckID"];
                        row2["ProponentID"] = rowDB["ProponentID"];
                        row2["ProponentName"] = rowDB["ProponentName"];
                        row2["HopperName"] = rowDB["HopperName"];

                        row2["TruckWash"] = rowDB["TruckWash"];
                        row2["ExitTime"] = rowDB["ExitTime"];

                        row2["NewMarkerID"] = rowDB["NewLoopID"];
                        row2["OldMarkerID"] = rowDB["OldLoopID"];
                        row2["ReArrivedAtEntry"] = rowDB["ReArrivedAtEntry"];
                        
                        //......................Status.............................
                        if (flagModbus == true)
                        {
                           // hopperID = Convert.ToInt16(rowDB["HopperID"]);
                            hopperStatus = 0;

                            try
                            {
                               // hopperStatus = objModbus.ReadPAD_Status((byte)hopperID);
                                hopperStatus = Convert.ToInt32(rowDB["HopperStatus"]);
                            }
                            catch
                            {
                            }

                            if (hopperStatus == 0)
                            {
                                row2["HopperStatus"] = "Ready";
                            }
                            else if (hopperStatus == 1)
                            {
                                row2["HopperStatus"] = "Fault";
                            }

                            else if (hopperStatus == 2)
                            {
                                row2["HopperStatus"] = "Not available. Maintenance.";
                            }
                            else
                            {
                                row2["HopperStatus"] = "NA " + hopperStatus.ToString();
                            }

                        }
                        else
                        {
                            row2["HopperStatus"] = "Ready";
                        }
                        //...........................................................


                        dtLane2.Rows.Add(row2);
                    }
                    //  }
                }
                //  }

                gvLane1.DataSource = dtLane1;
                gvLane1.DataBind();

                gvLane2.DataSource = dtLane2;
                gvLane2.DataBind();

                //*****************************************************************************************************


                // STATUS Signals

                if (dtLane1.Rows.Count == 0 && dtLane2.Rows.Count == 0)
                {
                    // Clear Lane1 & Lane2  Signals
                    clearLane1();
                    clearLane2();
                }
                else if (dtLane1.Rows.Count == 1 && dtLane2.Rows.Count == 0)
                {
                    DataRow row = dtLane1.Rows[0];
                    string msg = String.Empty;
                    if (IsStatusX(row, out msg))
                    {
                        // Display Status X
                        lblLane1Message.Text = msg;
                        setLaneStatus(1, 4, 0, msg);
                    }
                    else
                    {
                        // Lane 1  GO
                        lblLane1Message.Text = "Proceed to PAD.";
                        setLaneStatus(1, 1, 0, String.Empty);
                    }

                    // Clear Lane2  Signal
                    clearLane2();
                }
                else if (dtLane1.Rows.Count == 0 && dtLane2.Rows.Count == 1)
                {
                    DataRow row = dtLane2.Rows[0];
                    string msg = String.Empty;
                    if (IsStatusX(row, out msg))
                    {
                        // Display Status X
                        lblLane2Message.Text = msg;
                        setLaneStatus(2, 4, 0, msg);
                    }
                    else
                    {
                        // Lane 2  GO
                        lblLane2Message.Text = "Proceed to PAD.";
                        setLaneStatus(2, 1, 0, String.Empty);
                    }

                    // Clear Lane1  Signal
                    clearLane1();

                }
                else if (dtLane1.Rows.Count == 1 && dtLane2.Rows.Count == 1)
                {
                    // Take decision based on the PAD no. and signal the Truck.
                    // Lane1/Lane2  GO  other will WAIT


                    Boolean flagLane1 = false;
                    Boolean flagLane2 = false;
                    int Lane1HopperID = 0;
                    int Lane2HopperID = 0;

                    DataRow row1 = dtLane1.Rows[0];
                    DataRow row2 = dtLane2.Rows[0];

                    string msg1 = String.Empty;
                    if (IsStatusX(row1, out msg1))
                    {
                        // Display Status X
                        lblLane1Message.Text = msg1;
                        setLaneStatus(1, 4, 0, msg1);
                        flagLane1 = false;
                    }
                    else
                    {
                        // flag Lane 1  GO
                        flagLane1 = true;
                    }
                    Lane1HopperID = Convert.ToInt32(row1["HopperID"].ToString());


                    string msg2 = String.Empty;
                    if (IsStatusX(row2, out msg2))
                    {
                        // Display Status X
                        lblLane2Message.Text = msg2;
                        setLaneStatus(2, 4, 0, msg2);
                        flagLane2 = false;
                    }
                    else
                    {
                        // flag Lane 2  GO
                        flagLane2 = true;
                    }
                    Lane2HopperID = Convert.ToInt32(row2["HopperID"].ToString());


                    //if (flagLane1 == false && flagLane2 == false)
                    //{
                    //    // Display Status X
                    //    // Display Status X
                    //    setLaneStatus(1, 4, 0);
                    //    setLaneStatus(2, 4, 0);
                    //}
                    //else 
                    if (flagLane1 == true && flagLane2 == false)
                    {
                        // Lane 1  GO
                        // Display Status X
                        setLaneStatus(1, 1, 0, String.Empty);

                    }
                    else if (flagLane1 == false && flagLane2 == true)
                    {
                        // Display Status X
                        // Lane 2  GO

                        //setLaneStatus(1, 4, 0);
                        setLaneStatus(2, 1, 0, String.Empty);
                    }
                    else if (flagLane1 == true && flagLane2 == true)
                    {
                        // Hopper Priority
                        if (Lane1HopperID > Lane2HopperID)
                        {
                            // Lane 1  GO
                            // Lane 2  WAIT
                            setLaneStatus(1, 1, 0, String.Empty);
                            setLaneStatus(2, 2, 0, String.Empty);
                        }
                        else
                        {
                            // Lane 1  WAIT
                            // Lane 2  GO
                            setLaneStatus(1, 2, 0, String.Empty);
                            setLaneStatus(2, 1, 0, String.Empty);
                        }
                    }
                }
                else if (dtLane1.Rows.Count == 1 && dtLane2.Rows.Count > 1)
                {
                    // Lane 1  GO
                    DataRow row = dtLane1.Rows[0];
                    string msg = String.Empty;
                    if (IsStatusX(row, out msg))
                    {
                        // Display Status X
                        lblLane1Message.Text = msg;
                        setLaneStatus(1, 4, 0, msg);
                    }
                    else
                    {
                        // Lane 1  GO
                        lblLane1Message.Text = "Proceed to PAD.";
                        setLaneStatus(1, 1, 0, String.Empty);
                    }


                    // Lane 2  Display "n"
                    setLaneStatus(2, 3, dtLane2.Rows.Count, String.Empty);

                }
                else if (dtLane1.Rows.Count > 1 && dtLane2.Rows.Count == 1)
                {
                    // Lane 1  Display "n"
                    setLaneStatus(1, 3, dtLane1.Rows.Count, String.Empty);


                    // Lane 2  GO
                    DataRow row = dtLane2.Rows[0];
                    string msg = String.Empty;
                    if (IsStatusX(row, out msg))
                    {
                        // Display Status X
                        lblLane2Message.Text = msg;
                        setLaneStatus(2, 4, 0, msg);
                    }
                    else
                    {
                        // Lane 2  GO
                        lblLane2Message.Text = "Proceed to PAD.";
                        setLaneStatus(2, 1, 0, String.Empty);
                    }
                }
                else if (dtLane1.Rows.Count > 1 && dtLane2.Rows.Count > 1)
                {
                    // Lane 1  Display "n"
                    // Lane 2  Display "n"
                    setLaneStatus(1, 3, dtLane1.Rows.Count, String.Empty);
                    setLaneStatus(2, 3, dtLane2.Rows.Count, String.Empty);
                }
                else if (dtLane1.Rows.Count == 0 && dtLane2.Rows.Count > 1)
                {
                    // Clear Lane1  Signal
                    clearLane1();

                    // Lane 2  Display "n"
                    setLaneStatus(2, 3, dtLane2.Rows.Count, String.Empty);
                }
                else if (dtLane1.Rows.Count > 1 && dtLane2.Rows.Count == 0)
                {
                    // Lane 1  Display "n"
                    setLaneStatus(1, 3, dtLane1.Rows.Count, String.Empty);

                    // Clear Lane2  Signal
                    clearLane2();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Boolean IsStatusX(DataRow row, out string message)
        {
            bool result = false;
            message = ""; 
            try
            {    
                if (!String.IsNullOrEmpty(row["OldMarkerID"].ToString()) && (row["OldMarkerID"].ToString() == str_TruckWashMarker || row["OldMarkerID"].ToString() == str_ExitGateMarker))  // TW or EXIT
                {
                    //  rearrival
                    //flagX = true;
                    //if (!String.IsNullOrEmpty(row["TruckWash"].ToString()) || !String.IsNullOrEmpty(row["ExitTime"].ToString()))
                    if (!String.IsNullOrEmpty(row["ReArrivedAtEntry"].ToString()) &&  (Convert.ToBoolean(row["ReArrivedAtEntry"]) == true))
                    {
                        message = "Truck has re-arrived.";
                        result = true;
                    }                 
                }              
                // 2) Non Call up Truck
                if (result == false)
                {
                    if (row["EntryORQueue"].ToString() == Convert.ToInt32(Location.QUEUE).ToString()) // Queue
                    {
                        if (String.IsNullOrEmpty(row["TruckCallUp"].ToString()))
                        {
                            //  non call up
                            //flagX = true;
                            message = "Non called-up truck has arrived.";
                            result = true;
                        }
                    }
                }

                if (result == false)
                {
                    if (row["HopperStatus"].ToString() != "Ready")
                    {
                        message = row["HopperStatus"].ToString();
                        result = true;
                    }
                }               
               
            }
            catch (Exception ex)
            {
               // throw ex;
            }
            return result;
        }


        public void setLaneStatus(Int32 Lane, Int32 Status, Int32 count, string message)
        {
            if (Lane == 1)
            {
                if (Status == 1)
                {
                    colLane1Status.Attributes.Add("class", "green_box");
                    colLane1Msg.Attributes.Add("class", "border_strcture msg_green");
                    lblLane1Status.Text = "GO";
                    lblLane1Message.Text = "Proceed to PAD.";
                }
                else if (Status == 2)
                {
                    colLane1Status.Attributes.Add("class", "orange_box");
                    colLane1Msg.Attributes.Add("class", "border_strcture msg_orange");
                    lblLane1Status.Text = "WAIT";
                    lblLane1Message.Text = "Wait.";
                }
                else if (Status == 3)
                {
                    colLane1Status.Attributes.Add("class", "red_box blink");
                    colLane1Msg.Attributes.Add("class", "border_strcture msg_red");
                    lblLane1Status.Text = count.ToString();
                    lblLane1Message.Text = "Multiple Tags Found.";
                }
                else if (Status == 4)
                {
                    colLane1Status.Attributes.Add("class", "red_box blink");
                    colLane1Msg.Attributes.Add("class", "border_strcture msg_red");
                    lblLane1Status.Text = "X";
                    lblLane1Message.Text = message;
                }
            }

            else if (Lane == 2)
            {
                if (Status == 1)
                {
                    colLane2Status.Attributes.Add("class", "green_box");
                    colLane2Msg.Attributes.Add("class", "border_strcture msg_green");
                    lblLane2Status.Text = "GO";
                    lblLane2Message.Text = "Proceed to PAD.";
                }
                else if (Status == 2)
                {
                    colLane2Status.Attributes.Add("class", "orange_box");
                    colLane2Msg.Attributes.Add("class", "border_strcture msg_orange");
                    lblLane2Status.Text = "WAIT";
                    lblLane2Message.Text = "Wait.";
                }
                else if (Status == 3)
                {

                    colLane2Status.Attributes.Add("class", "red_box");
                    colLane2Msg.Attributes.Add("class", "border_strcture msg_red");
                    lblLane2Status.Text = count.ToString();
                    lblLane2Message.Text = "Multiple Tags Found.";
                }
                else if (Status == 4)
                {
                    colLane2Status.Attributes.Add("class", "red_box");
                    colLane2Msg.Attributes.Add("class", "border_strcture msg_red");
                    lblLane2Status.Text = "X";
                    lblLane2Message.Text = message;
                }
            }
        }

        public void clearLane1()
        {
            try
            {
                colLane1Status.Attributes.Remove("class");
                colLane1Msg.Attributes.Remove("class");
                lblLane1Status.Text = String.Empty;
                lblLane1Message.Text = String.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void clearLane2()
        {
            try
            {
                colLane2Status.Attributes.Remove("class");
                colLane2Msg.Attributes.Remove("class");
                lblLane2Status.Text = String.Empty;
                lblLane2Message.Text = String.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            LoadData();
        }


        public void GetConfigurationValues()
        {
            try
            {
                DataSet ds = DBConnectorUtility.GetConfigurationValues();

                int ID = 0;

                ConfigurationValues key;

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ID = Convert.ToInt32(dr["ID"]);
                            key = (ConfigurationValues)ID;

                            if (key == ConfigurationValues.EntryPageRefresh)
                            {
                                ScreenRefereshInterval = Convert.ToInt32(dr["Value"]);

                                Session["EntryPageInterval"] = ScreenRefereshInterval;

                                break;
                            }

                        }
                    }
                }

            }
            catch
            {
            }
        }

        void CreateDataTables()
        {
              dtLane1 = new DataTable();
            DataColumn dc1 = new DataColumn("ID", typeof(int));
            DataColumn dc2 = new DataColumn("TagID", typeof(int));
            DataColumn dc3 = new DataColumn("HopperID", typeof(int));
            DataColumn dc4 = new DataColumn("REDS", typeof(DateTime));
            DataColumn dc5 = new DataColumn("Queue", typeof(DateTime));
            DataColumn dc6 = new DataColumn("Entry1", typeof(String));
            DataColumn dc7 = new DataColumn("TruckCallUp", typeof(DateTime));
            DataColumn dc8 = new DataColumn("LastPosition", typeof(int));
            DataColumn dc9 = new DataColumn("EntryORQueue", typeof(int));
            DataColumn dc10 = new DataColumn("TagRFID", typeof(string));
            DataColumn dc11 = new DataColumn("TruckID", typeof(string));
            DataColumn dc12 = new DataColumn("ProponentID", typeof(int));
            DataColumn dc13 = new DataColumn("ProponentName", typeof(string));
            DataColumn dc14 = new DataColumn("HopperName", typeof(string));

            DataColumn dc15 = new DataColumn("NewMarkerID", typeof(int));
            DataColumn dc16 = new DataColumn("OldMarkerID", typeof(int));

            DataColumn dc17 = new DataColumn("HopperStatus", typeof(string));
            DataColumn dc18 = new DataColumn("ReArrivedAtEntry", typeof(bool)); 
          

            dtLane1.Columns.Add(dc1);
            dtLane1.Columns.Add(dc2);
            dtLane1.Columns.Add(dc3);
            dtLane1.Columns.Add(dc4);
            dtLane1.Columns.Add(dc5);
            dtLane1.Columns.Add(dc6);
            dtLane1.Columns.Add(dc7);
            dtLane1.Columns.Add(dc8);
            dtLane1.Columns.Add(dc9);
            dtLane1.Columns.Add(dc10);
            dtLane1.Columns.Add(dc11);
            dtLane1.Columns.Add(dc12);
            dtLane1.Columns.Add(dc13);
            dtLane1.Columns.Add(dc14);

            dtLane1.Columns.Add(dc15);
            dtLane1.Columns.Add(dc16);

            dtLane1.Columns.Add(dc17);
            dtLane1.Columns.Add(dc18);

            DataColumn dc = new DataColumn("TruckWash", typeof(String));
            dtLane1.Columns.Add(dc);
            dc = new DataColumn("ExitTime", typeof(String));
            dtLane1.Columns.Add(dc);


            dtLane2 = dtLane1.Copy();

            Session["dtLane"] = dtLane1;
        }


    }
}
