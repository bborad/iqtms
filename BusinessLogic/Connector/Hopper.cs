using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ramp.MiddlewareController.Common;
using Ramp.BusinessLogic.Common;
using DBUtility = Ramp.DBConnector.Common;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.DBConnector.Adapter;
using System.Threading;
using Modbus = Ramp.ModbusConnector;
using Ramp.MiddlewareController.Connector.BusinessLogicConnector;

using NLog;
using Ramp.MiddlewareController.Connector.VMSHardWareConnector;


namespace Ramp.BusinessLogic.Connector
{
    public class Hopper
    {

        private static Logger log = LogManager.GetCurrentClassLogger();

        public static int NoofTrucksInQueueArea { get; set; }

        public static int NoofTrucksDirectedtoQueue { get; set; }

        static bool maxQueueReported = false;

        Predicate<ITags> myMatch;

        public object lockList = new object();

        static object lockQueueLength = new object();

        public object lockHopper = new object();

        public object lockStartupShutDown = new object();

        public int HopperID { get; set; }

        public bool HopperAvailable { get; set; }
        public int MarkerID { get; set; }

        public int GroupID { get; set; }

        public int GroupOrder { get; set; }

        public string HopperName { get; set; }
        
        //public ArrayList lstQueue { get; set; }

        public List<ITags> lstQueue { get; set; }

        public System.Timers.Timer tmrQueue { get; private set; }
        public int tmrInterval { get; set; }

        public bool IsTimerPaused { get; set; }

        public bool NoTruckFoundOnCallup { get; set; }

        public bool timerFlag { get; set; }

        public Location Location_PadStartSet { get; set; }

        public DateTime PadStart_Time { get; set; }

        public System.Timers.Timer tmrPadStartup { get; private set; }

        public Thread thPadStartup { get; private set; }

        public DateTime TimerStartTime { get; set; }

        public DateTime TimerStoppedTime { get; set; }

        public DateTime LastTruckDetectionTime { get; set; }

        public int Ent2_Hopper { get; set; }

        public bool nextCallupOccured;

        object lockCallup = new object();

        public int Hopper_TW { get; set; }

        public string LastTruckCalledup { get; set; }

        public bool IsAdjustTimerCalledUp { get; set; }

        public Thread thQueue { get; private set; }

        public Hopper(bool testing)
        {

            ITags objtag = new Tags();
            objtag.TagRFID = "123456789";

            lstQueue.Add(objtag);

            objtag = new Tags();
            objtag.TagRFID = "555436789";
            lstQueue.Add(objtag);

            objtag = new Tags();

            objtag.TagRFID = "646446464";
            lstQueue.Add(objtag);

            Ent2_Hopper = 10;


        }

        public Hopper()
        {
            //lstQueue = new ArrayList();
            lstQueue = new List<ITags>();
            LastTruckDetectionTime = DateTime.Now.Subtract(new TimeSpan(0, 0, ExpectedValues.Unloading * 2));
            TimerStartTime = DateTime.Now;

            tmrPadStartup = new System.Timers.Timer();
            tmrPadStartup.Elapsed += new System.Timers.ElapsedEventHandler(tmrPadStartup_Elapsed);
            IsTimerPaused = false;

            PadStart_Time = DateTime.MinValue;
            NoTruckFoundOnCallup = false;
            HopperAvailable = true;
        }

        void tmrPadStartup_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
#if DEBUG
                log.Debug("tmrPadStartup timer elapsed for {0} pad. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                // Start Hopper using modbus library   
                // After Changes made on 29th Oct 2010
                tmrPadStartup.Stop();
                short padStatus = 0;

                byte padID = Convert.ToByte(HopperName);

                lock (lockStartupShutDown)
                {
                    if (BLUtility.IsModbusConnected)
                    {
                        padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(padID);
                    }
                    else
                    {
                        BLUtility.StartModbusServer();
                        if (BLUtility.IsModbusConnected)
                        {
                            padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(padID);
                        }
                    }

                    if (padStatus == 0)
                    {
                        if (!BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                                  x.Value.FoundAtHopperID == HopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID))
                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 1);
                    }
                    else
                    {
                        BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);

                        if (HopperAvailable)
                            BLUtility.UpdateHopperStatus();
                    }

                    Location_PadStartSet = Location.UnKnown;
                }

            }
            catch(Exception ex)
            {
#if DEBUG
                log.Debug("Exception thrown in tmrPadStartup timer elapsed method for {0} pad. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            }
            finally
            {
                timerFlag = false;
            }


        }

        public void InitializeTimer()
        {
            if (tmrQueue != null)
            {
                tmrQueue.Stop();
                tmrQueue.Close();
                tmrQueue = null;
            }

            tmrQueue = new System.Timers.Timer();
            tmrQueue.AutoReset = false;
            tmrQueue.Elapsed += new System.Timers.ElapsedEventHandler(tmrQueue_Elapsed);
        }

        void tmrQueue_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //try
            //{
            //    if (thQueue != null)
            //    {
            //        if (thQueue.ThreadState != ThreadState.Stopped && thQueue.ThreadState != ThreadState.StopRequested && thQueue.ThreadState != ThreadState.Aborted && thQueue.ThreadState != ThreadState.AbortRequested)
            //            thQueue.Abort();
            //    }
            //}
            //catch
            //{
            //}
            //  nextCallupOccured = true;
              nextCallupOccured = true;
                TruckCallup();
                nextCallupOccured = false;
           


        }

        public void TruckCallup(params string[] skipedTag)
        {
            nextCallupOccured = true;

            try
            {
                StopTimer();

#if DEBUG
                Algorithms.checkPoint = "TCall: Entered: 238";
                log.Debug("Entered into TruckCallup method in Hopper class for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif


                if (BLUtility.ModbusProcessor.ReadPAD_Status((byte)HopperID) != 0)
                    HopperAvailable = false;
                else
                    HopperAvailable = true;



#if DEBUG
                log.Debug("In TruckCallup method. Update HopperAvailable to {2} (Before updating hopper status). for {0} H. Thread {1}", HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString(), HopperAvailable.ToString());
#endif

                #region checking hopper status. Changed to update all hopper status in db
                if (BLUtility.IsModbusConnected)
                {
                    //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
                    BLUtility.UpdateHopperStatus();
                }
                else
                {
#if DEBUG
                    log.Debug("In TruckCallup method.Starting MODBUS server. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    BLUtility.StartModbusServer();
                    if (BLUtility.IsModbusConnected)
                    {
                        //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
                        BLUtility.UpdateHopperStatus();
                    }
                }
                #endregion

#if DEBUG
                log.Debug("In TruckCallup method. Finished updating hopper status. for {0} H. Thread {1}", HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString());
#endif


                Dictionary<string, int> taglist = null;
                string skipTagRFID = "", callupTagRFID = "";
                int TimeToWait = 0;


                try
                {
                    int gID = BLUtility.HoppersInFacility[HopperID.ToString()].GroupID;
                    Hopper hop = BLUtility.HoppersInFacility.Where(x => x.Value.GroupID == gID
                        && x.Value.HopperAvailable).OrderBy(x => x.Value.GroupOrder).First(x => x.Value.tmrQueue == null
                            || (!x.Value.nextCallupOccured && !x.Value.IsTimerPaused && (!x.Value.tmrQueue.Enabled))).Value;  // .ToDictionary(x=>x.Key,x=>x.Value);
                    if (hop != null && hop.GroupOrder < this.GroupOrder)//&& BLUtility.TrucksInTransaction.Any(x=>x.Value.LastPositiion == Location.QUEUE && BLUtility.HoppersInFacility[x.Value.TagDB.DestinationPad.ToString()].GroupID == GroupID))
                    {
#if DEBUG
                        log.Debug("In TruckCallup method. Calling truck calllup on other hopper[{2}] as it has higher priority. for {0} H. Thread {1}", HopperID,
                            Thread.CurrentThread.ManagedThreadId.ToString(), hop.HopperName);
#endif

                        hop.TruckCallup(skipedTag);
                        nextCallupOccured = false;
                        StartTimer(1000);
#if DEBUG
                        log.Debug("In TruckCallup method. Returning current callup as called up on hight priority hopper. for {0} H. Thread {1}", HopperID,
                            Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                        return;
                    }
                }
                catch  
                {

                    // it throw exception in normal case
                }

                if (skipedTag.Count() > 0 && !string.IsNullOrEmpty(skipedTag[0]))
                {
                    skipTagRFID = skipedTag[0];
#if DEBUG
                    log.Debug("In TruckCallup method. Skipped truck{2}. set CalledUp to false. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString(), skipedTag[0]);
#endif
                    if (BLUtility.TrucksInTransaction.ContainsKey(skipTagRFID))
                        BLUtility.TrucksInTransaction[skipTagRFID].CalledUp = false;

                }


                //need to skip current calledup truck if it doesn't respons, if hopper is not available

                if (!HopperAvailable) // if not operational
                {
#if DEBUG
                    log.Debug("In TruckCallup method. Calling ReassignAllPad as current hopper is off. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    //pad is not operational. reassign 
                    DBConnector.Adapter.ActiveReads.ReassignAllPad(GroupID, skipTagRFID);
                    nextCallupOccured = false;

                    try
                    {
                        BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)HopperID).ShowString("CLOSED", "`CFF0000", 0);
                    }
                    catch { }

#if DEBUG

                    log.Info("Starting timer with 2000 ms for {0} pad in TruckCallup as Hopper is not available", HopperID);
#endif
                    StartTimer(2000);
                    return;
                }


#if DEBUG
                log.Debug("In TruckCallup method. Getting lock on Algorithm.CallupLocker. for {0} H. Thread {1}", HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                lock (Algorithms.CallupLocker)
                {
#if DEBUG
                    log.Debug("In TruckCallup method. Locked CallupLocker. Calling DB - NextTruckToCallup. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    /*
                     * 10-7-2012 : Issue when a truck doesn't respond on callup, it calls truck in DB but seems to be throwing
                     * excepiton after returning from the DB and causes no truck to call
                     *                      * 
                     */ 
                    taglist = DBConnector.Adapter.ActiveReads.NextTruckToCallup(skipTagRFID, HopperID, out callupTagRFID, out TimeToWait);

                    int a=0;
                    try
                    {
                        if (taglist != null)
                            Int32.TryParse(taglist.Count.ToString(),out a);
                    }
                    catch { }
#if DEBUG
                    log.Debug("In TruckCallup method. Return from DB - NextTruckToCallup with {2} truck and {3} time to wait. Truck changed {4}. Checking for Reassigned trucks. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString(), callupTagRFID, TimeToWait,a);
#endif

                }//end of lock

                
                    #region update new des pad
                    if (taglist != null && taglist.Count > 0)
                    {
#if DEBUG
                        log.Debug("In TruckCallup method. Des pad of {2} trucks in q are changed. Getting lock on truckListLock. for {0} H. Thread {1}", HopperID,
                            Thread.CurrentThread.ManagedThreadId.ToString(), taglist.Count);
                        Algorithms.LogAllTruckHoppers(log);
#endif

                        

                        lock (BLUtility.truckListLock)
                        {

#if DEBUG
                            log.Debug("In TruckCallup method. Des pad of {2} trucks in q are changed. Locked truckListLock. for {0} H. Thread {1}", HopperID,
                                Thread.CurrentThread.ManagedThreadId.ToString(), taglist.Count);
#endif
                            foreach (string k in taglist.Keys)
                                if (BLUtility.TrucksInTransaction.ContainsKey(k))
                                {

                                    if (BLUtility.TrucksInTransaction[k].TagDB.DestinationPad != taglist[k])
                                    {
                                        try
                                        {
                                            BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)BLUtility.TrucksInTransaction[k].TagDB.DestinationPad).RemoveMessage(BLUtility.TrucksInTransaction[k].TagDB.TruckID, "`C00FF00");
                                        }
                                        catch { }
#if DEBUG
                                        log.Debug("In TruckCallup method. Des pad of {2} trucks in q are changed. Changing Des pad of {3} truck to {4}. for {0} H. Thread {1}", HopperID,
                                            Thread.CurrentThread.ManagedThreadId.ToString(), taglist.Count,k,taglist[k]);
#endif
                                        BLUtility.TrucksInTransaction[k].TagDB.DestinationPad = taglist[k];
#if DEBUG
                                        log.Debug("In TruckCallup method. Des pad of {2} trucks in q are changed. Changed Des pad of {3} truck to {4}. for {0} H. Thread {1}", HopperID,
                                            Thread.CurrentThread.ManagedThreadId.ToString(), taglist.Count, k, taglist[k]);
#endif
                                        try
                                        {
                                            BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)BLUtility.TrucksInTransaction[k].TagDB.DestinationPad).ShowString(BLUtility.TrucksInTransaction[k].TagDB.TruckID, "`C00FF00", 0);
                                        }
                                        catch { }

                                    }

                                    //Date:17-7-2012
                                    //Key may not exist in HopperInFacility if taglist[k] is (which will be destination pad and can be) null or 0 
                                    if(BLUtility.HoppersInFacility.ContainsKey(taglist[k].ToString()))
                                    { 
                                        if (BLUtility.HoppersInFacility[taglist[k].ToString()].tmrQueue == null 
                                            || (!BLUtility.HoppersInFacility[taglist[k].ToString()].tmrQueue.Enabled && 
                                                !BLUtility.HoppersInFacility[taglist[k].ToString()].IsTimerPaused && 
                                                    !BLUtility.HoppersInFacility[taglist[k].ToString()].nextCallupOccured))
                                            {

#if DEBUG
                                                log.Debug("In TruckCallup method. Getting nextCallupTime for {2} Hopper as trucks were reassigned. for {0} H. Thread {1}", HopperID,
                                                    Thread.CurrentThread.ManagedThreadId.ToString(), taglist[k]);
#endif
                                                int callTimer;
                                                ActiveReads.GetNextCallupTime(taglist[k], out callTimer);
                                                BLUtility.HoppersInFacility[taglist[k].ToString()].StartTimer(callTimer * 1000);



                                            }
                                    }//enf of if HoppersInFacility contains key

                                }//end of if TrucksInTransaction contains key

 
                        }
#if DEBUG
                        log.Debug("In TruckCallup method. Des pad of {2} trucks in q are changed. Release the lock on truckListLock. for {0} H. Thread {1}", HopperID,
                            Thread.CurrentThread.ManagedThreadId.ToString(), taglist.Count);
                        Algorithms.LogAllTruckHoppers(log);
#endif

                    }//end of if return taglist is not empty
#endregion 

#if DEBUG
                log.Debug("In TruckCallup method. Finished checking Reassigned trucks and Unlock Algorithm.CallupLocker. for {0} H. Thread {1}", HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                if (string.IsNullOrEmpty(callupTagRFID.Trim()))
                {
#if DEBUG
                    log.Debug("In TruckCallup method. No trucks to callup for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    //no truck found in queue to call up
                    //setting it to 1 will call next truck as soon as timer is started from queue
                    nextCallupOccured = false;
                    StartTimer(2000);
                }
                else
                {
#if DEBUG
                    log.Debug("In TruckCallup method. Calling UpdateTruckCallup for {2} truck. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString(), callupTagRFID);
#endif
                    LastTruckCalledup = callupTagRFID;
                    Algorithms.UpdateTruckCallUp(callupTagRFID, HopperID);
                    StartTimer(ExpectedValues.Unloading * 1000);
#if DEBUG
                    log.Debug("In TruckCallup method. Finished UpdateTruckCallup for{2} truck and started next callup timer. for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString(), callupTagRFID);
#endif
                }

                

            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException("Exception thrown in call up algorith.", ex);
#endif


            }
            finally
            {
#if DEBUG
                log.Debug("In TruckCallup method. Finished TruckCallup. for {0} H. Thread {1}", HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            }
            nextCallupOccured = false;



            #region < 1.5 version : Disabled
            //try
            //{
            //    nextCallupOccured = true; 
            //    tmrQueue.Stop();

            //    int Itemcount = 0;

            //    lock (lockList)
            //    {
            //        Itemcount = lstQueue.Count;
            //    }               

            //    if (Itemcount > 0)
            //    {

            //        NoTruckFoundOnCallup = false;

            //        ITags objTag = (ITags)lstQueue[0];

            //        BLUtility.WriteEventLog("Truck Callup Occured. " + objTag.TagRFID + " is called up.", System.Diagnostics.EventLogEntryType.Information);

            //        RemoveFromQueue(objTag);

            //        Itemcount = Itemcount - 1;

            //        if (Algorithms.UpdateTruckCallUp(objTag.TagRFID))
            //        {

            //           // RemoveFromQueue(0);                        
            //            //Thread th = new Thread(delegate() { CheckTruckResponse(objTag); });
            //            //th.Start();
            //        }
            //        else
            //        {


            //            if (Itemcount > 0 && !IsTimerPaused)
            //            {
            //                SetTimer(0.5);
            //                // tmrQueue.Start();
            //                StartTimer();
            //                IsTimerPaused = false;
            //            }


            //            nextCallupOccured = false;

            //            return;
            //        }

            //        if (Itemcount > 0 && !IsTimerPaused)
            //        {
            //            //AdjustTimer(objTag);
            //            //IsAdjustTimerCalledUp = true;

            //            SetTimer(ExpectedValues.Unloading);
            //            StartTimer();
            //            IsTimerPaused = false;

            //        }


            //        nextCallupOccured = false;

            //    }
            //    else
            //    {
            //        NoTruckFoundOnCallup = true;
            //        nextCallupOccured = false; 

            //        BLUtility.WriteEventLog("Truck Callup Occured, no trucks found in queue.", System.Diagnostics.EventLogEntryType.Information);


            //    }
            //}
            //catch
            //{
            //    nextCallupOccured = false; 
            //    //SetTimer(ExpectedValues.Unloading);
            //    SetTimer(0.5);               
            //    StartTimer();
            //}
            #endregion
        }

        //void _AdjustTimer(ITags objTag)
        //{
        //    try
        //    {
        //        lock (lockCallup)
        //        {
        //            int extraTime = 0;

        //            nextCallupOccured = false;

        //            string TagRFID = objTag.TagRFID;

        //            SetTimer(ExpectedValues.Unloading);
        //            StartTimer();

        //            IsTimerPaused = true;

        //            // int TruckCallResponseTime = ExpectedValues.TruckCallResponse; 

        //            // while (!DBUtility.DBConnectorUtility.IsTruckArrived(Location.ENTRY1, TagRFID))
        //            while (!BLUtility.IsTruckArrivedFromLocation(Location.ENTRY1, TagRFID))
        //            {
        //                if (nextCallupOccured)
        //                    return;

        //                extraTime++;
        //                if (extraTime > ExpectedValues.Queue_Ent1)
        //                {
        //                    if (IsTimerPaused == false)
        //                    {
        //                        IsTimerPaused = true;
        //                        tmrQueue.Stop();
        //                    }

        //                    if ((extraTime - ExpectedValues.Queue_Ent1) > ExpectedValues.Max_Delay) // Changes as per document ver 1.4 (TotalDelay > MaxDelay)
        //                    {
        //                        if (lstQueue.Count > 0)
        //                        {
        //                            SetTimer(ExpectedValues.Unloading);
        //                            StartTimer();
        //                        }

        //                        IsTimerPaused = false;

        //                        return;
        //                    }
        //                }

        //                /// Check to Skip non Responding Truck

        //                //if (TruckCallResponseTime <= 0)
        //                //{
        //                //    SkipNonResponding(objTag);
        //                //    return;
        //                //}
        //                //else
        //                //{
        //                //    TruckCallResponseTime--;
        //                //}

        //                System.Threading.Thread.Sleep(200);
        //            }

        //            if (extraTime > ExpectedValues.Queue_Ent1)
        //            {
        //                Int32 delay = Convert.ToInt32(tmrQueue.Interval) - (ExpectedValues.Queue_Ent1 * 1000);

        //                delay = delay / 1000;

        //                if (delay > 0)
        //                {
        //                    SetTimer(delay);
        //                    StartTimer();
        //                }

        //                IsTimerPaused = false;
        //            }

        //            // Changes made on 17/5/2011
        //            //int value = ExpectedValues.TruckCallResponse - TruckCallResponseTime;

        //            //if (value > ExpectedValues.Unloading)
        //            //{
        //            //    SetTimer(1);
        //            //}
        //            //else
        //            //{
        //            //    SetTimer(ExpectedValues.Unloading - value);
        //            //} 

        //            //StartTimer();
        //            //IsTimerPaused = false;

        //            extraTime = 0;

        //            while (!BLUtility.IsTruckArrivedFromLocation(Location.ENTRY2, TagRFID))
        //            {
        //                extraTime++;

        //                if (nextCallupOccured)
        //                    return;

        //                if (extraTime > ExpectedValues.Ent1_Ent2)
        //                {
        //                    //if (nextCallupOccured)
        //                    //    return;

        //                    if (IsTimerPaused == false)
        //                    {
        //                        IsTimerPaused = true;
        //                        tmrQueue.Stop();
        //                    }

        //                    if ((extraTime - ExpectedValues.Ent1_Ent2) > ExpectedValues.Max_Delay)
        //                    {
        //                        if (lstQueue.Count > 0)
        //                        {
        //                            //SetTimer(ExpectedValues.Unloading);
        //                            SetTimer(2);
        //                            StartTimer();
        //                        }

        //                        IsTimerPaused = false;

        //                        return;
        //                    }
        //                }

        //                //if (nextCallupOccured)
        //                //    return;

        //                System.Threading.Thread.Sleep(200);
        //            }

        //            if (extraTime > ExpectedValues.Ent1_Ent2)
        //            {
        //                Int32 delay = Convert.ToInt32(tmrQueue.Interval) - (ExpectedValues.Ent1_Ent2 * 1000);
        //                delay = delay / 1000;
        //                if (delay > 0)
        //                {
        //                    SetTimer(delay);
        //                    StartTimer();
        //                }
        //                IsTimerPaused = false;
        //            }

        //            extraTime = 0;

        //            while (!BLUtility.IsTruckArrivedFromLocation(Location.HOPPER, TagRFID))
        //            {
        //                extraTime++;

        //                if (nextCallupOccured)
        //                    return;

        //                if (extraTime > Ent2_Hopper)
        //                {
        //                    if (IsTimerPaused == false)
        //                    {
        //                        IsTimerPaused = true;
        //                        tmrQueue.Stop();
        //                    }

        //                    if ((extraTime - Ent2_Hopper) > ExpectedValues.Max_Delay)
        //                    {
        //                        if (lstQueue.Count > 0)
        //                        {
        //                            //SetTimer(ExpectedValues.Unloading);
        //                            SetTimer(2);
        //                            StartTimer();
        //                        }

        //                        IsTimerPaused = false;
        //                        return;
        //                    }
        //                }

        //                //if (nextCallupOccured)
        //                //    return;

        //                System.Threading.Thread.Sleep(200);
        //            }

        //            if (extraTime > Ent2_Hopper)
        //            {
        //                Int32 delay = Convert.ToInt32(tmrQueue.Interval) - (Ent2_Hopper * 1000);
        //                delay = delay / 1000;
        //                if (delay > 0)
        //                {
        //                    SetTimer(delay);
        //                    StartTimer();
        //                }
        //                IsTimerPaused = false;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        SetTimer(ExpectedValues.Unloading);
        //        StartTimer();
        //    }

        //    IsTimerPaused = false;
        //}


        public void AdjustTimer(int timerAdjust, bool newTruck)
        {
            IsTimerPaused = true;
            double timerInterval = 1;
            if (!nextCallupOccured)
            {
                try
                {
                    double newTimeInt = 1;
                    if (tmrQueue != null)
                    {
                        timerInterval = tmrQueue.Interval;
                        if (tmrQueue.Enabled)
                            StopTimer();
                    }

                    if (timerAdjust > 0 && !newTruck)
                        timerAdjust = 0;

                    newTimeInt = (timerInterval - (TimerStoppedTime.Subtract(TimerStartTime).TotalSeconds * 1000)) + (timerAdjust * 1000);


                    if (newTimeInt < 1)
                        newTimeInt = 1;

                    //SetTimer();
#if DEBUG
                    log.Info("Starting timer with {0} ms for {1} pad in AdjustTimer", newTimeInt, HopperID);
#endif
                    SetTimer(newTimeInt);
                    //StartTimer(newTimeInt);

                }
                catch (Exception ex)
                {
#if DEBUG
                    log.ErrorException(string.Format("Exception thrown in AdjustTimer method in Hopper class for {0} H. Thread {1}", HopperID,
                        Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
                }
            }
            IsTimerPaused = false;
        }

        //void AdjustCallUpTimer()
        //{
        //    try
        //    {
        //        //lock (lockCallup)
        //        //{    


        //        var delayedTrucksAtEntry1 = from ObjTruckAtE1 in BLUtility.TrucksInTransaction.Values where (ObjTruckAtE1.TagDB.DestinationPad == HopperID && ObjTruckAtE1.LastPositiion == Location.ENTRY1 && ObjTruckAtE1.LastReadTime.AddSeconds(ExpectedValues.Ent1_Ent2) < DateTime.Now) select ObjTruckAtE1;

        //        var delayedTrucksAtEntry2 = from ObjTruckAtE2 in BLUtility.TrucksInTransaction.Values where (ObjTruckAtE2.TagDB.DestinationPad == HopperID && ObjTruckAtE2.LastPositiion == Location.ENTRY2 && ObjTruckAtE2.LastReadTime.AddSeconds(Ent2_Hopper) < DateTime.Now) select ObjTruckAtE2;

        //        var TrucksAtQueue = from ObjTruckAtQueue in BLUtility.TrucksInTransaction.Values where (ObjTruckAtQueue.TagDB.DestinationPad == HopperID && ObjTruckAtQueue.LastPositiion == Location.QUEUE) select ObjTruckAtQueue;

        //        // int TruckCallResponseTime = ExpectedValues.TruckCallResponse; 

        //        // Check for the trucks delay betwwen Entry1 to PAD

        //        int count = 0;
        //        int countAtEntry1 = 0, countAtEntry2 = 0;

        //        while (true)
        //        {

        //            lock (BLUtility.truckListLock)
        //            {
        //                if (TrucksAtQueue.Count() <= 0)
        //                {
        //                    IsTimerPaused = false;
        //                    IsAdjustTimerCalledUp = false;
        //                    break;
        //                }

        //                countAtEntry1 = delayedTrucksAtEntry1.Count();
        //                countAtEntry2 = delayedTrucksAtEntry2.Count();
        //            }

        //            //var delayedTrucksAtEntry1 = from ObjTruckAtE1 in BLUtility.TrucksInTransaction.Values where (ObjTruckAtE1.TagDB.HopperID == HopperID && ObjTruckAtE1.LastPositiion == Location.ENTRY1 && ObjTruckAtE1.LastReadTime.AddSeconds(ExpectedValues.Ent1_Ent2) > DateTime.Now) select ObjTruckAtE1;

        //            //lock (BLUtility.truckListLock)
        //            //{
        //            //    countAtEntry1 = delayedTrucksAtEntry1.Count();
        //            //}

        //            if (countAtEntry1 > 0)
        //            {
        //                if (!IsTimerPaused && !nextCallupOccured)
        //                {
        //                    IsTimerPaused = true;
        //                    StopTimer();

        //                }
        //            }
        //            else
        //            {
        //                //var delayedTrucksAtEntry2 = from ObjTruckAtE2 in BLUtility.TrucksInTransaction.Values where (ObjTruckAtE2.TagDB.HopperID == HopperID && ObjTruckAtE2.LastPositiion == Location.ENTRY2 && ObjTruckAtE2.LastReadTime.AddSeconds(Ent2_Hopper) > DateTime.Now) select ObjTruckAtE2;

        //                //lock (BLUtility.truckListLock)
        //                //{
        //                //    countAtEntry2 = delayedTrucksAtEntry2.Count();
        //                //}

        //                if (countAtEntry2 > 0)
        //                {
        //                    if (!IsTimerPaused && !nextCallupOccured)
        //                    {
        //                        IsTimerPaused = true;
        //                        StopTimer();                               
        //                    }
        //                }
        //                else
        //                {
        //                    if (IsTimerPaused && !tmrQueue.Enabled)
        //                    {

        //                        double interval = 0;

        //                        if (TimerStartTime >= TimerStoppedTime)
        //                        {
        //                            interval = DateTime.Now.Subtract(TimerStartTime).TotalSeconds;
        //                        }
        //                        else
        //                        {
        //                            interval = TimerStoppedTime.Subtract(TimerStartTime).TotalSeconds;
        //                        }

        //                        interval = ExpectedValues.Unloading - Math.Abs(interval);

        //                        if (interval <= 0)
        //                            interval = 0.5;

        //                        SetTimer(interval);

        //                        StartTimer();
        //                        IsTimerPaused = false;
        //                    }


        //                }
        //            }

        //            System.Threading.Thread.Sleep(500);
        //        }


        //        // }
        //    }
        //    catch
        //    {
        //        if (IsTimerPaused && !tmrQueue.Enabled)
        //        {
        //            SetTimer(ExpectedValues.Unloading);
        //            StartTimer();
        //        }
        //    }

        //    IsTimerPaused = false;
        //}

        //void CheckTruckResponse(ITags objTag)
        //{
        //    try
        //    {
        //        int TruckCallResponseTime = ExpectedValues.TruckCallResponse - 1;

        //        while (!BLUtility.IsTruckArrivedFromLocation(Location.ENTRY1, objTag.TagRFID))
        //        {
        //            /// Check to Skip non Responding Truck
        //            if (TruckCallResponseTime <= 0)
        //            {
        //                SkipNonResponding(objTag);
        //                return;
        //            }
        //            else
        //            {
        //                TruckCallResponseTime--;
        //            }

        //            System.Threading.Thread.Sleep(1000);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        public void SetTimer(params double[] intv)
        {
            double interval = ExpectedValues.Unloading * 1000;

            if (intv.Count() > 0)
                interval = intv[0];

            if (tmrQueue == null)
            {
                tmrQueue = new System.Timers.Timer();
                tmrQueue.Elapsed += new System.Timers.ElapsedEventHandler(tmrQueue_Elapsed);
                tmrQueue.AutoReset = false;
            }
           
                tmrQueue.Stop();
            if (interval < 1)
                interval = 1;

            tmrQueue.Interval = interval;
            //BLUtility.WriteEventLog("Callup Timer Interval Set with value " + interval.ToString() + " secs.", System.Diagnostics.EventLogEntryType.Information);
        }

        public void StartTimer(params double[] intv)
        {

            try
            {
                if (tmrQueue != null && tmrQueue.Enabled)
                    StopTimer();

                if (tmrQueue == null)
                    SetTimer();

                if (intv.Count() > 0)
                    SetTimer(intv[0]);

                IsTimerPaused = false;
                tmrQueue.Start();

                TimerStartTime = DateTime.Now;

               // BLUtility.WriteEventLog("Callup Timer Started at " + TimerStartTime.ToString(), System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in StartTimer in Hopper class for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        public void StopTimer()
        {
            if (tmrQueue == null || !tmrQueue.Enabled)
                    return;

            TimerStoppedTime = DateTime.Now;
            
                tmrQueue.Stop();
           // BLUtility.WriteEventLog("Callup Timer Stoped at " + TimerStoppedTime.ToString(), System.Diagnostics.EventLogEntryType.Information);
        }



        public bool SkipNonResponding(ITags objTag)
        {
            bool result = false;

            bool IsTransClosed = false;

            if (tmrQueue != null && tmrQueue.Enabled)
                tmrQueue.Stop();

            try
            {


                ITruck objTruck = null;
                lock (BLUtility.truckListLock)
                {
                    objTruck = BLUtility.TrucksInTransaction[objTag.TagRFID];

                }

                if (objTruck != null)
                {
                    objTruck.iTruckCallupCount += 1;

                    #region removing VMS message
                    string formatString = "`C00FF00 ";
                    if (BLUtility.VMSDeviceList.ContainsKey(Location.QUEUE))
                    {
                        List<IVMSDevice> lstVMSDevices = BLUtility.VMSDeviceList[Location.QUEUE];

                        if (lstVMSDevices != null && lstVMSDevices.Count > 0)
                        {
                            foreach (IVMSDevice obj in lstVMSDevices)
                            {
                                obj.RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " - PAD " + HopperID.ToString().PadLeft(2, '0'), formatString);
                            }
                        }

                    }
                    if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                    {
                        try
                        {
                            BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)HopperID).RemoveMessage(objTag.TruckID, "`C00FF00");
                        }
                        catch { }
                    }
                    #endregion

                    if ((int)BLUtility.ConfigValues[ConfigurationValues.CallupRetries] > -1 && objTruck.iTruckCallupCount >= (int)BLUtility.ConfigValues[ConfigurationValues.CallupRetries])
                    {
                        #region closing transaction
                        // IsTransactionClosed = true;
                        objTruck.TransMessage = TransactionMessages._TruckNotRespondOnCallup;
                        objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Incomplete);
                        Algorithms.TransactionTimeOut(objTruck.TagRFID);
                        #endregion
                    }






                    //IsTruckStillInQueue(objTag.TagRFID);
                    TruckCallup(objTag.TagRFID);

                }



                #region < 1.5 Disabled
                //result = Algorithms.SkipNonRespondingTruck(objTag.TagRFID, ref IsTransClosed);

                //var TrucksForSameHopper = from objTruck in BLUtility.TrucksInTransaction.Values where (objTruck.TagDB.DestinationPad  == HopperID && ((objTruck.LastPositiion == Location.QUEUE))) select objTruck;
                //int count = 0;
                //lock (BLUtility.truckListLock)
                //{
                //    count = TrucksForSameHopper.Count();
                //}

                //if (result == true)
                //{

                //    AddToQueue(objTag, false);
                //    //TruckCallup();  

                //    if (count == 1)
                //    {
                //        if (IsTimerPaused == false && !tmrQueue.Enabled)
                //        {
                //            IsTimerPaused = false;
                //            SetTimer(0.5);
                //            //tmrQueue.Start();
                //            StartTimer();
                //        }
                //    }
                //    else if (count > 1)
                //    {
                //        if (IsTimerPaused == false && !tmrQueue.Enabled)
                //        {
                //            IsTimerPaused = false;
                //            SetTimer(ExpectedValues.Unloading);
                //            StartTimer();
                //            //tmrQueue.Start();
                //        }
                //    }

                //}
                //else
                //{
                //    if (!IsTransClosed)
                //    {
                //        ITruck nonResponsiveTruck = null;
                //        lock (BLUtility.truckListLock)
                //        {
                //            nonResponsiveTruck = BLUtility.TrucksInTransaction[objTag.TagRFID];
                //        }

                //        if (nonResponsiveTruck != null && nonResponsiveTruck.LastPositiion == Location.QUEUE)
                //        {
                //            IsTimerPaused = false;
                //            AddToQueue(objTag, false);
                //        }
                //    }


                //}
                #endregion






            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in SkipNonResponding method in Hopper class for {0} truck {1} H. Thread {2}", objTag.TagRFID, HopperID,
                    Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }



            StartTimer(ExpectedValues.Unloading * 1000);
#if DEBUG
            log.Info("Starting timer with {0} ms for {1} pad in SkipNonResponding", ExpectedValues.Unloading * 1000, HopperID);
#endif
            return result;
        }

        public bool AddToQueue(ITags objTag, bool sortByArrival)
        {
            bool addedToList = false;
            try
            {
                lock (lockList)
                {
                    if (!lstQueue.Exists(x => x.TagRFID == objTag.TagRFID))
                    {
                        lstQueue.Add(objTag);
                        //if (sortByArrival && lstQueue.Count > 1)
                        //    lstQueue.Sort(CompareTruckArrivalAtREDS);   
                        addedToList = true;
                        NoofTrucksInQueueArea++;
                    }
                }

                //if (!IsAdjustTimerCalledUp)
                //{
                //    IsAdjustTimerCalledUp = true;
                //    Thread th = new Thread(delegate() { AdjustCallUpTimer(); });
                //    th.Start();

                //} 

                CheckMaxQueueSize();
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in AddToQ method for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
            return addedToList;
        }

        public void RemoveFromQueue(int index)
        {
            try
            {
                lock (lockList)
                {
                    if (lstQueue.Count <= 0)
                        return;

                    lstQueue.RemoveAt(index);
                    NoofTrucksInQueueArea--;
                }
                if (maxQueueReported)
                {
                    CheckMaxQueueSize();
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in RemoveFromQ method for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        public void RemoveFromQueue(ITags objTag)
        {
            try
            {
                lock (lockList)
                {
                    if (lstQueue.Count <= 0)
                        return;

                    int index = lstQueue.FindIndex(x => x.TagRFID == objTag.TagRFID);

                    // if (lstQueue.Contains(objTag))
                    if (index >= 0)
                    {
                        //lstQueue.Remove(objTag);

                        lstQueue.RemoveAt(index);

                        //if (lstQueue.Count <= 0 && tmrQueue.Enabled)
                        //{
                        //    tmrQueue.Stop();
                        //    IsTimerPaused = false;
                        //}

                        NoofTrucksInQueueArea--;
                    }

                }

                if (maxQueueReported)
                {
                    CheckMaxQueueSize();
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in RemoveFromQ(Tag) method for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        public void RemoveFromQueue(string tagRFID)
        {
            try
            {
                lock (lockList)
                {
                    if (lstQueue.Count <= 0)
                        return;

                    int index = lstQueue.FindIndex(x => x.TagRFID == tagRFID);

                    // if (lstQueue.Contains(objTag))
                    if (index >= 0)
                    {
                        //lstQueue.Remove(objTag);

                        lstQueue.RemoveAt(index);

                        //if (lstQueue.Count <= 0 && tmrQueue.Enabled)
                        //{
                        //    tmrQueue.Stop();
                        //    IsTimerPaused = false;
                        //}

                        NoofTrucksInQueueArea--;
                    }

                }

                if (maxQueueReported)
                {
                    CheckMaxQueueSize();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in RemoveFromQ(st tagiD) method for {0} H. Thread {1}", HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }


        public void Startup_ShutDownPAD(Location checkPoint)
        {

            try
            {
                if (thPadStartup != null)
                {
                    if (!thPadStartup.IsAlive)
                    {
                        thPadStartup = new Thread(delegate() { Algorithms.Startup_ShutDown(HopperID, HopperName, checkPoint); });
                        thPadStartup.Name = "AlgSSHopper_" + DateTime.Now.ToString("HHmmss") + DateTime.Now.Millisecond.ToString();
                        thPadStartup.IsBackground = true;
                        thPadStartup.Start();
                    }
                }
                else
                {
                    thPadStartup = new Thread(delegate() { Algorithms.Startup_ShutDown(HopperID, HopperName, checkPoint); });
                    thPadStartup.Name = "AlgSSHopper_" + DateTime.Now.ToString("HHmmss") + DateTime.Now.Millisecond.ToString();
                    thPadStartup.IsBackground = true;
                    thPadStartup.Start();
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in Startup_ShutDownPad in Hopper class for {0} pad at {1} location. Thread {2}", HopperID, checkPoint.ToString(),
                    Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }



        // public static void Call

        public static void TruckAddedInQueue()
        {

        }

        public static void TruckRemovedFromQueue()
        {
        }

        public static void CheckMaxQueueSize()
        {
            try
            {
                lock (lockQueueLength)
                {
                    int totalQueueSize = NoofTrucksInQueueArea + NoofTrucksDirectedtoQueue;
                    if (totalQueueSize >= ExpectedValues.TruckQueueMax)
                    {
                        if (!maxQueueReported)
                        {
                            maxQueueReported = true;
                            WaitCallback updateAlert = new WaitCallback(UpdateAlerts);
                            ArrayList lstParameters = new ArrayList();

                            lstParameters.Add("Queue Size exceeded.");
                            lstParameters.Add(1);
                            lstParameters.Add(false);

                            ThreadPool.QueueUserWorkItem(updateAlert, lstParameters);
                        }

                    }
                    else if (maxQueueReported == true)
                    {
                        maxQueueReported = false;
                        WaitCallback updateAlert = new WaitCallback(UpdateAlerts);
                        ArrayList lstParameters = new ArrayList();

                        lstParameters.Add("");
                        lstParameters.Add(1);
                        lstParameters.Add(true);

                        ThreadPool.QueueUserWorkItem(updateAlert, lstParameters);
                    }
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in CheckMaxQSize method H. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        static void UpdateAlerts(object state)
        {
            try
            {
                ArrayList lstParameters = (ArrayList)state;
                DBConnector.Common.DBConnectorUtility.UpdateAlerts(lstParameters[0].ToString(), Convert.ToInt32(lstParameters[1]), Convert.ToBoolean(lstParameters[2]));
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in UpdateAlerts. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }

        }

        private static int CompareTruckArrivalAtREDS(ITags x, ITags y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're
                    // equal. 
                    return 0;
                }
                else
                {
                    // If x is null and y is not null, y
                    // is greater. 
                    return -1;
                }
            }
            else
            {
                // If x is not null...
                //
                if (y == null)
                // ...and y is null, x is greater.
                {
                    return 1;
                }
                else
                {

                    if (x.LastReadTime == y.LastReadTime)
                    {
                        return 0;
                    }
                    else if (x.LastReadTime > y.LastReadTime)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }

                }
            }
        }



    }
}
