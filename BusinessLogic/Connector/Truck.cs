using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.BusinessLogic.Common;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.BusinessLogicConnector;
using Ramp.MiddlewareController.Connector.DBConnector;
using DBAdapter = Ramp.DBConnector.Adapter;
using System.Threading;
using NLog;

namespace Ramp.BusinessLogic.Connector
{
    public class Truck : ITruck
    {

        //public Int64 TagID { get; set; }
        //public string TagRFID { get; set; }
        //public string TruckID { get; set; }
        //public int HopperID { get; set; }

#if DEBUG
        private static Logger log = LogManager.GetLogger("Ramp.BusinessLogic.Connector.Truck");
#endif

        public ITags TagDB { get; set; }
        public Location LastPositiion { get; set; }
        public string statusMessage { get; set; }
        public System.Timers.Timer tmrTransaction { get; private set; }

        public System.Timers.Timer tmrCheckResponseTime { get; set; }

        public RFIDTag TagRFID { get; set; }

        public Location EntryOrQueue { get; set; }

        public int FoundAtHopperID { get; set; }

        public DateTime LastReadTime { get; set; }

        public string TransMessage { get; set; }
        public int TransStatus { get; set; }

        public int End_Transaction { get; private set; }

        public int iTruckCallupCount { get; set; }
        public bool CalledUp { get; set; }

        Thread thGateQueue;

        public Truck()
        {
            CalledUp = false;
        }

        public Truck(bool testing, string TagRFID, int loc)
        {
            TagDB = DBAdapter.Tags.GetTagDetailsByTagID(TagRFID);
            CalledUp = false;
            if (TagDB == null)
            {
                statusMessage = "Tag not found in Database.";
            }
            else
            {
            }

            LastPositiion = (Location)loc;
            FoundAtHopperID = 0;
            //TagDB.DestinationPad = 0;
            iTruckCallupCount = 0;
        }

        public Truck(string tagID, Location detectAt)
        {
            CalledUp = false;
            TagDB = DBAdapter.Tags.GetTagDetailsByTagID(tagID);
            if (TagDB == null)
            {
                statusMessage = "Tag not found in Database.";
            }
            else
            {
            }

            LastPositiion = detectAt;
            FoundAtHopperID = 0;
            //TagDB.DestinationPad = 0;

            iTruckCallupCount = 0;
        }

        public Truck(RFIDTag tagRFID, Location detectAt)
        {
            TagRFID = tagRFID;
            CalledUp = false;

            //TagDB = DBAdapter.Tags.GetTagDetailsByTagID(tagRFID.TagID);

            //if (TagDB == null)
            //{
            //    statusMessage = "Tag not found in Database.";
            //}
            //else
            //{
            //}

            LastPositiion = detectAt;
            TransMessage = TransactionMessages._TransactionInComplete;
            TransStatus = Convert.ToInt32(TransactionStatus.Incomplete);
            FoundAtHopperID = 0;

            iTruckCallupCount = 0;
            EntryOrQueue = Location.UnKnown;
            InitialiseResTimer((ExpectedValues.TruckCallResponse - 1) * 1000);

        }

        void InitialiseResTimer(double interval)
        {
            tmrCheckResponseTime = new System.Timers.Timer();
            tmrCheckResponseTime.Elapsed += new System.Timers.ElapsedEventHandler(tmrCheckResponseTime_Elapsed);
            if (interval < 1)
                interval = 1;
            tmrCheckResponseTime.Interval = interval;
        }

        void tmrCheckResponseTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StopResTimer();
#if DEBUG
            log.Debug("Entered into tmrCheckResTimer elapsed method for {0} tag. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
             string hopperNo ="";
             Hopper objHopper;
            try
            {
               
                if (LastPositiion == Location.InboundEntry)
                {
                    // skip non responding truck;
                    
                      hopperNo = TagDB.DestinationPad.ToString();//.AssignedHopper.HopperName;

                    if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                    {
                        objHopper = BLUtility.HoppersInFacility[hopperNo];

                        if (objHopper != null)
                        {
                            objHopper.SkipNonResponding(this.TagDB);
                        }
                    }
                }
                else if ((LastPositiion == Location.REDS && EntryOrQueue == Location.ENTRY1) || LastPositiion == Location.EntryLane1 ||
                    LastPositiion == Location.EntryLane2 || LastPositiion == Location.ENTRY1 || LastPositiion == Location.ENTRY2 )
                {
                    hopperNo = TagDB.DestinationPad.ToString();
                    
                    if (BLUtility.HoppersInFacility.ContainsKey(hopperNo) && (EntryOrQueue != Location.QUEUE || CalledUp ||
                        BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[TagDB.HopperID.ToString()].GroupID).Count == 1))
                    {
                       if(LastPositiion == Location.REDS)
                       {
                           if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == this.TagDB.DestinationPad && x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1 && x.Value.LastReadTime > this.LastReadTime))
                               return;
                       }
                       else if (LastPositiion == Location.EntryLane1 || LastPositiion == Location.EntryLane2 || LastPositiion == Location.ENTRY1)
                       {
                           if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == this.TagDB.DestinationPad && ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                (x.Value.LastPositiion == Location.ENTRY1 && x.Value.LastReadTime > this.LastReadTime))))
                           {
                               return;
                           }
                       }
                       else if (LastPositiion == Location.ENTRY2)
                       {
                           if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == this.TagDB.DestinationPad & ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                (x.Value.LastPositiion == Location.ENTRY1 ) || (x.Value.LastPositiion  == Location.ENTRY2 && x.Value.LastReadTime > this.LastReadTime))))
                           {
                               return;
                           }

                       }

#if DEBUG
                       log.Debug("Pausing timer for {0} truck for {2} H. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString(),hopperNo);
#endif
                        BLUtility.HoppersInFacility[hopperNo].IsTimerPaused = true;

                        BLUtility.HoppersInFacility[hopperNo].StopTimer();
                    }
                }
                //else if (LastPositiion == Location.HOPPER)
                //{
                //    //Date: 26-3-2012 10:18 Bhavesh
                //    //Issue: trucks last location stays to Hopper after unload time causing issue when a new truck arrives at the pad. System finds multiple trucks at the pad even though first trucks has finished unloading and has left pad
                //    //Fix: Timer will set for Unloading time and then will change the last location to InboundExit  
                //    //disabled on 26-5-2012 all trucks will be right truck if their assigned pad is current pad
                //    //LastPositiion = Location.InboundExit;
                //}


            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in tmrCheckResTimer elapsed method in Truck class for {0} tag. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
                
            }
        }

        public void SkipNonRespondingTruck()
        {
            bool istransclosed = false;
            Algorithms.SkipNonRespondingTruck(TagDB.TagRFID, ref istransclosed);
        }


        public void GateQueue()
        {
            // thGateQueue = new Thread(delegate() { Algorithms.GateQueue(TagDB.TagRFID, TagDB.HopperID, 0,TagDB.TruckID.ToString()); });
            thGateQueue = new Thread(delegate() { Algorithms.GateQueue(this, 0); });
            thGateQueue.Name = "TruG_Q_" + DateTime.Now.ToString("HHmmss") + DateTime.Now.Millisecond.ToString();
#if DEBUG
            log.Debug("Calling G-Q on new thread {0} from Truck class for {1} tag. CurrentThread {2}", thGateQueue.ManagedThreadId.ToString(), TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            thGateQueue.Start();
        }

        void IncomingTruck() // only to be called when detected at REDS
        {
            // perform Gate/Queue Logic 
            // Send signal to VMS as per output from Gate/Queue Logic             
        }

        public int UpdateTransaction()
        {
            try
            {
                return DBAdapter.ActiveReads.UpdateTransaction(TagDB.ID, TagDB.TagRFID, LastPositiion);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void InitializeTimer()
        {
            if (tmrTransaction != null)
            {
                tmrTransaction.Stop();
                tmrTransaction.Close();
                tmrTransaction = null;
            }


            tmrTransaction = new System.Timers.Timer();
            tmrTransaction.Elapsed += new System.Timers.ElapsedEventHandler(tmrTransaction_Elapsed);
            tmrTransaction.AutoReset = false;


        }

        public void StartResTimer(double interval)
        {
            if (tmrCheckResponseTime == null)
                InitialiseResTimer(0);
            tmrCheckResponseTime.Stop();
            if (interval < 1)
                interval = 1;
            tmrCheckResponseTime.Interval = interval;
            tmrCheckResponseTime.Start();
        }

        public void StopResTimer()
        {
            if (tmrCheckResponseTime != null)
                tmrCheckResponseTime.Stop();
        }

        public void SetResTimer(int interval)
        {
            if (tmrCheckResponseTime == null)
                InitialiseResTimer(0);
            tmrCheckResponseTime.Stop();
            if (interval < 1)
                interval = 1;
            tmrCheckResponseTime.Interval = interval;
        }

        void tmrTransaction_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (LastPositiion == Location.REDS && EntryOrQueue == Location.QUEUE)
                {
                    Algorithms.TrafficTransactionOpenClose(TagRFID, Location.QUEUE);
                }
                else
                {
                    Algorithms.TransactionTimeOut(TagRFID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in tmrTransaction elapsed method for {0} truck. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

        public void SetTimer(int interval)
        {
            if (tmrTransaction == null)
            {
                tmrTransaction = new System.Timers.Timer();
                tmrTransaction.Elapsed += new System.Timers.ElapsedEventHandler(tmrTransaction_Elapsed);
                tmrTransaction.AutoReset = false;
            }

            tmrTransaction.Stop();

            if (interval <= 0)
                interval = 1;

            tmrTransaction.Interval = interval;
        }
        public void StartTimer(double interval)
        {
            if (tmrTransaction == null)
                InitializeTimer();
            tmrTransaction.Stop();

            if (interval <= 0)
                interval = 1;

            tmrTransaction.Interval = interval;
            tmrTransaction.Start();
        }


    }
}
