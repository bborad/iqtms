using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Ramp.MiddlewareController.Common;
using DBUtility = Ramp.DBConnector.Common;
using ReaderModule = Ramp.RFIDHWConnector.Common;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using DBModule = Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Connector.BusinessLogicConnector;
using Ramp.BusinessLogic.Connector;
using Ramp.MiddlewareController.Connector.VMSHardWareConnector;
using VMSModule = Ramp.VMSHWConnector.Adapter;
using System.Collections;
using Ramp.ModbusConnector;
using System.Configuration;
using System.Threading;
using System.Diagnostics;

using NLog;

namespace Ramp.BusinessLogic.Common
{
    public class BLUtility
    {
        private static Logger log = LogManager.GetLogger("Ramp.BusinessLogic.Common.BLUtility");

        public static Ramp.ModbusConnector.ModbusServer ModbusProcessor;

        static object modbusLock = new object();

        public static object VMSListLock = new object();

        public static bool LogFile { get; set; }

        static DataTable dtHopperStatus;

        public static object hopperAreaLock = new object();

        public static bool IsModbusConnected;

        public static int TagNewMarkerInterval;

        public static bool DisplayPadNo;
        public static string HGroupOrder = "";
        public static bool FinerLogging;

        public static int MultipleTagFilterInterval;

        public static Dictionary<ConfigurationValues, object> ConfigValues = new Dictionary<ConfigurationValues, object>();

        static Dictionary<Location, List<IReader>> lstAllRFIDReaders = new Dictionary<Location, List<IReader>>();

        public static Dictionary<Location, List<IReader>> RFIDAdapterReaders
        {
            get
            {
                return lstAllRFIDReaders;
            }
        }

        static Dictionary<string, ITruck> lstTrucksInTransaction = new Dictionary<string, ITruck>();

        public static Dictionary<string, ITruck> TrucksInTransaction
        {
            get
            {
                return lstTrucksInTransaction;
            }

        }

        static Dictionary<string, Hopper> lstHoppersInFacility = new Dictionary<string, Hopper>();

        public static Dictionary<string, Hopper> HoppersInFacility
        {
            get
            {
                return lstHoppersInFacility;
            }
        }

        static Dictionary<Location, List<IMarkers>> lstAllMarkers = new Dictionary<Location, List<IMarkers>>();

        public static Dictionary<Location, List<IMarkers>> Markers
        {
            get
            {
                return lstAllMarkers;
            }
        }

        static Dictionary<Location, List<IVMSDevice>> lstAllVMSDevice = new Dictionary<Location, List<IVMSDevice>>();

        public static Dictionary<Location, List<IVMSDevice>> VMSDeviceList
        {
            get
            {
                return lstAllVMSDevice;
            }
        }

        public static object truckListLock = new object();

        public static bool IsTruckArrived(Location loc, string TagRFID)
        {
            bool flagResult = false;
            try
            {

                ITruck objTruck = null;

                if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID))
                {
                    objTruck = BLUtility.TrucksInTransaction[TagRFID];

                    if (objTruck.LastPositiion == loc)
                    {
                        flagResult = true;
                    }

                }
                else
                {
                    flagResult = true; // Not exist in Truck List (may be transaction is Closed).

                    //flagResult = DBUtility.DBConnectorUtility.IsTruckArrived(loc, TagRFID); Check Last position of Truck in database 
                }

            }
            catch (Exception ex)
            {
                // throw ex;
            }

            return flagResult;

        }

        public static bool IsTruckArrivedFromLocation(Location loc, string TagRFID)
        {
            bool flagResult = false;
            try
            {

                ITruck objTruck = null;

                if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID))
                {
                    objTruck = BLUtility.TrucksInTransaction[TagRFID];

                    if (loc == Location.ENTRY1)
                    {
                        if (objTruck.LastPositiion == loc || objTruck.LastPositiion == Location.ENTRY2 || objTruck.LastPositiion == Location.HOPPER || objTruck.LastPositiion == Location.TRUCKWASH || objTruck.LastPositiion == Location.EXITGATE)
                        {
                            flagResult = true;
                        }
                    }
                    else if (loc == Location.ENTRY2)
                    {
                        if (objTruck.LastPositiion == loc || objTruck.LastPositiion == Location.HOPPER || objTruck.LastPositiion == Location.TRUCKWASH || objTruck.LastPositiion == Location.EXITGATE)
                        {
                            flagResult = true;
                        }
                    }
                    else if (loc == Location.HOPPER)
                    {
                        if (objTruck.LastPositiion == loc || objTruck.LastPositiion == Location.TRUCKWASH || objTruck.LastPositiion == Location.EXITGATE)
                        {
                            flagResult = true;
                        }
                    }

                }
                else
                {
                    flagResult = true; // Not exist in Truck List (may be transaction is Closed).

                    //flagResult = DBUtility.DBConnectorUtility.IsTruckArrived(loc, TagRFID); Check Last position of Truck in database 
                }

            }
            catch (Exception ex)
            {
                // throw ex;
            }

            return flagResult;

        }

        public static bool IsTruckAtLocation(Location loc, string TagRFID)
        {
            //bool flagResult = false;
            //try
            //{
            //    // Perform a tag read operation to search TagRFID at loc
            //    // Call appropriate method from RFIDHardware Lib
            //    flagResult = ReaderModule.RFIDConnectorUtility.IsTruckAtLocation(loc, TagRFID); 


            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //return flagResult;

            bool flagResult = false;
            try
            {

                List<IReader> lstReaders = new List<IReader>();

                if (BLUtility.RFIDAdapterReaders.ContainsKey(loc))
                {
                    lstReaders = BLUtility.RFIDAdapterReaders[loc];
                }
                else
                {
                    lstReaders = GetReadersAtLocation(loc);
                    BLUtility.RFIDAdapterReaders.Add(loc, lstReaders);
                }
                // Perform a tag read operation, BY READERS, to search TagRFID AT loc

                foreach (IReader objReader in lstReaders)
                {
                    if (objReader.SearchTag(TagRFID))
                    {
                        flagResult = true;
                        objReader.Dispose();
                        break;
                    }

                    objReader.Dispose();

                }

            }
            catch (Exception ex)
            {
                // throw ex;
            }

            return flagResult;

        }

        public static int iQueueSize;

        public static int iTruckCallupRetries;

        public static void ReadNStoreAllReaders()
        {
            //DataTable dtReaders = new DataTable("Readers");
            //DataColumn[] dcc = new DataColumn[]{ new DataColumn("IpAddress",typeof(System.String)),
            //                                      new DataColumn("PortNo",typeof(System.Int32)),
            //                                       new DataColumn("Power",typeof(System.Int32))
            //                                    };

            // dtReaders.Columns.AddRange(dcc);

            List<IReaders> lstDBReaders = DBModule.Readers.GetAllReaders();

            IReader obj = null;

            List<IReader> lstRFIDReaders;

            Location key;

           // DataRow drReader = null;

            foreach (IReaders dbReader in lstDBReaders)
            {
                if (dbReader.ID <= 0)
                    continue;

                //drReader = dtReaders.NewRow();

                //drReader["IpAddress"] = dbReader.IPAddress.Trim();

                //drReader["PortNo"] = dbReader.PortNo;

                //drReader["Power"] = dbReader.Power;

                //dtReaders.Rows.Add(drReader);

                obj = DBConnectorToRFIDReader(dbReader);
                key = (Location)obj.Location;
                if (lstAllRFIDReaders.ContainsKey(key))
                {
                    lstRFIDReaders = lstAllRFIDReaders[key];
                }
                else
                {
                    lstRFIDReaders = new List<IReader>();
                    lstAllRFIDReaders.Add(key, lstRFIDReaders);
                }

                if (!lstRFIDReaders.Contains(obj))
                {
                    lstRFIDReaders.Add(obj);
                }

                lstAllRFIDReaders[key] = lstRFIDReaders;
            }
            // dtReaders.AcceptChanges();

            //    Ramp.RFIDHWConnector.Adapters.Reader.ConfigureReaders(dtReaders);

        }

        public static IReader DBConnectorToRFIDReader(IReaders objreader)
        {
            IReader obj;
            try
            {
                obj = new RFIDHWConnector.Adapters.Reader();
                obj.ReaderName = objreader.ReaderName;
                obj.ReaderSerialNo = objreader.ReaderSerialNo;
                obj.IPAddress = objreader.IPAddress;
                obj.PortNo = objreader.PortNo;
                obj.Location = objreader.Location;
                obj.TxPower = objreader.Power;
            }
            catch (Exception)
            {
                throw;
            }
            return obj;
        }

        public static int GetMaxValue(params int[] values)
        {
            int maxValue = 0;

            foreach (int value in values)
            {
                if (value > maxValue)
                    maxValue = value;
            }

            return maxValue;
        }

        public static void DisposeReaders()
        {
            try
            {
                foreach (Location loc in RFIDAdapterReaders.Keys)
                {
                    RFIDAdapterReaders[loc].Clear();
                }

                RFIDAdapterReaders.Clear();
            }
            catch (Exception ex)
            {
            }
        }

        public static void GetAllHoppersInFacility()
        {
            Hopper objHopper = null;
            try
            {
                DataTable dtHoppers = DBModule.Hoppers.GetAllHoppers();
                if (dtHoppers != null && dtHoppers.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtHoppers.Rows)
                    {
                        if (Convert.ToInt32(dr["ID"]) != 0)
                        {
                            objHopper = new Hopper();

                            objHopper.HopperID = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : -1;
                            objHopper.HopperName = dr["HopperName"] != DBNull.Value ? dr["HopperName"].ToString() : "";
                            objHopper.Ent2_Hopper = dr["TimeFromEnt2"] != DBNull.Value ? Convert.ToInt32(dr["TimeFromEnt2"]) : 10;
                            objHopper.Hopper_TW = dr["TimeToTW"] != DBNull.Value ? Convert.ToInt32(dr["TimeToTW"]) : 10;
                            objHopper.MarkerID = dr["MarkerID"] != DBNull.Value ? Convert.ToInt32(dr["MarkerID"]) : 0;
                            objHopper.GroupID = dr["GroupID"] != DBNull.Value ? Convert.ToInt32(dr["GroupID"]) : 0;
                            objHopper.GroupOrder = dr["GroupOrder"] != DBNull.Value ? Convert.ToInt32(dr["GroupOrder"]) : 0;
                            if (!lstHoppersInFacility.ContainsKey(objHopper.HopperName))
                                lstHoppersInFacility.Add(objHopper.HopperName, objHopper);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void GetAllMarkers()
        {
            try
            {
                List<IMarkers> listByLoc = null;
                List<IMarkers> objMarkersList = DBModule.Markers.GetAllMarkers();

                Location key;

                if (objMarkersList != null && objMarkersList.Count > 0)
                {

                    foreach (IMarkers objMrk in objMarkersList)
                    {
                        key = (Location)objMrk.Location;
                        if (lstAllMarkers.ContainsKey(key))
                        {
                            listByLoc = lstAllMarkers[key];
                        }
                        else
                        {
                            listByLoc = new List<IMarkers>();
                        }

                        if (!listByLoc.Contains(objMrk))
                        {
                            listByLoc.Add(objMrk);
                            lstAllMarkers[key] = listByLoc;
                        }


                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        public static List<IReader> GetReadersAtLocation(Location loc)
        {
            List<IReader> lst_Readers = null;
            IReader obj;
            try
            {
                List<IReaders> lstReaders;

                lstReaders = DBModule.Readers.GetReadersAtLocation(loc);

                if (lstReaders != null && lstReaders.Count > 0)
                {
                    lst_Readers = new List<IReader>();
                    foreach (IReaders objreader in lstReaders)
                    {
                        obj = DBConnectorToRFIDReader(objreader);
                        lst_Readers.Add(obj);
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
            return lst_Readers;

        }

        public static void StartAllVMS()
        {
            foreach (List<IVMSDevice> vmslist in VMSDeviceList.Values)
            {
                foreach (IVMSDevice vms in vmslist)
                    vms.DisplayDefaultMessage();
            }

        }

        public static void GetAllVMSDevices()
        {
            List<IVMSDevices> lstVMSDevicesDB = null;
            List<IVMSDevice> lstVMSDevice = null;

            IVMSDevice obj;

            Location key;

            try
            {
                lstVMSDevicesDB = DBModule.VMSDevices.GetAllVMSDevices();

                // added on 09-04-2011, to get the VMS details 
                VMSDeviceList.Clear();

                if (lstVMSDevicesDB != null && lstVMSDevicesDB.Count > 0)
                {

                    foreach (IVMSDevices objVMsDevice in lstVMSDevicesDB)
                    {

                        if (objVMsDevice.SCL2008)
                            obj = new VMSModule.VMSDevice();
                        else
                            obj = new VMSModule.AussportVMS();
                        

                        obj.DeviceNo = objVMsDevice.DeviceNo;
                        obj.IPAddress = objVMsDevice.IPAddress;
                        obj.Password = objVMsDevice.Password;
                        obj.UDPPort = objVMsDevice.UDPPort;
                        obj.SCL2008 = objVMsDevice.SCL2008;
                        obj.LedHeight = objVMsDevice.LedHeight;
                        obj.LedWidth = objVMsDevice.LedWidth;
                        obj.Location = objVMsDevice.Location;

                        obj.AscFont = Convert.ToInt16(objVMsDevice.ASCFont);
                        obj.TextColor = Convert.ToInt16(objVMsDevice.TextColor);
                        obj.TextStartPosition = Convert.ToInt16(objVMsDevice.TextStartPosition);
                        obj.TextLength = Convert.ToInt16(objVMsDevice.TextLength);

                        obj.MessageTimeOut = objVMsDevice.MessageTimeOut;

                        obj.DefaultMessage = objVMsDevice.DefaultMessage;

                        //if(!string.IsNullOrEmpty(obj.DefaultMessage))
                        //    VMSHWConnector.Adapter.AussportVMS.DefaultMessage = obj.DefaultMessage;

                        obj.VMSInit();
                        key = (Location)obj.Location;

                        if (VMSDeviceList.ContainsKey(key))
                        {
                            lstVMSDevice = VMSDeviceList[key];
                        }
                        else
                        {
                            lstVMSDevice = new List<IVMSDevice>();
                            VMSDeviceList.Add(key, lstVMSDevice);
                        }

                        lstVMSDevice.Add(obj);

                        VMSDeviceList[key] = lstVMSDevice;

                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Exception thrown while getting and storing all VMSs in VMSList. :", ex.Message);    
            }

            try
            {
                
                StartAllVMS();
            }
            catch { }


        }

        public static void GetConfigurationValues()
        {
            try
            {
                DataSet ds = DBUtility.DBConnectorUtility.GetConfigurationValues();

                int ID = 0;

                ConfigurationValues key;

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ID = Convert.ToInt32(dr["ID"]);
                            key = (ConfigurationValues)ID;

                            try
                            {
                                if (ConfigValues.ContainsKey(key))
                                {
                                    ConfigValues[key] = Convert.ToInt32(dr["Value"]);
                                }
                                else
                                {
                                    ConfigValues.Add(key, Convert.ToInt32(dr["Value"]));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
            }

            try
            {
                TagNewMarkerInterval = 60;
                MultipleTagFilterInterval = 60;
                string value = ConfigurationManager.AppSettings["TagNewMarkerInterval"];

                if (value != null && value.Length > 0)
                {
                    TagNewMarkerInterval = Convert.ToInt32(value);
                }

                value = ConfigurationManager.AppSettings["MultipleTagFilterInterval"];

                if (value != null && value.Length > 0)
                {
                    MultipleTagFilterInterval = Convert.ToInt32(value);
                }

                value = ConfigurationManager.AppSettings["LogFile"];

                if (value != null && value.Length > 0)
                {
                    if (value == "1")
                        LogFile = true;
                    else
                        LogFile = false;
                }

                value = ConfigurationManager.AppSettings["DisplayPadNo"];

                if (value != null && value.Length > 0)
                {
                    if (value.Trim().ToLower() == "true")
                        DisplayPadNo = true;
                    else
                        DisplayPadNo = false;
                }

                value = ConfigurationManager.AppSettings["FinerLogging"];

                if (value != null && value.Length > 0)
                {
                    if (value.Trim().ToLower() == "true")
                        FinerLogging = true;
                    else
                        FinerLogging = false;
                }

                 


                if (!EventLog.Exists("IQTMSLog"))
                {
                    if (!EventLog.SourceExists("IQTMSSource"))
                        EventLog.CreateEventSource("IQTMSSource", "IQTMSLog");
                }

            }
            catch (Exception ex)
            {
            }


        }

        public static void StartModbusServer()
        {

            try
            {
                lock (modbusLock)
                {
                    ModbusProcessor = new Ramp.ModbusConnector.ModbusServer();
                    // ModbusProcessor.ReadPAD_Start(1);
                    IsModbusConnected = true;
                    SetPadMatchValue_AllPAD();


                }
            }
            catch (Exception ex)
            {
            }

        }

        public static void DisconnectVMSs()
        {
            foreach (List<IVMSDevice> vmslist in VMSDeviceList.Values)
            {
                foreach (IVMSDevice vms in vmslist)
                     vms.Disconnect();
                    
            }

        }

        public static void StopModbusServer()
        {

            try
            {

                if (IsModbusConnected == true)
                {
                    ModbusProcessor.StopModbusServer();
                    IsModbusConnected = false;
                }

            }
            catch (Exception ex)
            {
            }

        }

        public static void SetPadMatchValue_AllPAD()
        {
            try
            {
                for (byte i = 1; i <= 13; i++)
                {
                    ModbusProcessor.WritePAD_Match(i, 1);
                }

            }
            catch (Exception ex)
            {
            }
        }

        public static void UpdateHopperStatus()
        {
#if DEBUG
            log.Debug("In UpdateHopperStatus: Entered intot he UpdateHopperStatus. Thread {0}",
                Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            if (ModbusProcessor != null && IsModbusConnected == true)
            {

                try
                {
#if DEBUG
                    log.Debug("In UpdateHopperStatus: Getting lock on modbusLock. Thread {0}",
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    lock (modbusLock)
                    {
                     //   bool reassignAllPad = false;
#if DEBUG
                        log.Debug("In UpdateHopperStatus:Locked modbusLcok. Thread {0}",
                            Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    string PadStatuses = "";
                    //get HopperAvailable of all hoppers in list into a array to compare with actual status
                    bool[] pads = BLUtility.HoppersInFacility.OrderBy(x=> x.Value.HopperID).Select(z => z.Value.HopperAvailable).ToArray();
                    
                    byte thePAD = 0;

                    //CreateDataTableHopperStatus();

                    //dtHopperStatus.Rows.Clear();

                    //short[] resultAllPad = new short[13];

                    //DataRow dr;
                    
                    short currentStatus = 0;
                    for (int i = 0; i < 13; i++)
                    {
                       // dr = dtHopperStatus.NewRow();
                        thePAD = Convert.ToByte(i + 1);

                        //dr["ID"] = i + 1;

                            currentStatus = ModbusProcessor.ReadPAD_Status(thePAD);
                        
                        PadStatuses += currentStatus.ToString() + ",";

                        //dr["HopperStatus"] = Convert.ToInt32(resultAllPad[i]);
                        //dtHopperStatus.Rows.Add(dr);
                        if (currentStatus == 0)
                        {
                            try
                            {
                                BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == thePAD).RemoveMessage("CLOSED", "`CFF0000");
                            }
                            catch { }
                        }

                        if ((pads[i] && currentStatus != 0) || (!pads[i] && currentStatus == 0))
                        {
                            //pad is available now start timer
                          
                            if (!pads[i] && currentStatus == 0)
                            {
                                //reassignAllPad = true;
                                if (BLUtility.HoppersInFacility.ContainsKey((i + 1).ToString()))
                                {
#if DEBUG
                                    log.Debug("In UpdateHopperStatus: Calling Startup_ShutDownPad for {0} pad. Thread {1}",i+1,
                                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                                    BLUtility.HoppersInFacility[(i + 1).ToString()].Startup_ShutDownPAD(Location.HOPPER);
                                    //int calltimer = 0;
                                    ////DBConnector.Adapter.ActiveReads.ReassignAllPad(BLUtility.HoppersInFacility[(i + 1).ToString()].GroupID, "");
 
                                    //DBConnector.Adapter.ActiveReads.GetNextCallupTime(i + 1, out calltimer);
                                    //BLUtility.HoppersInFacility[(i + 1).ToString()].StartTimer(calltimer * 1000);

                                   

//#if DEBUG
//                                    log.Info("Starting callup timer with {0} ms for {1} pad", calltimer * 1000, i + 1);
//#endif
                                }
                               

                            } 
                            else if(currentStatus != 0)
                            {
                                try
                                {
                                    BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == thePAD).ShowString("CLOSED", "`CFF0000", 0);
                                }
                                catch { }
                                if (BLUtility.HoppersInFacility[(i + 1).ToString()].tmrQueue == null || BLUtility.HoppersInFacility[(i + 1).ToString()].tmrQueue.Interval > 2000)
                                    BLUtility.HoppersInFacility[(i + 1).ToString()].StartTimer(2000);
                            }
#if DEBUG
                            log.Debug("In UpdateHopperStatus: Updating currecnt HopperAvailable state to {0} for {1} pad. Thread {2}",(currentStatus == 0).ToString(),i+1,
                                Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                            BLUtility.HoppersInFacility[(i + 1).ToString()].HopperAvailable = (currentStatus == 0);

                        }

                    }

                    // dtHopperStatus.AcceptChanges();
#if DEBUG
                    log.Debug("In UpdateHopperStatus: Calling DB - UpdateHopperStatus with {0} string. Thread {1}", PadStatuses,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    DBModule.Hoppers.UpdateHoppersStatus(PadStatuses);
#if DEBUG
                    log.Debug("In UpdateHopperStatus: Finished DB - UpdateHopperStatus with {0} string. Thread {1}", PadStatuses,
                        Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                    }
                }
                catch(Exception ex)
                {
#if DEBUG
                    log.Error("Exception thrown while updating HopperStatus. Error"+ex.Message);
#endif

                }
            }
        }

        static void CreateDataTableHopperStatus()
        {
            if (dtHopperStatus == null)
            {
                dtHopperStatus = new DataTable();

                DataColumn[] dcc = new DataColumn[] { new DataColumn("ID",typeof(System.Int32)),
                                                      new DataColumn("HopperStatus",typeof(System.Int32))
                                                    };

                dtHopperStatus.Columns.AddRange(dcc);
                dtHopperStatus.AcceptChanges();
            }



        }

        public static int GetActiveTrucksInFacility(int forAllDays) // forAllDays = 1 and onlyForToday = 0
        {
            int activeTruckCount = 0;
            try
            {

                ITruck objTruck;
                ITags objTagDB;

                RFIDTag TagRFID;


                IHoppers assignedHopper = null;

                DataTable dtActiveReads = DBModule.ActiveReads.GetActiveTransactions(forAllDays);

                if (dtActiveReads != null && dtActiveReads.Rows.Count > 0)
                {
                    //activeTruckCount = dtActiveReads.Rows.Count;

                    foreach (DataRow dr in dtActiveReads.Rows)
                    {

                        try
                        {
                            TagRFID = new RFIDTag();

                            TagRFID.TagID = dr["TagRFID"].ToString();
                            TagRFID.NewMarkerID = Convert.ToInt32(dr["NewLoopID"]);
                            TagRFID.OldMarkerID = Convert.ToInt32(dr["OldLoopID"]);

                            objTagDB = new DBModule.Tags();
                            objTagDB.ID = Convert.ToInt32(dr["TagID"]);
                            objTagDB.HopperID = Convert.ToInt32(dr["HopperID"]);
                            objTagDB.ProponentID = Convert.ToInt32(dr["ProponentID"]);
                            objTagDB.TruckID = dr["TruckID"].ToString();
                            objTagDB.TagRFID = dr["TagRFID"].ToString();
                            objTagDB.Assigned = Convert.ToBoolean(dr["Assigned"]);
                            objTagDB.DestinationPad = Convert.ToInt32(dr["DestinationPad"]);

                            assignedHopper = new DBModule.Hoppers();
                            assignedHopper.HopperName = dr["HopperName"].ToString();
                            assignedHopper.TimeFromEnt2 = Convert.ToInt32(dr["TimeFromEnt2"]);
                            assignedHopper.TimeToTW = Convert.ToInt32(dr["TimeToTW"]);
                            assignedHopper.MarkerID = Convert.ToInt32(dr["MarkerID"]);

                            objTagDB.AssignedHopper = assignedHopper;

                            Location LastPosition = (Location)Convert.ToInt32(dr["LastPosition"]);

                            objTruck = new Truck(TagRFID, LastPosition);
                            objTruck.EntryOrQueue = (Location)Convert.ToInt32(dr["EntryORQueue"]);
                            objTruck.TagDB = objTagDB;
                            objTruck.TagRFID = TagRFID;

                            objTruck.FoundAtHopperID = dr["FoundAtHopper"] != DBNull.Value ? Convert.ToInt32(dr["FoundAtHopper"]) : 0;
                            //objTruck.TagDB.DestinationPad = objTruck.FoundAtHopperID;

                            if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID.TagID))
                            {
                                BLUtility.TrucksInTransaction[TagRFID.TagID] = objTruck;
                            }
                            else
                            {
                                objTruck.InitializeTimer();
                                BLUtility.TrucksInTransaction.Add(TagRFID.TagID, objTruck);

                                //Algorithms.TrafficTransactionOpenClose(TagRFID, LastPosition);

                                // Changes made on 07-04-2011 ( To set the timeout timer for the trucks at Truckwash/Exitgate, so that transaction can be completed ).
                                if (LastPosition == Location.TRUCKWASH || LastPosition == Location.EXITGATE)
                                {
                                    int delayTimer = ExpectedValues.Max_Delay + BLUtility.GetMaxValue(ExpectedValues.Exit_REDS, ExpectedValues.TruckWash_REDS);
                                    objTruck.TransMessage = TransactionMessages._TransactionComplete;
                                    objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Complete);
                                    objTruck.LastPositiion = LastPosition;
                                    objTruck.SetTimer(delayTimer * 1000);
                                    objTruck.tmrTransaction.Start();

                                }
                                else if (LastPosition == Location.REDS && objTruck.EntryOrQueue == Location.QUEUE)
                                {
                                    int delayTimer = ExpectedValues.REDS_Queue;
                                    objTruck.SetTimer(delayTimer * 1000);
                                    objTruck.tmrTransaction.Start();

                                }
                                else
                                {
                                    Algorithms.TrafficTransactionOpenClose(TagRFID, LastPosition);
                                }


                                activeTruckCount++;
                            }
                        }
                        catch
                        {
                        }

                    }

                    if (activeTruckCount > 0)
                    {
                        var trucksAtHopper = from hTruck in BLUtility.TrucksInTransaction.Values where hTruck.LastPositiion == Location.HOPPER select hTruck;

                        if (trucksAtHopper.Count() > 0)
                        {
                            foreach (ITruck hopTruck in trucksAtHopper)
                            {
                                Algorithms.TruckDetectedAtHopper(hopTruck);
                            }
                        }

                    }


                    foreach (Hopper hPAD in BLUtility.HoppersInFacility.Values)
                    {
                        var truckForHopper = from hopTruck in BLUtility.TrucksInTransaction.Values where hopTruck.TagDB.HopperID == hPAD.HopperID select hopTruck;
                        if (truckForHopper.Count() > 0)
                        {
                            var truckCountAtLocation = from truckAtLocation in truckForHopper where truckAtLocation.LastPositiion == Location.HOPPER select truckAtLocation;
                            if (truckCountAtLocation.Count() > 0)
                            {
                                hPAD.Startup_ShutDownPAD(Location.HOPPER);
                                continue;
                            }

                            var truckCountAtLocation1 = from truckAtLocation1 in truckForHopper where truckAtLocation1.LastPositiion == Location.ENTRY2 select truckAtLocation1;
                            if (truckCountAtLocation1.Count() > 0)
                            {
                                hPAD.Startup_ShutDownPAD(Location.ENTRY2);
                                continue;
                            }

                            hPAD.Startup_ShutDownPAD(Location.ENTRY1);
                        }

                        int calltimer = 0;
                        DBConnector.Adapter.ActiveReads.GetNextCallupTime(hPAD.HopperID, out calltimer);
                        hPAD.StartTimer(calltimer*1000);
#if DEBUG
                        log.Info("Starting timer with {0} ms for {1} pad in GetActiveTrucksInFacility", calltimer * 1000, hPAD.HopperID);
#endif
                    }
                }
            }
            catch (Exception ex)
            {}
                return activeTruckCount;
            
        }

        public static void WriteEventLog(string msg, EventLogEntryType eventType)
        {
            try
            {
                if(FinerLogging)
                 EventLog.WriteEntry("IQTMSSource", msg, eventType);
            }
            catch (Exception ex)
            {
            }
        }

        //protected static IVMSDevice DBVMSDeviceToHwVMSDevice(IVMSDevices objVMSDevice)
        //{
        //   // IVMSDevice obj = new VMSHWConnector.Adapter.VMSDevice(obj.DeviceNo
        //    try
        //    {

        //    }
        //    catch
        //    {
        //    }
        //}

    }


}
