using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
//using Middleware = Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Common;
using DBAdapters = Ramp.DBConnector.Adapter;
using Ramp.RFIDHWConnector.Common;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;
using ReaderModule = Ramp.RFIDHWConnector.Adapters;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;

using Ramp.VMSHWConnector.Adapter;
using Ramp.MiddlewareController.Connector.BusinessLogicConnector;
using Ramp.BusinessLogic.Connector;
using Ramp.MiddlewareController.Connector.VMSHardWareConnector;
using System.Net.NetworkInformation;
using System.Threading;

using NLog;

namespace Ramp.BusinessLogic.Common
{
    public class Algorithms
    {
        static NLog.Logger log = NLog.LogManager.GetLogger("Ramp.BusinessLogic.Common.Algorithms");
       public static string checkPoint = "0";
        static bool truckChanged = false;
        //private delegate void SendMessageToVMS(string[] vmsSettings,string message,bool setTimer);
        //private static SendMessageToVMS newVMS = new SendMessageToVMS(AussportVMS.GetInstance().SendMessageOnTCP);
        public readonly static object CallupLocker = new object();

        #region "SkipNonRespondingTruck"

        //public static float TruckCallResponseTime { get; set; }
        public static bool SkipNonRespondingTruck(string TagRFID, ref bool IsTransactionClosed)
        {
            // Get the value for "TruckCallResponse" from ExpectedTimes Table
            // Now initialize the variable with this value (TruckCallResponse) 

            //  TruckCallResponseTime = ExpectedValues.TruckCallResponse;
            // return CheckTruckStatus(TagRFID);

            bool isTruckSkipped = false;

            try
            {
                ITruck objTruck = null;
                lock (BLUtility.truckListLock)
                {
                    objTruck = BLUtility.TrucksInTransaction[TagRFID];

                }

                if (objTruck != null)
                {
                    objTruck.iTruckCallupCount += 1;

                    if ((int)BLUtility.ConfigValues[ConfigurationValues.CallupRetries] > -1 && objTruck.iTruckCallupCount >= (int)BLUtility.ConfigValues[ConfigurationValues.CallupRetries])
                    {
                        IsTransactionClosed = true;
                        objTruck.TransMessage = TransactionMessages._TruckNotRespondOnCallup;
                        objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Incomplete);

                        Algorithms.TransactionTimeOut(objTruck.TagRFID);

                        return false;
                    }

                    if (BLUtility.VMSDeviceList.ContainsKey(Location.QUEUE))
                    {
                        foreach (IVMSDevice obj in BLUtility.VMSDeviceList[Location.QUEUE])
                            obj.RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " Gate", "`C00FF00");

                    }

                    if (IsTruckStillInQueue(TagRFID))
                    {
                        //SendTruckToQueueBack(TagRFID);

                        isTruckSkipped = true;
                    }

                }


            }
            catch (Exception ex)
            {
            }

            return isTruckSkipped;

        }
        public static void SendTruckToQueueBack(string TagRFID)
        {
            DBAdapters.ActiveReads obj = new Ramp.DBConnector.Adapter.ActiveReads();
            obj.SendTruckToQueueBack(TagRFID);

            // Update the ActiveReads Table and set the "Queue" value to the Currrent Time. (for the current truck only)
            // also, set the "TruckCallUp" as NULL
            // Call appropriate method from Database Lib
        }
        protected static bool CheckTruckStatus(string TagRFID)
        {
            int TruckCallResponseTime = ExpectedValues.TruckCallResponse;
            bool flagArrived = false;

            bool isTruckSkipped = false;

            do
            {
                if ((BLUtility.IsTruckArrived(Location.ENTRY1, TagRFID)))   // pass Entry1
                {
                    // EXIT
                    flagArrived = true;
                    break;
                }

                TruckCallResponseTime--;
                System.Threading.Thread.Sleep(1000);
            }
            while (TruckCallResponseTime > 0);

            if (!flagArrived)
            {
                //Perform a read from Queue
                if (IsTruckStillInQueue(TagRFID))
                {
                    SendTruckToQueueBack(TagRFID);
                    isTruckSkipped = true;
                }
                ITruck objTruck = BLUtility.TrucksInTransaction[TagRFID];
                if (objTruck != null)
                {
                    string formatString = "`C0000FF ";
                    if (BLUtility.VMSDeviceList.ContainsKey(Location.QUEUE))
                    {
                        List<IVMSDevice> lstVMSDevices = BLUtility.VMSDeviceList[Location.QUEUE];

                        if (lstVMSDevices != null && lstVMSDevices.Count > 0)
                        {
                            foreach (IVMSDevice obj in lstVMSDevices)
                            {
                                obj.RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), formatString);
                            }
                        }

                    }

                }

                //else
                //{
                //    // end the transaction for lost truck ( Changes as per document ver 1.4)
                //    RFIDTag tagRFID = new RFIDTag();
                //    tagRFID.TagID = TagRFID;
                //    TransactionTimeOut(tagRFID);
                //}
            }

            return isTruckSkipped;
        }
        public static bool IsTruckStillInQueue(string TagRFID)
        {
            bool flagResult = false;
            try
            {

                // flagResult =  BLUtility.IsTruckAtLocation(Location.QUEUE, TagRFID); // Changed as there is no reader in Queue.

                if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID))
                {
                    if (BLUtility.TrucksInTransaction[TagRFID].LastPositiion == Location.QUEUE)
                    {
                        flagResult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // return false;
            }
            return flagResult;
        }

        #endregion




        #region "MaxQueueSizeCalculation"



        //public static void MaxQueueSizeCalculation(bool IsTruckArrived)
        //{
        //    try
        //    {
        //        if (IsTruckArrived)
        //        {
        //            GlobalValues.CurrentQueueSize += 1;
        //        }
        //        else
        //        {
        //            GlobalValues.CurrentQueueSize -= 1;
        //        }



        //        if (GlobalValues.CurrentQueueSize > ExpectedValues.TruckQueueMax)
        //        {
        //            //PerformReadQueue();

        //            //// Old Logic for READING from ACTIVEREADS
        //            //DBAdapters.ActiveReads obj = new Ramp.DBConnector.Adapter.ActiveReads();
        //            //int count = obj.GetTotalTagsAtLocation(Location.QUEUE);
        //            //GlobalValues.CurrentQueueSize = count;


        //            // New Logic for actual READ using READERS
        //            int count = RFIDConnectorUtility.GetTotalTagsAtLocation(Location.QUEUE);
        //            GlobalValues.CurrentQueueSize = count;


        //            if (GlobalValues.CurrentQueueSize > ExpectedValues.TruckQueueMax)
        //            {
        //                //SendQueueSizeAlert();   //  To be implemented
        //            }
        //        }
        //        else
        //        {
        //            //ClearQueueSizeAlert();    //  To be implemented
        //        }




        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        // Re-impementation
        public static string MaxQueueSizeCalculation()
        {
            try
            {
                Int32 queueSize = 0;
                DBAdapters.ActiveReads objActiveReads = new Ramp.DBConnector.Adapter.ActiveReads();
                queueSize = objActiveReads.GetTotalTagsAtLocation(Location.QUEUE);

                if (queueSize > ExpectedValues.TruckQueueMax)
                {
                    return "Alert"; //SendQueueSizeAlert();   //  To be implemented
                }
                else
                {
                    return "Clear Alert"; //ClearQueueSizeAlert();    //  To be implemented
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion




        #region "HopperPriority"

        public static string HopperPriority()
        {
            try
            {
                string Lane1_LoopID = String.Empty, Lane2_LoopID = String.Empty;
                Boolean flagLane1 = false, flagLane2 = false;

                string Lane1_TagRFID = String.Empty, Lane2_TagRFID = String.Empty;


                // Get marker IDs at both lane from DB
                List<IMarkers> objMarkersList = DBAdapters.Markers.GetMarkersAtLocation(Location.ENTRY1);



                // Get the LOOPID's of Lane 1 & Lane2
                foreach (Markers objMarker in objMarkersList)
                {
                    if (objMarker.MarkerPosition == Convert.ToString(MarkersPosition.ENTRY1Lane1))
                    {
                        Lane1_LoopID = objMarker.MarkerLoopID;
                    }
                    else if (objMarker.MarkerPosition == Convert.ToString(MarkersPosition.ENTRY1Lane2))
                    {
                        Lane2_LoopID = objMarker.MarkerLoopID;
                    }
                }



                // Get
                RFIDTag[] objRFIDTagArray = new RFIDTag[5];     // This Implementation will be replaced by XXXXXXX method.

                foreach (RFIDTag objRFIDTag in objRFIDTagArray)
                {
                    if (objRFIDTag.NewMarkerID.Equals(Lane1_LoopID))
                    {
                        Lane1_TagRFID = objRFIDTag.TagID;
                        flagLane1 = true;
                    }
                    else if (objRFIDTag.NewMarkerID.Equals(Lane2_LoopID))
                    {
                        Lane2_TagRFID = objRFIDTag.TagID;
                        flagLane2 = true;
                    }

                    if (flagLane1 == true && flagLane2 == true)
                    {
                        break;
                    }
                }



                // If Both Lanes are occupied 
                if (flagLane1 == true && flagLane2 == true)
                {

                    // CHECK for Hopper Priority
                    string strPriorityTagRFID = String.Empty;
                    strPriorityTagRFID = DBAdapters.Hoppers.GetPriority(Lane1_TagRFID, Lane2_TagRFID);

                    return strPriorityTagRFID;
                }

                return "";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion




        #region "GateQueue"



        //// At REDS during new truck read
        //public static string GateQueue(string TagRFID)
        //{
        //    try
        //    {
        //        ITags objTagDetails = new Tags();
        //        objTagDetails = Tags.GetTagDetailsByTagID(TagRFID);

        //        Int32 TagID = objTagDetails.ID;
        //        Int32 HopperID = objTagDetails.HopperID;

        //        //  SYNTAX for MODBUS method
        //        //  public Boolean IsHopperOperational(Int32 HopperID)
        //        //  Check is hopper operational?  (MODBUS will provide this information)
        //        //if (IsHopperOperational(HopperID))
        //        if (true)
        //        {
        //            IHoppers objHopper;
        //            objHopper = Hoppers.GetHopperDetailsByHopperID(HopperID);

        //            Int32 E2Hx = objHopper.TimeFromEnt2;
        //            Int32 HxTW = objHopper.TimeToTW;

        //            Int32 TravelTime;
        //            TravelTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx;



        //            Int32 TimeTillFree = 0;
        //            List<IActiveReads> objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);

        //            Int32 deltaTS = 0;
        //            DateTime iCurrentTruckREDS = DateTime.Now;
        //            DateTime iFrontTruckREDS = DateTime.Now;


        //            Int32 countQueue = 0;
        //            Int32 countEntry1 = 0;
        //            Int32 countEntry2 = 0;
        //            Int32 countHopperX = 0;


        //            IEnumerable<IActiveReads> arrREDS = (from obj1 in objActiveReads_Lst where obj1.LastPosition == Convert.ToInt32(Location.REDS) select obj1);
        //            arrREDS.OrderByDescending(obj => obj.ID);

        //            if (arrREDS.Count() > 1)
        //            {
        //                foreach (IActiveReads objReds in arrREDS)
        //                {
        //                    if (objReds.TagID == TagID)
        //                    {
        //                        iCurrentTruckREDS = objReds.REDS;
        //                    }
        //                    else
        //                    {
        //                        iFrontTruckREDS = objReds.REDS;
        //                        break;
        //                    }
        //                }

        //                TimeSpan objSpan = iCurrentTruckREDS.Subtract(iFrontTruckREDS);
        //                deltaTS = objSpan.Seconds;

        //                TimeTillFree = ExpectedValues.REDS_Queue - deltaTS + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx + HxTW;

        //            }
        //            else
        //            {
        //                // QUEUE
        //                IEnumerable<IActiveReads> arrQUEUE = (from obj1 in objActiveReads_Lst where obj1.LastPosition == Convert.ToInt32(Location.QUEUE) select obj1);
        //                if (arrQUEUE.Count() > 0)
        //                {
        //                    countQueue = arrQUEUE.Count();
        //                    TimeTillFree = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx + HxTW;
        //                }
        //                else
        //                {
        //                    // ENTRY1
        //                    IEnumerable<IActiveReads> arrENTRY1 = (from obj1 in objActiveReads_Lst where obj1.LastPosition == Convert.ToInt32(Location.ENTRY1) select obj1);
        //                    if (arrENTRY1.Count() > 0)
        //                    {
        //                        countEntry1 = arrENTRY1.Count();
        //                        TimeTillFree = ExpectedValues.Ent1_Ent2 + E2Hx + HxTW;
        //                    }
        //                    else
        //                    {
        //                        // ENTRY2
        //                        IEnumerable<IActiveReads> arrENTRY2 = (from obj1 in objActiveReads_Lst where obj1.LastPosition == Convert.ToInt32(Location.ENTRY2) select obj1);
        //                        if (arrENTRY2.Count() > 0)
        //                        {
        //                            countEntry2 = arrENTRY2.Count();
        //                            TimeTillFree = E2Hx + HxTW;
        //                        }
        //                        else
        //                        {
        //                            // HOPPERX
        //                            IEnumerable<IActiveReads> arrHOPPERX = (from obj1 in objActiveReads_Lst where obj1.LastPosition == Convert.ToInt32(Location.HOPPER) select obj1);
        //                            if (arrHOPPERX.Count() > 0)
        //                            {
        //                                countHopperX = arrHOPPERX.Count();
        //                                TimeTillFree = HxTW;
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            TimeTillFree += ExpectedValues.QueueTimePerTruck * countQueue + ExpectedValues.Unloading * countEntry1 + ExpectedValues.Unloading * countEntry2 + ExpectedValues.Unloading * countHopperX;


        //            if (TravelTime > TimeTillFree)
        //            {
        //                return "Gate";
        //            }
        //            else
        //            {
        //                return "Queue";
        //            }
        //        }
        //        else
        //        {
        //            return "Queue";
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}








        // Re-impementation

        // Original
        //public static string _GateQueue(string TagRFID, Int32 HopperID, Int32 HopperQueueTimer, string TruckID)
        //{
        //    try
        //    {
        //        //  SYNTAX for MODBUS method
        //        //  public int IsHopperOperational(Int32 HopperID)
        //        //  Check is hopper operational?  (MODBUS will provide this information)
        //        //Int32 result=IsHopperOperational(Int32 HopperID);
        //        short padStatus = -1;

        //        //Modbus.Connector.Processor objProcessor = new Ramp.Modbus.Connector.Processor();
        //        //if (objProcessor.StartModbusServer(Ramp.Modbus.Utility.ModbusConnectionTypes.TCPIP) == 0)
        //        //{
        //        //    padStatus = objProcessor.ReadPAD_Status((byte)HopperID);
        //        //    objProcessor.StopModbusServer();
        //        //}
        //        //else
        //        //{
        //        //}

        //        if (padStatus == 0)
        //        {
        //            // Simplified version (remove common values from both the sides)
        //            Int32 TravelTime;
        //            TravelTime = ExpectedValues.REDS_Queue;


        //            Int32 queueLength = 0;
        //            DBAdapters.ActiveReads objActiveReads = new Ramp.DBConnector.Adapter.ActiveReads();
        //            queueLength = objActiveReads.GetQueueLengthForHopper(HopperID);

        //            Int32 TimeTillFree = 0;
        //            TimeTillFree += ExpectedValues.Unloading * queueLength + HopperQueueTimer;


        //            if (TravelTime > TimeTillFree)
        //            {
        //                // DBAdapters.ActiveReads.UpdateEntryORQueue(0, TagRFID, Location.ENTRY1);

        //                VMSDevice objVMSDevice = new VMSDevice(10, 128, 32, "", 1234, "", true);
        //                if (objVMSDevice.Open())
        //                {
        //                    // objVMSDevice.ShowString(0, 0, 0, 0, 2, 1, TruckID + " Gate");

        //                    objVMSDevice.Close();
        //                }

        //                return "Gate";
        //            }
        //            else
        //            {
        //                //DBAdapters.ActiveReads.UpdateEntryORQueue(0, TagRFID, Location.QUEUE);
        //                VMSDevice objVMSDevice = new VMSDevice(10, 128, 32, "", 1234, "", true);
        //                if (objVMSDevice.Open())
        //                {
        //                    //  objVMSDevice.ShowString(0, 0, 0, 0, 2, 1, TruckID + " Queue");

        //                    objVMSDevice.Close();
        //                }
        //                return "Queue";
        //            }
        //        }
        //        else
        //        {
        //            //DBAdapters.ActiveReads.UpdateEntryORQueue(0, TagRFID, Location.QUEUE);
        //            VMSDevice objVMSDevice = new VMSDevice(10, 128, 32, "", 1234, "", true);
        //            if (objVMSDevice.Open())
        //            {
        //                // objVMSDevice.ShowString(0, 0, 0, 0, 2, 1, TruckID + " Queue");

        //                objVMSDevice.Close();
        //            }
        //            return "Queue";
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        // Testing


        //04-11-2011: Bhavesh: Tested Alg Before TMS 1.5
        #region TMS 1.4 GateQueue Algorithm

        //public static string GateQueue(ITruck objTruck, int HopperQueueTimer) //(string TagRFID, Int32 HopperID, Int32 HopperQueueTimer, string TruckID)
        //{

        //    //31-10-2011:Bhavesh
        //    log.Trace("Inside GateQueue Alg in new thread for {0} with hopper queue timer {1}sec. Thread ID: {2}", objTruck.TagRFID.TagID, HopperQueueTimer.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId);
        //    log.Trace("Entry Time :- {0} Thread ID: {1}", DateTime.Now, System.Threading.Thread.CurrentThread.ManagedThreadId);
        //    Hopper objHopper = null;

        //    string formatString = String.Empty;

        //    string returnValue = string.Empty;

        //    string hopperNo = objTruck.TagDB.AssignedHopper.HopperName;
        //    // Int32 queueLength = 0;
        //    // short padStatus = 0;

        //    //int VMSLogging = 0;

        //    //Bhavesh: 15-11-2011
        //    //GetNextFreePad stored procedure will assign next free pad from the group and return expected pad arrvial time with new pad id.
        //    int newPad = 0;
        //    DateTime newPadTime;
        //    DateTime expectedArrival;
        //    VMSParameters vmsParams = new VMSParameters();

        //    DataTable dtVMSSettings = null;

        //    try
        //    {

        //        if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
        //        {

        //            byte HopperID = Convert.ToByte(hopperNo);



        //            #region getting readers into "reader" at REDS to send LED command
        //            List<IReader> lstReadersAtREDS = null;

        //            ReaderModule.Reader reader = null;

        //            if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.REDS))
        //            {
        //                lstReadersAtREDS = BLUtility.RFIDAdapterReaders[Location.REDS];
        //                if (lstReadersAtREDS != null && lstReadersAtREDS.Count > 0)
        //                {
        //                    reader = (ReaderModule.Reader)lstReadersAtREDS[0];
        //                }
        //            }
        //            #endregion

        //            #region checking hopper status. Changed to update all hopper status in db
        //            if (BLUtility.IsModbusConnected)
        //            {
        //                //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
        //                BLUtility.UpdateHopperStatus();
        //            }
        //            else
        //            {
        //                BLUtility.StartModbusServer();
        //                if (BLUtility.IsModbusConnected)
        //                {
        //                    //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
        //                    BLUtility.UpdateHopperStatus();
        //                }
        //            }
        //            #endregion

        //            int delayTimer = 0;

        //            log.Trace("Calling GetNextFreePad for {0} tag with start time {1} .", objTruck.TagDB.TagRFID, objTruck.LastReadTime);
        //            //DBAdapters.ActiveReads.EntryOrQueue(objTruck.TagDB.TagRFID, objTruck.LastReadTime, Location.REDS, out newPad, out desLoc);
        //            DBAdapters.ActiveReads.GetNextFreePad(objTruck.TagDB.TagRFID, objTruck.LastReadTime, Location.REDS, out newPad, out newPadTime);

        //            log.Trace("Pad received Time :- {0} Thread ID: {1}", DateTime.Now, System.Threading.Thread.CurrentThread.ManagedThreadId);
        //            expectedArrival = objTruck.LastReadTime.AddSeconds(ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 +
        //                ((BLUtility.HoppersInFacility.Keys.Contains(newPad.ToString())) ? BLUtility.HoppersInFacility[newPad.ToString()].Ent2_Hopper : 0));



        //            if (expectedArrival >= newPadTime && objTruck.TagDB.TagType != (int)TagType.ManualTarp)
        //            {
        //                log.Trace(" Returned from GetNextFreePad. Sending {0} to the Gate. New Pad time is {1}(newPadTime) which is not >= {2} for PAD-{3}.", objTruck.TagDB.TagRFID,
        //                    newPadTime, expectedArrival.ToString(), newPad.ToString());

        //                #region To EntryGate: setting truckTraTimer, flashing tag LED
        //                objTruck.EntryOrQueue = Location.ENTRY1;

        //                delayTimer = ExpectedValues.Max_Delay + ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;
        //                objTruck.SetTimer(delayTimer * 1000);
        //                objTruck.tmrTransaction.Start();

        //                if (reader != null)
        //                {

        //                    int flashingInterval = 3000;
        //                    int flashingTotalBlinks = 30;

        //                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedInterval))
        //                    {
        //                        flashingInterval = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedInterval];
        //                    }

        //                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedTotalBlink))
        //                    {
        //                        flashingTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedTotalBlink];
        //                    }

        //                    TimeSpan ts = new TimeSpan(0, 0, 0, 0, flashingInterval);

        //                    //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
        //                    if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks))
        //                    {
        //                        reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
        //                    }
        //                }

        //                returnValue = "Gate";

        //                #endregion
        //            }
        //            else
        //            {
        //                log.Trace(" Returned from GetNextFreePad. Sending {0} to the Queue. New Pad time is {1}(newPadTime) which is >= {2} for PAD-{3}.", objTruck.TagDB.TagRFID,
        //                    newPadTime, expectedArrival.ToString(), newPad.ToString());

        //                //truck has to wait in queue and because expectedArrival time is less (early) than newPadTime (earliest pad free time)
        //                //expectedArrival time has to change to newPadTime as truck cannot arrive before this time at the PAD
        //                expectedArrival = newPadTime;

        //                #region send truck to the queue
        //                objTruck.EntryOrQueue = Location.QUEUE;

        //                int value = Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(objTruck.LastReadTime).TotalSeconds));

        //                if (value < 0)
        //                {
        //                    value = 0;
        //                }



        //                delayTimer = ExpectedValues.REDS_Queue - value;

        //                if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
        //                    delayTimer += ExpectedValues.UnTarpTime;

        //                objTruck.SetTimer(delayTimer * 1000);
        //                objTruck.tmrTransaction.Start();

        //                if (reader != null)
        //                {
        //                    int solidInterval = 100;
        //                    int solidTotalBlinks = 60;

        //                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedInterval))
        //                    {
        //                        solidInterval = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedInterval];
        //                    }

        //                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedTotalBlink))
        //                    {
        //                        solidTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedTotalBlink];
        //                    }

        //                    TimeSpan ts = new TimeSpan(0, 0, 0, 0, solidInterval);

        //                    //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //                    if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks))
        //                    {
        //                        reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //                    }
        //                }



        //                Hopper.NoofTrucksDirectedtoQueue++;
        //                returnValue = "Queue";
        //                formatString = "`C0000FF ";

        //                #endregion
        //            }
        //            DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, objTruck.EntryOrQueue, newPad, expectedArrival, ref vmsParams, ref dtVMSSettings);

        //            log.Trace("Pad assign Time :- {0} NewPad: {1} Thread ID: {2}", DateTime.Now, newPad, System.Threading.Thread.CurrentThread.ManagedThreadId);
        //            objTruck.TagDB.DestinationPad = newPad;

        //            log.Trace("Decision Time :- {0}", DateTime.Now);

        //            #region hopper startup_shutdown
        //            if (BLUtility.ModbusProcessor.ReadPAD_Status((byte)newPad) == 0)
        //            {
        //                objHopper = BLUtility.HoppersInFacility[newPad.ToString()];
        //                if (objHopper != null)
        //                {
        //                    objHopper.Startup_ShutDownPAD(Location.REDS);
        //                }
        //            }
        //            #endregion

        //            //Disabled
        //            #region if current pad is available
        //            //if (padStatus == 0)
        //            //{
        //            //    objHopper = BLUtility.HoppersInFacility[hopperNo];
        //            //    #region hopper lock
        //            //    lock (objHopper.lockHopper)
        //            //    {
        //            //        lock (objHopper.lockList)
        //            //        {
        //            //            queueLength = objHopper.lstQueue.Count;
        //            //        }

        //            //        if (queueLength <= 0) // && (objHopper.tmrQueue == null || !objHopper.tmrQueue.Enabled))
        //            //        {
        //            //            //objHopper.SetTimer(ExpectedValues.Unloading);
        //            //            //objHopper.StartTimer();

        //            //            var TrucksForSameHopper = from obj in BLUtility.TrucksInTransaction.Values
        //            //                                      where (obj.TagDB.HopperID == HopperID &&
        //            //                                          obj.TagDB.TagType != (int)TagType.ManualTarp &&
        //            //                                          ((obj.LastPositiion == Location.REDS && obj.EntryOrQueue == Location.QUEUE) ||
        //            //                                          obj.LastPositiion == Location.QUEUE))
        //            //                                      select obj;
        //            //            //logger.Trace("Search for any trucks inbetween REDS-Q but not ManualTarp truck and found " + TrucksForSameHopper.Count().ToString()+" trucks");
        //            //            // Check for trucks in between REDS and QUEUE
        //            //            //OR Trucks with last location Queue (which may be called up and hence not in queue list) 

        //            //            lock (BLUtility.truckListLock)
        //            //            {
        //            //                queueLength = TrucksForSameHopper.Count();
        //            //            }

        //            //            if (queueLength <= 0)
        //            //            {
        //            //                if (objHopper.LastTruckDetectionTime.AddSeconds(ExpectedValues.Unloading) < objTruck.LastReadTime)
        //            //                {
        //            //                    //var TrucksForSameHopper = from obj in BLUtility.TrucksInTransaction.Values where (objTruck.TagDB.HopperID == HopperID && (objTruck.LastPositiion == Location.REDS || objTruck.LastPositiion == Location.ENTRY1 || objTruck.LastPositiion == Location.ENTRY2)) select obj;

        //            //                    //if (TrucksForSameHopper.Count() < 0)
        //            //                    //{
        //            //                    //    objHopper.LastTruckDetectionTime = DateTime.Now;
        //            //                    //    HopperQueueTimer = 0;
        //            //                    //} 

        //            //                    // objHopper.LastTruckDetectionTime = DateTime.Now;
        //            //                    objHopper.LastTruckDetectionTime = objTruck.LastReadTime;
        //            //                    HopperQueueTimer = 0;

        //            //                }
        //            //                else
        //            //                {
        //            //                    //HopperQueueTimer = Convert.ToInt32(DateTime.Now.Subtract(objHopper.LastTruckDetectionTime).TotalSeconds);
        //            //                    HopperQueueTimer = Convert.ToInt32(objTruck.LastReadTime.Subtract(objHopper.LastTruckDetectionTime).TotalSeconds);
        //            //                    HopperQueueTimer = ExpectedValues.Unloading - HopperQueueTimer;
        //            //                }
        //            //            }
        //            //            else
        //            //            {
        //            //                objTruck.EntryOrQueue = Location.QUEUE;
        //            //            }

        //            //        }
        //            //        //else
        //            //        //{
        //            //        //    HopperQueueTimer = DateTime.Now.Subtract(objHopper.TimerStartTime).Seconds;
        //            //        //    HopperQueueTimer = ExpectedValues.Unloading - HopperQueueTimer;
        //            //        //}
        //            //    }
        //            //    #endregion 

        //            //    // Simplified version (remove common values from both the sides)
        //            //    Int32 TravelTime;
        //            //    TravelTime = ExpectedValues.REDS_Queue;

        //            //    if (HopperQueueTimer < 0)
        //            //    {
        //            //        HopperQueueTimer = 0;
        //            //    }

        //            //    Int32 TimeTillFree = 0;
        //            //    TimeTillFree += ExpectedValues.Unloading * queueLength + HopperQueueTimer + ExpectedValues.REDS_Queue;

        //            //    if (objTruck.TagDB.TagType != (int)TagType.ManualTarp && TravelTime >= TimeTillFree)
        //            //    {
        //            //        #region sending truck to Gate as travel time > time till free
        //            //        //objTruck.EntryOrQueue = Location.ENTRY1;

        //            //        //delayTimer = ExpectedValues.Max_Delay + ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;
        //            //        //objTruck.SetTimer(delayTimer * 1000);
        //            //        //objTruck.tmrTransaction.Start();

        //            //        //if (reader != null)
        //            //        //{

        //            //        //    int flashingInterval = 3000;
        //            //        //    int flashingTotalBlinks = 30;

        //            //        //    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedInterval))
        //            //        //    {
        //            //        //        flashingInterval = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedInterval];
        //            //        //    }

        //            //        //    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedTotalBlink))
        //            //        //    {
        //            //        //        flashingTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedTotalBlink];
        //            //        //    }

        //            //        //    TimeSpan ts = new TimeSpan(0, 0, 0, 0, flashingInterval);

        //            //        //    //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
        //            //        //    if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks))
        //            //        //    {
        //            //        //        reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
        //            //        //    }
        //            //        //}

        //            //        DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, Location.ENTRY1, ref vmsParams, ref dtVMSSettings);
        //            //        #endregion 

        //            //        returnValue = "Gate";

        //            //    }
        //            //    else
        //            //    {
        //            //        #region sending truck to the queue as travel time is < time till free
        //            //        //objTruck.EntryOrQueue = Location.QUEUE;

        //            //        //int value = Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(objTruck.LastReadTime).TotalSeconds));

        //            //        //if (value < 0)
        //            //        //{
        //            //        //    value = 0;
        //            //        //}

        //            //        //if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
        //            //        //    value += ExpectedValues.UnTarpTime;

        //            //        //delayTimer = ExpectedValues.REDS_Queue - value;
        //            //        //objTruck.SetTimer(delayTimer * 1000);
        //            //        //objTruck.tmrTransaction.Start();

        //            //        //if (reader != null)
        //            //        //{
        //            //        //    int solidInterval = 100;
        //            //        //    int solidTotalBlinks = 60;

        //            //        //    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedInterval))
        //            //        //    {
        //            //        //        solidInterval = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedInterval];
        //            //        //    }

        //            //        //    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedTotalBlink))
        //            //        //    {
        //            //        //        solidTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedTotalBlink];
        //            //        //    }

        //            //        //    TimeSpan ts = new TimeSpan(0, 0, 0, 0, solidInterval);

        //            //        //    //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //            //        //    if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks))
        //            //        //    {
        //            //        //        reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //            //        //    }
        //            //        //}

        //            //        //DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, Location.QUEUE, ref vmsParams, ref dtVMSSettings);

        //            //        //Hopper.NoofTrucksDirectedtoQueue++;
        //            //        #endregion

        //            //        returnValue = "Queue";
        //            //        formatString = "`C0000FF ";  // RED 

        //            //    }

        //            //    if (objHopper != null)
        //            //    {
        //            //        objHopper.Startup_ShutDownPAD(Location.REDS);
        //            //    }
        //            //}//end of if current pad is available
        //            //else
        //            //{
        //            //    #region sending truck to the queue as pad status <> 0
        //            //    objTruck.EntryOrQueue = Location.QUEUE;

        //            //    int value = Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(objTruck.LastReadTime).TotalSeconds));

        //            //    if (value < 0)
        //            //    {
        //            //        value = 0;
        //            //    }

        //            //    if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
        //            //        value += ExpectedValues.UnTarpTime;

        //            //    delayTimer = ExpectedValues.REDS_Queue - value;
        //            //    objTruck.SetTimer(delayTimer * 1000);
        //            //    objTruck.tmrTransaction.Start();

        //            //    if (reader != null)
        //            //    {
        //            //        int solidInterval = 100;
        //            //        int solidTotalBlinks = 60;

        //            //        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedInterval))
        //            //        {
        //            //            solidInterval = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedInterval];
        //            //        }

        //            //        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedTotalBlink))
        //            //        {
        //            //            solidTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedTotalBlink];
        //            //        }

        //            //        TimeSpan ts = new TimeSpan(0, 0, 0, 0, solidInterval);

        //            //        // reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //            //        if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks))
        //            //        {
        //            //            reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
        //            //        }
        //            //    }

        //            //    DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, Location.QUEUE, ref vmsParams, ref dtVMSSettings);

        //            //    Hopper.NoofTrucksDirectedtoQueue++;
        //            //    #endregion 

        //            //    returnValue = "Queue";

        //            //}//end of else pad is not available
        //            #endregion

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ex;
        //        log.ErrorException("Exception thrown in Gate-Queue algorithm. Truck " + objTruck.TagDB.TagRFID + ": Error message:", ex);
        //        objTruck.EntryOrQueue = Location.QUEUE;
        //        objTruck.SetTimer(ExpectedValues.REDS_Queue * 1000);
        //        objTruck.tmrTransaction.Start();

        //        DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, Location.QUEUE, 0, DateTime.MinValue, ref vmsParams, ref dtVMSSettings);
        //        //if (objHopper != null)
        //        //{
        //        //    objHopper.AddToQueue(objTruck.TagDB);
        //        //}
        //        Hopper.NoofTrucksDirectedtoQueue++;
        //        returnValue = "Queue";

        //    }


        //    if (returnValue == "Queue")
        //    {
        //        objTruck.EntryOrQueue = Location.QUEUE;
        //        formatString = "`C0000FF ";  // RED

        //        if (BLUtility.DisplayPadNo) // changed on 25-05-2011 
        //        {
        //            returnValue = "Queue " + newPad.ToString().PadLeft(2, '0'); // changed on 08-04-2011 add PAD no. to display string on VMS. 
        //        }
        //        else
        //        {
        //            returnValue = "Queue";
        //        }


        //        log.Trace("checking if newly assigned hopper : {0} is not null for: {1} truck", objTruck.TagDB.DestinationPad, objTruck.TagDB.TagRFID);
        //        if (objHopper != null)
        //        {
        //            lock (objHopper.lockHopper)
        //            {
        //                // start queue timer ( changes made on 18/04/2011 )
        //                log.Trace(" Hopper is available. checking timer for PAD: {0}", objHopper.HopperName);
        //                if (objHopper.tmrQueue == null || !objHopper.tmrQueue.Enabled)
        //                {
        //                    if (!objHopper.IsTimerPaused && !objHopper.nextCallupOccured)
        //                    {
        //                        log.Trace(" Timer is ready to reset. Re-set callup timer for PAD: {0} after truck {1}", objHopper.HopperName, objTruck.TagDB.TagRFID);

        //                        if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
        //                            HopperQueueTimer += ExpectedValues.UnTarpTime;

        //                        objHopper.SetTimer(ExpectedValues.REDS_Queue + HopperQueueTimer);
        //                        objHopper.StartTimer();
        //                    }
        //                }
        //            }//enf of hopper lock
        //        }//end of if hopper is not null

        //    }
        //    else
        //    {
        //        objTruck.EntryOrQueue = Location.ENTRY1;
        //        formatString = "`C00FF00 ";  // GREEN
        //        if (BLUtility.DisplayPadNo) // changed on 25-05-2011 
        //        {
        //            returnValue = "Gate  " + newPad.ToString().PadLeft(2, '0'); // changed on 08-04-2011 add PAD no. to display string on VMS. 
        //        }
        //        else
        //        {
        //            returnValue = "Gate ";
        //        }
        //    }

        //    #region send message to the VMS
        //    try
        //    {
        //        UpdateVMSSettings(dtVMSSettings, Location.REDS); // Update VMS Settings each time with values in DB.

        //        List<IVMSDevice> lstVMSDevices = null;

        //        lock (BLUtility.VMSListLock)
        //        {
        //            if (BLUtility.VMSDeviceList.ContainsKey(Location.REDS))
        //            {
        //                lstVMSDevices = BLUtility.VMSDeviceList[Location.REDS];
        //            }
        //        }

        //        if (lstVMSDevices != null && lstVMSDevices.Count > 0)
        //        {
        //            string vmsMessage = objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " " + returnValue;
        //            foreach (IVMSDevice obj in lstVMSDevices)
        //            {
        //                obj.UseRandomPortNo = vmsParams.UseRandomPort;
        //                obj.MinUDPPortNo = vmsParams.MinPortNo;
        //                obj.MaxUDPPortNo = vmsParams.MaxPortNo;

        //                obj.ShowString(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " " + returnValue, formatString, vmsParams.VMSLogging);
        //            }
        //        }




        //    }
        //    catch (Exception ex)
        //    {
        //        log.ErrorException(ex.Message, ex);
        //    }
        //    #endregion

        //    return returnValue;
        //}


        #endregion TMS 1.4 GateQueue Algorithm


        #region Using EntryOrQueue storedprocedure

        //04-11-2011: Bhavesh: Tested Alg Before TMS 1.5
        #region TMS 1.4 GateQueue Algorithm

        public static string GateQueue(ITruck objTruck, int HopperQueueTimer) //(string TagRFID, Int32 HopperID, Int32 HopperQueueTimer, string TruckID)
        {

            //31-10-2011:Bhavesh
            Hopper objHopper = null;

            string formatString = String.Empty;

            string returnValue = string.Empty;

            //string hopperNo = .ToString();
            int newPad = objTruck.TagDB.DestinationPad;
            //Location desLoc;
            //DateTime expectedArrival;
            VMSParameters vmsParams = new VMSParameters();

            DataTable dtVMSSettings = null;
            int delayTimer = 0;
            try
            {

                byte HopperID = Convert.ToByte(newPad);

                #region getting readers into "reader" at REDS to send LED command
                List<IReader> lstReadersAtREDS = null;

                ReaderModule.Reader reader = null;

                if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.REDS))
                {
                    lstReadersAtREDS = BLUtility.RFIDAdapterReaders[Location.REDS];
                    if (lstReadersAtREDS != null && lstReadersAtREDS.Count > 0)
                    {
                        reader = (ReaderModule.Reader)lstReadersAtREDS[0];
                    }
                }
                #endregion

                if (objTruck.EntryOrQueue == Location.ENTRY1)
                {

                    #region To EntryGate: setting truckTraTimer, flashing tag LED
                    // objTruck.EntryOrQueue = Location.ENTRY1;


                    delayTimer = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;
                    objTruck.StartTimer((ExpectedValues.Max_Delay + delayTimer) * 1000);
                    objTruck.StartResTimer(delayTimer * 1000);
                    //objTruck.StopResTimer();
                    // objTruck.SetResTimer();
                    //      ExpectedValues.Max_Delay + delayTimer, delayTimer, objTruck.TagDB.TagRFID);
                    //
                    //objTruck.tmrTransaction.Start();

                    if (reader != null)
                    {

                        int flashingInterval = 3000;
                        int flashingTotalBlinks = 30;

                        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedInterval))
                        {
                            flashingInterval = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedInterval];
                        }

                        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedTotalBlink))
                        {
                            flashingTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedTotalBlink];
                        }

                        TimeSpan ts = new TimeSpan(0, 0, 0, 0, flashingInterval);

                        //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
                        // if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks))
                        {
                            reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, flashingTotalBlinks);
                        }
                    }

                    returnValue = "Gate";

                    #endregion
                }
                else
                {
                    // objTruck.TagDB.DestinationPad);

                    //truck has to wait in queue and because expectedArrival time is less (early) than newPadTime (earliest pad free time)
                    //expectedArrival time has to change to newPadTime as truck cannot arrive before this time at the PAD
                    // expectedArrival = newPadTime;

                    #region send truck to the queue
                    //objTruck.EntryOrQueue = Location.QUEUE;

                    int value = Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(objTruck.LastReadTime).TotalSeconds));

                    if (value < 0)
                    {
                        value = 0;
                    }



                    delayTimer = ExpectedValues.REDS_Queue - value;

                    if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
                        delayTimer += ExpectedValues.UnTarpTime;


                    objTruck.StartTimer(delayTimer * 1000);


                    if (reader != null)
                    {
                        int solidInterval = 100;
                        int solidTotalBlinks = 60;

                        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedInterval))
                        {
                            solidInterval = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedInterval];
                        }

                        if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.SolidLedTotalBlink))
                        {
                            solidTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.SolidLedTotalBlink];
                        }

                        TimeSpan ts = new TimeSpan(0, 0, 0, 0, solidInterval);

                        //reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
                        //if (!reader.BlinkLED(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks))
                        {
                            reader.BlinkLEDAsync(objTruck.TagRFID.TagID, TagLedColor.GREEN, ts, solidTotalBlinks);
                        }
                    }



                    Hopper.NoofTrucksDirectedtoQueue++;
                    returnValue = "Queue";
                    formatString = "`CFF0000 ";

                    #endregion
                }

                //objTruck.TagDB.DestinationPad = newPad;


            }
            catch (Exception ex)
            {
                //throw ex;
                if (objTruck.tmrTransaction == null || !objTruck.tmrTransaction.Enabled)
                {
                    if (objTruck.EntryOrQueue == Location.ENTRY1)
                        delayTimer = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Max_Delay;
                    else
                        delayTimer = ExpectedValues.REDS_Queue;

                    objTruck.StartTimer(delayTimer * 1000);

                    if (objTruck.tmrCheckResponseTime == null || !objTruck.tmrCheckResponseTime.Enabled)
                        objTruck.StartResTimer((delayTimer - ExpectedValues.Max_Delay) * 1000);
                }


                //  objTruck.EntryOrQueue = Location.QUEUE;
                //objTruck.SetTimer(ExpectedValues.REDS_Queue * 1000);
                //objTruck.tmrTransaction.Start();

                DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID, objTruck.EntryOrQueue, 0, DateTime.MinValue, ref vmsParams, ref dtVMSSettings);
                //if (objHopper != null)
                //{
                //    objHopper.AddToQueue(objTruck.TagDB);
                //}
                if (objTruck.EntryOrQueue == Location.QUEUE)
                {
                    Hopper.NoofTrucksDirectedtoQueue++;
                    returnValue = "Queue";
                }
                else
                {
                    returnValue = "Gate";
                }

            }


            if (returnValue == "Queue" || objTruck.EntryOrQueue == Location.QUEUE)
            {
                //objTruck.EntryOrQueue = Location.QUEUE;
                formatString = "`CFF0000 ";  // RED

                if (BLUtility.DisplayPadNo) // changed on 25-05-2011 
                {
                    returnValue = "Queue " + newPad.ToString().PadLeft(2, '0'); // changed on 08-04-2011 add PAD no. to display string on VMS. 
                }
                else
                {
                    returnValue = "Queue";
                }


                if (objHopper != null)
                {
                    lock (objHopper.lockHopper)
                    {
                        // start queue timer ( changes made on 18/04/2011 )
                        if (objHopper.tmrQueue == null || !objHopper.tmrQueue.Enabled)
                        {
                            if (!objHopper.IsTimerPaused && !objHopper.nextCallupOccured)
                            {

                                if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
                                    HopperQueueTimer += ExpectedValues.UnTarpTime;

                                objHopper.StartTimer((ExpectedValues.REDS_Queue + HopperQueueTimer) * 1000);
#if DEBUG
                                log.Info("Starting timer with {0} ms for {1} pad in GateQueue Alg.", (ExpectedValues.REDS_Queue + HopperQueueTimer) * 1000, objHopper.HopperID);
#endif

                            }
                        }
                    }//enf of hopper lock
                }//end of if hopper is not null




            }
            else if (returnValue == "Gate" || objTruck.EntryOrQueue == Location.ENTRY1)
            {
                //objTruck.EntryOrQueue = Location.ENTRY1;
                formatString = "`C00FF00 ";  // GREEN
                if (BLUtility.DisplayPadNo) // changed on 25-05-2011 
                {
                    returnValue = "Gate  " + newPad.ToString().PadLeft(2, '0'); // changed on 08-04-2011 add PAD no. to display string on VMS. 
                }
                else
                {
                    returnValue = "Gate ";
                }

                try
                {
                    BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).ShowString(objTruck.TagDB.TruckID, "`C00FF00", 0);
                }
                catch { }
            }

            #region send message to the VMS
            try
            {

                List<IVMSDevice> lstVMSDevices = null;
                //string[] vmsSettings;
                //string vmsConnected = "1";
                //IAsyncResult iResult = null;


                string vmsMessage = objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " " + returnValue;

#if DEBUG
                log.Info("Message to send to VMS : [ {0} ]", vmsMessage);
#endif

                //new VMS
                //if (File.Exists("VMSConfig.txt"))
                //{
                //    vmsSettings = File.ReadAllLines("VMSConfig.txt");
                //    vmsConnected = vmsSettings.First(x => x.ToLower().Contains("vmsconnected"));
                //    if(!string.IsNullOrEmpty(vmsConnected) && vmsConnected.Contains(':'))
                //        vmsConnected = vmsConnected.Substring(vmsConnected.IndexOf(':')+1).Trim();
                //    //vmsConnected : 0-No VMS, 1-Default VMS, 2-New VMS, 3-Both VMS
                //    if (vmsConnected == "0")
                //        return returnValue;

                //    if (vmsConnected == "2" || vmsConnected == "3")
                //    {
                //        iResult = newVMS.BeginInvoke(vmsSettings, formatString + vmsMessage.ToUpper(),true, null, null);
                //    }
                //}



                //if (vmsConnected == "1" || vmsConnected == "3")
                //{
                lock (BLUtility.VMSListLock)
                {
                    if (BLUtility.VMSDeviceList.ContainsKey(Location.REDS))
                    {
                        lstVMSDevices = BLUtility.VMSDeviceList[Location.REDS];
                    }
                }
                //}

                if (lstVMSDevices != null && lstVMSDevices.Count > 0)
                {

                    foreach (IVMSDevice obj in lstVMSDevices)
                    {
                        // VMSDevice vmddev =  obj as VMSDevice;
                        if (obj != null)
                        {
                            obj.UseRandomPortNo = vmsParams.UseRandomPort;
                            obj.MinUDPPortNo = vmsParams.MinPortNo;
                            obj.MaxUDPPortNo = vmsParams.MaxPortNo;
                            obj.ShowString(vmsMessage, formatString, vmsParams.VMSLogging);
                        }
                    }
                }

                //if (iResult != null)
                //    newVMS.EndInvoke(iResult);

                if (dtVMSSettings != null && dtVMSSettings.Rows.Count > 0)
                    UpdateVMSSettings(dtVMSSettings, Location.REDS); // Update VMS Settings each time with values in DB.


            }
            catch (Exception ex)
            {
                log.Error("Exception thrown in GateQueue method in Algorithms. : {0}", ex.Message);
            }
            #endregion

            #region hopper startup_shutdown
            if (BLUtility.ModbusProcessor.ReadPAD_Status((byte)newPad) == 0)
            {
                objHopper = BLUtility.HoppersInFacility[newPad.ToString()];
                if (objHopper != null)
                {
                    objHopper.Startup_ShutDownPAD(Location.REDS);
                }
            }
            else
            {
                BLUtility.UpdateHopperStatus();
            }
            #endregion


            return returnValue;
        }


        #endregion TMS 1.4 GateQueue Algorithm


        #endregion



        /*
         * Date:4-11-2011
         * Modified By:Bhavesh
         * Detail: Implementing new algorithm to include Manual Tarping trucks and 
         *          PAD Grouping.
         * 
         */
        //public static string GateQueue(ITruck objTruck, int HopperQueueTimer) //(string TagRFID, Int32 HopperID, Int32 HopperQueueTimer, string TruckID)
        //{

        //    //31-10-2011:Bhavesh
        //    log.Trace("Inside GateQueue Alg in new thread for {0} with hopper queue timer {1}sec.",objTruck.TagRFID.TagID,HopperQueueTimer.ToString());

        //    if (objTruck.TagDB.TagType == (int)TagType.ManualTarp)
        //    {
        //        #region Manual Tarping truck
        //        log.Trace("Truck {0} is manual tarping truck.", objTruck.TagRFID.TagID);

        //        objTruck.EntryOrQueue = Location.InboundUntarping;

        //        int value = Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(objTruck.LastReadTime).TotalSeconds));

        //        if (value < 0)
        //            value = 0;

        //        delayTimer = ExpectedValues.REDS_Queue - value;
        //        objTruck.SetTimer(delayTimer * 1000);
        //        objTruck.tmrTransaction.Start();


        //        DBAdapters.ActiveReads.UpdateEntryORQueue(0, objTruck.TagRFID.TagID,objTruck.LastReadTime, Location.InboundUntarping, ref vmsParams, ref dtVMSSettings);

        //        Hopper.NoofTrucksDirectedtoQueue++;

        //        returnValue = "Queue";
        //        #endregion Manual Tarping truck

        //    }
        //    else
        //    {
        //        #region Auto Tarping truck
        //        log.Trace("Truck {0} is autoTrap truck."objTruck.TagRFID.TagID);


        //        #endregion Auto Tarping Truck
        //    }

        //    return returnValue;
        //}





        #endregion




        #region "TruckCallUp"

        //public static void TruckCallUp(int HopperID, string TagRFID)
        //{
        //    try
        //    {

        //        ////  SYNTAX for MODBUS method  Check is hopper operational?  (MODBUS will provide this information)
        //        ////  public int IsHopperOperational(Int32 HopperID)
        //        ////  Int32 result=IsHopperOperational(Int32 HopperID);
        //        ////While( IsHopperOperational ( int HopperID )!= 1 )
        //        //while (222 != 0)  // Not Operational
        //        //{
        //        //    // do nothing
        //        //}

        //        //// ASA becomes Operational.



        //        Int32 queueLength = 0;
        //        DBAdapters.ActiveReads objActiveReads = new Ramp.DBConnector.Adapter.ActiveReads();
        //        queueLength = objActiveReads.GetQueueLengthForHopper(HopperID);

        //        if (queueLength > 0)
        //        {

        //            // Call the First TRUCK   ***
        //            List<IActiveReads> objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);
        //            IEnumerable<IActiveReads> queueTrucksList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition == Convert.ToInt32(Location.QUEUE)) select obj1);
        //            queueTrucksList.OrderBy(obj => obj.Queue);

        //            Int32 TagID = 0;
        //            foreach (IActiveReads objReds in queueTrucksList)
        //            {
        //                TagID = objReds.TagID;
        //                break;
        //            }

        //            DBAdapters.ActiveReads.UpdateTruckCallUp(TagID);

        //            //1) LED Blink

        //            List<IReader> lstReadersInQueue = null;

        //            if (Ramp.RFIDHWConnector.Common.RFIDConnectorUtility.lstAllRFIDReaders.ContainsKey(Location.QUEUE))
        //            {
        //                lstReadersInQueue = Ramp.RFIDHWConnector.Common.RFIDConnectorUtility.lstAllRFIDReaders[Location.QUEUE];
        //            }
        //            else
        //            {
        //                lstReadersInQueue = Ramp.RFIDHWConnector.Adapters.Reader.GetReadersAtLocation(Location.QUEUE); 
        //            }

        //            foreach (IReader reader in lstReadersInQueue)
        //            {
        //                ReaderModule.Reader objReader = null;
        //                if (objReader.BlinkLED("", TagLedColor.GREEN, new TimeSpan(0, 0, 1)))
        //                {
        //                    break;
        //                }
        //            }                    

        //            //2) VMS signal




        //            // Reset & Initialize Timer
        //            //Timer = ExpectedValues.Unloading  


        //            // Start Timer

        //        }
        //        else
        //        {
        //            // To be Discussed
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        // Original
        //public static void _UpdateTruckCallUp(string TagRFID)
        //{
        //    try
        //    {
        //        // 1) UPDATE DB
        //        //DBAdapters.ActiveReads.UpdateTruckCallUp(TagRFID);


        //        // 2) LED Blink
        //        List<IReader> lstReadersInQueue = null;

        //        if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.QUEUE))
        //        {
        //            lstReadersInQueue = BLUtility.RFIDAdapterReaders[Location.QUEUE];
        //        }
        //        else
        //        {
        //            lstReadersInQueue = ReaderModule.Reader.GetReadersAtLocation(Location.QUEUE);
        //            if (lstReadersInQueue != null && lstReadersInQueue.Count > 0)
        //            {
        //                BLUtility.RFIDAdapterReaders[Location.QUEUE] = lstReadersInQueue;
        //            }
        //        }

        //        // 3) VMS signal
        //        VMSDevice objVMSDevice = new VMSDevice(10, 128, 32, "", 1234, "", true);
        //        if (objVMSDevice.Open())
        //        {
        //            // objVMSDevice.ShowString(0, 0, 0, 0, 2, 1, "GO");

        //            objVMSDevice.Disconnect();
        //        }

        //        if (lstReadersInQueue != null && lstReadersInQueue.Count > 0)
        //        {
        //            foreach (IReader reader in lstReadersInQueue)
        //            {
        //                ReaderModule.Reader objReader = null;
        //                if (objReader.BlinkLED(TagRFID, TagLedColor.GREEN, new TimeSpan(0, 0, 1)))
        //                {
        //                    break;
        //                }
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        // throw ex;
        //    }
        //}


        //public static void TruckCallup(Hopper objHopper, params ITags[] skipedTag)
        //{
        //    //nextCallupOccured = true;
        //    //tmrQueue.Stop();

        //    string skipTagRFID = "", callupTagRFID = "";

        //    if (skipedTag.Count() > 0 && !string.IsNullOrEmpty(skipedTag[0].TagRFID.Trim()))
        //        skipTagRFID = skipedTag[0].TagRFID;
        //    log.Trace("Thread {0} Entering CallupLocker.", System.Threading.Thread.CurrentThread.ManagedThreadId);
        //    lock (CallupLocker)
        //    {
        //        log.Trace("Thread {0} inside (before) CallupLocker.", System.Threading.Thread.CurrentThread.ManagedThreadId);
        //        callupTagRFID = DBConnector.Adapter.ActiveReads.NextTruckToCallup(skipTagRFID, HopperID);
        //        log.Trace("Thread {0} inside (after) CallupLocker.", System.Threading.Thread.CurrentThread.ManagedThreadId);
        //    }
        //    log.Trace("Thread {0} Exit CallupLocker.", System.Threading.Thread.CurrentThread.ManagedThreadId);
        //    if (!string.IsNullOrEmpty(callupTagRFID.Trim()))
        //    {
        //        //Starting ResponseTimer
        //        Algorithms.UpdateTruckCallUp(callupTagRFID);

        //    }


        //    //nextCallupOccured = false;
        //    //tmrQueue.Start();


        //}

        // Testing
        public static bool UpdateTruckCallUp(string TagRFID, int HopperID)
        {
            bool returnValue = true;
            try
            {
                string truckID = "00";
                string hopperID = "00";

                VMSParameters vmsParam = new VMSParameters();

                DataTable dtVMSSettings = null;

                // 1) UPDATE DB
                //int TruckCallUpRetries = 0;

                //NextTruckToCallUp will update the ActiveReads Table and Tags table as well
                //if (!DBAdapters.ActiveReads.UpdateTruckCallUp(TagRFID, ref vmsParam, ref TruckCallUpRetries, ref dtVMSSettings))
                //{
                //    //lock (BLUtility.truckListLock)
                //    //{
                //    //    BLUtility.TrucksInTransaction.Remove(TagRFID);
                //    //}

                //    return false;
                //}

                lock (BLUtility.truckListLock)
                {
                    if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID))
                    {
                        BLUtility.TrucksInTransaction[TagRFID].LastPositiion = Location.InboundEntry;
                        BLUtility.TrucksInTransaction[TagRFID].CalledUp = true;
                        truckID = BLUtility.TrucksInTransaction[TagRFID].TagDB.TruckID.ToString();
                        hopperID = HopperID.ToString();
                        BLUtility.TrucksInTransaction[TagRFID].TagDB.DestinationPad = HopperID;
                        //BLUtility.TrucksInTransaction[TagRFID].FoundAtHopperID = HopperID;

                        BLUtility.TrucksInTransaction[TagRFID].StartResTimer(ExpectedValues.TruckCallResponse * 1000);


                    }
                    else
                    {
                        return false;
                    }
                }


                //BLUtility.iTruckCallupRetries = TruckCallUpRetries;

                Hashtable htParams = new Hashtable();

                htParams.Add("vmsParam", vmsParam);
                htParams.Add("dtVMSSettings", dtVMSSettings);
                htParams.Add("truckID", truckID);
                htParams.Add("TagRFID", TagRFID);
                htParams.Add("hopperID", hopperID);
                htParams.Add("Location", Location.QUEUE);

                ThreadPool.QueueUserWorkItem(new WaitCallback(TagBlinkNVMSDisplay), htParams);

                try
                {
                    BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == Convert.ToInt16(hopperID)).ShowString(truckID, "`C00FF00", 0);

                }
                catch { }


            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in UpdateTruckCallup method for {0} T, {1} H. Thread {2}",TagRFID, HopperID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }

            return returnValue;
        }

        private static void TagBlinkNVMSDisplay(object objParams)
        {
            try
            {
                Hashtable htParams = (Hashtable)objParams;

                Location loc = (Location)htParams["Location"];

                VMSParameters vmsParam = (VMSParameters)htParams["vmsParam"];
                DataTable dtVMSSettings = null;

                if (loc == Location.QUEUE)
                {

                    dtVMSSettings = (DataTable)htParams["dtVMSSettings"];
                }
                else
                {
                    loc = Location.HOPPER;
                }
                string TagRFID = (string)htParams["TagRFID"];
                string truckID = (string)htParams["truckID"];
                string hopperID = (string)htParams["hopperID"];


                if (dtVMSSettings != null)
                    UpdateVMSSettings(dtVMSSettings, loc); // Update VMS Settings each time with values in DB.             

                string formatString = "`C00FF00 "; //Green

                // 2) VMS signal
                try
                {
                    List<IVMSDevice> lstVMSDevices = null;
                    lock (BLUtility.VMSListLock)
                    {
                        if (BLUtility.VMSDeviceList.ContainsKey(loc))
                        {
                            try
                            {
                                if (loc == Location.HOPPER)
                                    lstVMSDevices = BLUtility.VMSDeviceList[loc].FindAll(x => Convert.ToInt32(x.DeviceNo.ToString()) == Convert.ToInt32(hopperID));
                                else
                                    lstVMSDevices = BLUtility.VMSDeviceList[loc];
                            }
                            catch
                            {

                            }
                        }
                    }

                    if (lstVMSDevices != null && lstVMSDevices.Count > 0)
                    {
                        string vmsString = truckID.PadLeft(4, '0');
                        if (loc == Location.QUEUE)
                            vmsString += " - PAD " + hopperID.PadLeft(2, '0');

                        //if (loc == Location.QUEUE && BLUtility.DisplayPadNo) // changes made on 25-05-2011
                        //{
                        //    vmsString = vmsString  + hopperID;  // changes made on 09-04-2011, to add PAD no. in display string to VMS.
                        //}

                        Ping p = new Ping();

                        foreach (IVMSDevice obj in lstVMSDevices)
                        {
                            if (p.Send(obj.IPAddress.Trim(), 1000).Status != IPStatus.Success)
                            {
                                continue;
                            }
                            // VMSDevice vmsdev = obj as VMSDevice;
                            if (obj != null)
                            {
                                obj.UseRandomPortNo = vmsParam.UseRandomPort;
                                obj.MinUDPPortNo = vmsParam.MinPortNo;
                                obj.MaxUDPPortNo = vmsParam.MaxPortNo;
                                obj.ShowString(vmsString, formatString, vmsParam.VMSLogging);
                            }
                        }

                    }

                }
                catch
                {
                }


                // 3) LED Blink

                if (loc == Location.QUEUE)
                {
                    List<IReader> lstReadersInQueue = null;
                    if (BLUtility.RFIDAdapterReaders.ContainsKey(Location.QUEUE))
                    {
                        lstReadersInQueue = BLUtility.RFIDAdapterReaders[Location.QUEUE];
                    }
                    else
                    {
                        lstReadersInQueue = ReaderModule.Reader.GetReadersAtLocation(Location.QUEUE);
                        if (lstReadersInQueue != null && lstReadersInQueue.Count > 0)
                        {
                            BLUtility.RFIDAdapterReaders[Location.QUEUE] = lstReadersInQueue;
                        }
                    }

                    int flashingInterval = 60;
                    int flashingTotalBlinks = 50;

                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedInterval))
                    {
                        flashingInterval = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedInterval];
                    }

                    if (BLUtility.ConfigValues.ContainsKey(ConfigurationValues.FlashingLedTotalBlink))
                    {
                        flashingTotalBlinks = (int)BLUtility.ConfigValues[ConfigurationValues.FlashingLedTotalBlink];
                    }

                    TimeSpan ts = new TimeSpan(0, 0, flashingInterval);

                    if (lstReadersInQueue != null && lstReadersInQueue.Count > 0)
                    {
                        foreach (IReader reader in lstReadersInQueue)
                        {
                            if (reader.BlinkLED(TagRFID, TagLedColor.GREEN, ts, flashingTotalBlinks))
                            {
                                reader.Dispose();
                                break;
                            }

                            reader.Dispose();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                BLUtility.WriteEventLog(ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private static void UpdateVMSSettings(DataTable dtVMSSettings, Location loc)
        {
            try
            {

                VMSDevice obj;

                List<IVMSDevice> lstVMSDevice = new List<IVMSDevice>();

                lock (BLUtility.VMSListLock)
                {
                    if (BLUtility.VMSDeviceList.ContainsKey(loc))
                    {
                        lstVMSDevice = BLUtility.VMSDeviceList[loc];
                    }
                    else
                    {
                        lstVMSDevice = new List<IVMSDevice>();
                        BLUtility.VMSDeviceList.Add(loc, lstVMSDevice);
                    }
                }

                DataRow[] drr;

                if (dtVMSSettings != null && dtVMSSettings.Rows.Count > 0)
                {
                    foreach (IVMSDevice objVMSDevice in lstVMSDevice)
                    {
                        try
                        {
                            if (objVMSDevice != null)
                            {
                                drr = dtVMSSettings.Select("IPAddress = '" + objVMSDevice.IPAddress.Trim() + "'");
                                if (drr != null && drr.Length > 0)
                                {
                                    objVMSDevice.DeviceNo = drr[0]["DeviceNo"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["DeviceNo"]);
                                    objVMSDevice.LedHeight = drr[0]["LEDHeight"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["LEDHeight"]);
                                    objVMSDevice.LedWidth = drr[0]["LEDWidth"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["LEDWidth"]);

                                    objVMSDevice.UDPPort = drr[0]["PortNo"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["PortNo"]);
                                    objVMSDevice.SCL2008 = drr[0]["IsSCL2008"] == DBNull.Value ? false : Convert.ToBoolean(drr[0]["IsSCL2008"]);

                                    objVMSDevice.AscFont = drr[0]["ASCFont"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["ASCFont"]);
                                    objVMSDevice.TextColor = drr[0]["TextColor"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["TextColor"]);
                                    objVMSDevice.TextStartPosition = drr[0]["TextStartPosition"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["TextStartPosition"]);
                                    objVMSDevice.TextLength = drr[0]["TextLength"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(drr[0]["TextLength"]);

                                    objVMSDevice.MessageTimeOut = drr[0]["MessageTimeOut"] == DBNull.Value ? 1 : Convert.ToInt32(drr[0]["MessageTimeOut"]);

                                    objVMSDevice.DefaultMessage = drr[0]["DefaultMessage"] == DBNull.Value ? "" : Convert.ToString(drr[0]["DefaultMessage"]);
                                    //if(!string.IsNullOrEmpty(objVMSDevice.DefaultMessage))
                                    //    AussportVMS.DefaultMessage = objVMSDevice.DefaultMessage;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteLogs("Update VMS Settings : " + ex.Message);
                        }
                    }
                }
                else
                {
                    foreach (DataRow dr in dtVMSSettings.Rows)
                    {
                        try
                        {
                            obj = new VMSDevice();

                            obj.DeviceNo = dr["DeviceNo"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["DeviceNo"]);
                            obj.IPAddress = dr["IPAddress"] == DBNull.Value ? "" : Convert.ToString(dr["IPAddress"]);
                            obj.Password = dr["Password"] == DBNull.Value ? "" : Convert.ToString(dr["Password"]);
                            obj.UDPPort = dr["PortNo"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["PortNo"]);
                            obj.SCL2008 = dr["IsSCL2008"] == DBNull.Value ? false : Convert.ToBoolean(dr["IsSCL2008"]);
                            obj.LedHeight = dr["LEDHeight"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["LEDHeight"]);
                            obj.LedWidth = dr["LEDWidth"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["LEDWidth"]);
                            obj.Location = dr["Location"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Location"]);

                            obj.AscFont = dr["ASCFont"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["ASCFont"]);
                            obj.TextColor = dr["TextColor"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["TextColor"]);
                            obj.TextStartPosition = dr["TextStartPosition"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["TextStartPosition"]);
                            obj.TextLength = dr["TextLength"] == DBNull.Value ? Convert.ToInt16(0) : Convert.ToInt16(dr["TextLength"]);

                            obj.MessageTimeOut = dr["MessageTimeOut"] == DBNull.Value ? 1 : Convert.ToInt32(dr["MessageTimeOut"]);



                            obj.DefaultMessage = dr["DefaultMessage"] == DBNull.Value ? "" : Convert.ToString(dr["DefaultMessage"]);
#warning Need to set default message at all places
                            //if (!string.IsNullOrEmpty(obj.DefaultMessage))
                            //    AussportVMS.DefaultMessage = obj.DefaultMessage;

                            lstVMSDevice.Add(obj);

                        }
                        catch (Exception ex)
                        {
                            WriteLogs("Update VMS Settings : " + ex.Message);
                        }
                    }

                    lock (BLUtility.VMSListLock)
                    {
                        if (BLUtility.VMSDeviceList.ContainsKey(loc))
                        {
                            BLUtility.VMSDeviceList[loc] = lstVMSDevice;
                        }
                        else
                        {
                            BLUtility.VMSDeviceList.Add(loc, lstVMSDevice);
                        }

                    }
                }//end of else
            }
            catch (Exception ex)
            {
            }

        }//end of method

        #endregion


        #region "StartUpShutDown"

        // modified version 22/11/2010
        //public static void ___Startup_ShutDown(int HopperID, string HopperNo, Location checkPoint)
        //{
        //    try
        //    {
        //        Hopper objHopper;

        //        if (BLUtility.HoppersInFacility.ContainsKey(HopperNo))
        //        {
        //            objHopper = BLUtility.HoppersInFacility[HopperNo];
        //        }
        //        else
        //        {
        //            return;
        //        }

        //        if (objHopper == null)
        //        {
        //            return;
        //        }

        //        lock (objHopper.lockStartupShutDown)
        //        {
        //            byte padID = Convert.ToByte(HopperNo);
        //            Int32 atTime = 0;

        //            if (checkPoint == Location.TRUCKWASH || checkPoint == Location.EXITGATE)
        //            {
        //                // Called for Shutdown

        //                int SUSD = ExpectedValues.StartUp + ExpectedValues.ShutDown;

        //                List<IActiveReads> objActiveReads_Lst;
        //                IEnumerable<IActiveReads> finalList;

        //                objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);
        //                // no truck in facility for this Hopper
        //                if (objActiveReads_Lst == null)
        //                {
        //                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                    return;
        //                }

        //                objActiveReads_Lst.OrderBy(obj => obj.ID);

        //                finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition == Convert.ToInt32(Location.HOPPER)) select obj1);
        //                if (finalList.Count() > 0)
        //                {
        //                    return;
        //                }
        //                else
        //                {
        //                    finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition == Convert.ToInt32(Location.ENTRY2)) select obj1);

        //                    if (finalList.Count() > 0)
        //                    {
        //                        int ellapsedTime = Convert.ToInt32(DateTime.Now.Subtract(finalList.First().Entry2).TotalSeconds);

        //                        if ((objHopper.Ent2_Hopper - (ellapsedTime + SUSD)) > 0)
        //                        {
        //                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);

        //                            atTime = objHopper.Ent2_Hopper - (ellapsedTime + SUSD);

        //                            objHopper.timerFlag = true;
        //                            objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                            objHopper.tmrPadStartup.Start();
        //                            objHopper.Location_PadStartSet = Location.ENTRY2;

        //                        }
        //                        else
        //                        {
        //                            return;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition == Convert.ToInt32(Location.ENTRY1)) select obj1);

        //                        if (finalList.Count() > 0)
        //                        {
        //                            int ellapsedTime = Convert.ToInt32(DateTime.Now.Subtract(finalList.First().Entry1).TotalSeconds);

        //                            if ((ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - (ellapsedTime + SUSD)) > 0)
        //                            {
        //                                BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);

        //                                atTime = ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - (ellapsedTime + SUSD);

        //                                objHopper.timerFlag = true;
        //                                objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                                objHopper.tmrPadStartup.Start();
        //                                objHopper.Location_PadStartSet = Location.ENTRY1;

        //                            }
        //                            else
        //                            {
        //                                return;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                        }
        //                    }

        //                    // BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                }
        //                //}

        //                //IEnumerable<IActiveReads> finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition != Convert.ToInt32(Location.EXITGATE) && obj1.LastPosition != Convert.ToInt32(Location.REDS2) && obj1.LastPosition != Convert.ToInt32(Location.TRUCKWASH)) select obj1);                     
        //                //if (finalList.Count() <= 0)
        //                //{
        //                //    // Trun the PAD off (no trucks found for PAD in facility other than location at TruckWash/Exit)
        //                //    if (BLUtility.IsModbusConnected)
        //                //    {
        //                //        BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                //    }
        //                //}


        //            }
        //            else //if (checkPoint == Location.REDS || checkPoint == Location.QUEUE)
        //            {
        //                // Called to start UP (setting timer)

        //                if (!objHopper.timerFlag) // Check if timer is already set
        //                {
        //                    if (checkPoint == Location.REDS)
        //                    {
        //                        atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                    }
        //                    else if (checkPoint == Location.QUEUE)
        //                    {
        //                        atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                    }
        //                    else if (checkPoint == Location.ENTRY1)
        //                    {
        //                        atTime = ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                    }
        //                    else if (checkPoint == Location.ENTRY2)
        //                    {
        //                        atTime = objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                    }
        //                    else if (checkPoint == Location.HOPPER)
        //                    {
        //                        atTime = 0;
        //                    }

        //                    if (atTime <= 0)
        //                    {
        //                        atTime = 1;
        //                    }

        //                    objHopper.timerFlag = true;
        //                    objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                    objHopper.tmrPadStartup.Start();
        //                    objHopper.Location_PadStartSet = checkPoint;
        //                }
        //                // ------------Changes made on 6-4-2011 ( to reset pad startup timer when reading truck at entry gates )---------
        //                else if (checkPoint == Location.ENTRY1)
        //                {
        //                    // check for the last checkpoint where the timer was set.
        //                    if (objHopper.Location_PadStartSet == Location.HOPPER || (Convert.ToInt32(objHopper.Location_PadStartSet) < Convert.ToInt32(checkPoint)))
        //                    {
        //                        objHopper.tmrPadStartup.Stop();
        //                        objHopper.timerFlag = true;
        //                        atTime = ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                        if (atTime <= 0)
        //                        {
        //                            atTime = 1;
        //                        }
        //                        objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                        objHopper.tmrPadStartup.Start();
        //                        objHopper.Location_PadStartSet = checkPoint;

        //                    }

        //                }
        //                else if (checkPoint == Location.ENTRY2) // timer can be reset only at entry gates.
        //                {
        //                    // check for the last checkpoint where the timer was set.
        //                    if (objHopper.Location_PadStartSet != Location.HOPPER || (Convert.ToInt32(objHopper.Location_PadStartSet) < Convert.ToInt32(checkPoint)))
        //                    {
        //                        objHopper.tmrPadStartup.Stop();
        //                        atTime = objHopper.Ent2_Hopper - ExpectedValues.StartUp;
        //                        objHopper.timerFlag = true;
        //                        if (atTime <= 0)
        //                        {
        //                            atTime = 1;
        //                        }
        //                        objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                        objHopper.tmrPadStartup.Start();
        //                        objHopper.Location_PadStartSet = checkPoint;

        //                    }
        //                }
        //                // --------------------------------------------------------------------------------------------------------------

        //            }
        //            //else if (checkPoint == Location.ENTRY1 || checkPoint == Location.ENTRY2) 
        //            //{
        //            //    // Called to start UP (immmediately)
        //            //    atTime = 1;
        //            //    objHopper.tmrPadStartup.Stop();
        //            //    objHopper.timerFlag = true;
        //            //    objHopper.tmrPadStartup.Interval = atTime * 1000;
        //            //    objHopper.tmrPadStartup.Start();
        //            //}

        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        // modified version 05/05/2011
        public static void Startup_ShutDown(int HopperID, string HopperNo, Location checkPoint)
        {
#if DEBUG
            Algorithms.checkPoint = "PadSS: Entered: 2315";
 
            log.Debug("Entered Startup_ShutDown method for {0} hopper for {1} location. CheckPoint {2}. Thread :{3} ", HopperNo, checkPoint.ToString(), Algorithms.checkPoint,
                Thread.CurrentThread.ManagedThreadId.ToString());

 
#endif
            try
            {
                Hopper objHopper;

                if (BLUtility.HoppersInFacility.ContainsKey(HopperNo))
                {
                    objHopper = BLUtility.HoppersInFacility[HopperNo];
                }
                else
                {
#if DEBUG
                    Algorithms.checkPoint = "PadSS: Ret hopper doesn't exist: 2329";

#endif
                    return;
                }

                if (objHopper == null)
                {
#if DEBUG
                    Algorithms.checkPoint = "PadSS: Ret hopper null: 2338";

#endif
                    return;
                }

                lock (objHopper.lockStartupShutDown)
                {
                    byte padID = Convert.ToByte(HopperNo);
                    float atTime = 0;

                    int SUSD = ExpectedValues.StartUp + ExpectedValues.ShutDown;

                    List<IActiveReads> objActiveReads_Lst = null;
                    IEnumerable<IActiveReads> finalList;

                    // check if PAD is operational

                    if (BLUtility.ModbusProcessor.ReadPAD_Status(padID) == 0) // if operational
                    {

                        objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);
                        // no truck in facility for this Hopper
                        if (objActiveReads_Lst == null || objActiveReads_Lst.Count <= 0)
                        {
                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
                            return;
                        }

                        objActiveReads_Lst.OrderBy(obj => obj.ID);

                        // check if the PAD is already On

                        if (BLUtility.ModbusProcessor.ReadPAD_Start(padID) == 0) // PAD is not started
                        {
                            if (checkPoint == Location.TRUCKWASH || checkPoint == Location.EXITGATE)
                            {
                                if (!BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == HopperID && x.Value.LastPositiion != Location.TRUCKWASH && x.Value.LastPositiion != Location.EXITGATE))
                                {
                                    if (objHopper.tmrPadStartup != null)
                                        objHopper.tmrPadStartup.Enabled = false;
#if DEBUG
                                    Algorithms.checkPoint = "PadSS: No truck for this pad: 2380";

#endif
                                    return;
                                }


                            }
                            // check any truck with last position as Hopper
                            finalList = from objTruckForHopper in objActiveReads_Lst
                                        where objTruckForHopper.LastPosition == Convert.ToInt32(Location.HOPPER)
                                        select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                //Changed: 26-5-2012 : All trucks will be right trucks if their assigned pad is same as current pad
                                //if ((BLUtility.TrucksInTransaction.Count(x => x.Value.LastPositiion == Location.HOPPER &&
                                //                                          x.Value.FoundAtHopperID == HopperID) > 1) || (BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                //                                          x.Value.FoundAtHopperID == HopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID)))

                                if ((BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                                                          x.Value.FoundAtHopperID == HopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID)))
                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
                                else
                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 1);

                                objHopper.Location_PadStartSet = Location.UnKnown;
#if DEBUG
                                Algorithms.checkPoint = "PadSS: found T at H: 2408";

#endif
                                return;
                            }

                            // check any truck with last position as Entry2
                            finalList = from objTruckForHopper in objActiveReads_Lst
                                        where objTruckForHopper.LastPosition == Convert.ToInt32(Location.ENTRY2)
                                        select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = objHopper.Ent2_Hopper - ExpectedValues.StartUp;

                                if (objHopper.Location_PadStartSet == Location.ENTRY2)
                                {
                                    finalList.OrderByDescending(obj => obj.Entry2);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Entry2).TotalSeconds));
                                }

                                if (atTime <= 0)
                                {
                                    atTime = 0.2f;
                                }
                                objHopper.tmrPadStartup.Stop();
                                objHopper.Location_PadStartSet = Location.ENTRY2;
                                objHopper.timerFlag = true;
                                objHopper.tmrPadStartup.Interval = atTime * 1000;
                                objHopper.tmrPadStartup.Start();
#if DEBUG
                                Algorithms.checkPoint = "PadSS: Set H tmr E2: 2440";

#endif
                                return;
                            }

                            // check any truck with last position as Entry1
                            finalList = from objTruckForHopper in objActiveReads_Lst
                                        where objTruckForHopper.LastPosition == Convert.ToInt32(Location.ENTRY1)
                                        select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;

                                if (objHopper.Location_PadStartSet == Location.ENTRY1)
                                {
                                    finalList.OrderByDescending(obj => obj.Entry1);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Entry1).TotalSeconds));
                                }

                                if (atTime <= 0)
                                {
                                    atTime = 0.2f;
                                }
                                objHopper.tmrPadStartup.Stop();
                                objHopper.Location_PadStartSet = Location.ENTRY1;
                                objHopper.timerFlag = true;
                                objHopper.tmrPadStartup.Interval = atTime * 1000;
                                objHopper.tmrPadStartup.Start();
#if DEBUG
                                Algorithms.checkPoint = "PadSS: set H tmr E1: 2472";

#endif
                                return;
                            }

                            // check any truck with last position as Queue
                            //finalList = from objTruckForHopper in objActiveReads_Lst where objTruckForHopper.LastPosition == Convert.ToInt32(Location.QUEUE) select objTruckForHopper;

                            //if (finalList.Count() > 0)
                            //{
                            //    atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;

                            //    if (objHopper.Location_PadStartSet == Location.QUEUE)
                            //    {
                            //        finalList.OrderByDescending(obj => obj.Queue);

                            //        atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Queue).TotalSeconds));
                            //    }

                            //    if (atTime <= 0)
                            //    {
                            //        atTime = 0.2f;
                            //    }
                            //    objHopper.tmrPadStartup.Stop();
                            //    objHopper.Location_PadStartSet = Location.QUEUE;
                            //    objHopper.timerFlag = true;
                            //    objHopper.tmrPadStartup.Interval = atTime * 1000;
                            //    objHopper.tmrPadStartup.Start();

                            //    return;
                            //}

                            // check any truck with last position as REDS
                            finalList = from objTruckForHopper in objActiveReads_Lst
                                        where objTruckForHopper.LastPosition == Convert.ToInt32(Location.REDS)
                                        select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - ExpectedValues.StartUp;

                                if (objHopper.Location_PadStartSet == Location.REDS)
                                {
                                    finalList.OrderByDescending(obj => obj.REDS);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().REDS).TotalSeconds));
                                }

                                if (atTime <= 0)
                                {
                                    atTime = 0.2f;
                                }
                                objHopper.tmrPadStartup.Stop();
                                objHopper.Location_PadStartSet = Location.REDS;
                                objHopper.timerFlag = true;
                                objHopper.tmrPadStartup.Interval = atTime * 1000;
                                objHopper.tmrPadStartup.Start();
#if DEBUG
                                Algorithms.checkPoint = "PadSS: set H tmr REDS: 2531";

#endif
                                return;
                            }


                        }
                        else // PAD is on
                        {
                            if (checkPoint == Location.TRUCKWASH || checkPoint == Location.EXITGATE)
                            {
                                // check if there are any truck in facility at location other than TW and EXIT 
                                // finalList = from objTruckForHopper in objActiveReads_Lst where (objTruckForHopper.LastPosition != Convert.ToInt32(Location.TRUCKWASH) || objTruckForHopper.LastPosition != Convert.ToInt32(Location.EXITGATE)) select objTruckForHopper;

                                // ( modification on 17/5/2011 {} )

                                if (!BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == HopperID
                                    && x.Value.LastPositiion != Location.TRUCKWASH && x.Value.LastPositiion != Location.EXITGATE
                                    && x.Value.LastPositiion != Location.QUEUE))
                                {
                                    if (objHopper.tmrPadStartup != null)
                                        objHopper.tmrPadStartup.Enabled = false;

#if DEBUG
                                    Algorithms.checkPoint = "PadSS: Ret No truck for H: 2315";

#endif
                                    return;
                                }
                                //finalList = from objTruckForHopper in objActiveReads_Lst
                                //            where (objTruckForHopper.LastPosition != Convert.ToInt32(Location.TRUCKWASH)
                                //                && objTruckForHopper.LastPosition != Convert.ToInt32(Location.EXITGATE)
                                //                && objTruckForHopper.LastPosition != Convert.ToInt32(Location.QUEUE)
                                //                //&& objTruckForHopper.LastPosition != Convert.ToInt32(Location.IssueTagOffice)
                                //                //&& objTruckForHopper.LastPosition != Convert.ToInt32(Location.REDS2)
                                //                //&& objTruckForHopper.LastPosition != Convert.ToInt32(Location.ControlRoom)
                                //                )
                                //            select objTruckForHopper;

                                //if (finalList.Count() <= 0) // found no trucks 
                                //{
                                //    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
                                //    objHopper.Location_PadStartSet = Location.UnKnown;
                                //    if (objHopper.tmrPadStartup != null && objHopper.tmrPadStartup.Enabled)
                                //        objHopper.tmrPadStartup.Stop();
                                //    return;
                                //}

                            }

                            SUSD = ExpectedValues.StartUp + ExpectedValues.ShutDown;
                            // check any truck with last position as Hopper
                            finalList = from objTruckForHopper in objActiveReads_Lst
                                        where objTruckForHopper.LastPosition == Convert.ToInt32(Location.HOPPER)
                                        select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                //Changed: 26-5-2012 : All trucks will be right trucks if their assigned pad is same as current pad
                                //if ((BLUtility.TrucksInTransaction.Count(x => x.Value.LastPositiion == Location.HOPPER &&
                                //                                          x.Value.FoundAtHopperID == HopperID) > 1) || (BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                //                                          x.Value.FoundAtHopperID == HopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID)))

                                if ((BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                                                          x.Value.FoundAtHopperID == HopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID)))
                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
                                else
                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 1);

                                objHopper.Location_PadStartSet = Location.UnKnown;
#if DEBUG
                                Algorithms.checkPoint = "PadSS: found T at H start H: 2315";

#endif
                                return;
                            }

                            // check any truck with last position as Entry2
                            finalList = from objTruckForHopper in objActiveReads_Lst where objTruckForHopper.LastPosition == Convert.ToInt32(Location.ENTRY2) select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = objHopper.Ent2_Hopper - SUSD;
                                if (objHopper.Location_PadStartSet == Location.ENTRY2)
                                {
                                    finalList.OrderBy(obj => obj.Entry2);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Entry2).TotalSeconds));
                                }
                                if (atTime >= ExpectedValues.Precision)
                                {
                                    if (objHopper.tmrPadStartup != null && objHopper.tmrPadStartup.Enabled)
                                        objHopper.tmrPadStartup.Stop();

                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0); // set immediate PAD stop

                                    // set the new timer
                                    objHopper.Location_PadStartSet = Location.ENTRY2;
                                    objHopper.timerFlag = true;
                                    objHopper.tmrPadStartup.Interval = atTime * 1000;
                                    objHopper.tmrPadStartup.Start();
#if DEBUG
                                    Algorithms.checkPoint = "PadSS: set H tmr E2: 2634";

#endif
                                }

                                return;
                            }

                            // check any truck with last position as Entry1
                            finalList = from objTruckForHopper in objActiveReads_Lst where objTruckForHopper.LastPosition == Convert.ToInt32(Location.ENTRY1) select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = (ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper) - SUSD;
                                if (objHopper.Location_PadStartSet == Location.ENTRY1)
                                {
                                    finalList.OrderBy(obj => obj.Entry1);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Entry1).TotalSeconds));
                                }
                                if (atTime >= ExpectedValues.Precision)
                                {
                                    if (objHopper.tmrPadStartup != null && objHopper.tmrPadStartup.Enabled)
                                        objHopper.tmrPadStartup.Stop();

                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0); // set immediate PAD stop

                                    // set the new timer
                                    objHopper.Location_PadStartSet = Location.ENTRY1;
                                    objHopper.timerFlag = true;
                                    objHopper.tmrPadStartup.Interval = atTime * 1000;
                                    objHopper.tmrPadStartup.Start();
#if DEBUG
                                    Algorithms.checkPoint = "PadSS: set H tmr E1: 2667";

#endif
                                }

                                return;
                            }

                            // check any truck with last position as Queue
                            //finalList = from objTruckForHopper in objActiveReads_Lst where objTruckForHopper.LastPosition == Convert.ToInt32(Location.QUEUE) select objTruckForHopper;

                            //if (finalList.Count() > 0)
                            //{
                            //    atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper - SUSD;

                            //    if (objHopper.Location_PadStartSet == Location.QUEUE)
                            //    {
                            //        finalList.OrderBy(obj => obj.Queue);

                            //        atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().Queue).TotalSeconds));
                            //    }

                            //    if (atTime >= ExpectedValues.Precision)
                            //    {
                            //        objHopper.tmrPadStartup.Stop();

                            //        BLUtility.ModbusProcessor.WritePAD_Start(padID, 0); // set immediate PAD stop

                            //        // set the new timer
                            //        objHopper.Location_PadStartSet = Location.QUEUE;
                            //        objHopper.timerFlag = true;
                            //        objHopper.tmrPadStartup.Interval = atTime * 1000;
                            //        objHopper.tmrPadStartup.Start();
                            //    }
                            //    return;
                            //}

                            // check any truck with last position as REDS
                            finalList = from objTruckForHopper in objActiveReads_Lst where objTruckForHopper.LastPosition == Convert.ToInt32(Location.REDS) select objTruckForHopper;

                            if (finalList.Count() > 0)
                            {
                                atTime = (ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper) - SUSD;
                                if (objHopper.Location_PadStartSet == Location.REDS)
                                {
                                    finalList.OrderBy(obj => obj.REDS);

                                    atTime = atTime - Convert.ToInt32((DateTime.Now.Subtract(finalList.First().REDS).TotalSeconds));
                                }
                                if (atTime >= ExpectedValues.Precision)
                                {
                                    if (objHopper.tmrPadStartup != null && objHopper.tmrPadStartup.Enabled)
                                        objHopper.tmrPadStartup.Stop();

                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0); // set immediate PAD stop

                                    // set the new timer
                                    objHopper.Location_PadStartSet = Location.REDS;
                                    objHopper.timerFlag = true;
                                    objHopper.tmrPadStartup.Interval = atTime * 1000;
                                    objHopper.tmrPadStartup.Start();
#if DEBUG
                                    Algorithms.checkPoint = "PadSS: set H tmr REDS: 2729";

#endif
                                }
                                return;
                            }

                        }

                    }
                    else
                    {

                        if (objHopper.tmrPadStartup != null && objHopper.tmrPadStartup.Enabled)
                            objHopper.tmrPadStartup.Stop();
                        BLUtility.ModbusProcessor.WritePAD_Start(padID, 0); // stop the pad.

                        if (objHopper.HopperAvailable)
                            BLUtility.UpdateHopperStatus();
                    }

                }

            }
            catch (Exception ex)
            {
#if DEBUG
                log.Error("Exception thrown in Startup_Shutdown for " + HopperID.ToString() + " error: " + ex.Message, ex);
#endif
            }
            finally
            {
#if DEBUG
                log.Debug("Finished Startup_ShutDown method for {0} hopper for {1} location. CheckPoint {2}. Thread :{3} ", HopperNo, checkPoint.ToString(), Algorithms.checkPoint,
                    Thread.CurrentThread.ManagedThreadId.ToString());

#endif
            }
        }

        //// ORIGINAL (not in use 22/11/2010) 
        //public static void StartUpShutDown(int HopperID, string HopperNo)
        //{

        //    // Modbus.Connector.Processor objProcessor = null;

        //    try
        //    {

        //        Hopper objHopper;

        //        if (BLUtility.HoppersInFacility.ContainsKey(HopperNo))
        //        {
        //            objHopper = BLUtility.HoppersInFacility[HopperNo];
        //        }
        //        else
        //        {
        //            return;
        //        }

        //        if (objHopper == null)
        //        {
        //            return;
        //        }

        //        lock (objHopper.lockStartupShutDown)
        //        {

        //            if (objHopper.timerFlag)
        //            {
        //                return;
        //            }

        //            Int32 E2Hx = objHopper.Ent2_Hopper;
        //            Int32 HxTW = objHopper.Hopper_TW;

        //            Int32 atTime = 0;
        //            Int32 padStatus = 0;
        //            byte statusModbus;

        //            byte padID = Convert.ToByte(HopperNo);


        //            if (BLUtility.IsModbusConnected)
        //            {
        //                padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(padID);
        //            }
        //            else
        //            {
        //                BLUtility.StartModbusServer();
        //                if (BLUtility.IsModbusConnected)
        //                {
        //                    padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(padID);
        //                }
        //            }


        //            if (padStatus == 0)  // Operational
        //            {

        //                List<IActiveReads> objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);
        //                objActiveReads_Lst.OrderBy(obj => obj.ID);

        //                IEnumerable<IActiveReads> finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition != Convert.ToInt32(Location.EXITGATE) && obj1.LastPosition != Convert.ToInt32(Location.REDS2) && obj1.LastPosition != Convert.ToInt32(Location.TRUCKWASH)) select obj1);


        //                if (finalList.Count() <= 0)
        //                {
        //                    // OFF
        //                    atTime = 0;
        //                    // objProcessor.WritePAD_Start(padID, 0);
        //                    if (BLUtility.IsModbusConnected)
        //                    {
        //                        padStatus = BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                    }
        //                }
        //                else
        //                {
        //                    short isHopperOn = 0;

        //                    if (BLUtility.IsModbusConnected)
        //                    {
        //                        isHopperOn = BLUtility.ModbusProcessor.ReadPAD_Start(padID);
        //                    }

        //                    if (isHopperOn == 0)    // MODBUS   (Hopper is OFF)
        //                    {
        //                        // HOPPERX
        //                        IEnumerable<IActiveReads> arrHOPPERX = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.HOPPER) select obj1);
        //                        if (arrHOPPERX.Count() > 0)
        //                        {
        //                            atTime = 0;
        //                            // ON                                
        //                        }
        //                        else
        //                        {
        //                            // ENTRY2
        //                            IEnumerable<IActiveReads> arrENTRY2 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY2) select obj1);
        //                            if (arrENTRY2.Count() > 0)
        //                            {
        //                                atTime = E2Hx - ExpectedValues.StartUp;
        //                                // ON
        //                            }
        //                            else
        //                            {
        //                                // ENTRY1
        //                                IEnumerable<IActiveReads> arrENTRY1 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY1) select obj1);
        //                                if (arrENTRY1.Count() > 0)
        //                                {
        //                                    atTime = ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                    // ON
        //                                }
        //                                else
        //                                {
        //                                    // QUEUE
        //                                    IEnumerable<IActiveReads> arrQUEUE = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.QUEUE) select obj1);
        //                                    if (arrQUEUE.Count() > 0)
        //                                    {
        //                                        atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                        // ON
        //                                    }
        //                                    else
        //                                    {
        //                                        // REDS
        //                                        IEnumerable<IActiveReads> arrREDS = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.REDS) select obj1);
        //                                        if (arrREDS.Count() > 0)
        //                                        {
        //                                            atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                            // ON
        //                                        }
        //                                        else
        //                                        {
        //                                            //
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                    }
        //                    else if (isHopperOn == 1) // MODBUS   (Hopper is ON)
        //                    {

        //                        // HOPPERX
        //                        IEnumerable<IActiveReads> arrHOPPERX = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.HOPPER) select obj1);
        //                        if (arrHOPPERX.Count() > 0)
        //                        {
        //                            atTime = 0;
        //                            // ON
        //                        }
        //                        else
        //                        {
        //                            Int32 SUSD = ExpectedValues.StartUp + ExpectedValues.ShutDown;

        //                            // ENTRY2
        //                            IEnumerable<IActiveReads> arrENTRY2 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY2) select obj1);
        //                            if (arrENTRY2.Count() > 0)
        //                            {
        //                                if ((E2Hx - SUSD) > 0)
        //                                {
        //                                    atTime = 0;
        //                                    // OFF
        //                                    // objProcessor.WritePAD_Start(padID, 0);
        //                                    if (BLUtility.IsModbusConnected)
        //                                    {
        //                                        BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                                    }
        //                                    atTime = E2Hx - ExpectedValues.StartUp;
        //                                    // ON
        //                                }
        //                                else
        //                                {
        //                                    atTime = 0;
        //                                    // ON
        //                                }
        //                            }
        //                            else
        //                            {

        //                                // ENTRY1
        //                                IEnumerable<IActiveReads> arrENTRY1 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY1) select obj1);
        //                                if (arrENTRY1.Count() > 0)
        //                                {
        //                                    if ((ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                    {
        //                                        atTime = 0;
        //                                        // OFF
        //                                        // objProcessor.WritePAD_Start(padID, 0);
        //                                        if (BLUtility.IsModbusConnected)
        //                                        {
        //                                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                                        }

        //                                        atTime = ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                        // ON
        //                                    }
        //                                    else
        //                                    {
        //                                        atTime = 0;
        //                                        // ON
        //                                    }
        //                                }
        //                                else
        //                                {

        //                                    // QUEUE
        //                                    IEnumerable<IActiveReads> arrQUEUE = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.QUEUE) select obj1);
        //                                    if (arrQUEUE.Count() > 0)
        //                                    {
        //                                        if ((ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                        {
        //                                            atTime = 0;
        //                                            // OFF
        //                                            // objProcessor.WritePAD_Start(padID, 0);
        //                                            if (BLUtility.IsModbusConnected)
        //                                            {
        //                                                BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                                            }

        //                                            atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                            // ON
        //                                        }
        //                                        else
        //                                        {
        //                                            atTime = 0;
        //                                            // ON
        //                                        }
        //                                    }
        //                                    else
        //                                    {

        //                                        // REDS
        //                                        IEnumerable<IActiveReads> arrREDS = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.REDS) select obj1);
        //                                        if (arrREDS.Count() > 0)
        //                                        {
        //                                            if ((ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                            {
        //                                                atTime = 0;
        //                                                // OFF
        //                                                // objProcessor.WritePAD_Start(padID, 0);
        //                                                if (BLUtility.IsModbusConnected)
        //                                                {
        //                                                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                                                }

        //                                                atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                                // ON
        //                                            }
        //                                            else
        //                                            {
        //                                                atTime = 0;
        //                                                // ON
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            //
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }

        //                    if (atTime > 0)
        //                    {
        //                        objHopper.timerFlag = true;
        //                        // After Changes made on 29th Oct 2010
        //                        objHopper.tmrPadStartup.Interval = atTime * 1000;
        //                        objHopper.tmrPadStartup.Start();

        //                        // Before Changes made on 29th Oct 2010
        //                        //objHopper.tmrPadStartup.Interval = atTime;
        //                        //objHopper.StartTimer();

        //                    }
        //                    else
        //                    {
        //                        // objProcessor.WritePAD_Start(padID, 1);
        //                        if (BLUtility.IsModbusConnected)
        //                        {
        //                            // After Changes made on 29th Oct 2010
        //                            BLUtility.ModbusProcessor.WritePAD_Start(padID, 1);

        //                            // Before Changes made on 29th Oct 2010
        //                            //BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                        }
        //                    }
        //                }
        //            }
        //            else  // Not Operational
        //            {
        //                // OFF
        //                atTime = 0;
        //                // objProcessor.WritePAD_Start(padID, 0);
        //                if (BLUtility.IsModbusConnected)
        //                {
        //                    BLUtility.ModbusProcessor.WritePAD_Start(padID, 0);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public static string StartUpShutDown(int HopperID, int OperationalVar, int OnVar)
        //{
        //    try
        //    {
        //        // IHoppers objHopper;
        //        //objHopper = Hoppers.GetHopperDetailsByHopperID(HopperID);

        //        string HopperName = String.Empty;

        //        Hopper objHopper;

        //        if (BLUtility.HoppersInFacility.ContainsKey(HopperName))
        //        {
        //            objHopper = BLUtility.HoppersInFacility[HopperName];
        //        }
        //        else
        //        {
        //            return "";
        //        }

        //        if (objHopper == null)
        //        {
        //            return "";
        //        }

        //        Int32 E2Hx = objHopper.Ent2_Hopper;
        //        Int32 HxTW = objHopper.Hopper_TW;


        //        Int32 atTime = 0;
        //        //  SYNTAX for MODBUS method
        //        //  public int IsHopperOperational(Int32 HopperID)
        //        //  Check is hopper operational?  (MODBUS will provide this information)
        //        //Int32 result=IsHopperOperational(Int32 HopperID);

        //        string result = String.Empty;

        //        if (OperationalVar == 0)  // Operational
        //        {

        //            List<IActiveReads> objActiveReads_Lst = DBAdapters.ActiveReads.GetAllRecordsByHopperID(HopperID);
        //            objActiveReads_Lst.OrderBy(obj => obj.ID);

        //            IEnumerable<IActiveReads> finalList = (from obj1 in objActiveReads_Lst where (obj1.LastPosition != Convert.ToInt32(Location.EXITGATE) && obj1.LastPosition != Convert.ToInt32(Location.REDS2) && obj1.LastPosition != Convert.ToInt32(Location.TRUCKWASH)) select obj1);

        //            if (finalList.Count() <= 0)
        //            {
        //                // OFF
        //                atTime = 0;
        //                result = "SHUT DOWN after time :" + atTime.ToString();

        //            }
        //            else
        //            {
        //                if (OnVar == 0)    // MODBUS   (Hopper is OFF)
        //                {
        //                    // HOPPERX
        //                    IEnumerable<IActiveReads> arrHOPPERX = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.HOPPER) select obj1);
        //                    if (arrHOPPERX.Count() > 0)
        //                    {
        //                        atTime = 0;
        //                        // ON
        //                        result = "START UP  after time :" + atTime.ToString();
        //                    }
        //                    else
        //                    {
        //                        // ENTRY2
        //                        IEnumerable<IActiveReads> arrENTRY2 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY2) select obj1);
        //                        if (arrENTRY2.Count() > 0)
        //                        {
        //                            atTime = E2Hx - ExpectedValues.StartUp;
        //                            // ON
        //                            result = "START UP  after time :" + atTime.ToString();
        //                        }
        //                        else
        //                        {
        //                            // ENTRY1
        //                            IEnumerable<IActiveReads> arrENTRY1 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY1) select obj1);
        //                            if (arrENTRY1.Count() > 0)
        //                            {
        //                                atTime = ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                // ON
        //                                result = "START UP  after time :" + atTime.ToString();
        //                            }
        //                            else
        //                            {
        //                                // QUEUE
        //                                IEnumerable<IActiveReads> arrQUEUE = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.QUEUE) select obj1);
        //                                if (arrQUEUE.Count() > 0)
        //                                {
        //                                    atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                    // ON
        //                                    result = "START UP  after time :" + atTime.ToString();
        //                                }
        //                                else
        //                                {
        //                                    // REDS
        //                                    IEnumerable<IActiveReads> arrREDS = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.REDS) select obj1);
        //                                    if (arrREDS.Count() > 0)
        //                                    {
        //                                        atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                        // ON
        //                                        result = "START UP  after time :" + atTime.ToString();
        //                                    }
        //                                    else
        //                                    {
        //                                        //
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }

        //                }
        //                else  // MODBUS   (Hopper is ON)
        //                {

        //                    // HOPPERX
        //                    IEnumerable<IActiveReads> arrHOPPERX = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.HOPPER) select obj1);
        //                    if (arrHOPPERX.Count() > 0)
        //                    {
        //                        atTime = 0;
        //                        // ON
        //                        result = "START UP  after time :" + atTime.ToString();
        //                    }
        //                    else
        //                    {
        //                        Int32 SUSD = ExpectedValues.StartUp + ExpectedValues.ShutDown;

        //                        // ENTRY2
        //                        IEnumerable<IActiveReads> arrENTRY2 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY2) select obj1);
        //                        if (arrENTRY2.Count() > 0)
        //                        {
        //                            if ((E2Hx - SUSD) > 0)
        //                            {
        //                                atTime = 0;
        //                                // OFF
        //                                result = "SHUT DOWN after time :" + atTime.ToString();

        //                                atTime = E2Hx - ExpectedValues.StartUp;
        //                                // ON
        //                                result += "START UP  after time :" + atTime.ToString();
        //                            }
        //                            else
        //                            {
        //                                atTime = 0;
        //                                // ON
        //                                result = "START UP  after time :" + atTime.ToString();
        //                            }
        //                        }
        //                        else
        //                        {

        //                            // ENTRY1
        //                            IEnumerable<IActiveReads> arrENTRY1 = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.ENTRY1) select obj1);
        //                            if (arrENTRY1.Count() > 0)
        //                            {
        //                                if ((ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                {
        //                                    atTime = 0;
        //                                    // OFF
        //                                    result = "SHUT DOWN after time :" + atTime.ToString();

        //                                    atTime = ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                    // ON
        //                                    result += "START UP  after time :" + atTime.ToString();
        //                                }
        //                                else
        //                                {
        //                                    atTime = 0;
        //                                    // ON
        //                                    result = "START UP  after time :" + atTime.ToString();
        //                                }
        //                            }
        //                            else
        //                            {

        //                                // QUEUE
        //                                IEnumerable<IActiveReads> arrQUEUE = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.QUEUE) select obj1);
        //                                if (arrQUEUE.Count() > 0)
        //                                {
        //                                    if ((ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                    {
        //                                        atTime = 0;
        //                                        // OFF
        //                                        result = "SHUT DOWN after time :" + atTime.ToString();

        //                                        atTime = ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                        // ON
        //                                        result += "START UP  after time :" + atTime.ToString();
        //                                    }
        //                                    else
        //                                    {
        //                                        atTime = 0;
        //                                        // ON
        //                                        result = "START UP  after time :" + atTime.ToString();
        //                                    }
        //                                }
        //                                else
        //                                {

        //                                    // REDS
        //                                    IEnumerable<IActiveReads> arrREDS = (from obj1 in finalList where obj1.LastPosition == Convert.ToInt32(Location.REDS) select obj1);
        //                                    if (arrREDS.Count() > 0)
        //                                    {
        //                                        if ((ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - SUSD) > 0)
        //                                        {
        //                                            atTime = 0;
        //                                            // OFF
        //                                            result = "SHUT DOWN after time :" + atTime.ToString();

        //                                            atTime = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + E2Hx - ExpectedValues.StartUp;
        //                                            // ON
        //                                            result += "START UP  after time :" + atTime.ToString();
        //                                        }
        //                                        else
        //                                        {
        //                                            atTime = 0;
        //                                            // ON
        //                                            result = "START UP  after time :" + atTime.ToString();
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        //
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else  // Not Operational
        //        {
        //            // OFF
        //            atTime = 0;
        //            result = "SHUT DOWN after time :" + atTime.ToString();
        //        }

        //        return result;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion


         


        #region "Traffic Transaction Open Close"

        public static void TrafficTransactionOpenClose(RFIDTag TagRFID, Location loc)
        {
            //31-10-2011:Bhavesh
            //logger.Trace("Called TrafficTransactionOpenClose at " + loc.ToString() + " for " + TagRFID.TagID);
#if DEBUG
            checkPoint = "TraTraOC:Entered";
            truckChanged = false;
            log.Debug("Entered into TrafficTransaction Open Close method with {0} tag at {1} location. CheckPoint: {2} Thread {3}", TagRFID.TagID, loc.ToString(),checkPoint, Thread.CurrentThread.ManagedThreadId.ToString());
#endif


            try
            {

                

                lock (BLUtility.truckListLock)
                {
                    string NewLoopID = "";
                    string OldLoopID = "";
                    int timerAdjust = 0; //time in second to adjust the hopper/callup timer.
                    bool setTimer = true;

                    NewLoopID = TagRFID.NewMarkerID.ToString();
                    OldLoopID = TagRFID.OldMarkerID.ToString();

                    ITags objITag = null;

                    ITruck objTruck = null;


                    bool newTruck = false;

                    int delayTimer = 0;

                    if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID.TagID))
                    {
                        objTruck = BLUtility.TrucksInTransaction[TagRFID.TagID];
#if DEBUG
                        checkPoint = "TraTraOC: Truck Exist:3321";
                        log.Debug("Truck {0} found in TrucksInTransactionList. Thread MId {1}", TagRFID.TagID,
                            System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                        if (loc == Location.ControlRoom || loc == Location.IssueTagOffice)
                        {
#if DEBUG
                            truckChanged = true;
#endif
                            string newHOrder = "";
#if DEBUG
                            checkPoint = "TraTraOC: Loc is ControlRoom || IssueTagOffice";
                            log.Debug("Truck {0} found in TrucksInTransactionList at {1} location. Calling DB to close transaction. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(),System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
                                
#endif
                            DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, loc, 3, TagRFID.NewMarkerID.ToString(), TagRFID.OldMarkerID.ToString(), objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);

#if DEBUG
 
                            log.Debug("Truck {0} at {1} location after (before updating/checking for HGroupOrder) Calling DB to close transaction. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(),System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
                                 
#endif
                            if (BLUtility.HGroupOrder != newHOrder)
                            {
                                int i = 1;
                                foreach (string order in newHOrder.Split(','))
                                {
                                    try
                                    {
                                        if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                        {
                                            BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                            BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                        }
                                    }
                                    catch { }
                                    i++;
                                }
                                BLUtility.HGroupOrder = newHOrder;
                            }

                            string hopperNo = objTruck.TagDB.DestinationPad.ToString();

#if DEBUG
                             
                            log.Debug("Truck {0} found in TrucksInTransactionList at {1} location. Removing from TrucksInTransList. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            BLUtility.TrucksInTransaction.Remove(TagRFID.TagID);


                            //pad foundat and callup timer clean up 101220111406

                            if (objTruck != null)
                            {
#if DEBUG
                                 
                                log.Debug("Truck {0} found in TrucksInTransactionList at {1} location. Calling HopperStartupCallAdjustOnClose. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                HopperStartupCallupAdjustOnClose(objTruck.TagDB.DestinationPad.ToString(), objTruck.FoundAtHopperID.ToString(), objTruck.TagRFID.TagID);
                            }

                            #region Disabled moved to HopperStartupCallupAdjustOnClose method
                            //if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                            //{
                            //       Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                            //        if (objHopper != null)
                            //        {
                            //            log.Debug("Found Hopper {0} (Hopper Name) and closing it at ControlRoom or IssueTagOffice by calling Startup-Shutdown alg used by {1}", objHopper.HopperName, TagRFID.TagID);
                            //            objHopper.Startup_ShutDownPAD(loc);
                            //            objHopper.RemoveFromQueue(objTruck.TagDB);

                            //        }
                            //}
                            #endregion

                            if (BLUtility.VMSDeviceList.ContainsKey(Location.QUEUE))
                            {
                                foreach (IVMSDevice obj in BLUtility.VMSDeviceList[Location.QUEUE])
                                    obj.RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0') + " Gate", "`C00FF00");

                            }
                            try
                            {
                                if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                                {
                                    BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                                }
                            }
                            catch { }
#if DEBUG
                            checkPoint = "TraTraOC: Ret loc is controlRoom or TagIssue: 3389";
                            log.Debug("Truck {0} found in TrucksInTransactionList at {1} location. Returning from Method. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

 

                            return;
                        }
                    }
                    else if (loc == Location.TRUCKWASH || loc == Location.EXITGATE || loc == Location.ControlRoom || loc == Location.IssueTagOffice)
                    {
                        //update time into TagsTo Issue table : 081202111034

#if DEBUG
                        checkPoint = "TraTraOC: Ret loc is controlRoom, TagIssue, TW or Ex : 3399";
#endif
                        return; // do not start new transation at Truck wash, Exit Gate and Control Room
                    }
                    else
                    {

#if DEBUG
                        checkPoint = "TraTraOC: New Truck: 3406";
                        truckChanged = true;
                        log.Debug("Truck {0} not found in TrucksInTransactionList at {1} location. Calling DB to open transaction. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
 
                        //31-10-2011:Bhavesh
                        string newHOrder = "";
                        objTruck = new Truck(TagRFID, loc);
                        objITag = DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, loc, 1, NewLoopID, OldLoopID, objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);
                        timerAdjust = 0;
#if DEBUG

                        log.Debug("Truck {0} not found in TrucksInTransactionList at {1} location. After opening Transaction in DB (Before updating/checking HGroupOrder. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        if (BLUtility.HGroupOrder != newHOrder)
                        {
                            int i = 1;
                            foreach (string order in newHOrder.Split(','))
                            {
                                try
                                {
                                    if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                    {
                                        BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                        BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                    }
                                }
                                catch { }
                                i++;
                            }
                            BLUtility.HGroupOrder = newHOrder;
                        }

                        if (objITag != null && objITag.ID > 0 && objITag.HopperID > 0) // objITag.HopperID > 0 Check is added on 09-04-2011, to not start transaction if hopper is not assigned
                        {

                            //31-10-2011:Bhavesh
                            objTruck.LastReadTime = DateTime.Now;
                            objTruck.LastPositiion = loc;
                            objTruck.TagDB = objITag;
                            BLUtility.TrucksInTransaction.Add(TagRFID.TagID, objTruck);
                            newTruck = true;
                        }
                        else
                        {
#if DEBUG
                            checkPoint = "TraTraOC: Ret New truck is invlaid: 3445";
                            log.Debug("Truck {0} notfound in TrucksInTransactionList at {1} location. Returning from method as transaction was not created in DB. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
 
                            return;
                            // throw new Exception("Tag not found in database.");
                        }

                    }

                    //Stop response timer if truck has changed state and timer is still running.
                    if (objTruck.LastPositiion != loc && objTruck.tmrCheckResponseTime.Enabled)
                        objTruck.tmrCheckResponseTime.Stop();

#if DEBUG

                    log.Debug("Truck {0} at {1} location. Stopped ResponseTimer if was running. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

                    if (loc == Location.TRUCKWASH || loc == Location.EXITGATE)
                    {
                        delayTimer = ExpectedValues.Max_Delay + BLUtility.GetMaxValue(ExpectedValues.Exit_REDS, ExpectedValues.TruckWash_REDS, ExpectedValues.TruckWash_Ent1, ExpectedValues.Exit_Ent1);
                        objTruck.TransMessage = TransactionMessages._TransactionComplete;
                        objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Complete);

                        try
                        {
                            if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                            {
                                BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                            }
                        }
                        catch { }




                    }
                    else if (loc == Location.REDS)
                    {
                        if (newTruck == true)
                        {
#if DEBUG
                            truckChanged = true;
#endif
                            delayTimer = ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;
                            setTimer = false;
                        }
                        else if (objTruck.EntryOrQueue == Location.ENTRY1)
                        {
                            delayTimer = ExpectedValues.Max_Delay + ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1;

                        }
                        else if (objTruck.EntryOrQueue == Location.QUEUE)
                        {

                            delayTimer = ExpectedValues.REDS_Queue;
                            setTimer = false;
                        }
                        else
                        {
                            delayTimer = ExpectedValues.Max_Delay + BLUtility.GetMaxValue(ExpectedValues.REDS_Queue, ExpectedValues.REDS_Queue + ExpectedValues.Queue_Ent1);
                        }

                    }
                    else if (loc == Location.QUEUE)
                    {
                        //delayTimer = ExpectedValues.Max_Delay + BLUtility.GetMaxValue(ExpectedValues.Queue_Ent1);

                    }
                    else if (loc == Location.ENTRY1)
                    {

                        delayTimer = ExpectedValues.Max_Delay + BLUtility.GetMaxValue(ExpectedValues.Ent1_Ent2);



                        if (objTruck.LastPositiion == Location.TRUCKWASH || objTruck.LastPositiion == Location.EXITGATE)
                        {
                            try
                            {
                                if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                                {
                                    BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                                }

#if DEBUG

                                log.Debug("Truck {0}  at {1} location. Stopping PadStartup if is running. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                if (BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].tmrPadStartup != null && BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].tmrPadStartup.Enabled)
                                    BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].tmrPadStartup.Enabled = false;

                            }
                            catch { }
 
#if DEBUG
 truckChanged = true;
                            log.Debug("Truck {0} at {1} location. Calling DB to close and reopen new transaction at E1 (option=2). Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            string newHOrder = "";
                            objITag = DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, loc, 2, NewLoopID, OldLoopID, objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);
                            timerAdjust = 0;

                            objTruck.TagDB = objITag;

                            objTruck.TransMessage = TransactionMessages._TransactionInComplete;
                            objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Incomplete);
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Reopened new transaction at E1(before checking HGOrder). Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            if (BLUtility.HGroupOrder != newHOrder)
                            {
                                int i = 1;
                                foreach (string order in newHOrder.Split(','))
                                {
                                    try
                                    {
                                        if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                        {
                                            BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                            BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                        }
                                    }
                                    catch { }
                                    i++;
                                }
                                BLUtility.HGroupOrder = newHOrder;
                            }

                            newTruck = true;
                        }

                        //Hashtable htParams = new Hashtable();

                        //htParams.Add("vmsParam", new VMSParameters());
                        ////htParams.Add("dtVMSSettings", dtVMSSettings);
                        //htParams.Add("truckID", objTruck.TagDB.TruckID.PadLeft(4,'0'));
                        //htParams.Add("TagRFID", objTruck.TagRFID.TagID);
                        //htParams.Add("hopperID", objTruck.TagDB.DestinationPad.ToString().PadLeft(2,'0'));
                        //htParams.Add("Location", Location.HOPPER);

                        //TagBlinkNVMSDisplay(htParams);                            
                    }
                    else if (loc == Location.ENTRY2)
                    {
                        //if truck is assign to pad 00. timer will be set for Pad 13
                        if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.DestinationPad.ToString()))
                            delayTimer = ExpectedValues.Max_Delay + BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].Ent2_Hopper;
                        else
                            delayTimer = ExpectedValues.Max_Delay + BLUtility.HoppersInFacility["13"].Ent2_Hopper;
                    }
                    else if (loc == Location.HOPPER)
                    {
                        //if truck is assign to pad 00. timer will be set for Pad 1

                        if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.DestinationPad.ToString()))
                            delayTimer = ExpectedValues.Max_Delay + BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].Hopper_TW;
                        else
                            delayTimer = ExpectedValues.Max_Delay + BLUtility.HoppersInFacility["1"].Hopper_TW;




                    }

#if DEBUG

                    log.Debug("Truck {0}  at {1} location. Finish assigning delayTimer for next location. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

                    //Date 8-12-2011 2:07pm : Changed to include hopper startup shutdown logic for new truck at REDS,Entry1,Entry2
                    #region if is new truck at Entry or REDS
                    if (newTruck == true)
                    {

#if DEBUG

                        log.Debug("Truck {0}  at {1} location is new truck. Check HStatus changed. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        //int callupInteval = 0;
                        Hopper objHopper = null;

                        #region checking hopper status. Changed to update all hopper status in db
                        if (BLUtility.IsModbusConnected)
                        {
                            //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
                            BLUtility.UpdateHopperStatus();
                        }
                        else
                        {
                            BLUtility.StartModbusServer();
                            if (BLUtility.IsModbusConnected)
                            {
                                //padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(HopperID);
                                BLUtility.UpdateHopperStatus();
                            }
                        }
                        #endregion

                        //31-10-2011:Bhavesh
                        //logger.Trace("Calling GateQueue Alg for " + TagRFID.TagID);
                        int desPad = 0;
                        Location desLoc;
                        //if new truck arrives at E1 E2 or PAD - it will be assigned first available pad and delaytimer will be returned 
                        //to adjust call up timer for that pad.

                        //Date: 8-2-2012 : Change TMS will not assign
                        //new pad to a truck at E1 or E2. Pad will be assign at REDS and in Queu only.

#if DEBUG

                        log.Debug("Truck {0}  at {1} location is new truck. Finished checking HopperStatus Changed. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        if (loc == Location.REDS || loc == Location.QUEUE ||
                            BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1)//(loc != Location.HOPPER)
                        {
 
#if DEBUG
                            checkPoint = "TraTraOC: Calling DB EntryOrQueue: 3634";
                            log.Debug("Truck {0}  at {1} location.Calling EntryOrQueue sp in DB. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            DBAdapters.ActiveReads.EntryOrQueue(objTruck.TagDB.TagRFID, objTruck.LastReadTime, loc, out desPad, out desLoc);
                            objTruck.TagDB.DestinationPad = desPad;
                            // objTruck.FoundAtHopperID = desPad;
                            if (desLoc == Location.ENTRY1 || (loc != Location.REDS && loc != Location.QUEUE))
                                objTruck.EntryOrQueue = Location.ENTRY1;
                            else
                                objTruck.EntryOrQueue = Location.QUEUE;
 
#if DEBUG
                                checkPoint = "TraTraOC: Finish EntryOrQueue in DB: 3646";
                            log.Debug("Truck {0}  at {1} location. After calling EntryOrQueue in DB. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        }
                        else if (loc == Location.HOPPER &&
                            BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count > 1)
                        {
                            objTruck.TagDB.DestinationPad = 0;//BLUtility.HoppersInFacility.First(x => x.Value.MarkerID == BLUtility.Markers[Location.HOPPER].Find(z => z.MarkerLoopID == NewLoopID).ID).Value.HopperID;
                        }

                        if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.DestinationPad.ToString()))
                        {
                            objHopper = BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()];

                        }

                        if (loc == Location.REDS)
                        {

                            //send VMS message and starts response and transaction timers
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. . Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            objTruck.GateQueue();
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. After calling GateQueue. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            if (objHopper != null)
                            {
                                if (objTruck.EntryOrQueue == Location.ENTRY1)
                                {
                                    objHopper.StartTimer((ExpectedValues.REDS_Queue + ExpectedValues.Unloading) * 1000);
#if DEBUG
                                    log.Debug("Started timer for {2} tag at {3} location with {0} ms for {1} pad in TrafficOpenClose loc=REDS after G-Q()",
                                        (ExpectedValues.REDS_Queue + ExpectedValues.Unloading) * 1000, objHopper.HopperID,TagRFID.TagID,loc.ToString());
#endif
                                }
                                else if (objHopper.tmrQueue == null || (!objHopper.tmrQueue.Enabled && !objHopper.IsTimerPaused && !objHopper.nextCallupOccured))
                                {
                                    objHopper.StartTimer(ExpectedValues.REDS_Queue * 1000);
#if DEBUG
                                    log.Debug("Starting timer for {2} tag at {3} location with {0} ms for {1} pad in TrafficOpenClose loc=REDS after G-Q()", 
                                        ExpectedValues.REDS_Queue * 1000, objHopper.HopperID,TagRFID.TagID,loc.ToString());
#endif
                                }
                            }


                        }
                        else if (loc == Location.ENTRY1 || loc == Location.EntryLane1 || loc == Location.EntryLane2 ||
                                                                                   loc == Location.ENTRY2 || loc == Location.HOPPER)
                        {
                            int callTimer; // = ExpectedValues.Unloading;
                            //if (loc == Location.ENTRY1 || loc == Location.EntryLane1 || loc == Location.EntryLane2)
                            //    callTimer -= ExpectedValues.Queue_Ent1;

                            //if (loc == Location.ENTRY2)
                            //    callTimer -= (ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2);

                            //if (loc == Location.HOPPER && objHopper != null)
                            //    callTimer -= (ExpectedValues.Queue_Ent1 + ExpectedValues.Ent1_Ent2 + objHopper.Ent2_Hopper);

                            //new truck was shown up at E1-Pad, add delay in current timer
                            //get call up timer


                            if (objHopper != null && (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp ||
                                BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1))
                            {
 
#if DEBUG
checkPoint = "TraTraOC: Calling DB GetNextCallupTime: 37007";
                                log.Debug("Truck {0}  at {1} location. Getting next callup time. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                DBAdapters.ActiveReads.GetNextCallupTime(objHopper.HopperID, out callTimer);

                                //objHopper.AdjustTimer(callTimer, newTruck);
 

#if DEBUG
checkPoint = "TraTraOC: Fin GetNextCallupTime in DB: 3713";
                                log.Debug("Truck {0}  at {1} location. After getting next callup time before starting timer for hopper. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                if (loc != Location.REDS && loc != Location.QUEUE && objHopper != null)
                                {
                                    if (BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1)
                                    {
                                        //objHopper.StopTimer();
                                        objHopper.SetTimer(callTimer);
                                        if (!objHopper.IsTimerPaused)
                                            objHopper.StartTimer();
                                    }
                                }
                                else if (objHopper != null)
                                {
                                    if (!objHopper.IsTimerPaused)
                                        objHopper.StartTimer(callTimer * 1000);
                                }
#if DEBUG
                                log.Debug("Starting callup time for {2} tag at {3} location with {0} ms for {1} pad in TrafficOpenClose 0501201201622", 
                                    callTimer * 1000, objHopper.HopperID,TagRFID.TagID,loc.ToString());
#endif

                            }

                            if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                            {
                                if (BLUtility.VMSDeviceList[Location.HOPPER].FindAll(x => x.DeviceNo == Convert.ToInt16(objTruck.TagDB.DestinationPad)).Count > 0)
                                {
                                    foreach (IVMSDevice vms in BLUtility.VMSDeviceList[Location.HOPPER].FindAll(x => x.DeviceNo == Convert.ToInt16(objTruck.TagDB.DestinationPad)))
                                        vms.ShowString(objTruck.TagDB.TruckID.PadLeft(4, '0'), "`C00FF00", 0);

                                }

                            }
                        }

                        //else if (loc == Location.ENTRY1 || loc == Location.ENTRY2 || loc == Location.HOPPER) // Changes made on 08/11/2010 
                        //{
                        // Calling Startup/Shutdown hopper 

                        //Hopper objHopper;


                        if (objHopper != null && loc != Location.HOPPER)
                        {
                            //if (loc == Location.HOPPER)
                            //{
                            //    objTruck.FoundAtHopperID = BLUtility.HoppersInFacility.First(x => x.Value.MarkerID == objHopper.MarkerID).Value.HopperID;

                            //}
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Hopper startup shutdDonw for {3} Hopper. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString(),objHopper.HopperID.ToString());

#endif
                            objHopper.Startup_ShutDownPAD(loc);

                        }
#if DEBUG
checkPoint = "TraTraOC: Fin if is new truck: 3768";
                        log.Debug("Truck {0}  at {1} location. Finish if is new truck. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
 
                        //}

                    }//end of if truck is new
                    #endregion


                    if (loc == Location.QUEUE)
                    {
                        // 5-3-2012 : 6:46pm 18:46
                        //System.Threading.Thread th = new System.Threading.Thread(delegate() { TruckDetectedAtQueue(objTruck); });
                        //th.IsBackground = true;
                        //th.Start();
#if DEBUG

                        log.Debug("Truck {0}  at {1} location. Calling Truck detected at Queue. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        TruckDetectedAtQueue(objTruck);


                    }
#if DEBUG

                    log.Debug("Truck {0}  at {1} location. Checking for REDS2 (finish TruckDetected at Queue is loc is Q). Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                    // check for REDS2 event
                    #region
                    if (loc == Location.REDS && (objTruck.LastPositiion != Location.REDS || objTruck.LastPositiion == Location.REDS2))
                    {
                        loc = Location.REDS2;

                        if (objTruck.LastPositiion != Location.REDS2)
                        {
                            // Insert Alert : Truck Leaving with Tags
                            objTruck.TransMessage = TransactionMessages._TruckLeavingWithTags;
                            objTruck.TransStatus = Convert.ToInt32(TransactionStatus.Alert);

                            //string hopperNo = objTruck.TagDB.AssignedHopper.HopperName;

                            //if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                            //{
                            //    Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                            //    objHopper.RemoveFromQueue(objTruck.TagDB);

                            //}
                            objTruck.tmrTransaction.Stop();

                            Hopper objHopper;
                            string hopperNo = objTruck.TagDB.AssignedHopper.HopperName;

                            if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                            {
                                objHopper = BLUtility.HoppersInFacility[hopperNo];

                                if (objHopper != null)
                                {

                                    objHopper.RemoveFromQueue(objTruck.TagDB);

                                }
                            }
                            string newHOrder = "";
                            DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, loc, 3, objTruck.TagRFID.NewMarkerID.ToString(), objTruck.TagRFID.OldMarkerID.ToString(), objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);
                            BLUtility.TrucksInTransaction.Remove(TagRFID.TagID);
                            if (BLUtility.HGroupOrder != newHOrder)
                            {
                                int i = 1;
                                foreach (string order in newHOrder.Split(','))
                                {
                                    try
                                    {
                                        if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                            BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32(order);
                                    }
                                    catch { }
                                    i++;
                                }
                            }

                            return;
                        }

                    }
                    #endregion

#if DEBUG

                    log.Debug("Truck {0}  at {1} location. Finished checking for REDS2. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

                    //Date: 8-12-2011 2:05pm : Changed to include hopper startup shutdown logic
                    #region if not a new truck
                    if (newTruck == false)
                    {
#if DEBUG
                        checkPoint = "TraTraOC: is not new truck: 3854";
                        log.Debug("Current truck is not new truck. {0} truck at {1} location. Thread is: {2}", TagRFID.TagID, loc.ToString(),
                            Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                        if (objTruck.LastPositiion != loc || objTruck.TagRFID.NewMarkerID.ToString() != NewLoopID) // Check for new detection point for Current Transaction
                        {
#if DEBUG
                            checkPoint = "TraTraOC: Calling DB OpenCloseTran: 3860";
                            truckChanged = true;
                            log.Debug("Calling OpenCloseTransaction in DB to update {0} truck at {1} location. Thread is : {2}", TagRFID.TagID, loc.ToString(),
                                Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                            //will update new location in Activerad and in TimeTable with current time from DB server
                            //if truck arrive at Queue and at the same time other pad is also ready to call a truck from queue
                            //callup from here needs to be happen first as this will allow higher priority pad to call first
                            string newHOrder = "";
                            objITag = DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, loc, 1, NewLoopID, OldLoopID,
                                    objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);
                            objTruck.TagDB = objITag;

                            objTruck.TagRFID = TagRFID;

                            objTruck.LastReadTime = DateTime.Now;
                            objTruck.LastPositiion = loc;
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. After updating DB (before checking HGOrder). Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

                            if (BLUtility.HGroupOrder != newHOrder)
                            {
                                int i = 1;
                                foreach (string order in newHOrder.Split(','))
                                {
                                    try
                                    {
                                        if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                        {
                                            BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                            BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                        }
                                    }
                                    catch { }
                                    i++;
                                }
                                BLUtility.HGroupOrder = newHOrder;
                            }

#if DEBUG
                            checkPoint = "TraTraOC: Finish call OpenCloseTra in DB: 3898";
#endif

                            if (loc == Location.TRUCKWASH || loc == Location.EXITGATE)
                            {
#if DEBUG

                                log.Debug("Truck {0}  at {1} location. Starting timer. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                objTruck.StartTimer(delayTimer * 1000);

                                try
                                {
                                    if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                                    {
                                        BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                                    }
                                }
                                catch { }

                                #region Disabled: hopper startup shutdown
                                //Hopper objHopper;
                                //string hopperNo = objTruck.TagDB.DestinationPad.ToString();

                                //if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                                //{
                                //    objHopper = BLUtility.HoppersInFacility[hopperNo];

                                //    if (objHopper != null)
                                //    {
                                //        log.Trace("Found Hopper and closing it by calling Startup-Shutdown alg used by {0}", TagRFID.TagID);
                                //        objHopper.Startup_ShutDownPAD(loc);

                                //    }

                                //    objHopper.RemoveFromQueue(objTruck.TagDB);
                                //}
                                // } 
                                #endregion

                            }

                            #region Disabled: hopper startup shutdown
                            //else if (loc == Location.ENTRY1) // Remove truck from Hopper Queue, if exists in Queue, when detected at Enrty Gate 
                            //{
                            //    string hopperNo = objTruck.TagDB.DestinationPad.ToString();

                            //    if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                            //    {
                            //        Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                            //        objHopper.RemoveFromQueue(objTruck.TagDB);

                            //        // added on 06-04-2011 ( to perform pad startup when truck detected at Entry Gate)
                            //        if (objHopper != null)
                            //        {
                            //            objHopper.Startup_ShutDownPAD(loc);
                            //        }
                            //    }
                            //}
                            //else if (loc == Location.ENTRY2)  // added on 06-04-2011 ( to perform pad startup when truck detected at Entry Gate)
                            //{
                            //    string hopperNo = objTruck.TagDB.DestinationPad.ToString();

                            //    if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                            //    {
                            //        Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                            //        objHopper.RemoveFromQueue(objTruck.TagDB);

                            //        if (objHopper != null)
                            //        {
                            //            objHopper.Startup_ShutDownPAD(loc);
                            //        }
                            //    }
                            //}// end of if loc =  ENTRY2
                            #endregion

#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Checking for Startup_ShutDwon hopper. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            //calling hopper startup_shutdown alg
                            string hopperNo = objTruck.TagDB.DestinationPad.ToString();
                            if (BLUtility.HoppersInFacility.ContainsKey(hopperNo) && (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp ||
                                BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1))
                            {
                                Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                                //             lstMarkers = BLUtility.Markers[Location.HOPPER];

                                //var foundAtMarker = from objMarker in lstMarkers where objMarker.MarkerLoopID.Trim() == objTruck.TagRFID.NewMarkerID.ToString() select objMarker;


                                if (objHopper != null && loc != Location.QUEUE && loc != Location.InboundEntry && loc != Location.HOPPER)
                                {
                                    //if locaion is pad update FoundAtHopperID to current pad as 
                                    //Startup_ShutdownHopper check for multiple trucks at the pad by getting trucks from TrucksInTransactionList with loc = hopper or FoundAtHopperID = current hopperID
                                    //if loc is hopper, algoritm will be called from TruckDetected at Hopper
                                    //if (loc == Location.HOPPER)
                                    //    objTruck.FoundAtHopperID = BLUtility.HoppersInFacility.First(x => x.Value.MarkerID == objHopper.MarkerID).Value.HopperID;
 
#if DEBUG
checkPoint = "TraTraOC: Starting Hopper: 3994";
                                    log.Debug("Truck {0}  at {1} location. Calling Startup_ShutDown hopper. Thread MId {2}", TagRFID.TagID,
                                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                    objHopper.Startup_ShutDownPAD(loc);

                                }

                                if (objTruck.LastPositiion != Location.REDS || objTruck.LastPositiion != Location.QUEUE || objTruck.LastPositiion != Location.InboundUntarping ||
                                    objTruck.LastPositiion != Location.InboundQueue)
                                    objHopper.RemoveFromQueue(objTruck.TagDB);
                            }

#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Finished checking Startup_ShtuDownHopper. Checking to start pad timer. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            if (loc == Location.ENTRY1 || loc == Location.EntryLane1 || loc == Location.EntryLane2
                                || loc == Location.ENTRY2 || loc == Location.HOPPER || loc == Location.TRUCKWASH || loc == Location.EXITGATE)
                            {
                                if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.DestinationPad.ToString()) && (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp ||
                                    BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1))
                                {

                                    // if (BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].tmrQueue != null)
                                    //{
                                    bool startTimer = true;
                                    if (loc == Location.EntryLane1 || loc == Location.EntryLane2 || loc == Location.ENTRY1)
                                    {
                                        if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == objTruck.TagDB.DestinationPad && ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                             (x.Value.LastPositiion == Location.ENTRY1 && x.Value.LastReadTime > objTruck.LastReadTime))))
                                        {
                                            startTimer = false;
                                        }
                                    }
                                    else if (loc == Location.ENTRY2)
                                    {
                                        if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == objTruck.TagDB.DestinationPad & ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                             (x.Value.LastPositiion == Location.ENTRY1) || (x.Value.LastPositiion == Location.ENTRY2 && x.Value.LastReadTime > objTruck.LastReadTime))))
                                        {
                                            startTimer = false;
                                        }

                                    }
                                    else if (loc == Location.HOPPER)
                                    {
                                        if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == objTruck.TagDB.DestinationPad & ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                             (x.Value.LastPositiion == Location.ENTRY1) || (x.Value.LastPositiion == Location.ENTRY2) ||
                                                 (x.Value.LastPositiion == Location.HOPPER && x.Value.LastReadTime > objTruck.LastReadTime))))
                                        {
                                            startTimer = false;
                                        }

                                    }
                                    else if (loc == Location.TRUCKWASH || loc == Location.EXITGATE)
                                    {
                                        if (BLUtility.TrucksInTransaction.Any(x => x.Value.TagDB.DestinationPad == objTruck.TagDB.DestinationPad & ((x.Value.LastPositiion == Location.REDS && x.Value.EntryOrQueue == Location.ENTRY1) ||
                                             (x.Value.LastPositiion == Location.ENTRY1) || (x.Value.LastPositiion == Location.ENTRY2) || (x.Value.LastPositiion == Location.HOPPER))))
                                        {
                                            startTimer = false;
                                        }

                                    }

                                    if (startTimer)
                                        BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].StartTimer(timerAdjust * 1000);// AdjustTimer(timerAdjust, newTruck);

#if DEBUG
                                    log.Debug("Setting/starting call up timer for {3} tag at {4} location with {0} ms for {1} hopper in TrafficTransactionOpen Close. CheckPoint {2}",
                                        timerAdjust * 1000, objTruck.TagDB.DestinationPad, checkPoint,TagRFID.TagID,loc.ToString());
#endif
                                    //}
                                }

                                //Hashtable htParams = new Hashtable();

                                //htParams.Add("vmsParam", new VMSParameters());
                                ////htParams.Add("dtVMSSettings", dtVMSSettings);
                                //htParams.Add("truckID", objTruck.TagDB.TruckID.PadLeft(4, '0'));
                                //htParams.Add("TagRFID", objTruck.TagRFID.TagID);
                                //htParams.Add("hopperID", objTruck.TagDB.DestinationPad.ToString().PadLeft(2, '0'));
                                //htParams.Add("Location", Location.HOPPER);

                                //TagBlinkNVMSDisplay(htParams);

#if DEBUG

                                log.Debug("Truck {0}  at {1} location. Finished starting pad timer and before sending messages to the VMS. Thread MId {2}", TagRFID.TagID,
                                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                if (loc == Location.ENTRY1 || loc == Location.EntryLane1 || loc == Location.EntryLane2
                                || loc == Location.ENTRY2 || loc == Location.HOPPER)
                                {
                                    if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER) && (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp ||
                                        BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1))
                                    {
                                        if (BLUtility.VMSDeviceList[Location.HOPPER].FindAll(x => x.DeviceNo == Convert.ToInt16(objTruck.TagDB.DestinationPad)).Count > 0)
                                        {
                                            foreach (IVMSDevice vms in BLUtility.VMSDeviceList[Location.HOPPER].FindAll(x => x.DeviceNo == Convert.ToInt16(objTruck.TagDB.DestinationPad)))
                                                vms.ShowString(objTruck.TagDB.TruckID.PadLeft(4, '0'), "`C00FF00", 0);

                                        }

                                    }
                                }
                                if (BLUtility.VMSDeviceList.ContainsKey(Location.QUEUE))
                                {
                                    foreach (IVMSDevice obj in BLUtility.VMSDeviceList[Location.QUEUE])
                                        obj.RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                                }



                            }

#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Checking for callup as loc is Queue. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            if (loc == Location.QUEUE)
                            {
                                try
                                {
                                    int gID = BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].GroupID;
                                    Hopper hop = BLUtility.HoppersInFacility.Where(x => x.Value.GroupID == gID
                                        && x.Value.HopperAvailable).OrderBy(x => x.Value.GroupOrder).First(x => x.Value.tmrQueue == null
                                            || (!x.Value.nextCallupOccured && !x.Value.IsTimerPaused && (!x.Value.tmrQueue.Enabled || x.Value.tmrQueue.Interval <= 2000))).Value;  // .ToDictionary(x=>x.Key,x=>x.Value);


                                    if (hop != null)
                                    {
#if DEBUG

                                        log.Debug("Truck {0}  at {1} location. Calling TruckCallup. Thread MId {2}", TagRFID.TagID,
                                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                                        
                                        //Bhavesh: 20-7-2012: do not call TruckCallup directly as it also tries to lock BLUtility.truckInList object if it 
                                        //needs to change destination pad of other trucks in q after reassigning new pads
                                        //hop.TruckCallup();
                                        hop.StopTimer();
                                        hop.StartTimer(1000);

                                        if (objTruck.LastPositiion == Location.InboundEntry && loc == Location.QUEUE)
                                            loc = Location.InboundEntry;
                                    }


                                }
                                catch (Exception ex)
                                {
#if DEBUG
                                    checkPoint = "TraTraOC: Error 4124";
                                    log.ErrorException("Exception thrown for " + TagRFID.TagID + " truck at Queue", ex);
#endif
                                }
                            }
#if DEBUG

                            log.Debug("Truck {0}  at {1} location. Finished checking callup for Queue. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif



                        }
                        else
                        {
#if DEBUG
 checkPoint = "TraTraOC: Dup read: 4137";
                            log.Debug("Truck {0}  at {1} location is not new truck and hasn't change location. Updating TagRFID object in TransList. Thread MId {2}", TagRFID.TagID,
                                loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                            objTruck.TagRFID = TagRFID;
 
                        }

                    }
#if DEBUG

                    log.Debug("Truck {0}  at {1} location. Finished if not a new truck. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                    //else
                    //{
                    //    if (loc == Location.HOPPER)
                    //    {
                    //        // Check for PAD match and accordingly update th1
                    //        System.Threading.Thread th = new System.Threading.Thread(delegate() { TruckDetectedAtHopper(objTruck); });
                    //        th.IsBackground = true;
                    //        th.Start();
                    //    }
                    //}
                    #endregion


                    if (loc == Location.HOPPER)
                    {
#if DEBUG
checkPoint = "TraTraOC: TruckAtHopper started: 4162";
                        log.Debug("Truck {0}  at {1} location. Calling Truck detected at H in new thread. Thread MId {2}", TagRFID.TagID,
                            loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                        // Check for PAD match and accordingly update the Modbus .
                        System.Threading.Thread th = new System.Threading.Thread(delegate() { TruckDetectedAtHopper(objTruck); });
                        th.Name = "AlgTDetAtH_" + DateTime.Now.ToString("HHmmss") + DateTime.Now.Millisecond.ToString();
                        th.IsBackground = true;
                        th.Start();
 
                        #region Disabled: hopper startup shutdown
                        //Hopper objHopper;
                        //string hopperNo = objTruck.TagDB.DestinationPad.ToString();

                        //if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                        //{
                        //    objHopper = BLUtility.HoppersInFacility[hopperNo];

                        //    if (objHopper != null)
                        //    {
                        //        objHopper.Startup_ShutDownPAD(loc);
                        //    }
                        //}
                        #endregion

                    }

#if DEBUG

                    log.Debug("Truck {0}  at {1} location. After calling Truck detected at H if loc is H. Checking for to set timer. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif
                    if (setTimer == true && loc != Location.TRUCKWASH && loc != Location.EXITGATE)
                    {
                        if (loc != Location.QUEUE && loc != Location.InboundEntry && loc != Location.REDS2)//&& (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp)) // do not reset timer when detected at REDS2 and QUEUE
                        {
                            objTruck.StartTimer(delayTimer * 1000);
#if DEBUG
                            checkPoint = "TraTraOC: StartTimer for truck: 4188";
                            truckChanged = true;
                            log.Debug("Call {3} StartTimer for {0} truck at {1} location. Thread is : {2}", TagRFID.TagID,
                                loc.ToString(), Thread.CurrentThread.ManagedThreadId.ToString(), (delayTimer * 1000).ToString());
#endif
                            if (objTruck.tmrCheckResponseTime == null || !objTruck.tmrCheckResponseTime.Enabled)
                            {
                                if (delayTimer > ExpectedValues.Max_Delay)
                                    delayTimer = delayTimer - ExpectedValues.Max_Delay;

                                //Date: 26-3-2012 10:18 Bhavesh
                                //Issue: trucks last location stays to Hopper after unload time causing issue when a new truck arrives at the pad. System finds multiple trucks at the pad even though first trucks has finished unloading and has left pad
                                //Fix: Timer will set for Unloading time and then will change the last location to InboundExit  
                                //was disabled but enabled again on 24-5-2012
                                //disabled on 26-5-2012 all trucks will be right truck if their assigned pad is current pad
                                //if (loc == Location.HOPPER)
                                //{
                                //    delayTimer = ExpectedValues.Unloading;
                                //}

                                objTruck.StartResTimer((delayTimer) * 1000);
#if DEBUG
                                checkPoint = "TraTraOC: Start ResTimer: 4209";
                                log.Debug("Starting Response {3} timer for {0} truck at {1} location. Thread is : {2}", TagRFID.TagID, loc.ToString(),
                                    Thread.CurrentThread.ManagedThreadId.ToString(), (delayTimer * 1000).ToString());
#endif
                            }
                            //objTruck.StartResTimer();
                            //objTruck.tmrTransaction.Start();

                        }
                        else if (objTruck.tmrTransaction.Enabled)
                        {
#if DEBUG
                            checkPoint = "TraTraOC: Stoping TransTimer: 4221";
                            truckChanged = true;
                            log.Debug("Stopping Transaction timer for {0} truck at {1} location. Thread is :{2}", TagRFID.TagID, loc.ToString(),
                                Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                            objTruck.tmrTransaction.Stop();
                        }
                    }

#if DEBUG
checkPoint = "TraTraOC: Update truck: 4234";
                    log.Debug("Truck {0}  at {1} location. Finished checking if to set timer. Updateing LastPosition to current location and truck object. Thread MId {2}", TagRFID.TagID,
                        loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

#endif

                    objTruck.LastPositiion = loc;

                    BLUtility.TrucksInTransaction[TagRFID.TagID] = objTruck;

 

                    //if loc = Q

                }

            }
            catch (Exception ex)
            {
                BLUtility.WriteEventLog(ex.Message, System.Diagnostics.EventLogEntryType.Error);
#if DEBUG
                log.Debug("Unhandled exceptin occured in TrafficTransactionOpenClose method with {0} truck at {1} location. Thread is: ", TagRFID.TagID, loc.ToString(), Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                //throw ex;
            }
            finally
            {
#if DEBUG
                 log.Debug("Truck {0}  at {1} location.CheckPoint: {3}  . Thread MId {2}", TagRFID.TagID,
                    loc.ToString(), System.Threading.Thread.CurrentThread.ManagedThreadId.ToString(),checkPoint);
 
               if(truckChanged)
                   LogAllTruckHoppers(log);
#endif
            }

        }

        public static void LogAllTruckHoppers(Logger xlog)
        {
#if DEBUG
            StringBuilder AllTruck = new StringBuilder();
            foreach (ITruck tt in BLUtility.TrucksInTransaction.Values)
            {
                try
                {
                    AllTruck.AppendLine(String.Format("TagRFID {0},TruckID {1},CalledUp {2},End_Transaction {3},EntryOrQueue {4},FoundAtHopperID {5},CallupCount {6},LastPos {7},LastReadTime {8},StatusMsg {9},DesPad {10},TagDB.LastReadTime {11},TagType {12},MarkerNewPosTime {13},MarkerOldPosTime {14},TagRFID.NewMarkerID {15},TagRFID.OldMarkerID {16},TagRFID.TimeLastSeen {17},ResTimerInt {18},ResTimerAutReset {19},ResTimerEnabled {20},TranTimerInt {21},TranTimerAutoReset {22},TranTimerEnabled {23},TransStatus {24}",
                        tt.TagRFID.TagID, tt.TagDB.TruckID, tt.CalledUp.ToString(), tt.End_Transaction, tt.EntryOrQueue, tt.FoundAtHopperID, tt.iTruckCallupCount, tt.LastPositiion,
                        tt.LastReadTime.ToLongTimeString(), tt.statusMessage, tt.TagDB.DestinationPad, tt.TagDB.LastReadTime.ToLongTimeString(), tt.TagDB.TagType,
                           tt.TagRFID.MarkerNewPositionTime.ToLongTimeString(), tt.TagRFID.MarkerOldPositionTime.ToLongTimeString(), tt.TagRFID.NewMarkerID, tt.TagRFID.OldMarkerID,
                           tt.TagRFID.TimeLastSeen.ToLongTimeString(), (tt.tmrCheckResponseTime != null) ? tt.tmrCheckResponseTime.Interval.ToString() : "NULL",
                           (tt.tmrCheckResponseTime != null) ? tt.tmrCheckResponseTime.AutoReset.ToString() : "NULL", (tt.tmrCheckResponseTime != null) ? tt.tmrCheckResponseTime.Enabled.ToString() : "NULL",
                           (tt.tmrTransaction != null) ? tt.tmrTransaction.Interval.ToString() : "NULL", (tt.tmrTransaction != null) ? tt.tmrTransaction.AutoReset.ToString() : "NULL", (tt.tmrTransaction != null) ? tt.tmrTransaction.Enabled.ToString() : "NULL",
                           tt.TransStatus));
                }
                catch (Exception eee)
                {
#if DEBUG
                    xlog.ErrorException("Exception thrown when loggin all trucks", eee);
#endif
                }
            }
            StringBuilder AllHopper = new StringBuilder();
            foreach (Hopper hh in BLUtility.HoppersInFacility.Values)
            {
                try
                {
                    AllHopper.AppendLine(String.Format("HopperID {0},Available {1},IsAdjustTimerCalledUP {2},IsTimerPaused {3},LastTruckCalledup {4},LastTruckDetectionTime {5},PadStartSet {6},NoTruckFoundOnCallup {7},PadStartTime {8},thPadStartupAlive {9},thPadStartupThreadID {10},thPadStartupState {11},timerFlag {12},TimerStartTime {13},TimerStopTime {14},tmrInterval {15},tmrPadStartup.AutoReset {16},tmrPadStartup.Enabled {17},tmrPadStartup.Int {18},tmrQ.Auto {19},tmrQ.Enabled {20},tmrQ.Int {21}",
                        hh.HopperID, hh.HopperAvailable, hh.IsAdjustTimerCalledUp, hh.IsTimerPaused, hh.LastTruckCalledup, hh.LastTruckDetectionTime.ToLongTimeString(), hh.Location_PadStartSet, hh.NoTruckFoundOnCallup,
                        hh.PadStart_Time.ToLongTimeString(), (hh.thPadStartup != null) ? hh.thPadStartup.IsAlive.ToString() : "NULL", (hh.thPadStartup != null) ? hh.thPadStartup.ManagedThreadId.ToString() : "NULL",
                        (hh.thPadStartup != null) ? hh.thPadStartup.ThreadState.ToString() : "NULL", hh.timerFlag, hh.TimerStartTime.ToLongTimeString(),
                        hh.TimerStoppedTime.ToLongTimeString(), hh.tmrInterval, (hh.tmrPadStartup != null) ? hh.tmrPadStartup.AutoReset.ToString() : "NULL", (hh.tmrPadStartup != null) ? hh.tmrPadStartup.Enabled.ToString() : "NULL",
                        (hh.tmrPadStartup != null) ? hh.tmrPadStartup.Interval.ToString() : "NULL", (hh.tmrQueue != null) ? hh.tmrQueue.AutoReset.ToString() : "NULL", (hh.tmrQueue != null) ? hh.tmrQueue.Enabled.ToString() : "NULL",
                        (hh.tmrQueue != null) ? hh.tmrQueue.Interval.ToString() : "NULL"));
                }
                catch (Exception eee)
                {
#if DEBUG
                    xlog.ErrorException("Exception thrown when loggin all trucks", eee);
#endif

                }
            }

            xlog.Debug("{0}------------------AllTrucks-------------------{0}{1}", System.Environment.NewLine, AllTruck.ToString());
            xlog.Debug("{0}------------------AllHopper-------------------{0}{1}", System.Environment.NewLine, AllHopper.ToString());
#endif
        }

        public static void TransactionTimeOut(RFIDTag TagRFID)
        {
            try
            {
#if DEBUG
                log.Debug("Entered TransactionTimeOut method for {0} tag. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                int timerAdjust = 0;
                ITruck objTruck = null;
                string despad = "";
                int foundat = 0;
                string tagid = "";
                int groupid = 0;


                lock (BLUtility.truckListLock)
                {
                    if (BLUtility.TrucksInTransaction.ContainsKey(TagRFID.TagID))
                    {
                        objTruck = BLUtility.TrucksInTransaction[TagRFID.TagID];
                        despad = objTruck.TagDB.DestinationPad.ToString();
                        foundat = objTruck.FoundAtHopperID;
                        tagid = objTruck.TagRFID.TagID;
                        if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.HopperID.ToString()))
                            groupid = BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID;

                        BLUtility.TrucksInTransaction.Remove(TagRFID.TagID);
                    }
                }

                if (objTruck == null)
                {
                    string newHOrder = "";
                    DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, 0, 3, TagRFID.NewMarkerID.ToString(), TagRFID.OldMarkerID.ToString(), Convert.ToInt32(TransactionStatus.Incomplete), TransactionMessages._TransactionInComplete, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);
                    if (BLUtility.HGroupOrder != newHOrder)
                    {
                        int i = 1;
                        foreach (string order in newHOrder.Split(','))
                        {
                            try
                            {
                                if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                {
                                    BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                    BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                }
                            }
                            catch { }
                            i++;
                        }
                        BLUtility.HGroupOrder = newHOrder;
                    }
                }
                else
                {
                    string newHOrder = "";
                    DBAdapters.ActiveReads.OpenCloseTransaction(TagRFID.TagID, 0, 3, TagRFID.NewMarkerID.ToString(), TagRFID.OldMarkerID.ToString(), objTruck.TransStatus, objTruck.TransMessage, TagRFID.BatteryStatus.ToString(), out timerAdjust, out newHOrder);

                    try
                    {
                        if (BLUtility.VMSDeviceList.ContainsKey(Location.HOPPER))
                        {
                            BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)objTruck.TagDB.DestinationPad).RemoveMessage(objTruck.TagDB.TruckID.ToString().PadLeft(4, '0'), "`C00FF00");

                        }

                    }
                    catch { }
                    if (BLUtility.HGroupOrder != newHOrder)
                    {
                        int i = 1;
                        foreach (string order in newHOrder.Split(','))
                        {
                            try
                            {
                                if (BLUtility.HoppersInFacility.ContainsKey(i.ToString()))
                                {
                                    BLUtility.HoppersInFacility[i.ToString()].GroupID = Convert.ToInt32((order.Split(':'))[0]);
                                    BLUtility.HoppersInFacility[i.ToString()].GroupOrder = Convert.ToInt32((order.Split(':'))[1]);
                                }
                            }
                            catch { }
                            i++;
                        }
                        BLUtility.HGroupOrder = newHOrder;
                    }


                    #region Disabled moved to HopperStartupCallupAdjustOnClose
                    //string hopperNo = objTruck.TagDB.DestinationPad.ToString();//.AssignedHopper.HopperName;
                    //string foundAt = objTruck.FoundAtHopperID.ToString();
                    //log.Trace("Closed transaction in DB and checking Hopper startup shutdown. Truck went to {1} pad and was des. to {2} pad.", TagRFID.TagID,
                    //    foundAt,hopperNo);

                    //if (foundAt != hopperNo && BLUtility.HoppersInFacility.ContainsKey(foundAt))
                    //{
                    //    log.Trace("Calling hopper statrup shutdown ag on {0} pad for {1} tag.", foundAt, TagRFID.TagID);
                    //    BLUtility.HoppersInFacility[foundAt].Startup_ShutDownPAD(Location.EXITGATE);

                    //}

                    //if (BLUtility.HoppersInFacility.ContainsKey(hopperNo))
                    //{
                    //    Hopper objHopper = BLUtility.HoppersInFacility[hopperNo];

                    //    objHopper.RemoveFromQueue(objTruck.TagDB);
                    //    log.Trace("Tag {1} removed from the pad queue and calling hopper statrup shutdown alg on {0} pad.", foundAt, TagRFID.TagID);
                    //    objHopper.Startup_ShutDownPAD(Location.EXITGATE);

                    //    //call a truck if there were not tuck called up from the queue after 
                    //    // this truck else have no affect as timer will set by truck after this
                    //     if(foundAt == "0" && objHopper.LastTruckCalledup != TagRFID.TagID && !objHopper.nextCallupOccured)
                    //         objHopper.TruckCallup();
                    //}
                    #endregion

                }

                if (foundat == 0 && groupid > 0)
                {
                    //Dictionary<string, int> taglist = null;
                    lock (Algorithms.CallupLocker)
                    {
                        // taglist = DBConnector.Adapter.ActiveReads.NextTruckToCallup(skipTagRFID, HopperID, out callupTagRFID, out TimeToWait);

                        DBConnector.Adapter.ActiveReads.ReassignAllPad(groupid, "");

                    }

                    //                if (taglist != null && taglist.Count > 0)
                    //                {
                    //                    lock (BLUtility.truckListLock)
                    //                    {

                    //                        foreach (string k in taglist.Keys)
                    //                            if (BLUtility.TrucksInTransaction.ContainsKey(k))
                    //                            {

                    //                                BLUtility.TrucksInTransaction[k].TagDB.DestinationPad = taglist[k];
                    //                                if (BLUtility.HoppersInFacility[taglist[k].ToString()].tmrQueue == null || (!BLUtility.HoppersInFacility[taglist[k].ToString()].tmrQueue.Enabled && !BLUtility.HoppersInFacility[taglist[k].ToString()].IsTimerPaused))
                    //                                {


                    //                                    int callTimer;
                    //                                    ActiveReads.GetNextCallupTime(HopperID, out callTimer);
                    //                                    BLUtility.HoppersInFacility[taglist[k].ToString()].StartTimer(callTimer * 1000);

                    //#if DEBUG
                    //                                    log.Info("Setting {0} sec for callup timer for {1} pad.", callTimer, HopperID);
                    //#endif

                    //                                }
                    //                            }
                    //                    }

                    //                }//end of if return taglist is not empty
                }
                if (!string.IsNullOrEmpty(tagid))//(foundat == 0 && !string.IsNullOrEmpty(tagid)&& !string.IsNullOrEmpty(despad))
                    HopperStartupCallupAdjustOnClose(despad, foundat.ToString(), tagid);

            }
            catch (Exception ex)
            {
#if DEBUG
               log.ErrorException(string.Format("Exception thrown in TransactionTimeOut method for {0} truck. Thread {1}",TagRFID.TagID,Thread.CurrentThread.ManagedThreadId.ToString()),ex);
#endif
            }
            finally
            {
#if DEBUG
                log.Debug("Finish TransactionTimeOut method for {0} truck. Thread {1}", TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            }
        }

        private static void HopperStartupCallupAdjustOnClose(string destPad, string foundAt, string tagRFID)
        {
            try
            {
#if DEBUG
                log.Debug("Entered HopperStatupCallupAdjustOnClose for {0} truck, foundAt {1}, DesPAD {2}. Thread {3}", tagRFID, foundAt, destPad, Thread.CurrentThread.ManagedThreadId.ToString());
#endif


                if (foundAt != destPad && BLUtility.HoppersInFacility.ContainsKey(foundAt))
                {
                    BLUtility.HoppersInFacility[foundAt].Startup_ShutDownPAD(Location.EXITGATE);

                }

                if (BLUtility.HoppersInFacility.ContainsKey(destPad))
                {
                    Hopper objHopper = BLUtility.HoppersInFacility[destPad];

                    objHopper.RemoveFromQueue(tagRFID);
                    objHopper.Startup_ShutDownPAD(Location.EXITGATE);
                    int foundat = 0;
                    Int32.TryParse(foundAt, out foundat);
                    //call a truck if there were not tuck called up from the queue after 
                    // this truck else have no affect as timer will set by truck after this

                    int callTimer = 0;
                    /*
                     * 10-7-2012 : issue found in log reivew. When truck times out after overdue they'r not calling next truck from Q
                     * Sometime last called up truck doens't respond and gets placed into the queue and doesn't get callup
                     * or truck might get timedout before this truck overdues at pad
                     * in this case last truck won't be the current truck and this will not call next truck when current 
                     * truck times out 
                     * 
                     * Call the next callup time and change IsTimerPaused = false if not calling next callup timer
                     */
                    if (foundat == 0 && !objHopper.nextCallupOccured && (string.IsNullOrEmpty(objHopper.LastTruckCalledup) || objHopper.LastTruckCalledup == tagRFID))
                    {
                        ActiveReads.GetNextCallupTime(objHopper.HopperID, out callTimer);
                        objHopper.StartTimer(callTimer * 1000);
#if DEBUG
                        log.Info("Starting callup timer with {0} ms for {1} pad in HopperStatupCallupAdjustOnClose", callTimer * 1000, objHopper.HopperID);
#endif

                    }

                }

            }
            catch (Exception ex)
            {
#if DEBUG
               log.ErrorException(string.Format("Exception thrown while setting hopper callup and shtartupShutdow for {0} pad. for {1} tag", destPad, tagRFID),ex);
#endif
                  
            }
            finally
            {
#if DEBUG
                log.Debug("Finish HopperStartupCallupAdjustOnClose for {0} truck, foundAt {1},desPad {2}. Thread {3}", tagRFID, foundAt, destPad, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
            }
        }//end of HopperStartup_CallupAdjustOnClose

        //If manual truck arrives at Queue - re-assigned to new pad
        //If a truck arrives that is not assigned to any pad(due to an exception thrown in GateQueue algorithm) - assing it to new pad
        //Add truck into relavent pad queue in all cases
        public static void TruckDetectedAtQueue(ITruck objTruck)
        {
#if DEBUG
            Algorithms.checkPoint = "TDetQ: Entered: 4598";
            log.Debug("Entered TruckDetectedATQueue method for {0} truck. Thread {1}", objTruck.TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif

            int desPad = 0;
            Location desLoc;

            DateTime currentTime = DateTime.Now;
            try
            {
                //if manual tarping truck arrives at Queue, assign that truck to a first available pad
                //then proceed with new pad.
                #region if truck is manual tarping truck or not assigned to any  pad

                //6-3-2012 : 21:23
                if (objTruck.TagDB.TagType == (int)TagType.ManualTarp || objTruck.TagDB.DestinationPad < 1)
                {
#if DEBUG
                    Algorithms.checkPoint = "TDetH: Calling E0rQ in DB MT: 4616";
#endif
                    //will assign new pad and update current location into Tags table
                    DBAdapters.ActiveReads.EntryOrQueue(objTruck.TagDB.TagRFID, DateTime.Now, Location.QUEUE, out desPad, out desLoc);

                    //objTruck.FoundAtHopperID = desPad;
                    objTruck.TagDB.DestinationPad = desPad;

                }
                #endregion

                Hopper objHopper = null;
                try
                {
                    objHopper = BLUtility.HoppersInFacility.First(x => Convert.ToInt32(x.Key) == objTruck.TagDB.DestinationPad).Value;
                }
                catch { }

                if (objHopper != null)
                {


                    // if (!objHopper.lstQueue.Contains(objTruck.TagDB))
                    if (objHopper.AddToQueue(objTruck.TagDB, true))
                    {
                        // objHopper.AddToQueue(objTruck.TagDB, true);
                        if (Hopper.NoofTrucksDirectedtoQueue > 0)
                            Hopper.NoofTrucksDirectedtoQueue--;
                        else
                            Hopper.NoofTrucksDirectedtoQueue = 0;
                    }

#region commented
                    //7-3-2012 : Calling callup on priority pad from Tran-OpenClose Alg

                    //                        if (objHopper.tmrQueue == null || !objHopper.tmrQueue.Enabled)
                    //                        {
                    //                            if (!objHopper.IsTimerPaused && !objHopper.nextCallupOccured)
                    //                            {
                    //                                //Timer should be set to 1ms if no truck was found on last call up
                    //                                //passing no argument will start the timer with previously set interval or will set it to
                    //                                //  unload time if timer is null
                    //                                int calltimer = 0;
                    //                                ActiveReads.GetNextCallupTime(objHopper.HopperID, out calltimer);
                    //                                objHopper.StartTimer(calltimer * 1000);
                    //#if DEBUG
                    //                                log.Info("starting callup timer with {0} for {1} hopper in TruckDetectedAtQueue", calltimer * 1000, objHopper.HopperID);
                    //#endif

                    //                                //if (objHopper.NoTruckFoundOnCallup)
                    //                                //{
                    //                                //    //logger.Trace("No truck found on callup and calling next truck");
                    //                                //    objHopper.NoTruckFoundOnCallup = false;
                    //                                //    objHopper.TruckCallup();
                    //                                //}
                    //                                //else
                    //                                //{
                    //                                //    log.Trace("Re-setting callup timer of the PAD : {0} to unloading time for next truck", objHopper.HopperName);
                    //                                //    objHopper.StartTimer(ExpectedValues.Unloading*1000);
                    //                                //}
                    //                            }
                    //                        }//end of if tmrQueue is enabled
#endregion


                }
                #region commented
                //                else
                //                {
                //                    if (BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.HopperID.ToString()))
                //                    {
                //                        if (BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].tmrQueue == null ||
                //                                    (!BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].tmrQueue.Enabled && BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].IsTimerPaused))
                //                        {
                //                            int calltimer = 0;
                //                            ActiveReads.GetNextCallupTime(objTruck.TagDB.HopperID, out calltimer);
                //                            BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].StartTimer(calltimer * 1000);
                //#if DEBUG
                //                            log.Info("Starting call up timer with {0} ms for {1} pad in TruckDetectedAtQueue Ln4054", calltimer * 1000, objTruck.TagDB.HopperID);
                //#endif
                //                        }
                //                    }
                //                }
#endregion 

            }
            catch (Exception ex)
            {
#if DEBUG
               log.ErrorException(String.Format("Exception thrown in TruckDetectedAtQueue method for {0} truck. Thread {1}",objTruck.TagRFID.TagID,Thread.CurrentThread.ManagedThreadId.ToString()),ex);
#endif
            }
            finally
            {

#if DEBUG
                log.Debug("Finish TruckDetectedAtQueue method for {0} truck. CheckPoint {1}. Thread {2}",objTruck.TagRFID.TagID,Algorithms.checkPoint, Thread.CurrentThread.ManagedThreadId.ToString());
                Algorithms.LogAllTruckHoppers(log);
#endif
            }

        }

        #endregion

        #region PAD Procedure (Truck Detected at Hopper)

        public static void TruckDetectedAtHopper(ITruck objTruck)
        {
            int prevHopper = 0;
            string logSteps = "";
            int count = 0;
            try
            {
#if DEBUG
                Algorithms.checkPoint = "TDetH: Entered: 4710";
                log.Debug("Entered TruckDetectedAtHopper method for {0} truck. Thread : {1}", objTruck.TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString());
#endif
                // Check for previous PAD and reset the flag if no more trucks are at that PAD --- // Changes made on 08/11/2010 
                // if (objTruck.TagDB.DestinationPad > 0)
                // {
                prevHopper = objTruck.FoundAtHopperID;
                var trucksAtPreviousPAD = from aTruck in BLUtility.TrucksInTransaction.Values where aTruck.FoundAtHopperID == objTruck.FoundAtHopperID select aTruck;
                if (BLUtility.IsModbusConnected)
                {
                    // check if the previous PAD is working

                    // remove check for PAD Ststus ( reverting back to 23/11/2010 ) 
                    // short padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(Convert.ToByte(objTruck.FoundAtHopperID));

                    short padStatus = 0;

                    if (padStatus == 0) // Pad working
                    {
                        lock (BLUtility.truckListLock)
                        {
                            count = 0;
                            count = trucksAtPreviousPAD.Count();
                        }

                        if (count <= 1)
                        {
                            BLUtility.ModbusProcessor.WritePAD_Match(Convert.ToByte(objTruck.FoundAtHopperID), 1);
                            logSteps = logSteps + "\r\nPrevious PAD  (" + objTruck.FoundAtHopperID.ToString() + ") set to 1";
                        }
                        else if (count == 2)
                        {
                            var findTruckAtCorrectPAD = from truckAtPAD in trucksAtPreviousPAD where truckAtPAD.FoundAtHopperID == truckAtPAD.TagDB.DestinationPad && truckAtPAD.TagRFID.TagID != objTruck.TagRFID.TagID select truckAtPAD;
                            if (findTruckAtCorrectPAD.Count() == 1)
                            {
                                BLUtility.ModbusProcessor.WritePAD_Match(Convert.ToByte(objTruck.FoundAtHopperID), 1);

                                if (BLUtility.LogFile == true)
                                {
                                    logSteps = logSteps + "\r\nPrevious PAD  (" + objTruck.FoundAtHopperID.ToString() + ") set to 1";
                                }

                            }
                        }
                    }
                    else // Pad not working
                    {
                        // set pad match bit for previous pad to 0 (incorrect) 
                        BLUtility.ModbusProcessor.WritePAD_Match(Convert.ToByte(objTruck.FoundAtHopperID), 0);

                        if (BLUtility.LogFile == true)
                        {
                            logSteps = logSteps + "\r\nPrevious PAD  (" + objTruck.FoundAtHopperID.ToString() + ") set to 0 as it's not working.";
                        }

                    }


                }
            }
            catch(Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in TruckDetectedAtHopper method for {0} T when setting prev H. Thread {1}", objTruck.TagRFID.TagID, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
#if DEBUG
            Algorithms.checkPoint = "TDetH: Fin setting prev H: 4772";
#endif
            try
            {
                // }

                //string hopperNo = objTruck.FoundAtHopperID.ToString();

                //int assignedMarkerID = BLUtility.HoppersInFacility[objTruck.FoundAtHopperID.ToString()].MarkerID;
                //int tagAssignGroupID = BLUtility.HoppersInFacility[objTruck.TagDB.AssignedHopper.HopperName].GroupID;
                //int desHopperID =  objTruck.TagDB.DestinationPad;
                List<IMarkers> lstMarkers = null;

                //byte HopperID = 0;
                short value = -1;

                if (BLUtility.Markers.ContainsKey(Location.HOPPER))
                {
                    lstMarkers = BLUtility.Markers[Location.HOPPER];

                    var foundAtMarker = from objMarker in lstMarkers where objMarker.MarkerLoopID.Trim() == objTruck.TagRFID.NewMarkerID.ToString() select objMarker;

                    if (foundAtMarker != null && foundAtMarker.Count() > 0)
                    {

                        IMarkers objMarker = foundAtMarker.First();
                        //int currentPadGroupID = BLUtility.HoppersInFacility.First(x => x.Value.MarkerID == objMarker.ID).Value.GroupID;
                        int currentHopperID = BLUtility.HoppersInFacility.First(x => x.Value.MarkerID == objMarker.ID).Value.HopperID;
                        objTruck.FoundAtHopperID = Convert.ToInt32(currentHopperID);
                        //if (assignedMarkerID == objMarker.ID)

                        //Changed: 26-5-2012 : All trucks will be right trucks if their assigned pad is same as current pad
                        //if (currentHopperID == objTruck.TagDB.DestinationPad && (objTruck.EntryOrQueue == Location.ENTRY1 || objTruck.CalledUp ||
                        //   BLUtility.HoppersInFacility.ToList().FindAll(x => x.Value.GroupID == BLUtility.HoppersInFacility[objTruck.TagDB.HopperID.ToString()].GroupID).Count == 1))

                        if (currentHopperID == objTruck.TagDB.DestinationPad && !(BLUtility.TrucksInTransaction.Any(x => x.Value.LastPositiion == Location.HOPPER &&
                                                                      x.Value.FoundAtHopperID == currentHopperID && x.Value.TagDB.DestinationPad != x.Value.FoundAtHopperID)))
                        {

                            // Truck reached at Correct Hopper ( Inverting the value 0 for Wrong PAD and 1 for correct PAD )
                            //HopperID = Convert.ToByte(hopperNo);
                            //value = 0; ( Inverting the value 0 for Wrong PAD and 1 for correct PAD )

                            value = 1;

                            if (BLUtility.LogFile == true)
                            {
                                logSteps = logSteps + "\r\n Found at correct PAD (" + currentHopperID + ")";
                            }

                            try
                            {
                                BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)currentHopperID).ShowCurrentString(objTruck.TagDB.TruckID, "`C00FF00");
                            }
                            catch { }
                            //objTruck.FoundAtHopperID = objTruck.TagDB.AssignedHopper.ID;
                            //if (BLUtility.IsModbusConnected)
                            //{
                            //    short padStatus = BLUtility.ModbusProcessor.WritePAD_Match(HopperID, 0);
                            //}
                        }
                        else
                        {
                            // Truck reached at Wrong Hopper
                            // Get the Hopper ID associated with Marker 

                            if (BLUtility.LogFile == true)
                            {
                                logSteps = logSteps + "\r\n Found at wrong PAD";
                            }

                            try
                            {
                                BLUtility.ModbusProcessor.WritePAD_Start(Convert.ToByte(currentHopperID), 0);
                            }
                            catch { }
                            try
                            {
                                BLUtility.VMSDeviceList[Location.HOPPER].First(x => x.DeviceNo == (short)currentHopperID).ShowCurrentString("WRONG PAD", "`CFF0000");
                            }
                            catch { }

                            //var foundAtHopper = from objHopper in BLUtility.HoppersInFacility.Values 
                            //                    where objHopper.MarkerID == objMarker.ID select objHopper;

                            //if (foundAtHopper != null && foundAtHopper.Count() > 0)
                            //{
                            //    Hopper assoicatedHopper = foundAtHopper.First();

                            //    HopperID = Convert.ToByte(assoicatedHopper.HopperName);

                            // value = 1;( Inverting the value 0 for Wrong PAD and 1 for correct PAD )

                            value = 0;

                            if (BLUtility.LogFile == true)
                            {
                                logSteps = logSteps + "\r\n Wrong PAD (" + currentHopperID + ")";
                            }
                            //objTruck.FoundAtHopperID = assoicatedHopper.HopperID;
                            //if (BLUtility.IsModbusConnected)
                            //{
                            //    short padStatus = BLUtility.ModbusProcessor.WritePAD_Match(HopperID, 1);
                            //}
                            //}
                        }

                        // Changes made on 08/11/2010 
                        if (value >= 0)
                        {
                            // Check for multiple trucks at PAD 
                            // DateTime interval = DateTime.Now.AddSeconds(-BLUtility.MultipleTagFilterInterval);     

                            if (value == 1)
                            {
                                // check for PAD status
                                // short padStatus = BLUtility.ModbusProcessor.ReadPAD_Status(Convert.ToByte(HopperID));

                                short padStatus = 0;

                                if (padStatus == 0)
                                {
                                    // check for multiple trucks at PAD when found at correct PAD
                                    //23-2-2012 Bhavesh: 
                                    //var trucksAtPAD = from aTruck in BLUtility.TrucksInTransaction.Values where aTruck.FoundAtHopperID == Convert.ToInt32(HopperID) && aTruck.LastPositiion == Location.HOPPER && aTruck.TagRFID.TagID != objTruck.TagRFID.TagID select aTruck; // && aTruck.LastReadTime >= interval -- && 

                                    var trucksAtPAD = from aTruck in BLUtility.TrucksInTransaction.Values
                                                      where aTruck.FoundAtHopperID == currentHopperID &&
                                                      aTruck.LastPositiion == Location.HOPPER &&
                                                      aTruck.TagDB.DestinationPad != aTruck.FoundAtHopperID
                                                      select aTruck; // && aTruck.LastReadTime >= interval -- && 

                                    lock (BLUtility.truckListLock)
                                    {
                                        count = 0;
                                        count = trucksAtPAD.Count();
                                    }

                                    if (count > 0)
                                    {
                                        if (BLUtility.LogFile == true)
                                        {
                                            logSteps = logSteps + "\r\n Multiple Trucks found at PAD (" + currentHopperID.ToString() + ")";
                                        }

                                        // value = 1;( Inverting the value 0 for Wrong PAD and 1 for correct PAD )
                                        value = 0;
                                    }
                                }
                                else
                                {
                                    if (BLUtility.LogFile == true)
                                    {
                                        logSteps = logSteps + "\r\n PAD  (" + currentHopperID.ToString() + ") set to 0 as it's not working.";
                                    }
                                    value = 0;
                                }
                            }

                            lock (BLUtility.hopperAreaLock)
                            {
                                if (BLUtility.IsModbusConnected)
                                {
                                    BLUtility.ModbusProcessor.WritePAD_Match(Convert.ToByte(currentHopperID), value);

                                    if (BLUtility.LogFile == true)
                                    {
                                        logSteps = logSteps + "\r\n PAD (" + currentHopperID.ToString() + ") set to value " + value.ToString();
                                    }
                                }

                                objTruck.FoundAtHopperID = Convert.ToInt32(currentHopperID);
                                // objTruck.TagDB.DestinationPad = Convert.ToInt32(HopperID);
                            }
                        }

                    }
                    else
                    {
                        if (BLUtility.LogFile == true)
                        {
                            logSteps = logSteps + "\r\n No marker found with LoopID " + objTruck.TagRFID.NewMarkerID.ToString();
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                if (BLUtility.LogFile == true)
                {
                    logSteps = logSteps + "\r\n Exception Occured -- " + ex.Message;
                }
#if DEBUG
                log.ErrorException(String.Format("Exception thrown in TruckDetectedAtHopper for {0} truck FoundAt{1}, Des{2} hopper. Thread is: {3}", objTruck.TagRFID.TagID, objTruck.FoundAtHopperID, objTruck.TagDB.DestinationPad,
                    Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
            finally
            {
#if DEBUG
                log.Debug("check point in TruckDetectedAtHopper method {0}. Thread : {1}", Algorithms.checkPoint, Thread.CurrentThread.ManagedThreadId.ToString());
                Algorithms.LogAllTruckHoppers(log);
#endif
            }

            try
            {
                if (BLUtility.HoppersInFacility.ContainsKey(prevHopper.ToString()))
                    BLUtility.HoppersInFacility[prevHopper.ToString()].Startup_ShutDownPAD(Location.HOPPER);
            }
            catch(Exception ex) {
#if DEBUG
                log.ErrorException(String.Format("Exception thrown when calling Startup_ShutDownPad in TruckDetectedAtHopper method for {0} truck {1} pad. Thread is: {2}",
                    objTruck.TagRFID.TagID, prevHopper, Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }

            if (objTruck.FoundAtHopperID != 0 && BLUtility.HoppersInFacility.ContainsKey(objTruck.FoundAtHopperID.ToString()))
            {
                BLUtility.HoppersInFacility[objTruck.FoundAtHopperID.ToString()].Startup_ShutDownPAD(Location.HOPPER);
            }
            else if (objTruck.TagDB.DestinationPad != 0 && BLUtility.HoppersInFacility.ContainsKey(objTruck.TagDB.DestinationPad.ToString()))
            {
                BLUtility.HoppersInFacility[objTruck.TagDB.DestinationPad.ToString()].Startup_ShutDownPAD(Location.HOPPER);
            }

            if (BLUtility.LogFile == true && logSteps != "")
            {
                WriteLogs(logSteps);
            }

        }

        #endregion

        #region TruckWash/TagReturn Procedure

        #endregion

        static void WriteLogs(string strLog)
        {
            try
            {
                if (!System.IO.Directory.Exists(@"C:\Logging"))
                {
                    System.IO.Directory.CreateDirectory(@"C:\Logging");
                }

                string fileName = "Logs_" + DateTime.Now.ToString("dd-MM-yyyy");


                System.IO.StreamWriter wr = new System.IO.StreamWriter(@"C:\Logging\" + fileName + ".log", true);

                wr.WriteLine("--------------------------------------" + DateTime.Now.ToString() + "--------------------------------------");
                wr.WriteLine(strLog);
                wr.WriteLine("------------------------------------------------------------------------------------------------------------");

                wr.Flush();
                wr.Close();

                //finfo.

            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException(string.Format("Exception thrown in WriteLogs method in Algorithms. Thread {0}", Thread.CurrentThread.ManagedThreadId.ToString()), ex);
#endif
            }
        }

    }
}
