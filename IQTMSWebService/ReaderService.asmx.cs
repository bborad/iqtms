


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Ramp.IdentacReaderLib.Readers;
using RMC = Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.RFIDHardwareConnector;
using Ramp.Modbus;
using System.Data;
using Ramp.IdentacReaderLib.Common;
using System.IO;
using System.Configuration;

using NLog;
using System.Net;
using System.Xml.Serialization;
using System.Text;

namespace Ramp.IQTMSWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ReaderService : System.Web.Services.WebService
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        /*
         *Date: 28-10-2011 
         *Modified By: Bhavesh
         *Change: create new method to read from REDS csv file
         *Detail: 
         *
         */
        private List<RMC.RFIDTag> GetREDSTag(string ipaddress)
        {
            List<RMC.RFIDTag> lstTags = new List<RMC.RFIDTag>();
            string tags = "";
            string fname = @"C:\" + ipaddress + "Tags.csv";
            // File.AppendAllText(@"C:\ReaderServiceLog.txt",System.Environment.NewLine + "Entered into GetREDSTags");
            if (System.IO.File.Exists(fname))
            {
                try
                {
                    using (Stream f = File.Open(fname, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                    {
                        if (f != null)
                        {
                            StreamReader sr = new StreamReader(f); 
                            tags = sr.ReadToEnd();
                            f.SetLength(0);
                            sr.Close();
                            sr.Dispose();
                        }
                    }
          // File.AppendAllText(@"C:\ReaderServiceLog.txt", System.Environment.NewLine + "Finished reading file. Found: "+tags);
                    
                    foreach (string tag in tags.Split('\n'))
                    {
 
                        string[] tagProperties = tag.Split(',');
                        if (tagProperties != null)
                        {
                            RMC.RFIDTag t = new RMC.RFIDTag();
                            if (tagProperties.Length > 0)
                            {
                                t.TagID = tagProperties[0];
                                t.SerialLabel = tagProperties[0];
                            }
                            if (tagProperties.Length > 1)
                                int.TryParse(tagProperties[1].Replace("\r",""), out t.NewMarkerID);
                            if (tagProperties.Length > 2)
                                int.TryParse(tagProperties[2].Replace("\r", ""), out t.OldMarkerID);
                             
                                t.MarkerNewPositionTime = DateTime.Now;
                             
                                t.MarkerOldPositionTime = DateTime.Now.AddHours(-5);
                             
                                t.TimeFirstSeen = DateTime.Now;
                             
                                t.TimeLastSeen = DateTime.Now;
                            
                            t.BatteryStatus = Ramp.MiddlewareController.Common.TagBatteryStatus.Good;
                            
                             
                            if(t.TagID.Trim() != "" )
                                    lstTags.Add(t);
                        }
                    }
                }
                catch(Exception ex)
                {
                    File.AppendAllText(@"C:\ReaderServiceLog.txt",System.Environment.NewLine + "Exception thrown: "+ex.Message);
                    logger.Info("ExceptionThrown in webservice ", ex);
                }
            }
            return lstTags;
        }

        //Added for test hook 09/12/2011
        //internal RMC.RFIDTag fromXML(String xmlstr)
        //{
        //    try
        //    {
        //        xmlstr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlstr;
        //        RMC.RFIDTag temp = new RMC.RFIDTag();
        //        MemoryStream m = new MemoryStream(UTF8Encoding.UTF8.GetBytes(xmlstr));

        //        XmlSerializer ser = new XmlSerializer(typeof(RMC.RFIDTag));

        //        temp = (RMC.RFIDTag)ser.Deserialize(m);

        //        return temp;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new RMC.RFIDTag();
        //    }
        //}
        //

        [WebMethod]
        public List<RMC.RFIDTag> GetTags(string IpAddress, int PortNo)
        {
            List<RMC.RFIDTag> lstTags = new List<RMC.RFIDTag>();

            /*
             *Date: 26-10-2011 
             *Modified By: Bhavesh
             *Change: added condition to run in test mode (reading tags from .csv for REDS reader)while testing in office
             *Detail: if TESTING is define it will not request reader for tag list but will read tags from .csv file.
             *        file name should be REDSTag.csv in C drive
             *        this file will contain list of tags with all required detail separated by comma in folloing order.
             *        TagID,SerialLabel,SerialNo,NewMarkerID,OldMarkerID,MarkerNewPositionTime,MarkerOldPositionTime,TimeFristSeen,
             *        TimeLastSeen,BatteryStatus,MarkerName
             */
    #if TESTING

            #region reading from csv file
            try
            {
                 
                    return GetREDSTag(IpAddress);
                 
            }
            catch
            {
                return lstTags;
            }
            #endregion

    #endif

            //System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
            //client.Connect(Environment.MachineName, 2000);

            //byte[] buffer = new byte[2000];

            //client.GetStream().Read(buffer, 0, 2000);

            //string recievedData = Encoding.Unicode.GetString(buffer);

           // ConvertToRFIDTag(recievedData);



            #region Reading from the reader
            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];

                WebClient client = new WebClient();
                String response = "";
                try
                {
                    logger.Info("{0} request Entering GetTags:", IpAddress);
                    if (System.Configuration.ConfigurationManager.AppSettings["TagReadLogging"].ToLower() == "true")
                        File.AppendAllText(@"C:\TMSReaderServiceLog_" + IpAddress + ".txt", "-------------------" + DateTime.Now.ToString() + "   :   " + IpAddress + "   :   " + "---------------------\n" +
                            System.Environment.NewLine + "Before requesting for tag read" + System.Environment.NewLine);

                    client.Headers.Add("user-agent", @"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    response = client.UploadString(ConfigurationManager.AppSettings["TestSimulator"], @"<readerIP>" + IpAddress + @"</readerIP>");
                    logger.Info("{0} received response:[ {1} ] in GetTags:", IpAddress, response.ToString());
                    if (System.Configuration.ConfigurationManager.AppSettings["TagReadLogging"].ToLower() == "true")
                        File.AppendAllText(@"C:\TMSReaderServiceLog_" + IpAddress + ".txt", "-------------------" + DateTime.Now.ToString() + "   :   " + IpAddress + "   :   " + "---------------------\n" +
                            System.Environment.NewLine + response.ToString() + System.Environment.NewLine);

                }
                catch (WebException ex)
                {
                    File.AppendAllText(@"C:\TMSReaderSerivceErrorLog_" + IpAddress + ".txt", "-------------------" + DateTime.Now.ToString() + "   :   " + IpAddress + "   :   " + "---------------------\n" +
                            System.Environment.NewLine + "Exception thrown: " + ex.Message + System.Environment.NewLine);
                }

                lock (ipLock)
                {
                    
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);

                    if (objReader != null && objReader.IsConnected)
                    {

                        objReader.clearTagList = true;
                        lstTags = objReader.GetTags();

                    }
                     

                    string[] lines = response.Split(new char[] { '\n' });

                    foreach (string line in lines)
                    {
                        if (string.IsNullOrEmpty(line))
                            continue;

                        string[] element = line.Split(new char[] { ',' });

                        try
                        {

                            RMC.RFIDTag temp = new Ramp.MiddlewareController.Common.RFIDTag();

                            temp.TagID = element[0];
                            temp.SerialLabel = element[1];
                            temp.SerialNo = Int32.Parse(element[2]);
                            temp.NewMarkerID = Int32.Parse(element[3]);
                            temp.OldMarkerID = Int32.Parse(element[4]);
                            temp.MarkerNewPositionTime = DateTime.Parse(element[5]);
                            temp.MarkerOldPositionTime = DateTime.Parse(element[6]);
                            temp.TimeFirstSeen = DateTime.Parse(element[7]);
                            temp.TimeLastSeen = DateTime.Parse(element[8]);
                            temp.BatteryStatus = Ramp.MiddlewareController.Common.TagBatteryStatus.Good;
                            temp.MarkerName = element[10];

                            lstTags.Add(temp);
                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText(@"C:\TMSReaderSerivceErrorLog_" + IpAddress + ".txt", "-------------------" + DateTime.Now.ToString() + "   :   " + IpAddress + "   :   " + "---------------------\n" +
                           System.Environment.NewLine + "Exception thrown: " + ex.Message + System.Environment.NewLine);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\TMSReaderSerivceErrorLog_" + IpAddress + ".txt", "-------------------" + DateTime.Now.ToString() + "   :   " + IpAddress + "   :   " + "---------------------\n" +
                            System.Environment.NewLine + "Exception thrown: " + ex.Message + System.Environment.NewLine);
            }
            #endregion




            return lstTags;

        }

        //private List<RMC.RFIDTag> ConvertToRFIDTag(string taglist)
        //{
        //    List<RMC.RFIDTag> tlist = new List<Ramp.MiddlewareController.Common.RFIDTag>();
        //    try
        //    {
        //        foreach (string tag in taglist.Replace("\r", "").Split('\n'))
        //        {
        //            string[] fields = tag.Split(',');
        //            RMC.RFIDTag rfidtag = new Ramp.MiddlewareController.Common.RFIDTag();

        //            if (fields.Length > 0)
        //                rfidtag.SerialLabel = rfidtag.TagID = rfidtag.SerialNo = fields[0];
        //            if (fields.Length > 1)
        //                rfidtag.NewMarkerID = (int)fields[1];
        //            if (fields.Length > 2)
        //                rfidtag.MarkerNewPositionTime = rfidtag.TimeFirstSeen = rfidtag.TimeLastSeen = Convert.ToDateTime(fields[2]);
        //            if (fields.Length > 3)
        //                rfidtag.OldMarkerID = (int)fields[3];
        //            if (fields.Length > 4)
        //                rfidtag.MarkerOldPositionTime = Convert.ToDateTime(fields[4]);
        //            rfidtag.BatteryStatus = Ramp.MiddlewareController.Common.TagBatteryStatus.Good;

        //            tlist.Add(rfidtag);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        File.AppendAllText(@"C:\IQReaderServiceLog.txt",System.Environment.NewLine+System.DateTime.Now.ToString()+
        //            " Exception thrown while converting csv to rfid tag.:");
        //    }
        //    return tlist;

        //}

        [WebMethod]
        public List<RMC.RFIDTag> ReadTags(string IpAddress, int PortNo)
        {
            return GetTags(IpAddress, PortNo);

            List<RMC.RFIDTag> lstTags = new List<RMC.RFIDTag>();
            /*
            *Date: 26-10-2011 
            *Modified By: Bhavesh
            *Change: added condition to run in test mode (reading tags from .csv for REDS reader)while testing in office
            *Detail: if TESTING is define it will not request reader for tag list but will read tags from .csv file.
            *        file name should be REDSTag.csv in C drive
            *        this file will contain list of tags with all required detail separated by comma in folloing order.
            *        TagID,SerialLabel,SerialNo,NewMarkerID,OldMarkerID,MarkerNewPositionTime,MarkerOldPositionTime,TimeFristSeen,
            *        TimeLastSeen,BatteryStatus,MarkerName
           
#if TESTING

            #region reading from csv file
            try
            {
                if ((!string.IsNullOrEmpty(IpAddress)) && IpAddress == "192.168.168.91")
                {
                    return GetREDSTag();
                }
            }
            catch
            {
                return lstTags;
            }
            #endregion

#endif
            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);

                    if (objReader != null && objReader.IsConnected)
                    {

                        objReader.clearTagList = false;
                        lstTags = objReader.GetTags();

                        

                        

                    }
                }
            }
            catch
            {
            }

            return lstTags;
             */
            File.AppendAllText(@"C:\TMSReaderSerivceErrorLog.txt", "-------------------" + DateTime.Now.ToString() + "---------------------" +
                            System.Environment.NewLine + "Calling not implemented method: ReadTags "+ System.Environment.NewLine);
        }

        [WebMethod]
        public List<RMC.RFIDTag> ScanForTags(string IpAddress, int PortNo, int count)
        {
            List<RMC.RFIDTag> lstTags = new List<RMC.RFIDTag>();
            /*
            *Date: 26-10-2011 
            *Modified By: Bhavesh
            *Change: added condition to run in test mode (reading tags from .csv for REDS reader)while testing in office
            *Detail: if TESTING is define it will not request reader for tag list but will read tags from .csv file.
            *        file name should be REDSTag.csv in C drive
            *        this file will contain list of tags with all required detail separated by comma in folloing order.
            *        TagID,SerialLabel,SerialNo,NewMarkerID,OldMarkerID,MarkerNewPositionTime,MarkerOldPositionTime,TimeFristSeen,
            *        TimeLastSeen,BatteryStatus,MarkerName
            */
#if TESTING

            #region reading from csv file
            try
            {
                if ((!string.IsNullOrEmpty(IpAddress)) && IpAddress == "192.168.168.91")
                {
                    return GetREDSTag(IpAddress);
                }
            }
            catch
            {

            }
            #endregion

#endif

            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                    if (objReader != null && objReader.IsConnected)
                    {

                        lstTags = objReader.ScanForTags(count);

                    }
                }
            }
            catch
            {
            }

            return lstTags;

        }

        [WebMethod]
        public bool SearchTag(string IpAddress, int PortNo, string TagRFID)
        {
            bool flagResult = false;

            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                    if (objReader != null && objReader.IsConnected)
                    {

                        flagResult = objReader.SearchTag(TagRFID);

                    }
                }
            }
            catch
            {
            }

            return flagResult;


        }

        [WebMethod]
        public bool IsReaderConnected(string IpAddress, int PortNo)
        {
            bool flagResult = false;

            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                    if (objReader != null && objReader.IsConnected)
                    {

                        flagResult = objReader.IsReaderConnected();

                    }
                }
            }
            catch
            {
            }

            return flagResult;


        }

        [WebMethod]
        public bool BlinkLED(string IpAddress, int PortNo, string tagToBlink, int milliseconds)
        {
            bool flagResult = false;

            RMC.TagLedColor ledColor = RMC.TagLedColor.RED;
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, milliseconds);

            try
            {
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                    if (objReader != null && objReader.IsConnected)
                    {
                        //object ipLock = IdentacReaderUtility.lstIpAddressLocks[IpAddress + ":" + PortNo.ToString()];
                        //lock (ipLock)
                        //{
                        flagResult = objReader.BlinkLED(tagToBlink, ledColor, ts);
                        //}
                    }
                }
            }
            catch
            {
            }

            return flagResult;


        }


        [WebMethod]
        public bool BlinkLEDWithColor(string IpAddress, int PortNo, string tagToBlink, RMC.TagLedColor ledColor, int milliSeconds, int totalBlinks)
        {
            bool flagResult = false;

            try
            {
                TimeSpan tts = new TimeSpan(0, 0, 0, 0, milliSeconds);
                object ipLock;

                string key = IpAddress + ":" + PortNo.ToString();

                if (!IdentacReaderUtility.lstIpAddressLocks.ContainsKey(key))
                {
                    IdentacReaderUtility.lstIpAddressLocks.Add(key, new object());
                }

                ipLock = IdentacReaderUtility.lstIpAddressLocks[key];
                lock (ipLock)
                {
                    ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                    if (objReader != null && objReader.IsConnected)
                    {

                        flagResult = objReader.BlinkLED(tagToBlink, ledColor, tts, totalBlinks);

                    }
                }

            }
            catch
            {
            }

            return flagResult;


        }

        [WebMethod]
        public void ClearTagList(string IpAddress, int PortNo)
        {

            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                objReader.ClearTagList();
            }
            catch
            {
            }


        }

        [WebMethod]
        public void Dispose(string IpAddress, int PortNo)
        {

            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                objReader.Dispose();
            }
            catch
            {
            }


        }

        [WebMethod]
        public string GetSerialNo(string IpAddress, int PortNo)
        {
            string result = "";
            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                if (objReader != null && objReader.IsConnected)
                {
                    result = objReader.GetSerialNo();
                }
            }
            catch
            {
            }

            return result;

        }

        [WebMethod]
        public string GetReaderStatus(string IpAddress, int PortNo)
        {
            string result = "";
            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                if (objReader != null && objReader.IsConnected)
                {
                    result = objReader.GetReaderStatus();
                }
            }
            catch
            {
            }

            return result;

        }

        [WebMethod]
        public void SetTxPower(string IpAddress, int PortNo, int TxPower)
        {
            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                objReader.TxPower = TxPower;
            }
            catch
            {
            }
        }

        [WebMethod]
        public int GetTxPower(string IpAddress, int PortNo)
        {
            int returnValue = 0;
            try
            {
                ReaderIPortM350 objReader = ReaderIPortM350.GetReader(IpAddress, PortNo);
                if (objReader != null && objReader.IsConnected)
                {
                    returnValue = objReader.TxPower;
                }
            }
            catch
            {
            }
            return returnValue;
        }

        [WebMethod]
        public void ConfigureReaders(DataTable dtReaders)
        {
            try
            {
                ReaderIPortM350.ConfigureReaders(dtReaders);

            }
            catch
            {
            }
        }

        [WebMethod]
        public DataTable GetMarkersStatus()
        {
            DataTable dtMarkers = new DataTable("dtMarkers");
            try
            {
                dtMarkers = IdentacReaderLib.Common.IdentacReaderUtility.GetMarkersStatus();

            }
            catch
            {
            }
            return dtMarkers;
        }

        [WebMethod]
        public DataTable SetMarkersLoopID(DataTable dtMarkers)
        {
            DataTable dtMarkerInfo = null;
            try
            {
                dtMarkerInfo = IdentacReaderLib.Common.IdentacReaderUtility.SetMarkersLoopID(dtMarkers);
            }
            catch
            {
            }
            return dtMarkerInfo;
        }

        [WebMethod]
        public DataTable GetMarkersStatusAtLocation(int location)
        {
            DataTable dtMarkers = new DataTable("dtMarkers");
            try
            {
                dtMarkers = IdentacReaderLib.Common.IdentacReaderUtility.GetMarkersStatus(location);

            }
            catch
            {
            }
            return dtMarkers;
        }

        [WebMethod]
        public bool SetPingTimeOut(int timeOut)
        {
            bool result = false;
            try
            {
                IdentacReaderUtility.pingTimeOut = timeOut;
                result = true;
            }
            catch
            {
            }
            return result;
        }

        [WebMethod]
        public int GetPingTimeOut()
        {

            try
            {
                return IdentacReaderUtility.pingTimeOut;

            }
            catch
            {
                return -1;
            }

        }

        # region --- Code not in use  ----------
        // [WebMethod]
        public short ReadBoom_Gate(byte theGate)
        {
            short result = -1;
            try
            {
                //  ModbusServer mServer = new ModbusServer();
                //  result = mServer.ReadBoom_Gate(theGate);
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public void SetTCPPort(int thePort)
        {

            try
            {
                // ModbusServer mServer = new ModbusServer();

                // mServer.SetTCPPort(thePort);
            }
            catch
            {
            }

        }

        // [WebMethod]
        public byte StopModbusServer()
        {
            byte result = 0;
            try
            {
                //  ModbusServer mServer = new ModbusServer();
                //    result = mServer.StopModbusServer();
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public short ReadPAD_Match(byte thePAD)
        {
            short result = -1;
            try
            {
                // ModbusServer mServer = new ModbusServer();

                // result = mServer.ReadPAD_Match(thePAD);
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public short ReadPAD_Start(byte thePAD)
        {
            short result = -1;
            try
            {
                // ModbusServer mServer = new ModbusServer();

                //result = mServer.ReadPAD_Start(thePAD);
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public short ReadPAD_Status(byte thePAD)
        {
            short result = -1;
            try
            {
                // ModbusServer mServer = new ModbusServer();

                // result = mServer.ReadPAD_Status(thePAD);
            }
            catch
            {
            }
            return result;
        }

        //[WebMethod]
        public byte WriteBoom_Gate(byte theGate, short theValue)
        {
            byte result = 0;
            try
            {
                //ModbusServer mServer = new ModbusServer();

                //result = mServer.WriteBoom_Gate(theGate, theValue);
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public byte WritePAD_Match(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                //ModbusServer mServer = new ModbusServer();

                //result = mServer.WritePAD_Match(thePAD, theValue);
            }
            catch
            {
            }
            return result;
        }

        // [WebMethod]
        public byte WritePAD_Start(byte thePAD, short theValue)
        {
            byte result = 0;
            try
            {
                //ModbusServer mServer = new ModbusServer();

                //result = mServer.WritePAD_Start(thePAD, theValue);
            }
            catch
            {
            }
            return result;
        }

        #endregion --- Code not in use  ----------
    }
}
