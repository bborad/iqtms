

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;


using VMSLibrary;
using Ramp.VMSHWConnector.Common;
using Ramp.MiddlewareController.Connector.VMSHardWareConnector;
using Ramp.MiddlewareController.Common;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;


using NLog;


namespace Ramp.VMSHWConnector.Adapter
{
    public class VMSDevice : IVMSDevice
    {
#if DEBUG

        private Logger log = LogManager.GetCurrentClassLogger();


#endif



        public short DeviceNo { get; set; }
        private string ipAddress;
        public string IPAddress
        {

            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
                log = LogManager.GetLogger("VMSLog_" + ipAddress);

            }
        }
        public bool SCL2008 { get; set; }
        public string Password { get; set; }
        public short UDPPort { get; set; }

        public short LedNum { get; set; } // value 0
        public short ComPort { get; set; } // value 1     
        public int Location { get; set; }

        public short LedWidth { get; set; } // value 128
        public short LedHeight { get; set; } // value 32  

        public short ledMaxSize;

        public short TextColor { get; set; } // value 255
        public short AscFont { get; set; }
        public short TextStartPosition { get; set; }
        public short TextLength { get; set; }

        public int SpaceFieldLength { get; set; }

        private short RandomPort { get; set; }
        public string VMSLogPath { get; set; }

        public bool UseRandomPortNo { get; set; }

        public short MinUDPPortNo { get; set; }

        public short MaxUDPPortNo { get; set; }

        private int LastPortNoUsed;

        int StatusUpdateCheck;

        int maxStringLength = 50;

        public int MessageTimeOut { get; set; }

        public string DefaultMessage { get; set; }

        Timer tmrRefreshScreen = new Timer();

        public object myVMSLock = new object();


        public List<string> currentMessage { get; set; }

        //private static Socket tcpListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        //private static bool firstMessge = true;
        private string lastmsg = "";
        private string curdismsg="";
         private readonly object lockerVMSMessage = new object();
        private readonly object lockerQueueMsg = new object();
        //private static readonly object vmslocker = new object();
        private System.Windows.Forms.MethodInvoker messageList;
        private bool methodCalled = false;
        private bool init  = false; 
        VMS objVMS;
        //private int pingTimeout = 3000;
        
        private string blankScreen ="";

        private void SendVMSMessage_SignVMS()
        {
          

#if DEBUG
               log.Debug("Entered into SendVMSMessage method with {0} messages for {1} vms. Thread {2}", lastmsg, this.ipAddress, 
                    System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif


               bool res = false;
            lock (lockerVMSMessage)
             {

                if(string.IsNullOrEmpty(blankScreen))
                    BuildClearScreenString();

                 if (string.IsNullOrEmpty(lastmsg))
                    {
                        methodCalled = false;
    #if DEBUG
                         log.Info("Exiting SendVMSMessage (lastmsag is empty) for {0} vms in Thread {1}", IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
    #endif
                        return  ;
                    }
                        
            
                    #region Removed. VMS object has to initialised on start
                                    //if (UseRandomPortNo)
                                    //    {
                                    //        Random rdn = new Random();

                                    //        do
                                    //        {
                                    //            RandomPort = Convert.ToInt16(rdn.Next(MinUDPPortNo, MaxUDPPortNo));
                                    //        }
                                    //        while (RandomPort == LastPortNoUsed);

                                    //        LastPortNoUsed = RandomPort;
                                        
                                    //        //objVMS = new VMS(DeviceNo,LedWidth,LedHeight,IPAddress,RandomPort,10,5);
                                    //    }
                    #endregion
                                    
                try
                {

#if DEBUG
                   log.Debug("Sending  to {0} vms. Thread {1}",   IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                 
                       
                        
                       

                       

                        #region get message to display
                        lock (lockerQueueMsg)
                        {
                            //Display message even though it's same as prev. 
                            //this will show that vms is still working if was restarted 
//                            if (curdismsg == lastmsg)
//                            {
//#if DEBUG
//                                log.Debug("Current message and new message are same at {0} VMS",IPAddress);
//#endif 
//                                methodCalled = false;
//                                return  ;
//                            }
                            curdismsg = lastmsg;
                            methodCalled = false;
                        }
                        #endregion

                        if (string.IsNullOrEmpty(blankScreen))
                            BuildClearScreenString();

                            res = objVMS.ShowString(TextStartPosition, 0, TextColor, AscFont, blankScreen);

                            res = objVMS.ShowString(TextStartPosition, 0, TextColor, AscFont, curdismsg.Replace("'", "`"));
                        
                        
#if DEBUG
                        log.Debug("Sent message {0} to {1} vms. :Res {2} Thread {3}", curdismsg.Replace("'", "`"), IPAddress, res, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    UpdateVMSConnStatus(res);

                }
                catch (Exception ex)
                {
                    methodCalled = false;
#if DEBUG
                    log.Error("Exception thrown in SendVMSMessage method with {0} message for {1} vms. Object initialised (SCL_NetInitial) {3}. Error: {2}", curdismsg.Replace("'", "`"), IPAddress, ex.Message,init.ToString());
#endif
                }

            }//release lock

            
             
        }


//        public void ReadDefMsgFromConfig()
//        {
//            try
//            {
//                object value = System.Configuration.ConfigurationSettings.AppSettings["VMSWrongTruckAtPad"];
//                if (!string.IsNullOrEmpty(value.ToString()))
//                    wrongtruckmsg = value.ToString();

//                  value = System.Configuration.ConfigurationSettings.AppSettings["VMSPadClosed"];
//                if (!string.IsNullOrEmpty(value.ToString()))
//                    closedmsg = value.ToString();
//            }
//            catch (Exception ex)
//            {
//#if DEBUG 
//                log.ErrorException("Exception thrown when reading WrongTrucAtPad and PadClosed msg from config file.",ex);
//#endif
//            }
//        }

        public VMSDevice()
        {
            messageList = new System.Windows.Forms.MethodInvoker(SendVMSMessage_SignVMS);
            int workth, ioth;
            System.Threading.ThreadPool.GetMaxThreads(out workth, out ioth);
            if (workth < 50 || ioth < 50)
                System.Threading.ThreadPool.SetMaxThreads(50, 50);

            tmrRefreshScreen.Elapsed += new ElapsedEventHandler(tmrRefreshScreen_Elapsed);
            StatusUpdateCheck = -1;
            maxStringLength = 50;
            currentMessage = new List<string>();
            try
            {
                object value = System.Configuration.ConfigurationSettings.AppSettings["SpaceFieldLength"];

                if (value != null)
                {
                    SpaceFieldLength = Convert.ToInt32(value);
                }
                else
                {
                    SpaceFieldLength = 0;
                }


              
                value = System.Configuration.ConfigurationSettings.AppSettings["VMSLogPath"];
                if (value != null)
                {
                    VMSLogPath = Convert.ToString(value);
                }
                else
                {
                    VMSLogPath = "";
                }
            }
            catch
            {
#if DEBUG
                log.Error("Exception thrown in VMSDevice constructor for {0} vms while reader SpaceFieldLength, VMSLogPath and VMSPingTimeoutInMs from config", IPAddress);
#endif

            }
            UseRandomPortNo = false;
            MinUDPPortNo = 1000;
            MaxUDPPortNo = 25000;
            LastPortNoUsed = 0;

            try
            {
                if (VMSLogPath.Length > 0)
                {
                    if (!System.IO.Directory.Exists(VMSLogPath))
                    {
                        System.IO.Directory.CreateDirectory(VMSLogPath);
                    }
                }
            }
            catch
            {
                VMSLogPath = "";
            }
   

            tmrRefreshScreen.Interval = 60 * 1000;
            tmrRefreshScreen.Start();

        }

        public bool VMSInit()
        {
                
            objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, UDPPort, 10, 5);
            init = objVMS.VMSInit();
#if DEBUG
            log.Debug("VMS {0} initialised with {2} port {3} devid : {1}",IPAddress, init.ToString(),UDPPort.ToString(),DeviceNo.ToString());
#endif
            return init;
        }
        private void BuildClearScreenString()
        {
                try
                {
                    

                    string blankString = "`C000000";
                    blankString = blankString.PadRight(blankString.Length + (int)TextLength, ' ');

                    

                    for (int index = 0; index < Convert.ToInt32(LedHeight /((LedWidth  / TextLength)*2))  ; index++)
                    {
                        blankScreen += blankString+System.Environment.NewLine;
                        
                    }
                }
                catch(Exception ex) {  
#if DEBUG
                    log.ErrorException("Exception thrown while creating string to clear screen",ex);
#endif
                }
        }

        void tmrRefreshScreen_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                tmrRefreshScreen.Stop();

                DisplayDefaultMessage();

            }
            catch
            {
            }
            if (!tmrRefreshScreen.Enabled)
                tmrRefreshScreen.Start();
        }

        //public VMSDevice(short deviceNo, short ledWeigth, short ledHeight, string ipAddress, short udpPort, string password, bool scl2008)
        //{
        //    messageList = new System.Windows.Forms.MethodInvoker(SendVMSMessage_SignVMS);
        //    int workth, ioth;
        //    System.Threading.ThreadPool.GetMaxThreads(out workth, out ioth);
        //    if (workth < 50 || ioth < 50)
        //        System.Threading.ThreadPool.SetMaxThreads(50, 50);

        //    LedHeight = ledHeight;
        //    LedWidth = ledWeigth;
        //    DeviceNo = deviceNo;
        //    IPAddress = ipAddress;
        //    UDPPort = udpPort;
        //    Password = password;
        //    SCL2008 = scl2008;
        //    StatusUpdateCheck = -1;
        //    maxStringLength = 50;
        //}

        public bool Open()
        {
            bool flagResult = false;

            try
            {
                //flagResult = SCLAPI.SCLNetInitial(DeviceNo, Password, IPAddress, VMSHWUtility.TimeOut, VMSHWUtility.RetryTimes, UDPPort, SCL2008);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public bool Disconnect()
        {
            bool flagResult = false;
            try
            {
                objVMS.CloseDevice();
                // flagResult = SCLAPI.SCLClose(DeviceNo);
            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException("Exception thrown when closing VMSDevice "+IPAddress ,ex);
#endif
            }
            return flagResult;
        }

        #region disabled
        //private bool ShowString(short Left, short Top, short XPos, short YPos, short Color, short ASCFont, string Str_)
        //{
        //    bool flagResult = false;
        //    //try
        //    //{
        //    //    TextInfoType textInfo = new TextInfoType();
        //    //    textInfo.Left_ = Left;
        //    //    textInfo.Top_ = Top;
        //    //    textInfo.Height_ = LedHeight;
        //    //    textInfo.Width_ = LedWidth;
        //    //    textInfo.XPos = XPos;
        //    //    textInfo.YPos = YPos;

        //    //    textInfo.Color = Color;
        //    //    textInfo.ASCFont = ASCFont;

        //    //    if(SCL2008)
        //    //    {
        //    //        ledMaxSize = 4096;                   
        //    //    }
        //    //    else
        //    //    {
        //    //        ledMaxSize = 960;                    
        //    //    }

        //    //    textInfo.Left_ = (short)(ledMaxSize - LedWidth);

        //    //    flagResult = SCLAPI.SCLShowString(DeviceNo, textInfo, Str_);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //}
        //    return flagResult;
        //}

        //public bool _ShowString(string Str_, string formatInfo)
        //{
        //    bool flagResult = false;
        //    try
        //    {     

        //        short YPos = 0;
        //        lock (myVMSLock)
        //        {
        //            Str_ = formatInfo + Str_;

        //            int index1 = currentMessage.FindIndex(x => x == Str_);
        //            if (index1 >= 0)
        //            {
        //                currentMessage.RemoveAt(index1);
        //            }

        //            currentMessage.Insert(0, Str_);

        //            if ((currentMessage.Count * TextLength) > LedHeight)
        //            {
        //                int startIndex = Convert.ToInt32(LedHeight / TextLength);
        //                currentMessage.RemoveRange(startIndex, currentMessage.Count - startIndex);
        //            }

        //            VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, UDPPort, 10, 5);

        //            StringBuilder toDisplay; 

        //            for (int index = 0; index < currentMessage.Count; index++)
        //            {
        //                YPos = Convert.ToInt16(index * TextLength);

        //                toDisplay = new StringBuilder(currentMessage[index]);

        //                if (maxStringLength > toDisplay.Length)
        //                    toDisplay.Append(' ', maxStringLength - currentMessage[index].Length);

        //                if (flagResult == false)
        //                    flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());
        //                else
        //                    objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());

        //               // ShowString(toDisplay.ToString() + " -- On VMS At  " + DateTime.Now.ToString());
        //            }
        //        }

        //        tmrRefreshScreen.Stop();
        //        tmrRefreshScreen.Interval = MessageTimeOut;
        //        tmrRefreshScreen.Start();

        //        if (flagResult == true)
        //        {
        //            if (StatusUpdateCheck != 0) // 0 for Status "OK"
        //            {
        //                StatusUpdateCheck = 0;
        //                System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
        //                System.Threading.ThreadPool.QueueUserWorkItem(updateJob, "ok");
        //            }
        //        }
        //        else
        //        {
        //            if (StatusUpdateCheck != 1) // 1 for Status "Fault"
        //            {
        //                StatusUpdateCheck = 1;
        //                System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
        //                System.Threading.ThreadPool.QueueUserWorkItem(updateJob, "Fault");
        //            }
        //        }


        //    }
        //    catch
        //    {
        //    }

        //    return flagResult;
        //}
        #endregion
        public bool ShowString(string Str_, string formatInfo, int VMSLogging)
        {
            
#if DEBUG

           log.Debug("Entered into ShowString method in VMSDevice with [ {0} ]  message.", Str_);

#endif
 
            bool flagResult = false;
            try
            {


                short YPos = 0;
                lock (myVMSLock)
                {
                    // Str_ = formatInfo + Str_;
                    tmrRefreshScreen.Stop();

                    bool firstMsg = false;

                    if (currentMessage == null || currentMessage.Count <= 0)
                    {
                        firstMsg = true;
                    }


                    //cff0000 doesn't display text in red. red color is define as c0000ff in vms
                    if (formatInfo.ToLower().Trim().Contains("cff0000"))
                        formatInfo = formatInfo.ToLower().Replace("cff0000", "C0000FF");

                    Str_ = formatInfo.Trim() + Str_.ToUpper().Trim();

                    Str_ = Str_.Replace("'", "`");
#if DEBUG
                  log.Debug("Formated Message [ {0} ]", Str_);
#endif

                    int index1 = -1;
                    if (currentMessage != null && currentMessage.Count > 0)
                        index1 = currentMessage.FindIndex(x => x == Str_);

                    if (index1 > 0)
                    {
                        currentMessage.RemoveAt(index1);
                    }

                    if (!Str_.ToLower().Contains("closed") && currentMessage.FindIndex(x => x == Str_) < 0
                    && currentMessage.FindAll(x => x.ToLower().Contains("closed")).Count < 1)
                    {
                        if (Location == 5)
                            currentMessage.Add(Str_);
                        else
                            currentMessage.Insert(0, Str_);
                    }


                      
                    if ( currentMessage.Count   > Convert.ToInt32(LedHeight /((LedWidth  / TextLength)*2)))
                    {
                        int startIndex = Convert.ToInt32((LedHeight /((LedWidth  / TextLength)*2)));
                        currentMessage.RemoveRange(startIndex, currentMessage.Count - startIndex);
                    }

                    StringBuilder toDisplay  = new StringBuilder();



                    #region not needed as padding not required
                        //int strLength = 0;

                        //if (SpaceFieldLength == 0)
                        //{
                        //    if (LedWidth > 32)
                        //    {
                        //        strLength = formatInfo.Length + 16;
                        //    }
                        //    else
                        //    {
                        //        strLength = formatInfo.Length + 13;
                        //    }
                        //}
                        //else
                        //{
                        //    strLength = formatInfo.Length + SpaceFieldLength;
                        //} 
                    #endregion


#if DEBUG
                   log.Debug("Total messages in the list [ {0} ]", currentMessage.Count);
#endif

                    if (Str_.ToLower().Contains("closed") && Location == 5)
                    {
                        if (currentMessage.Count == 1 && currentMessage[0].ToLower().Contains("closed"))
                            return true;

                        currentMessage.Clear();
                         
                        currentMessage.Insert(0, "`C0000FF"+System.Environment.NewLine+"CLOSED");
                        

                        //toDisplay.Append("`CFFFF00PAD " + DeviceNo.ToString().PadLeft(2, '0') + " `CFF0000CLOSED");
                        firstMsg = true;

                    }
                    //else
                    //{
                    int totalline = 0;
                    int totaltruck = 0;
                    for (int index = 0; index < currentMessage.Count; index++)
                    {

                        if (Location == 5)
                        {
                            if (index == 0)
                            {
                                toDisplay.AppendLine("`C00FFFFPAD" + DeviceNo.ToString().PadLeft(2, '0'));
                                totalline++;
                            }
                            //else
                            //    toDisplay.Append("       ");

                            
                        }

                        if (totalline > Convert.ToInt32(LedHeight / ((LedWidth / TextLength) * 2)) - 1)
                                break;

                        if (Location == 5)
                        {
                            toDisplay.Append(currentMessage[index]+" ");
                            totaltruck++;
                            if (totaltruck >= 2 || currentMessage[index].ToLower().Replace(" ","").Contains("wrongpad"))
                            {
                                toDisplay.Append(System.Environment.NewLine);
                                totalline++;
                                totaltruck = 0;
                            }
                        }
                        else
                        {
                            toDisplay.AppendLine(currentMessage[index].Trim());
                            totalline++;
                        }
                        #region SendVMSMessage will cleare screen before sending message so not padding required. New should be added for earch msg
                        //if (strLength > currentMessage[index].Length)
                        //{
                        //    toDisplay.Append(' ', strLength - currentMessage[index].Length);
                        //}
                        //if (index > 0)
                        //{
                        //    toDisplay.Append(System.Environment.NewLine);
                        //}
                        #endregion



                        //if (maxStringLength > toDisplay.Length)
                        //    toDisplay.Append(' ', maxStringLength - currentMessage[index].Length); 

                    }

#if DEBUG
                        log.Debug("Final string to display [ {0} ]", toDisplay.ToString());
#endif
                    // }
                    //ShowString(toDisplay.ToString() + " -- On VMS At  " + DateTime.Now.ToString());
                    short portNo = UDPPort;

                    

                   // RandomPort = portNo;

                    // VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);                   

#region Disabled SendVMSMessage will clear screen every time before sending message

//                    if (firstMsg == true)
//                    {
//                        string blankString;

//                        blankString = "`C000000 ";
//                        blankString = blankString.PadRight(blankString.Length + SpaceFieldLength, ' ');

//                        StringBuilder blankScreen = new StringBuilder(blankString);

//                        for (int index = 0; index < Convert.ToInt32(LedHeight / TextLength) - 1; index++)
//                        {

//                            blankScreen.Append(System.Environment.NewLine);

//                            blankScreen.Append(blankString);

//                        }

//#if DEBUG
//                        //log.Info("Sending blank line to display [ {0} ]. to {1}vms. Thread {2}", blankScreen.ToString(), IPAddress,System.Threading.Thread.CurrentThread.ManagedThreadId);
//#endif

//                        //Moved to async call to SendVMSMessage
//                        //flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
//                        lock (lockerQueueMsg)
//                        {
//                            lastmsg = blankScreen.ToString();
//                            // queuedMessage.Add(DateTime.Now, blankScreen.ToString());
//                        }
//                        if (!methodCalled)
//                        {
//                            methodCalled = true;
//                            messageList.BeginInvoke(null, null);
//                        }

//                        //                        if (flagResult == false)
//                        //                        {
//                        //                            //objVMS = null;
//                        //                            //portNo = ++portNo;
//                        //                            //objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
//                        //                            //objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
//                        //#if DEBUG
//                        //                            log.Error("Show string returned false when sending [ {0} ] message to the {1} VMS.", blankScreen.ToString(),IPAddress );
//                        //#endif
//                        //                        }
//                    }
#endregion

                    //Moved to async call to SendVMSMessage
                    //flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());
#if DEBUG
                 log.Debug("Adding {0} to {1}vms. Thread {2}",toDisplay.ToString(),IPAddress,System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    lock (lockerQueueMsg)
                    {
                        lastmsg = toDisplay.ToString();

                    }

                    #region ping
                    //                    try
                    //                    {
                    //                        if(new Ping().Send(IPAddress,pingTimeout).Status != IPStatus.Success)
                    //                        {
                    //#if DEBUG 
                    //                                   log.Debug("VMS {0} is not online. Ping{1} Failed",IPAddress,pingTimeout.ToString());
                    //#endif
                                                
                    //                            return flagResult;
                    //                        }

                    //                        flagResult = true;
                    //                    }catch{}
                    #endregion

                    if (!methodCalled)
                    {
                        methodCalled = true;
                        messageList.BeginInvoke(null, null);
                    }
                     
                }//release myvmslock

                // tmrRefreshScreen.Stop();

             


            }
            catch (Exception ex)
            {
#if DEBUG
              log.ErrorException("Exception thrown while sending message to"+IPAddress+" VMS.", ex);
#endif
            }
            finally
            {
                if (tmrRefreshScreen != null)
                {
                    if (!tmrRefreshScreen.Enabled)
                    {
                        tmrRefreshScreen.Interval = MessageTimeOut;
                        tmrRefreshScreen.Start();
                    }
                }
            }

            
            return flagResult;
        }

        public void UpdateVMSConnStatus(bool flagResult)
        {
            #region update DB status
            string status;

            if (flagResult == true)
            {
                status = "ok";
                if (StatusUpdateCheck != 0) // 0 for Status "OK"
                {
                    StatusUpdateCheck = 0;
                    System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
                    System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
                }
            }
            else
            {
                status = "Fault";
                if (StatusUpdateCheck != 1) // 1 for Status "Fault"
                {
                    StatusUpdateCheck = 1;
                    System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
                    System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
                }
            }
            #endregion

        }


        public bool ShowCurrentString(string Str_, string formatInfo)
        {

           

#if DEBUG
            log.Debug("Entered into ShowString method in VMSDevice with [ {0} ]  message. Thread {1}", Str_,
              System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif

            //New VMS

            bool flagResult = false;
            try
            {


                short YPos = 0;
                lock (myVMSLock)
                {
                    // Str_ = formatInfo + Str_;
                    tmrRefreshScreen.Stop();

                    bool firstMsg = false;

                    if (currentMessage == null || currentMessage.Count <= 0)
                    {
                        firstMsg = true;
                    }

                    if (formatInfo.ToLower().Trim().Contains("cff0000"))
                        formatInfo = formatInfo.ToLower().Replace("cff0000", "C0000FF");

                    if(formatInfo.ToLower().Trim().Contains("cffffff"))
                        formatInfo = formatInfo.ToLower().Replace("cffffff", "C00FFFF");

                    if (formatInfo.ToLower().Trim().Contains("cffff00"))
                        formatInfo = formatInfo.ToLower().Replace("cffff00", "C00FFFF");

                    Str_ = formatInfo + Str_.ToUpper();

                    Str_ = Str_.Replace("'", "`");

#if DEBUG
                   log.Debug("Formated Message [ {0} ]", Str_);
#endif
                   if (Str_.ToLower().Replace(" ", "").Contains("wrongpad"))
                    {

                        if (currentMessage.Count > 0 && currentMessage[0].ToLower().Replace(" ", "").Contains("wrongpad"))
                            return true;
                        currentMessage.Clear();
                        currentMessage.Add(Str_);
                    }
                    else
                    {
                        if (currentMessage.FindIndex(x => x.ToLower().Contains(Str_.ToLower())) > 0)
                        {
                            for (int i = 0; i < currentMessage.FindIndex(x => x.ToLower().Contains(Str_.ToLower())); i++)
                                currentMessage.RemoveAt(i);
                        }
                        else if (currentMessage.FindIndex(x => x.ToLower().Contains(Str_.ToLower())) < 0)
                        {
                            if (currentMessage.FindIndex(x => x.ToLower().Replace(" ", "").Contains("wrongpad")) > -1)
                                currentMessage.RemoveAt(currentMessage.FindIndex(x => x.ToLower().Replace(" ","").Contains("wrongpad")));

                            currentMessage.Insert(0, Str_.ToUpper());
                        }
                    }
                   

                    if ( currentMessage.Count   > Convert.ToInt32(LedHeight /((LedWidth  / TextLength)*2)))
                    {
                        int startIndex = Convert.ToInt32((LedHeight /((LedWidth  / TextLength)*2)));
                        currentMessage.RemoveRange(startIndex, currentMessage.Count - startIndex);
                    }

                    

                    StringBuilder toDisplay  = new StringBuilder();

                    #region not needed. See ShowString method for detail
                        //int strLength = 0;

                        //if (SpaceFieldLength == 0)
                        //{
                        //    if (LedWidth  > 32)
                        //    {
                        //        strLength = formatInfo.Length + 16;
                        //    }
                        //    else
                        //    {
                        //        strLength = formatInfo.Length + 13;
                        //    }
                        //}
                        //else
                        //{
                        //    strLength = formatInfo.Length + SpaceFieldLength;
                        //}
                    #endregion

#if DEBUG
                    log.Debug("Total messages in the list [ {0} ] Thread {1}", currentMessage.Count,
                      System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif

                   
                    #region build message from currentlist
                    int totalline = 0;
                    int  totaltruck = 0;
                                        for (int index = 0; index < currentMessage.Count; index++)
                                        {

                                            if (Location == 5)
                                            {
                                                if (index == 0)
                                                {
                                                    toDisplay.AppendLine("`C00FFFFPAD" + DeviceNo.ToString().PadLeft(2, '0'));
                                                    totalline++;
                                                }
                                                //else
                                                //    toDisplay.Append("       ");

                                               
                                            }

                                            if (totalline > Convert.ToInt32((LedHeight / ((LedWidth / TextLength) * 2))) - 1)
                                                    break;

                                            if (Location == 5)
                                            {
                                                toDisplay.Append(currentMessage[index]+" ");
                                                totaltruck++;
                                                if (totaltruck >= 2)
                                                {
                                                    toDisplay.Append(System.Environment.NewLine);
                                                    totalline++;
                                                    totaltruck = 0;
                                                }
                                            }
                                            else
                                            {
                                                toDisplay.AppendLine(currentMessage[index]);
                                                totalline++;
                                            }

                                                //toDisplay.AppendLine();

                                            #region not needed. see in ShowString method for detail
                                            //if (index > 0)
                                            //{
                                            //    toDisplay.Append(System.Environment.NewLine);
                                            //}


                                            //if (strLength > currentMessage[index].Length)
                                            //{
                                            //    toDisplay.Append(' ', strLength - currentMessage[index].Length);
                                            //}
                                            #endregion

                                        }
                    #endregion
                    // }
                                        //ShowString(toDisplay.ToString() + " -- On VMS At  " + DateTime.Now.ToString());
                    #region disabled. move to SeneVMSMessage method
                        //short portNo = UDPPort;

                        //if (UseRandomPortNo)
                        //{
                        //    Random rdn = new Random();

                        //    do
                        //    {
                        //        portNo = Convert.ToInt16(rdn.Next(MinUDPPortNo, MaxUDPPortNo));
                        //    }
                        //    while (portNo == LastPortNoUsed);

                        //    LastPortNoUsed = portNo;
                        //}
                        //RandomPort = portNo;
                        ////  VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
                    #endregion

                    #region disbaled. SendVMSMessage send blank string before sending messages
                    //                    if (firstMsg == true)
//                    {
//                        string blankString;

//                        blankString = "`C000000 ";
//                        blankString = blankString.PadRight(blankString.Length + SpaceFieldLength, ' ');

//                        StringBuilder blankScreen = new StringBuilder(blankString);

//                        for (int index = 0; index < Convert.ToInt32(LedHeight / TextLength) - 1; index++)
//                        {

//                            blankScreen.Append(System.Environment.NewLine);

//                            blankScreen.Append(blankString);

//                        }

//#if DEBUG
//                        //log.Info("Sending blank line to display [ {0} ] to {1}vms. Thread {2}", blankScreen.ToString(),IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
//#endif
//                        //Moved to async call to SendVMSMessate
//                        //flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
//                        lock (lockerQueueMsg)
//                        {
//                            lastmsg = blankScreen.ToString();
//                            //queuedMessage.Add(DateTime.Now, blankScreen.ToString());
//                        }
//                        if (!methodCalled)
//                        {
//                            methodCalled = true;
//                            messageList.BeginInvoke(null, null);
//                        }


//                    }
#endregion

                    //Moved to async call to SendVMSMessage
                    //flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());
#if DEBUG
                    log.Debug("Adding {0} to {1}vms. Thread {2}", toDisplay.ToString(), IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    lock (lockerQueueMsg)
                    {
                        lastmsg = toDisplay.ToString();
                        //queuedMessage.Add(DateTime.Now, toDisplay.ToString());
                    }

                     
                    #region ping
//                    try
//                    {

//                        if(new Ping().Send(IPAddress,pingTimeout).Status != IPStatus.Success)
//                        {
//#if DEBUG 
//                            log.Debug("VMS {0} is not online. Ping {1} failed.",IPAddress,pingTimeout.ToString());
//#endif
//                            return flagResult;
//                        }
//                        flagResult = true;

//                    }catch{ }
                    #endregion

                    if (!methodCalled)
                    {
                        methodCalled = true;
                        messageList.BeginInvoke(null, null);
                    }
                     
                }
  
                #region update status in DB
                //                string status;

                //                if (flagResult == true)
                //                {
                //                    status = "ok";
                //                    if (StatusUpdateCheck != 0) // 0 for Status "OK"
                //                    {
                //                        StatusUpdateCheck = 0;
                //                        System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
                //                        System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
                //                    }
                //                }
                //                else
                //                {
                //                    status = "Fault";
                //                    if (StatusUpdateCheck != 1) // 1 for Status "Fault"
                //                    {
                //                        StatusUpdateCheck = 1;
                //                        System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
                //                        System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
                //                    }
                //                }
                #endregion
                 
            }
            catch (Exception ex)
            {
#if DEBUG
              log.ErrorException("Exception thrown while sending message to "+IPAddress+" VMS: Error:  ",  ex );
#endif
            }
            finally
            {
                if(tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                {
                        tmrRefreshScreen.Interval = MessageTimeOut;
                        tmrRefreshScreen.Start();
                }
            }

            return flagResult;
        }


        //sending message to new VMS

        //public static void SendMessageOnTCP(string[] vmsSettings,string message)
        //{

        //    if (tmrRefreshScreen != null || tmrRefreshScreen.Enabled)
        //        tmrRefreshScreen.Stop();

        //    message= message.Replace("`","'");

        //    #if DEBUG
        //    vmslog.Debug("-----------------------{0}{1}{0}", System.Environment.NewLine, message);
        //    #endif

        //    if (tcpListener.RemoteEndPoint == null || !tcpListener.Connected)
        //    {
        //        string ipadd = vmsSettings.First(x=>x.ToLower().Contains("ipaddress"));
        //        string strPort = vmsSettings.First(x=>x.ToLower().Contains("port"));
        //        string vmsid =vmsSettings.First(x => x.ToLower().Contains("vmsid"));
        //        vmsid = (string.IsNullOrEmpty(vmsid)) ? "01" : vmsid.Substring(vmsid.IndexOf(':') + 1).Trim();

        //        int port = 0;
        //        Int32.TryParse(strPort.Substring(strPort.IndexOf(':')+1).Trim(),out port);
        //        if(string.IsNullOrEmpty(ipadd))
        //            return;

        //        tcpListener.Connect(new IPEndPoint( System.Net.IPAddress.Parse(ipadd.Substring(ipadd.IndexOf(':') + 1).Trim()), port));
        //        byte[] recData = new byte[250];
        //        tcpListener.ReceiveTimeout = 2000;
        //        tcpListener.Receive(recData);
        //        tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(vmsid));
        //    }

        //    try
        //    {
        //        message = "'\\n" + message;
        //        if (firstMessge)
        //        {
        //            message =  message.Replace("'\\n","'B");
        //            firstMessge = false;
        //        }
        //        tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(message));

        //    }
        //    catch (Exception ex)
        //    {
        //        tcpListener.Disconnect(false);
        //        log.Error("Exception thrown while send message to new VMS: [ {0} ]", ex.Message);
        //    }

        //    StartRefreshTimer();


        //}

        //private static void StartRefreshTimer()
        //{
        //    if (tmrRefreshScreen == null)
        //    {
        //        tmrRefreshScreen = new Timer(MessageTimeOut);
        //        tmrRefreshScreen.Elapsed += new ElapsedEventHandler(tmrRefreshScreen_Elapsed);
        //    }

        //    tmrRefreshScreen.Stop();
        //    tmrRefreshScreen.Start();
        //}

        //public bool ShowString(string Str_, string formatInfo)
        //{
        //    bool flagResult = false;
        //    try
        //    {


        //        short YPos = 0;
        //        lock (myVMSLock)
        //        {
        //            // Str_ = formatInfo + Str_;
        //            tmrRefreshScreen.Stop();

        //            bool firstMsg = false;

        //            if (currentMessage.Count <= 0)
        //            {
        //                firstMsg = true;
        //            }

        //            Str_ = formatInfo + Str_.ToUpper();

        //            int index1 = currentMessage.FindIndex(x => x == Str_);
        //            if (index1 >= 0)
        //            {
        //                currentMessage.RemoveAt(index1);
        //            }

        //            currentMessage.Insert(0, Str_);

        //            if ((currentMessage.Count * TextLength) > LedHeight)
        //            {
        //                int startIndex = Convert.ToInt32(LedHeight / TextLength);
        //                currentMessage.RemoveRange(startIndex, currentMessage.Count - startIndex);
        //            }

        //            StringBuilder toDisplay;

        //            toDisplay = new StringBuilder();

        //            int strLength = 0;

        //            if (SpaceFieldLength == -999)
        //            {
        //                if (AscFont > 2)
        //                {
        //                    strLength = formatInfo.Length + 12;
        //                }
        //                else
        //                {
        //                    strLength = formatInfo.Length + 8;
        //                }
        //            }
        //            else
        //            {
        //                strLength = formatInfo.Length + SpaceFieldLength;
        //            }



        //            for (int index = 0; index < currentMessage.Count; index++)
        //            {

        //                if (index > 0)
        //                {
        //                    toDisplay.Append(System.Environment.NewLine);
        //                }

        //                toDisplay.Append(currentMessage[index]);

        //                if (strLength > currentMessage[index].Length)
        //                {
        //                    toDisplay.Append(' ', strLength - currentMessage[index].Length);
        //                }

        //                //if (maxStringLength > toDisplay.Length)
        //                //    toDisplay.Append(' ', maxStringLength - currentMessage[index].Length); 

        //            }

        //            //ShowString(toDisplay.ToString() + " -- On VMS At  " + DateTime.Now.ToString());

        //            short portNo = UDPPort;

        //            if (UseRandomPortNo)
        //            {
        //                Random rdn = new Random();

        //                do
        //                {
        //                    portNo = Convert.ToInt16(rdn.Next(MinUDPPortNo, MaxUDPPortNo));
        //                }
        //                while (portNo == LastPortNoUsed);

        //                LastPortNoUsed = portNo;
        //            }

        //            VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);


        //            if (firstMsg == true)
        //            {
        //                string blankString;

        //                blankString = "`C000000 ";
        //                blankString = blankString.PadRight(blankString.Length + SpaceFieldLength, ' ');

        //                StringBuilder blankScreen = new StringBuilder(blankString);

        //                for (int index = 0; index < Convert.ToInt32(LedHeight / TextLength) - 1; index++)
        //                {

        //                    blankScreen.Append(System.Environment.NewLine);

        //                    blankScreen.Append(blankString);

        //                }

        //                flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());

        //                if (flagResult == false)
        //                {
        //                    objVMS = null;
        //                    portNo = (short)(portNo + currentMessage.Count + 1);
        //                    objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
        //                    objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
        //                }
        //            }


        //            flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());

        //            if (flagResult == false)
        //            {
        //                objVMS = null;
        //                portNo = (short)(portNo + currentMessage.Count + 1);
        //                objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
        //                objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, toDisplay.ToString());
        //            }

        //        }

        //        // tmrRefreshScreen.Stop();
        //        tmrRefreshScreen.Interval = MessageTimeOut;
        //        tmrRefreshScreen.Start();

        //        string status;

        //        if (flagResult == true)
        //        {
        //            status = "ok";
        //            if (StatusUpdateCheck != 0) // 0 for Status "OK"
        //            {
        //                StatusUpdateCheck = 0;
        //                System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
        //                System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
        //            }
        //        }
        //        else
        //        {
        //            status = "Fault";
        //            if (StatusUpdateCheck != 1) // 1 for Status "Fault"
        //            {
        //                StatusUpdateCheck = 1;
        //                System.Threading.WaitCallback updateJob = new System.Threading.WaitCallback(UpdateStatus);
        //                System.Threading.ThreadPool.QueueUserWorkItem(updateJob, status);
        //            }
        //        } 

        //    }
        //    catch
        //    {
        //    }

        //    return flagResult;
        //}

        //private void ShowString(string msg)
        //{
        //    try
        //    {
        //        string drivePath = @"C:\";
        //        string filePath = drivePath + "VMSMessages.txt";

        //        //if (!System.IO.File.Exists(filePath))
        //        //{
        //        //    System.IO.File.Create(filePath);
        //        //}

        //        System.IO.StreamWriter wr = new System.IO.StreamWriter(filePath, true);

        //        //wr.WriteLine("--------------------------------------" + DateTime.Now.ToString() + "--------------------------------------");
        //        wr.WriteLine(msg);
        //        wr.WriteLine("------------------------------------------------------------------------------------------------------------");

        //        wr.Flush();
        //        wr.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        public void DisplayDefaultMessage()
        {


#if DEBUG
            log.Debug("Enterd to display default message to {0}vms. Thread {1}", IPAddress,
                 System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
            try
            {
                lock (myVMSLock)
                {

                    if (Location == 1)
                    {
                        if (currentMessage != null)
                            currentMessage.Clear();
                    }
                    else if (Location == 2 || Location == 5)
                    {
                        if (currentMessage != null && currentMessage.Count > 0)
                            return;
                    }

                    if (IPAddress == null || IPAddress.Trim().Length <= 0)
                    {
                        tmrRefreshScreen.Start();
                        return;
                    }

                    //IVMSDevices objVMsDevice = VMSDevices.GetVMSDevicesByLocation((Location)this.Location);

                    //UpdateVMSSettings(objVMsDevice);


                    string defaultMsg = "`C00FFFF" + DefaultMessage;

                    defaultMsg = defaultMsg.Replace("'", "`");
                    
                    #region disabled. SendVMSMessage send blank string before send vms message
                    //string blankString;

                    //blankString = "`C000000 ";
                    //blankString = blankString.PadRight(blankString.Length + SpaceFieldLength, ' ');

                   

                    //defaultMsg = defaultMsg.PadRight(defaultMsg.Length + SpaceFieldLength, ' ');

                    //StringBuilder blankScreen = new StringBuilder(defaultMsg);

                    //int index = 0;

                    //for (index = 0; index < Convert.ToInt32(LedHeight / TextLength) - 1; index++)
                    //{

                    //    blankScreen.Append(System.Environment.NewLine);

                    //    //  YPos = Convert.ToInt16(index * TextLength); 

                    //    blankScreen.Append(blankString);

                    //    //objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, "`C000000 " + blankString.ToString()); // Black color
                    //} 
                    #endregion


#if DEBUG
                    log.Debug("Clearing current message list. {0}vms", IPAddress);
#endif

                    // currentMessage.Clear();

                    #region disbaled. Move to SendVMSMessage method
                    //short portNo = UDPPort;

                    //if (UseRandomPortNo)
                    //{
                    //    Random rdn = new Random();

                    //    do
                    //    {
                    //        portNo = Convert.ToInt16(rdn.Next(MinUDPPortNo, MaxUDPPortNo));
                    //    }
                    //    while (portNo == LastPortNoUsed);

                    //    LastPortNoUsed = portNo;
                    //}
                    //RandomPort = portNo;

                    ////VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
                    #endregion

                    bool flagResult = false;

                    if (Location == 5)
                    {
                        defaultMsg = "`C00FFFFPAD" + DeviceNo.ToString().PadLeft(2, '0') + System.Environment.NewLine + defaultMsg;
                    }

                    //Moved to aync call to SendVMSMessage
                    //flagResult = objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
                    lock (lockerQueueMsg)
                    {
                        lastmsg = defaultMsg;
                        //queuedMessage.Add(DateTime.Now, blankScreen.ToString());
                    }
                    #region ping
//                    try
//                    {
//                        if (new Ping().Send(IPAddress, pingTimeout).Status != IPStatus.Success)
//                        {
//#if DEBUG
//                            log.Debug("VMS {0} is not online. Ping{1} Failed.", IPAddress, pingTimeout);
//#endif
//                            return    ;
//                        }
//                        flagResult = true;

//                    }
//                    catch
//                    {

//                    }

                    #endregion


                    if (!methodCalled)
                    {
                        methodCalled = true;
                        messageList.BeginInvoke(null, null);
                    }




                }

            }
            catch (Exception ex)
            {
#if DEBUG
                log.ErrorException("Exception thrown while sending default message to " + IPAddress + " vms : ", ex);
#endif
            }
            finally
            {
                if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                {
                    tmrRefreshScreen.Interval = MessageTimeOut;
                    tmrRefreshScreen.Start();
                }
            }

          
             
        }

        protected void UpdateStatus(object status)
        {
            try
            {

                VMSDevices.UpdateVMSStatus(IPAddress, status.ToString());
            }
            catch
            {
            }
        }

        protected void WriteVMSLog(object status)
        {
            try
            {
                Ramp.MiddlewareController.Common.Location loc = (Ramp.MiddlewareController.Common.Location)Location;

                string fileName = "Log_" + loc.ToString() + "_" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt";
                fileName = VMSLogPath + fileName;
                lock (VMSLogPath)
                {
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName, true);
                    sw.WriteLine("Status : " + status.ToString() + " at " + DateTime.Now.ToString());
                    sw.Flush();
                    sw.Close();
                }
            }
            catch
            {
            }
        }

        public void RemoveMessage(string Str_, string formatInfo)
        {
           
            try
            {
                lock (myVMSLock)
                {
                    if (currentMessage == null)
                    {
                        currentMessage = new List<string>();
                        return;
                    }

                 
                    Str_ = Str_.Replace("'", "`");
                    formatInfo = formatInfo.Replace("'", "`");

                    if (!currentMessage.Any(x => x.ToLower().Contains(Str_.Trim().ToLower().Substring(0, (Str_.Length > 4) ? 4 : Str_.Length))))
                        return;

                    currentMessage.RemoveAll(x => x.ToLower().Contains(Str_.Trim().ToLower().Substring(0, (Str_.Length > 4) ? 4 : Str_.Length)));

                    if (currentMessage.Count < 1)
                    {
                        DisplayDefaultMessage();
                        return;
                    }

                    #region not needed. No need to pad message as screen will be cleared before send msg
                        //int strLength = 0;

                        //if (SpaceFieldLength == -999)
                        //{
                        //    if (AscFont > 2)
                        //    {
                        //        strLength = formatInfo.Length + 12;
                        //    }
                        //    else
                        //    {
                        //        strLength = formatInfo.Length + 8;
                        //    }
                        //}
                        //else
                        //{
                        //    strLength = formatInfo.Length + SpaceFieldLength;
                        //} 
                    #endregion

                    StringBuilder toDisplay = new StringBuilder();
                    int totalline = 0;
                    int totaltruck = 0;
                    for (int index = 0; index < currentMessage.Count; index++)
                    {

                        if (Location == 5)
                        {
                            if (index == 0)
                            {
                                toDisplay.AppendLine("`C00FFFFPAD" + DeviceNo.ToString().PadLeft(2, '0'));
                                totalline++;
                            }
                            
                        }

                        if (totalline > Convert.ToInt32((LedHeight / ((LedWidth / TextLength) * 2))) - 1)
                            break;

                        if (Location == 5)
                        {
                            toDisplay.Append(currentMessage[index]+" ");
                            totaltruck++;
                            if (totaltruck >= 2)
                            {
                                toDisplay.Append(System.Environment.NewLine);
                                totalline++;
                                totaltruck = 0;
                            }
                        }
                        else
                        {
                            toDisplay.AppendLine(currentMessage[index]);
                            totalline++;
                        }

                        #region disabled. padding not required
                            //if (index > 0)
                            //{
                            //    toDisplay.Append(System.Environment.NewLine);
                            //}



                            //if (strLength > currentMessage[index].Length)
                            //{
                            //    toDisplay.Append(' ', strLength - currentMessage[index].Length);
                            //} 
                        #endregion


                    }

#if DEBUG
                    log.Debug("Final string to display [ {0} ]", toDisplay.ToString());
#endif

                    //ShowString(toDisplay.ToString() + " -- On VMS At  " + DateTime.Now.ToString());
                   
                    #region Disabled. Move to SendVMSMessage
                        //short portNo = UDPPort;

                        //if (UseRandomPortNo)
                        //{
                        //    Random rdn = new Random();

                        //    do
                        //    {
                        //        portNo = Convert.ToInt16(rdn.Next(MinUDPPortNo, MaxUDPPortNo));
                        //    }
                        //    while (portNo == LastPortNoUsed);

                        //    LastPortNoUsed = portNo;
                        //}
                        //RandomPort = portNo;

                        ////  VMS objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5); 
                    #endregion

                    #region Disabled. no need to send blank string as screen will be clear before sending any messages
                    //                    bool firstMsg = false;

                    //                    if (currentMessage == null || currentMessage.Count <= 0)
                    //                    {
                    //                        firstMsg = true;
                    //                    }

                    //                    if (firstMsg == true)
                    //                    {
                    //                        string blankString;

                    //                        blankString = "`C000000 ";
                    //                        blankString = blankString.PadRight(blankString.Length + SpaceFieldLength, ' ');

                    //                        StringBuilder blankScreen = new StringBuilder(blankString);

                    //                        for (int index = 0; index < Convert.ToInt32(LedHeight / TextLength) - 1; index++)
                    //                        {

                    //                            blankScreen.Append(System.Environment.NewLine);

                    //                            blankScreen.Append(blankString);

                    //                        }

                    //#if DEBUG
                    //                        //log.Info("Sending blank line to display [ {0} ].to {1}vms. Thread {2}", blankScreen.ToString(), IPAddress,
                    //                        //    System.Threading.Thread.CurrentThread.ManagedThreadId);
                    //#endif
                    //                        //Moved to aync call to SendVMSMessage
                    //                        //objVMS.ShowString(TextStartPosition, 0, TextColor, AscFont, blankScreen.ToString());
                    //                        lock (lockerQueueMsg)
                    //                        {
                    //                            lastmsg = blankScreen.ToString();
                    //                            //queuedMessage.Add(DateTime.Now, blankScreen.ToString());
                    //                        }
                    //                        if (!methodCalled)
                    //                        {
                    //                            methodCalled = true;
                    //                            messageList.BeginInvoke(null, null);
                    //                        }

                    //                        //if (flagResult == false)
                    //                        //{
                    //                        //    objVMS = null;
                    //                        //    portNo = ++portNo;
                    //                        //    objVMS = new VMS(DeviceNo, LedWidth, LedHeight, IPAddress, portNo, 10, 5);
                    //                        //    objVMS.ShowString(TextStartPosition, YPos, TextColor, AscFont, blankScreen.ToString());
                    //                        //}
                    //                    } 
                    #endregion

                    //Moved to async call to SendVMSMessage
                    //objVMS.ShowString(TextStartPosition, 0, TextColor, AscFont, toDisplay.ToString());
                    lock (lockerQueueMsg)
                    {
                        lastmsg = toDisplay.ToString();
                        //queuedMessage.Add(DateTime.Now, toDisplay.ToString());
                    }

                    #region ping

//                    try
//                    {
//                        if (new Ping().Send(IPAddress, pingTimeout).Status != IPStatus.Success)
//                        {
//#if DEBUG
//                            log.Debug("VMS {0} not online. RemoveMessage mtd. Ping {1} Failed.",IPAddress,pingTimeout);
//#endif
//                            return;
//                        }
                        

//                    }
//                    catch
//                    {

//                    }

                    #endregion

                    if (!methodCalled)
                    {
                        methodCalled = true;
                        messageList.BeginInvoke(null, null);
                    }

                }
            }
            catch { }
        }

        public void UpdateVMSSettings(IVMSDevices objVMsDevice)
        {
            if (objVMsDevice != null)
            {
                try
                {
                    DeviceNo = objVMsDevice.DeviceNo;
                    IPAddress = objVMsDevice.IPAddress;
                    Password = objVMsDevice.Password;
                    UDPPort = objVMsDevice.UDPPort;
                    SCL2008 = objVMsDevice.SCL2008;
                    LedHeight = objVMsDevice.LedHeight;
                    LedWidth = objVMsDevice.LedWidth;
                    Location = objVMsDevice.Location;

                    AscFont = Convert.ToInt16(objVMsDevice.ASCFont);
                    TextColor = Convert.ToInt16(objVMsDevice.TextColor);
                    TextStartPosition = Convert.ToInt16(objVMsDevice.TextStartPosition);
                    TextLength = Convert.ToInt16(objVMsDevice.TextLength);

                    MessageTimeOut = objVMsDevice.MessageTimeOut;

                    DefaultMessage = objVMsDevice.DefaultMessage;

                    objVMsDevice = null;

                }
                catch
                {
                }
            }
        }
    }
}
