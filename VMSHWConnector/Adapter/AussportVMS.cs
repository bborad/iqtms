﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

using VMSLibrary;
using Ramp.VMSHWConnector.Common;
using Ramp.MiddlewareController.Connector.VMSHardWareConnector;
using Ramp.MiddlewareController.Common;
using Ramp.DBConnector.Adapter;
using Ramp.MiddlewareController.Connector.DBConnector;


using NLog;

namespace Ramp.VMSHWConnector.Adapter
{
    //public delegate void QueueVMSMessage(string vmsMessage);

    public class AussportVMS : IVMSDevice
    {
#if DEBUG

        private  Logger log = LogManager.GetCurrentClassLogger();

#endif

        // private static AussportVMS ausVMS=null;

        public short DeviceNo { get; set; }
        private string ipAddress;
        public string IPAddress {

            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
                 log = LogManager.GetLogger("VMSLog_" + ipAddress);

            }
        }
        public bool SCL2008 { get; set; }
        public string Password { get; set; }
        public short UDPPort { get; set; }

        public short LedNum { get; set; } // value 0
        public short ComPort { get; set; } // value 1     
        public int Location { get; set; }

        public short LedWidth { get; set; } // value 128
        public short LedHeight { get; set; } // value 32

        public short ledMaxSize;

        public short TextColor { get; set; } // value 255
        public short AscFont { get; set; }
        public short TextStartPosition { get; set; }
        public short TextLength { get; set; }

        public int SpaceFieldLength { get; set; }

        public string VMSLogPath { get; set; }

        public bool UseRandomPortNo { get; set; }

        public short MinUDPPortNo { get; set; }

        public short MaxUDPPortNo { get; set; }

        private int LastPortNoUsed;

        int StatusUpdateCheck;

        int maxStringLength = 50;

        public int MessageTimeOut { get; set; }

        public string DefaultMessage { get; set; }

        private Timer tmrRefreshScreen = new Timer();

        public object myVMSLock = new object();

        public List<string> currentMessage { get; set; }

        private Socket tcpListener;
        private bool firstMessge = true;

       // private Dictionary<string, DateTime> queuedMessage = new Dictionary<string, DateTime>();
         private string lastmsg = "";
         string wrongtruckmsg = "WrongPAD";
         string closedmsg = "Closed";
        private readonly object lockerVMSMessage = new object();
        private readonly object lockerQueueMsg = new object();
        private System.Windows.Forms.MethodInvoker messageList;
        private bool methodCalled = false;

        private void SendVMSMessage()
        {
            lock (lockerVMSMessage)
            {

#if DEBUG
                    log.Info("Entered into SendVMSMessage method with {0} messages for {1} vms. Thread {2}", lastmsg, IPAddress,
                       System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                string msg = "";
                int mtimeout = 0;
                try
                {
 

#if DEBUG
                     log.Info("Connecting   {0} vms. Thread {1}", IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    //if (!string.IsNullOrEmpty(msg))
                    //{

                    if (string.IsNullOrEmpty(lastmsg))
                    {
                        methodCalled = false;
#if DEBUG
                         log.Info("Exiting SendVMSMessage (lastmsag is empty) for {0} vms in Thread {1}",IPAddress,System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                        return; 
                    }
#if DEBUG
                    //log.Info("SEnding {0} msg to {1} vms. Thread {2}", lastmsg, IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                     
                        if (OpenConnection())
                        {
                            lock (lockerQueueMsg)
                            {
                                //if (queuedMessage.Count > 0)
                                //{
                                //    msg = queuedMessage.First(x => x.Value == queuedMessage.Max(v => v.Value)).Key;
                                //    queuedMessage.Clear();
                                //}
                                //  foreach(string dd in queuedMessage.Keys)
                                msg = lastmsg;
                                methodCalled = false;
                            }
                            tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(msg.Replace("`", "'")));
                               
#if DEBUG
                                log.Info("Sent {0} msg to {1} vms. Thead {2}", lastmsg, IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif     
                                //queuedMessage.Clear();
                           
                            Disconnect();
                           

                          //return;
                            //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(msg));
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(1000);

#if DEBUG
                            log.Info("Trying again after 1000ms to conn to {0} vms. Thread {1}", IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
      
                            if (OpenConnection())
                            {
                                lock (lockerQueueMsg)
                                {
                                    //if (queuedMessage.Count > 0)
                                    //{
                                    //    msg = queuedMessage.First(x => x.Value == queuedMessage.Max(v => v.Value)).Key;
                                    //    queuedMessage.Clear();
                                    //}
                                    //  foreach(string dd in queuedMessage.Keys)

                                    msg = lastmsg;
                                    methodCalled = false;
                                     
                                }
                                tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(msg.Replace("`", "'")));
                                    
#if DEBUG
                                     log.Info("Sent {0} msg to {1} vms. Thread {2}", lastmsg, IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif     
                                    //queuedMessage.Clear();
                               
                                Disconnect();
                               // return;
                                //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(msg));
                            }
                            methodCalled = false;

                        }
                        
                    //}
                    //bool res = objVMS.ShowString(TextStartPosition, 0, TextColor, AscFont, msg);

                    //only for AusSport VMS
                    //for (int v = 0; v < 2 && !OpenConnection(); v++) ;
                    //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(vmsMessage));
                    //Disconnect();
 
                }
                catch (Exception ex)
                {
                    methodCalled = false;
                    Disconnect();
#if DEBUG
                    log.Error("Exception thrown in SendVMSMessage method with {0} message for {1} vms. Error: {2}", msg + lastmsg, IPAddress, ex.Message);
#endif
                }

#if DEBUG
                 //log.Info("Exiting SendVMSMessage for {0} vms in Thread {1}",IPAddress,System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
            }
        }



        public AussportVMS()
        {

            int workth, ioth;
            System.Threading.ThreadPool.GetMaxThreads(out workth, out ioth);
            if (workth < 50 || ioth < 50)
                System.Threading.ThreadPool.SetMaxThreads(50, 50);


            messageList = new System.Windows.Forms.MethodInvoker(SendVMSMessage);

            tmrRefreshScreen.Elapsed += new ElapsedEventHandler(tmrRefreshScreen_Elapsed);
            StatusUpdateCheck = -1;
            maxStringLength = 100;

            DefaultMessage = "TMS";
            UseRandomPortNo = false;
            MinUDPPortNo = 1000;
            MaxUDPPortNo = 25000;
            LastPortNoUsed = 0;

            object value = System.Configuration.ConfigurationSettings.AppSettings["VMSLogPath"];
            if (value != null)
            {
                VMSLogPath = Convert.ToString(value);
            }
            else
            {
                VMSLogPath = "";
            }

            try
            {
                if (VMSLogPath.Length > 0)
                {
                    if (!System.IO.Directory.Exists(VMSLogPath))
                    {
                        System.IO.Directory.CreateDirectory(VMSLogPath);
                    }
                }
            }
            catch
            {
                VMSLogPath = "";
            }


            tmrRefreshScreen.Interval = 60 * 1000;
            tmrRefreshScreen.Start();
        }

        public bool VMSInit()
        {
            //only used for signtronics VMS
            return true;
        }

        void tmrRefreshScreen_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                tmrRefreshScreen.Stop();

                DisplayDefaultMessage();

            }
            catch
            {
            }
            if (!tmrRefreshScreen.Enabled)
                tmrRefreshScreen.Start();
        }


        public bool Open()
        {


            return false;
        }

        public bool Disconnect()
        {
            try
            {
                if (tcpListener != null)
                {
                    tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'D"));
                    tcpListener.Receive(new byte[255]);
                    tcpListener.Close();
#if DEBUG
                    //log.Info("Connection closed to {0} vms. Thread {1}",IPAddress,System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                 log.Info("Exception thrown while closing connection to {0} vms.Error: {2}. Thread {1}", IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId,
                    ex.Message);
#endif

            }

            tcpListener = null;
            return true;
        }



        //sending message to new VMS

       

        public bool ShowString(string message, string formatInfo, int VMSLogging)
        {

            tmrRefreshScreen.Stop();
            message = message.ToUpper();
            message = formatInfo + message;
            message = message.Replace("`", "'");

            if (currentMessage == null)
                currentMessage = new List<string>();
            try
            {
                lock (myVMSLock)
                {
                    if (!message.ToLower().Contains("closed") && !currentMessage.Contains(message)
                        && currentMessage.FindAll(x => x.ToLower().Contains("closed")).Count < 1)
                    {
                        if (Location == 5)
                            currentMessage.Add(message);
                        else
                            currentMessage.Insert(0, message);
                    }

                    string vmsMessage = "'B";

                    if (message.ToLower().Contains("closed"))
                    {
                        currentMessage.Clear();  
                        if (!currentMessage.Contains("'CFF0000CLOSED"))
                        { currentMessage.Insert(0, "'CFF0000CLOSED"); }
                        else
                        { 
                            if(!tmrRefreshScreen.Enabled)
                             tmrRefreshScreen.Start();
                            return true;
                        }
                        //vmsMessage += "'CFFFF00PAD " + DeviceNo.ToString().PadLeft(2, '0') + " 'CFF0000CLOSED";
                    }
                    //else
                    //{
                    int i = 0;

                    foreach (string line in currentMessage)
                    {
                        if (Location == 5)
                        {
                            if (i == 0)
                                vmsMessage += "'CFFFF00PAD " + DeviceNo.ToString().PadLeft(2, '0') + " ";
                            else
                                vmsMessage += "       ";
                        }


                        vmsMessage += line + "'\\n";
                        i++;
                        if (i >= LedHeight)
                            break;
                    }
                    //}
#if DEBUG
                    //log.Info("Adding {1} in {0} vms in Thread {2}", IPAddress, vmsMessage, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    //Moved to async call to SendVMSMEssage
                    //for (int v = 0; v < 2 && !OpenConnection(); v++) ;
                    //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(vmsMessage));
                    //Disconnect();
                    lock (lockerQueueMsg)
                    {
                       // queuedMessage[vmsMessage] = DateTime.Now;
                         lastmsg = vmsMessage;
                    }
                    if (!methodCalled)
                    { methodCalled = true; messageList.BeginInvoke(null, null); }
                    //messageList();
 
                    // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(" "));
                }
            }
            catch (Exception ex)
            {
                try
                {

                    if (tcpListener != null)
                    {
                        // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'D"));
                        //tcpListener.Disconnect(false);
                        tcpListener = null;
                    }
#if DEBUG
                     log.Error("Exception thrown while send [ {1} ] message to new {2} VMS: [ {0} ]", ex.Message, message, IPAddress);
#endif
                    
                }
                catch { tcpListener = null; }
                if (tmrRefreshScreen!= null && !tmrRefreshScreen.Enabled)
                    tmrRefreshScreen.Start();
                return false;
            }



            if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                tmrRefreshScreen.Start();
            return true;

        }

        public bool ShowCurrentString(string message, string formatInfo)
        {

            tmrRefreshScreen.Stop();
            message = message.ToUpper();
            message = formatInfo + message;
            message = message.Replace("`", "'");

            if (currentMessage == null)
                currentMessage = new List<string>();



            lock (myVMSLock)
            {
                if (message.ToLower().Contains("wrong pad"))
                {

                    if (currentMessage.Count > 0 && currentMessage[0].ToLower().Contains("wrong pad"))
                    {
                        if (!tmrRefreshScreen.Enabled)
                            tmrRefreshScreen.Start(); 
                        return true;
                    }
                    currentMessage.Clear();  
                    currentMessage.Add(message);
                }
                else
                {
                    if (currentMessage.FindIndex(x => x.ToLower().Contains(message.ToLower())) > 0)
                    {
                        for (int i = 0; i < currentMessage.FindIndex(x => x.ToLower().Contains(message.ToLower())); i++)
                            currentMessage.RemoveAt(i);
                    }
                    else if (currentMessage.FindIndex(x => x.ToLower().Contains(message.ToLower())) < 0)
                    {
                        if (currentMessage.FindIndex(x => x.ToLower().Contains("wrong pad")) > -1)
                            currentMessage.RemoveAt(currentMessage.FindIndex(x => x.ToLower().Contains("wrong pad")));

                        currentMessage.Insert(0, message.ToUpper());
                    }
                }
            }

            try
            {

                lock (myVMSLock)
                {

                    string vmsMessage = "'B";


                    int i = 0;

                    foreach (string line in currentMessage)
                    {
                        if (Location == 5)
                        {
                            if (i == 0)
                                vmsMessage += "'CFFFF00PAD " + DeviceNo.ToString().PadLeft(2, '0') + " ";
                            else
                                vmsMessage += "       ";
                        }


                        vmsMessage += line + "'\\n";
                        i++;
                        if (i >= LedHeight)
                            break;
                    }
                    //}
#if DEBUG
                    //log.Info("Adding {1} in {0} vms in Thread {2}", IPAddress, vmsMessage, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    //Moved to async call to SendVMSMessage
                    //for (int v = 0; v < 2 && !OpenConnection(); v++) ;
                    //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(vmsMessage));
                    //Disconnect();
                    lock (lockerQueueMsg)
                    {
                        //queuedMessage[vmsMessage] = DateTime.Now;
                       lastmsg = vmsMessage;
                    }
                    if (!methodCalled)
                    { methodCalled = true; messageList.BeginInvoke(null, null); }
                    //messageList();
 
                    // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(" "));
                }
            }
            catch (Exception ex)
            {
                try
                {

                    if (tcpListener != null)
                    {
                        // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'D"));
                        // tcpListener.Disconnect(false);
                        tcpListener = null;
                    }
#if DEBUG
                    log.Error("Exception thrown while send [ {1} ] message to new {2} VMS: [ {0} ]", ex.Message, message, IPAddress);
#endif
                   
                }
                catch { tcpListener = null; }
                if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                    tmrRefreshScreen.Start();
                return false;
            }


            if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                tmrRefreshScreen.Start();
            return true;

        }


        private bool OpenConnection()
        {
            try
            {
#if DEBUG
                 log.Info("Trying to connect to {0} vms on {1} port Device no {2} with 5000ms IP ping timeout. Thread{3}", IPAddress, UDPPort, DeviceNo.ToString().PadLeft(2, '0'), System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                //if (new Ping().Send(System.Net.IPAddress.Parse(IPAddress),1000).Status != IPStatus.Success)
                //    return false;

                tcpListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                tcpListener.Connect(new IPEndPoint(System.Net.IPAddress.Parse(IPAddress), UDPPort));
                byte[] recData = new byte[250];
                tcpListener.Receive(recData);
                tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(DeviceNo.ToString().PadLeft(2, '0')));
                tcpListener.Receive(recData);
                firstMessge = true;
            }
            catch (Exception ex)
            {
#if DEBUG
                 log.Error("Exception thrown while opening conncetion to VMS. : {0}", ex.Message);
#endif
                return false;
            }

#if DEBUG
             log.Info("Connected to {0} vms on {1} port Device no {2}. Thread {3}", IPAddress, UDPPort, DeviceNo.ToString().PadLeft(2, '0'),System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
            return true;
        }

        private void SetTimer(int intver)
        {
            try
            {
                if (tmrRefreshScreen == null)
                    tmrRefreshScreen = new Timer();

                tmrRefreshScreen.Stop();
                tmrRefreshScreen.Interval = intver;
                tmrRefreshScreen.Elapsed += new ElapsedEventHandler(tmrRefreshScreen_Elapsed);
            }
            catch (Exception ex)
            {
#if DEBUG
                 log.Error("Exception thrown while setting timer for default VMS message. : {0}", ex.Message);
#endif
            }

        }

       

        public void DisplayDefaultMessage()
        {
#if DEBUG
            log.Info("Enterd to display default message  Thread {0}",System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif



            //newVMWS
            firstMessge = true;
            try
            {
                tmrRefreshScreen.Stop();

                lock (myVMSLock)
                {
                    if (Location == 1)
                    {
                        if (currentMessage != null)
                        { currentMessage.Clear();   }
                    }
                    else if (Location == 2 || Location == 5)
                    {
                        if (currentMessage != null && currentMessage.Count > 0)
                        {
                            if (!tmrRefreshScreen.Enabled)
                                tmrRefreshScreen.Start(); 
                            return;
                        }
                    }


#if DEBUG
                    //log.Info("Adding DefaultMessage for {0} vms in Thread {1}", IPAddress, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                    //Moved to async call in SendVMSMessage
                    //for (int v = 0; v < 2 && !OpenConnection(); v++) ;
                    //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'B'CFFFF00" + DefaultMessage.ToUpper()));
                    //Disconnect();
                    lock (lockerQueueMsg)
                    {
                       // queuedMessage["'B'CFFFF00" + DefaultMessage.ToUpper()] = DateTime.Now;
                         lastmsg = "'B'CFFFF00" + DefaultMessage.ToUpper();
                    }
                   if(!methodCalled)
                   { methodCalled = true; messageList.BeginInvoke(null, null); }
                    //messageList();
                    

 
                }

            }
            catch (Exception ex1)
            {
#if DEBUG
                 log.Error("Exception thrown while sending default message to new {1} VMS: [ {0} ]", ex1.Message, IPAddress);
#endif
                if (tcpListener != null)
                {
                    //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'D"));
                    //tcpListener.Disconnect(false);
                    tcpListener = null;
                }


                //StartRefreshTimer();
            }

            if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                tmrRefreshScreen.Start();
        }

        public void RemoveMessage(string Str_, string formatInfo)
        {
            try
            {

                if (currentMessage == null)
                {
                    currentMessage = new List<string>();
                    return;
                }

                string vmsMessage = "'B";
                lock (myVMSLock)
                {
                    currentMessage.Remove(currentMessage.Find(x => x.ToLower().Contains(Str_.Trim().ToLower().Substring(0, (Str_.Length > 4) ? 4 : Str_.Length))));
                    int i = 0;
                    if (currentMessage.Count < 1)
                    {
                        DisplayDefaultMessage();
                        return;
                    }
                    foreach (string line in currentMessage)
                    {
                        if (Location == 5)
                        {
                            if (i == 0)
                                vmsMessage += "'CFFFF00PAD " + DeviceNo.ToString().PadLeft(2, '0') + " ";
                            else
                                vmsMessage += "       ";
                        }


                        vmsMessage += line + "'\\n";
                        i++;
                        if (i >= LedHeight)
                            break;


                    }


                    if (currentMessage.Count > 0)
                    {
#if DEBUG
                        //log.Info("Adding {1} in {0} vms in Thread {2}", IPAddress, vmsMessage, System.Threading.Thread.CurrentThread.ManagedThreadId);
#endif
                        //Moved to async call with SendVMSMessage
                        //for (int v = 0; v < 2 && !OpenConnection(); v++) ;
                        //tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(vmsMessage));
                        //Disconnect();
                        lock (lockerQueueMsg)
                        {
                            // queuedMessage[vmsMessage] = DateTime.Now;
                            lastmsg = vmsMessage;
                        }
                        if (!methodCalled)
                        { methodCalled = true; messageList.BeginInvoke(null, null); }
                        //messageList();
                    }
                    else
                    {
                        DisplayDefaultMessage();
                    }

                    // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes(" "));
                }

                if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                    tmrRefreshScreen.Start();


            }
            catch (Exception ex)
            {
                try
                {

                    if (tcpListener != null)
                    {
                        // tcpListener.Send(ASCIIEncoding.ASCII.GetBytes("'D"));
                        //tcpListener.Disconnect(false);
                        tcpListener = null;
                    }
#if DEBUG
                    log.Error("Exception thrown while removing message to new VMS: [ {0} ]", ex.Message);
#endif

                }
                catch
                {
                    tcpListener = null;
                }
                if (tmrRefreshScreen != null && !tmrRefreshScreen.Enabled)
                    tmrRefreshScreen.Start();
            }
        }

    }//end of AussportVMS
}//end of namespace
