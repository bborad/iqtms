using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ramp.VMSHWConnector.Common
{
    public class VMSHWUtility
    {        
        public static short RetryTimes { get; set; }
        public static short TimeOut { get; set; }
        public static int Baudrate { get; set; } // value 38400
        
    }
}
