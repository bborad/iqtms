/**************************************************************************************
 * Author : Rahul Panwar
 * Created Date : 12-March-2008, Wednesday
 * Last Modified by : 
 * Last Modified : 12-March-2008, Wednesday
 * Module Name : Business
 * Decription : This class will be an interface to the DataAccess layer, any database 
 *              operation performed in this layer will be performed from this class.
 **************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Ramp.Data;
using Ramp.Data.Sql;
using System.Data.SqlClient;
using System.Timers;

///NameSpace didnt change because its refered in other projects
namespace Ramp.DataAccess.Data
{
    public class DatabaseFacade
    {
        #region "Constructers"

        /// <summary>
        /// Static Constructer. This contructer is called when class is loaded by Class Loader in CLR
        /// </summary>
        static DatabaseFacade()
        {
            /// initialize sqlDataBase object with connection string "activeConnectionString" from configuration
            /// file ( web.config )
            /// 

            //string activeConnectionString = ConfigurationManager.ConnectionStrings["PrimaryConnectionString"].ConnectionString;
            //ConnectionStrings = new string[2];
            //ConnectionStrings[0] = 

            ConnectionString = ConfigurationManager.ConnectionStrings["activeConnectionString"].ConnectionString;

            //SQLDB = new SqlDatabase(ConnectionString);

            // changes made on 28/05/2011 for DB failover functionality
            if(ConfigurationManager.ConnectionStrings["activeConnectionString2"] != null)
             ConnectionString2 = ConfigurationManager.ConnectionStrings["activeConnectionString2"].ConnectionString;

            string value = ConfigurationManager.AppSettings["DBFailOverCheckInterval"];

            if (value != null && value.Length > 0)
            {
                DBFailOverInterval = Convert.ToInt32(value);
            }
            else
            {
                DBFailOverInterval = 0;
            }

            if (DBFailOverInterval <= 0)
            {
                SQLDB = new SqlDatabase(ConnectionString);
            }
            else
            {
                // check if connection string 1 is working

                SqlConnection sqlcon = new SqlConnection(ConnectionString);
                try
                {
                    sqlcon.Open();
                    if (sqlcon.State == System.Data.ConnectionState.Open)
                        sqlcon.Close();

                    SQLDB = new SqlDatabase(ConnectionString);
                    IsPrimaryServer = true;
                }
                catch
                {
                    try
                    {
                        sqlcon = new SqlConnection(ConnectionString2);
                        sqlcon.Open();
                        if (sqlcon.State == System.Data.ConnectionState.Open)
                            sqlcon.Close();

                        SQLDB = new SqlDatabase(ConnectionString2);
                        IsPrimaryServer = false;
                    }
                    catch
                    {
                    }
                }

                tmrCheckFailover = new Timer();
                tmrCheckFailover.Elapsed += new ElapsedEventHandler(tmrCheckFailover_Elapsed);
                tmrCheckFailover.Interval = DBFailOverInterval * 1000;
                tmrCheckFailover.Start();
            }

        }

        static void tmrCheckFailover_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                tmrCheckFailover.Stop();
                CheckSQLDBFailover();
            }
            catch
            {
            }
            tmrCheckFailover.Start();
        }

        #endregion

        #region "Public Static Varibles"

        /// <summary>
        /// Database Class object for SQL server
        /// </summary>
        public static SqlDatabase SQLDB;
        public static string ConnectionString;

        public static string ConnectionString2;

        public static bool IsPrimaryServer;

        #endregion

        #region "Public Properties"
        public static Timer tmrCheckFailover;
        static int DBFailOverInterval;
        #endregion

        #region "Public static Methods"
        /// <summary>
        /// Modified by : Anupam Garg
        /// Date 08 July 2008
        /// Check for Guid
        /// </summary>
        static public bool IsGuid(string num)
        {
            try
            {
                Guid guidID = new Guid(num);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //static public bool ConfigureDataBase()
        //{
        //    bool result = false;
        //    try
        //    {


        //    }
        //    catch
        //    {
        //    }

        //    return result;
        //}

        public static void CheckSQLDBFailover()
        {
            SqlConnection sqlcon;

            if (IsPrimaryServer)
            {
                try
                {   
                    sqlcon = new SqlConnection(ConnectionString);
                    sqlcon.Open();
                    if (sqlcon.State == System.Data.ConnectionState.Open)
                        sqlcon.Close();

                    //SQLDB = new SqlDatabase(ConnectionString);
                    //IsPrimaryServer = true;
                }
                catch
                {
                    try
                    {
                        sqlcon = new SqlConnection(ConnectionString2);
                        sqlcon.Open();
                        if (sqlcon.State == System.Data.ConnectionState.Open)
                            sqlcon.Close();

                        SQLDB = new SqlDatabase(ConnectionString2);
                        IsPrimaryServer = false;
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                try
                {
                    sqlcon = new SqlConnection(ConnectionString2);
                    sqlcon.Open();
                    if (sqlcon.State == System.Data.ConnectionState.Open)
                        sqlcon.Close();

                    //SQLDB = new SqlDatabase(ConnectionString);
                    //IsPrimaryServer = false;
                }
                catch
                {
                    try
                    {
                        sqlcon = new SqlConnection(ConnectionString);
                        sqlcon.Open();
                        if (sqlcon.State == System.Data.ConnectionState.Open)
                            sqlcon.Close();

                        SQLDB = new SqlDatabase(ConnectionString2);
                        IsPrimaryServer = true;
                    }
                    catch
                    {
                    }
                }
            }


        }

        #endregion

        #region "Private Methods"
        #endregion

        #region "Static Methods"
        #endregion


    }
}
