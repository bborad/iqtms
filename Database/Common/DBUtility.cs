using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Middleware = Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Common;
using System.Data.SqlClient;
using System.Data;
using Ramp.DataAccess.Data;
using System.Net.Mail;

using NLog;

namespace Ramp.Database.Common
{
    public class DBUtility
    {
        public static bool IsTruckArrived(Location loc, string TagRFID)
        {

            bool flagResult = false;
            try
            {
                // check in the active reads table whether last position of the TagRFID is loc 
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetLastPosition";

                    command.Parameters.Add("TagRFID", SqlDbType.NVarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;

                    int iPosition = -1;

                    object value = DatabaseFacade.SQLDB.ExecuteScalar(command);
                    if (value != null && value != DBNull.Value)
                    {
                        iPosition = (Int32)value;

                        if (Convert.ToInt32(loc) == iPosition)
                        {
                            flagResult = true;
                        }
                    }
                    else
                    {
                        flagResult = true;
                    }

                  
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        //  MAX QUEUE 
        public static void GetExpectedTimeValues()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_GetValues";

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ExpectedValues.REDS_Queue = reader.GetInt32(reader.GetOrdinal("REDS_Queue"));
                                ExpectedValues.Queue_Ent1 = reader.GetInt32(reader.GetOrdinal("Queue_Ent1"));
                                ExpectedValues.Ent1_Ent2 = reader.GetInt32(reader.GetOrdinal("Ent1_Ent2"));
                                ExpectedValues.QueueTimePerTruck = reader.GetInt32(reader.GetOrdinal("QueueTimePerTruck"));
                                ExpectedValues.Unloading = reader.GetInt32(reader.GetOrdinal("Unloading"));
                                ExpectedValues.StartUp = reader.GetInt32(reader.GetOrdinal("StartUp"));
                                ExpectedValues.ShutDown = reader.GetInt32(reader.GetOrdinal("ShutDown"));
                                ExpectedValues.TruckCallResponse = reader.GetInt32(reader.GetOrdinal("TruckCallResponse"));
                                ExpectedValues.TruckQueueMax = reader.GetInt32(reader.GetOrdinal("TruckQueueMax"));
                                ExpectedValues.End_Transaction = reader.GetInt32(reader.GetOrdinal("End_Transaction"));
                                ExpectedValues.End_Transaction2 = reader.GetInt32(reader.GetOrdinal("End_Transaction2"));

                                ExpectedValues.Exit_REDS = reader.GetInt32(reader.GetOrdinal("Exit_REDS"));
                                ExpectedValues.TruckWash_REDS = reader.GetInt32(reader.GetOrdinal("TruckWash_REDS"));
                                ExpectedValues.Precision = reader.GetInt32(reader.GetOrdinal("Precision"));
                                ExpectedValues.TagReadFrequency = reader.GetInt32(reader.GetOrdinal("TagReadFrequency"));

                                ExpectedValues.Max_Delay = reader.GetInt32(reader.GetOrdinal("MaxDelay"));
                                ExpectedValues.Exit_Ent1 = reader.GetInt32(reader.GetOrdinal("Exit_Ent1"));
                                ExpectedValues.TruckWash_Ent1 = reader.GetInt32(reader.GetOrdinal("TruckWash_Ent1"));
                                ExpectedValues.UnTarpTime = reader.GetInt32(reader.GetOrdinal("UnTarpTime"));
                               

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Common Mathod For Send Email 
        /// </summary>
        /// <returns></returns>
        public static void SendEmail(string FromAddress, string ToAddress, string CC, string BCC, string Attachment, string Subject, string MessageBody)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromAddress);
                mailMessage.To.Add(ToAddress);

                if (CC.Trim() != string.Empty)
                {
                    //mailMessage.CC.Add(new MailAddress(CC));
                    string[] strCC = CC.Split(',');
                    foreach (string sCC in strCC)
                    {
                        if (sCC.Trim() != string.Empty)
                            mailMessage.CC.Add(new MailAddress(sCC));
                    }
                }
                if (BCC.Trim() != string.Empty)
                    mailMessage.Bcc.Add(new MailAddress(BCC));
                mailMessage.Subject = Subject;//"Online Account at firstrehab.com";
                mailMessage.Body = MessageBody;
                if (Attachment.Trim() != string.Empty)
                {
                    mailMessage.Attachments.Add(new Attachment(Attachment));
                }
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = MailPriority.Normal;
                SmtpClient smtpClient = new SmtpClient();
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                smtpClient.Send(mailMessage);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get ALL Expected Time Values
        /// </summary>
        public static DataSet GetALLExpectedTimeValues()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_GetAllValues";

                    //    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    //    {
                    //        if (reader != null && reader.HasRows)
                    //        {
                    //            while (reader.Read())
                    //            {
                    //                ExpectedValues.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                    //                ExpectedValues.REDS_Queue = reader.GetInt32(reader.GetOrdinal("REDS_Queue"));
                    //                ExpectedValues.Queue_Ent1 = reader.GetInt32(reader.GetOrdinal("Queue_Ent1"));
                    //                ExpectedValues.Ent1_Ent2 = reader.GetInt32(reader.GetOrdinal("Ent1_Ent2"));
                    //                ExpectedValues.QueueTimePerTruck = reader.GetInt32(reader.GetOrdinal("QueueTimePerTruck"));
                    //                ExpectedValues.Unloading = reader.GetInt32(reader.GetOrdinal("Unloading"));
                    //                ExpectedValues.StartUp = reader.GetInt32(reader.GetOrdinal("StartUp"));
                    //                ExpectedValues.ShutDown = reader.GetInt32(reader.GetOrdinal("ShutDown"));
                    //                ExpectedValues.TruckCallResponse = reader.GetInt32(reader.GetOrdinal("TruckCallResponse"));
                    //                ExpectedValues.TruckQueueMax = reader.GetInt32(reader.GetOrdinal("TruckQueueMax"));
                    //                ExpectedValues.End_Transaction = reader.GetInt32(reader.GetOrdinal("End_Transaction"));
                    //                ExpectedValues.End_Transaction2 = reader.GetInt32(reader.GetOrdinal("End_Transaction2"));

                    //                ExpectedValues.Exit_REDS = reader.GetInt32(reader.GetOrdinal("Exit_REDS"));
                    //                ExpectedValues.TruckWash_REDS = reader.GetInt32(reader.GetOrdinal("TruckWash_REDS"));
                    //                ExpectedValues.Precision = reader.GetInt32(reader.GetOrdinal("Precision"));
                    //                ExpectedValues.TagReadFrequency = reader.GetInt32(reader.GetOrdinal("TagReadFrequency"));
                    //            }
                    //        }
                    //    }

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateExpectedTimeValues(string ID,string REDS_Queue, string Queue_Ent1, string Ent1_Ent2, string QueueTimePerTruck, string Unloading, string StartUp,
           string ShutDown, string TruckCallResponse, string TruckQueueMax, string Exit_REDS, string TruckWash_REDS, string Precision, string TagReadFrequency,
           string MaxDelay, string Exit_Ent1, string TruckWash_Ent1)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_UpdateExpectedTimes";
                    ///ADD Parameters
                    ///
                    command.Parameters.Add("ID", SqlDbType.Int);
                    if (ID != string.Empty)
                    {
                        command.Parameters["ID"].Value = ID;
                    }
                    else
                    {
                        command.Parameters["ID"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("REDS_Queue", SqlDbType.Int);
                    if (REDS_Queue != string.Empty)
                    {
                        command.Parameters["REDS_Queue"].Value = REDS_Queue;
                    }
                    else
                    {
                        command.Parameters["REDS_Queue"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("Queue_Ent1", SqlDbType.Int);
                    if (Queue_Ent1 != string.Empty)
                    {
                        command.Parameters["Queue_Ent1"].Value = Queue_Ent1;
                    }
                    else
                    {
                        command.Parameters["Queue_Ent1"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("Ent1_Ent2", SqlDbType.Int);
                    if (Ent1_Ent2 != string.Empty)
                    {
                        command.Parameters["Ent1_Ent2"].Value = Ent1_Ent2;
                    }
                    else
                    {
                        command.Parameters["Ent1_Ent2"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("QueueTimePerTruck", SqlDbType.Int);
                    if (QueueTimePerTruck != string.Empty)
                    {
                        command.Parameters["QueueTimePerTruck"].Value = QueueTimePerTruck;
                    }
                    else
                    {
                        command.Parameters["QueueTimePerTruck"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("Unloading", SqlDbType.Int);
                    if (Unloading != string.Empty)
                    {
                        command.Parameters["Unloading"].Value = Unloading;
                    }
                    else
                    {
                        command.Parameters["Unloading"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("StartUp", SqlDbType.Int);
                    if (StartUp != string.Empty)
                    {
                        command.Parameters["StartUp"].Value = StartUp;
                    }
                    else
                    {
                        command.Parameters["StartUp"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("ShutDown", SqlDbType.Int);
                    if (ShutDown != string.Empty)
                    {
                        command.Parameters["ShutDown"].Value = ShutDown;
                    }
                    else
                    {
                        command.Parameters["ShutDown"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TruckCallResponse", SqlDbType.Int);
                    if (TruckCallResponse != string.Empty)
                    {
                        command.Parameters["TruckCallResponse"].Value = TruckCallResponse;
                    }
                    else
                    {
                        command.Parameters["TruckCallResponse"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TruckQueueMax", SqlDbType.Int);
                    if (TruckQueueMax != string.Empty)
                    {
                        command.Parameters["TruckQueueMax"].Value = TruckQueueMax;
                    }
                    else
                    {
                        command.Parameters["TruckQueueMax"].Value = DBNull.Value;
                    }
                    //command.Parameters.Add("End_Transaction", SqlDbType.Int);
                    //if (End_Transaction != string.Empty)
                    //{
                    //    command.Parameters["End_Transaction"].Value = End_Transaction;
                    //}
                    //else
                    //{
                    //    command.Parameters["End_Transaction"].Value = DBNull.Value;
                    //}
                    //command.Parameters.Add("End_Transaction2", SqlDbType.Int);
                    //if (End_Transaction2 != string.Empty)
                    //{
                    //    command.Parameters["End_Transaction2"].Value = End_Transaction2;
                    //}
                    //else
                    //{
                    //    command.Parameters["End_Transaction2"].Value = DBNull.Value;
                    //}
                    command.Parameters.Add("Exit_REDS", SqlDbType.Int);
                    if (Exit_REDS != string.Empty)
                    {
                        command.Parameters["Exit_REDS"].Value = Exit_REDS;
                    }
                    else
                    {
                        command.Parameters["Exit_REDS"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TruckWash_REDS", SqlDbType.Int);
                    if (TruckWash_REDS != string.Empty)
                    {
                        command.Parameters["TruckWash_REDS"].Value = TruckWash_REDS;
                    }
                    else
                    {
                        command.Parameters["TruckWash_REDS"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("Precision", SqlDbType.Int);
                    if (Precision != string.Empty)
                    {
                        command.Parameters["Precision"].Value = Precision;
                    }
                    else
                    {
                        command.Parameters["Precision"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TagReadFrequency", SqlDbType.Int);
                    if (TagReadFrequency != string.Empty)
                    {
                        command.Parameters["TagReadFrequency"].Value = TagReadFrequency;
                    }
                    else
                    {
                        command.Parameters["TagReadFrequency"].Value = DBNull.Value;
                    }

                    command.Parameters.Add("MaxDelay", SqlDbType.Int);
                    if (MaxDelay != string.Empty)
                    {
                        command.Parameters["MaxDelay"].Value = MaxDelay;
                    }
                    else
                    {
                        command.Parameters["MaxDelay"].Value = DBNull.Value;
                    }

                    command.Parameters.Add("Exit_Ent1", SqlDbType.Int);
                    if (Exit_Ent1 != string.Empty)
                    {
                        command.Parameters["Exit_Ent1"].Value = Exit_Ent1;
                    }
                    else
                    {
                        command.Parameters["Exit_Ent1"].Value = DBNull.Value;
                    }

                    command.Parameters.Add("TruckWash_Ent1", SqlDbType.Int);
                    if (TruckWash_Ent1 != string.Empty)
                    {
                        command.Parameters["TruckWash_Ent1"].Value = TruckWash_Ent1;
                    }
                    else
                    {
                        command.Parameters["TruckWash_Ent1"].Value = DBNull.Value;
                    }
                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }


        /// <summary>
        /// To Get VMS status for system status report
        /// </summary>        
        /// <returns></returns>
        public static DataSet GetVMSStatusForSystemReport()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "VMSDevices_GetVMSStatusForSystemReport";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// To Get Today's Alters for system status report
        /// </summary>        
        /// <returns></returns>
        public static DataSet GetTodaysAltersForSystemReport()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Alters_GetTodaysAlters";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// To Get All Security Groups
        /// </summary>        
        /// <returns></returns>
        public static DataSet GetAllSecurityGroup()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SecurityGroups_GetSecutityGroups";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get ALL Expected Time Values
        /// </summary>
        public static DataSet ArchiveExpectedAndHopperTime()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_ArchiveExpectedAndHopperTime";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static DataSet GetConfigurationValues()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configurations_GetAll";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public static void UpdateConfigurations(Int32 ID, string Value, int VMSDeviceLocation)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configurations_Update";

                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;

                    command.Parameters.Add("Value", SqlDbType.NVarChar);
                    command.Parameters["Value"].Value = Value;

                    command.Parameters.Add("VMSDeviceLocation", SqlDbType.Int);
                    command.Parameters["VMSDeviceLocation"].Value = VMSDeviceLocation;

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void UpdateAlerts(string alertDesc, int alertID, bool IsDeleted)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Alerts_Update";

                    command.Parameters.Add("AlertID", SqlDbType.Int);
                    command.Parameters["AlertID"].Value = alertID;

                    command.Parameters.Add("AlertDesc", SqlDbType.NVarChar);
                    command.Parameters["AlertDesc"].Value = alertDesc;

                    command.Parameters.Add("IsDeleted", SqlDbType.Bit);
                    command.Parameters["IsDeleted"].Value = IsDeleted;

                    

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static DataTable GetTagsToDispatch()
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = null;
            
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "TagsToIssue_GetTagsToDispatch";
                    ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    if (ds != null && ds.Tables[0] != null)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            catch (Exception)
            {
                
            }
            finally
            {
               
            }
            return dt;
        }

        public static DataTable GetRunningServerInfo()
        {
            DataTable dt = null;
            try
            {
                DataSet ds = null;

                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ServerInfo_GetStatus";
                    ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    if (ds != null && ds.Tables[0] != null)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return dt;
        }

        public static int SwitchToNewServer(string ServerName)
        {
            int result = 0;
            try
            { 
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ServerInfo_SwitchToNewServer";

                    command.Parameters.Add("ServerName", SqlDbType.VarChar,50);
                    command.Parameters["ServerName"].Value = ServerName;

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);   
                }
            }
            catch (Exception)
            {
                result = -1;
            }

            return result;
          
        }

        public static int UpdateServerStatus(string ServerName)
        {
            int result = 0;
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ServerInfo_UpdateStatus";

                    command.Parameters.Add("ServerName", SqlDbType.VarChar,50);
                    command.Parameters["ServerName"].Value = ServerName;

                    object objResult = DatabaseFacade.SQLDB.ExecuteScalar(command);

                    if (objResult != null)
                    {
                        result = Convert.ToInt32(objResult);
                    }
                                        
                }
            }
            catch (Exception)
            {
                result = -1;
            }

            return result;

        }

        public static void UpdateTagsToIssue(string TagRFID, Int64 TagID, Location lastLoc, bool IsDispatched, bool IsDeleted)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "TagsToIssue_Update";

                    command.Parameters.Add("TagID", SqlDbType.BigInt);
                    command.Parameters["TagID"].Value = TagID;

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar,50);
                    command.Parameters["TagRFID"].Value = TagRFID;

                    command.Parameters.Add("LastLocation", SqlDbType.Int);
                    command.Parameters["LastLocation"].Value = (int)lastLoc;

                    command.Parameters.Add("IsDispatched", SqlDbType.Bit);
                    command.Parameters["IsDispatched"].Value = IsDispatched;

                    command.Parameters.Add("IsDeleted", SqlDbType.Bit);
                    command.Parameters["IsDeleted"].Value = IsDeleted;

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }

        public static void AddUpdateTagsToIssue(DataTable dtTagsToIssue)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "TagsToIssue_Update";
                    
                    command.Parameters.Add("@TagID", SqlDbType.BigInt, 0, "TagID");
                   
                    command.Parameters.Add("@TagRFID", SqlDbType.VarChar, 0, "TagRFID"); 
                     
                    command.Parameters.Add("@LastLocation", SqlDbType.Int, 0, "LastLocation");                                      

                    command.Parameters.Add("@IsDispatched", SqlDbType.Bit, 0, "IsDispatched"); 
                
                    command.Parameters.Add("@IsDeleted", SqlDbType.Bit, 0, "IsDeleted");

                    SqlDataAdapter da = (SqlDataAdapter)DatabaseFacade.SQLDB.GetDataAdapter();

                    da.InsertCommand = command;

                    da.UpdateCommand = command;

                    command.Connection = (SqlConnection)DatabaseFacade.SQLDB.CreateConnection();

                    da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

                    da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                    da.UpdateBatchSize = 20;

                    da.Update(dtTagsToIssue);
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }

        public static DataTable GetAllRefernceNo()
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = null;

                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "IssuedTags_GetAllReferenceNo";
                    ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    if (ds != null && ds.Tables[0] != null)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return dt;
        }


    }
}
