using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.Database.Connector
{
    public class ActiveReads : IActiveReads
    {
        #region "private variables"

        public int ID { get; set; }
        public int TagID { get; set; }
        public int HopperID { get; set; }
        public int LastPosition { get; set; }
        public int EntryORQueue { get; set; }
        public DateTime REDS { get; set; }
        public DateTime Queue { get; set; }
        public DateTime Entry1 { get; set; }
        public DateTime Entry2 { get; set; }
        public DateTime HopperTime { get; set; }
        public DateTime TruckWash { get; set; }
        public DateTime REDS2 { get; set; }
        public DateTime TruckCallUp { get; set; }


        #endregion

        #region "Public Methods"

        public void SendTruckToQueueBack(string TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_SendTruckToQueueBack";

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                     

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //  MAX QUEUE 
        public Int32 GetTotalTagsAtLocation(Location loc)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetTotalTags";

                    command.Parameters.Add("LastPosition", SqlDbType.Int);
                    command.Parameters["LastPosition"].Value = (Int32)loc;

                    return (Int32)DatabaseFacade.SQLDB.ExecuteScalar(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        //  MAX QUEUE 
        public Int32 GetQueueLengthForHopper(Int32 HopperID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetQueueLengthForHopper";

                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = (Int32)Location.QUEUE;

                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;


                    return (Int32)DatabaseFacade.SQLDB.ExecuteScalar(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Public Static Methods"


        public static List<IActiveReads> GetAllRecordsByHopperID(int HopperID)
        {

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetAllRecordsByHopperID";

                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    ActiveReads objActiveRead;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                objActiveRead = new ActiveReads();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objActiveRead.ID = reader.GetInt32(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TagID")))
                                    objActiveRead.TagID = reader.GetInt32(reader.GetOrdinal("TagID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
                                    objActiveRead.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("REDS")))
                                    objActiveRead.REDS = reader.GetDateTime(reader.GetOrdinal("REDS"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Queue")))
                                    objActiveRead.Queue = reader.GetDateTime(reader.GetOrdinal("Queue"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Entry1")))
                                    objActiveRead.Entry1 = reader.GetDateTime(reader.GetOrdinal("Entry1"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Entry2")))
                                    objActiveRead.Entry2 = reader.GetDateTime(reader.GetOrdinal("Entry2"));

                                if (!reader.IsDBNull(reader.GetOrdinal("HopperTime")))
                                    objActiveRead.HopperTime = reader.GetDateTime(reader.GetOrdinal("HopperTime"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TruckWash")))
                                    objActiveRead.TruckWash = reader.GetDateTime(reader.GetOrdinal("TruckWash"));

                                if (!reader.IsDBNull(reader.GetOrdinal("REDS2")))
                                    objActiveRead.REDS2 = reader.GetDateTime(reader.GetOrdinal("REDS2"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TruckCallUp")))
                                    objActiveRead.TruckCallUp = reader.GetDateTime(reader.GetOrdinal("TruckCallUp"));

                                if (!reader.IsDBNull(reader.GetOrdinal("LastPosition")))
                                    objActiveRead.LastPosition = reader.GetInt32(reader.GetOrdinal("LastPosition"));

                                objActiveReads_Lst.Add(objActiveRead);
                            }

                            return objActiveReads_Lst;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static int UpdateTransaction(Int64 TagID, string TagRFID, Location LastPosition)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_UpdateTransaction";

                    command.Parameters.Add("TagID", SqlDbType.BigInt);
                    command.Parameters["TagID"].Value = TagID;

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    

                    command.Parameters.Add("LastPosition", SqlDbType.Int);
                    command.Parameters["LastPosition"].Value = (Int32)LastPosition;

                    return (Int32)DatabaseFacade.SQLDB.ExecuteScalar(command);

                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static bool UpdateTruckCallUp(string TagRFID, ref VMSParameters vmsParam, ref int TruckCallUpRetries, ref DataTable dtVMSSettings)
        {
            bool IsTransExists = false;

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_UpdateTruckCallUp";

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    

                    command.Parameters.Add("VMSLogging", SqlDbType.Int);
                    command.Parameters["VMSLogging"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSUseRandomPortNo", SqlDbType.Int);
                    command.Parameters["VMSUseRandomPortNo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSMinPortNo", SqlDbType.Int);
                    command.Parameters["VMSMinPortNo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSMaxPortNo", SqlDbType.Int);
                    command.Parameters["VMSMaxPortNo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("TruckCallUpRetries", SqlDbType.Int);
                    command.Parameters["TruckCallUpRetries"].Direction = ParameterDirection.Output;

                    object result = null;

                    //result = DatabaseFacade.SQLDB.ExecuteScalar(command);

                    int value = 0; 

                    DataSet ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        value = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                    }

                    if (value == 1)
                    {
                        IsTransExists = true;

                        dtVMSSettings = ds.Tables[1];

                        result = command.Parameters["TruckCallUpRetries"].Value;
                        if (result != null)
                        {
                            TruckCallUpRetries = Convert.ToInt16(result);
                        }
                        else
                        {
                            TruckCallUpRetries = -1;
                        }

                        result = command.Parameters["VMSLogging"].Value;
                        if (result != null)
                        {
                            vmsParam.VMSLogging = Convert.ToInt32(result);
                        }

                        result = command.Parameters["VMSUseRandomPortNo"].Value;
                        if (result != null)
                        {
                            vmsParam.UseRandomPort = Convert.ToBoolean(result);
                        }

                        result = command.Parameters["VMSMinPortNo"].Value;
                        if (result != null)
                        {
                            vmsParam.MinPortNo = Convert.ToInt16(result);
                        }

                        result = command.Parameters["VMSMaxPortNo"].Value;
                        if (result != null)
                        {
                            vmsParam.MaxPortNo = Convert.ToInt16(result);
                        }

                    }

                }
            }
            catch (Exception)
            {
                // throw;

            }
            return IsTransExists;
        }

        public static ITags OpenCloseTransaction(string TagRFID, Location loc, Int32 Option, string NewLoopID, string OldLoopID, int TransStatus, string TransMessage, string TagBatteryStatus,out int timerAdjust,out string newHOrder)
        {
            timerAdjust = 0;
            newHOrder = "";
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_OpenCloseTransaction";

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                     

                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = (Int32)loc;

                    command.Parameters.Add("Option", SqlDbType.Int);
                    command.Parameters["Option"].Value = Option;

                    command.Parameters.Add("TransStatus", SqlDbType.Int);
                    command.Parameters["TransStatus"].Value = TransStatus;

                    command.Parameters.Add("TransMessage", SqlDbType.VarChar);
                    command.Parameters["TransMessage"].Value = TransMessage;
                 

                    command.Parameters.Add("TagBatteryStatus", SqlDbType.VarChar);
                    command.Parameters["TagBatteryStatus"].Value = TagBatteryStatus;

                    
                    command.Parameters.Add("NewLoopID", SqlDbType.VarChar);
                    if (NewLoopID.Length > 0)
                    {
                        command.Parameters["NewLoopID"].Value = NewLoopID;
                    }
                    else
                    {
                        command.Parameters["NewLoopID"].Value = DBNull.Value;
                    }
                     
                    command.Parameters.Add("OldLoopID", SqlDbType.VarChar);
                    if (OldLoopID.Length > 0)
                    {
                        command.Parameters["OldLoopID"].Value = OldLoopID;
                    }
                    else
                    {
                        command.Parameters["OldLoopID"].Value = DBNull.Value;
                    }
                    
                    ITags objTag;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                objTag = new Tags();
                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objTag.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("TagRFID")))
                                    objTag.TagRFID = reader.GetString(reader.GetOrdinal("TagRFID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ProponentID")))
                                    objTag.ProponentID = reader.GetInt32(reader.GetOrdinal("ProponentID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
                                    objTag.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Assigned")))
                                    objTag.Assigned = Convert.ToBoolean(reader.GetInt32(reader.GetOrdinal("Assigned")));
                                if (!reader.IsDBNull(reader.GetOrdinal("TruckID")))
                                    objTag.TruckID = reader.GetString(reader.GetOrdinal("TruckID"));  //reader.GetInt32(reader.GetOrdinal("TruckID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("DesPad")))
                                    objTag.DestinationPad = reader.GetInt32(reader.GetOrdinal("DesPad"));

                                

                                objTag.AssignedHopper = new Hoppers();

                                objTag.AssignedHopper.ID = objTag.HopperID;

                                if (!reader.IsDBNull(reader.GetOrdinal("HopperName")))
                                    objTag.AssignedHopper.HopperName = reader.GetString(reader.GetOrdinal("HopperName"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeFromEnt2")))
                                    objTag.AssignedHopper.TimeFromEnt2 = reader.GetInt32(reader.GetOrdinal("TimeFromEnt2"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeToTW")))
                                    objTag.AssignedHopper.TimeToTW = reader.GetInt32(reader.GetOrdinal("TimeToTW"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerID")))
                                    objTag.AssignedHopper.MarkerID = reader.GetInt32(reader.GetOrdinal("MarkerID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TagType")))
                                    objTag.TagType = reader.GetInt32(reader.GetOrdinal("TagType"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimerAdjust")))
                                    timerAdjust = reader.GetInt32(reader.GetOrdinal("TimerAdjust"));

                                if (!reader.IsDBNull(reader.GetOrdinal("HGroupOrder")))
                                    newHOrder = reader.GetString(reader.GetOrdinal("HGroupOrder"));

                                
                                return objTag;
                            }
                            
                            return null;
                        }
                        else
                        {
                            return null;
                        }
                    }

                }
              
            }
            catch (Exception ex)
            {
                //
               // //logger.ErrorException("Exception thrown while opening transaciton in DB and returning TagType for "+TagRFID+" tag", ex);
                if (DatabaseFacade.tmrCheckFailover.Enabled)
                {
                    DatabaseFacade.tmrCheckFailover.Stop();
                    DatabaseFacade.CheckSQLDBFailover();
                    DatabaseFacade.tmrCheckFailover.Start();

                }
                throw;
            }
        }

        //public static List<IActiveReads> GetRecordsByTagIDs(string strTagIDs,Location loc)
        //{

        //    try
        //    {
        //        using (SqlCommand command = new SqlCommand())
        //        {
        //            List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

        //            command.CommandType = CommandType.StoredProcedure;
        //            command.CommandText = "ActiveReads_GetRecordsByTagIDs";

        //            command.Parameters.Add("TagIDs", SqlDbType.VarChar);
        //            command.Parameters["TagIDs"].Value = strTagIDs;

        //            command.Parameters.Add("Location", SqlDbType.Int);
        //            command.Parameters["Location"].Value = (Int32)loc;

        //            ActiveReads objActiveRead;

        //            using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
        //            {
        //                if (reader != null && reader.HasRows)
        //                {
        //                    while (reader.Read())
        //                    {
        //                        objActiveRead = new ActiveReads();

        //                        if (!reader.IsDBNull(reader.GetOrdinal("ID")))
        //                            objActiveRead.ID = reader.GetInt32(reader.GetOrdinal("ID"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("TagID")))
        //                            objActiveRead.TagID = reader.GetInt32(reader.GetOrdinal("TagID"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
        //                            objActiveRead.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("REDS")))
        //                            objActiveRead.REDS = reader.GetDateTime(reader.GetOrdinal("REDS"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("Queue")))
        //                            objActiveRead.Queue = reader.GetDateTime(reader.GetOrdinal("Queue"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("Entry1")))
        //                            objActiveRead.Entry1 = reader.GetDateTime(reader.GetOrdinal("Entry1"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("Entry2")))
        //                            objActiveRead.Entry2 = reader.GetDateTime(reader.GetOrdinal("Entry2"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("HopperTime")))
        //                            objActiveRead.HopperTime = reader.GetDateTime(reader.GetOrdinal("HopperTime"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("TruckWash")))
        //                            objActiveRead.TruckWash = reader.GetDateTime(reader.GetOrdinal("TruckWash"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("REDS2")))
        //                            objActiveRead.REDS2 = reader.GetDateTime(reader.GetOrdinal("REDS2"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("TruckCallUp")))
        //                            objActiveRead.TruckCallUp = reader.GetDateTime(reader.GetOrdinal("TruckCallUp"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("LastPosition")))
        //                            objActiveRead.LastPosition = reader.GetInt32(reader.GetOrdinal("LastPosition"));

        //                        if (!reader.IsDBNull(reader.GetOrdinal("EntryORQueue")))
        //                            objActiveRead.EntryORQueue = reader.GetInt32(reader.GetOrdinal("EntryORQueue"));

        //                        objActiveReads_Lst.Add(objActiveRead);
        //                    }

        //                    return objActiveReads_Lst;
        //                }
        //                else
        //                {
        //                    return null;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //}

        public static DataSet GetRecordsByTagIDs(string strTagIDs, Location loc)
        {

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetRecordsByTagIDs";

                    command.Parameters.Add("TagIDs", SqlDbType.VarChar);
                    command.Parameters["TagIDs"].Value = strTagIDs;
                  

                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = (Int32)loc;

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// To Get all the records for current traffic page
        /// </summary>
        /// <returns></returns>
        public static DataSet GetAllRecordsForTrafficPage()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetAllForTrafficPage";

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// To Get the Closed Tx for current traffic page
        /// </summary>
        /// <returns></returns>
        public static DataSet GetClsedTxForTrafficPage()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetClosedTransactions";

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static void AssignNewPad(string tagRFID, DateTime readTime, Location loc, int newPad, DateTime newPadTime)
        {
            try
            {

                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "AssignNewPad";


                    com.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    com.Parameters["TagRFID"].Value = tagRFID;
                     

                    com.Parameters.Add("ReadTime", SqlDbType.DateTime);
                    com.Parameters["ReadTime"].Value = readTime;

                    com.Parameters.Add("Location", SqlDbType.Int);
                    com.Parameters["Location"].Value = (Int32)loc;
                    
                    com.Parameters.Add("NewPadID", SqlDbType.Int);
                    com.Parameters["NewPadID"].Value = newPad;

                    com.Parameters.Add("NewPadTime", SqlDbType.DateTime);
                    com.Parameters["NewPadTime"].Value = newPadTime;


                    DatabaseFacade.SQLDB.ExecuteNonQuery(com);
                                       

                }


            }
            catch(Exception ex)
            {
            }
        }

        public static void GetNextFreePad(string tagID,DateTime startTime,Location loc,out int padId ,out DateTime padTime)
        {
            padId = 0;
            padTime = DateTime.MaxValue;
            try
            {
                 
                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "GetFirstFreePad";
                    
                    SqlParameter pTagID = new SqlParameter("TagRFID", SqlDbType.VarChar);
                    pTagID.Value = tagID;
                    pTagID.Direction = ParameterDirection.Input;
                    
                    com.Parameters.Add(pTagID);
                    SqlParameter pStartTime = new SqlParameter("ReadTime", SqlDbType.DateTime);
                    pStartTime.Value = startTime;
                    pStartTime.Direction = ParameterDirection.Input;
                    com.Parameters.Add(pStartTime);
                    SqlParameter pLocatioin = new SqlParameter("Location", SqlDbType.Int);
                    pLocatioin.Value = (int)loc;
                    pLocatioin.Direction = ParameterDirection.Input;
                    com.Parameters.Add(pLocatioin);
                    SqlParameter pPadID = new SqlParameter("ResultPadID", SqlDbType.Int);
                    pPadID.Direction = ParameterDirection.Output;
                    com.Parameters.Add(pPadID);
                    SqlParameter pPadTime = new SqlParameter("ResultPadTime", SqlDbType.DateTime);
                    pPadTime.Direction = ParameterDirection.Output;
                    com.Parameters.Add(pPadTime);

                    IDataReader reader = DatabaseFacade.SQLDB.ExecuteReader(com);
                    reader.Close();
                    //padTime = Convert.ToDateTime(com.Parameters["ResultPadTime"].Value);
                    DateTime.TryParse(com.Parameters["ResultPadTime"].Value.ToString(),out padTime);
                    padId = Convert.ToInt32(com.Parameters["ResultPadID"].Value);
                    
                }
               
            }
            catch (Exception ex)
            {
            }

        }
        public static void ReassignAllPad(int GroupID,string skipTag)
        {
            try
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandText = "ReassignAllPad";
                    
                    comm.Parameters.Add("GroupID", SqlDbType.Int);
                    comm.Parameters["GroupID"].Value = GroupID;

                    comm.Parameters.Add("SkipTagID", SqlDbType.VarChar,50 );
                    comm.Parameters["SkipTagID"].Value = skipTag;

                    DatabaseFacade.SQLDB.ExecuteNonQuery(comm);
                    

                }
            }
            catch (Exception ex)
            {
                
            }

        }

        public static Dictionary<string ,int> NextTruckToCallup(string skipTagRFID,int HopperID,out string CallupTagRFID,out int TimeToWait)
        {
            CallupTagRFID = "";
            TimeToWait = 0;
            //Stored re-assigned tag
            Dictionary<string, int> taglist = new Dictionary<string, int>();

            try
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandText = "NextTruckToCallup";

                    comm.Parameters.Add("SkipTagRFID", SqlDbType.VarChar,50);
                    comm.Parameters["SkipTagRFID"].Value = skipTagRFID;
                   
                    
                    comm.Parameters.Add("HopperID", SqlDbType.Int);
                    comm.Parameters["HopperID"].Value = HopperID;

                    comm.Parameters.Add("CallupTagRFID", SqlDbType.VarChar,50);
                    comm.Parameters["CallupTagRFID"].Direction = ParameterDirection.Output;

                    comm.Parameters.Add("TimeToWait", SqlDbType.Int);
                    comm.Parameters["TimeToWait"].Direction = ParameterDirection.Output;

                                          
                   using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(comm))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("TagRFID")) && !reader.IsDBNull(reader.GetOrdinal("NewDesPad")))
                                    if(!taglist.ContainsKey(reader.GetString(reader.GetOrdinal("TagRFID"))))
                                        taglist.Add(reader.GetString(reader.GetOrdinal("TagRFID")), reader.GetInt32(reader.GetOrdinal("NewDesPad")));
                                
                            }
                        }

                        reader.Close();
                        CallupTagRFID = Convert.ToString(comm.Parameters["CallupTagRFID"].Value);
                        TimeToWait = Convert.ToInt32(comm.Parameters["TimeToWait"].Value);
                    }
                    
                }
            }
            catch(Exception ex)
            {
                
                return null;
            }

            return taglist;
        }
        public static void GetNextCallupTime(int HopperID, out int callTimer)
        {
            callTimer = 5;

            try
            {
                using (SqlCommand comm = new SqlCommand())
                {

                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandText = "GetNextCallupTime";

                    comm.Parameters.Add("HopperID", SqlDbType.Int);
                    comm.Parameters["HopperID"].Value = HopperID;

                    comm.Parameters.Add("CallupTime", SqlDbType.Int);
                    comm.Parameters["CallupTime"].Direction = ParameterDirection.Output;

                    IDataReader reader = DatabaseFacade.SQLDB.ExecuteReader(comm);
                    reader.Close();

                    callTimer = Convert.ToInt32(comm.Parameters["CallupTime"].Value);
                }
            }
            catch (Exception ex)
            {
                
            }

        }
        public static void EntryOrQueue(string tagRFID, DateTime readTime, Location loc, out int desPadId, out Location desLoc)
        {
            try
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.CommandText = "EntryOrQueue";

                    comm.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    comm.Parameters["TagRFID"].Value = tagRFID;
                    

                    comm.Parameters.Add("ReadTime", SqlDbType.DateTime);
                    comm.Parameters["ReadTime"].Value = readTime;

                    comm.Parameters.Add("Location", SqlDbType.Int);
                    comm.Parameters["Location"].Value = loc;

                    comm.Parameters.Add("DesPadID", SqlDbType.Int);
                    comm.Parameters["DesPadID"].Direction = ParameterDirection.Output;

                    comm.Parameters.Add("DesLocation", SqlDbType.Int);
                    comm.Parameters["DesLocation"].Direction = ParameterDirection.Output;

                    

                    IDataReader reader = DatabaseFacade.SQLDB.ExecuteReader(comm);
                    reader.Close();

                    desPadId = Convert.ToInt32(comm.Parameters["DesPadID"].Value); 
                    desLoc = (Location) Convert.ToInt32(comm.Parameters["DesLocation"].Value);
                    
                }
            }
            catch (Exception ex)
            {
                desLoc = loc;
                desPadId = 0;
            }

        }//end of entryor queue
        
        /*
         * Date:4-11-2011
         * Modified By: Bhavesh
         * Detail: Add parameters use to update TimeTable (for Manual Tarping and Primary-Secondary Pad change)
         */ 
        public static void UpdateEntryORQueue(int TagID, string TagRFID, Location EntryORQueue,int newPad,DateTime expectedArrival,
            ref VMSParameters vmsParam, ref DataTable dtVMSSettings)
        {


            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IActiveReads> objActiveReads_Lst = new List<IActiveReads>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_UpdateEntryORQueue";

                    command.Parameters.Add("TagID", SqlDbType.Int);
                    command.Parameters["TagID"].Value = TagID;

                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    

                    command.Parameters.Add("EntryORQueue", SqlDbType.Int);
                    command.Parameters["EntryORQueue"].Value = EntryORQueue;

                    command.Parameters.Add("NewPad", SqlDbType.Int);
                    command.Parameters["NewPad"].Value = newPad; 

                    command.Parameters.Add("ExpectedArrival", SqlDbType.DateTime);
                   command.Parameters["ExpectedArrival"].Value = expectedArrival;// expected arrival time at new (assigned) pad

                    command.Parameters.Add("VMSLogging", SqlDbType.Int);
                    command.Parameters["VMSLogging"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSUseRandomPortNo", SqlDbType.Int);
                    command.Parameters["VMSUseRandomPortNo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSMinPortNo", SqlDbType.Int);
                    command.Parameters["VMSMinPortNo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("VMSMaxPortNo", SqlDbType.Int);
                    command.Parameters["VMSMaxPortNo"].Direction = ParameterDirection.Output;

                   // DatabaseFacade.SQLDB.ExecuteNonQuery(command);

                    DataSet ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        dtVMSSettings = ds.Tables[0];
                    }

                    object result = command.Parameters["VMSLogging"].Value;
                    if (result != null)
                    {
                        vmsParam.VMSLogging = Convert.ToInt32(result);
                    }

                    result = command.Parameters["VMSUseRandomPortNo"].Value;
                    if (result != null)
                    {
                        vmsParam.UseRandomPort = Convert.ToBoolean(result);
                    }

                    result = command.Parameters["VMSMinPortNo"].Value;
                    if (result != null)
                    {
                        vmsParam.MinPortNo = Convert.ToInt16(result);
                    }

                    result = command.Parameters["VMSMaxPortNo"].Value;
                    if (result != null)
                    {
                        vmsParam.MaxPortNo = Convert.ToInt16(result);
                    }


                }
            }
            catch (Exception ex)
            {
                // throw;
            }

        }


        public static DataTable GetActiveTransactions(int forAllDays)
        {
            DataTable dtActiveReads = null;
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = null;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ActiveReads_GetActiveTransactions";

                    command.Parameters.Add("option", typeof(System.Int32));
                    command.Parameters["option"].Value = forAllDays;

                    ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    if (ds != null && ds.Tables[0] != null)
                    {
                        dtActiveReads = ds.Tables[0];
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
            return dtActiveReads;
        }



        #endregion

    }

}
