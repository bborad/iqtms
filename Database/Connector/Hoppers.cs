using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;

using NLog;

namespace Ramp.Database.Connector
{
    public class Hoppers : IHoppers
    {
        #region "private variables"

        public int ID { get; set; }
        public string HopperName { get; set; }
        public int MarkerID { get; set; }
        public Int32 TimeFromEnt2 { get; set; }
        public Int32 TimeToTW { get; set; }

        #endregion




        #region "Public Methods"
        /// <summary>
        /// Assign Hoppers and Proponents
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public int AssignHoppersProponents(int ProponentID, int HopperID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_AssignHoppersProponents";
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    return (Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command)));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion




        #region "Public Static Methods"

        public static IHoppers GetHopperDetailsByHopperID(int HopperID)
        {

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetHopperDetailsByHopperID";
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    Hoppers objHopper;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                objHopper = new Hoppers();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objHopper.ID = reader.GetInt32(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("HopperName")))
                                    objHopper.HopperName = reader.GetString(reader.GetOrdinal("HopperName"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerID")))
                                    objHopper.MarkerID = reader.GetInt32(reader.GetOrdinal("MarkerID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeFromEnt2")))
                                    objHopper.TimeFromEnt2 = reader.GetInt32(reader.GetOrdinal("TimeFromEnt2"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeToTW")))
                                    objHopper.TimeToTW = reader.GetInt32(reader.GetOrdinal("TimeToTW"));

                                return objHopper;
                            }

                            return null;

                        }

                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetPriority(string Lane1_TagRFID, string Lane2_TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetPriority";

                    command.Parameters.Add("Lane1_TagRFID", SqlDbType.VarChar);
                    command.Parameters["Lane1_TagRFID"].Value = Lane1_TagRFID;
                    

                    command.Parameters.Add("Lane2_TagRFID", SqlDbType.VarChar);
                    command.Parameters["Lane2_TagRFID"].Value = Lane2_TagRFID;
                    

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                string PriorityTagRFID = String.Empty;

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    PriorityTagRFID = reader.GetString(reader.GetOrdinal("PriorityTagRFID"));

                                return PriorityTagRFID;
                            }

                            return null;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// Get all Hoppers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllHoppers()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetAllHoppers";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get PAD Status
        /// </summary>
        /// <returns></returns>
        public static DataSet GetHoppers()
        {
            DataSet dsResult = null;
            try
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetHoppers";
                    dsResult = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    return dsResult;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Get Proponent by Hopper ID
        /// </summary>
        /// <param name="HopperID"></param>
        /// <returns></returns>
        public static DataSet GetProponentsByHopperID(int HopperID)
        {

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetProponentsByHopperID";
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// Validate that the given proponent id is already associated with the other proponent or not
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataTable CheckProponentIsAlreadyAssignedToOtherHopper(int ProponentID, int HopperID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_CheckProponentIsAlreadyAssignedToOtherHopper";
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Get Hoppers Expected Time values
        /// </summary>
        /// <param name="HopperID"></param>
        /// <returns></returns>
        public static DataSet GetDetailsForExpectedTimes()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_GetDetailsForExpectedTimes";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TagRFID"></param>
        /// <param name="ProponentID"></param>
        /// <param name="HopperID"></param>
        /// <param name="Assigned"></param>
        /// <param name="TruckID"></param>
        public void UpdateExpectedTimeEntry2(string ExpectedTimeID, string TimeFromEnt2, string TimeToTW, int MarkerID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_UpdateExpectedTimeEntry2";
                    ///ADD Parameters
                    ///   
                    command.Parameters.Add("ID", SqlDbType.Int);

                    if (ExpectedTimeID != string.Empty)
                    {
                        command.Parameters["ID"].Value = ExpectedTimeID;
                    }
                    else
                    {
                        command.Parameters["ID"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TimeFromEnt2", SqlDbType.Int);
                    if (TimeFromEnt2 != string.Empty)
                    {
                        command.Parameters["TimeFromEnt2"].Value = TimeFromEnt2;
                    }
                    else
                    {
                        command.Parameters["TimeFromEnt2"].Value = DBNull.Value;
                    }
                    command.Parameters.Add("TimeToTW", SqlDbType.Int);
                    if (TimeFromEnt2 != string.Empty)
                    {
                        command.Parameters["TimeToTW"].Value = TimeToTW;
                    }
                    else
                    {
                        command.Parameters["TimeToTW"].Value = DBNull.Value;
                    }

                    command.Parameters.Add("MarkerID", SqlDbType.Int);
                    if (MarkerID != 0)
                    {
                        command.Parameters["MarkerID"].Value = MarkerID;
                    }
                    else
                    {
                        command.Parameters["MarkerID"].Value = DBNull.Value;
                    }

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TimeFromEnt2"></param>
        //public void UpdateExpectedTimeTW(string ID, string TimeToTW)
        //{
        //    try
        //    {
        //        using (SqlCommand command = new SqlCommand())
        //        {
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.CommandText = "Hoppers_UpdateExpectedTimeTW";
        //            ///ADD Parameters
        //            ///   
        //            command.Parameters.Add("ID", SqlDbType.Int);

        //            if (ID != string.Empty)
        //            {
        //                command.Parameters["ID"].Value = ID;
        //            }
        //            else
        //            {
        //                command.Parameters["ID"].Value = DBNull.Value;
        //            }
        //            command.Parameters.Add("TimeToTW", SqlDbType.Int);
        //            if (TimeToTW != string.Empty)
        //            {
        //                command.Parameters["TimeToTW"].Value = TimeToTW;
        //            }
        //            else
        //            {
        //                command.Parameters["TimeToTW"].Value = DBNull.Value;
        //            }
        //            DatabaseFacade.SQLDB.ExecuteNonQuery(command);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}


        /// <summary>
        /// Get all Hoppers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllProponents()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Proponents_GetAllProponents";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }


        public static void UpdateHoppersStatus(string dtHopperStatus)
        {
            

            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Hoppers_UpdateHopperStatus";

                    

                    command.Parameters.Add("HopperStatus", SqlDbType.VarChar, 100);
                    command.Parameters["HopperStatus"].Value = dtHopperStatus;

                      DatabaseFacade.SQLDB.ExecuteNonQuery(command);

                }
            }
            catch (Exception ex)
            {
                
            }

            
        }

        #endregion
    }
}
