/**************************************************************************************
* Author : Pratiksha Bandi
* Created Date : 27-July-2010, Wednesday
* Last Modified by : 
* Last Modified : 
* Module Name : Readers
* Decription :  This call will have all common functions/methods which will use in Readers. For example: Get Readers detail.
* **************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.Database.Connector
{
    public class Readers : IReaders
    {
	
        #region "private variables"
        public int ID { get; set; }
        public string ReaderSerialNo { get; set; }
        public string ReaderName { get; set; }
        public string IPAddress { get; set; }
        public int PortNo { get; set; }
        public int Location { get; set; }
        public int Power { get; set; }
        public string ReaderStatus { get; set; }

        #endregion

        #region "Public Methods"


        /// <summary>
        /// Insert reader
        /// </summary>
        /// <param name="ReaderSerialNo"></param>
        /// <param name="ReaderName"></param>
        /// <param name="IPAddress"></param>
        /// <param name="PortNo"></param>
        /// <param name="MarkerIDs"></param>
        //public int InsertReader(string ReaderSerialNo, string ReaderName, int IPAddress, int PortNo, string MarkerIDs, int Locaton)
        public int InsertReader(string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_InsertReader";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("ReaderSerialNo", SqlDbType.NVarChar);
                    command.Parameters["ReaderSerialNo"].Value = ReaderSerialNo;
                    command.Parameters.Add("ReaderName", SqlDbType.NVarChar);
                    command.Parameters["ReaderName"].Value = ReaderName;
                    command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    command.Parameters["IPAddress"].Value = IPAddress;
                    command.Parameters.Add("PortNo", SqlDbType.Int);
                    command.Parameters["PortNo"].Value = PortNo;
                    command.Parameters.Add("MarkerIDs", SqlDbType.NVarChar);
                    command.Parameters["MarkerIDs"].Value = MarkerIDs;
                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = Location;
                    command.Parameters.Add("Power", SqlDbType.Int);
                    command.Parameters["Power"].Value = Power;
                    int ReaderID = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return ReaderID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }


        /// <summary>
        ///  Update reader detail by Reader ID 
        /// </summary>
        /// <param name="ReaderID"></param>
        /// <param name="ReaderName"></param>
        /// <param name="IPAddress"></param>
        /// <param name="PortNo"></param>
        /// <param name="MarkerIDs"></param>
        /// <returns></returns>
        //public int UpdateReader(string ReaderSerialNo, string ReaderName, int IPAddress, int PortNo, string MarkerIDs, int Locaton)
        public int UpdateReader(int ReaderID, string ReaderSerialNo, string ReaderName, string IPAddress, int PortNo, string MarkerIDs, int Location, int Power)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_UpdateReader";
                    ///ADD Parameters
                    /// 
                    command.Parameters.Add("ReaderID", SqlDbType.Int);
                    command.Parameters["ReaderID"].Value = ReaderID;
                    command.Parameters.Add("ReaderSerialNo", SqlDbType.NVarChar);
                    command.Parameters["ReaderSerialNo"].Value = ReaderSerialNo;
                    command.Parameters.Add("ReaderName", SqlDbType.NVarChar);
                    command.Parameters["ReaderName"].Value = ReaderName;
                    command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    command.Parameters["IPAddress"].Value = IPAddress;
                    command.Parameters.Add("PortNo", SqlDbType.Int);
                    command.Parameters["PortNo"].Value = PortNo;
                    command.Parameters.Add("MarkerIDs", SqlDbType.NVarChar);
                    command.Parameters["MarkerIDs"].Value = MarkerIDs;
                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = Location;
                    command.Parameters.Add("Power", SqlDbType.Int);
                    command.Parameters["Power"].Value = Power;
                    int OutReaderID = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return OutReaderID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Delete Reader By ID
        /// </summary>
        /// <param name="ID"></param>
        public void DeleteReaderByID(int ID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_DeleteReaderByID";
                    ///ADD Parameters
                    /// 
                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;

                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }


        #endregion

        #region "Public Static Methods"
        /// <summary>
        /// Get All Readers
        /// </summary>
        /// <returns></returns>
        public static List<IReaders> GetAllReaders()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_GetAllReaders";
                    List<IReaders> lstReaders = new List<IReaders>();
                    /// Execute query
                    /// 
                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            IReaders objReader;
                            /// move to first record
                            /// 
                            while (reader.Read())
                            {
                                objReader = new Readers();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objReader.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ReaderSerialNo")))
                                    objReader.ReaderSerialNo = reader.GetString(reader.GetOrdinal("ReaderSerialNo"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ReaderName")))
                                    objReader.ReaderName = reader.GetString(reader.GetOrdinal("ReaderName"));
                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objReader.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objReader.Location = reader.GetInt32(reader.GetOrdinal("Location"));
                                if (!reader.IsDBNull(reader.GetOrdinal("PortNo")))
                                    objReader.PortNo = reader.GetInt32(reader.GetOrdinal("PortNo"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Power")))
                                    objReader.Power = reader.GetInt32(reader.GetOrdinal("Power"));

                                lstReaders.Add(objReader);
                            }
                            return lstReaders;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public static List<IReaders> GetReadersAtLocation(Location loc)
        {
            try
            {
                // check in the active reads table whether last position of the TagRFID is loc 
                using (SqlCommand command = new SqlCommand())
                {
                    List<IReaders> lstReaders = null;

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_GetReadersAtLocation";

                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = Convert.ToInt32(loc);

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            lstReaders = new List<IReaders>();
                            IReaders objReader;
                            /// move to first record
                            /// 
                            while (reader.Read())
                            {
                                objReader = new Readers();
                                objReader.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                objReader.ReaderSerialNo = reader.GetString(reader.GetOrdinal("ReaderSerialNo"));
                                objReader.ReaderName = reader.GetString(reader.GetOrdinal("ReaderName"));
                                objReader.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));
                                objReader.PortNo = reader.GetInt32(reader.GetOrdinal("PortNo"));
                                objReader.Location = reader.GetInt32(reader.GetOrdinal("Location"));
                                lstReaders.Add(objReader);
                            }
                        }
                    }
                    return lstReaders;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Get Reader Detail By ReaderID 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static IReaders GetReaderDetailsByID(int ID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_GetReaderDetailByID";
                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;
                    IReaders objReader;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                objReader = new Readers();
                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objReader.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ReaderSerialNo")))
                                    objReader.ReaderSerialNo = reader.GetString(reader.GetOrdinal("ReaderSerialNo"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ReaderName")))
                                    objReader.ReaderName = reader.GetString(reader.GetOrdinal("ReaderName"));
                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objReader.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));
                                if (!reader.IsDBNull(reader.GetOrdinal("PortNo")))
                                    objReader.PortNo = reader.GetInt32(reader.GetOrdinal("PortNo"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objReader.Location = reader.GetInt32(reader.GetOrdinal("Location"));
                                return objReader;
                            }
                            return null;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Reader and Its Markers Detail By ID
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataSet ReaderAndItsMarkersDetailByID(int ReaderID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_ReaderAndItsMarkersDetailByID";
                    command.Parameters.Add("ReaderID", SqlDbType.Int);
                    command.Parameters["ReaderID"].Value = ReaderID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get All Readers With Its Marker
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllReadersWithItsMarker()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_GetAllReadersWithItsMarker";
                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Check IP Address already exists for reader
        /// </summary>
        /// <returns></returns>
        public static int CheckIPAddressAlreadyExists(int ReaderID, string IPAddress)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_CheckIPAddressAlreadyExists";
                    command.Parameters.Add("ReaderID", SqlDbType.Int);
                    command.Parameters["ReaderID"].Value = ReaderID;
                    command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    command.Parameters["IPAddress"].Value = IPAddress;

                    return Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// To Get Reader status for system status report
        /// </summary>        
        /// <returns></returns>
        public static DataSet GetReaderStatusForSystemReport()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_GetReaderStatusForSystemReport";

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static DataTable UpdateStatus(string IpAddress,string Status)
        {
            DataTable dtResult = new DataTable();
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Readers_UpdateStatus";

                    command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    command.Parameters["IPAddress"].Value = IpAddress;
                   
                    command.Parameters.Add("ReaderStatus", SqlDbType.VarChar );
                    command.Parameters["ReaderStatus"].Value = Status;

                    DataSet ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        dtResult = ds.Tables[0];
                    }

                    return dtResult;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion
    }
}
