/**************************************************************************************
* Author : Pratiksha Bandi
* Created Date : 27-July-2010, Wednesday
* Last Modified by : 
* Last Modified : 
* Module Name : Tags
* Decription :  This call will have all common functions/methods which will use in Tags. For example: Get/Set Tags detail.
* **************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using Ramp.MiddlewareController.Common;
using System.Data.SqlClient;
using System.Data;
using Ramp.DataAccess.Data;

using NLog;

namespace Ramp.Database.Connector
{
    public class Tags : ITags
    {
        Logger log = LogManager.GetCurrentClassLogger();

        #region "private variables"
        public int ID { get; set; }
        public string TagRFID { get; set; }
        public int ProponentID { get; set; }
        public int HopperID { get; set; }
        //public int TruckID { get; set; }
        public string TruckID { get; set; }
        public bool Assigned { get; set; }
        public int TagType { get; set; }
        public int DestinationPad { get; set; }
         
        public DateTime LastReadTime { get; set; }

        public IHoppers AssignedHopper { get; set; }

        #endregion

        #region "Public Methods"

        public Tags()
        {
        }

        /// <summary>
        /// Insert Tag details 
        /// </summary>
        /// <param name="TagRFID"></param>
        /// <param name="ProponentID"></param>
        /// <param name="HopperID"></param>
        /// <param name="Assigned"></param>
        /// <param name="TruckID"></param>
        public void InsertTagDetails(string TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tag_InsertTagDetails";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;
                    command.Parameters.Add("Assigned", SqlDbType.Bit);
                    command.Parameters["Assigned"].Value = Assigned;
                    command.Parameters.Add("TruckID", SqlDbType.VarChar);
                    command.Parameters["TruckID"].Value = TruckID;
                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Insert bluk Tagids using CSV string 
        /// </summary>
        /// <param name="strTagRFIDs"></param>
        public int InsertTags(string TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_InsertTags";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;

                    int countaddedrows = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return countaddedrows;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Check if 
        /// </summary>
        /// <param name="TagRFID"></param>
        /// <returns></returns>
        public DataTable CheckTagAlreadyAssigned(string TagRFID, int ProponentID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tag_CheckTagAlreadyAssigned";
                    ///ADD Parameters
                    ///   
                    command.Parameters.Add("TagRFID", SqlDbType.NVarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Update Tag details 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TagRFID"></param>
        /// <param name="ProponentID"></param>
        /// <param name="HopperID"></param>
        /// <param name="Assigned"></param>
        /// <param name="TruckID"></param>
        public void UpdateTagDetails(int ID, int TagRFID, int ProponentID, int HopperID, bool Assigned, string TruckID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tag_UpdateTagDetails";
                    ///ADD Parameters
                    ///   
                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;
                    command.Parameters.Add("TagRFID", SqlDbType.Int);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;
                    command.Parameters.Add("Assigned", SqlDbType.Bit);
                    command.Parameters["Assigned"].Value = Assigned;
                    command.Parameters.Add("TruckID", SqlDbType.VarChar);
                    command.Parameters["TruckID"].Value = TruckID;
                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Dispatch Tags By TagsID(TagIDs are in CSV)
        /// </summary>
        /// <param name="IssuedTagsID"></param>
        /// <param name="TruckRegNo"></param>
        public int DispatchTagsByTagsID(string IssuedTagsID, string TruckRegNo, string TagBattery)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "IssuedTags_DispatchTagsByTagsID";
                    ///ADD Parameters
                    ///   
                    command.Parameters.Add("IssuedTagsID", SqlDbType.NVarChar);
                    command.Parameters["IssuedTagsID"].Value = IssuedTagsID;
                    command.Parameters.Add("TruckRegNo", SqlDbType.NVarChar);
                    command.Parameters["TruckRegNo"].Value = TruckRegNo;
                    command.Parameters.Add("TagBattery", SqlDbType.NVarChar);
                    command.Parameters["TagBattery"].Value = TagBattery;
                    int countaddedrows = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return countaddedrows;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Assign Tags to Hopper and Proponent
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TagRFID"></param>
        /// <param name="ProponentID"></param>
        /// <param name="HopperID"></param>
        /// <param name="Assigned"></param>
        /// <param name="TruckID"></param>
        public int AssignTags(string TagRFID, string TruckID, int HopperID, int ProponentID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_AssignTags";
                    ///ADD Parameters
                    ///   
                    command.Parameters.Add("TagRFID", SqlDbType.NVarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    command.Parameters.Add("TruckID", SqlDbType.NVarChar);
                    command.Parameters["TruckID"].Value = TruckID;
                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;
                    return (Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command)));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Unassign selected Tags List
        /// </summary>
        /// <param name="TagRFID"></param>
        /// <returns></returns>
        public int UnAssignTags(string TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_UnAssignTags";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("TagRFID", SqlDbType.NVarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;

                    int countaddedrows = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return countaddedrows;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }


        /// <summary>
        /// Unassign selected Tags List
        /// </summary>
        /// <param name="TagRFID"></param>
        /// <returns></returns>
        public int RemoveTags(string TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_RemoveTags";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("TagRFID", SqlDbType.NVarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;

                    int countaddedrows = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return countaddedrows;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion

        #region "Public Static Methods"

        /// <summary>
        /// Get All Tags
        /// </summary>
        /// <returns></returns>
        public static List<ITags> GetAllTags()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tag_GetAllTags";
                    List<ITags> lstTags = new List<ITags>();
                    /// Execute query
                    /// 
                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            Tags objTag;
                            /// move to first record
                            /// 
                            while (reader.Read())
                            {
                                objTag = new Tags();
                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objTag.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("TagRFID")))
                                    objTag.TagRFID = reader.GetString(reader.GetOrdinal("TagRFID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ProponentID")))
                                    objTag.ProponentID = reader.GetInt32(reader.GetOrdinal("ProponentID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
                                    objTag.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Assigned")))
                                    objTag.Assigned = reader.GetBoolean(reader.GetOrdinal("Assigned"));
                                if (!reader.IsDBNull(reader.GetOrdinal("TruckID")))
                                    objTag.TruckID = reader.GetString(reader.GetOrdinal("TruckID")); //reader.GetInt32(reader.GetOrdinal("TruckID"));
                                lstTags.Add(objTag);
                            }
                            return lstTags;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Tag Details By TagID (Single Tag)
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static ITags GetTagDetailsByTagID(string TagRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tag_GetTagDetailsByTagID";
                    command.Parameters.Add("TagRFID", SqlDbType.VarChar);
                    command.Parameters["TagRFID"].Value = TagRFID;
                    ITags objTag;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                objTag = new Tags();
                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objTag.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("TagRFID")))
                                    objTag.TagRFID = reader.GetString(reader.GetOrdinal("TagRFID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ProponentID")))
                                    objTag.ProponentID = reader.GetInt32(reader.GetOrdinal("ProponentID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
                                    objTag.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Assigned")))
                                    objTag.Assigned = reader.GetBoolean(reader.GetOrdinal("Assigned"));
                                if (!reader.IsDBNull(reader.GetOrdinal("TruckID")))
                                    objTag.TruckID = reader.GetString(reader.GetOrdinal("TruckID")); //reader.GetInt32(reader.GetOrdinal("TruckID"));

                                objTag.AssignedHopper = new Hoppers();

                                if (!reader.IsDBNull(reader.GetOrdinal("HopperName")))
                                    objTag.AssignedHopper.HopperName = reader.GetString(reader.GetOrdinal("HopperName"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeFromEnt2")))
                                    objTag.AssignedHopper.TimeFromEnt2 = reader.GetInt32(reader.GetOrdinal("TimeFromEnt2"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TimeToTW")))
                                    objTag.AssignedHopper.TimeToTW = reader.GetInt32(reader.GetOrdinal("TimeToTW"));

                                return objTag;
                            }
                            return null;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Tag Details By TagsID (Parameter TagsRFID is passed as CSV string)
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataTable GetTagsDetailsByTagsID(string TagsRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_GetTagsDetailsByTagsID";
                    command.Parameters.Add("TagsRFID", SqlDbType.NVarChar);
                    command.Parameters["TagsRFID"].Value = TagsRFID;
                    //Tags objTag;

                    //using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    //{
                    //    if (reader != null && reader.HasRows)
                    //    {
                    //        if (reader.Read())
                    //        {
                    //            objTag = new Tags();
                    //            if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                    //                objTag.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("TagRFID")))
                    //                objTag.TagRFID = reader.GetString(reader.GetOrdinal("TagRFID"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("ProponentID")))
                    //                objTag.ProponentID = reader.GetInt32(reader.GetOrdinal("ProponentID"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("ProponentName")))
                    //                objTag.ProponentID = reader.GetString(reader.GetOrdinal("ProponentName"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("HopperID")))
                    //                objTag.HopperID = reader.GetInt32(reader.GetOrdinal("HopperID"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("HopperName")))
                    //                objTag.HopperID = reader.GetString(reader.GetOrdinal("HopperName"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("Assigned")))
                    //                objTag.Assigned = reader.GetBoolean(reader.GetOrdinal("Assigned"));
                    //            if (!reader.IsDBNull(reader.GetOrdinal("TruckID")))
                    //                objTag.TruckID = reader.GetInt32(reader.GetOrdinal("TruckID"));
                    //            return objTag;
                    //        }
                    //        return null;
                    //    }
                    //    else
                    //        return null;
                    //}

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Give dataset with two DataTables 
        /// Get all assigned tags by
        ///	1. Different Proponent ID
        /// 2. Selected Proponent ID
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <param name="ProponentID"></param>
        /// <returns></returns>
        public static DataSet GetAllAssignedTagsByProponentID(string TagsRFID, int ProponentID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_GetAllAssignedTagsByProponentID";
                    command.Parameters.Add("TagsRFID", SqlDbType.NVarChar);
                    command.Parameters["TagsRFID"].Value = TagsRFID;
                    command.Parameters.Add("ProponentID", SqlDbType.Int);
                    command.Parameters["ProponentID"].Value = ProponentID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Tag Details for Unassign By TagsID (Parameter TagsRFID is passed as CSV string)
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataTable GetTagsDetailForUnAssign(string TagsRFID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_GetTagsDetailForUnAssign";
                    command.Parameters.Add("TagsRFID", SqlDbType.NVarChar);
                    command.Parameters["TagsRFID"].Value = TagsRFID;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public static DataTable GetDetailsByTruckIDs(string TruckIDs)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_GetDetailsByTruckIDs";
                    command.Parameters.Add("TruckIDs", SqlDbType.NVarChar);
                    command.Parameters["TruckIDs"].Value = TruckIDs;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Assign and Un-Assign Tag report
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataTable AssignUnAssignTagReport(string TagRFIDs, string HopperIDs, string ProponentIDs, string DateFrom, string DateTo)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_AssignUnAssignTagReport";
                    command.Parameters.Add("TagRFIDs", SqlDbType.NVarChar);
                    command.Parameters["TagRFIDs"].Value = TagRFIDs;
                    command.Parameters.Add("HopperIDs", SqlDbType.NVarChar);
                    command.Parameters["HopperIDs"].Value = HopperIDs;
                    command.Parameters.Add("ProponentIDs", SqlDbType.NVarChar);
                    command.Parameters["ProponentIDs"].Value = ProponentIDs;
                    command.Parameters.Add("DateFrom", SqlDbType.NVarChar);
                    command.Parameters["DateFrom"].Value = DateFrom;
                    command.Parameters.Add("DateTo", SqlDbType.NVarChar);
                    command.Parameters["DateTo"].Value = DateTo;

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Issue Tags report
        /// </summary>
        /// <param name="TagsRFID"></param>
        /// <returns></returns>
        public static DataTable IssueTagReport(string TagRFIDs, string HopperIDs, string ProponentIDs, string DateFrom, string DateTo,string ReferenceNo)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Tags_IssueTagReport";
                    command.Parameters.Add("TagRFIDs", SqlDbType.NVarChar);
                    command.Parameters["TagRFIDs"].Value = TagRFIDs;
                    command.Parameters.Add("HopperIDs", SqlDbType.NVarChar);
                    command.Parameters["HopperIDs"].Value = HopperIDs;
                    command.Parameters.Add("ProponentIDs", SqlDbType.NVarChar);
                    command.Parameters["ProponentIDs"].Value = ProponentIDs;
                    command.Parameters.Add("DateFrom", SqlDbType.NVarChar);
                    command.Parameters["DateFrom"].Value = DateFrom;
                    command.Parameters.Add("DateTo", SqlDbType.NVarChar);
                    command.Parameters["DateTo"].Value = DateTo;
                    command.Parameters.Add("ReferenceNo", SqlDbType.VarChar);

                    if (ReferenceNo.Trim().Length > 0)
                    {
                        command.Parameters["ReferenceNo"].Value = ReferenceNo;
                    }
                    else
                    {
                        command.Parameters["ReferenceNo"].Value = DBNull.Value;
                    }

                    return (DatabaseFacade.SQLDB.ExecuteDataSet(command).Tables[0]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        #endregion

    }

}
