using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;

using NLog;

namespace Ramp.Database.Connector
{

    public class VMSDevices : IVMSDevices
    {
		
        public short DeviceNo { get;  set; }
        public string IPAddress { get; set; }
        public bool SCL2008 { get; set; }
        public string Password { get; set; }
        public short UDPPort { get; set; }
        public short LedWidth { get; set; } // value 128
        public short LedHeight { get; set; } // value 32

        public int Location { get; set; }

        public int TextStartPosition { get; set; }
        public int TextLength { get; set; }
        public int TextColor { get; set; }
        public int ASCFont { get; set; }

        public int MessageTimeOut { get; set; }

        public string DefaultMessage { get; set; }


        public Int64 ID { get; set; }
        public string VMSStatus { get; set; }

        public static List<IVMSDevices> GetAllVMSDevices()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    List<IVMSDevices> lstVMSDevices = new List<IVMSDevices>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "VMSDevices_GetAll";

                    IVMSDevices objVMSDevice;
                    bool isSCL2008 = true;
                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(reader.GetOrdinal("IsSCL2008")))
                                    isSCL2008 = reader.GetBoolean(reader.GetOrdinal("IsSCL2008"));
                               //if(isSCL2008)
                                objVMSDevice = new VMSDevices();
                                 //else
                                  // objVMSDevice = new Aussport

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objVMSDevice.ID = reader.GetInt64(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objVMSDevice.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));

                                if (!reader.IsDBNull(reader.GetOrdinal("PortNo")))
                                    objVMSDevice.UDPPort = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("PortNo")));

                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objVMSDevice.Location = reader.GetInt32(reader.GetOrdinal("Location"));

                                if (!reader.IsDBNull(reader.GetOrdinal("VMSStatus")))
                                    objVMSDevice.VMSStatus = reader.GetString(reader.GetOrdinal("VMSStatus"));

                                if (!reader.IsDBNull(reader.GetOrdinal("LEDWidth")))
                                    objVMSDevice.LedWidth = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("LEDWidth")));
                                //else
                                //    objVMSDevice.LedWidth = 128; // Default Value

                                if (!reader.IsDBNull(reader.GetOrdinal("LEDHeight")))
                                    objVMSDevice.LedHeight = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("LEDHeight")));
                                //else
                                //    objVMSDevice.LedHeight = 32; // Default Value

                                objVMSDevice.SCL2008 = isSCL2008;
                                //else
                                //    objVMSDevice.SCL2008 = true; // Default Value

                                if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                                    objVMSDevice.Password = reader.GetString(reader.GetOrdinal("Password"));

                                if (!reader.IsDBNull(reader.GetOrdinal("DeviceNo")))
                                    objVMSDevice.DeviceNo = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("DeviceNo")));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextColor")))
                                    objVMSDevice.TextColor = reader.GetInt32(reader.GetOrdinal("TextColor"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextLength")))
                                    objVMSDevice.TextLength = reader.GetInt32(reader.GetOrdinal("TextLength"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextStartPosition")))
                                    objVMSDevice.TextStartPosition = reader.GetInt32(reader.GetOrdinal("TextStartPosition"));

                                if (!reader.IsDBNull(reader.GetOrdinal("ASCFont")))
                                    objVMSDevice.ASCFont = reader.GetInt32(reader.GetOrdinal("ASCFont"));

                                if (!reader.IsDBNull(reader.GetOrdinal("DefaultMessage")))
                                    objVMSDevice.DefaultMessage = reader.GetString(reader.GetOrdinal("DefaultMessage"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MessageTimeOut")))
                                    objVMSDevice.MessageTimeOut = reader.GetInt32(reader.GetOrdinal("MessageTimeOut"));
                               
                                lstVMSDevices.Add(objVMSDevice);
                            }

                            return lstVMSDevices;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void UpdateVMSStatus(string IPAddress, string VMSStatus)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "VMSDevices_UpdateVMSStatus";

                    command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    command.Parameters["IPAddress"].Value = IPAddress;

                    command.Parameters.Add("VMSStatus", SqlDbType.VarChar);
                    command.Parameters["VMSStatus"].Value = VMSStatus;
                   
                    DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        public static IVMSDevices GetVMSDevicesByLocation(Location loc)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                { 
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "VMSDevices_GetAllByLocation";

                    command.Parameters.Add(new SqlParameter("@LocationID", SqlDbType.Int) { Value = Convert.ToInt32(loc) });

                    VMSDevices objVMSDevice = null;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                objVMSDevice = new VMSDevices();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objVMSDevice.ID = reader.GetInt64(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objVMSDevice.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));

                                if (!reader.IsDBNull(reader.GetOrdinal("PortNo")))
                                    objVMSDevice.UDPPort = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("PortNo")));

                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objVMSDevice.Location = reader.GetInt32(reader.GetOrdinal("Location"));

                                if (!reader.IsDBNull(reader.GetOrdinal("VMSStatus")))
                                    objVMSDevice.VMSStatus = reader.GetString(reader.GetOrdinal("VMSStatus"));

                                if (!reader.IsDBNull(reader.GetOrdinal("LEDWidth")))
                                    objVMSDevice.LedWidth = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("LEDWidth")));
                                //else
                                //    objVMSDevice.LedWidth = 128; // Default Value

                                if (!reader.IsDBNull(reader.GetOrdinal("LEDHeight")))
                                    objVMSDevice.LedHeight = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("LEDHeight")));
                                //else
                                //    objVMSDevice.LedHeight = 32; // Default Value

                                if (!reader.IsDBNull(reader.GetOrdinal("IsSCL2008")))
                                    objVMSDevice.SCL2008 = reader.GetBoolean(reader.GetOrdinal("IsSCL2008"));
                                //else
                                //    objVMSDevice.SCL2008 = true; // Default Value

                                if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                                    objVMSDevice.Password = reader.GetString(reader.GetOrdinal("Password"));

                                if (!reader.IsDBNull(reader.GetOrdinal("DeviceNo")))
                                    objVMSDevice.DeviceNo = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("DeviceNo")));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextColor")))
                                    objVMSDevice.TextColor = reader.GetInt32(reader.GetOrdinal("TextColor"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextLength")))
                                    objVMSDevice.TextLength = reader.GetInt32(reader.GetOrdinal("TextLength"));

                                if (!reader.IsDBNull(reader.GetOrdinal("TextStartPosition")))
                                    objVMSDevice.TextStartPosition = reader.GetInt32(reader.GetOrdinal("TextStartPosition"));

                                if (!reader.IsDBNull(reader.GetOrdinal("ASCFont")))
                                    objVMSDevice.ASCFont = reader.GetInt32(reader.GetOrdinal("ASCFont"));

                                if (!reader.IsDBNull(reader.GetOrdinal("DefaultMessage")))
                                    objVMSDevice.DefaultMessage = reader.GetString(reader.GetOrdinal("DefaultMessage"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MessageTimeOut")))
                                    objVMSDevice.MessageTimeOut = reader.GetInt32(reader.GetOrdinal("MessageTimeOut"));

                                 
                            }

                            return objVMSDevice;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
