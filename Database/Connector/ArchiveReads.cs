using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;

namespace Ramp.Database.Connector
{
    public class ArchiveReads
    {
        //public DateTime Date;
        //string TruckID;
        //DateTime REDS = DateTime.MinValue;
        //DateTime Queue = DateTime.MinValue;
        //DateTime Entry1 = DateTime.MinValue;
        //DateTime Entry2 = DateTime.MinValue;
        //string HopperName;
        //DateTime HopperTime = DateTime.MinValue;
        //DateTime TruckWash = DateTime.MinValue;
        //DateTime ExitTime = DateTime.MinValue;

        public static int UploadOfflineTransactions(DataTable dtAdd)
        {
            try
            {
                int result = 0;

                SqlCommand command = new SqlCommand();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "ArchiveReads_AddOfflineTransactions";

                command.Parameters.Add("@TransDate", SqlDbType.DateTime, 0, "TransDate");
                command.Parameters.Add("@TruckID", SqlDbType.VarChar, 0, "TruckID");
                command.Parameters.Add("@REDS", SqlDbType.DateTime, 0, "REDS");
                command.Parameters.Add("@Queue", SqlDbType.DateTime, 0, "Queue");
                command.Parameters.Add("@Entry1", SqlDbType.DateTime, 0, "Entry1");
                command.Parameters.Add("@Entry2", SqlDbType.DateTime, 0, "Entry2");
                command.Parameters.Add("@HopperName", SqlDbType.VarChar, 0, "HopperName");
                command.Parameters.Add("@HopperTime", SqlDbType.DateTime, 0, "HopperTime");
                command.Parameters.Add("@TruckWash", SqlDbType.DateTime, 0, "TruckWash");
                command.Parameters.Add("@ExitTime", SqlDbType.DateTime, 0, "ExitTime");

                command.Parameters.Add("@TransStatus", SqlDbType.Int, 0, "TransStatus");
                command.Parameters.Add("@TransMessage", SqlDbType.VarChar, 0, "TransMessage");


                SqlDataAdapter da = (SqlDataAdapter)DatabaseFacade.SQLDB.GetDataAdapter();

                da.InsertCommand = command;

                command.Connection = (SqlConnection)DatabaseFacade.SQLDB.CreateConnection();

                da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

                da.UpdateBatchSize = 100;

                result = da.Update(dtAdd);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
