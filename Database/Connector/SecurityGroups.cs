using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using System.Data;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Connector.DBConnector;

namespace Ramp.Database.Connector
{
    public class SecurityGroups : ISecurityGroups 
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }

        public static List<ISecurityGroups> GetSecurityGroups()
        {
            List<ISecurityGroups> lstSecurityGroup = null;
            ISecurityGroups objSecurityGroups;
            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "SecurityGroups_GetSecutityGroups";    
                

                using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                {
                    if (reader != null && reader.HasRows)
                    {
                        lstSecurityGroup = new List<ISecurityGroups>();
                        while (reader.Read())
                        {
                            objSecurityGroups = new SecurityGroups();
                            if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                objSecurityGroups.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("Description")))
                                objSecurityGroups.Description = reader.GetString(reader.GetOrdinal("Description"));
                            if (!reader.IsDBNull(reader.GetOrdinal("Level")))
                                objSecurityGroups.Level = reader.GetInt32(reader.GetOrdinal("Level"));

                            lstSecurityGroup.Add(objSecurityGroups);
                        }
                        
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstSecurityGroup;
        }

    }
}
