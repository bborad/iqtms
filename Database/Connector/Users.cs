using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using System.Data;
using DBModule = Ramp.Database.Connector;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.Database.Connector
{
    public class Users : IUsers
    {
	
        #region "private variables"
        public int ID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int SecurityGroupID { get; set; }
        public string EmailID { get; set; }
        #endregion

        #region "Public Static Methods"

        /// <summary>
        /// Get data for user to login if usename and Password exists
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static IUsers UserLoginCheck(string Username, string Password)
        {
            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Users_UserLoginCheck";
                command.Parameters.Add("Username", SqlDbType.NVarChar);
                command.Parameters["Username"].Value = Username;
                command.Parameters.Add("Password", SqlDbType.NVarChar);
                command.Parameters["Password"].Value = Password;

                //  DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                IUsers objUser;

                using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                {
                    if (reader != null && reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            objUser = new Users();
                            if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                objUser.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("UserID")))
                                objUser.UserID = reader.GetString(reader.GetOrdinal("UserID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("UserName")))
                                objUser.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                            if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                                objUser.Password = reader.GetString(reader.GetOrdinal("Password"));
                            if (!reader.IsDBNull(reader.GetOrdinal("SecurityGroupID")))
                                objUser.SecurityGroupID = reader.GetInt32(reader.GetOrdinal("SecurityGroupID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("EmailID")))
                                objUser.EmailID = reader.GetString(reader.GetOrdinal("EmailID"));
                            return objUser;
                        }
                        return null;
                    }
                    else
                        return null;
                }
            }

            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="EmailID"></param>
        /// <returns></returns>
        public static IUsers GetPasswordByUserNameEmailID(string Username, string EmailID)
        {
            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Users_GetPasswordByUserNameEmailID";
                command.Parameters.Add("Username", SqlDbType.NVarChar);
                command.Parameters["Username"].Value = Username;
                command.Parameters.Add("EmailID", SqlDbType.NVarChar);
                command.Parameters["EmailID"].Value = EmailID;

                //   DatabaseFacade.SQLDB.ExecuteNonQuery(command);
                IUsers objUser;

                using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                {
                    if (reader != null && reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            objUser = new Users();
                            if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                objUser.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("UserID")))
                                objUser.UserID = reader.GetString(reader.GetOrdinal("UserID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("UserName")))
                                objUser.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                            if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                                objUser.Password = reader.GetString(reader.GetOrdinal("Password"));
                            if (!reader.IsDBNull(reader.GetOrdinal("SecurityGroupID")))
                                objUser.SecurityGroupID = reader.GetInt32(reader.GetOrdinal("SecurityGroupID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("EmailID")))
                                objUser.EmailID = reader.GetString(reader.GetOrdinal("EmailID"));
                            return objUser;
                        }
                        return null;
                    }
                    else
                        return null;
                }
            }

            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable GetAllUsers()
        {
            DataTable dtUsers = null;
            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Users_GetUsers";
                command.Parameters.Add("Option", SqlDbType.Int);

                command.Parameters["Option"].Value = 0;

                DataSet dsUsers = null;

                dsUsers = DatabaseFacade.SQLDB.ExecuteDataSet(command);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    dtUsers = dsUsers.Tables[0];
                }

            }

            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return dtUsers;
        }

        public int AddUser()
        {
            int result = 0;

            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Users_AddUser";
                command.Parameters.Add("UserName", SqlDbType.VarChar);
                command.Parameters.Add("UserID", SqlDbType.VarChar);
                command.Parameters.Add("EmailID", SqlDbType.VarChar);
                command.Parameters.Add("SecurityGroupID", SqlDbType.Int);
                command.Parameters.Add("Password", SqlDbType.VarChar);

                command.Parameters["UserName"].Value = UserName;
                command.Parameters["UserID"].Value = UserID;
                command.Parameters["EmailID"].Value = EmailID;
                command.Parameters["SecurityGroupID"].Value = SecurityGroupID;
                command.Parameters["Password"].Value = Password;

                object objResult = DatabaseFacade.SQLDB.ExecuteScalar(command);

                if (objResult != null)
                {
                    result = Convert.ToInt32(objResult);
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            return result;
        }

        public int UpdateUser()
        {
            int result = 0;

            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Users_UpdateUser";
                command.Parameters.Add("ID", SqlDbType.VarChar);
                command.Parameters["ID"].Value = ID;
                command.Parameters.Add("UserName", SqlDbType.VarChar);
                command.Parameters["UserName"].Value = UserName;
                command.Parameters.Add("SecurityGroupID", SqlDbType.Int);
                command.Parameters["SecurityGroupID"].Value = SecurityGroupID;
                command.Parameters.Add("EmailID", SqlDbType.VarChar);
                command.Parameters["EmailID"].Value = EmailID;

                object objResult = DatabaseFacade.SQLDB.ExecuteScalar(command);

                if (objResult != null)
                {
                    result = Convert.ToInt32(objResult);
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            return result;
        }


        public static bool IsDuplicateUserName(string userid)
        {
            bool flagResult = false;
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Users_IsDuplicateUserName";

                    command.Parameters.Add("UserID", SqlDbType.NVarChar);
                    command.Parameters["UserID"].Value = userid;

                    DataSet dsUsers = DatabaseFacade.SQLDB.ExecuteDataSet(command);
                    if (dsUsers != null && dsUsers.Tables.Count > 0)
                    {
                        flagResult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flagResult;
        }

        public static IUsers GetUserDetail(int ID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Users_GetUserDetails";

                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;

                    IUsers objUser;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                objUser = new Users();
                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objUser.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("UserID")))
                                    objUser.UserID = reader.GetString(reader.GetOrdinal("UserID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("UserName")))
                                    objUser.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                                if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                                    objUser.Password = reader.GetString(reader.GetOrdinal("Password"));
                                if (!reader.IsDBNull(reader.GetOrdinal("SecurityGroupID")))
                                    objUser.SecurityGroupID = reader.GetInt32(reader.GetOrdinal("SecurityGroupID"));
                                if (!reader.IsDBNull(reader.GetOrdinal("EmailID")))
                                    objUser.EmailID = reader.GetString(reader.GetOrdinal("EmailID"));
                                return objUser;
                            }
                            return null;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

    }
}
