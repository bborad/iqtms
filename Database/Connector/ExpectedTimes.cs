using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.Database.Connector
{
    public class ExpectedTimes : IExpectedTimes
    {
        #region "Public Static Methods"


        public static DataSet GetDateRange()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_GetDateRange";

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public static DataSet GetReport(int HopperID, int TableID, int ID)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExpectedTimes_GetReport";

                    command.Parameters.Add("HopperID", SqlDbType.Int);
                    command.Parameters["HopperID"].Value = HopperID;

                    command.Parameters.Add("TableID", SqlDbType.Int);
                    command.Parameters["TableID"].Value = TableID;

                    command.Parameters.Add("ID", SqlDbType.Int);
                    command.Parameters["ID"].Value = ID;

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }






        #endregion

    }
}
