using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ramp.MiddlewareController.Connector.DBConnector;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;

using NLog;

namespace Ramp.Database.Connector
{
    public class Markers : IMarkers
    {
        #region "private variables"
        public int ID { get; set; }
        public string MarkerLoopID { get; set; }
        public string IPAddress { get; set; }
        //public Int32 MarkerPosition { get; set; }
        public string MarkerPosition { get; set; }
        public Int32 Location { get; set; }

        #endregion

        #region "Public Methods"
        /// <summary>
        /// Insert Marker 
        /// </summary>
        /// <param name="ReaderName"></param>
        /// <param name="IPAddress"></param>
        /// <param name="PortNo"></param>
        /// <param name="MarkerIDs"></param>
        /// <returns></returns>
        //public int InsertMarker(string MarkerLoopID, int IPAddress, string MarkerPosition, string Location)
        public int InsertMarker(string MarkerLoopID, string MarkerPosition, int Location, string SerialNo)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_InsertMarker";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("MarkerLoopID", SqlDbType.NVarChar);
                    command.Parameters["MarkerLoopID"].Value = MarkerLoopID;
                    //command.Parameters.Add("IPAddress", SqlDbType.NVarChar);
                    //command.Parameters["IPAddress"].Value = IPAddress;
                    command.Parameters.Add("MarkerPosition", SqlDbType.NVarChar);
                    command.Parameters["MarkerPosition"].Value = MarkerPosition;
                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = Location;

                    command.Parameters.Add("SerialNumber", SqlDbType.VarChar);
                    command.Parameters["SerialNumber"].Value = SerialNo;

                    int MarkerID = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return MarkerID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Delete Markers by ID (Marker ID in CSV )
        /// </summary>
        /// <param name="MarkerIDs"></param>
        /// <returns></returns>
        public int DeleteMarkerByID(string MarkerIDs)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_DeleteMarkerByID";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("MarkerIDs", SqlDbType.NVarChar);
                    command.Parameters["MarkerIDs"].Value = MarkerIDs;

                    int MarkerID = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return MarkerID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }


        #endregion

        #region "Public Static Methods"

        public static List<IMarkers> GetMarkersAtLocation(Location loc)
        {
            List<IMarkers> lstMarkers = new List<IMarkers>();
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_GetMarkersAtLocation";

                    command.Parameters.Add("Location", SqlDbType.Int);
                    command.Parameters["Location"].Value = Convert.ToInt32(loc);

                   

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            Markers objMarker;

                            while (reader.Read())
                            {
                                objMarker = new Markers();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objMarker.ID = reader.GetInt32(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerLoopID")))
                                    objMarker.MarkerLoopID = reader.GetString(reader.GetOrdinal("MarkerLoopID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objMarker.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerPosition")))
                                    objMarker.MarkerPosition = reader.GetString(reader.GetOrdinal("MarkerPosition"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objMarker.Location = reader.GetInt32(reader.GetOrdinal("Location"));

                                lstMarkers.Add(objMarker);
                            }

                            //return lstMarkers;
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
               // throw;
            }
            return lstMarkers;
        }

        /// <summary>
        /// Get All Markers
        /// </summary>
        /// <returns></returns>
        public static List<IMarkers> GetAllMarkers()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_GetAllMarkers";

                    List<IMarkers> lstMarkers = new List<IMarkers>();

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            Markers objMarker;

                            while (reader.Read())
                            {
                                objMarker = new Markers();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    objMarker.ID = reader.GetInt32(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerLoopID")))
                                    objMarker.MarkerLoopID = reader.GetString(reader.GetOrdinal("MarkerLoopID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("IPAddress")))
                                    objMarker.IPAddress = reader.GetString(reader.GetOrdinal("IPAddress"));

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerPosition")))
                                    objMarker.MarkerPosition = reader.GetString(reader.GetOrdinal("MarkerPosition"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Location")))
                                    objMarker.Location = reader.GetInt32(reader.GetOrdinal("Location"));

                                lstMarkers.Add(objMarker);
                            }

                            return lstMarkers;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// To Get all the records for system status report
        /// </summary>
        /// <returns></returns>
        public static DataSet GetLoopStatusForSystemReport()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    DataSet ds = new DataSet();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_GetLoopStatusForSystemReport";

                    return DatabaseFacade.SQLDB.ExecuteDataSet(command);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static DataTable GetMarkersInfo(out int markerHealthCheck, out int LogFileClearingInterval)
        {
            DataTable dtMarkers = null;
            try
            {
                using (SqlCommand command = new SqlCommand())
                {

                    dtMarkers = new DataTable("dtMarkers");

                    DataColumn[] dcc = new DataColumn[] { new DataColumn("SerialNo"),
                                                           new DataColumn("LoopId")
                                                        };

                    dtMarkers.Columns.AddRange(dcc);

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_GetMarkersInfo";

                    command.Parameters.Add("@MarkerHealthCheckFlag", SqlDbType.VarChar);
                    command.Parameters["@MarkerHealthCheckFlag"].Size = 10;
                    command.Parameters["@MarkerHealthCheckFlag"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("@LogFileClearingInterval", SqlDbType.VarChar);
                    command.Parameters["@LogFileClearingInterval"].Size = 10;
                    command.Parameters["@LogFileClearingInterval"].Direction = ParameterDirection.Output;

                                      
                    DataRow dr;

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        { 
                            while (reader.Read())
                            {
                                dr = dtMarkers.NewRow();

                                if (!reader.IsDBNull(reader.GetOrdinal("MarkerLoopID")))
                                    dr["LoopId"] = reader.GetString(reader.GetOrdinal("MarkerLoopID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber")))
                                    dr["SerialNo"] = reader.GetString(reader.GetOrdinal("SerialNumber"));

                                dtMarkers.Rows.Add(dr);
                            } 
                        }
                        
                    }

                    object value = command.Parameters["@MarkerHealthCheckFlag"].Value;

                    if (value != null)
                    {
                        markerHealthCheck = Convert.ToInt32(value);
                    }
                    else
                    {
                        markerHealthCheck = -1;
                    }

                    value = command.Parameters["@LogFileClearingInterval"].Value;

                    if (value != null)
                    {
                        LogFileClearingInterval = Convert.ToInt32(value);
                    }
                    else
                    {
                        LogFileClearingInterval = -1;
                    }

                }
            }
            catch (Exception ex)
            {
                markerHealthCheck = -1;
                throw ex;
            }
            return dtMarkers;
        }

        /// <summary>
        /// Get All Markers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetMarkers()
        {
            DataTable dtMarkers = null;
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Markers_GetMarkers";

                   DataSet ds = DatabaseFacade.SQLDB.ExecuteDataSet(command);

                   if (ds != null && ds.Tables[0] != null)
                   {
                       dtMarkers = ds.Tables[0];
                   }                   
                    
                }
            }
            catch (Exception ex)
            {
                
            }
            return dtMarkers;
        }

        #endregion
    }
}
