using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ramp.DataAccess.Data;
using Ramp.MiddlewareController.Common;
using Ramp.MiddlewareController.Connector.DBConnector;

using NLog;

namespace Ramp.Database.Connector
{
    public class Locations : ILocations
    {
        #region "private variables"
        public int ID { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "public methods"

        public int InsertLocation(string LocationName, string Description)
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Locations_InsertLocation";
                    ///ADD Parameters
                    ///                 
                    command.Parameters.Add("LocationName", SqlDbType.NVarChar);
                    command.Parameters["LocationName"].Value = LocationName;
                    command.Parameters.Add("Description", SqlDbType.NVarChar);
                    command.Parameters["Description"].Value = Description;
                    //command.Parameters.Add("IsDeleted", SqlDbType.Bit);
                    //command.Parameters["IsDeleted"].Value = IsDeleted;
                    int ID = Convert.ToInt32(DatabaseFacade.SQLDB.ExecuteScalar(command));
                    return ID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }
        #endregion

        #region "public static methods"

        /// <summary>
        /// Get All Locations
        /// </summary>
        /// <returns></returns>
        public static List<ILocations> GetAllLocations()
        {
            try
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Locations_GetAllLocations";

                    List<ILocations> lstLocations = new List<ILocations>();

                    using (SqlDataReader reader = (SqlDataReader)DatabaseFacade.SQLDB.ExecuteReader(command))
                    {
                        if (reader != null && reader.HasRows)
                        {
                            Locations lstLocation;

                            while (reader.Read())
                            {
                                lstLocation = new Locations();

                                if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                                    lstLocation.ID = reader.GetInt32(reader.GetOrdinal("ID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("LocationName")))
                                    lstLocation.LocationName = reader.GetString(reader.GetOrdinal("LocationName"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Description")))
                                    lstLocation.Description = reader.GetString(reader.GetOrdinal("Description"));

                                if (!reader.IsDBNull(reader.GetOrdinal("IsDeleted")))
                                    lstLocation.IsDeleted = reader.GetBoolean(reader.GetOrdinal("IsDeleted"));

                                lstLocations.Add(lstLocation);
                            }

                            return lstLocations;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion


    }
}
